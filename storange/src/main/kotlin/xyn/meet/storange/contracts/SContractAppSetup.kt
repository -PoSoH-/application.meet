package xyn.meet.storange.contracts

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import xyn.meet.storange.models.EntityApplicationSetup

object SContractAppSetup {
    const val fetchAppSetup = "SELECT * FROM ${ContractTableRoom.application_setup}"
}

//    const val insertAppSetup = "SELECT * FROM ${ContractTableRoom.application_setup} WHERE appSetupId = :appSetupId"
//    const val deleteAppSetup = "DELETE FROM ${ContractTableRoom.application_setup} WHERE appSetupId = :appSetupId"

//    @Query(.fetch)
//    fun fetchAppSetup(): List<EntityApplicationSetup>
//    @Insert(entity = EntityApplicationSetup::class, onConflict = OnConflictStrategy.REPLACE)
//    fun inserdAppSetup(cardEntity: EntityApplicationSetup)
//    @Delete(entity = EntityApplicationSetup::class)
//    fun deleteAppSetup(cardEntity: EntityApplicationSetup)
//}