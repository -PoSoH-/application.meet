package xyn.meet.storange.contracts

class ContractTableRoom {
    companion object{
        //name database
        const val database = "_app_sumra_meet_.db"
        // enable application setup table
        const val application_setup            = "_application_setup_"
        // session table
        const val meet_tbl_session             = "_table_session_"
        // history table for all meetings
        const val meet_tbl_history             = "_table_history_"
        // meeting parameter table
        const val meet_tbl_meet_parameter      = "_table_meet_parameter_"
        // enable chat of meeting table
        const val meet_tbl_meet_chat           = "_table_meet_chat_"

//        const val table_customers_subscription = "_table_customers_subscription_"
//        const val table_customers_tax_ids      = "_table_customers_tax_ids_"
//        const val table_customers_sources      = "_table_customers_sources_"
//        // account table
//        const val table_accounts               = "_table_accounts_"
//        // card table
//        const val table_cards                  = "_table_cards_"
//        // pairs table
//        const val table_pairs                  = "_table_pairs_"
    }
}

