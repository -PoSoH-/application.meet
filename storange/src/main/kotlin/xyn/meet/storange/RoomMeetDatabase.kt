package xyn.meet.storange


import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import xyn.meet.storange.contracts.ContractTableRoom
import xyn.meet.storange.dao.*
import xyn.meet.storange.models.*
import java.util.*

@Database(
    entities = [
        SessionEntity::class,
        HistoryEntity::class,
        ParametersEntity::class,
        MeetChatEntity::class,
        EntityApplicationSetup::class]
    , version = 3
    , exportSchema = true
)
abstract class RoomMeetDatabase: RoomDatabase() {
    abstract fun sessionDao()       : SessionDao
    abstract fun historyDao()       : HistoryDao
    abstract fun meetParametersDao(): MeetParameterDao
    abstract fun meetСhatDao()      : MeetChatDao
    abstract fun applicationSetup() : IDAOApplicationSetup

    companion object {

//        private val databaseCallback = object : RoomDatabase.Callback() {
//            override fun onCreate(db: SupportSQLiteDatabase) {
//                super.onCreate(db)
//                Log.d("RoomMeetDatabase", "onCreate")
////            CoroutineScope(Dispatchers.IO).launch {
////                addSampleBooksToDatabase()
////            }
//            }
//        }

        fun buildDataSource(context: Context): RoomMeetDatabase = Room.databaseBuilder(
            context
            , RoomMeetDatabase::class.java
            , ContractTableRoom.database
        )
            .addMigrations(MIGRATION_1_2, MIGRATION_1_3)
            .fallbackToDestructiveMigration()
            .build()
    }
}

val MIGRATION_1_2: Migration = object : Migration(1, 2) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.run {
            execSQL("ALTER TABLE user ADD COLUMN phone TEXT DEFAULT '' NOT NULL")
        }
    }
}

val MIGRATION_1_3: Migration = object : Migration(2, 3) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.run {
//            execSQL("CREATE TABLE ${ContractTableRoom.application_setup} state DEFAULT true NOT NULL")

            execSQL("CREATE TABLE ${ContractTableRoom.application_setup} (id STRING PRIMARY KEY NOT NULL," +
                    "stateVideo BOOL NOT NULL, " +
                    "stateVoice BOOL NOT NULL, " +
                    "stateDarkMode BOOL NOT NULL, " +
                    "stateAdditional BOOL NOT NULL," +
                    "stateA BOOL NOT NULL" +
                    "stateB BOOL NOT NULL" +
                    "stateC BOOL NOT NULL" +
                    "stateD BOOL NOT NULL)")

            execSQL("INSERT INTO ${ContractTableRoom.application_setup} (" +
                    "primaryID, " +
                    "stateVideo, " +
                    "stateVoice, " +
                    "stateDarkMode, " +
                    "stateAdditional, " +
                    "phone)" +
                    "SELECT primaryID ${UUID.randomUUID()}, " +
                    "SELECT stateVideo true," +
                    "SELECT stateVoice true," +
                    "SELECT stateDarkMode false," +
                    "SELECT stateAdditional false," +
                    "SELECT stateA false" +
                    "SELECT stateB false" +
                    "SELECT stateC false" +
                    "SELECT stateD false)")
        }
    }
}

//val MIGRATION_1_4: Migration = object : Migration(3, 4) {
//    override fun migrate(database: SupportSQLiteDatabase) {
//        database.run {
//            execSQL("ALTER TABLE ${ContractTableRoom.application_setup}")
//        }
//    }
//}

//val MIGRATION_2_3: Migration = object : Migration(2, 3) {
//    override fun migrate(database: SupportSQLiteDatabase) {
//        database.run {
//            execSQL("CREATE TABLE user_new (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
//                    "account_id INTEGER NOT NULL, username TEXT NOT NULL, email TEXT NOT NULL, address TEXT NOT NULL," +
//                    "phone TEXT NOT NULL)")
//            execSQL("INSERT INTO user_new (account_id, username, email, address, phone)" +
//                    "SELECT account_id, username, email, '', phone FROM user")
//            execSQL("DROP TABLE user")
//            execSQL("ALTER TABLE user_new RENAME TO user")
//        }
//    }
//}
//
//val MIGRATION_1_3: Migration = object : Migration(1, 3) {
//    override fun migrate(database: SupportSQLiteDatabase) {
//        database.run {
//            execSQL("CREATE TABLE user_new (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
//                    "account_id INTEGER NOT NULL, username TEXT NOT NULL, email TEXT NOT NULL, address TEXT NOT NULL," +
//                    "phone TEXT NOT NULL)")
//            execSQL("INSERT INTO user_new (account_id, username, email, address, phone)" +
//                    "SELECT account_id, username, email, '', '' FROM user")
//            execSQL("DROP TABLE user")
//            execSQL("ALTER TABLE user_new RENAME TO user")
//        }
//    }
//}