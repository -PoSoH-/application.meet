package xyn.meet.storange.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import xyn.meet.storange.contracts.ContractTableRoom

@Entity(tableName = ContractTableRoom.meet_tbl_session)
data class SessionEntity (
    @PrimaryKey (autoGenerate = false)
    val id: Long? = null,  // this place set id user
    var session: String,
    var code: String,
    var userName: String,
    var homeServer: String,
    var homeUserId: String,
    var avatar: String? // form Base64 format
)