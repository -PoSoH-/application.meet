package xyn.meet.storange.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import xyn.meet.storange.contracts.ContractTableRoom

@Entity(tableName = ContractTableRoom.meet_tbl_history)
data class HistoryEntity (
    @PrimaryKey ( autoGenerate = true)
    val historyId   : Long,
    val meetName    : String,
    val sessionMeet : String,

    val startTimeMeet : Long,
    val finalTimeMeet : Long,

    val meetParameterId : Long,
    val meetTextChatId  : Long
)