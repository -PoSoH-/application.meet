package xyn.meet.storange.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import xyn.meet.storange.contracts.ContractTableRoom

@Entity(tableName = ContractTableRoom.application_setup)
data class EntityApplicationSetup (
    @PrimaryKey val primaryID: String,
    var stateVideo: Boolean = true,
    var stateVoice: Boolean = true,
    var stateDarkMode: Boolean = false,
    var stateAdditional: Boolean = false,
    var stateA: Boolean = false,
    var stateB: Boolean = false,
    var stateC: Boolean = false,
    var stateD: Boolean = false
)