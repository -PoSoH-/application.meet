package xyn.meet.storange.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import xyn.meet.storange.contracts.ContractTableRoom

@Entity(tableName = ContractTableRoom.meet_tbl_meet_parameter)
data class ParametersEntity(
    /**************************************************
     *      Entity for setup meet session params...
     *   a. enable/disable audio about start meet
     *   b. enable/disable video about start meet
     **************************************************/
    @PrimaryKey(autoGenerate = true)
    val meetParamId     : Long,
    val timeStampStart  : Long,
    val timeStampFinal  : Long,
    val stateAudio      : Boolean,
    val stateVideo      : Boolean,
    val meetTextMessage : String
)
