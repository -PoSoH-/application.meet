package xyn.meet.storange.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import xyn.meet.storange.contracts.ContractTableRoom

@Entity(tableName = ContractTableRoom.meet_tbl_meet_chat)
data class MeetChatEntity (
    @PrimaryKey (autoGenerate = true)
    val meetChatId : Long,
    val linkMeetId : Long,
    val startChatTime : Long,
    val finalChatTime : Long,
    val messageChat: String
)