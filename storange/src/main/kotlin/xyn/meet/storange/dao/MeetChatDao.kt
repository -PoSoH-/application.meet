package xyn.meet.storange.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import xyn.meet.storange.contracts.MeetChatContract
import xyn.meet.storange.models.MeetChatEntity

@Dao
interface MeetChatDao {

    @Query(MeetChatContract.fetchAllChatWithMeet)
    fun fetchAllChatWithMeet(meetId: Long): List<MeetChatEntity>

    @Query(MeetChatContract.fetchChat)
    fun fetchWithId(chatId: Long): MeetChatEntity

    @Insert(entity = MeetChatEntity::class, onConflict = OnConflictStrategy.IGNORE)
    fun insertChat(chat: MeetChatEntity)
}