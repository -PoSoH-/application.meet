package xyn.meet.storange.dao

import androidx.room.*
import xyn.meet.storange.contracts.HistoryContract
import xyn.meet.storange.models.HistoryEntity

@Dao
interface HistoryDao {

    @Query(HistoryContract.fetchAll)
    fun fetchAllHistory(): List<HistoryEntity>

    @Query(HistoryContract.fetch)
    fun fetchHistory(hId: Long): HistoryEntity

    @Insert(entity = HistoryEntity::class, onConflict = OnConflictStrategy.REPLACE)
    fun insertHistory(cardEntity: HistoryEntity)

    @Update(entity = HistoryEntity::class)
    fun updateHistory(cardEntity: HistoryEntity)

    @Delete(entity = HistoryEntity::class)
    fun deleteHistory(cardEntity: HistoryEntity)

    @Query(HistoryContract.deleteWithId)
    fun deleteHistoryWithId(hId: Long)

}
