package xyn.meet.storange.dao

import androidx.room.*
import xyn.meet.storange.contracts.SessionContract
import xyn.meet.storange.models.SessionEntity

@Dao
interface SessionDao {

    @Query(SessionContract.fetch)
    fun fetchSession(): List<SessionEntity>

    @Insert(entity = SessionEntity::class, onConflict = OnConflictStrategy.REPLACE)
    fun insertSession(cardEntity: SessionEntity)

    @Delete(entity = SessionEntity::class)
    fun deleteSession(cardEntity: SessionEntity)


}