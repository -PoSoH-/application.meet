package xyn.meet.storange.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import xyn.meet.storange.contracts.MeetParamsContract
import xyn.meet.storange.models.ParametersEntity

@Dao
interface MeetParameterDao {
    @Query(MeetParamsContract.fetchParams)
    fun fetchWithId(parameterId: Long): ParametersEntity

    @Insert(entity = ParametersEntity::class, onConflict = OnConflictStrategy.REPLACE)
    fun insertParameter(parameterId: ParametersEntity)
}