package xyn.meet.storange.dao

import androidx.room.*
import xyn.meet.storange.contracts.SContractAppSetup
import xyn.meet.storange.contracts.SessionContract
import xyn.meet.storange.models.EntityApplicationSetup
import xyn.meet.storange.models.SessionEntity

@Dao
interface IDAOApplicationSetup {

    @Query(SContractAppSetup.fetchAppSetup)
    fun fetchAppSetup(): EntityApplicationSetup

    @Insert(entity = EntityApplicationSetup::class, onConflict = OnConflictStrategy.REPLACE)
    fun insertAppSetup(cardEntity: EntityApplicationSetup)

    @Delete(entity = EntityApplicationSetup::class)
    fun deleteAooSetup(cardEntity: EntityApplicationSetup)
}