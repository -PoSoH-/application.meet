/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.api.failure

import sdk.net.meet.api.MeetCallback

fun <A> Result<A>.foldToCallback(callback: MeetCallback<A>): Unit = fold(
        { callback.onSuccess(it) },
        { callback.onFailure(it) }
)
