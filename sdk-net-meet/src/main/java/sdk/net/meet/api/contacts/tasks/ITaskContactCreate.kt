package sdk.net.meet.api.contacts.tasks

import sdk.net.meet.api.contacts.request.FromAddContact
import sdk.net.meet.api.contacts.response.FetchContactCreate
import sdk.net.meet.net.tasks.Task

interface ITaskContactCreate: Task<ITaskContactCreate.Params, FetchContactCreate> {
    data class Params(
        val authHeaders: String,
        val contentBody: FromAddContact
    )
}

