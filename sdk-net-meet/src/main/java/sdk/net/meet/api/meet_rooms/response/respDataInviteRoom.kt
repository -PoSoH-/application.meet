package sdk.net.meet.api.meet_rooms.response

import com.google.gson.annotations.SerializedName

object respDataInviteRoom {
    data class FetchRoomByInvite(
        @SerializedName("type")
        val type   : String, // "success",
        @SerializedName("title")
        val title  : String, // "Invite user to meeting",
        @SerializedName("message")
        val message: String, // "Information about the invited user was sent successfully",
        @SerializedName("data")
        val data   : RoomByInvite
    )

    data class RoomByInvite(
        @SerializedName("id")
        val id        : String, // "948cf01a-8eee-4aa8-9683-9325aeec0870",
        @SerializedName("name")
        val name      : String, // "testroomq",
        @SerializedName("status")
        val status    : Int,    // 1,
        @SerializedName("invite")
        val invite    : String, // "yxz-ryv-fhz-fu5",
        @SerializedName("room_users")
        val roomUsers: List<RoomByInviteParticipants>?
    )

    data class RoomByInviteParticipants(
        @SerializedName("id")
        val id         : String, // "00000000-1000-1000-1000-000000000000",
        @SerializedName("display_name")
        val displayName: String, // "Prof. Herman Prohaska MD",
        @SerializedName("phone")
        val phone      : String, // "+18102148983",
        @SerializedName("email")
        val email      : String  // "bradford12@example.net"
    )
}

/*** Example response from server
{
    "type": "success",
    "title": "Invite user to meeting",
    "message": "Information about the invited user was sent successfully",
    "data": {
        "id": "948cf01a-8eee-4aa8-9683-9325aeec0870",
        "name": "testroomq",
        "status": 1,
        "invite": "yxz-ryv-fhz-fu5",
        "room_users": [
            {
                "id": "00000000-1000-1000-1000-000000000000",
                "display_name": "Prof. Herman Prohaska MD",
                "phone": "+18102148983",
                "email": "bradford12@example.net"
            }
        ]
    }
}
***/