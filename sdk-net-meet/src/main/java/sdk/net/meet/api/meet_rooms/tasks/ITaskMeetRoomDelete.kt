package sdk.net.meet.api.meet_rooms.tasks

import sdk.net.meet.api.meet_rooms.TaskRespMeetRoom
import sdk.net.meet.api.meet_rooms.request.FromMeetRooms
import sdk.net.meet.net.tasks.Task

interface ITaskMeetRoomDelete: Task<ITaskMeetRoomDelete.Params, TaskRespMeetRoom> {
    data class Params(
        internal val authParams: String,
        internal val appId: String,
        internal val roomId: String
    )
}