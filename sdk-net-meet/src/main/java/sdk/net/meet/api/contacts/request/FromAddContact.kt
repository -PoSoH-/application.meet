package sdk.net.meet.api.contacts.request

import com.google.gson.annotations.SerializedName

data class FromAddContact(
    @SerializedName("prefix_name")
    val prefixName: String? = null,
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("middle_name")
    val middleName: String? = null,
    @SerializedName("last_name")
    val lastName: String,
    @SerializedName("suffix_name")
    val suffixName: String? = null,
    @SerializedName("display_name")
    val displayName: String? = null,
    @SerializedName("nickname")
    val nickname: String? = null,
    @SerializedName("birthday")
    val birthday: String? = null, // "1984-10-25",
    @SerializedName("avatar")
    val avatar: String? = null, //"",
    @SerializedName("note")
    val note: String? = null, // "",
    @SerializedName("is_favorite")
    val isFavorite: Boolean = false,
    @SerializedName("phones")
    val phones: List<UserPhone>,
    @SerializedName("emails")
    val emails: List<UserEmail>
){
    data class UserEmail(
        @SerializedName("email")
        val email: String, //"test@tes.com",
        @SerializedName("type")
        val type: String, //"home",
        @SerializedName("is_default")
        val isDefault: Boolean = true
    )
    data class UserPhone(
        @SerializedName("phone")
        val phone: String, //"test@tes.com",
        @SerializedName("type")
        val type: String, //"home",
        @SerializedName("is_default")
        val isDefault: Boolean = true
    )
}





//{
//    "prefix_name": "",
//    "first_name": "",
//    "middle_name": "",
//    "last_name": "",
//    "suffix_name": "",
//    "write_as_name": "",
//    "nickname": "",
//    "birthday": "1984-10-25",
//    "avatar": "",
//    "note": "",
//    "is_favorite": false,
//    "phones": [
//    {
//        "phone": "(555)-777-1234",
//        "type": "home",
//        "is_default": false
//    }
//    ],
//    "emails": [
//    {
//        "email": "test@tes.com",
//        "type": "home",
//        "is_default": true
//    }
//    ],
//
//}
