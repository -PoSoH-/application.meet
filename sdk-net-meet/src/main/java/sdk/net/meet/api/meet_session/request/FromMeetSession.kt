package sdk.net.meet.api.meet_session.request

import com.google.gson.annotations.SerializedName

data class FromMeetSession (
    @SerializedName("room_id")
    val roomId: String,
)