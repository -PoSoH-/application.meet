/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.api.failure

sealed class GlobalError {
    data class InvalidToken(val softLogout: Boolean) : GlobalError()
    data class ConsentNotGivenError(val consentUri: String) : GlobalError()
}