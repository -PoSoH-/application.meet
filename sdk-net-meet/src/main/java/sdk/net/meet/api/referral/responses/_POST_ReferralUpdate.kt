package sdk.net.meet.api.referral.responses

import sdk.net.meet.api.referral.objModelReferralCode

sealed class _POST_ReferralUpdate{
    data class Success(val success: objModelReferralCode.ReferralResult)   : _POST_ReferralUpdate()
    data class Failure(val failure: ReferralsFailureObject): _POST_ReferralUpdate()
}
