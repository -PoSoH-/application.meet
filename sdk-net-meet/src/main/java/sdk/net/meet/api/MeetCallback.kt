/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.api

interface MeetCallback<in T> {
    fun onSuccess(data: T) {
        // no-op
    }
    fun onFailure(failure: Throwable) {
        // no-op
    }
}

class NoOpMeetCallback<T> : MeetCallback<T>
