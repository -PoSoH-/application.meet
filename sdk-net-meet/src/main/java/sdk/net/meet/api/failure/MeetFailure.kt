package sdk.net.meet.api.failure


import com.google.gson.annotations.SerializedName

data class MeetFailure(
    @SerializedName("data")
    val failure: DataFailure
)

data class DataFailure(
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String = "danger",
    @SerializedName("title")
    val title: String,
    @SerializedName("data")
    val code: Int? = null
)
