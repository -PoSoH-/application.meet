package sdk.net.meet.api.referral.responses

import sdk.net.meet.api.referral.objModelReferralCode

sealed class _GET_ReferralCodeById{
    data class Success(val success: objModelReferralCode.ReferralResult) : _GET_ReferralCodeById()
    data class Failure(val failure: ReferralsFailureObject): _GET_ReferralCodeById()
}
