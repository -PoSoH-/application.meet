package sdk.net.meet.api.contacts.response

import com.google.gson.annotations.SerializedName

data class FetchContactCreate(
    @SerializedName("type")
    val type: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val saveResult: SaveContact
)

data class SaveContact(
    @SerializedName("prefix_name")
    val prefixName: String, //: "01",
    @SerializedName("first_name")
    val firstName: String, //: "Petro",
    @SerializedName("middle_name")
    val midleName: String, //: "Kunitsa",
    @SerializedName("last_name")
    val lastName: String, //: "Ivanovitch",
    @SerializedName("suffix_name")
    val suffixName: String, //: "02",
    @SerializedName("nickname")
    val nickname: String, //: "Bolgur",
    @SerializedName("birthday")
    val birthday: String, //: "1975-10-25",
    @SerializedName("note")
    val note: String, //: "I cool man",
    @SerializedName("is_favorite")
    val isFavorite: Boolean, //: false,
    @SerializedName("write_as_name")
    val writeAsName: String, //: "Petro Kunitsa",
    @SerializedName("user_id")
    val userId: String, //: 1000,
    @SerializedName("id")
    val id: String, //: "941249cb-be03-4cc4-a6b0-7e06313955bf",
    @SerializedName("display_name")
    val displayName: String, //: "Petro Kunitsa"
)