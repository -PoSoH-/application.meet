package sdk.net.meet.api.contacts.response

import com.google.gson.annotations.SerializedName

data class ResponseContact (
    @SerializedName("device_name")
    val dName: String,
    @SerializedName("first_name")
    val fName: String,
    @SerializedName("last_name")
    val lName: String,
    @SerializedName("email_address")
    val eMail: String,
    @SerializedName("phone_number")
    val pNumb: String,
    @SerializedName("avatar")
    val photo: String // link of as3 file server amazon
)