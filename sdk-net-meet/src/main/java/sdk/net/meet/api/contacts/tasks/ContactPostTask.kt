package sdk.net.meet.api.contacts.tasks

import sdk.net.meet.api.contacts.response.ResponseContact
import sdk.net.meet.net.tasks.Task

interface ContactPostTask : Task<ContactPostTask.Params, ResponseContact> {
    data class Params(
        val authParam  : String,
        val bodyContent: String
    )
}