package sdk.net.meet.api.meet_session

import sdk.net.meet.api.failure.DataFailure

sealed class TaskRespMeetSession {
    data class Data(val body: Any): TaskRespMeetSession()
    data class Fail(val fail: DataFailure): TaskRespMeetSession()
}