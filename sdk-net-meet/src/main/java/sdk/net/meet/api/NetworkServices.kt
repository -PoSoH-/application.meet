package sdk.net.meet.api

import sdk.net.meet.api.meet_calendar.IWizardMeetCalendar
import sdk.net.meet.api.contacts.ContactWizard
import sdk.net.meet.api.meet_profile.IWizardMeetProfile
import sdk.net.meet.api.referral.ReferralWizard
import sdk.net.meet.api.meet_session.IWizardMeetSession
import sdk.net.meet.api.meet_rooms.IWizardMeetRooms

interface NetworkServices {
    fun getReferralWizard(): ReferralWizard
    fun getContactWizard(): ContactWizard

    fun getWizardProfile(): IWizardMeetProfile
    fun getWizardRooms(): IWizardMeetRooms
    fun getWizardSession(): IWizardMeetSession
    fun getWizardCalendar(): IWizardMeetCalendar
}