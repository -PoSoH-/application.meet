package sdk.net.meet.api.meet_rooms

import sdk.net.meet.api.meet_rooms.request.FromMeetByInvite
import sdk.net.meet.api.meet_rooms.request.FromMeetRooms

interface IWizardMeetRooms {
    suspend fun roomPst(authHeader: String
                        , appId: String
                        , body: FromMeetRooms
    ): TaskRespMeetRoom

//    suspend fun fetchInvite(authHeader: String
//                        , appId: String
//                        , body: FromMeetByInvite
//    ): TaskRespMeetRoom

    suspend fun roomPut(authHeader: String
                        , appId: String
                        , roomId: String
                        , body: FromMeetRooms
    ): TaskRespMeetRoom

    suspend fun roomDel(authHeader: String
                        , appId: String
                        , roomId: String
    ): TaskRespMeetRoom
}