/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.api.contacts

import sdk.net.meet.api.contacts.request.FromAddContact
import sdk.net.meet.api.contacts.request.FromImportContactByJson
import sdk.net.meet.api.contacts.tasks.TaskAllContacts
import sdk.net.meet.api.contacts.tasks.TaskCategory

interface ContactWizard {
    suspend fun fetchContactCategories(authHeader: String): TaskCategory
    suspend fun fetchAllContacts (authHeader: String, appId: String, config: ApiGetConfig): TaskAllContacts

    suspend fun fetchContactsPagination(authHeader: String
                                        , limit: Int
                                        , page: Int
                                        , search: String
                                        , isFavorite: Boolean
                                        , isRecently: Boolean
                                        , byLetter: String
                                        , groupId : String
                                        , sortBy: String
                                        , sortOrder: String): _ResponseContacts
    suspend fun fetchContactsById    (authHeader: String, contactId: String): _ResponseContacts

    suspend fun createContact (authHeader: String, modelContact: FromAddContact): _ResponseContacts
    suspend fun addImportContactsByJson (authHeader: String, jsonContact: FromImportContactByJson): _ResponseContacts

}