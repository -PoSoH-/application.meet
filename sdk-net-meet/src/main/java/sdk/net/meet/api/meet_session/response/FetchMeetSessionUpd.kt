package sdk.net.meet.api.meet_session.response

import com.google.gson.annotations.SerializedName

data class FetchMeetSessionUpd (
    @SerializedName("type")
    val type: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val body: FinalSession
)

data class FinalSession(
    @SerializedName("id")
    val id: String,
    @SerializedName("user_id")
    val userId: String,
    @SerializedName("room_id")
    val roomId: String,
    @SerializedName("status")
    val status: Int,
    @SerializedName("start_time")
    val startTime: String,
    @SerializedName("finish_time")
    val finishTime: String,
    @SerializedName("deleted_at")
    val deletedAt: String
)


/**
{
    "type": "success",
    "title": "History was success",
    "message": "Create history was successful",
    "data": {
        "id": "941ec0b8-48b3-402b-94b6-b0bc509adb2f",
        "user_id": "1000",
        "room_id": "941ea5d7-1e7d-413e-9334-b405c7133612",
        "status": 0,
        "start_time": "2021-08-10 12:20:32",
        "finish_time": "2021-08-10T12:52:23.344283Z",
        "deleted_at": null
    }
}
*/
