package sdk.net.meet.api.referral.responses

import sdk.net.meet.api.referral.objModelReferralCode

sealed class _GetReferralCodes{
    data class Success(val success: objModelReferralCode.ResultGetReferralCode) : _GetReferralCodes()
    data class Failure(val failure: ReferralsFailureObject): _GetReferralCodes()
}

sealed class _GetReferralCode{
    data class Success(val success: objModelReferralCode.ReferralResult) : _GetReferralCodes()
    data class Failure(val failure: ReferralsFailureObject): _GetReferralCodes()
}