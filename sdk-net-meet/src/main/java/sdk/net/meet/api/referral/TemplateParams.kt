package sdk.net.meet.api.referral


import com.google.gson.annotations.SerializedName

data class TemplateParams(
    @SerializedName("id")
    val id   : Int,
    @SerializedName("html")
    val html : String,
    @SerializedName("title")
    val title: String
)