package sdk.net.meet.api.meet_calendar

import sdk.net.meet.api.failure.DataFailure

sealed class TaskRespCalendar{
    data class Data(val body: Any): TaskRespCalendar()
    data class Fail(val fail: DataFailure): TaskRespCalendar()
}
