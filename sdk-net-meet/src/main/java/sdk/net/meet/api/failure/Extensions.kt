/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.api.failure

import com.google.gson.Gson
import sdk.net.meet.net.network.tryOrNull
import java.io.IOException
import javax.net.ssl.HttpsURLConnection

fun Throwable.is401() =
        this is Failure.OtherServerError
                && httpCode == HttpsURLConnection.HTTP_UNAUTHORIZED /* 401 */
//                && error.code == Failure.MeetError.M_UNAUTHORIZED

//fun Throwable.isTokenError() =
//        this is Failure.ServerError
//                && (error.code == MeetError.M_UNKNOWN_TOKEN || error.code == MeetError.M_MISSING_TOKEN)

fun Throwable.shouldBeRetried(): Boolean {
    return this is Failure.NetworkConnection
            || this is IOException
            || (this is Failure.OtherServerError) // && error.code == MeetError.M_LIMIT_EXCEEDED)
}

fun Throwable.isReferralError(): MeetFailure? {
    return if(this is Failure.OtherServerError
        && httpCode == HttpsURLConnection.HTTP_UNAUTHORIZED /* 401 */
        && httpCode == HttpsURLConnection.HTTP_INTERNAL_ERROR  /* 500 */) {
            tryOrNull {
                Gson().fromJson(errorBody, MeetFailure::class.java)
            }
        } else {
           MeetFailure(DataFailure(message = this.message!!, status = ER_KEY.DANGER, title = "Oh.. Sorry failure"))
        }
}

fun Throwable.isCalendarError(): MeetFailure? {
    return if(this is Failure.OtherServerError
        && httpCode == HttpsURLConnection.HTTP_UNAUTHORIZED  /* 401 */
        && httpCode == HttpsURLConnection.HTTP_INTERNAL_ERROR  /* 500 */) {
        tryOrNull {
            Gson().fromJson(errorBody, MeetFailure::class.java)
        }
    } else {
        MeetFailure(DataFailure(message = this.message!!, status = ER_KEY.DANGER, title = "Oh.. Sorry failure"))
    }
}

fun Throwable.isContactError(): MeetFailure? {
    return if(this is Failure.OtherServerError
        && httpCode == HttpsURLConnection.HTTP_UNAUTHORIZED  /* 401 */
        && httpCode == HttpsURLConnection.HTTP_INTERNAL_ERROR  /* 500 */) {
        tryOrNull {
            Gson().fromJson(errorBody, MeetFailure::class.java)
        }
    } else {
        MeetFailure(DataFailure(message = this.message!!, status = ER_KEY.DANGER, title = "Oh.. Sorry failure"))
    }
}

object ER_KEY{
    const val PRIMARY = "primary"
    const val SECONDARY = "secondary"
    const val SUCCESS = "success"
    const val INFO = "info"
    const val WARNING = "warning"
    const val DANGER = "danger"
    const val LIGHT = "light"
    const val DARK = "dark"
}

//    internal val errDanger = "danger"
//        get() = field
//    internal val errWarning = "warning"
//        get() = field
//    internal val errSuccess = "success"
//        get() = field
//    internal val errInfo = "info"
//        get() = field


//fun Throwable.isContactError(): MeetFailure? {
//    return if ( this is Failure.OtherServerError ) {
//        tryOrNull {
//            Gson().fromJson(errorBody, MeetFailure::class.java)
//        }
//    } else {
//        MeetFailure(DataFailure(message = this.message!!, status = "Other Server Failure...", title = "Oh.. Sorry failure"))
//    }
//}