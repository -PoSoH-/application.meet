package sdk.net.meet.api.meet_profile

import sdk.net.meet.api.meet_profile.request.FromAddProfile
import sdk.net.meet.api.meet_rooms.TaskRespMeetRoom
import sdk.net.meet.api.meet_rooms.request.FromMeetByInvite

interface IWizardMeetProfile {
    suspend fun userProfileGet(authHeader: String
                               , appId: String): TaskProfileResult
    suspend fun userProfilePut(authHeader: String
                               , appId: String
                               , profileId: String
                               , body: FromAddProfile): TaskProfileResult
    suspend fun userProfilePos(authHeader: String
                               , appId: String
                               , body: FromAddProfile): TaskProfileResult
    suspend fun userMeetRoomInvitePos(authHeader: String
                               , appId: String
                               , body: FromMeetByInvite): TaskRespMeetRoom

//    suspend fun userMeetFetchInviteGet( authHeader: String
//                                      , appId: String
//                                      , invite: String): TaskRespMeetRoom



}