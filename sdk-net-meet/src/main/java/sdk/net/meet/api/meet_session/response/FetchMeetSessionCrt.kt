package sdk.net.meet.api.meet_session.response

import com.google.gson.annotations.SerializedName


data class FetchMeetSessionCrt (
    @SerializedName("type")
    val type: String,    //
    @SerializedName("title")
    val title: String,   //
    @SerializedName("message")
    val message: String, //
    @SerializedName("data")
    val body: MeetSession
)

data class MeetSession(
    @SerializedName("user_id")
    val userId: String,    //"1000",
    @SerializedName("room_id")
    val roomId: String,    // "941ea5d7-1e7d-413e-9334-b405c7133612",
    @SerializedName("start_time")
    val startTime: String, //"2021-08-10T12:20:32.279089Z",
    @SerializedName("id")
    val id: String         //"941ec0b8-48b3-402b-94b6-b0bc509adb2f"
)



/**
{
    "type": "success",
    "title": "History was success",
    "message": "Create history was successful",
    "data": {
        "user_id": "1000",
        "room_id": "941ea5d7-1e7d-413e-9334-b405c7133612",
        "start_time": "2021-08-10T12:20:32.279089Z",
        "id": "941ec0b8-48b3-402b-94b6-b0bc509adb2f"
    }
}
*/
