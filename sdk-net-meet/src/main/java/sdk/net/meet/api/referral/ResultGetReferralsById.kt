package sdk.net.meet.api.referral


import com.google.gson.annotations.SerializedName

data class ResultGetReferralsById(
    @SerializedName("code")
    val code: objModelReferralCode.Code
)

