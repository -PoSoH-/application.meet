/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.api.failure

import com.google.gson.annotations.SerializedName
// import com.squareup.moshi.Json
// import com.squareup.moshi.JsonClass

/**
 * This data class holds the error defined by the Meet specifications.
 * You shouldn't have to instantiate it.
 * Ref: https://Meet.org/docs/spec/client_server/latest#api-standards
 */

data class MeetError(
    @SerializedName( "success")
    val success: Boolean? = null,
    @SerializedName("error_message")
    val error: String? = null
) {
    companion object {
        // server error
        const val M_AUTHORIZED = "M_AUTHORIZED"
        const val M_FORBIDDEN = "M_FORBIDDEN"

        // Json error
        const val M_BAD_JSON = "M_BAD_JSON"
        const val M_NOT_JSON = "M_NOT_JSON"
        const val M_NOT_FOUND = "M_NOT_FOUND"

        /** Too many requests have been sent in a short period of time. Wait a while then try again. */
        const val M_LIMIT_EXCEEDED = "M_LIMIT_EXCEEDED"
        const val M_UNABLE_CONVERTER = "M_UNABLE_CONVERTER"

        /** =========================================================================================
         *                    Other error codes the client might encounter are
         ** ========================================================================================= */

        /** Encountered when trying to register a user ID which has been taken. */
        const val M_USER_IN_USE = "M_USER_IN_USE"
    }
}
