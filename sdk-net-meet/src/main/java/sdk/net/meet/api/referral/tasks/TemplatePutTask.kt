package sdk.net.meet.api.referral.tasks

import sdk.net.meet.api.referral.ResultPostTemplate
import sdk.net.meet.net.tasks.Task

internal interface TemplatePutTask: Task<TemplatePutTask.Params, ResultPostTemplate> {
    data class Params(
        val authParam : String,
        val referralID: String,
        val body      : String
    )
}
