package sdk.net.meet.api.contacts.response

import com.google.gson.annotations.SerializedName

data class FetchAllUserContact(
    @SerializedName("current_page")
    val currentPage: Int,
    @SerializedName("first_page_url")
    val firstPageUrl: String?,
    @SerializedName("from")
    val from: Int,
    @SerializedName("last_page")
    val lastPage: Int,
    @SerializedName("last_page_url")
    val lastPageUrl: String?,
    @SerializedName("next_page_url")
    val nextPageUrl: String?,
    @SerializedName("path")
    val path: String,
    @SerializedName("per_page")
    val perPage: Int,
    @SerializedName("prev_page_url")
    val prevPageUrl: String?,
    @SerializedName("to")
    val to: Int,
    @SerializedName("total")
    val total: Int,
    @SerializedName("letters")
    val letters: List<String>,
    @SerializedName("links")
    val links: List<Link>,
    @SerializedName("data")
    val contacts: List<FetchContact>
)

data class FetchContact(
    @SerializedName("avatar")
    var avatar: String = "",
    @SerializedName("birthday")
    val birthday: String,
    @SerializedName("display_name")
    val displayName: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("groups")
    val groups: List<Group>,
    @SerializedName("id")
    val id: String,
    @SerializedName("is_favorite")
    val isFavorite: Boolean,
    @SerializedName("last_name")
    val lastName: String,
    @SerializedName("middle_name")
    val middleName: String?,
    @SerializedName("nickname")
    val nickname: String?,
    @SerializedName("note")
    val note: String?,
    @SerializedName("phone")
    val phone: String?,
    @SerializedName("prefix_name")
    val prefixName: String?,
    @SerializedName("suffix_name")
    val suffixName: String?,
    @SerializedName("user_id")
    val userId: String,
    @SerializedName("write_as_name")
    val writeAsName: String?
)

data class Group(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String
)

data class Link(
    @SerializedName("active")
    val active: Boolean,
    @SerializedName("label")
    val label: String,
    @SerializedName("url")
    val url: String?
)