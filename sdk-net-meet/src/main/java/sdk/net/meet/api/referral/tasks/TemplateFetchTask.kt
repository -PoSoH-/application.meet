package sdk.net.meet.api.referral.tasks

import sdk.net.meet.api.referral.ResultGetTemplate
import sdk.net.meet.net.tasks.Task

internal interface TemplateFetchTask: Task<TemplateFetchTask.Params, ResultGetTemplate> {
    data class Params(
        val authParam: String,
        val limit: Int,
        val page : Int?
    )
}
