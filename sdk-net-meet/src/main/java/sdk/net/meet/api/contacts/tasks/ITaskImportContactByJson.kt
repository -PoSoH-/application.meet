package sdk.net.meet.api.contacts.tasks

import sdk.net.meet.api.contacts.request.FromImportContactByJson
import sdk.net.meet.api.contacts.response.FetchImportContactByJson
import sdk.net.meet.net.tasks.Task

interface ITaskImportContactByJson: Task<ITaskImportContactByJson.Params, FetchImportContactByJson> {
    data class Params(
        val authHeaders: String,
        val contentBody: FromImportContactByJson
    )
}