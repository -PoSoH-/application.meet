package sdk.net.meet.api.contacts.response


import com.google.gson.annotations.SerializedName

data class _GET_ContactList(
    @SerializedName("letters")
    val letters: List<String>,
    @SerializedName("current_page")
    val currentPage: Int,

    @SerializedName("first_page_url")
    val firstPageUrl: String,
    @SerializedName("from")
    val from: Int,
    @SerializedName("last_page")
    val lastPage: Int,
    @SerializedName("last_page_url")
    val lastPageUrl: String,
    @SerializedName("next_page_url")
    val nextPageUrl: String?,
    @SerializedName("path")
    val path: String,
    @SerializedName("per_page")
    val perPage: Int,
    @SerializedName("prev_page_url")
    val prevPageUrl: String?,
    @SerializedName("to")
    val to: Int?,
    @SerializedName("total")
    val total: Int,

    @SerializedName("data")
    val content: List<ResponseContact>?,
    @SerializedName("links")
    val links: List<Link>
)
{
    data class Link(
        @SerializedName("active")
        val active: Boolean,
        @SerializedName("label")
        val label: String,
        @SerializedName("url")
        val url: String?
    )
}