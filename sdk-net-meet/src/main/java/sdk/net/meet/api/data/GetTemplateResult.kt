package sdk.net.meet.api.data

import sdk.net.meet.api.failure.MeetFailure

internal sealed class GetTemplateResult {
    data class Success(val referrals: GetTemplateResult): GetTemplateResult()
    data class Failure(val failure: MeetFailure): GetTemplateResult()
}