/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.api.referral

import sdk.net.meet.api.referral.responses.*

interface ReferralWizard {

    suspend fun sendReferralsInformation(authHeader: String, body: String):_POST_RefCodeInfoSend

    suspend fun fetchAdminTemplate(authHeader: String, limit: Int = 20, page : Int? = 1): ResultGetTemplate
    suspend fun postAdminTemplate (authHeader: String, id: Int, html: String, title: String): ResultPostTemplate


    suspend fun fetchReferralCodes    (authHeader: String, appID: String): _GetReferralCodes
    suspend fun postReferralCodes     (authHeader: String,
                                       body: objModelReferralCode.ReferralCodeParamsPst): _POST_ReferralUpdate
    suspend fun fetchReferralCodeById (authHeader: String, id: String): _GET_ReferralCodeById
    suspend fun updateReferralCodeById(authHeader: String, id: String,
                                       body: objModelReferralCode.ReferralCodeParamsPut): _PUT_ReferralUpdate
    suspend fun deleteReferralCodeById(authHeader: String, id: String): _DEL_ReferralUpdate

    suspend fun fetchReferralCodeNew  (authHeader: String, id: String): _GET_ReferralCodeById
}