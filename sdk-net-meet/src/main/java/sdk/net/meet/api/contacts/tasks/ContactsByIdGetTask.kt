package sdk.net.meet.api.contacts.tasks

import sdk.net.meet.api.contacts.response.ResponseContact
import sdk.net.meet.net.tasks.Task

interface ContactsByIdGetTask : Task<ContactsByIdGetTask.Params, ResponseContact> {
    data class Params(
        val authParam: String,
        val contactId: String
    )
}