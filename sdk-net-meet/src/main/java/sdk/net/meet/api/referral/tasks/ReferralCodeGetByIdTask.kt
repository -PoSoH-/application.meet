package sdk.net.meet.api.referral.tasks

import sdk.net.meet.api.referral.objModelReferralCode
import sdk.net.meet.net.tasks.Task

internal interface ReferralCodeGetByIdTask: Task<ReferralCodeGetByIdTask.Params, objModelReferralCode.ReferralResult> {
    data class Params(
        val authParam : String,
        val referralID: String)
}
