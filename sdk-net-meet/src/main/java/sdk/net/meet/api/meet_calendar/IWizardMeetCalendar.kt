package sdk.net.meet.api.meet_calendar

import sdk.net.meet.api.meet_calendar.request.FromCreateCalendar

interface IWizardMeetCalendar {
    suspend fun calendarGet(authHeader: String
                        , appId: String): TaskRespCalendar
    suspend fun calendarPst(authHeader: String
                        , appId: String
                        , body: FromCreateCalendar): TaskRespCalendar
    suspend fun calendarByIdGet(authHeader: String
                        , appId: String
                        , calendarId: String): TaskRespCalendar
    suspend fun calendarByIdPut(authHeader: String
                        , appId: String
                        , calendarId: String
                        , body: FromCreateCalendar): TaskRespCalendar
    suspend fun calendarByIdDel(authHeader: String
                        , appId: String
                        , calendarId: String): TaskRespCalendar
}