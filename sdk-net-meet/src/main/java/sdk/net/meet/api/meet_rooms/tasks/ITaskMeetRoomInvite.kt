package sdk.net.meet.api.meet_rooms.tasks

import sdk.net.meet.api.meet_rooms.TaskRespMeetRoom
import sdk.net.meet.api.meet_rooms.request.FromMeetByInvite
import sdk.net.meet.net.tasks.Task

interface ITaskMeetRoomInvite: Task<ITaskMeetRoomInvite.Params, TaskRespMeetRoom> {
    data class Params(
        internal val authParams: String,
        internal val appId: String,
        internal val body: FromMeetByInvite,
    )
}

//interface ITaskMeetRoomFetchInvite: Task<ITaskMeetRoomFetchInvite.Params, TaskRespMeetRoom> {
//    data class Params(
//        internal val authParams: String,
//        internal val appId : String,
//        internal val invite: String,
//    )
//}