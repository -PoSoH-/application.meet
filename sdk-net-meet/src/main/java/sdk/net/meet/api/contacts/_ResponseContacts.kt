package sdk.net.meet.api.contacts

import sdk.net.meet.api.failure.Failure

sealed class _ResponseContacts {
    data class _ResultSuccess(val content: Any): _ResponseContacts()
    data class _ResultFailure(val content: Failure): _ResponseContacts()
}