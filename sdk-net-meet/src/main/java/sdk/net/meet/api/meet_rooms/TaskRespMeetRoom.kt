package sdk.net.meet.api.meet_rooms

import sdk.net.meet.api.failure.DataFailure

sealed class TaskRespMeetRoom {
    data class Data(val body: Any): TaskRespMeetRoom()
    data class Fail(val fail: DataFailure): TaskRespMeetRoom()
}