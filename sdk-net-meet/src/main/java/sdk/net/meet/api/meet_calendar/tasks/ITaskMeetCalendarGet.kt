package sdk.net.meet.api.meet_calendar.tasks

import sdk.net.meet.api.meet_calendar.TaskRespCalendar
import sdk.net.meet.api.meet_calendar.response.FetchMeetCalendarAll
import sdk.net.meet.net.tasks.Task

interface ITaskMeetCalendarGet: Task<ITaskMeetCalendarGet.Params, TaskRespCalendar> {
    data class Params(
        val authParam: String,
        val apiID: String,
    )
}