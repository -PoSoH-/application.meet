package sdk.net.meet.api.meet_profile.request

import com.google.gson.annotations.SerializedName

data class FromAddProfile (
    @SerializedName("id")
    val id: String, //: "44444444-a444-q4444-w4444-asd4444444",
    @SerializedName("display_name")
    val displayName: String, //": "Igor",
    @SerializedName("email")
    val email: String, // ": "gug@gug.gug",
    @SerializedName("phone")
    val phone: String // "+12404225709"
)