package sdk.net.meet.api.referral.tasks

import sdk.net.meet.api.referral.responses._POST_RefCodeInfoSend
import sdk.net.meet.net.tasks.Task

internal interface RefCodeInfoLinkPostTask: Task<RefCodeInfoLinkPostTask.Params, _POST_RefCodeInfoSend> {
    data class Params(
        val authParam: String,
        val body     : String
    )
}
