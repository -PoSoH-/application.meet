package sdk.net.meet.api.meet_profile

import sdk.net.meet.api.failure.DataFailure

sealed class TaskProfileResult{
    data class Body(val body: Any): TaskProfileResult()
    data class Fail(val fail: DataFailure): TaskProfileResult()
}