package sdk.net.meet.api.contacts.request

import com.google.gson.annotations.SerializedName

data class FromImportContactByJson (
    @SerializedName("display_name")
    val uNameD  : String,
    @SerializedName("phones")
    val uPhones : List<String>,
    @SerializedName("emails")
    val uEmails : List<String>,
    @SerializedName("avatar")
    val uAvatar : String,
    @SerializedName("is_shared")
    val isShared: Boolean = true
)

