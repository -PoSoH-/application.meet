package sdk.net.meet.api.referral.responses

import com.google.gson.annotations.SerializedName

class RefCodeInfoTemplate (
    @SerializedName("data")
    val response: RefCodeInfoContent
)
class RefCodeInfoContent(
    @SerializedName("type")
    val type: String,   //"danger",
    @SerializedName("title")
    val title: String,  //"Auth error",
    @SerializedName("message")
    val message: String, // "Unauthorized access"
    @SerializedName("data")
    val content: String
)