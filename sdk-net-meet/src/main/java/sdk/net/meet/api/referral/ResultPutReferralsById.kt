package sdk.net.meet.api.referral


import com.google.gson.annotations.SerializedName

data class ResultPutReferralsById(
    @SerializedName("result")
    val code: Boolean
)

