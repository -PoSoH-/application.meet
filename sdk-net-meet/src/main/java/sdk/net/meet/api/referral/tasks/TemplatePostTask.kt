package sdk.net.meet.api.referral.tasks

import sdk.net.meet.api.referral.ResultPostTemplate
import sdk.net.meet.net.tasks.Task

internal interface TemplatePostTask: Task<TemplatePostTask.Params, ResultPostTemplate> {
    data class Params(
        val authParam: String,
        val body     : String
    )
}
