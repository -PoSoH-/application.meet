package sdk.net.meet.api.contacts

data class ApiGetConfig(
    val limit      : Int?,
    val page       : Int?,           // Limit contacts of page
    val search     : String?,        // Count contacts of page
    val isFavorite : Boolean?,       // Search keywords
    val isRecently : Boolean?,       // Show contacts that is favorite
    val byLetter   : String?,        // Show recently added contacts
    val groupId    : String?,        // Show contacts by letter
    val sortBy     : Array<String>?, // Sort by field (name, email, phone)
    val sortOrder  : Array<String>?, // Sort order asc or desc
){
    companion object{
        fun getDefault() = ApiGetConfig(
            limit = null, page = null, search = null, isFavorite = null, isRecently = null,
            byLetter = null, groupId  = null, sortBy  = null, sortOrder = null)
        fun getByPagination(page: Int) = ApiGetConfig(
            limit = 16, page = page, search = null, isFavorite = null, isRecently = null,
            byLetter = null, groupId  = null, sortBy  = null, sortOrder = null)
        fun getSearch(text: String) = ApiGetConfig(
            limit = null, page = null, search = text, isFavorite = null, isRecently = null,
            byLetter = null, groupId  = null, sortBy  = null, sortOrder = null)
        fun getFavorite() = ApiGetConfig(
            limit = null, page = null, search = null, isFavorite = true, isRecently = null,
            byLetter = null, groupId  = null, sortBy  = null, sortOrder = null)
    }
}
