package sdk.net.meet.api.referral.responses

sealed class TemplateFetch{
    data class Success(val success: String) : TemplateFetch()
    data class Failure(val failure: String) : TemplateFetch()
}
