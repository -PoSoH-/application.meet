package sdk.net.meet.api.referral


import com.google.gson.annotations.SerializedName

data class ResultOtherRequest(
    @SerializedName("data")
    val success: Boolean
)

