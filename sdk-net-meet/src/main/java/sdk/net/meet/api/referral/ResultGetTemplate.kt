package sdk.net.meet.api.referral


import com.google.gson.annotations.SerializedName

data class ResultGetTemplate(
    @SerializedName("data")
    val templates: String
)

