/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.api.failure

interface Cancelable{
    fun cancel() {

    }
}
object NoOpCancellable : Cancelable