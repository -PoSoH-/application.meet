package sdk.net.meet.api.contacts.tasks

import sdk.net.meet.api.contacts._ResponseContacts
import sdk.net.meet.api.contacts.response.FetchContactCategories
import sdk.net.meet.api.failure.DataFailure
import sdk.net.meet.net.tasks.Task

interface ITaskContactCategory : Task<ITaskContactCategory.Params, TaskCategory> {
    data class Params(
        val authParam: String
    )
}

sealed class TaskCategory {
    data class Success(val body: FetchContactCategories): TaskCategory()
    data class Failure(val body: DataFailure)           : TaskCategory()
}