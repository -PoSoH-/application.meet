package sdk.net.meet.api.referral.models

import com.google.gson.annotations.SerializedName

object objModelReferralDelRes {

    data class ResultDeleteReferrals(
        @SerializedName("data")
        val content: DeleteData
    )

    data class DeleteData(
        @SerializedName("status")
        val status: String,
        @SerializedName("title")
        val title: String,
        @SerializedName("message")
        val message: String
    )

}