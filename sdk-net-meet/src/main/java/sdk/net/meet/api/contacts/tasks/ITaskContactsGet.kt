package sdk.net.meet.api.contacts.tasks

import sdk.net.meet.api.contacts.response.FetchAllUserContact
import sdk.net.meet.api.failure.DataFailure
import sdk.net.meet.net.tasks.Task

interface ITaskContactsGet : Task<ITaskContactsGet.Params, TaskAllContacts> {
    data class Params(
        val authParam : String,
        val limit     : Int? = null,
        val page      : Int? = null,
        val search    : String? = null,
        val isFavorite: Boolean? = null,
        val isRecently: Boolean? = null,
        val byLetter  : String? = null,
        val groupId   : String? = null,
        val by        : Array<String>? = null,
        val order     : Array<String>? = null,
    )
}

sealed class TaskAllContacts {
    data class Success(val body: FetchAllUserContact): TaskAllContacts()
    data class Failure(val body: DataFailure): TaskAllContacts()
}