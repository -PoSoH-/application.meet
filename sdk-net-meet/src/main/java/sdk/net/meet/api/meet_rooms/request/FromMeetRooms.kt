package sdk.net.meet.api.meet_rooms.request

import com.google.gson.annotations.SerializedName

data class FromMeetRooms (
    @SerializedName("name")
    val name: String, // "sumra_meet_test_room_ansfd",
    @SerializedName("status")
    val status: Boolean, //": true,
    @SerializedName("password")
    val password: String? = null // null
)

data class FromMeetByInvite (
    @SerializedName("invite")
    val invite: String
)



