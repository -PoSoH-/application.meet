package sdk.net.meet.api.referral.tasks

import sdk.net.meet.api.referral.models.objModelReferralDelRes
import sdk.net.meet.net.tasks.Task

internal interface ReferralCodeDeleteTask: Task<ReferralCodeDeleteTask.Params, objModelReferralDelRes.ResultDeleteReferrals> {
    data class Params(
        val authParam : String,
        val referralID: String
    )
}
