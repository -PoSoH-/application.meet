package sdk.net.meet.api.referral

import com.google.gson.annotations.SerializedName

internal data class ReferralCodeParams (
    @SerializedName("application_id")
    val applicationID: String
)