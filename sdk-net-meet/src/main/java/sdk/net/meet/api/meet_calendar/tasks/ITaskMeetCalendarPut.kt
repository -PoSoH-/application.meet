package sdk.net.meet.api.meet_calendar.tasks

import sdk.net.meet.api.meet_calendar.TaskRespCalendar
import sdk.net.meet.api.meet_calendar.request.FromCreateCalendar
import sdk.net.meet.api.meet_calendar.response.FetchMeetCalendar
import sdk.net.meet.api.meet_calendar.response.FetchMeetCalendarAll
import sdk.net.meet.net.tasks.Task

interface ITaskMeetCalendarPut: Task<ITaskMeetCalendarPut.Params, TaskRespCalendar> {
    data class Params(
        val authParam: String,
        val apiID: String,
        val calendarID: String,
        val body: FromCreateCalendar
    )
}