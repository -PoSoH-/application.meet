package sdk.net.meet.api.meet_profile.response

import com.google.gson.annotations.SerializedName

object dataOfMeeting {

    data class DataResponse(
        @SerializedName("type")
        val type: String, // ": "success",
        @SerializedName("title")
        val title: String, // ": "Operation was success",
        @SerializedName("message")
        val message: String, // ": "Meeting created successfully",
        @SerializedName("data")
        val data: InviteMeeting // ": {
    )

    data class InviteMeeting(
        @SerializedName("")
        val name: String, // ": "sumra_meet_test_room_ansfd",
        @SerializedName("")
        val status: Boolean, // ": true,
        @SerializedName("")
        val id: String, // ": "94879036-6c9a-40a5-9676-00b1ca7a37dc",
        @SerializedName("")
        val invite: String, // ": "yxz-ryv-ynu-0ld"
    )

}