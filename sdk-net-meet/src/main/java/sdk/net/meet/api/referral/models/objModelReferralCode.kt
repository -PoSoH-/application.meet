package sdk.net.meet.api.referral


import com.google.gson.annotations.SerializedName

object objModelReferralCode {

    data class ResultGetReferralCode(
        @SerializedName("type")
        val status: String,
        @SerializedName("title")
        val title: String,
        @SerializedName("message")
        val message: String,
        @SerializedName("data")
        val refCodes: List<Code>
    )

    data class Code(
        @SerializedName("id")
        val id: String, // "93f8415a-e409-4728-ad78-b3135f4faede",
        @SerializedName("code")
        val code: String, // "558hLm",
        @SerializedName("application_id")
        val applicationID: String, // "web.sumra.wallet",
        @SerializedName("user_id")
        val userID: String, // "1000",
        @SerializedName("link")
        val referralLink: String,
        @SerializedName("is_default")
        val isDefault: Boolean,    // true/false,
        @SerializedName("note")
        val note: String, // null,
        @SerializedName("count")
        val count: Int = 0, // 3258
        @SerializedName("created_at")
        val createdAt: String, // "2021-07-22T09:02:56.000000Z",
        @SerializedName("updated_at")
        val updatedAt: String, // "2021-07-22T09:02:56.000000Z",
        @SerializedName("deleted_at")
        val deletedAt: String, // "2021-07-22T09:02:56.000000Z",
    )

    data class CodeAttributes(
        @SerializedName("id")
        val id: String, // "93f8415a-e409-4728-ad78-b3135f4faede",
        @SerializedName("code")
        val code: String, // "5578ghLm",
        @SerializedName("application_id")
        val applicationID: String, // "web.sumra.wallet",
        @SerializedName("user_id")
        val userID: String, // "1000",
        @SerializedName("link")
        val referralLink: String, // "https://smr.page.link/KPLH",
        @SerializedName("is_default")
        val isDefault: Long,   // 0,
        @SerializedName("note")
        val note: String, // null,
        @SerializedName("resource_url")
        val resourceURL: String  // "http://ec2-34-208-108-203.us-west-2.compute.amazonaws.com:8091/admin/links/93f8415a-e409-4728-ad78-b3135f4faede"
    )

    data class ReferralCodeParamsPst(
        @SerializedName("application_id")
        val appID    : String,   // "app.sumra.chat",
        @SerializedName("is_default")
        val isDefault: Boolean , //false,
        @SerializedName("note")
        val note     : String    //"Code for facebook"
    )

    data class ReferralCodeParamsPut(
        @SerializedName("is_default")
        val isDefault: Boolean , //false,
        @SerializedName("note")
        val note     : String    //"Code for facebook"
    )

    data class ReferralResult(
        @SerializedName("status")
        val status: String,
        @SerializedName("title")
        val title: String,
        @SerializedName("message")
        val message: String,
        @SerializedName("data")
        val code: objModelReferralCode.Code
    )


    /***
    {
      "application_id": "app.sumra.chat",
      "is_default": false,
      "note": "Code for facebook"
    }
    ***/

}