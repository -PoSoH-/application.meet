package sdk.net.meet.api.referral.tasks

import sdk.net.meet.api.referral.ResultGetReferralsById
import sdk.net.meet.api.referral.ResultGetTemplate
import sdk.net.meet.net.tasks.Task

internal interface TemplateFetchTaskById: Task<TemplateFetchTaskById.Params, ResultGetReferralsById> {
    data class Params(
        val authParam : String,
        val referralID: Int
    )
}
