package sdk.net.meet.api.referral.responses


import com.google.gson.annotations.SerializedName

data class ReferralsFailureObject(
    @SerializedName("data")
    val errorObject: ErrorReferralObject
) {
    data class ErrorReferralObject(
        @SerializedName("message")
        val message: String,
        @SerializedName("status")
        val status: String,
        @SerializedName("title")
        val title: String
    )
}