package sdk.net.meet.api.meet_rooms.response

import com.google.gson.annotations.SerializedName

data class FetchOwnerRoom (
    @SerializedName("type")
    val type:  String, // "success",
    @SerializedName("title")
    val title: String, // "Creating a room",
    @SerializedName("message")
    val message: String, // "Room created successfully",
    @SerializedName("data")
    val body: MeetRoom? = null
)

data class FetchAllOwnerRoom (
    @SerializedName("type")
    val type:  String, // "success",
    @SerializedName("title")
    val title: String, // "Creating a room",
    @SerializedName("message")
    val message: String, // "Room created successfully",
    @SerializedName("data")
    val body: List<MeetRoom>
)

data class MeetRoom (
    @SerializedName("name")
    val name: String,    // "sumra_meet_test_room_ansfd",
    @SerializedName("status")
    val status: Boolean, // true,
    @SerializedName("id")
    val id: String,      // "941ea5d7-1e7d-413e-9334-b405c7133612",
    @SerializedName("slug")
    val slug: String,    // "sumra-meet-test-room-ansfd",
    @SerializedName("invite")
    val invite: String,  // "n4h-ks0-3fu-al2",
    @SerializedName("room_users")
    val roomUsers: List<InviteRoomUsers> //"
)

data class FetchUpdateRoom(
    @SerializedName("type")
    val type:  String, // "success",
    @SerializedName("title")
    val title: String, // "Creating a room",
    @SerializedName("message")
    val message: String, // "Room created successfully",
    @SerializedName("data")
    val body: MeetRoom? = null
)

data class FetchDeleteRoom (
    @SerializedName("type")
    val type:  String, // "success",
    @SerializedName("title")
    val title: String, // "Creating a room",
    @SerializedName("message")
    val message: String, // "Room created successfully",
    @SerializedName("data")
    val body: MeetRoom? = null
)

/***   ***   ***   ***   ***   ***   ***   ***   ***   ***   ***   ***   ***   ***   ***   ***/
data class FetchInviteRoomBase (
    @SerializedName("type")
    val type:  String, // "success",
    @SerializedName("title")
    val title: String, // "Creating a room",
    @SerializedName("message")
    val message: String, // "Room created successfully",
    @SerializedName("data")
    val body: InviteRoom? = null
)
data class InviteRoom(
    @SerializedName("id")
    val mId: String,           // "94737fab-97d6-4bcd-b7cc-64c0f8bf43d6",
    @SerializedName("name")
    val mName: String,         // "testedRoomm",
    @SerializedName("slug")
    val mSlug: String? = null, // "qxl-yzr",
    @SerializedName("status")
    val mStatus: Boolean,      // 1,
    @SerializedName("invite")
    val mInvite:  String,      // "myq-5ld-qxl-yzr",
    @SerializedName("room_users")
    val mRoomUsers: List<InviteRoomUsers>
)
data class InviteRoomUsers(
    @SerializedName("id")
    val id: String,           // "00000000-2000-2000-2000-000000000000"
    @SerializedName("display_name")
    val mDisplayName: String, // "Gudrun Gleason",
    @SerializedName("slug")
    val mSlug: String,        // "myq-5ld",
    @SerializedName("phone")
    val mPhone: String,       // "+15345336619",
    @SerializedName("email")
    val mEmail: String,       // "ernest.grimes@example.org",
    @SerializedName("total_time")
    val mTotalTime: Float     // 428
)

data class FetchInviteMeeting (
    @SerializedName("type")
    val type:  String, // "success",
    @SerializedName("title")
    val title: String, // "Creating a room",
    @SerializedName("message")
    val message: String, // "Room created successfully",
    @SerializedName("data")
    val body: InviteMeeting? = null
)

data class InviteMeeting(
    @SerializedName("")
    val name: String, // ": "sumra_meet_test_room_ansfd",
    @SerializedName("")
    val status: Boolean, // ": true,
    @SerializedName("")
    val id: String, // ": "94879036-6c9a-40a5-9676-00b1ca7a37dc",
    @SerializedName("")
    val invite: String, // ": "yxz-ryv-ynu-0ld"
)

/**
    "type": "success",
    "title": "Invite user to meeting",
    "message": "Information about the invited user was sent successfully",
    "data": {
 **/

/**
{
    "type": "success",
    "title": "Creating a room",
    "message": "Room created successfully",
    "data": {
        "name": "sumra_meet_test_room_ansfd",
        "status": true,
        "id": "941ea5d7-1e7d-413e-9334-b405c7133612",
        "slug": "sumra-meet-test-room-ansfd",
        "room_users": [
            {
                "id": "1000",
                "display_name": "Igor Petrovitch",
                "phone": "+380653698521",
                "email": "petrovitch@data.data"
            }
        ]
    }
}
**/
