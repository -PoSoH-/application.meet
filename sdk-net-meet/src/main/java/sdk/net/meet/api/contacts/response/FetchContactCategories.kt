package sdk.net.meet.api.contacts.response


import com.google.gson.annotations.SerializedName

data class FetchContactCategories(
    @SerializedName("data")
    val content: List<ContactCategory>
)

data class ContactCategory(
    @SerializedName("children")
    val children: List<Children>,
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String
)

data class Children(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String
)
