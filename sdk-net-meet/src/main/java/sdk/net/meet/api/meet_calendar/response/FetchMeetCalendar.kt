package sdk.net.meet.api.meet_calendar.response

import com.google.gson.annotations.SerializedName

data class FetchMeetCalendar (
    @SerializedName("type")
    val type: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val body: MeetCalendar?
)

data class FetchMeetCalendarAll (
    @SerializedName("type")
    val type: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val body: List<MeetCalendar>
)

data class FetchMeetCalendarUpdate (
    @SerializedName("type")
    val type: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val body: MeetCalendar?
)

data class MeetCalendar (
    @SerializedName("type")
    val type: String, //": "calendars",
    @SerializedName("id")
    val id: String,  //": "941e70fa-246c-4d23-87a6-5be29e6f5edc",
    @SerializedName("attributes")
    val attributes: MeetCalendarAttr
)

data class MeetCalendarAttr(
    @SerializedName("title")
    val title: String,      // ": "Valentine's Day",
    @SerializedName("note")
    val note: String,       // ": "Valentine's Day",
    @SerializedName("start_date")
    val startDate: String,  //  "2015-02-14",
    @SerializedName("end_date")
    val finalDate: String,
    @SerializedName("id")
    val id: String,         //  "941e70fa-246c-4d23-87a6-5be29e6f5edc",
    @SerializedName("password")
    val password: String? = null,
    @SerializedName("user_id")
    val ownerId: String,    //  "1000",
    @SerializedName("participants")
    val participants: List<String>,
    @SerializedName("is_lobby")
    val isLobby: Boolean = false,
    @SerializedName("live_stream")
    val liveStream: MeetStream?,
    @SerializedName("updated_at")
    val updated_at: String, //  "2021-08-10T08:37:33.000000Z",
    @SerializedName("created_at")
    val created_at: String, //  "2021-08-10T08:37:33.000000Z"
)

data class MeetStream (
    @SerializedName("stream_key")
    val streamKey: String? = null,
    @SerializedName("stream_link")
    val streamLink: String? = null,
)



