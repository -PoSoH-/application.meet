package sdk.net.meet.api.meet_profile.tasks

import sdk.net.meet.api.meet_profile.TaskProfileResult
import sdk.net.meet.net.tasks.Task

interface ITaskProfileGet: Task<ITaskProfileGet.Params, TaskProfileResult>{
    data class Params(
        val auth : String,
        val appId: String,
    )
}

