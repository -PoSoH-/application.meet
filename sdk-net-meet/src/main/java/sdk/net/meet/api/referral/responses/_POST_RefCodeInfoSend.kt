package sdk.net.meet.api.referral.responses

import sdk.net.meet.api.referral.ResultGetTemplate

sealed class _POST_RefCodeInfoSend{
    data class Success(val success: RefCodeInfoTemplate)  : _POST_RefCodeInfoSend()
    data class Failure(val failure: ReferralsFailureObject): _POST_RefCodeInfoSend()
}
