package sdk.net.meet.api.referral.tasks

import sdk.net.meet.net.tasks.Task

internal interface TemplateRemoveTask: Task<TemplateRemoveTask.Params, Boolean> {
    data class Params(
        val authParam : String,
        val referralID: String
    )
}
