package sdk.net.meet.api.contacts.response

import com.google.gson.annotations.SerializedName

data class FetchImportContactByJson(
    @SerializedName("data")
    val importResult: ResponseMessage,
)

data class ResponseMessage (
    @SerializedName("type")
    val type: Boolean,
    @SerializedName("title")
    val title: String,
    @SerializedName("message")
    val message: String?
)