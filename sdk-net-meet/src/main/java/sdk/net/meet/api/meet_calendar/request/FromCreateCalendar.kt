package sdk.net.meet.api.meet_calendar.request

import com.google.gson.annotations.SerializedName
import sdk.net.meet.api.meet_calendar.response.MeetStream

data class FromCreateCalendar(
    @SerializedName("title")
    val title: String,      // ": "Valentine's Day",
    @SerializedName("note")
    val note: String,       // ": "Valentine's Day",
    @SerializedName("start_date")
    val startDate: String,  //  "2022-02-14T20:00:00.000000Z",
    @SerializedName("end_date")
    val finalDate: String,  //  "2022-02-14T22:05:00.000000Z",
    @SerializedName("id")
    val id: String,         //  "924e55fa-246c-4d23-87a6-5be29e6f5edc",
    @SerializedName("password")
    val password: String? = null,
    @SerializedName("user_id")
    val ownerId: String,    //  "458770fa-246c-4d23-87a6-5be29e6f5edc",
    @SerializedName("participants")
    val participants: List<String>, // ["941e70fa-246c-aaaa-87a6-5be29e6f5edc","941e70fa-bbbb-4d23-87a6-5be29e6f5edc","941e70fa-cccc-4d23-87a6-5be29e6f5edc","941e70fa-dddd-4d23-87a6-5be29e6f5edc",]
    @SerializedName("is_lobby")
    val isLobby: Boolean = false,
    @SerializedName("live_stream")
    val liveStream: MeetStream? = null,  // this -> {"stream_key":"key_for_stream", "stream_link":"link.key"} or null
    @SerializedName("updated_at")
    val updated_at: String, //  "2021-09-10T10:25:00.000000Z",  full date format
    @SerializedName("created_at")
    val created_at: String, //  "2021-09-10T09:25:00.000000Z"
)


//{
//    "title":"Valentine`s day",
//    "note":"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful",
//    "start_date":"2022-02-14T22:05:00.000000Z",
//    "end_date"  :"2022-02-14T22:05:00.000000Z",
//    "id":"I send Id. Must ",
//    "password":"password or null",
//    "user_id":"458770fa-246c-4d23-87a6-5be29e6f5edc",
//    "participants":[
//        "941e70fa-aaaa-4d23-87a6-5be29e6f5edc",
//        "941e70fa-bbbb-4d23-87a6-5be29e6f5edc",
//        "941e70fa-cccc-4d23-87a6-5be29e6f5edc",
//        "941e70fa-dddd-4d23-87a6-5be29e6f5edc",
//    ]
//    "is_lobby":false,
//    "live_stream":
//        {
//            "stream_key":"key_for_stream",
//            "stream_link":"link.key"
//        }/*or null*/
//    "updated_at":"2021-09-10T10:25:00.000000Z",  /*full date format*/
//    "created_at":"2021-09-10T09:25:00.000000Z"
//}

