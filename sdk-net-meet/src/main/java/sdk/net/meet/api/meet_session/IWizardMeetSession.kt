package sdk.net.meet.api.meet_session

import sdk.net.meet.api.meet_session.request.FromMeetSession

interface IWizardMeetSession {
    suspend fun roomSessionPst(authHeader: String
                        , appId: String
                        , body: FromMeetSession ): TaskRespMeetSession

    suspend fun roomSessionPut(authHeader: String
                               , appId: String
                               , sessionId: String
                               , body: FromMeetSession ): TaskRespMeetSession
}