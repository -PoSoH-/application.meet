package sdk.net.meet.api.meet_session.tasks

import sdk.net.meet.api.meet_session.TaskRespMeetSession
import sdk.net.meet.api.meet_session.request.FromMeetSession
import sdk.net.meet.net.tasks.Task

interface ITaskMeetSessionCreate: Task<ITaskMeetSessionCreate.Params, TaskRespMeetSession> {
    data class Params(
        val auth: String,
        val app : String,
        val body: FromMeetSession
    )
}