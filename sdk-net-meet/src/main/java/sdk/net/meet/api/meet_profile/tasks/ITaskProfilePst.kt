package sdk.net.meet.api.meet_profile.tasks

import sdk.net.meet.api.meet_profile.TaskProfileResult
import sdk.net.meet.api.meet_profile.request.FromAddProfile
import sdk.net.meet.net.tasks.Task

interface ITaskProfilePst: Task<ITaskProfilePst.Params, TaskProfileResult>{
    data class Params(
        val auth : String,
        val appId: String,
        val body : FromAddProfile
    )
}

/*
sealed class TaskProfileResult{
    data class Success(val body: Any): TaskProfileResult()
    data class Failure(val fail: Failure): TaskProfileResult()
}*/
