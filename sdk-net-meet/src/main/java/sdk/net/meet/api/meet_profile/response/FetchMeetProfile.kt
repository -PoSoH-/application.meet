package sdk.net.meet.api.meet_profile.response

import com.google.gson.annotations.SerializedName

data class FetchMeetProfile (
    @SerializedName("data")
    val body: MeetUserInfo,
    @SerializedName("message")
    val message: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("type")
    val status: String
)

data class MeetUserInfo (
    @SerializedName("id")
    val id: String,
    @SerializedName("display_name")
    val displayName: String,
    @SerializedName("slug")
    val slug: String,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("total_time")
    val totalTime: Float,
    @SerializedName("avatar")
    val avatar: String,
    @SerializedName("user_rooms")
    val userRooms: List<UserRoom>
)

data class UserRoom (
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("slug")
    val slug: String,
    @SerializedName("status")
    val status: Int
)
