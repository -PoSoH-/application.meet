package sdk.net.meet.api.referral.tasks

import sdk.net.meet.api.referral.objModelReferralCode
import sdk.net.meet.net.tasks.Task

internal interface ReferralCodeGetTask: Task<ReferralCodeGetTask.Params, objModelReferralCode.ResultGetReferralCode> {
    data class Params(
        val authParam    : String,
        val applicationID: String)
}
