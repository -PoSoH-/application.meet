package sdk.net.meet.api.referral.responses

import sdk.net.meet.api.referral.models.objModelReferralDelRes

sealed class _DEL_ReferralUpdate{
    data class Success(val success: objModelReferralDelRes.ResultDeleteReferrals) : _DEL_ReferralUpdate()
    data class Failure(val failure: ReferralsFailureObject): _DEL_ReferralUpdate()
}
