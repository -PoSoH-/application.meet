/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.api.failure

import sdk.net.meet.api.failure.MeetFailure
import java.io.IOException

sealed class Failure(cause: Throwable? = null) : Throwable(cause = cause) {
    data class Unknown  (val throwable: Throwable? = null) : Failure(throwable)
    data class Cancelled(val throwable: Throwable? = null) : Failure(throwable)
    data class NetworkConnection(val ioException: IOException? = null) : Failure(ioException)

    data class ServerError(val error: MeetFailure) : Failure(RuntimeException(error.toString()))
    data class OtherServerError(val errorBody: String, val httpCode: Int) : Failure(RuntimeException("HTTP $httpCode: $errorBody"))

    object SuccessError : Failure(RuntimeException(RuntimeException("SuccessResult is false")))

    abstract class FeatureFailure : Failure()
}
