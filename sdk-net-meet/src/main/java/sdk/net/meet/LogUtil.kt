package sdk.net.meet

import android.util.Log

object LogUtil {
    fun error(txt: String){
        if(BuildConfig.DEBUG){
            Log.e("${BuildConfig.APP_ID}::", txt)
        }
    }
    fun error(tag: String, txt: String){
        if(BuildConfig.DEBUG){
            Log.e("${BuildConfig.APP_ID}:$tag:", txt)
        }
    }
    fun info(txt: String){
        if(BuildConfig.DEBUG){
            Log.i("${BuildConfig.APP_ID}::", txt)
        }
    }
    fun info(tag: String, txt: String){
        if(BuildConfig.DEBUG){
            Log.i("${BuildConfig.APP_ID}:$tag:", txt)
        }
    }
}