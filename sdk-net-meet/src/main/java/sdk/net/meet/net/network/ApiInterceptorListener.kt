/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.net.network

interface ApiInterceptorListener {
    fun onApiResponse(path: ApiPath, response: String)
}