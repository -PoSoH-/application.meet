package sdk.net.meet.net.meet_rooms

import sdk.net.meet.api.meet_rooms.IWizardMeetRooms
import sdk.net.meet.api.meet_rooms.TaskRespMeetRoom
import sdk.net.meet.api.meet_rooms.request.FromMeetByInvite
import sdk.net.meet.api.meet_rooms.request.FromMeetRooms
import sdk.net.meet.api.meet_rooms.tasks.ITaskMeetRoomCreate
import sdk.net.meet.api.meet_rooms.tasks.ITaskMeetRoomDelete
import sdk.net.meet.api.meet_rooms.tasks.ITaskMeetRoomUpdate

class WizardMeetRooms(apiMeetRooms: ApiMeetRooms) : IWizardMeetRooms{

    private val taskCreate = TasksMeetRooms.TaskMeetRoomCreate(apiMeetRooms)
    private val taskUpdate = TasksMeetRooms.TaskMeetRoomUpdate(apiMeetRooms)
    private val taskDelete = TasksMeetRooms.TaskMeetRoomDelete(apiMeetRooms)

    override suspend fun roomPst(
        authHeader: String,
        appId: String,
        body: FromMeetRooms
    ): TaskRespMeetRoom {
        val res = taskCreate.execute(ITaskMeetRoomCreate
            .Params(
                authParams = authHeader,
                appId = appId,
                body = body))
        return res
    }

    override suspend fun roomPut(
        authHeader: String,
        appId: String,
        roomId: String,
        body: FromMeetRooms
    ): TaskRespMeetRoom {
        val t =  taskUpdate.execute(ITaskMeetRoomUpdate
            .Params(
                authParams = authHeader,
                appId = appId,
                roomId = roomId,
                body = body))
        return t
    }

    override suspend fun roomDel(
        authHeader: String,
        appId: String,
        roomId: String
    ): TaskRespMeetRoom {
        return taskDelete.execute(ITaskMeetRoomDelete
            .Params(
                authParams = authHeader,
                appId = appId,
                roomId = roomId))
    }
}