package sdk.net.meet.net.network

import android.util.Log
import kotlinx.coroutines.delay
import retrofit2.HttpException
import sdk.net.meet.LogUtil
import sdk.net.meet.api.failure.Failure
import sdk.net.meet.api.failure.shouldBeRetried
import java.io.IOException
import kotlin.coroutines.cancellation.CancellationException

internal suspend inline fun <DATA> executeRequest(globalErrorReceiver: GlobalErrorReceiver?,
                                                  canRetry: Boolean = false,
                                                  maxDelayBeforeRetry: Long = 32_000L,
                                                  maxRetriesCount: Int = 4,
                                                  noinline requestBlock: suspend () -> DATA): DATA {
    var currentRetryCount = 0
    var currentDelay = 1_000L

    while (true) {
        try {
            return requestBlock()
        } catch (throwable: Throwable) {
            val exception = when (throwable) {
                is KotlinNullPointerException -> IllegalStateException("The request returned a null body")
                is HttpException -> throwable.toFailure(globalErrorReceiver)
                else                          -> throwable
            }

            // Log some details about the request which has failed.
            val request = (throwable as? HttpException)?.response()?.raw()?.request

            if (request == null) {
                LogUtil.error("HTTP REQUEST","Exception when executing request")
            } else {
                LogUtil.error("HTTP REQUEST","Exception when executing request ${request.method} ${request.url.toString().substringBefore("?")}")
            }

            // Check if this is a certificateException
//            CertUtil.getCertificateException(exception)
//                // TODO Support certificate error once logged
//                // ?.also { unrecognizedCertificateException ->
//                //    // Send the error to the bus, for a global management
//                //    eventBus?.post(GlobalError.CertificateError(unrecognizedCertificateException))
//                // }
//                ?.also { unrecognizedCertificateException -> throw unrecognizedCertificateException }

            currentRetryCount++

            if (exception is Failure.ServerError
                && exception.error != null
//                && exception.error.code == MatrixError.M_LIMIT_EXCEEDED
                && currentRetryCount < maxRetriesCount) {
                // 429, we can retry
//                delay(exception.getRetryDelay(1_000))
            } else if (canRetry && currentRetryCount < maxRetriesCount && exception.shouldBeRetried()) {
                delay(currentDelay)
                currentDelay = currentDelay.times(2L).coerceAtMost(maxDelayBeforeRetry)
                // Try again (loop)
            } else {
                throw when (exception) {
                    is IOException -> Failure.NetworkConnection(exception)
                    is Failure.ServerError,
                    is Failure.OtherServerError -> exception
                    is CancellationException    -> Failure.Cancelled(exception)
                    else                        -> Failure.Unknown(exception)
                }
            }
        }
    }
}