package sdk.net.meet.net.meet_profile

import sdk.net.meet.api.failure.isContactError
import sdk.net.meet.api.meet_profile.TaskProfileResult
import sdk.net.meet.api.meet_profile.tasks.ITaskProfileGet
import sdk.net.meet.api.meet_profile.tasks.ITaskProfilePst
import sdk.net.meet.api.meet_profile.tasks.ITaskProfilePut
import sdk.net.meet.api.meet_rooms.TaskRespMeetRoom
//import sdk.net.meet.api.meet_rooms.tasks.ITaskMeetRoomFetchInvite
import sdk.net.meet.api.meet_rooms.tasks.ITaskMeetRoomInvite

class TaskMeetProfiles {

    internal class TaskDefaultMeetProfileGet(private val api: ApiMeetProfile): ITaskProfileGet {
        override suspend fun execute(params: ITaskProfileGet.Params): TaskProfileResult {
            try {
                val res = api.userDataGET(params.auth) //, params.appId)
                return TaskProfileResult.Body(body = res)
            } catch (ex: Throwable){
                ex.isContactError().let{
                    return TaskProfileResult.Fail(fail = it!!.failure)
                }
            }
        }
    }

    internal class TaskDefaultMeetProfilePst(private val api: ApiMeetProfile): ITaskProfilePst {
        override suspend fun execute(params: ITaskProfilePst.Params): TaskProfileResult {
            try {
                val res = api.userDataPOST(params.auth, params.body) //, params.appId)
                return TaskProfileResult.Body(body = res)
            } catch (ex: Throwable){
                ex.isContactError().let{
                    return TaskProfileResult.Fail(fail = it!!.failure)
                }
            }
        }
    }

    internal class TaskDefaultMeetProfilePut(private val api: ApiMeetProfile): ITaskProfilePut {
        override suspend fun execute(params: ITaskProfilePut.Params): TaskProfileResult {
            try {
                val res = api.userDataPUT(authHeader = params.auth
                    , profileId = params.profileId
                    , body = params.body) //, params.appId)
                return TaskProfileResult.Body(body = res)
            } catch (ex: Throwable){
                ex.isContactError().let{
                    return TaskProfileResult.Fail(fail = it!!.failure)
                }
            }
        }
    }

    /***   ***/

    internal class TaskDefaultMeetRoomInvitePst(private val api: ApiMeetProfile): ITaskMeetRoomInvite {
        override suspend fun execute(params: ITaskMeetRoomInvite.Params): TaskRespMeetRoom {
            try {
                val res = api.fetchRoomByInvite(authHeader = params.authParams
//                    , profileId = params.appId
                    , body = params.body) //, params.appId)
                return TaskRespMeetRoom.Data(body = res)
            } catch (ex: Throwable){
                ex.isContactError().let{
                    return TaskRespMeetRoom.Fail(fail = it!!.failure)
                }
            }
        }
    }

//    internal class TaskDefaultMeetRoomInviteGet(private val api: ApiMeetProfile): ITaskMeetRoomFetchInvite {
//        override suspend fun execute(params: ITaskMeetRoomFetchInvite.Params): TaskRespMeetRoom {
//            try {
//                 val res = api.fetchRoomByInvitePst(authHeader = params.authParams
////                    , appId = params.appId
//                    , invite = params.invite) //, params.appId)
//                return TaskRespMeetRoom.Data(body = res)
//            } catch (ex: Throwable){
//                ex.isContactError().let{
//                    return TaskRespMeetRoom.Fail(fail = it!!.failure)
//                }
//            }
//        }
//    }
}