package sdk.net.meet.net.meet_rooms

import sdk.net.meet.api.failure.isContactError
import sdk.net.meet.api.meet_rooms.tasks.ITaskMeetRoomCreate
import sdk.net.meet.api.meet_rooms.TaskRespMeetRoom
import sdk.net.meet.api.meet_rooms.tasks.ITaskMeetRoomDelete
import sdk.net.meet.api.meet_rooms.tasks.ITaskMeetRoomUpdate

internal class TasksMeetRooms {

    internal class TaskMeetRoomCreate(private val apiMeetRooms: ApiMeetRooms): ITaskMeetRoomCreate {
        override suspend fun execute(params: ITaskMeetRoomCreate.Params): TaskRespMeetRoom {
            try {
                val resp = apiMeetRooms.meetRoomPST(
                    authHeader = params.authParams,
                    body = params.body)
                return TaskRespMeetRoom.Data(body = resp)
            } catch (th: Throwable){
                th.isContactError().let {
                    return TaskRespMeetRoom.Fail(it!!.failure)
                }
            }
        }
    }

    internal class TaskMeetRoomUpdate(private val apiMeetRooms: ApiMeetRooms): ITaskMeetRoomUpdate {
        override suspend fun execute(params: ITaskMeetRoomUpdate.Params): TaskRespMeetRoom {
            try {
                val resp = apiMeetRooms.meetRoomPUT(
                    authHeader = params.authParams,
                    roomId = params.roomId,
                    body = params.body)
                return TaskRespMeetRoom.Data(body = resp)
            } catch (th: Throwable){
                th.isContactError().let {
                    return TaskRespMeetRoom.Fail(it!!.failure)
                }
            }
        }
    }

    internal class TaskMeetRoomDelete(private val apiMeetRooms: ApiMeetRooms): ITaskMeetRoomDelete {
        override suspend fun execute(params: ITaskMeetRoomDelete.Params): TaskRespMeetRoom {
            try {
                val resp = apiMeetRooms.meetRoomDEL(
                    authHeader = params.authParams,
                    roomId = params.roomId)
                return TaskRespMeetRoom.Data(body = resp)
            } catch (th: Throwable){
                th.isContactError().let {
                    return TaskRespMeetRoom.Fail(it!!.failure)
                }
            }
        }
    }

}