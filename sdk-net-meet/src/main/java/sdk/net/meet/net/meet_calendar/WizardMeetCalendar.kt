package sdk.net.meet.net.meet_calendar

import sdk.net.meet.api.meet_calendar.IWizardMeetCalendar
import sdk.net.meet.api.meet_calendar.TaskRespCalendar
import sdk.net.meet.api.meet_calendar.request.FromCreateCalendar
import sdk.net.meet.api.meet_calendar.tasks.*

class WizardMeetCalendar (apiMeetCalendar: ApiMeetCalendar): IWizardMeetCalendar {

    private val tasCalendarGet = TasksMeetCalendar.TaskCalendarsFetch(apiMeetCalendar)
    private val tasCalendarByIdGet = TasksMeetCalendar.TaskCalendarsFetchById(apiMeetCalendar)
    private val tasCalendarPut = TasksMeetCalendar.TaskCalendarsUpdate(apiMeetCalendar)
    private val tasCalendarPst = TasksMeetCalendar.TaskCalendarsCreate(apiMeetCalendar)
    private val tasCalendarDel = TasksMeetCalendar.TaskCalendarsDelete(apiMeetCalendar)

    override suspend fun calendarGet(authHeader: String, appId: String): TaskRespCalendar {
        return tasCalendarGet.execute(params = ITaskMeetCalendarGet
            .Params(authParam = authHeader, apiID = appId))
    }

    override suspend fun calendarPst(
        authHeader: String,
        appId: String,
        body: FromCreateCalendar
    ): TaskRespCalendar {
        return tasCalendarPst.execute(params = ITaskMeetCalendarPst
            .Params(authParam = authHeader, apiID = appId, body = body))
    }

    override suspend fun calendarByIdGet(
        authHeader: String,
        appId: String,
        calendarId: String
    ): TaskRespCalendar {
        return tasCalendarByIdGet.execute(params = ITaskMeetCalendarByIdGet
            .Params(authParam = authHeader, apiID = appId, calendarId = calendarId))
    }

    override suspend fun calendarByIdPut(
        authHeader: String,
        appId: String,
        calendarId: String,
        body: FromCreateCalendar
    ): TaskRespCalendar {
        return tasCalendarPut.execute(params = ITaskMeetCalendarPut
            .Params(authParam = authHeader, apiID = appId, calendarID = calendarId, body = body))
    }

    override suspend fun calendarByIdDel(
        authHeader: String,
        appId: String,
        calendarId: String
    ): TaskRespCalendar {
        return tasCalendarDel.execute(params = ITaskMeetCalendarDel
            .Params(authParam = authHeader, apiID = appId, calendarID = calendarId))
    }


}