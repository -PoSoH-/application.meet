/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.net.network

enum class ApiPath(val path: String, val method: String) {

    /**  Auth wrapper  **/
//    SEND_CODE          (path = "${NetConst.AUTH}/send-code",                    method = "POST"),
//    REGISTRATION       (path = "${NetConst.AUTH}/registration",                 method = "POST"),
//    VALIDATE           (path = "${NetConst.AUTH}/validate",                     method = "GET"),
//    CHAT_AUTHENTICATE  (path = "${NetConst.AUTH}/chat/authenticate",            method = "POST"),
//    MEET_AUTHENTICATE  (path = "${NetConst.AUTH}/meet/authenticate",            method = "POST"),
//    ABOUT              (path =    NetConst.AUTH,                                method = "GET"),
//    TELEGRAM_POST      (path = "${NetConst.AUTH}/messengers/telegram/webhook",  method = "POST"),
//    VIBER_POST         (path = "${NetConst.AUTH}/messengers/viber/webhook",     method = "POST"),
//    FACEBOOK_GET       (path = "${NetConst.AUTH}/messengers/facebook/webhook",  method = "GET"),
//    FACEBOOK_POST      (path = "${NetConst.AUTH}/messengers/facebook/webhook",  method = "POST"),
//    WHATSAPP_POST      (path = "${NetConst.AUTH}/messengers/whatsapp/webhook",  method = "POST"),
//    LINE_POST          (path = "${NetConst.AUTH}/messengers/line/webhook",      method = "POST"),
//    DISCORD_GET        (path = "${NetConst.AUTH}/messengers/discord/activate",  method = "GET"),
//    SIGNAL_GET         (path = "${NetConst.AUTH}/messengers/signal/activate",   method = "GET"),
//    SUBMIT_CODE_POST   (path = "${NetConst.AUTH}/submit-code",                  method = "POST"),
//    ZOOM_POST          (path = "${NetConst.AUTH}/messengers/zoom/webhook",      method = "POST"),
//
//    /**  Referrals wrapper  **/
//    TEMPLATE_GET       (path = "${NetConst.REFERRAL}/admin/template",           method = "GET"),
//    TEMPLATE_POST      (path = "${NetConst.REFERRAL}/admin/template",           method = "POST"),
//    ADMIN_GET          (path = "${NetConst.REFERRAL}/admin/referrals-list",     method = "GET"),
//    ADMIN_BY_ID_GET    (path = "${NetConst.REFERRAL}/admin/referrals-list/{id}",method = "POST"),
//    LANDING_PAGE_GET   (path = "${NetConst.REFERRAL}/landing-page",             method = "GET"),
//    LANDING_PAGE_POST  (path = "${NetConst.REFERRAL}/landing-page",             method = "POST"),
//    MANAGEMENT_POST    (path = "${NetConst.REFERRAL}/manager/validate/referrer",method = "POST"),
//    CODE_GET           (path = "${NetConst.REFERRAL}/referral-codes",           method = "GET"),
//    CODE_POST          (path = "${NetConst.REFERRAL}/referral-codes",           method = "POST"),
//    CODE_BY_ID_GET     (path = "${NetConst.REFERRAL}/referral-codes/{id}",      method = "GET"),
//    CODE_BY_ID_PUT     (path = "${NetConst.REFERRAL}/referral-codes/{id}",      method = "PUT"),
//    CODE_BY_ID_DELETE  (path = "${NetConst.REFERRAL}/referral-codes/{id}",      method = "DELETE"),
//    CODE_BY_ID_SET_GET (path = "${NetConst.REFERRAL}/referral-codes/{id}/set",  method = "GET"),
//    MAIN_GET           (path =    NetConst.REFERRAL,                            method = "GET"),
//    MAIN_POST          (path = "${NetConst.REFERRAL}/inviting",                 method = "POST"),



}