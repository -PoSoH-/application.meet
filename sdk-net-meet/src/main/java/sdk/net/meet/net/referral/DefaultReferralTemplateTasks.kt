/*
 * Copyright 2021 The BGC Global Partners
 */


package sdk.net.meet.net.referral

import sdk.net.meet.api.failure.Failure
import sdk.net.meet.api.failure.isReferralError
import sdk.net.meet.api.referral.ResultGetTemplate
import sdk.net.meet.api.referral.ResultPostTemplate
import sdk.net.meet.api.referral.responses.RefCodeInfoContent
import sdk.net.meet.api.referral.responses.RefCodeInfoTemplate
import sdk.net.meet.api.referral.responses._POST_RefCodeInfoSend
import sdk.net.meet.api.referral.tasks.RefCodeInfoLinkPostTask
import sdk.net.meet.api.referral.tasks.TemplateFetchTask
import sdk.net.meet.api.referral.tasks.TemplatePostTask
import sdk.net.meet.net.network.executeRequest

internal class DefaultReferralTemplateFetchTask(private val apiReferrals: ApiReferral): TemplateFetchTask {
    override suspend fun execute(params: TemplateFetchTask.Params): ResultGetTemplate {
        try {
            val result =  executeRequest(null) {
                apiReferrals.fetchAdminTemplate(authHeader = params.authParam,
                    limit = params.limit,
                    page  =  params.page)
            }
            return result
        } catch (th: Throwable) {
            throw th.isReferralError()
                ?.let{
                    Failure.ServerError(it)
                } ?: th
        }
    }
}

internal class DefaultReferralTemplatePostTask(private val apiReferrals: ApiReferral): TemplatePostTask {
    override suspend fun execute(params: TemplatePostTask.Params): ResultPostTemplate {
        try {
            val result = executeRequest(null) {
                apiReferrals.postAdminTemplate(
                    authHeader = params.authParam,
                    body = params.body
                )
            }
            return result
        } catch (th: Throwable) {
            throw th.isReferralError()
                ?.let {
                    Failure.ServerError(it)
                } ?: th
        }
    }

    internal class DefaultRefCodeInfoPostTask(private val apiReferrals: ApiReferral) :
        RefCodeInfoLinkPostTask {
        override suspend fun execute(params: RefCodeInfoLinkPostTask.Params): _POST_RefCodeInfoSend {
            return try {
                val result = executeRequest(null) {
                    apiReferrals.sendReferralsInformation(
                        authHeader = params.authParam,
                        body = params.body
                    )
                }
                _POST_RefCodeInfoSend.Success(result)
            } catch (th: Throwable) {
                _POST_RefCodeInfoSend.Failure( throw th.isReferralError()
                    ?.let {
                        Failure.ServerError(it)
                    } ?: th)
            }
        }
    }
}