/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.net.referral

import retrofit2.http.*
import sdk.net.meet.api.referral.*
import sdk.net.meet.api.referral.models.objModelReferralDelRes
import sdk.net.meet.api.referral.responses.RefCodeInfoTemplate
import sdk.net.meet.net.network.NetConst

interface ApiReferral {
    companion object{
        const val SUCCESS = "success"
        internal fun SUCCESS(value: String) = SUCCESS.equals(value)
    }
//    /***************************************************
//     *    section referrals analytics endpoint...
//     ***************************************************/
//    @GET("${NetConst.REFERRAL}")
//    suspend fun fetchAnalyticsLink(@Query("user-id"      ) userID: Int,
//                                   @Query("package_name" ) packageName:   String? = null,
//                                   @Query("dynamic_link" ) dynamicLink:   String? = null,
//                                   @Query("duration_days") durationDAys: String? = null ): String
//
//    @GET("${NetConst.API_VER_REF}")
//    suspend fun fetchUnregistered(@Query("referrer_code") referrerCode:  String? = null ): String
//
//    /***************************************************
//     *    section referrals main endpoint...
//     ***************************************************/
//    @GET("${NetConst.API_VER_MEET}")
//    suspend fun fetchInvite(@Query("user-id") userID: Int,
//                            @Query("package_name") packageName : String? = null): String
//
//    @GET   ("${NetConst.API_VER_MEET}/")
//    suspend fun fetchReferralsByUserId(@Query("user-id") userID: Int): String
//
//    @POST("${NetConst.API_VER_MEET}/")
//    suspend fun postReferralsByUserId(@Header("user-id") userID: Int,
//                                      @Body data: String): String

    /*********************************************************
     *    section referrals template...
     *********************************************************/
    @POST ("${NetConst.API_VER_REF}")  // update this address for working
    suspend fun sendReferralsInformation(
        @Header("user-id") authHeader: String,
//        @Header("authHeader")authHeader: String,
        @Body body: String): RefCodeInfoTemplate


    @GET   ("${NetConst.API_VER_REF}/admin/template")
    suspend fun fetchAdminTemplate(
        @Header("user-id") authHeader: String,
//        @Header("Authorization") authHeader: String,
                                   @Query ("limit") limit             : Int,
                                   @Query ("page" ) page              : Int?): ResultGetTemplate
    @POST  ("${NetConst.API_VER_REF}/admin/template")
    suspend fun postAdminTemplate (
        @Header("user-id") authHeader: String,
//        @Header("Authorization") authHeader: String,
                                   @Body body: String): ResultPostTemplate

//    /*********************************************************
//     *    section referrals admin...
//     *********************************************************/
//    @GET   ("${NetConst.API_VER_REF}/admin/referrals-list/")
//    suspend fun fetchAdminList(@Header("Authorization") authHeader     : String,
//                               @Query ("orderBy")        orderBy       : String,
//                               @Query ("orderDirection") orderDirection: String,
//                               @Query ("search")         search        : String,
//                               @Query ("page")           page          : Int,
//                               @Query ("per_page")       perPage       : Int,
//                               @Query ("bulk" )          bulk: Boolean): String
//    @GET   ("${NetConst.API_VER_REF}/admin/referrals-list/{id}")
//    suspend fun fetchAdminListById(@Header("Authorization") authHeader : String ): String

    /*********************************************************
     *    section referrals management endpoint...
     *********************************************************/
//    @POST("${NetConst.API_VER_MEET}${NetConst.REF_VAL_USER}")
//    suspend fun postReferralsValidUser(@Body data: String): String  // this endpoint deleted in new version

//    @POST  ("${NetConst.API_VER_REF}/manager/validate/referrer")
//    suspend fun postReferralsValidReferer(@Header("Authorization") authHeader: String,
//                                          @Body data: String): ResultGetReferrals

    /*********************************************************
     *    section referral code...
     *********************************************************/
    @GET   ("${NetConst.API_VER_REF}/referral-codes")
    suspend fun fetchReferralCodes(
        @Header("user-id") authHeader : String,
//        @Header("Authorization") authHeader : String
        @Query("application_id") appID: String,
    ): objModelReferralCode.ResultGetReferralCode

    @POST  ("${NetConst.API_VER_REF}/referral-codes")
    suspend fun postReferralCodes (
        @Header("user-id") authHeader: String,
//        @Header("Authorization") authHeader: String,
        @Body body: objModelReferralCode.ReferralCodeParamsPst
    ): objModelReferralCode.ReferralResult

    @GET   ("${NetConst.API_VER_REF}/referral-codes/{id}")
    suspend fun fetchReferralCodeById(
//        @Header("Authorization") authHeader: String,
        @Header("user-id") authHeader: String,
        @Path("id") id: String): objModelReferralCode.ReferralResult

    @PUT   ("${NetConst.API_VER_REF}/referral-codes/{id}")
    suspend fun updateReferralCodeById(
//        @Header("Authorization") authHeader: String,
        @Header("user-id") authHeader: String,
        @Path("id")                id: String,
        @Body body: objModelReferralCode.ReferralCodeParamsPut): objModelReferralCode.ReferralResult

    @DELETE("${NetConst.API_VER_REF}/referral-codes/{id}")
    suspend fun deleteReferralCodeById(
//        @Header("Authorization") authHeader: String,
        @Header("user-id") authHeader: String,
        @Path("id") id: String): objModelReferralDelRes.ResultDeleteReferrals

    @GET   ("${NetConst.API_VER_REF}/referral-codes/{id}/set")
    suspend fun fetchReferralCodeNew(
//        @Header("Authorization") authHeader: String,
        @Header("user-id") authHeader: String,
        @Query("id") id: String): ResultGetReferralsById

}