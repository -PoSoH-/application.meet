/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.net.network

import android.util.Log
import sdk.net.meet.LogUtil

inline fun <A> tryOrNull(message: String? = null, operation: () -> A): A? {
    return try {
        operation()
    } catch (any: Throwable) {
        if (message != null) {
            LogUtil.error("NETWORK ERROR SHOW",  "${any} -> ${message}")
        }
        null
    }
}
