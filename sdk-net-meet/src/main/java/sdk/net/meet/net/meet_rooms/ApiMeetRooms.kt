package sdk.net.meet.net.meet_rooms

import retrofit2.http.*
import sdk.net.meet.api.meet_rooms.request.FromMeetRooms
import sdk.net.meet.api.meet_rooms.response.FetchAllOwnerRoom
import sdk.net.meet.api.meet_rooms.response.FetchOwnerRoom
import sdk.net.meet.api.meet_rooms.response.FetchDeleteRoom
import sdk.net.meet.api.meet_rooms.response.FetchUpdateRoom
import sdk.net.meet.net.network.NetConst

interface ApiMeetRooms {

    @GET("${NetConst.API_VER_MEE}/meetings")
    suspend fun meetRoomGET(
        @Header("user-id") authHeader: String,
        @Body body: FromMeetRooms
    ): FetchAllOwnerRoom


    @POST("${NetConst.API_VER_MEE}/meetings")
    suspend fun meetRoomPST(
        @Header("user-id") authHeader: String,
//        @Header("Authorization") authHeader: String,
//        @Header("application_id") appId: String,
        @Body body: FromMeetRooms
    ): FetchOwnerRoom

    @GET("${NetConst.API_VER_MEE}/meetings{id}")
    suspend fun meetRoomByIdGET(
        @Header("user-id") authHeader: String,
//        @Header("Authorization") authHeader: String,
//        @Header("application_id") appId: String,
        @Path("id") roomId: String,
        @Body body: FromMeetRooms
    ): FetchOwnerRoom

    @PUT("${NetConst.API_VER_MEE}/meetings/{id}")
    suspend fun meetRoomPUT(
        @Header("user-id") authHeader: String,
//        @Header("Authorization") authHeader: String,
//        @Header("application_id") appId: String,
        @Path("id") roomId: String,
        @Body body: FromMeetRooms
    ): FetchUpdateRoom

    @DELETE("${NetConst.API_VER_MEE}/meetings/{id}")
    suspend fun meetRoomDEL(
        @Header("user-id") authHeader: String,
//        @Header("Authorization") authHeader: String,
//        @Header("application_id") appId: String,
        @Path("id") roomId: String,
    ): FetchDeleteRoom
}