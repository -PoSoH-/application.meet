package sdk.net.meet.net.contacts

import sdk.net.meet.api.contacts.response.*
import sdk.net.meet.api.contacts.tasks.*
import sdk.net.meet.api.failure.Failure
import sdk.net.meet.api.failure.isContactError
import sdk.net.meet.api.failure.isReferralError
import sdk.net.meet.net.network.executeRequest

internal class DefaultContactTasks {

    internal class DefaultContactCategoryTask(private val apiContacts: ApiContacts): ITaskContactCategory {
        override suspend fun execute(params: ITaskContactCategory.Params): TaskCategory {
            try {
                val result =  executeRequest(null) {
                    apiContacts.fetchContactCategories(authHeader = params.authParam)
                }
                return TaskCategory.Success(result)
            } catch (th: Throwable) {
                val tmp = th.isContactError()
                return TaskCategory.Failure(tmp!!.failure)
            }
        }
    }

    internal class DefaultContactFetchTask(private val apiContacts: ApiContacts): ITaskContactsGet {
        override suspend fun execute(params: ITaskContactsGet.Params): TaskAllContacts {
            try {
                val result =  executeRequest(null) {
                    apiContacts.fetchContacts(authHeader = params.authParam,
                        limit = params.limit,
                        page = params.page,
                        search = params.search,
                        isFavorite = params.isFavorite,
                        isRecently = params.isRecently,
                        byLetter = params.byLetter,
                        groupId = params.groupId,
                        by = params.by,
                        order = params.order)
                }
                return TaskAllContacts.Success(result)
            } catch (th: Throwable) {
                val tmp = th.isContactError()
                return TaskAllContacts.Failure(tmp!!.failure)
//                throw th.isContactError()
//                    ?.let{
//                        Failure.ServerError(it)
//                    } ?: th
            }
        }
    }

    internal class DefaultContactByIdTask(private val apiContacts: ApiContacts): ContactsByIdGetTask {
        override suspend fun execute(params: ContactsByIdGetTask.Params): ResponseContact {
            try {
                val result =  executeRequest(null) {
                    apiContacts.fetchContactById(
                        authHeader = params.authParam,
                        id = params.contactId)
                }
                return result
            } catch (th: Throwable) {
                throw th.isReferralError()
                    ?.let{
                        Failure.ServerError(it)
                    } ?: th
            }
        }
    }

    internal class DefaultContactPostTask(private val apiContacts: ApiContacts): ContactPostTask {
        override suspend fun execute(params: ContactPostTask.Params): ResponseContact {
            try {
                val result =  executeRequest(null) {
                    apiContacts.contactSendPost(
                        authHeader = params.authParam,
                        bodyContent = params.bodyContent)
                }
                return result
            } catch (th: Throwable) {
                throw th.isReferralError()
                    ?.let{
                        Failure.ServerError(it)
                    } ?: th
            }
        }
    }

    internal class TaskDefaultContactImportByJson(private val apiContacts: ApiContacts): ITaskImportContactByJson {
        override suspend fun execute(params: ITaskImportContactByJson.Params): FetchImportContactByJson {
            try {
                val result =  executeRequest(null) {
                    apiContacts.postContactImportByJson(
                        authHeader = params.authHeaders,
                        bodyContent = params.contentBody)
                }
                return result
            } catch (th: Throwable) {
                throw th.isReferralError()
                    ?.let{
                        Failure.ServerError(it)
                    } ?: th
            }
        }
    }

    internal class TaskDefaultContactCreate(private val apiContacts: ApiContacts): ITaskContactCreate {
        override suspend fun execute(params: ITaskContactCreate.Params): FetchContactCreate {
            try {
                val result =  executeRequest(null) {
                    apiContacts.postContactCreate(
                        authHeader = params.authHeaders,
                        bodyContent = params.contentBody)
                }
                return result
            } catch (th: Throwable) {
                throw th.isReferralError()
                    ?.let{
                        Failure.ServerError(it)
                    } ?: th
            }
        }
    }

}