package sdk.net.meet.net.meet_calendar

import sdk.net.meet.api.failure.isCalendarError
import sdk.net.meet.api.meet_calendar.TaskRespCalendar
import sdk.net.meet.api.meet_calendar.tasks.*

class TasksMeetCalendar {

    internal class TaskCalendarsFetch(private val apiMeetCalendar: ApiMeetCalendar): ITaskMeetCalendarGet {
        override suspend fun execute(params: ITaskMeetCalendarGet.Params): TaskRespCalendar {
            try {
                val resp = apiMeetCalendar.meetCalendarGET(params.authParam)
                return TaskRespCalendar.Data(body = resp.body)
            } catch (th: Throwable){
                th.isCalendarError().let{
                    return TaskRespCalendar.Fail(it!!.failure)
                }
            }
        }
    }

    internal class TaskCalendarsFetchById(private val apiMeetCalendar: ApiMeetCalendar): ITaskMeetCalendarByIdGet {
        override suspend fun execute(params: ITaskMeetCalendarByIdGet.Params): TaskRespCalendar {
            try {
                val resp = apiMeetCalendar.meetCalendarByIdGET(params.authParam, params.calendarId)
                return TaskRespCalendar.Data(body = resp)
            } catch (th: Throwable){
                th.isCalendarError().let{
                    return TaskRespCalendar.Fail(it!!.failure)
                }
            }
        }
    }

    internal class TaskCalendarsUpdate(private val apiMeetCalendar: ApiMeetCalendar): ITaskMeetCalendarPut {
        override suspend fun execute(params: ITaskMeetCalendarPut.Params): TaskRespCalendar {
            try {
                val resp = apiMeetCalendar.meetCalendarPUT(params.authParam, params.calendarID, params.body)
                return TaskRespCalendar.Data(body = resp)
            } catch (th: Throwable){
                th.isCalendarError().let{
                    return TaskRespCalendar.Fail(it!!.failure)
                }
            }
        }
    }

    internal class TaskCalendarsCreate(private val apiMeetCalendar: ApiMeetCalendar): ITaskMeetCalendarPst {
        override suspend fun execute(params: ITaskMeetCalendarPst.Params): TaskRespCalendar {
            try {
                val resp = apiMeetCalendar.meetCalendarPST(params.authParam, params.body)
                return TaskRespCalendar.Data(body = resp)
            } catch (th: Throwable){
                th.isCalendarError().let{
                    return TaskRespCalendar.Fail(it!!.failure)
                }
            }
        }
    }

    internal class TaskCalendarsDelete(private val apiMeetCalendar: ApiMeetCalendar): ITaskMeetCalendarDel {
        override suspend fun execute(params: ITaskMeetCalendarDel.Params): TaskRespCalendar {
            try {
                val resp = apiMeetCalendar.meetCalendarDEL(params.authParam, params.calendarID)
                return TaskRespCalendar.Data(body = resp)
            } catch (th: Throwable){
                th.isCalendarError().let{
                    return TaskRespCalendar.Fail(it!!.failure)
                }
            }
        }
    }

}