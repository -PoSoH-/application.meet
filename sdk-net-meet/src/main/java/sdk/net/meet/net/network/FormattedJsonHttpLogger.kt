/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.net.network

import android.util.Log
import androidx.annotation.NonNull
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import sdk.net.meet.LogUtil

class FormattedJsonHttpLogger: HttpLoggingInterceptor.Logger {
    companion object {
        private const val INDENT_SPACE = 2
    }

    /**
     * Log the message and try to log it again as a JSON formatted string
     * Note: it can consume a lot of memory but it is only in DEBUG mode
     *
     * @param message
     */
    @Synchronized
    override fun log(@NonNull message: String) {
        Log.v("FormattedJsonHttpLogger", message)

        if (message.startsWith("{")) {
            // JSON Detected
            try {
                val o = JSONObject(message)
                logJson(o.toString(INDENT_SPACE))
            } catch (e: JSONException) {
                // Finally this is not a JSON string...
                LogUtil.error("FormattedJsonHttpLogger", e.message.toString())
            }
        } else if (message.startsWith("[")) {
            // JSON Array detected
            try {
                val o = JSONArray(message)
                logJson(o.toString(INDENT_SPACE))
            } catch (e: JSONException) {
                // Finally not JSON...
                Log.v("FormattedJsonHttpLogger", e.message.toString())
            }
        }
        // Else not a json string to log
    }

    private fun logJson(formattedJson: String) {
        formattedJson
            .lines()
            .dropLastWhile { it.isEmpty() }
            .forEach { Log.v("FormattedJsonHttpLogger", it) }
    }
}