/*
 * Copyright 2021 BGC Global Partners
 */

package sdk.net.meet.net.auth

import dagger.Module

@Module
internal abstract class AuthModule {

//    @Module
//    companion object {
//        private const val DB_ALIAS = "Meet-sdk-auth"
//
//        @JvmStatic
//        @Provides
//        fun providesRealmConfiguration(context: Context){ //: RealmConfiguration {
////            val old = File(context.filesDir, "Meet-sdk-auth")
////            if (old.exists()) {
////                old.renameTo(File(context.filesDir, "Meet-sdk-auth.realm"))
////            }
//
////            return RealmConfiguration.Builder()
////                    .apply {
////                        realmKeysUtils.configureEncryption(this, DB_ALIAS)
////                    }
////                    .name("Meet-sdk-auth.realm")
////                    .modules(AuthRealmModule())
////                    .schemaVersion(AuthRealmMigration.SCHEMA_VERSION)
////                    .migration(AuthRealmMigration)
////                    .build()
//        }
//    }

//    @Binds
//    abstract fun bindLegacySessionImporter(importer: DefaultLegacySessionImporter): LegacySessionImporter
//
//    @Binds
//    abstract fun bindSessionParamsStore(store: RealmSessionParamsStore): SessionParamsStore
//
//    @Binds
//    abstract fun bindPendingSessionStore(store: RealmPendingSessionStore): PendingSessionStore
//
//    @Binds
//    abstract fun bindAuthenticationService(service: DefaultAuthenticationService): AuthenticationService
//
//    @Binds
//    abstract fun bindSessionCreator(creator: DefaultSessionCreator): SessionCreator
//
//    @Binds
//    abstract fun bindDirectLoginTask(task: DefaultDirectLoginTask): DirectLoginTask
//
//    @Binds
//    abstract fun bindIsValidClientServerApiTask(task: DefaultIsValidClientServerApiTask): IsValidClientServerApiTask
//
//    @Binds
//    abstract fun bindHomeServerHistoryService(service: DefaultHomeServerHistoryService): HomeServerHistoryService

}
