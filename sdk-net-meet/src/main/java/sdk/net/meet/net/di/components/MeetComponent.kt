package sdk.net.meet.net.di.components

import android.content.Context
import android.content.res.Resources
import com.google.gson.Gson
import dagger.BindsInstance
import dagger.Component
import okhttp3.OkHttpClient
import sdk.net.meet.api.NetworkServices
import sdk.net.meet.api.referral.ReferralWizard
import sdk.net.meet.net.MeetCoroutineDispatchers
import sdk.net.meet.net.di.modules.MeetModule
import sdk.net.meet.net.di.modules.NetworkModule
import sdk.net.meet.net.di.scoupe.MeetNetScope
import sdk.net.meet.net.network.DefaultNetworkService
import javax.inject.Inject

@Component(modules = [
    MeetModule::class,
    NetworkModule::class
])
@MeetNetScope
interface MeetComponent  {

//    fun context(): Context

//    fun meetCoroutineDispatchers(): MeetCoroutineDispatchers
//
    fun getNetworkServices(): NetworkServices

    fun gson(): Gson

//    fun resources(): Resources

    fun okHttpClient(): OkHttpClient

//    @MockHttpInterceptor
//    fun testInterceptor(): ?

//    fun authService()     : AuthService

//    fun referralsService(): ReferralWizard

//    fun rawService(): RawService
//
//    fun homeServerHistoryService(): HomeServerHistoryService
//


//
//    fun MeetConfiguration(): MeetConfiguration
//



//    @CacheDirectory
//    fun cacheDir(): File
//    fun olmManager(): OlmManager
//    fun taskExecutor(): TaskExecutor
//    fun sessionParamsStore(): SessionParamsStore
//    fun backgroundDetectionObserver(): BackgroundDetectionObserver
//    fun sessionManager(): SessionManager
//    fun inject(Meet: Meet)

//    @Component.Factory
//    interface Factory {
//        fun create(@BindsInstance context: Context,
//                   @BindsInstance MeetConfiguration: MeetConfiguration): MeetComponent
//    }

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): MeetComponent
    }

}

