/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.net.referral

import sdk.net.meet.api.failure.Failure
import sdk.net.meet.api.failure.isReferralError
import sdk.net.meet.api.referral.models.objModelReferralDelRes
import sdk.net.meet.api.referral.objModelReferralCode
import sdk.net.meet.api.referral.tasks.ReferralCodeDeleteTask
import sdk.net.meet.api.referral.tasks.ReferralCodeGetByIdTask
import sdk.net.meet.api.referral.tasks.ReferralCodeGetTask
import sdk.net.meet.api.referral.tasks.ReferralCodePostTask
import sdk.net.meet.api.referral.tasks.ReferralCodePutTask
import sdk.net.meet.net.network.executeRequest

internal class DefaultReferralCodeGetTask(private val apiReferrals: ApiReferral): ReferralCodeGetTask {
    override suspend fun execute(params: ReferralCodeGetTask.Params): objModelReferralCode.ResultGetReferralCode {
        try {
            val result =  executeRequest(null) {
                apiReferrals.fetchReferralCodes(authHeader = params.authParam, appID = params.applicationID)
            }
            return result
        } catch (th: Throwable) {
            throw th.isReferralError()
                ?.let{
                    Failure.ServerError(it)
                }
                ?: th
        }
    }

}

internal class DefaultReferralCodeGetByIdTask(private val apiReferrals: ApiReferral): ReferralCodeGetByIdTask {
    override suspend fun execute(params: ReferralCodeGetByIdTask.Params): objModelReferralCode.ReferralResult {
        try {
            val result =  executeRequest(null) {
                apiReferrals.fetchReferralCodeById(authHeader = params.authParam, id = params.referralID)
            }
            return result
        } catch (th: Throwable) {
            throw th.isReferralError()
                ?.let{
                    Failure.ServerError(it)
                }
                ?: th
        }
    }

}

internal class DefaultReferralCodePostTask(private val apiReferrals: ApiReferral): ReferralCodePostTask {
    override suspend fun execute(params: ReferralCodePostTask.Params): objModelReferralCode.ReferralResult {
        try {
             val result =  executeRequest(null) {
                 apiReferrals.postReferralCodes(authHeader = params.authParam,
                     body = params.body)
             }
            return result
        } catch (th: Throwable) {
            throw th.isReferralError()
                ?.let {
                    Failure.ServerError(it)
                } ?: th
        }
    }
}

internal class DefaultReferralCodePutTask(private val apiReferrals: ApiReferral): ReferralCodePutTask {
    override suspend fun execute(params: ReferralCodePutTask.Params): objModelReferralCode.ReferralResult {
        try {
            val result =  executeRequest(null) {
                apiReferrals.updateReferralCodeById(authHeader = params.authParam,
                    id = params.referralID,
                    body = params.body)
            }
            return result
        } catch (th: Throwable) {
            throw th.isReferralError()
                ?.let{
                    Failure.ServerError(it)
                } ?: th
        }
    }
}

internal class DefaultReferralCodeDelTask(private val apiReferrals: ApiReferral): ReferralCodeDeleteTask {
    override suspend fun execute(params: ReferralCodeDeleteTask.Params): objModelReferralDelRes.ResultDeleteReferrals {
        try {
            val result =  executeRequest(null) {
                apiReferrals.deleteReferralCodeById(authHeader = params.authParam, id = params.referralID)
            }
            return result
        } catch (th: Throwable) {
            throw th.isReferralError()
                ?.let{
                    Failure.ServerError(it)
                } ?: th
        }
    }
}