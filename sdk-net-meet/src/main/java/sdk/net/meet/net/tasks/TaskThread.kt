/*
 * Copyright 2021 The BGC Global Partners
 */


package sdk.net.meet.net.tasks

internal enum class TaskThread {
    MAIN,
    COMPUTATION,
    IO,
    CALLER,
//    CRYPTO,
    DM_VERIF
}
