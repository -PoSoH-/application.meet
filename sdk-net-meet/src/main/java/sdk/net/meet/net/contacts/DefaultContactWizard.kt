package sdk.net.meet.net.contacts

import sdk.net.meet.api.contacts.ApiGetConfig
import sdk.net.meet.api.contacts.ContactWizard
import sdk.net.meet.api.contacts._ResponseContacts
import sdk.net.meet.api.contacts.request.FromAddContact
import sdk.net.meet.api.contacts.request.FromImportContactByJson
import sdk.net.meet.api.contacts.tasks.*

class DefaultContactWizard(apiCurrentPath: ApiContacts): ContactWizard {

    private val taskExecCategory = DefaultContactTasks.DefaultContactCategoryTask(apiCurrentPath)
    private val taskFetchContacts = DefaultContactTasks.DefaultContactFetchTask(apiCurrentPath)
    private val taskContactById = DefaultContactTasks.DefaultContactByIdTask(apiCurrentPath)
    private val taskContactPost = DefaultContactTasks.DefaultContactPostTask(apiCurrentPath)
    private val taskContactImportByJson = DefaultContactTasks.TaskDefaultContactImportByJson(apiCurrentPath)
    private val taskContactCreate = DefaultContactTasks.TaskDefaultContactCreate(apiCurrentPath)

    override suspend fun fetchContactCategories(authHeader: String): TaskCategory {
        val task = taskExecCategory.execute(ITaskContactCategory.Params(authParam = authHeader))
        return task
    }

    override suspend fun fetchAllContacts(authHeader: String, appId: String, config: ApiGetConfig): TaskAllContacts {
        val task = taskFetchContacts.execute(ITaskContactsGet.Params(authParam = authHeader,
            limit = config.limit,
            page = config.page,
            search = config.search,
            isFavorite = config.isFavorite,
            isRecently = config.isRecently,
            byLetter = config.byLetter,
            groupId = config.groupId,
            by = config.sortBy,
            order =  config.sortOrder))
        return task
    }

    override suspend fun fetchContactsPagination(
        authHeader: String,
        limit: Int,
        page: Int,
        search: String,
        isFavorite: Boolean,
        isRecently: Boolean,
        byLetter: String,
        groupId: String,
        sortBy: String,
        sortOrder: String
    ): _ResponseContacts {
        TODO("Not yet implemented")
    }

    override suspend fun fetchContactsById(authHeader: String, contactId: String): _ResponseContacts {
        TODO("Not yet implemented")
    }

    override suspend fun createContact(
        authHeader: String,
        modelContact: FromAddContact
    ): _ResponseContacts {
        val tmp = taskContactCreate.execute(ITaskContactCreate.Params(authHeaders = authHeader, contentBody = modelContact))
        return _ResponseContacts._ResultSuccess(tmp)
    }

    override suspend fun addImportContactsByJson(authHeader: String, jsonContact: FromImportContactByJson): _ResponseContacts {
        TODO("Not yet implemented")
    }
}