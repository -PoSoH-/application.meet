/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.net.network

import sdk.net.meet.api.failure.GlobalError

internal interface GlobalErrorReceiver {
    fun handleGlobalError(globalError: GlobalError)
}