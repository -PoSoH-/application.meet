/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.net

import kotlinx.coroutines.Job
import sdk.net.meet.api.failure.Cancelable

internal fun Job.toCancelable(): Cancelable {
    return CancelableCoroutine(this)
}

private class CancelableCoroutine(private val job: Job) : Cancelable {
    override fun cancel() {
        if (!job.isCancelled) {
            job.cancel()
        }
    }
}
