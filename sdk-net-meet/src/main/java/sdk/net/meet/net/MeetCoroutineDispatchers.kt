/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.net

import kotlinx.coroutines.CoroutineDispatcher

internal data class MeetCoroutineDispatchers(
        val io: CoroutineDispatcher,
        val computation: CoroutineDispatcher,
        val main: CoroutineDispatcher,
//        val crypto: CoroutineDispatcher,
        val dmVerif: CoroutineDispatcher
)