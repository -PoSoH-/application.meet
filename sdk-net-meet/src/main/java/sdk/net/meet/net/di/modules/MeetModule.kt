package sdk.net.meet.net.di.modules

import android.content.Context
import android.content.res.Resources
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.asCoroutineDispatcher
import sdk.net.meet.NetworkConnectURL
import sdk.net.meet.net.MeetCoroutineDispatchers
import sdk.net.meet.net.di.scoupe.MeetNetScope
import java.util.concurrent.Executors

@Module
internal object MeetModule {

    @JvmStatic
    @Provides
    @MeetNetScope
    fun providesCoroutineDispatchers(): MeetCoroutineDispatchers {
        return MeetCoroutineDispatchers(io = Dispatchers.IO,
            computation = Dispatchers.Default,
            main = Dispatchers.Main,
            dmVerif = Executors.newSingleThreadExecutor().asCoroutineDispatcher()
        )
    }

    @JvmStatic
    @Provides
    fun providesResources(context: Context): Resources {
        return context.resources
    }

    @JvmStatic
    @Provides
    fun providesReferralUrl(): NetworkConnectURL {
        return NetworkConnectURL()
    }
}