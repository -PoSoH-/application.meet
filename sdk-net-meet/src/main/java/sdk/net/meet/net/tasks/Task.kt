/*
 * Copyright 2021 The BGC Global Partners
 */


package sdk.net.meet.net.tasks

interface Task<PARAMS, RESULT> {

    suspend fun execute(params: PARAMS): RESULT
}
