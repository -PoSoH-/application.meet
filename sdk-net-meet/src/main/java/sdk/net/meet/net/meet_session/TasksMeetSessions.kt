package sdk.net.meet.net.meet_session

import sdk.net.meet.api.failure.isContactError
import sdk.net.meet.api.meet_session.TaskRespMeetSession
import sdk.net.meet.api.meet_session.tasks.ITaskMeetSessionCreate
import sdk.net.meet.api.meet_session.tasks.ITaskMeetSessionUpdate

internal class TasksMeetSessions {

    internal class TaskMeetSessionCreate(private val api: ApiMeetSession): ITaskMeetSessionCreate{
        override suspend fun execute(params: ITaskMeetSessionCreate.Params): TaskRespMeetSession {
            try {
                val resp = api.meetSessionPST(params.auth, params.body)
                return TaskRespMeetSession.Data(resp)
            } catch (th: Throwable) {
                th.isContactError().let {
                    return TaskRespMeetSession.Fail(it!!.failure)
                }
            }
        }

    }

    internal class TaskMeetSessionUpdate(private val api: ApiMeetSession): ITaskMeetSessionUpdate{
        override suspend fun execute(params: ITaskMeetSessionUpdate.Params): TaskRespMeetSession {
            try {
                val resp = api.meetSessionPUT(params.auth, params.roomId, params.body)
                return TaskRespMeetSession.Data(resp)
            } catch (th: Throwable) {
                th.isContactError().let {
                    return TaskRespMeetSession.Fail(it!!.failure)
                }
            }
        }
    }
}