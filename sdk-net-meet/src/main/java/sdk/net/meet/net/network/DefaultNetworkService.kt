/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.net.network

import dagger.Lazy
import okhttp3.OkHttpClient
import sdk.net.meet.LogUtil
import sdk.net.meet.NetworkConnectURL
import sdk.net.meet.api.NetworkServices
import sdk.net.meet.api.contacts.ContactWizard
import sdk.net.meet.api.meet_calendar.IWizardMeetCalendar
import sdk.net.meet.api.meet_profile.IWizardMeetProfile
import sdk.net.meet.api.meet_profile.TaskProfileResult
import sdk.net.meet.api.meet_rooms.IWizardMeetRooms
import sdk.net.meet.api.meet_session.IWizardMeetSession
import sdk.net.meet.api.referral.*
import sdk.net.meet.net.contacts.ApiContacts
import sdk.net.meet.net.contacts.DefaultContactWizard
import sdk.net.meet.net.meet_calendar.ApiMeetCalendar
import sdk.net.meet.net.meet_calendar.TasksMeetCalendar
import sdk.net.meet.net.meet_calendar.WizardMeetCalendar
import sdk.net.meet.net.meet_profile.ApiMeetProfile
import sdk.net.meet.net.meet_profile.WizardMeetProfile
import sdk.net.meet.net.meet_rooms.ApiMeetRooms
import sdk.net.meet.net.meet_rooms.TasksMeetRooms
import sdk.net.meet.net.meet_rooms.WizardMeetRooms
import sdk.net.meet.net.meet_session.ApiMeetSession
import sdk.net.meet.net.meet_session.TasksMeetSessions
import sdk.net.meet.net.meet_session.WizardMeetSession
import sdk.net.meet.net.referral.ApiReferral
import sdk.net.meet.net.referral.DefaultReferralsWizard
import javax.inject.Inject

internal class DefaultNetworkService @Inject constructor(
    private val okHttpClient   : Lazy<OkHttpClient>,
    private val retrofitFactory: RetrofitFactory,
    private val basePathURL    : NetworkConnectURL
): NetworkServices {

    private var currentReferralWizard: ReferralWizard? = null
    private var currentContactWizard : ContactWizard?  = null

    private var currentMeetCalendarWizard: IWizardMeetCalendar? = null
    private var currentMeetRoomsWizard   : IWizardMeetRooms? = null
    private var currentMeetSessionWizard : IWizardMeetSession? = null
    private var currentMeetProfileWizard : IWizardMeetProfile? = null


    override fun getReferralWizard(): ReferralWizard {
        val content = currentReferralWizard
            ?: let {
                basePathURL.referralURL.let{
                    DefaultReferralsWizard(buildReferralAPI(basePathURL.referralURL))
                }.also {
                    currentReferralWizard = it
                }?: error("Please call getLoginFlow() with success first")
            }
        return content
    }

    override fun getContactWizard(): ContactWizard {
        val content = currentContactWizard
            ?: let {
                basePathURL.contactsURL.let{
                    DefaultContactWizard(buildContactAPI(basePathURL.contactsURL))
                }.also {
                    currentContactWizard = it
                }?: error("Please call getLoginFlow() with success first")
            }
        return content
    }

    override fun getWizardProfile(): IWizardMeetProfile {
        val profile = currentMeetProfileWizard
            ?: let {
                basePathURL.meetServiceURL.let{
                    WizardMeetProfile(buildMeetProfileAPI(it))
                }.also {
                    currentMeetProfileWizard = it
                }
            }
        return profile
    }

    override fun getWizardRooms(): IWizardMeetRooms {
        val room = currentMeetRoomsWizard
            ?: let {
                basePathURL.meetServiceURL.let{
                    WizardMeetRooms(buildMeetRoomsAPI(it))
                }.also {
                    currentMeetRoomsWizard = it
                } ?: error("Please call getLoginFlow() with success first")
            }
        return room
    }

    override fun getWizardSession(): IWizardMeetSession {
        val session = currentMeetSessionWizard
            ?: let {
                basePathURL.meetServiceURL.let{
                    WizardMeetSession(buildMeetSessionAPI(it))
                }.also {
                    currentMeetSessionWizard = it
                } ?: error("Please call getLoginFlow() with success first")
            }
        return session
    }

    override fun getWizardCalendar(): IWizardMeetCalendar {
        val calendar = currentMeetCalendarWizard
            ?: let {
                basePathURL.meetServiceURL.let{
                    WizardMeetCalendar(buildMeetCalendarAPI(basePathURL.meetServiceURL))
                }.also {
                    currentMeetCalendarWizard = it
                } ?: error("Please call getLoginFlow() with success first")
            }
        return calendar
    }

//    private fun buildAuthAPI(homeServerConnectionConfig: HomeServerConnectionConfig): AuthAPI {
//        val retrofit = retrofitFactory.create(buildClient(homeServerConnectionConfig), homeServerConnectionConfig.homeServerUri.toString())
//        return retrofit.create(AuthAPI::class.kotlin)
//    }

    private fun buildMeetCalendarAPI(urlPath: String): ApiMeetCalendar {
        buildClient()
        val retrofit = retrofitFactory.create(okHttpClient, urlPath)
        LogUtil.info("OK HTTP CLIENT", retrofit.baseUrl().toString())
        val temp = retrofit.create(ApiMeetCalendar::class.java)
        return temp //retrofit.create(ApiReferral::class.kotlin)
    }
    private fun buildMeetRoomsAPI(urlPath: String): ApiMeetRooms {
        buildClient()
        val retrofit = retrofitFactory.create(okHttpClient, urlPath)
        LogUtil.info("OK HTTP CLIENT", retrofit.baseUrl().toString())
        val temp = retrofit.create(ApiMeetRooms::class.java)
        return temp //retrofit.create(ApiReferral::class.kotlin)
    }
    private fun buildMeetSessionAPI(urlPath: String): ApiMeetSession {
        buildClient()
        val retrofit = retrofitFactory.create(okHttpClient, urlPath)
        LogUtil.info("OK HTTP CLIENT", retrofit.baseUrl().toString())
        val temp = retrofit.create(ApiMeetSession::class.java)
        return temp //retrofit.create(ApiReferral::class.kotlin)
    }
    private fun buildMeetProfileAPI(urlPath: String): ApiMeetProfile {
        buildClient()
        val retrofit = retrofitFactory.create(okHttpClient, urlPath)
        LogUtil.info("OK HTTP CLIENT", retrofit.baseUrl().toString())
        val temp = retrofit.create(ApiMeetProfile::class.java)
        return temp //retrofit.create(ApiReferral::class.kotlin)
    }

    private fun buildReferralAPI(currentUrlPath: String): ApiReferral {
        buildClient()
        val retrofit = retrofitFactory.create(okHttpClient, currentUrlPath)
        LogUtil.info("OK HTTP CLIENT", retrofit.baseUrl().toString())
        val temp = retrofit.create(ApiReferral::class.java)
        return temp //retrofit.create(ApiReferral::class.kotlin)
    }

    private fun buildContactAPI(currentUrlPath: String): ApiContacts {
        buildClient()
        val retrofit = retrofitFactory.create(okHttpClient, currentUrlPath)
        LogUtil.info("OK HTTP CLIENT", retrofit.baseUrl().toString())
        val temp = retrofit.create(ApiContacts::class.java)
        return temp //retrofit.create(ApiReferral::class.kotlin)
    }

//    private fun buildAuthenticateAPI(homeServerConnectionConfig: HomeServerConnectionConfig): AuthAPI {
//        val retrofit = retrofitFactory.create(buildClient(homeServerConnectionConfig), homeServerConnectionConfig.sumraAuthWraperUri.toString())
//        return retrofit.create(AuthAPI::class.kotlin)
//    }

    private fun buildClient(): OkHttpClient {
        return okHttpClient.get()
            .newBuilder()
//            .addSocketFactory(referralURL)
            .build()
    }

//    private fun buildClient(referralURL: String): OkHttpClient {
//        return okHttpClient.get()
//            .newBuilder()
////            .addSocketFactory(referralURL)
//            .build()
//    }

}
