package sdk.net.meet.net.meet_profile

import sdk.net.meet.api.meet_profile.IWizardMeetProfile
import sdk.net.meet.api.meet_profile.TaskProfileResult
import sdk.net.meet.api.meet_profile.request.FromAddProfile
import sdk.net.meet.api.meet_profile.tasks.ITaskProfileGet
import sdk.net.meet.api.meet_profile.tasks.ITaskProfilePst
import sdk.net.meet.api.meet_profile.tasks.ITaskProfilePut
import sdk.net.meet.api.meet_rooms.TaskRespMeetRoom
import sdk.net.meet.api.meet_rooms.request.FromMeetByInvite
//import sdk.net.meet.api.meet_rooms.tasks.ITaskMeetRoomFetchInvite
import sdk.net.meet.api.meet_rooms.tasks.ITaskMeetRoomInvite

class WizardMeetProfile(apiProfile: ApiMeetProfile): IWizardMeetProfile {

    private val taskMeetProfileGet = TaskMeetProfiles.TaskDefaultMeetProfileGet(apiProfile)
    private val taskMeetProfilePst = TaskMeetProfiles.TaskDefaultMeetProfilePst(apiProfile)
    private val taskMeetProfilePut = TaskMeetProfiles.TaskDefaultMeetProfilePut(apiProfile)
    private val taskMeetRoomByInvite = TaskMeetProfiles.TaskDefaultMeetRoomInvitePst(apiProfile)
//    private val taskMeetRoomFetchInvite = TaskMeetProfiles.TaskDefaultMeetRoomInviteGet(apiProfile)

    override suspend fun userProfileGet(authHeader: String, appId: String): TaskProfileResult {
        return taskMeetProfileGet.execute(ITaskProfileGet.Params(authHeader, appId))
    }

    override suspend fun userProfilePut(
        authHeader: String,
        appId: String,
        profileId: String,
        body: FromAddProfile
    ): TaskProfileResult {
        return taskMeetProfilePut.execute(ITaskProfilePut.Params(authHeader, appId, profileId, body))
    }

    override suspend fun userProfilePos(
        authHeader: String,
        appId: String,
        body: FromAddProfile
    ): TaskProfileResult {
        return taskMeetProfilePst.execute(ITaskProfilePst.Params(authHeader, appId, body))
    }

/***   ***   ***   ***   ***   ***   ***   ***/
    override suspend fun userMeetRoomInvitePos(
        authHeader: String,
        appId: String,
        body: FromMeetByInvite
    ): TaskRespMeetRoom {
        return taskMeetRoomByInvite.execute(ITaskMeetRoomInvite.Params(authHeader, appId, body))
    }

//    override suspend fun userMeetFetchInviteGet(
//        authHeader: String,
//        appId: String,
//        invite: String
//    ): TaskRespMeetRoom {
//        val t = taskMeetRoomFetchInvite.execute(ITaskMeetRoomFetchInvite.Params(authHeader, appId, invite))
//        return t
//    }

}