/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.net

import android.net.Uri

internal class UriGsonAdapter {
    fun toJson(uri: Uri): String {
        return uri.toString()
    }
    fun fromJson(uriString: String): Uri {
        return Uri.parse(uriString)
    }
}