/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.net.network

internal object NetConst {

//    /***    base address auth microservices              ***/
//    const val AUTH        = "${API}/auth/v1"
//
//    /***    base address referrals microservices         ***/
//    const val REFERRAL    = "${API}/referrals/1/v1/referrals"
//
//    /***    base address statistics microservices        ***/
//    const val STATISTIC   = "${API}/statistics/1/v1/statistics"

    /**************************************************************************************************/

    const val API_VER_CONT = "auth/v1/"
    const val API_VER_MEET = "auth/v1/"
    const val API_VER_REF = "/referrals"
    const val API_VER_CON = "/contacts"
    const val API_VER_MEE = "/v1/sumrameet"




}