/*
 *
 * Copyright 2020 The Meet.org Foundation C.I.C.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *     http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package sdk.net.meet.net.network

import android.util.Log
import com.google.gson.Gson
import kotlinx.coroutines.suspendCancellableCoroutine
import okhttp3.ResponseBody
import retrofit2.HttpException
import retrofit2.Response
import sdk.net.meet.api.failure.Failure
import sdk.net.meet.api.failure.MeetError
import java.io.IOException
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

internal suspend fun okhttp3.Call.awaitResponse(): okhttp3.Response {
    return suspendCancellableCoroutine { continuation ->
        continuation.invokeOnCancellation {
            cancel()
        }

        enqueue(object : okhttp3.Callback {
            override fun onResponse(call: okhttp3.Call, response: okhttp3.Response) {
                continuation.resume(response)
            }

            override fun onFailure(call: okhttp3.Call, e: IOException) {
                continuation.resumeWithException(e)
            }
        })
    }
}

/**
 * Convert a retrofit Response to a Failure, and eventually parse errorBody to convert it to a MeetError
 */
internal fun <T> Response<T>.toFailure(globalErrorReceiver: GlobalErrorReceiver?): Failure {
    return toFailure(errorBody(), code(), globalErrorReceiver)
}

/**
 * Convert a HttpException to a Failure, and eventually parse errorBody to convert it to a MeetError
 */
internal fun HttpException.toFailure(globalErrorReceiver: GlobalErrorReceiver?): Failure {
    return toFailure(response()?.errorBody(), code(), globalErrorReceiver)
}

/**
 * Convert a okhttp3 Response to a Failure, and eventually parse errorBody to convert it to a MeetError
 */
internal fun okhttp3.Response.toFailure(globalErrorReceiver: GlobalErrorReceiver?): Failure {
    return toFailure(body, code, globalErrorReceiver)
}

private fun toFailure(errorBody: ResponseBody?, httpCode: Int, globalErrorReceiver: GlobalErrorReceiver?): Failure {
    if (errorBody == null) {
        return Failure.Unknown(RuntimeException("errorBody should not be null"))
    }

    val errorBodyStr = errorBody.string()

    val meetErrorAdapter = Gson().getAdapter(MeetError::class.java)

    try {
        val meetError = meetErrorAdapter.fromJson(errorBodyStr)

        if (meetError != null) {
//            if (meetError.code == MeetError.M_CONSENT_NOT_GIVEN && !meetError.consentUri.isNullOrBlank()) {
//                // Also send this error to the globalErrorReceiver, for a global management
//                globalErrorReceiver?.handleGlobalError(GlobalError.ConsentNotGivenError(meetError.consentUri))
//            } else if (httpCode == HttpURLConnection.HTTP_UNAUTHORIZED /* 401 */
//                    && meetError.code == MeetError.M_UNKNOWN_TOKEN) {
//                // Also send this error to the globalErrorReceiver, for a global management
//                globalErrorReceiver?.handleGlobalError(GlobalError.InvalidToken(meetError.isSoftLogout))
//            }
            return Failure.OtherServerError(meetError.error!!, httpCode)
        }
    } catch (ex: Exception) {
        Log.w("HTTP EXCEPTION", "The error returned by the server is not a MeetError")
    } catch (ex: RuntimeException) {
        Log.w("HTTP EXCEPTION","The error returned by the server is not a MeetError, probably HTML string")
    }

    return Failure.OtherServerError(errorBodyStr, httpCode)
}
