package sdk.net.meet.net.meet_profile

import retrofit2.http.*
import sdk.net.meet.api.meet_profile.request.FromAddProfile
import sdk.net.meet.api.meet_profile.response.FetchMeetProfile
import sdk.net.meet.api.meet_rooms.request.FromMeetByInvite
import sdk.net.meet.api.meet_rooms.response.FetchInviteRoomBase
import sdk.net.meet.api.meet_rooms.response.respDataInviteRoom
import sdk.net.meet.net.network.NetConst

interface ApiMeetProfile {

    companion object{
        private const val SUCCESS = "success"
        internal fun SUCCESS(value: String) = SUCCESS.equals(value)
    }

    @GET("${NetConst.API_VER_MEE}/profile")
    suspend fun userDataGET(
        @Header("user-id") authHeader: String,
//        @Header("Authorization") authHeader: String,
//        @Query("id") appId: String
    ): FetchMeetProfile

    @POST("${NetConst.API_VER_MEE}/profile")
    suspend fun userDataPOST(
        @Header("user-id") authHeader: String,
//        @Header("Authorization") authHeader: String,
        @Body bodyProfile: FromAddProfile
    ): FetchMeetProfile


    @PUT("${NetConst.API_VER_MEE}/profile/{id}")
    suspend fun userDataPUT(
        @Header("user-id") authHeader: String,
//        @Header("Authorization") authHeader: String,
        @Path("id") profileId: String,
        @Body body: FromAddProfile
    ): FetchMeetProfile


    @POST("${NetConst.API_VER_MEE}/profile/invite")
    suspend fun fetchRoomByInvite(
        @Header("user-id") authHeader: String,
//        @Header("Authorization") authHeader: String,
//        @Path("id") profileId: String,
        @Body body: FromMeetByInvite
    ): respDataInviteRoom.FetchRoomByInvite


//    @POST("${NetConst.API_VER_MEE}/profile/invite")
//    suspend fun fetchRoomByInvitePst(
//        @Header("user-id") authHeader: String,
////        @Header("Authorization") authHeader: String,
////        @Path("id") appId: String,
//        @Body invite: String
//    ): respDataInviteRoom.FetchRoomByInvite //dataOfMeeting.DataResponse

}