/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.net.contacts

import retrofit2.http.*
import sdk.net.meet.api.contacts.request.FromAddContact
import sdk.net.meet.api.contacts.request.FromImportContactByJson
import sdk.net.meet.api.contacts.response.*
import sdk.net.meet.net.network.NetConst

interface ApiContacts {
    companion object{
        const val SUCCESS = "success"
        internal fun SUCCESS(value: String) = SUCCESS.equals(value)
    }
    /*********************************************************
     *    section contacts categories...
     *********************************************************/
    @GET   ("${NetConst.API_VER_CON}/categories")
    suspend fun fetchContactCategories(
        @Header("user-id") authHeader: String,
//        @Header("Authorization") authHeader: String,
                                                        ): FetchContactCategories

    /*********************************************************
     *    section contacts...
     *********************************************************/
    @GET   ("${NetConst.API_VER_CON}")
    suspend fun fetchContacts(
        @Header("user-id") authHeader : String,
//        @Header("Authorization") authHeader : String
        @Query("limit")      limit: Int? = null,
        @Query("page")       page: Int? = null,
        @Query("search")     search: String? = null,
        @Query("isFavorite") isFavorite: Boolean? = null,
        @Query("isRecently") isRecently: Boolean? = null,
        @Query("byLetter")   byLetter: String? = null,
        @Query("groupId")    groupId: String? = null,
        @Query("sort[by]")   by: Array<String>? = null,
        @Query("sort[order]")order: Array<String>? = null,
    ): FetchAllUserContact

    @GET   ("${NetConst.API_VER_CON}/{id}")
    suspend fun fetchContactById(
        @Header("user-id") authHeader : String,
//        @Header("Authorization") authHeader : String
        @Part("id") id: String
    ): ResponseContact

    @GET   ("${NetConst.API_VER_CON}")
    suspend fun contactSendPost(
        @Header("user-id") authHeader : String,
//        @Header("Authorization") authHeader : String
        @Body bodyContent: String
    ): ResponseContact

    @POST  ("${NetConst.API_VER_CON}/import/json")
    suspend fun postContactImportByJson(
        @Header("user-id") authHeader : String,
//        @Header("Authorization") authHeader : String,
        @Body bodyContent: FromImportContactByJson
    ): FetchImportContactByJson

    @POST  ("${NetConst.API_VER_CON}")
    suspend fun postContactCreate(
        @Header("user-id") authHeader : String,
//        @Header("Authorization") authHeader : String,
        @Body bodyContent: FromAddContact
    ): FetchContactCreate
}