/*
 * Copyright 2021 The BGC Global Partners
 */


package sdk.net.meet.net.tasks

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import sdk.net.meet.api.MeetCallback
import sdk.net.meet.api.failure.Cancelable
import sdk.net.meet.api.failure.foldToCallback
import sdk.net.meet.net.toCancelable
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

internal fun <T> CoroutineScope.launchToCallback(
    context: CoroutineContext = EmptyCoroutineContext,
    callback: MeetCallback<T>,
    block: suspend () -> T
): Cancelable = launch(context, CoroutineStart.DEFAULT) {
    val result = runCatching {
        block()
    }
    withContext(Dispatchers.Main) {
        result.foldToCallback(callback)
    }
}.toCancelable()
