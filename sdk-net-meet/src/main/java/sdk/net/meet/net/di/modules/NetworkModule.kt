package sdk.net.meet.net.di.modules

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import sdk.net.meet.api.NetworkServices
import sdk.net.meet.net.di.TimeOutInterceptor
import sdk.net.meet.net.di.scoupe.MeetNetScope
import sdk.net.meet.net.network.FormattedJsonHttpLogger
import sdk.net.meet.net.network.ApiInterceptor
import sdk.net.meet.net.network.DefaultNetworkService
import java.util.concurrent.TimeUnit
import javax.inject.Inject


@Module
internal object NetworkModule { // @Inject constructor (private val networkServices: NetworkServices) {

    @JvmStatic
    @Provides
    fun providesNetworkServices(networkServices: DefaultNetworkService): NetworkServices {
        return networkServices
    }

    @JvmStatic
    @Provides
    fun providesHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val logger = FormattedJsonHttpLogger()
        val interceptor = HttpLoggingInterceptor(logger)
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

    @JvmStatic
    @MeetNetScope
    @Provides
    fun providesOkHttpClient(timeoutInterceptor     : TimeOutInterceptor,
                             httpLoggingInterceptor : HttpLoggingInterceptor,
                             apiInterceptor         : ApiInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout   (60, TimeUnit.SECONDS)
            .writeTimeout  (60, TimeUnit.SECONDS)
            .addInterceptor(timeoutInterceptor)
            .addInterceptor(httpLoggingInterceptor)
            .addInterceptor(apiInterceptor)
            .build()
    }

    @JvmStatic
    @Provides
    fun providesGson(): Gson {
        return Gson()
    }

}