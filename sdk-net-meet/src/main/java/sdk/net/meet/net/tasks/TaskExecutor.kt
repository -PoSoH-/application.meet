/*
 * Copyright 2021 The BGC Global Partners
 */


package sdk.net.meet.net.tasks

import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import sdk.net.meet.LogUtil
import sdk.net.meet.api.failure.Cancelable
import sdk.net.meet.api.failure.foldToCallback
import sdk.net.meet.net.MeetCoroutineDispatchers
import sdk.net.meet.net.di.scoupe.MeetNetScope
import sdk.net.meet.net.toCancelable
import javax.inject.Inject
import kotlin.coroutines.EmptyCoroutineContext

@MeetNetScope
internal class TaskExecutor @Inject constructor(private val coroutineDispatchers: MeetCoroutineDispatchers) {

    val executorScope = CoroutineScope(SupervisorJob())

    fun <PARAMS, RESULT> execute(task: ConfigurableTask<PARAMS, RESULT>): Cancelable {
        return executorScope
                .launch(task.callbackThread.toDispatcher()) {
                    val resultOrFailure = runCatching {
                        withContext(task.executionThread.toDispatcher()) {
                            LogUtil.error("LOG TASK EXECUTOR","Enqueue task $task")
                            LogUtil.error("LOG TASK EXECUTOR","Execute task $task on ${Thread.currentThread().name}")
                            task.execute(task.params)
                        }
                    }
                    resultOrFailure
                            .onFailure {
                                LogUtil.error("LOG TASK EXECUTOR", "${it}, Task failed")
                            }
                            .foldToCallback(task.callback)
                }
                .toCancelable()
    }

    fun cancelAll() = executorScope.coroutineContext.cancelChildren()

    private fun TaskThread.toDispatcher() = when (this) {
        TaskThread.MAIN        -> coroutineDispatchers.main
        TaskThread.COMPUTATION -> coroutineDispatchers.computation
        TaskThread.IO          -> coroutineDispatchers.io
        TaskThread.CALLER      -> EmptyCoroutineContext
//        TaskThread.CRYPTO      -> coroutineDispatchers.crypto
        TaskThread.DM_VERIF    -> coroutineDispatchers.dmVerif
    }
}
