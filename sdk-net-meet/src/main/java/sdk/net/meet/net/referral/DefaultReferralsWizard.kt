/*
 * Copyright 2021 The BGC Global Partners
 */

package sdk.net.meet.net.referral

import com.google.gson.Gson
import sdk.net.meet.LogUtil
import sdk.net.meet.api.referral.*
import sdk.net.meet.api.referral.responses.*
import sdk.net.meet.api.referral.tasks.*
import sdk.net.meet.api.referral.tasks.ReferralCodeGetTask
import sdk.net.meet.api.referral.tasks.ReferralCodePostTask
import sdk.net.meet.api.referral.tasks.ReferralCodePutTask
import sdk.net.meet.api.referral.tasks.TemplateFetchTask
import sdk.net.meet.api.referral.tasks.TemplatePostTask

internal class DefaultReferralsWizard (
    apiReferral: ApiReferral
): ReferralWizard {

    private val templateFetchTask = DefaultReferralTemplateFetchTask(apiReferral)
    private val templatePostTask  = DefaultReferralTemplatePostTask(apiReferral)

    private val refCodeInfoPostTask  = DefaultReferralTemplatePostTask.DefaultRefCodeInfoPostTask(apiReferral)

    private val referralCodeGetTask  = DefaultReferralCodeGetTask(apiReferral)
    private val referralCodeGetByIdTask  = DefaultReferralCodeGetByIdTask(apiReferral)
    private val referralCodePostTask = DefaultReferralCodePostTask(apiReferral)
    private val referralCodePutTask  = DefaultReferralCodePutTask(apiReferral)
    private val referralCodeDelTask  = DefaultReferralCodeDelTask(apiReferral)

    override suspend fun sendReferralsInformation(
        authHeader: String,
        body: String
    ): _POST_RefCodeInfoSend {
        val d =  refCodeInfoPostTask.execute(params = RefCodeInfoLinkPostTask.Params(
            authParam = authHeader,
            body = body
        ))
        return d
    }

    override suspend fun fetchAdminTemplate(authHeader: String, limit: Int, page: Int?): ResultGetTemplate {
        return templateFetchTask.execute(TemplateFetchTask.Params(authParam = authHeader, limit = limit, page = page))
    }

    override suspend fun postAdminTemplate(authHeader: String,
                                           id: Int,
                                           html: String,
                                           title: String): ResultPostTemplate {
        return templatePostTask.execute(TemplatePostTask.Params(
            authParam = authHeader,
            body = Gson().toJson(TemplateParams(id = id, html = html, title = title)))
        )
    }

    override suspend fun fetchReferralCodes(authHeader: String, appID: String): _GetReferralCodes {
        val result = referralCodeGetTask.execute(ReferralCodeGetTask.Params(authHeader, appID))
        if (result.status.equals(ApiReferral.SUCCESS)) {
            return _GetReferralCodes.Success(result)
        } else {
            return _GetReferralCodes.Failure(ReferralsFailureObject(result as ReferralsFailureObject.ErrorReferralObject))
        }
    }

    override suspend fun postReferralCodes(authHeader: String,
                                           body: objModelReferralCode.ReferralCodeParamsPst): _POST_ReferralUpdate {
        val resultat = referralCodePostTask.execute(ReferralCodePostTask.Params(authParam = authHeader, body = body))
        LogUtil.info("TAG", resultat.message)
        if (ApiReferral.SUCCESS(resultat.status)){
            val d = _POST_ReferralUpdate.Success(resultat)
            return d
        }else{
            val f = _POST_ReferralUpdate.Failure(ReferralsFailureObject(resultat as ReferralsFailureObject.ErrorReferralObject))
            return f
        }
    }

    override suspend fun updateReferralCodeById(authHeader: String,
                                                id: String,
                                                body: objModelReferralCode.ReferralCodeParamsPut): _PUT_ReferralUpdate {
        val result = referralCodePutTask.execute(ReferralCodePutTask.Params(
            authParam = authHeader,
            referralID = id,
            body = body
        ))
        when(ApiReferral.SUCCESS(result.status)){
            true -> { return _PUT_ReferralUpdate.Success(result) }
            else -> { return _PUT_ReferralUpdate
                .Failure(ReferralsFailureObject(result as ReferralsFailureObject.ErrorReferralObject)) }
        }
    }

    override suspend fun deleteReferralCodeById(authHeader: String, id: String): _DEL_ReferralUpdate {
        val result = referralCodeDelTask.execute(ReferralCodeDeleteTask.Params(
            authParam = authHeader,
            referralID = id
        ))
        when(ApiReferral.SUCCESS(result.content.status)){
            true -> { return _DEL_ReferralUpdate.Success(result) }
            else -> { return _DEL_ReferralUpdate
                .Failure(ReferralsFailureObject(result as ReferralsFailureObject.ErrorReferralObject)) }
        }
    }

    override suspend fun fetchReferralCodeById(authHeader: String, id: String): _GET_ReferralCodeById {
        val result = referralCodeGetByIdTask.execute(ReferralCodeGetByIdTask.Params(
            authParam = authHeader,
            referralID = id
        ))
        when(ApiReferral.SUCCESS(result.status)){
            true -> { return _GET_ReferralCodeById.Success(result) }
            else -> { return _GET_ReferralCodeById
                .Failure(ReferralsFailureObject(result as ReferralsFailureObject.ErrorReferralObject)) }
        }
    }

    override suspend fun fetchReferralCodeNew(authHeader: String, id: String): _GET_ReferralCodeById {
        TODO("Not yet implemented")
    }

}