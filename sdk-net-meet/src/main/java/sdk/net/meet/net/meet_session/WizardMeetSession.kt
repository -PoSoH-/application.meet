package sdk.net.meet.net.meet_session

import sdk.net.meet.api.meet_session.IWizardMeetSession
import sdk.net.meet.api.meet_session.TaskRespMeetSession
import sdk.net.meet.api.meet_session.request.FromMeetSession
import sdk.net.meet.api.meet_session.tasks.ITaskMeetSessionCreate
import sdk.net.meet.api.meet_session.tasks.ITaskMeetSessionUpdate

class WizardMeetSession(apiMeetSession: ApiMeetSession): IWizardMeetSession {

    private val taskCreate = TasksMeetSessions.TaskMeetSessionCreate(apiMeetSession)
    private val taskUpdate = TasksMeetSessions.TaskMeetSessionUpdate(apiMeetSession)

    override suspend fun roomSessionPst(
        authHeader: String,
        appId: String,
        body: FromMeetSession
    ): TaskRespMeetSession {
        return taskCreate.execute(ITaskMeetSessionCreate
            .Params(auth = authHeader, app = appId, body = body))
    }

    override suspend fun roomSessionPut(
        authHeader: String,
        appId: String,
        sessionId: String,
        body: FromMeetSession
    ): TaskRespMeetSession {
        return taskUpdate.execute(ITaskMeetSessionUpdate
            .Params(auth = authHeader, app = appId, roomId = sessionId, body = body))
    }

}