package sdk.net.meet.net.meet_calendar

import retrofit2.http.*
import sdk.net.meet.api.meet_calendar.request.FromCreateCalendar
import sdk.net.meet.api.meet_calendar.response.FetchMeetCalendar
import sdk.net.meet.api.meet_calendar.response.FetchMeetCalendarAll
import sdk.net.meet.api.meet_calendar.response.FetchMeetCalendarUpdate
import sdk.net.meet.net.network.NetConst

interface ApiMeetCalendar {
    companion object{
        private const val SUCCESS = "success"
        internal fun SUCCESS(value: String) = SUCCESS.equals(value)
    }

    @GET("${NetConst.API_VER_MEE}/calendars")
    suspend fun meetCalendarGET(
        @Header("user-id") authHeader: String,
//        @Header("Authorization") authHeader: String,
//        @Header("application_id") appId: String,
    ): FetchMeetCalendarAll

    @POST("${NetConst.API_VER_MEE}/calendars")
    suspend fun meetCalendarPST(
        @Header("user-id") authHeader: String,
//        @Header("Authorization") authHeader: String,
//        @Header("application_id") appId: String,
        @Body bodyProfile: FromCreateCalendar
    ): FetchMeetCalendar

    @GET("${NetConst.API_VER_MEE}/calendars/{id}")
    suspend fun meetCalendarByIdGET(
        @Header("user-id") authHeader: String,
//        @Header("Authorization") authHeader: String,
//        @Header("application_id") appId: String,
        @Path("id") calendarId: String
    ): FetchMeetCalendar

    @PUT("${NetConst.API_VER_MEE}/calendars/{id}")
    suspend fun meetCalendarPUT(
        @Header("user-id") authHeader: String,
//        @Header("Authorization") authHeader: String,
//        @Header("application_id") appId: String,
        @Path("id") calendarId: String,
        @Body body: FromCreateCalendar
    ): FetchMeetCalendarUpdate

    @DELETE("${NetConst.API_VER_MEE}/calendars/{id}")
    suspend fun meetCalendarDEL(
        @Header("user-id") authHeader: String,
//        @Header("Authorization") authHeader: String,
//        @Header("application_id") appId: String,
        @Path("id") calendarId: String,
    ): FetchMeetCalendar
}