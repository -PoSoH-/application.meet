package sdk.net.meet.net.meet_session

import retrofit2.http.*
import sdk.net.meet.api.meet_calendar.response.FetchMeetCalendar
import sdk.net.meet.api.meet_calendar.response.FetchMeetCalendarUpdate
import sdk.net.meet.api.meet_session.request.FromMeetSession
import sdk.net.meet.api.meet_session.response.FetchMeetSessionCrt
import sdk.net.meet.api.meet_session.response.FetchMeetSessionUpd
import sdk.net.meet.net.network.NetConst

interface ApiMeetSession {

    @POST("${NetConst.API_VER_MEE}/sessions")
    suspend fun meetSessionPST(
        @Header("user-id") authHeader: String,
//        @Header("Authorization") authHeader: String,
//        @Header("application_id") appId: String,
        @Body body: FromMeetSession
    ): FetchMeetSessionCrt

    @PUT("${NetConst.API_VER_MEE}/sessions/{id}")
    suspend fun meetSessionPUT(
        @Header("user-id") authHeader: String,
//        @Header("Authorization") authHeader: String,
//        @Header("application_id") appId: String,
        @Path("id") roomId: String,
        @Body body: FromMeetSession
    ): FetchMeetSessionUpd
}