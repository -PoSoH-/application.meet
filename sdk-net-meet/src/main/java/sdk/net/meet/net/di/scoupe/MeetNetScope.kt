package sdk.net.meet.net.di.scoupe

import javax.inject.Scope

@Scope
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
internal annotation class MeetNetScope