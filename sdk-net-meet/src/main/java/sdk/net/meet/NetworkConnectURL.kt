package sdk.net.meet

import sdk.net.meet.api.contacts.response.FetchContact

data class NetworkConnectURL (
      val referralURL: String = "http://ec2-52-43-1-93.us-west-2.compute.amazonaws.com:8091"        // "http://ec2-34-208-108-203.us-west-2.compute.amazonaws.com:8091"
    , val contactsURL: String = "http://ec2-54-186-224-210.us-west-2.compute.amazonaws.com:8107"    // "http://ec2-54-212-236-7.us-west-2.compute.amazonaws.com:8107"

    , val meetServiceURL: String = "http://ec2-34-217-12-19.us-west-2.compute.amazonaws.com:8111"   // "http://ec2-54-244-207-224.us-west-2.compute.amazonaws.com:8111"
    , val meetMemberShipURL: String = "http://ec2-35-85-58-137.us-west-2.compute.amazonaws.com:8106" //
)

//    , val meetContactURL: String = "http://ec2-54-244-207-224.us-west-2.compute.amazonaws.com:8111"
//    , val meetRoomsURL  : String = "http://ec2-54-244-207-224.us-west-2.compute.amazonaws.com:8111"
//    , val meetSessionURL: String = "http://ec2-54-244-207-224.us-west-2.compute.amazonaws.com:8111"

//  http://ec2-52-43-1-93.us-west-2.compute.amazonaws.com:8091/api/documentation -> Новая ссылка на рефералы
//  http://ec2-54-186-224-210.us-west-2.compute.amazonaws.com:8107/api/documentation ->  Contacts
//  00000000-2000-2000-2000-000000000000  -> user-id

