//package net.sumra.auth.di
//
//import android.content.Context
//import android.content.res.Resources
//import dagger.BindsInstance
//import dagger.Component
//import net.sumra.auth.AuthWrapperApp
//import net.sumra.auth.data.remote.ConnectService
//import javax.inject.Singleton
//
//@Component(modules = [
//    WrapperModule::class,
//    NetworkModule::class,
//    AuthModule::class
//    ])
//@Singleton
//interface WrapperComponent {
//
//    fun inject(authWrapperApplication: AuthWrapperApp)
//
//    fun appContext(): Context
//
//    fun resources(): Resources
//
//    fun injectConnect() : ConnectService
//
//    @Component.Factory
//    interface Factory {
//        fun create(@BindsInstance context: Context): WrapperComponent
//    }
//
//}