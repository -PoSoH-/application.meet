package net.sumra.auth.di

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import dagger.Binds
import dagger.Module
import dagger.Provides
import net.sumra.auth.data.remote.ConnectService
import net.sumra.auth.data.remote.DefaultConnectService

@Module
class WrapperModule {
    @Module
    companion object {

        @Provides
        @JvmStatic
        fun providesResources(context: Context): Resources {
            return context.resources
        }

        @Provides
        @JvmStatic
        fun providesSharedPreferences(context: Context): SharedPreferences {
            return context.getSharedPreferences("application.sumra.meet", Context.MODE_PRIVATE)
        }

//        @Provides
//        @JvmStatic
//        fun providesCurrentSession(activeSessionHolder: ActiveSessionHolder): Session {
//            // TODO: handle session injection better
//            return activeSessionHolder.getActiveSession()
//        }

        @Provides
        @JvmStatic
        fun bindAuthenticationService(service: DefaultConnectService): ConnectService {
            return service
        }

    }
}