package net.sumra.auth.di

import dagger.Binds
import dagger.Module
import net.sumra.auth.auth.ImplPendingSessionStore
import net.sumra.auth.auth.PendingSessionStore

@Module
internal abstract class AuthModule {

//    @Binds
//    abstract fun bindAuthenticationService(service: DefaultConnectService): DefaultConnectService

    @Binds
    abstract fun bindPendingSessionStore(store: ImplPendingSessionStore): PendingSessionStore

}