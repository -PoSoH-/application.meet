package net.sumra.auth.states

sealed class MessageTypeEvents{
    data class MessageSuccess(val message: String): MessageTypeEvents()
    data class MessageSimple (val message: String): MessageTypeEvents()
    data class MessageFailure(val message: String): MessageTypeEvents()
}