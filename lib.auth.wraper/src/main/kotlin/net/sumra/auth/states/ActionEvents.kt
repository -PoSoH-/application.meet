package net.sumra.auth.states

import net.sumra.auth.ControllerWrapper

sealed class ActionEvents{
    object ActionEventsSuccess: ActionEvents()
    object ActionEventsFailure: ActionEvents()

    data class ActionEventsContacts(val adapter: ControllerWrapper.AdapterContact): ActionEvents()
}