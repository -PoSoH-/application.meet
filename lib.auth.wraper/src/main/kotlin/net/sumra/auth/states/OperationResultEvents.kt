package net.sumra.auth.states

import android.text.Editable
import androidx.fragment.app.Fragment
import chat.sumra.app.features.login.sumra.messengers.Messengers
import net.sumra.auth.models.MappedContact

sealed class OperationResultEvents{
    data class EventBackStack(val view: Fragment)  : OperationResultEvents()
//    data class EventPopBackStack(val id: Identity): OperationResultEvents()
    object EventPopBackStack             : OperationResultEvents()
    object EventCongrats                 : OperationResultEvents()

    object EventThermsSuccess            : OperationResultEvents()

    data class EventSelectedMessengers(val type: Messengers): OperationResultEvents()
    data class EventSelectedPhone(val phoneCode: String, val phoneNumber: String): OperationResultEvents()
    data class EventConfirmPhone(val phoneNumber: String): OperationResultEvents()
    object EventSelectedSumraId          : OperationResultEvents()

    data class EventEnterReceiverCode(val code: String) : OperationResultEvents()

    data class EventCheckUsername(val candidate: String): OperationResultEvents()
    data class EventAuthUp(val candidateName: String, val authCode: String, val sidCode: String): OperationResultEvents()

    object EventSendReferralContacts     : OperationResultEvents()
    object EventReferralsSendSuccess     : OperationResultEvents()

    data class EventSubmitContacts(val contacts: List<MappedContact>): OperationResultEvents()

    data class EventSumraIdUsernameCheck(val username: Editable): OperationResultEvents()
    data class EventSumraIdLogin(val username: Editable, val password: Editable): OperationResultEvents()

    data class EventPushSuccess(val message: String): OperationResultEvents()
    data class EventPushFailure(val message: String): OperationResultEvents()
    data class EventPushSimple (val message: String): OperationResultEvents()

}