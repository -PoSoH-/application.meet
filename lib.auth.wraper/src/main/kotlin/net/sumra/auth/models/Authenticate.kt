package net.sumra.auth.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Authenticate(val password: String, val username: String): Parcelable
