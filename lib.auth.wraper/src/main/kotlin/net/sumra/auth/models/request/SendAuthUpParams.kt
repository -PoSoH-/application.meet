package net.sumra.auth.models.request

import com.google.gson.annotations.SerializedName

data class SendAuthUpParams(
    @SerializedName("username")
    val candidateName: String,
    @SerializedName("code")
    val candidateCode: String)
