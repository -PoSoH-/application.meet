package net.sumra.auth.models

import com.google.gson.annotations.SerializedName

data class AuthParameter(
    @SerializedName("username")
    val authUsername: String,
    @SerializedName("password")
    val authPassword: String
)