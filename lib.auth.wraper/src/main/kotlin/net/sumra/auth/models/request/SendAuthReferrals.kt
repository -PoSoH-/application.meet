package net.sumra.auth.models.request

import com.google.gson.annotations.SerializedName
import net.sumra.auth.models.MappedContact

data class SendAuthReferrals(
    @SerializedName("referrals")
    val referralsProgram: List<MappedContact>)
