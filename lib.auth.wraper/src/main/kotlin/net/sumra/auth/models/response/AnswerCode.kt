package net.sumra.auth.models.response

data class AnswerCode(
    val code   : Int,
    val error  : String?,
    val content: String?)
