package net.sumra.auth.models.response

data class AnswerNet(
    val code   : Int,
    val error  : String? = null,
    val content: String? = null
)
