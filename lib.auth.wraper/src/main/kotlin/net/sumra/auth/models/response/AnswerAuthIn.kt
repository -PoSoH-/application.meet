package net.sumra.auth.models.response

import com.google.gson.annotations.SerializedName

data class AnswerAuthIn(
    /*
    тут мають бути відповіді від сервера:
        - з робочим токеном;
        - токеном відновлення;
        - часом дії;
    можливо ще щось??? (При адресації для сумра чату окремий
                        об'єкт для відповіді матрікс серверу...)
    */
    @SerializedName("token")
    val authToken: String
)
