package net.sumra.auth.models.request

import com.google.gson.annotations.SerializedName

data class SendAuthInParams(
    @SerializedName("username") // username will be send of md5 format or other hash
    val candidateName: String,
    @SerializedName("password") // password will be send of md5 format or other hash
    val candidateCode: String? = null)
