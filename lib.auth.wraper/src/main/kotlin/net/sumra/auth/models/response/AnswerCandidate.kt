package net.sumra.auth.models.response

data class AnswerCandidate(
    val code   : Int,
    val error  : String?,
    val content: String?)
