package net.sumra.auth.models

import android.net.Uri

class MappedContactBuilder(
        val id: Long,
        val displayName: String
) {
    var photoURI: Uri? = null
    val msisdns = mutableListOf<String>()
    val emails = mutableListOf<String>()

    fun build(): MappedContact {
        return MappedContact(
                id = id,
                displayName = displayName,
                photoURI = photoURI,
                msisdns = msisdns,
                emails = emails
        )
    }
}

data class MappedContact(
        val id: Long,
        val displayName: String,
        val photoURI: Uri? = null,
        val msisdns: List<String> = emptyList(),
        val emails: List<String> = emptyList()
)

//data class MappedEmail(
//        val email: String,
//        val matrixId: String?
//)
//
//data class MappedMsisdn(
//        val phoneNumber: String,
//        val matrixId: String?
//)