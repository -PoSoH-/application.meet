package net.sumra.auth.models.response

import com.google.gson.annotations.SerializedName

data class AnswerAuthUp(
    @SerializedName("username") // username will be send of md5 format or other hash
    val candidateName: String,
    @SerializedName("password") // password will be send of md5 format or other hash
    val candidateCode: String)
