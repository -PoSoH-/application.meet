package net.sumra.auth.models.request

import com.google.gson.annotations.SerializedName

data class SendCandidate(
    @SerializedName("username")
    val username: String,
    @SerializedName("code")
    val code: String? = null)
