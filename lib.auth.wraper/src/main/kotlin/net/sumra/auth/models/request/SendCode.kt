package net.sumra.auth.models.request

import com.google.gson.annotations.SerializedName

data class SendCode(
    @SerializedName("phone_number")
    val fullNumber: String,
    @SerializedName("device_id")
    val deviceId: String? = null)
