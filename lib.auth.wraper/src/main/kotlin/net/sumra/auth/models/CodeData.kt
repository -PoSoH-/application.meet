package net.sumra.auth.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CodeData(val receiverCode: String): Parcelable
