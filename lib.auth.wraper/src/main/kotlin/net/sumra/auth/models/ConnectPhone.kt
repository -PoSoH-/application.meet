package net.sumra.auth.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ConnectPhone(val fullPhone: String) : Parcelable
