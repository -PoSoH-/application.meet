package net.sumra.auth.models

class LibraryError(val data: Int) {
    fun errorParse(): String{
        return when(data) {
            in 300..399 -> "Помилка перенаправлення..."
            in 400..499 -> "Помилка в коді серверу..."
            in 500..599 -> "Помилка в залізі серверу..."
            else -> "Сталася не відома помилка..."
        }
    }
}