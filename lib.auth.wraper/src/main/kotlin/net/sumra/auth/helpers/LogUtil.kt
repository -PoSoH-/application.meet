package net.sumra.auth.helpers

import android.util.Log
import net.sumra.auth.BuildConfig

object LogUtil {
    fun error(txt: String){
        if(BuildConfig.DEBUG){
            Log.e("${BuildConfig.LIBRARY_PACKAGE_NAME}::", txt)
        }
    }
    fun error(tag: String, txt: String){
        if(BuildConfig.DEBUG){
            Log.e("${BuildConfig.LIBRARY_PACKAGE_NAME}:$tag:", txt)
        }
    }
    fun info(txt: String){
        if(BuildConfig.DEBUG){
            Log.i("${BuildConfig.LIBRARY_PACKAGE_NAME}::", txt)
        }
    }
    fun info(tag: String, txt: String){
        if(BuildConfig.DEBUG){
            Log.i("${BuildConfig.LIBRARY_PACKAGE_NAME}:$tag:", txt)
        }
    }
}