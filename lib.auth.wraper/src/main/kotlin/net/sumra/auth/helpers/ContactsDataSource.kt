package net.sumra.auth.helpers

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.ContactsContract
import android.util.Log
import net.sumra.auth.models.MappedContact
import net.sumra.auth.models.MappedContactBuilder
//import net.sumra.auth.models.MappedEmail
//import net.sumra.auth.models.MappedMsisdn
import kotlin.system.measureTimeMillis

class ContactsDataSource (val context: Context) {

    fun getContacts(
            withEmails: Boolean,
            withMsisdn: Boolean
    ): List<MappedContact> {
        val map = mutableMapOf<Long, MappedContactBuilder>()
        val contentResolver = context.contentResolver

        measureTimeMillis {  // special for see time to fetch contacts....
            contentResolver.query(
                    ContactsContract.Contacts.CONTENT_URI,
                    arrayOf(
                            ContactsContract.Contacts._ID,
                            ContactsContract.Data.DISPLAY_NAME,
                            ContactsContract.Data.PHOTO_URI
                    ),
                    null,
                    null,
                    // Sort by Display name
                    ContactsContract.Data.DISPLAY_NAME
            )
                    ?.use { cursor ->
                        if (cursor.count > 0) {
                            while (cursor.moveToNext()) {
                                val id = cursor.getLong(ContactsContract.Contacts._ID) ?: continue
                                val displayName = cursor.getString(ContactsContract.Contacts.DISPLAY_NAME) ?: continue

                                val mappedContactBuilder = MappedContactBuilder(
                                        id = id,
                                        displayName = displayName
                                )

                                cursor.getString(ContactsContract.Data.PHOTO_URI)
                                        ?.let { Uri.parse(it) }
                                        ?.let { mappedContactBuilder.photoURI = it }

                                map[id] = mappedContactBuilder
                            }
                        }
                    }

            // Get the phone numbers
            if (withMsisdn) {
                contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        arrayOf(
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                                ContactsContract.CommonDataKinds.Phone.NUMBER
                        ),
                        null,
                        null,
                        null)
                        ?.use { innerCursor ->
                            while (innerCursor.moveToNext()) {
                                val mappedContactBuilder = innerCursor.getLong(ContactsContract.CommonDataKinds.Phone.CONTACT_ID)
                                        ?.let { map[it] }
                                        ?: continue
                                innerCursor.getString(ContactsContract.CommonDataKinds.Phone.NUMBER)
                                        ?.let {
                                            mappedContactBuilder.msisdns.add(it
//                                                    MappedMsisdn(
//                                                            phoneNumber = it,
//                                                            matrixId = null
//                                                    )
                                            )
                                        }
                            }
                        }
            }

            // Get Emails
            if (withEmails) {
                contentResolver.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                        arrayOf(
                                ContactsContract.CommonDataKinds.Email.CONTACT_ID,
                                ContactsContract.CommonDataKinds.Email.DATA
                        ),
                        null,
                        null,
                        null)
                        ?.use { innerCursor ->
                            while (innerCursor.moveToNext()) {
                                // This would allow you get several email addresses
                                // if the email addresses were stored in an array
                                val mappedContactBuilder = innerCursor.getLong(ContactsContract.CommonDataKinds.Email.CONTACT_ID)
                                        ?.let { map[it] }
                                        ?: continue
                                innerCursor.getString(ContactsContract.CommonDataKinds.Email.DATA)
                                        ?.let {
                                            mappedContactBuilder.emails.add( it
//                                                    MappedEmail(
//                                                            email = it,
//                                                            matrixId = null
//                                                    )
                                            )
                                        }
                            }
                        }
            }
        }.also { Log.d("contacts time ->","took ${it}ms to fetch ${map.size} contact(s)") }

        return map
                .values
                .filter { it.emails.isNotEmpty() || it.msisdns.isNotEmpty() }
                .map { it.build() }
    }

    private fun Cursor.getString(column: String): String? {
        return getColumnIndex(column)
                .takeIf { it != -1 }
                ?.let { getString(it) }
    }

    private fun Cursor.getLong(column: String): Long? {
        return getColumnIndex(column)
                .takeIf { it != -1 }
                ?.let { getLong(it) }
    }
}