package net.sumra.auth.helpers

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.fragment.app.Fragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.sumra.auth.BuildConfig
import timber.log.Timber
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

fun extWriteThermsContactsState(permission: Boolean, preferences: SharedPreferences) {
    CoroutineScope(Dispatchers.IO).launch {
        preferences.edit().putBoolean(Finals.CONTACT, permission).commit()
    }
}
fun extWriteThermsUpdatesState(permission: Boolean, preferences: SharedPreferences) {
    CoroutineScope(Dispatchers.IO).launch {
        preferences.edit().putBoolean(Finals.UPDATES, permission).apply()
    }
}

fun extReadThermsContactsState(preferences: SharedPreferences) = preferences.getBoolean(Finals.CONTACT, false)
fun extReadThermsUpdatesState(preferences: SharedPreferences)  = preferences.getBoolean(Finals.UPDATES, false)

fun Fragment.logI(log: String) {
    if(BuildConfig.DEBUG){
        Timber.i("library one-step auth - type info :: $log")
    }
}

fun Fragment.logE(log: String) {
    if(BuildConfig.DEBUG){
        Timber.i("library one-step auth - type error :: $log")
    }
}

fun View.hideKeyboard(){
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

fun EditText.enterSymbol(c: String){
    text.clear()
    text = Editable.Factory().newEditable(c)
}

object libBundle {

    private const val KEY_ARG = "arg32Ldf-0gdt-GjkF-aJfs-ksn6wBpSdp3"

    fun <V : Any> libraryArgs() = object : ReadOnlyProperty<Fragment, V> {
        var value: V? = null

        override fun getValue(thisRef: Fragment, property: KProperty<*>): V {
            if (value == null) {
                val args = thisRef.arguments
                    ?: throw IllegalArgumentException("There are no fragment arguments!")
                val argUntyped = args.get(KEY_ARG)
                argUntyped
                    ?: throw IllegalArgumentException("Sumra arguments not found at key ExtAllArgs.KEY_ARG!")
                @Suppress("UNCHECKED_CAST")
                value = argUntyped as V
            }
            return value ?: throw IllegalArgumentException("")
        }
    }

    fun Parcelable?.toLibraryBundle(): Bundle? {
        return this?.let {
            Bundle().apply { putParcelable(KEY_ARG, it) }
        }
    }
}
