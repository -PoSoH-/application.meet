package net.sumra.auth.helpers

object Finals {
    // base const
    const val min_username_symbols = 8

    // for preferences keys
    const val KEY_REFERRER_PREF = "a700abcb-8cae-412d-9dfb-0aa985ef160a"

    const val CONTACT = "fa4d2b6c-0a45-46aa-896b-bad3aee8941a"
    const val UPDATES = "ab1e98c6-1875-493d-a46f-736c9f0155eb"

    // for bundle keys
//    val FULL_PHONE = "JSaOsu-PSncgd34JSfL-HHD20-PkdNN77Sg"
    const val FULL_CODE  = "899ba328-386d-4f94-b121-d2ecda18a269"

    const val FONT_FACE  = "fonts/dm_sans_medium.ttf"
}