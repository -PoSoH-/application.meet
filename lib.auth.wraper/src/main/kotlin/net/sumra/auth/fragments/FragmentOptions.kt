package net.sumra.auth.fragments

import android.content.Intent
import android.content.res.AssetManager
import android.content.res.ColorStateList
import android.graphics.Typeface
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.StateListDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.font.*
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import chat.sumra.app.features.login.sumra.messengers.ControllerMessengers
import chat.sumra.app.features.login.sumra.messengers.IButtonPressListener
import chat.sumra.app.features.login.sumra.messengers.Messengers
import net.sumra.auth.FragmentListener
import net.sumra.auth.states.OperationResultEvents
import net.sumra.auth.R
import net.sumra.auth.databinding.FragmentOptionsBinding
import net.sumra.auth.helpers.hideKeyboard
import timber.log.Timber
import kotlin.properties.Delegates

class FragmentOptions(): Fragment() {

    companion object{
        fun getInstance(
            resId: Int,
            frgListener: FragmentListener,
            lightColor: Int,
            darkColor : Int,
            textColPrimDark : Int,
            textColNormDark : Int,
            textColPrimary  : Int,
            textColLight    : Int,
            textColSelect   : Int,
            fontAssetsPath  : String?,
            backgroundColor : Int
            ): Fragment{
            return Builder().apply {
                this.resId = resId
                this.listener = frgListener

                _lightColor = lightColor
                _darkColor  = darkColor

                _textColPrimDark  = textColPrimDark
                _textColNormDark  = textColNormDark
                _textColPrimary   = textColPrimary
                _textColLight     = textColLight
                _textColSelect    = textColSelect
                _fontAssetsPath   = fontAssetsPath
                _backgroundColor  = backgroundColor
            }.build()
        }
    }

    private class Builder{
        var resId: Int? = null
        var listener: FragmentListener? = null
        var _lightColor by Delegates.notNull<Int>()
        var _darkColor  by Delegates.notNull<Int>()
        var _textColPrimDark by Delegates.notNull<Int>()
        var _textColNormDark by Delegates.notNull<Int>()
        var _textColPrimary  by Delegates.notNull<Int>()
        var _textColLight    by Delegates.notNull<Int>()
        var _textColSelect   by Delegates.notNull<Int>()
        var _backgroundColor by Delegates.notNull<Int>()
        var _fontAssetsPath : String? = null

        fun build(): FragmentOptions {
            val fragment = FragmentOptions()
            fragment.addResourceLogo(resId!!)
            fragment.addListener(listener!!)
            fragment.lightColor = _lightColor
            fragment.darkColor  = _darkColor
            fragment.textColPrimDark = _textColPrimDark
            fragment.textColPrimary  = _textColPrimary
            fragment.textColNormDark = _textColNormDark
            fragment.textColLight    = _textColLight
            fragment.textColSelect   = _textColSelect
            fragment.fontAssetsPath  = _fontAssetsPath
            fragment.backgroundColor = _backgroundColor
            return fragment
        }
    }

    private lateinit var controlerMessangers: ControllerMessengers
    private var type: Messengers = Messengers.UNDEFINED

    private var resId: Int? = null
    private lateinit var frgListener: FragmentListener

    private lateinit var _frgBind: FragmentOptionsBinding

    private var lightColor      by Delegates.notNull<Int>()         // Color.parseColor("#FFCA8F31")
    private var darkColor       by Delegates.notNull<Int>()         // Color.parseColor("#FFFFAF1A")

    private var textColPrimDark by Delegates.notNull<Int>()         // Color.parseColor("#FF4C4E5A")
    private var textColNormDark by Delegates.notNull<Int>()         // Color.parseColor("#FF4C4E5A")
    private var textColPrimary  by Delegates.notNull<Int>()         // Color.parseColor("#FF4C4E5A")
    private var textColLight    by Delegates.notNull<Int>()         // Color.parseColor("#FF4C4E5A")
    private var textColSelect   by Delegates.notNull<Int>()         // Color.parseColor("#FF4C4E5A")

    private var fontAssetsPath : String? = null

    private var backgroundColor : Int = android.graphics.Color.parseColor("#FFFFFFFF")

    private val buttonClick: IButtonPressListener = object: IButtonPressListener {
        override fun pressed(mesOk: Boolean, type: Messengers, intent: Intent) {
            if(mesOk) {
                Timber.i("select messenger :: Start ${type.name} messengers for receive code...")
                frgListener.resultActionListener(OperationResultEvents.EventSelectedMessengers(type))
                startActivity(intent)
            }else{
                startActivity(intent)
            }
        }
    }

    @ExperimentalComposeUiApi
    @ExperimentalTextApi
    @ExperimentalMaterialApi
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _frgBind = FragmentOptionsBinding.inflate(layoutInflater)
        setupUI()
        addListeners()
        setupCustomFont()
        return _frgBind.root
    }

    @ExperimentalComposeUiApi
    @ExperimentalTextApi
    @ExperimentalMaterialApi
    private fun setupUI(){
//        _frgBind.clickSelectSignUpByTelegramm.showImageRound(R.drawable.ic_app_icon_telegram)
//        _frgBind.clickSelectSignUpByMessenger.showImageRound(R.drawable.ic_app_icon_messenger)
//        _frgBind.clickSelectSignUpByWhatsapp.showImageRound(R.drawable.ic_app_icon_whatsapp)
//        _frgBind.clickSelectSignUpByViber.showImageRound(R.drawable.ic_app_icon_viber)
//        _frgBind.clickSelectSignUpBySignal.showImageRound(R.drawable.ic_app_icon_signal)

        // setup background color...   */
        _frgBind.viewScrollBased.setBackgroundColor(backgroundColor)

        // setup messengers group...   */
        controlerMessangers = ControllerMessengers(requireContext())
        _frgBind.processSignBlockButtons.removeAllViews()
        controlerMessangers.updateButtonView(_frgBind.processSignBlockButtons)
        controlerMessangers.updateButtonListener(buttonClick)

        // setup application logo...   */
        resId?.run {
            _frgBind.optionsLogo.setImageResource(this)
        }

        // text color base label
        _frgBind.stepNameOptions.text = HtmlCompat.fromHtml("""<span><p><b>
            |<font color = ${String.format("#%06X", (0xffffff and textColPrimDark))}>
            |${resources.getString(R.string.title_label_options)}
            |</font></b></p></span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)

        // text color using messengers
        _frgBind.labelSelectTheMessenger.text = HtmlCompat.fromHtml(
            """<span><p><font color = ${String.format("#%06X", (0xffffff and textColNormDark))}>
                |${resources.getString(R.string.step_label_sign_with_a)}</font>
                |<b><font color = ${String.format("#%06X", (0xffffff and textColPrimDark))}>
                | ${resources.getString(R.string.step_label_sign_with_b)}
                |</font></b></p></span>""".trimMargin(),
            HtmlCompat.FROM_HTML_MODE_COMPACT
        )

        // text color separate string (2 positions)
        _frgBind.separatorFirst.text = HtmlCompat.fromHtml(
            """<span><b><font color = ${String.format("#%06X", (0xffffff and textColNormDark))}>
                |${resources.getString(R.string.step_label_separate_text)}
                |</font></b></span>""".trimMargin(),
            HtmlCompat.FROM_HTML_MODE_COMPACT
        )
        _frgBind.separatorSecond.text = HtmlCompat.fromHtml(
            """<span><b><font color = ${String.format("#%06X", (0xffffff and textColNormDark))}>
                |${resources.getString(R.string.step_label_separate_text)}
                |</font></b></span>""".trimMargin(),
            HtmlCompat.FROM_HTML_MODE_COMPACT
        )

        // text color using mobile phone
        _frgBind.labelSelectThePhone.text = HtmlCompat.fromHtml(
            """<span><p><font color = ${String.format("#%06X", (0xffffff and textColNormDark))}>
                |${resources.getString(R.string.step_label_sign_with_a)}</font>
                |<b><font color = ${String.format("#%06X", (0xffffff and textColPrimDark))}>
                | ${resources.getString(R.string.step_label_sign_with_c)}
                |</font></b></p></span>""".trimMargin(),
            HtmlCompat.FROM_HTML_MODE_COMPACT
        )

        _frgBind.optionsPhoneNumberEnter.hint = HtmlCompat.fromHtml(
            """<span><p><font color = ${String.format("#%06X", (0xffffff and textColNormDark))}>
                | ${resources.getString(R.string.step_options_hint_mobile_phone)}
                |</font></p></span>""".trimMargin(),
            HtmlCompat.FROM_HTML_MODE_COMPACT
        )
        _frgBind.optionsPhoneNumberEnter.setTextColor(android.graphics.Color.parseColor("#2C2948"))

        _frgBind.optionsPhoneCodeSelected.setTextColorPicker(android.graphics.Color.parseColor("#2C2948"))
        _frgBind.clickButtonSignUpPhone.run {
            imageTintList = ColorStateList(
                arrayOf(intArrayOf(android.R.attr.state_enabled, -android.R.attr.state_pressed), intArrayOf(android.R.attr.state_enabled, android.R.attr.state_pressed)),
                intArrayOf(darkColor, lightColor))
        }

        // text color label name the button
        _frgBind.optionsNextStepTheButton.text = HtmlCompat.fromHtml(
            """<span><b><font color = ${String.format("#%06X", (0xffffff and android.graphics.Color.parseColor("#ffffff")))}>
                |${resources.getString(R.string.options_button_confirmation)}
                |</font></b></span>""".trimMargin(),
            HtmlCompat.FROM_HTML_MODE_COMPACT
        )

        // text color bottom text private policy...
        _frgBind.optionsBottomHelp.text = HtmlCompat
            .fromHtml("""<span><p><b>
            |<font color=${String.format("#%06X", (0xffffff and textColLight))}>
            |${resources.getString(R.string.therms_private_policy_all)}</font></b></p>
            |<p><b><font color=${String.format("#%06X", (0xffffff and textColSelect))}>
            |${resources.getString(R.string.therms_private_policy)}</font></b></p></span>""".trimMargin()
                .trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)

        // registered field for entered phone number.....
        _frgBind.optionsPhoneCodeSelected.registerCarrierNumberEditText(_frgBind.optionsPhoneNumberEnter)

        _frgBind.optionsPhoneNumberOperationContainer.run{
            val gradientDisabledColors = intArrayOf(android.graphics.Color.parseColor("#FFFFFF"),
                android.graphics.Color.parseColor("#FFFFFF"))
            val gradientDisabled = GradientDrawable(GradientDrawable.Orientation.TR_BL, gradientDisabledColors)
            val figureRadius = context?.run{
                resources.getDimension(R.dimen.sign_process_radius_r07)
            }?: 0F
            gradientDisabled.cornerRadius = figureRadius
            background = gradientDisabled
            isEnabled  = false
            elevation  = resources.getDimension(R.dimen.button_elevation_e04)
        }

        //setup button SignWithSumraId
        _frgBind.clickButtonSignInSumra.run{
            val gradientDisabledColors = intArrayOf(textColLight, textColLight)
            val gradientDisabled = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientDisabledColors)

            val gradientUnPressedColors = intArrayOf(darkColor, lightColor)
            val gradientUnPressed = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientUnPressedColors)

            val gradientOkPressedColors = intArrayOf(lightColor, darkColor)
            val gradientOkPressed = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientOkPressedColors)

            val figureRadius = context?.run{
                resources.getDimension(R.dimen.sign_process_radius_r07)
            }?: 0F

            gradientDisabled .cornerRadius = figureRadius
            gradientUnPressed.cornerRadius = figureRadius
            gradientOkPressed.cornerRadius = figureRadius

            val stateList = StateListDrawable()
            stateList.run {
                addState(
                    intArrayOf(-android.R.attr.state_enabled),
                    gradientDisabled
                )
                addState(
                    intArrayOf(android.R.attr.state_pressed),
                    gradientUnPressed
                )
                addState(
                    intArrayOf(-android.R.attr.state_pressed),
                    gradientOkPressed
                )
            }
            background = stateList
            radius = figureRadius
            elevation = resources.getDimension(R.dimen.button_elevation_e00)
        }

        _frgBind.optionsNextIconTheButton.visibility = View.GONE
        _frgBind.optionCompose.setContent{ DialogPhone(isPhoneInfo) }
    }
    lateinit var cTypeFace: androidx.compose.ui.text.font.Typeface
    private fun setupCustomFont(){
        fontAssetsPath?.let{ font->
            context?.run{
                val typeface = Typeface.createFromAsset(this.assets, font)
                cTypeFace = androidx.compose.ui.text.font.Typeface(typeface)
                _frgBind.stepNameOptions.typeface          = typeface
                _frgBind.labelSelectTheMessenger.typeface  = typeface
                _frgBind.labelSelectThePhone.typeface      = typeface
                _frgBind.separatorFirst.typeface           = typeface
                _frgBind.separatorSecond.typeface          = typeface
                _frgBind.optionsBottomHelp.typeface        = typeface
                _frgBind.optionsNextStepTheButton.typeface = typeface
                _frgBind.optionsPhoneCodeSelected.typeFace = typeface
                _frgBind.optionsPhoneNumberEnter.typeface  = typeface
            }
        }
    }

    fun addListeners(){
        _frgBind.clickButtonSignUpPhone.setOnClickListener { btn ->
            btn.hideKeyboard()
            if(_frgBind.optionsPhoneCodeSelected.isValidFullNumber) {
                frgListener.resultActionListener(
                    OperationResultEvents.EventSelectedPhone(
                        phoneCode = _frgBind.optionsPhoneCodeSelected.getSelectedCountryCode(),
                        phoneNumber = _frgBind.optionsPhoneCodeSelected.fullNumberWithPlus
                    ))
            }else {
                frgListener.resultActionListener(OperationResultEvents.EventPushFailure(getString(R.string.number_phone_error)))
            }
        }

        _frgBind.clickButtonSignInSumra.setOnClickListener { v ->
            frgListener.resultActionListener(OperationResultEvents.EventSelectedSumraId)
        }
    }

    private fun addResourceLogo(resId: Int){
        this.resId = resId
    }

    private fun addListener(listener: FragmentListener){
        this.frgListener = listener
    }

    fun updateDialog(article: String){
//        stateT.value = title
//        stateA.value = article
        stateP.value = "${_frgBind.optionsPhoneCodeSelected.fullNumberWithPlus}"
        isPhoneInfo.value = true
    }

    private val stateT = mutableStateOf("Confirmation!") //resources.getString(R.string._400))
    private val stateA = mutableStateOf("Please confirm you phone number:")
    private val stateP = mutableStateOf("")

    private val isPhoneInfo  = mutableStateOf(false)
    private val meetJoinName = mutableStateOf("")
    private val dH   = 56
    private val bR   =  7
    private val padD = 10
    private val spH  = 24

    @ExperimentalComposeUiApi
    @Composable
    @ExperimentalTextApi
    @ExperimentalMaterialApi
    fun DialogPhone(isShowDialog: MutableState<Boolean>) {
        if (isShowDialog.value) {
            val brush = Brush.horizontalGradient(
                colors = listOf(
                    Color(0xff2188FF),
                    Color(0xff0366D6)
                )
            )
            val dis = Color(0xff959BAD)

            Dialog(
                onDismissRequest = { }
//                properties = DialogProperties(usePlatformDefaultWidth = false)
            ) {
                Surface(
//                    modifier = Modifier.fillMaxSize(),
                    shape = RoundedCornerShape(16.dp),
                    color = Color.LightGray
                ) {
//                    Box(
//                        contentAlignment = Alignment.Center
//                    ) {
//                        Text(modifier = Modifier.align(Alignment.TopCenter),
//                            text = "top")
//                        Text("center")
//                        Text(
//                            modifier = Modifier.align(Alignment.BottomCenter),
//                            text = "bottom")
//                    }

                    Column(
                        modifier = Modifier
                            .fillMaxWidth(1f)
                            .padding(16.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(
                            text = stateT.value, // stringResource(id = R.string.text_recent_meet_join_title),
                            style = StyleText.txtShowTitle(requireActivity().assets, fontAssetsPath!!),
                            textAlign = TextAlign.Center,
                            modifier = Modifier.fillMaxWidth(1f))

                        Spacer(modifier = Modifier.height(spH.dp))

                        Text(
                            text = stateA.value, // stringResource(id = R.string.text_recent_meet_join_title),
                            style = StyleText.txtShowArticle(requireActivity().assets, fontAssetsPath!!),
                            textAlign = TextAlign.Center,
                            modifier = Modifier.fillMaxWidth(1f))

                        Spacer(modifier = Modifier.height(spH.dp))

                        Text(
                            text = stateP.value, // stringResource(id = R.string.text_recent_join_conversation)
                            style = StyleText.txtShowPhone(requireActivity().assets, fontAssetsPath!!),
                            textAlign = TextAlign.Center,
                            modifier = Modifier
                                .fillMaxWidth(1f)
                        )

                        Spacer(modifier = Modifier.height(spH.dp))

                        Row (
                            modifier = Modifier.fillMaxWidth(1f),
                            horizontalArrangement = Arrangement.Center
                        ){

                            Surface(
                                onClick = {
                                    isShowDialog.value = false },
                                modifier = Modifier
                                    .height(dH.dp),
//                                    .padding(bottom = padD.dp),
                                shape = RoundedCornerShape(size = bR.dp),
                            ) {
                                Box(
                                    modifier = Modifier
                                        .background(
                                            color = dis,
                                            shape = RoundedCornerShape(size = bR.dp)
                                        ),
                                    contentAlignment = Alignment.CenterStart
                                ) {
                                    Text(
                                        text = "NO", // stringResource(id = R.string.text_recent_meet_cancel),
                                        style = StyleText.txtButtonLbl(cTypeFace),
                                        modifier = Modifier.padding(horizontal = 20.dp)
                                    )
                                }
                            }
                            Spacer(modifier = Modifier.width(spH.dp))

                            Surface(
                                onClick = {
                                          frgListener.resultActionListener(OperationResultEvents
                                              .EventConfirmPhone(phoneNumber = stateP.value))
                                },
                                modifier = Modifier
                                    .height(dH.dp).background(color = Color.Transparent),
//                                    .padding(end = padD.dp)
                                shape = RoundedCornerShape(size = bR.dp)
                            ) {
                                Box(
                                    modifier = Modifier
                                        .background(
                                            brush = brush,
                                            shape = RoundedCornerShape(size = bR.dp)
                                        ),
                                    contentAlignment = Alignment.CenterEnd
                                ) {
                                    Text(
                                        text = "YES", // stringResource(id = R.string.text_recent_button_meet_join),
                                        style = StyleText.txtButtonLbl(cTypeFace),
                                        modifier = Modifier
                                            .padding(horizontal = 20.dp)
                                    )
                                }
                            }
                        }
                    }
                }
            }

//            AlertDialog(
//                properties = DialogProperties(usePlatformDefaultWidth = false),
//                modifier = Modifier.fillMaxSize(1f),
//                shape = RoundedCornerShape(16.dp),
//                onDismissRequest = {  },
//                title = null,
//                dismissButton = {
//                    Surface(
//                        onClick = {
//                            // Change the state to close the dialog
//                            isShowDialog.value = false },
//                        modifier = Modifier
//                            .height(dH.dp)
//                            .padding(bottom = padD.dp),
//                        shape = RoundedCornerShape(size = bR.dp)
//                    ) {
//                        Box(
//                            modifier = Modifier
//                                .background(
//                                    color = dis,
//                                    shape = RoundedCornerShape(size = bR.dp)
//                                ),
//                            contentAlignment = Alignment.Center
//                        ) {
//                            Text(
//                                text = "NO", // stringResource(id = R.string.text_recent_meet_cancel),
////                                style = StyleText.txtButtonLbl(requireActivity().assets, fontAssetsPath!!),
//                                modifier = Modifier.padding(horizontal = 20.dp)
//                            )
//                        }
//                    }
//                },
//                confirmButton = {
//                    Surface(
//                        onClick = {
////                            viewModelHome.validMeetingCode(meetJoinName.value)
//                                  },
//                        modifier = Modifier
//                            .height(dH.dp)
//                            .padding(bottom = padD.dp, end = padD.dp),
//                        shape = RoundedCornerShape(size = bR.dp)
//                    ) {
//                        Box(
//                            modifier = Modifier
//                                .background(
//                                    brush = brush,
//                                    shape = RoundedCornerShape(size = bR.dp)
//                                ),
//                            contentAlignment = Alignment.Center
//                        ) {
//                            Text(
//                                text = "YES", // stringResource(id = R.string.text_recent_button_meet_join),
////                                style = StyleText.txtButtonLbl(requireActivity().assets, fontAssetsPath!!),
//                                modifier = Modifier
//                                    .padding(horizontal = 20.dp)
//                            )
//                        }
//                    }
//                },
//                text = {
//                    Column(
//                        modifier = Modifier
//                            .fillMaxWidth(1f),
//                        horizontalAlignment = Alignment.CenterHorizontally
//                    ) {
//                        Text(
//                            text = stateT.value, // stringResource(id = R.string.text_recent_meet_join_title),
////                            style = StyleText.txtListHead(requireActivity().assets, fontAssetsPath!!),
//                            textAlign = TextAlign.Center,
//                            modifier = Modifier.fillMaxWidth(1f))
//                        Spacer(modifier = Modifier.height(spH.dp))
//                        Text(
//                            text = stateA.value, // stringResource(id = R.string.text_recent_meet_join_title),
////                            style = StyleText.txtListHead(requireActivity().assets, fontAssetsPath!!),
//                            textAlign = TextAlign.Center,
//                            modifier = Modifier.fillMaxWidth(1f))
////                        OutlinedTextField(
////                            label = {
////                                Text(
////                                    text = "BUTTON A", // stringResource(id = R.string.text_recent_meet_join),
//////                                    style = StyleText.txtInfoLbl(requireActivity().assets, fontAssetsPath!!),
////                                    textAlign = TextAlign.Start
////                                )
////                            },
////                            colors = TextFieldDefaults.outlinedTextFieldColors(
////                                unfocusedBorderColor =  Color(0xffECEDF3),
////                                focusedBorderColor = Color(0xff0366D6),
////
////                                focusedLabelColor  = Color(0xffDBEDFF),
////                                unfocusedLabelColor = Color(0xff6A737D),
////
////                                backgroundColor = Color(0xffDBEDFF),
////
////                                textColor =  Color(0xff444D56),
////                                cursorColor        = Color(0xff444D56)
////                            ),
////                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
//////                            KeyboardOptions.Default.copy(keyboardType = KeyboardType.Text),
////                            value = meetJoinName.value,
////                            onValueChange = {
////                                meetJoinName.value = it // .take(12)
////                            },
////                            shape = RoundedCornerShape(8.dp),
////                            modifier = Modifier
////                                .fillMaxWidth()
////                                .height(56.dp)
////                        )
//                        Spacer(modifier = Modifier.height(spH.dp))
//                        Text(
//                            text = stateP.value, // stringResource(id = R.string.text_recent_join_conversation)
////                            style = StyleText.txtInfoLbl(requireActivity().assets, fontAssetsPath!!),
//                            textAlign = TextAlign.Center,
//                            modifier = Modifier
//                                .fillMaxWidth(1f)
//                        )
//                        Spacer(modifier = Modifier.height(spH.dp))
//
//                        Row (modifier = Modifier.fillMaxWidth(1f)){
//                            Surface(
//                                onClick = {
//                                    // Change the state to close the dialog
//                                    isShowDialog.value = false },
//                                modifier = Modifier
//                                    .height(dH.dp)
//                                    .padding(bottom = padD.dp),
//                                shape = RoundedCornerShape(size = bR.dp)
//                            ) {
//                                Box(
//                                    modifier = Modifier
//                                        .background(
//                                            color = dis,
//                                            shape = RoundedCornerShape(size = bR.dp)
//                                        ),
//                                    contentAlignment = Alignment.Center
//                                ) {
//                                    Text(
//                                        text = "NO", // stringResource(id = R.string.text_recent_meet_cancel),
////                                style = StyleText.txtButtonLbl(requireActivity().assets, fontAssetsPath!!),
//                                        modifier = Modifier.padding(horizontal = 20.dp)
//                                    )
//                                }
//                            }
//                            Spacer(modifier = Modifier.width(spH.dp))
//                            Surface(
//                                onClick = {
////                            viewModelHome.validMeetingCode(meetJoinName.value)
//                                },
//                                modifier = Modifier
//                                    .height(dH.dp)
//                                    .padding(bottom = padD.dp, end = padD.dp),
//                                shape = RoundedCornerShape(size = bR.dp)
//                            ) {
//                                Box(
//                                    modifier = Modifier
//                                        .background(
//                                            brush = brush,
//                                            shape = RoundedCornerShape(size = bR.dp)
//                                        ),
//                                    contentAlignment = Alignment.Center
//                                ) {
//                                    Text(
//                                        text = "YES", // stringResource(id = R.string.text_recent_button_meet_join),
////                                style = StyleText.txtButtonLbl(requireActivity().assets, fontAssetsPath!!),
//                                        modifier = Modifier
//                                            .padding(horizontal = 20.dp)
//                                    )
//                                }
//                            }
//                        }
//                    }
//                },
//            )
        }
    }

    private val d = mutableStateOf(true)
    @ExperimentalComposeUiApi
    @ExperimentalTextApi
    @Preview
    @Composable
    @ExperimentalMaterialApi
    fun FrgPreview(){
        DialogPhone(d)
    }

    private object StyleText {

        @ExperimentalTextApi
        fun txtListHead(aM: AssetManager, path: String) = androidx.compose.ui.text.TextStyle(
            fontSize = 20.sp,
            fontFamily = FontFamily(Font(assetManager = aM, path = path, weight = FontWeight.W900, style = FontStyle.Normal)),
            textAlign = TextAlign.Justify,
            color = Color(0xff0366D6)
        )

        @ExperimentalTextApi
        fun txtShowTitle(aM: AssetManager, path: String) = androidx.compose.ui.text.TextStyle(
            fontSize = 28.sp,
            fontFamily = FontFamily(Font(assetManager = aM, path = path, weight = FontWeight.W900, style = FontStyle.Normal)),
            textAlign = TextAlign.Justify,
            color = Color(0xff2E3140)
        )

        @ExperimentalTextApi
        fun txtShowPhone(aM: AssetManager, path: String) = androidx.compose.ui.text.TextStyle(
            fontSize = 16.sp,
            fontFamily = FontFamily(Font(assetManager = aM, path = path, weight = FontWeight.W900, style = FontStyle.Normal)),
            textAlign = TextAlign.Justify,
            color = Color(0xff0366D6)
        )


        @ExperimentalTextApi
        fun txtButtonLbl(typeFace: androidx.compose.ui.text.font.Typeface) = androidx.compose.ui.text.TextStyle(
            fontSize   = 20.sp,
            fontFamily = FontFamily(typeFace),
            textAlign  = TextAlign.Justify,
            color      = Color(0xffFFFFFF)
        )

        @ExperimentalTextApi
        fun txtShowArticle(aM: AssetManager, path: String) = androidx.compose.ui.text.TextStyle(
            fontSize = 16.sp,
            fontFamily = FontFamily(Font(assetManager = aM, path = path, weight = FontWeight.W900, style = FontStyle.Normal)),
            textAlign = TextAlign.Justify,
            color = Color(0xff2E3140)
        )
    }
}