package net.sumra.auth.fragments

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.StateListDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import net.sumra.auth.ControllerWrapper
import net.sumra.auth.FragmentAction
import net.sumra.auth.FragmentListener
import net.sumra.auth.R
import net.sumra.auth.databinding.FragmentCongratsBinding
import net.sumra.auth.helpers.*
import net.sumra.auth.states.ActionEvents
import net.sumra.auth.states.OperationResultEvents

//@Parcelize
//data class AddReceiverCode(val code: String): Parcelable

class FragmentCongrats: FragmentBase(), FragmentAction {

    companion object{
        fun getInstance(context: Context, frgHandler: FragmentListener): Fragment {
            return Builder().apply {
                this.context = context
                this.handler = frgHandler
            }.build()
        }
    }

    internal class Builder{
        internal var handler: FragmentListener? = null
        internal var context: Context? = null
        internal fun build(): FragmentCongrats {
            val fragment = FragmentCongrats()
            context?.run { fragment.setContext(this) }
            fragment.addListener    (handler!!)
            return fragment
        }
    }

    internal lateinit var context: Context
    private lateinit var _frgListener : FragmentListener
    private lateinit var _frgBind     : FragmentCongratsBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is FragmentListener){
            logI("Inject FragmentListener SUCCESS")
        }else{
            logE("Inject FragmentListener FAILURE")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _frgBind = FragmentCongratsBinding.inflate(layoutInflater)
        setupFont()
        setupUI()
        addListeners()
        return _frgBind.root
    }

    private fun setupUI(){

    /** Global setup views and buttons ========================================================================= **/

        ControllerWrapper.congratsIcon?.run {
            _frgBind.congratsIcon.setImageResource(this)
        }

    /** Setup texts block for insert texts, weight and color setup============================================== **/

        _frgBind.congratsVictory.text = HtmlCompat.fromHtml("""<span>
            |<b><font color = ${String.format("#%06X", (0xffffff and ControllerWrapper.textColPrimDark))}>
            |${resources.getString(R.string.congrats_victory_label)}
            |</font></b>
            |</span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)

        _frgBind.congratsVictoryHelp.text = HtmlCompat.fromHtml("""<span>
            |<p><font color = ${String.format("#%06X", (0xffffff and ControllerWrapper.textColPrimary))}>
            |${resources.getString(R.string.congrats_victory_help_a)}
            |</font></p>
            |<p><font color = ${String.format("#%06X", (0xffffff and ControllerWrapper.textColPrimary))}>
            |${resources.getString(R.string.congrats_victory_help_b)}
            |</font></p>
            |<p><font color = ${String.format("#%06X", (0xffffff and ControllerWrapper.textColPrimary))}>
            |${resources.getString(R.string.congrats_victory_help_c)}
            |</font></p>
            |</span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)


        _frgBind.congratsConfirmText.text = HtmlCompat.fromHtml("""<span>
            |<b><font color = ${String.format("#%06X", (0xffffff and Color.parseColor("#ffffff")))}>
            |${resources.getString(R.string.congrats_confirm)}
            |</font></b>
            |</span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)

        _frgBind.congratsConfirm.apply {
            radius = resources.getDimension(R.dimen.sign_process_radius_r07)
            elevation = resources.getDimension(R.dimen.button_elevation_e00)
            background = ControllerWrapper.setupSelectorButtonContinue(context)
        }
    }

    private fun addListeners(){
        _frgBind.congratsConfirm.setOnClickListener { view ->
            _frgListener.resultActionListener(OperationResultEvents.EventCongrats)
        }
    }

    private fun setupFont(){
        context?.run {
            ControllerWrapper.fontAssetsPath?.let { font ->
                val typeFace = Typeface.createFromAsset(this.assets, font)
                _frgBind.congratsVictory.typeface = typeFace
                _frgBind.congratsVictoryHelp.typeface = typeFace
                _frgBind.congratsConfirmText.typeface = typeFace
            }
        }
    }

    override fun onBackPressed() {
        toBackStack()
    }

    private fun toBackStack() {
        _frgListener.resultActionListener(OperationResultEvents.EventPopBackStack)
    }

    private fun addListener(handler: FragmentListener){
        _frgListener = handler
    }

    private fun setContext(context: Context){
        this.context = context
    }

    override fun updateFragmentByAction(result: ActionEvents) {
        TODO("Not yet implemented")
    }
}