package net.sumra.auth.fragments

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.StateListDrawable
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.text.HtmlCompat
import net.sumra.auth.ControllerWrapper
import net.sumra.auth.FragmentAction
import net.sumra.auth.FragmentListener
import net.sumra.auth.R
import net.sumra.auth.databinding.FragmentSumraIdBinding
import net.sumra.auth.helpers.*
import net.sumra.auth.states.ActionEvents
import net.sumra.auth.states.OperationResultEvents
import java.lang.NullPointerException
import java.util.concurrent.DelayQueue

class FragmentSumraID: FragmentBase(), FragmentAction {

    class Builder {
        var buildLogo: Int? = null
        lateinit var buildListener: FragmentListener
        fun build(): FragmentSumraID {
            val fragment = FragmentSumraID()
            fragment._frgListener = buildListener
            fragment._resourceLogo = buildLogo
            return fragment
        }
    }

    private var _resourceLogo: Int? = null
    private lateinit var _frgListener: FragmentListener
    private lateinit var _frgBind: FragmentSumraIdBinding
    private var usernameMin      = false
    private var passwordMin      = false
    private var passwordShowSate = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _frgBind = FragmentSumraIdBinding.inflate(layoutInflater)
        setupUI()
        setupTypeFace()
        addListeners()
        return _frgBind.root
    }

    private fun setupUI(){

        _frgBind.sumraIdEnterUsername.text?.run{
            if(length >= 8) {
                _frgListener.resultActionListener(OperationResultEvents
                    .EventSumraIdUsernameCheck(this))
            }
        }

        _resourceLogo?.run {
            _frgBind.sumraIdEnterLogo.setImageResource(this)
        }?: {
            _frgBind.sumraIdAppBarLogoContainer.visibility = View.GONE
        }

        _frgBind.sumraIdButtonBackByToolbar.apply {
            imageTintList = ColorStateList(
                arrayOf(intArrayOf(-android.R.attr.state_pressed), intArrayOf(android.R.attr.state_pressed)),
                intArrayOf(ControllerWrapper.darkColor, ControllerWrapper.lightColor))
        }

        _frgBind.sumraIdPasswordShow.colorTintUpdate(
            ColorStateList(
                arrayOf(intArrayOf(-android.R.attr.state_pressed), intArrayOf(android.R.attr.state_pressed)),
                intArrayOf(ControllerWrapper.darkColor, ControllerWrapper.darkColor)
        ))

        /** Setup texts block for insert texts, weight and color setup============================== **/

        _frgBind.sumraIdTextLabel.text = HtmlCompat.fromHtml("""<span>
            |<b><font color = ${String.format("#%06X", (0xffffff and ControllerWrapper.textColPrimDark))}>
            |${resources.getString(R.string.sumra_id_toolbar_label)}
            |</font></b>
            |</span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)

        _frgBind.sumraIdHeaderText.text = HtmlCompat.fromHtml("""<span>
            |<p><font color = ${String.format("#%06X", (0xffffff and ControllerWrapper.textColPrimary))}>
            |${resources.getString(R.string.sumra_id_label_help_text_a)}
            |</font></p>
            |<p><font color = ${String.format("#%06X", (0xffffff and ControllerWrapper.textColPrimary))}>
            |${resources.getString(R.string.sumra_id_label_help_text_b)}
            |</font></p>
            |</span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)

        _frgBind.sumraIdEnterUsername.apply {
            setHintTextColor ( ControllerWrapper.textColPrimary )
            setTextColor     ( ControllerWrapper.textColPrimary )
            hint = resources.getString(R.string.sumra_id_username_hint)
            elevation = resources.getDimension(R.dimen.button_elevation_e04)
            background = setupSelectorEditUsername()
        }

        _frgBind.sumraIdEnterPassword.apply {
            setHintTextColor ( ControllerWrapper.textColPrimary )
            setTextColor     ( ControllerWrapper.textColPrimary )
            hint = resources.getString(R.string.sumra_id_password_hint)
            elevation = resources.getDimension(R.dimen.button_elevation_e04)
            background = setupSelectorEditUsername()
        }

        _frgBind.sumraIdButtonLogo.text = HtmlCompat.fromHtml("""<span>
            |<b><font>
            |${resources.getString(R.string.sumra_id_button_label)}
            |</font></b>
            |</span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)

        _frgBind.sumraIdBottomHelp.text = HtmlCompat.fromHtml("""<span>
            |<p><b><font color=${String.format("#%06X", (0xffffff and ControllerWrapper.textColLight))}>
            |${resources.getString(R.string.therms_private_policy_all)}
            |</font></b></p>
            |<p><b><font color=${String.format("#%06X", (0xffffff and ControllerWrapper.textColSelect))}>
            |${resources.getString(R.string.therms_private_policy)}
            |</font></b></p>
            |</span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)

        // setup selector for login the button
        _frgBind.sumraIdButtonLoginConfirmation.apply {
            radius = resources.getDimension(R.dimen.sign_process_radius_r07)
            elevation = resources.getDimension(R.dimen.button_elevation_e00)
            background = ControllerWrapper.setupSelectorButtonContinue(context)
        }

        // setup listStateColor back the button
        _frgBind.sumraIdBackTheButton.apply {
            isFocusableInTouchMode = true
            imageTintList = ControllerWrapper.setupBackButtonTint()
            requestFocus()
        }

        if(!usernameMin && !passwordMin) {
            _frgBind.sumraIdButtonLoginConfirmation.isEnabled = false
        }

        _frgBind.sumraIdPasswordShow.renderState(true)
    }

    private fun addListeners(){
        _frgBind.sumraIdEnterUsername.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {/*TODO("Not yet implemented"*/}
            override fun afterTextChanged(s: Editable?) { /*TODO("Not yet implemented")*/ }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.run {
                    usernameMin = length >= Finals.min_username_symbols
                    switchedButton()
                }
            }
        })

        _frgBind.sumraIdEnterPassword.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {/*TODO("Not yet implemented"*/}
            override fun afterTextChanged(s: Editable?) { /*TODO("Not yet implemented")*/ }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.run {
                    passwordMin = length >= Finals.min_username_symbols
                    switchedButton()
                }
            }
        })

        _frgBind.sumraIdButtonLoginConfirmation.setOnClickListener { btn ->
            btn.hideKeyboard()
            try {
                _frgListener.resultActionListener(
                    OperationResultEvents.EventSumraIdLogin(
                        username = _frgBind.sumraIdEnterUsername.text!!,
                        password = _frgBind.sumraIdEnterPassword.text!!
                    )
                )
            }catch (ex: NullPointerException){
                _frgListener.resultActionListener(
                    result = OperationResultEvents.EventPushFailure(message = "Username or password cannot be empty!"))
            }
        }

        _frgBind.sumraIdButtonBackByToolbar.setOnClickListener { btn ->
            toBackStack()
            btn.hideKeyboard()
        }

        _frgBind.sumraIdBackTheButton.setOnClickListener { btn ->
            toBackStack()
            btn.hideKeyboard()
        }

        _frgBind.sumraIdPasswordShow.setOnClickListener { view->
            _frgBind.sumraIdEnterPassword.run {
                when (passwordShowSate) {
                    true -> {
                        passwordShowSate = false
                        transformationMethod = PasswordTransformationMethod()
                        _frgBind.sumraIdPasswordShow.renderState(true)
                    }
                    else -> {
                        passwordShowSate = true
                        transformationMethod = HideReturnsTransformationMethod()
                        _frgBind.sumraIdPasswordShow.renderState(false)
                    }
                }
                setSelection(text?.length ?: 0)
            }
        }
    }

//    fun EditText.showPassword(visible: Boolean, updateCursor: Boolean = true) {
//        if (visible) {
//            inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
//        } else {
//            inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
//        }
//        if (updateCursor) setSelection(text?.length ?: 0)
//    }

    private fun setupSelectorEditUsername(): StateListDrawable {
        val gradientUnFocusedColors = intArrayOf(Color.parseColor("#ffffff"), Color.parseColor("#ffffff"))
        val gradientUnFocused = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientUnFocusedColors)

        val gradientOkFocusedColors = intArrayOf(ControllerWrapper.lightColor, ControllerWrapper.lightColor)
        val gradientOkFocused = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientOkFocusedColors)

        val figureRadius = context?.run{
            resources.getDimension(R.dimen.sign_process_radius_r07)
        }?: 0F

        gradientUnFocused.cornerRadius = figureRadius
        gradientOkFocused.cornerRadius = figureRadius
        gradientOkFocused.setStroke(resources.getDimension(R.dimen.phone_border_weight).toInt(), ControllerWrapper.darkColor)

        val stateList = StateListDrawable()
        stateList.run {
            addState(
                intArrayOf(-android.R.attr.state_focused),
                gradientUnFocused
            )
            addState(
                intArrayOf(android.R.attr.state_focused),
                gradientOkFocused
            )
        }
        return stateList
    }

    private fun setupTypeFace() {
        context?.run {
            ControllerWrapper.fontAssetsPath?.let { font ->
                val typeFace = Typeface.createFromAsset(this.assets, font)
                _frgBind.sumraIdAppBarLabel.typeface     = typeFace

                _frgBind.sumraIdTextLabel.typeface     = typeFace
                _frgBind.sumraIdHeaderText.typeface    = typeFace
                _frgBind.sumraIdEnterUsername.typeface = typeFace
                _frgBind.sumraIdEnterPassword.typeface = typeFace
                _frgBind.sumraIdButtonLogo.typeface    = typeFace
                _frgBind.sumraIdBottomHelp.typeface    = typeFace
            }
        }
    }

    private fun switchedButton(){
        _frgBind.sumraIdButtonLoginConfirmation.isEnabled = usernameMin && passwordMin
    }

    override fun onBackPressed() {
        toBackStack()
    }

    private fun toBackStack() {
        _frgListener.resultActionListener(OperationResultEvents.EventPopBackStack)
    }


    override fun updateFragmentByAction(result: ActionEvents) {
        if(result == ActionEvents.ActionEventsSuccess){
//            _frgBind.sumraIdUsernamePictureSuccess.visibility = View.VISIBLE
//            _frgBind.sumraIdUsernamePictureFailure.visibility = View.GONE
            _frgBind.sumraIdButtonLoginConfirmation.isEnabled = true
        }else{
//            _frgBind.sumraIdUsernamePictureFailure.visibility = View.VISIBLE
//            _frgBind.sumraIdUsernamePictureSuccess.visibility = View.GONE
        }
    }
}