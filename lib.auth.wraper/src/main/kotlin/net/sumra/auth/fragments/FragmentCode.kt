package net.sumra.auth.fragments

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.StateListDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.text.HtmlCompat
import androidx.core.view.children
import androidx.core.view.forEachIndexed
import androidx.fragment.app.Fragment
import net.sumra.auth.ControllerListener
import net.sumra.auth.ControllerWrapper
import net.sumra.auth.FragmentListener
import net.sumra.auth.R
import net.sumra.auth.databinding.FragmentCodeBinding
import net.sumra.auth.helpers.enterSymbol
import net.sumra.auth.helpers.hideKeyboard
import net.sumra.auth.helpers.logI
import net.sumra.auth.states.OperationResultEvents
import kotlin.properties.Delegates

class FragmentCode(): FragmentBase() {

    companion object{
        fun getInstance(
            resId: Int,
            frgListener: FragmentListener,
            /* ------------------------ */
            lightColor: Int,
            darkColor : Int,
            textColPrimDark : Int,
            textColNormDark : Int,
            textColPrimary  : Int,
            textColLight    : Int,
            textColSelect   : Int,
            fontAssetsPath  : String?,
            backgroundColor : Int,
            codeColorFocusNo: Int,
            codeColorFocusOk: Int,
            phoneSending    : String? = null): Fragment {
            return Builder().apply {
                this.resId       = resId
                this.listener    = frgListener
                /* ---------------------------------------------------------- */
                _lightColor      = lightColor
                _darkColor       = darkColor
                _textColPrimDark = textColPrimDark
                _textColPrimary  = textColPrimary
                _textColNormDark = textColNormDark
                _textColLight    = textColLight
                _textColSelect   = textColSelect
                _fontAssetsPath  = fontAssetsPath
                _backgroundColor = backgroundColor
                _codeColorFocusNo = codeColorFocusNo
                _codeColorFocusOk = codeColorFocusOk
                _phoneSending    = phoneSending
            }.build()
        }
    }

    internal class Builder{
        internal var resId    : Int? = null
        internal var listener : FragmentListener? = null
        var _lightColor       by Delegates.notNull<Int>()
        var _darkColor        by Delegates.notNull<Int>()
        var _textColPrimDark  by Delegates.notNull<Int>()
        var _textColNormDark  by Delegates.notNull<Int>()
        var _textColPrimary   by Delegates.notNull<Int>()
        var _textColLight     by Delegates.notNull<Int>()
        var _textColSelect    by Delegates.notNull<Int>()
        var _backgroundColor  by Delegates.notNull<Int>()
        var _codeColorFocusNo by Delegates.notNull<Int>()
        var _codeColorFocusOk by Delegates.notNull<Int>()
        var _fontAssetsPath   : String? = null
        var _phoneSending     : String? = null

        internal fun build(): FragmentCode {
            val fragment = FragmentCode()
            fragment.addResourceLogo(resId!!)
            fragment.addListener(listener!!)
            fragment.lightColor       = _lightColor
            fragment.darkColor        = _darkColor
            fragment.textColPrimDark  = _textColPrimDark
            fragment.textColPrimary   = _textColPrimary
            fragment.textColNormDark  = _textColNormDark
            fragment.textColLight     = _textColLight
            fragment.textColSelect    = _textColSelect
            fragment.fontAssetsPath   = _fontAssetsPath

            fragment.codeColorFocusNo = _codeColorFocusNo
            fragment.codeColorFocusOk = _codeColorFocusOk

            fragment.backgroundColor  = _backgroundColor
            fragment.phoneSending     = _phoneSending
            return fragment
        }
    }

    private var _resId: Int? = null
    private lateinit var _frgListener: FragmentListener

    private var lightColor         by Delegates.notNull<Int>()
    private var darkColor          by Delegates.notNull<Int>()
    private var textColPrimDark    by Delegates.notNull<Int>()
    private var textColNormDark    by Delegates.notNull<Int>()
    private var textColPrimary     by Delegates.notNull<Int>()
    private var textColLight       by Delegates.notNull<Int>()
    private var textColSelect      by Delegates.notNull<Int>()

    private var codeColorFocusNo   by Delegates.notNull<Int>()
    private var codeColorFocusOk   by Delegates.notNull<Int>()

    private var fontAssetsPath     : String? = null
    private var backgroundColor    : Int = Color.parseColor("#FFFFFF")
    private var phoneSending       : String? = null

    private lateinit var _frgBind: FragmentCodeBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _frgBind = FragmentCodeBinding.inflate(layoutInflater)
        setupUI()
        setupFont()
        addListeners()
        return _frgBind.root
    }

    private fun setupUI(){
        _frgBind.codeEnterLogo.visibility = View.GONE
        _frgBind.securityCodeButtonIcon.visibility = View.GONE
        _frgBind.securityCodeContainer.setBackgroundColor(backgroundColor)

        _resId?.run {
            _frgBind.codeEnterLogo.setImageResource(this)
        }?: { _frgBind.codeEnterLogo.visibility = View.GONE }

        _frgBind.stepNameSecurityCode.text = HtmlCompat
            .fromHtml("""<span>
                |<b><font color = ${String.format("#%06X", (0xffffff and textColPrimDark))}>
                |${resources.getString(R.string.title_label_code)}</font></b>
                |</span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)

        _frgBind.securityCodeLabelInfo.text = HtmlCompat
            .fromHtml("""<span>
                |<font color = ${String.format("#%06X", (0xffffff and textColPrimary))}>
                |${resources.getString(R.string.security_code_section_a)}</font>
                |<b><font color = ${String.format("#%06X", (0xffffff and textColPrimary))}>
                | ${resources.getString(R.string.security_code_section_b)}</font></b>
                |<font color = ${String.format("#%06X", (0xffffff and textColPrimary))}>
                | ${resources.getString(R.string.security_code_section_c)}</font
                |</span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)

        phoneSending?.run {
            _frgBind.securityCodePhoneInfo.text = HtmlCompat
                .fromHtml("""<span>
                    |<font color = ${String.format("#%06X", (0xffffff and textColPrimary))}>
                    |${resources.getString(R.string.security_code_section_d)}</font>
                    |<b><font color=${String.format("#%06X", (0xffffff and textColSelect))}>
                    |${this}</font></b>
                    |</span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT
            )
            _frgBind.securityCodeReceive.text = HtmlCompat
                .fromHtml("""<span>
                    |<font color=${String.format("#%06X", (0xffffff and textColPrimary))}>
                    |${resources.getString(R.string.security_code_receive)}</font>
                    |</span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)

            _frgBind.securityResendCode.text = HtmlCompat
                .fromHtml("""<span>
                    |<b><font color=${String.format("#%06X", (0xffffff and textColSelect))}>
                    |${resources.getString(R.string.security_code_resend)}</font></b>
                    |</span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)
        }?:{
            _frgBind.securityCodePhoneInfo.visibility = View.GONE
            _frgBind.securityCodeReceive.visibility   = View.GONE
            _frgBind.securityResendCode.visibility    = View.GONE
        }

        _frgBind.securityCodeButtonLabel.text = HtmlCompat
            .fromHtml("""<span>
                |<font color = ${String.format("#%06X", (0xffffff and Color.parseColor("#FFFFFF")))}>
                |${resources.getString(R.string.button_confirmation_code)}</font>
                |</span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)

        // text color bottom text private policy...
        _frgBind.securityCodeTermsPolicy.text = HtmlCompat
            .fromHtml("""<span><p><b>
                |<font color=${String.format("#%06X", (0xffffff and textColLight))}>
                |${resources.getString(R.string.therms_private_policy_all)}</font></b></p>
                |<p><b><font color=${String.format("#%06X", (0xffffff and textColSelect))}>
                |${resources.getString(R.string.therms_private_policy)}</font></b></p>
                |</span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)

        _frgBind.securityCodeEnteredContainer.children.forEach { itemView ->
            val appView = (itemView as AppCompatEditText)
            appView.hint = HtmlCompat
                .fromHtml("""<span>
                    |<font color = ${String.format("#%06X", (0xffffff and textColLight))}>
                    |</font>
                    |</span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)
            appView.setTextColor(Color.parseColor("#ffffff"))
            appView.isCursorVisible = false
//            appView.textCursorDrawable =
            appView.apply {
                background = setupEnterCodeDrawableState()
                elevation  = resources.getDimension(R.dimen.button_elevation_e08)
            }
        }

        _frgBind.securityCodeButtonNext.apply {
            background = setupEnterNextDrawableStateList()
            radius     = resources.getDimension(R.dimen.sign_process_radius_r07)
            elevation  = resources.getDimension(R.dimen.button_elevation_e00)
        }

        _frgBind.securityCodeBackTheButton.apply {
            imageTintList = ControllerWrapper.setupBackButtonTint()
        }

        checkButtonState(false)
    }

    private fun addListeners(){
        _frgBind.securityCodeButtonNext.setOnClickListener { btn ->
            btn.hideKeyboard()
            _frgListener.resultActionListener(OperationResultEvents.EventEnterReceiverCode(getCode()))
        }

        _frgBind.securityCodeBackTheButton.setOnClickListener { btn ->
            btn.hideKeyboard()
            _frgListener.resultActionListener(OperationResultEvents.EventPopBackStack)
        }

        _frgBind.securityCodeEnteredContainer.children.forEachIndexed {pos, cell ->
            cell.setOnFocusChangeListener { view, hasFocus ->
                _frgBind.securityCodeEnteredContainer.children.forEach { cell -> (cell as EditText).setTextColor(textColPrimary) }
                (view as EditText).setTextColor(Color.parseColor("#ffffff"))  //(textEnterCodeColor)
            }
            (cell as AppCompatEditText).addTextChangedListener( object: TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {logI("before")}
                override fun afterTextChanged(s: Editable?) {logI("after")}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    if(pos == 0) {
                        // code for serve first symbol
                        if(count == 1 && before == 0){
                            // to next pos
                            _frgBind.securityCodeEnteredContainer.getChildAt(pos+1).requestFocus()
                        }
                    }else if(pos == _frgBind.securityCodeEnteredContainer.childCount -1){
                        if(count == 0 && before == 1){
                            // to before position
                            _frgBind.securityCodeEnteredContainer.getChildAt(pos-1).requestFocus()
                            checkButtonState(false)
                        }else{
                            checkButtonState(true)
                        }
                    }else{
                        //code for serve end symbol
                        if(count == 1 && before == 0){
                            // to next pos
                            _frgBind.securityCodeEnteredContainer.getChildAt(pos+1).requestFocus()
                        }
                        if(count == 0 && before == 1){
                            // to before position
                            _frgBind.securityCodeEnteredContainer.getChildAt(pos-1).requestFocus()
                        }
                    }
                }
            })
        }
//        { lis ->
//            val myClip: ClipData?
//            val clipboard: ClipboardManager? = requireContext()
//                .getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
//            myClip = clipboard?.primaryClip!!
//            val txt = myClip.getItemAt(0).text
//            _frgBind.securityCodeEnteredContainer.children.forEachIndexed{indx, vw ->
//                (vw as EditText).enterSymbol(txt[indx].toString())
//            }
//            true
//        }
        _frgBind.securityCodeEnteredContainer.setOnLongClickListener(longCopyCode())
        _frgBind.optionsEnteredCodePosition01.setOnLongClickListener(longCopyCode())
        _frgBind.optionsEnteredCodePosition02.setOnLongClickListener(longCopyCode())
        _frgBind.optionsEnteredCodePosition03.setOnLongClickListener(longCopyCode())
        _frgBind.optionsEnteredCodePosition04.setOnLongClickListener(longCopyCode())
        _frgBind.optionsEnteredCodePosition05.setOnLongClickListener(longCopyCode())
        _frgBind.optionsEnteredCodePosition06.setOnLongClickListener(longCopyCode())
    }

    private fun longCopyCode() = View.OnLongClickListener { blk ->
        val myClip: ClipData?
        val clipboard: ClipboardManager? = requireContext()
            .getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
        myClip = clipboard?.primaryClip!!
        _frgBind.securityCodeEnteredContainer.children.forEachIndexed{indx, vw ->
            (vw as EditText).enterSymbol(myClip.getItemAt(0).text[indx].toString())
        }
        true
    }

    private fun setupFont(){
        context?.run {
            val typeFace = Typeface.createFromAsset(this.assets, fontAssetsPath)
            _frgBind.stepNameSecurityCode.typeface    = typeFace
            _frgBind.securityCodeLabelInfo.typeface   = typeFace
            _frgBind.securityCodePhoneInfo.typeface   = typeFace
            _frgBind.securityCodeReceive.typeface     = typeFace
            _frgBind.securityResendCode.typeface      = typeFace
            _frgBind.securityCodeButtonLabel.typeface = typeFace
            _frgBind.securityCodeTermsPolicy.typeface = typeFace

            _frgBind.securityCodeEnteredContainer.children.forEach { itemView ->
                (itemView as AppCompatEditText).typeface = typeFace
            }
        }
    }

    private fun setupEnterCodeDrawableState(): StateListDrawable {
        val gradientUnFocusedColors = intArrayOf(codeColorFocusNo, codeColorFocusNo)
        val gradientUnFocused = GradientDrawable(
            GradientDrawable.Orientation.LEFT_RIGHT,
            gradientUnFocusedColors)

        val gradientOkFocusedColors = intArrayOf(codeColorFocusOk, codeColorFocusOk)
        val gradientOkFocused = GradientDrawable(
            GradientDrawable.Orientation.LEFT_RIGHT,
            gradientOkFocusedColors)

        val figureRadius = context?.run{
            resources.getDimension(R.dimen.sign_process_radius_r07)
        }?: 0F

        gradientUnFocused.cornerRadius = figureRadius
        gradientOkFocused.cornerRadius = figureRadius

        val stateList = StateListDrawable()
        stateList.run {
            addState(
                intArrayOf(-android.R.attr.state_focused),
                gradientUnFocused
            )
            addState(
                intArrayOf(android.R.attr.state_focused),
                gradientOkFocused
            )
        }
        return stateList
    }

    private fun setupEnterNextDrawableStateList() : StateListDrawable {
        val gradientDisabledColors = intArrayOf(textColLight, textColLight)
        val gradientDisabled = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientDisabledColors)

        val gradientUnPressedColors = intArrayOf(darkColor, lightColor)
        val gradientUnPressed = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientUnPressedColors)

        val gradientOkPressedColors = intArrayOf(lightColor, darkColor)
        val gradientOkPressed = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientOkPressedColors)

        val figureRadius = context?.run{
            resources.getDimension(R.dimen.sign_process_radius_r07)
        }?: 0F

        gradientDisabled .cornerRadius = figureRadius
        gradientUnPressed.cornerRadius = figureRadius
        gradientOkPressed.cornerRadius = figureRadius

        val stateList = StateListDrawable()
        stateList.run {
            addState(
                intArrayOf(-android.R.attr.state_enabled),
                gradientDisabled
            )
            addState(
                intArrayOf(android.R.attr.state_pressed),
                gradientUnPressed
            )
            addState(
                intArrayOf(-android.R.attr.state_pressed),
                gradientOkPressed
            )
        }
        return stateList
    }


    private fun getCode(): String {
        var symbols =""
        _frgBind.securityCodeEnteredContainer.children.forEach { symbol ->
           symbols = symbols + (symbol as EditText).text.toString()
        }
        return symbols
    }

    private fun checkButtonState(isEnabled: Boolean){
        _frgBind.securityCodeButtonNext.isEnabled = isEnabled
    }

    override fun onStart() {
        if((requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager).primaryClip != null){
            val myClip = (requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?)?.primaryClip!!
            if(myClip.itemCount > 0){
                if(myClip.getItemAt(0).text.length == 6){
                    _frgBind.securityCodeEnteredContainer.forEachIndexed { indx, vw ->
                        (vw as EditText).enterSymbol(myClip.getItemAt(0).text[indx].toString())
                    }
                }
            }
        }
        super.onStart()
    }

    override fun onBackPressed(){
        _frgListener.resultActionListener(OperationResultEvents.EventPopBackStack)
    }

    private fun addResourceLogo(resId: Int){
        _resId = resId
    }

    private fun addListener(listener: FragmentListener){
        _frgListener = listener
    }

}