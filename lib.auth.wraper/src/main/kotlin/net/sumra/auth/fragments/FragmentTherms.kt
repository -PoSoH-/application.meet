package net.sumra.auth.fragments

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import net.sumra.auth.FragmentListener
import net.sumra.auth.states.OperationResultEvents
import net.sumra.auth.databinding.FragmentThermsBinding
import android.content.res.ColorStateList

import android.graphics.drawable.*
import net.sumra.auth.R

import android.graphics.drawable.ColorDrawable

import android.graphics.drawable.StateListDrawable
import kotlin.properties.Delegates


class FragmentTherms(/*val resId: Int,  val frgListener: FragmentListener*/): Fragment() {

    companion object{
        fun getInstance(
            logoResId: Int,
            checked  : Int,
            frgListener: FragmentListener,
            lightColor: Int,       //  = Color.parseColor("#FFCA8F31")
            darkColor : Int,       //  = Color.parseColor("#FFFFAF1A")

            trackEnable : Int,     // = Color.parseColor("#FFCA8F31")
            trackDisable: Int,     // = Color.parseColor("#FFCA8F31")
            thumbEnable : Int,     // = Color.parseColor("#FFCA8F31")
            thumbDisable: Int,     // = Color.parseColor("#FFCA8F31")

            textColPrimDark : Int, // = Color.parseColor("#FF4C4E5A")
            textColNormDark : Int, // = Color.parseColor("#FF4C4E5A")
            textColPrimary  : Int, // = Color.parseColor("#FF4C4E5A")
            textColLight    : Int, // = Color.parseColor("#FF4C4E5A")
            textColSelect   : Int, // = Color.parseColor("#FF4C4E5A")
            fontAssetsPath  : String?,
            backgroundColor : Int
        ): Fragment {
            return Builder().apply {
                resId = logoResId
                _checked = checked
                listener = frgListener

                _lightColor = lightColor
                _darkColor  = darkColor

                _trackEnable  = trackEnable
                _trackDisable = trackDisable
                _thumbEnable  =  thumbEnable
                _thumbDisable =  thumbDisable

                _textColPrimDark  = textColPrimDark
                _textColNormDark  = textColNormDark
                _textColPrimary   = textColPrimary
                _textColLight     = textColLight
                _textColSelect    = textColSelect
                _fontAssetsPath   = fontAssetsPath
                _backgroundColor  = backgroundColor
            }.build()
        }
    }

    private lateinit var _listener: FragmentListener
    private var _resLogo : Int? = null
    private var checked  : Int? = null
    private lateinit var _frgBind : FragmentThermsBinding

    private var lightColor: Int  = Color.parseColor("#FFCA8F31")
    private var darkColor : Int  = Color.parseColor("#FFFFAF1A")

    private var trackEnable : Int  = Color.parseColor("#FFCA8F31")
    private var trackDisable: Int  = Color.parseColor("#FFCA8F31")
    private var thumbEnable : Int  = Color.parseColor("#FFCA8F31")
    private var thumbDisable: Int  = Color.parseColor("#FFCA8F31")

    private var textColPrimDark  = Color.parseColor("#FF4C4E5A")
    private var textColNormDark  = Color.parseColor("#FF4C4E5A")
    private var textColPrimary   = Color.parseColor("#FF4C4E5A")
    private var textColLight     = Color.parseColor("#FF4C4E5A")
    private var textColSelect    = Color.parseColor("#FF4C4E5A")

    private var fontAssetsPath : String? = null

    private var backgroundColor : Int = Color.parseColor("#FFFFFFFF")

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _frgBind = FragmentThermsBinding.inflate(layoutInflater)

        setupView()
        setupCustomFont()
        initView()
        addListeners()

        checkThermsControl()

        return _frgBind.root
//        return super.onCreateView(inflater, container, savedInstanceState)
    }

    private fun initView(){

        _frgBind.stepNameTerms.text = HtmlCompat.fromHtml(
            """<span><b><font color = ${String.format("#%06X", (0xffffff and textColPrimDark))}>
            |${resources.getString(R.string.therms_private_title)}
            |</font></b></span>""".trimMargin(),
                HtmlCompat.FROM_HTML_MODE_COMPACT)

        val textContacts = StringBuilder("<span><b><font color=${String.format("#%06X", (0xffffff and textColPrimDark))}>${resources.getString(
            R.string.therms_private_contact_500_gray)}</font></b>")
            .append("<font color=${String.format("#%06X", (0xffffff and textColNormDark))}>")
            .append(" ${resources.getString(R.string.therms_private_contact_400_gray)}")
            .append("</font>")
            .append("<b><font color=${String.format("#%06X", (0xffffff and textColSelect))}> ${resources.getString(R.string.therms_private_contact_600_gold)}</font></b>")
            .append("<b><font color=${String.format("#%06X", (0xffffff and textColSelect))}> ${resources.getString(R.string.therms_private_contact_600_gray)}</font></b></span>")

        val textUpdates = """<span><b><font color=${String.format("#%06X", (0xffffff and textColPrimDark))}>
            | ${resources.getString(R.string.therms_private_spend_600_gray)}</font></b>
            |<font color=${String.format("#%06X", (0xffffff and textColNormDark))}>
            | ${resources.getString(R.string.therms_private_spend_400_gray)}</font></span>""".trimMargin()

        val textUnlimited = """<span><b>
            |<font color=${String.format("#%06X", (0xffffff and textColNormDark))}>
            |${resources.getString(R.string.therms_checked_unlimited_600_gray)}</font></b>
            |<b><font color=${String.format("#%06X", (0xffffff and textColSelect))}>
            |${resources.getString(R.string.therms_checked_unlimited_600_gold)}</font></b>
            |<font color=${String.format("#%06X", (0xffffff and textColNormDark))}>
            |${resources.getString(R.string.therms_checked_unlimited_400_norm)}</font></span>""".trimMargin()

        val textExchange = """<span><b>
            |<font color=${String.format("#%06X", (0xffffff and textColNormDark))}>
            |${resources.getString(R.string.therms_checked_exchange_600_gray)}</font></b>
            |<b><font color=${String.format("#%06X", (0xffffff and textColSelect))}>
            | ${resources.getString(R.string.therms_checked_exchange_600_gold)}</font></b></span>""".trimMargin()

        val textBottom = """<span><p><b>
            |<font color=${String.format("#%06X", (0xffffff and textColLight))}>
            |${resources.getString(R.string.therms_private_policy_all)}</font></b></p>
            |<p><b><font color=${String.format("#%06X", (0xffffff and textColSelect))}>
            |${resources.getString(R.string.therms_private_policy)}</font></b></p></span>""".trimMargin()

        _frgBind.thermsContacts.text = HtmlCompat
            .fromHtml(textContacts.toString(), HtmlCompat.FROM_HTML_MODE_COMPACT)
        _frgBind.thermsUpdates.text = HtmlCompat
            .fromHtml(textUpdates, HtmlCompat.FROM_HTML_MODE_COMPACT)
        _frgBind.thermsUnlimited.text = HtmlCompat
            .fromHtml(textUnlimited, HtmlCompat.FROM_HTML_MODE_COMPACT)
        _frgBind.thermsExchange.text = HtmlCompat
            .fromHtml(textExchange, HtmlCompat.FROM_HTML_MODE_COMPACT)
        _frgBind.thermsBottomHelp.text = HtmlCompat
            .fromHtml(textBottom, HtmlCompat.FROM_HTML_MODE_COMPACT)
    }

    private fun setupCustomFont(){
        context?.run{
            fontAssetsPath?.let{ path ->
                val face = Typeface.createFromAsset(this.assets, path)
                _frgBind.stepNameTerms   .typeface = face
                _frgBind.thermsContacts  .typeface = face
                _frgBind.thermsUpdates   .typeface = face
                _frgBind.thermsUnlimited .typeface = face
                _frgBind.thermsExchange  .typeface = face
                _frgBind.thermsBottomHelp.typeface = face
                _frgBind.termsButtonLabel.typeface = face
            }
        }
    }

    private fun setupView(){
        val res = StateListDrawable().apply {
            setExitFadeDuration(250)
            addState(intArrayOf(android.R.attr.state_checked), ColorDrawable(textColLight))
            addState(intArrayOf(android.R.attr.state_checked), ColorDrawable(darkColor))
        }

        val track = ColorStateList(
            arrayOf(intArrayOf(android.R.attr.state_enabled, -android.R.attr.state_checked), intArrayOf(android.R.attr.state_enabled, android.R.attr.state_checked)),
            intArrayOf(trackDisable, trackEnable))

        val thumb = ColorStateList(
            arrayOf(intArrayOf(android.R.attr.state_enabled, -android.R.attr.state_checked), intArrayOf(android.R.attr.state_enabled, android.R.attr.state_checked)),
            intArrayOf(thumbDisable, thumbEnable))

        _frgBind.thermsSwitchContacts.trackTintList = track
        _frgBind.thermsSwitchContacts.thumbTintList = thumb
        _frgBind.thermsSwitchUpdates.trackTintList  = track
        _frgBind.thermsSwitchUpdates.thumbTintList  = thumb

        _frgBind.thermsIndicatorExchange .setImageResource(checked!!)
        _frgBind.thermsIndicatorUnlimited.setImageResource(checked!!)

        /****************************************************************************************
        * dynamic color state list for material design concept
        *****************************************************************************************/

//        val selector = ColorStateList(
//            arrayOf(
//                /*disabled*/ intArrayOf(-android.R.attr.state_enabled, -android.R.attr.state_pressed),
//                /*enabled */ intArrayOf(+android.R.attr.state_enabled, -android.R.attr.state_pressed),
//                /*pressed */ intArrayOf(+android.R.attr.state_enabled, +android.R.attr.state_pressed)),
//            intArrayOf(textColLight, lightColor, darkColor))
//        _frgBind.thermsButtonConfirmation.backgroundTintList = selector

        /****************************************************************************************
        * dynamic GradientColor state list for material design concept
        *****************************************************************************************/

        val gradientDisabledColors = intArrayOf(textColLight, textColLight)
        val gradientDisabled = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientDisabledColors)

        val gradientUnPressedColors = intArrayOf(darkColor, lightColor)
        val gradientUnPressed = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientUnPressedColors)

        val gradientOkPressedColors = intArrayOf(lightColor, darkColor)
        val gradientOkPressed = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientOkPressedColors)

        context?.run{
            gradientDisabled .cornerRadius = resources.getDimension(R.dimen.sign_process_radius_r07)
            gradientUnPressed.cornerRadius = resources.getDimension(R.dimen.sign_process_radius_r07)
            gradientOkPressed.cornerRadius = resources.getDimension(R.dimen.sign_process_radius_r07)
        }

        val stateList = StateListDrawable()
        stateList.run {
            addState(
                intArrayOf(-android.R.attr.state_enabled),
                gradientDisabled
            )
            addState(
                intArrayOf(android.R.attr.state_pressed),
                gradientUnPressed
            )
            addState(
                intArrayOf(-android.R.attr.state_pressed),
                gradientOkPressed
            )
        }

        context?.run {
            _frgBind.thermsButtonConfirmation.run{
                background = stateList
                radius = resources.getDimension(R.dimen.sign_process_radius_r07)
                elevation = resources.getDimension(R.dimen.button_elevation_e00)
            }
        }

        _resLogo?.run{
            _frgBind.thermsLogo.setImageResource(this)
        }

        _frgBind.icInputTerms.visibility = View.GONE

    }

    private fun addListeners(){
        _frgBind.thermsSwitchContacts
            .setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
                override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                checkThermsControl()
            }
        })
        _frgBind.thermsSwitchUpdates
            .setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
                override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                    checkThermsControl()
                }
            })
        _frgBind.thermsButtonConfirmation
            .setOnClickListener(object : View.OnClickListener{
                override fun onClick(v: View?) {
                    //TODO("Not yet implemented")
                    //update
                    _listener.resultActionListener(OperationResultEvents.EventThermsSuccess)
                }
            })
    }

    private fun checkThermsControl(){
        if(_frgBind.thermsSwitchContacts.isChecked && _frgBind.thermsSwitchUpdates.isChecked)
             _frgBind.thermsButtonConfirmation.isEnabled = true
        else _frgBind.thermsButtonConfirmation.isEnabled = false
    }

    fun addResource(resId: Int){
        _resLogo = resId
    }

    fun addListener(listener: FragmentListener){
        _listener = listener
    }

    class Builder {
        internal var resId: Int? = null
        internal var listener: FragmentListener? = null
        internal var _lightColor by Delegates.notNull<Int>()
        internal var _darkColor  by Delegates.notNull<Int>()

        internal var _trackEnable  by Delegates.notNull<Int>()
        internal var _trackDisable by Delegates.notNull<Int>()
        internal var _thumbEnable  by Delegates.notNull<Int>()
        internal var _thumbDisable by Delegates.notNull<Int>()

        internal var _textColPrimDark by Delegates.notNull<Int>()
        internal var _textColNormDark by Delegates.notNull<Int>()
        internal var _textColPrimary  by Delegates.notNull<Int>()
        internal var _textColLight    by Delegates.notNull<Int>()
        internal var _textColSelect   by Delegates.notNull<Int>()
        internal var _fontAssetsPath : String? = null

        internal var _checked: Int? = null

        internal var _backgroundColor by Delegates.notNull<Int>()

        fun build(): FragmentTherms {
            val frg = FragmentTherms()
            frg.addResource(resId!!)
            frg.addListener(listener!!)

            frg.lightColor = _lightColor
            frg.darkColor  = _darkColor

            frg.thumbEnable  = _thumbEnable
            frg.thumbDisable = _thumbDisable
            frg.trackEnable  = _trackEnable
            frg.trackDisable = _trackDisable

            frg.textColPrimDark = _textColPrimDark
            frg.textColPrimary  = _textColPrimary
            frg.textColNormDark = _textColNormDark
            frg.textColLight    = _textColLight
            frg.textColSelect   = _textColSelect
            frg.fontAssetsPath  = _fontAssetsPath

            frg.checked         = _checked
            frg.backgroundColor = _backgroundColor
            return frg
        }
    }

}

