package net.sumra.auth.fragments

import android.content.Context
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import net.sumra.auth.helpers.logI

abstract class FragmentBase: Fragment() {

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    logI ("BackPressed -> Click")
                    onBackPressed()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    abstract fun onBackPressed() //     logI("On back pressed clicked... ")
}