package net.sumra.auth.fragments

import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import net.sumra.auth.ControllerWrapper
import net.sumra.auth.FragmentListener
import net.sumra.auth.R
import net.sumra.auth.databinding.FragmentContactsReferralBinding
import net.sumra.auth.states.OperationResultEvents

class FragmentReferral: FragmentBase(){

    companion object{
        fun getInstance(resId: Int, frgListener: FragmentListener, adapter: ControllerWrapper.AdapterContact): Fragment {
            return Builder().apply {
                this.resourceLogo = resId
                this.listener = frgListener
                this.adapter = adapter
            }.build()
        }
    }

    private class Builder() {
        var resourceLogo: Int? = null
        var listener: FragmentListener? = null
        var adapter: ControllerWrapper.AdapterContact? = null
        fun build(): FragmentReferral {
            val frg = FragmentReferral()
            resourceLogo?.run{
                frg.addResourceLogo(this)
            }
            listener?.run {
                frg.addFragmentListeners(this)
            }
            adapter?.run {
                frg.addAdapter(this)
            }
            return frg
        }
    }

    private var _resILogo: Int? = null
    private lateinit var _frgListener: FragmentListener
    private lateinit var _adapter: ControllerWrapper.AdapterContact
    private lateinit var _frgBind: FragmentContactsReferralBinding

    override fun onBackPressed() {
        /*TODO("if button show to screen this logic for previous fragment")*/
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _frgBind = FragmentContactsReferralBinding.inflate(layoutInflater)
        setupUI()
        addListeners()
        return _frgBind.root
    }

    private fun setupUI(){
//         Name contacts page color ant text setup
        _frgBind.referralsToolbarLabel.text = HtmlCompat.fromHtml(
            """<span><b><font color = ${String.format("#%06X", (0xffffff and ControllerWrapper.textColPrimDark))}>
                |${resources.getString(R.string.referrals_label)}
                |</font></b></span>""".trimMargin(),
            HtmlCompat.FROM_HTML_MODE_COMPACT)
//         Share Sumra referrals info
        _frgBind.contactReferralInformation.text = HtmlCompat.fromHtml(
            """<span>
                |<font color = ${String.format("#%06X", (0xffffff and ControllerWrapper.textColPrimary))}>
                |${resources.getString(R.string.referral_label_part_one)}
                |</font>
                |<b><font color = ${String.format("#%06X", (0xffffff and ControllerWrapper.textColSelect))}>
                | ${resources.getString(R.string.referral_label_part_two)}
                |</font></b>
                |<b><font color = ${String.format("#%06X", (0xffffff and ControllerWrapper.textColPrimary))}>
                | ${resources.getString(R.string.referral_label_part_three)}
                |</font></b>
                |</span>""".trimMargin(),
            HtmlCompat.FROM_HTML_MODE_COMPACT)
//        Help hint ant text for
        _frgBind.contactReferralInformation.apply {
            setHintTextColor(ControllerWrapper.textColLight)
            setTextColor    (ControllerWrapper.textColPrimary)
            hint = resources.getString(R.string.referrals_enter_name_for_search)
        }
//        Setup text button submit all
        _frgBind.referralsSubmitAllLabel.text = HtmlCompat.fromHtml(
            """<span>
                |<b><font color = ${String.format("#%06X", (0xffffff and Color.WHITE))}>
                |${resources.getString(R.string.referral_button_all)}
                |</font></b>
                |</span>""".trimMargin(),
            HtmlCompat.FROM_HTML_MODE_COMPACT)
//        Setup text button submit selected
        _frgBind.referralsSubmitSelectLabel.text = HtmlCompat.fromHtml(
            """<span>
                |<b><font color = ${String.format("#%06X", (0xffffff and Color.WHITE))}>
                |${resources.getString(R.string.referral_button_submit)}
                |</font></b>
                |</span>""".trimMargin(),
            HtmlCompat.FROM_HTML_MODE_COMPACT)
        // Setup search the button enable/disable state colors list

        _frgBind.buttonSearchProcess.apply {
            buttonTintList = ControllerWrapper.setupSearchButtonTint()
        }

        _frgBind.buttonSubmitAllContacts.apply {
            radius = 0F
            background = ControllerWrapper.setupSelectorButtonNotRadius(context, true)
        }

        _frgBind.buttonSubmitSelectContacts.apply {
            radius = 0F
            background = ControllerWrapper.setupSelectorButtonNotRadius(context, false)
        }

        // Setup search the button enable/disable state colors list
        _frgBind.referralContactList.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = _adapter
        }

        updateSearchView(false)
    }

    private fun addListeners(){
        _frgBind.buttonSubmitAllContacts.setOnClickListener    { button->
            _frgListener.resultActionListener(OperationResultEvents
                .EventSubmitContacts(_adapter.getContactsAll()))
        }

        _frgBind.buttonSubmitSelectContacts.setOnClickListener { button ->
            _frgListener.resultActionListener(OperationResultEvents
                .EventSubmitContacts(_adapter.getContactsSelected()))
        }

        _frgBind.buttonSearchProcess.setOnCheckedChangeListener { view, check ->
            updateSearchView(check)
        }

        _frgBind.referralEnterSearch.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                /*TODO("Not yet implemented")*/
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(count >= 3){
                    _adapter.updateListByFilter(s.toString())
                }else if(count<3){
                    _adapter.updateListByFilter("")
                }
            }

            override fun afterTextChanged(s: Editable?) {
                /*TODO("Not yet implemented")*/
            }
        })
    }

    private fun updateSearchView(check: Boolean){
        when(check){
            true -> {
                _frgBind.toolbarSeparateLineOne.visibility = View.VISIBLE
                _frgBind.referralEnterSearch.visibility = View.VISIBLE
            }
            else -> {
                _frgBind.toolbarSeparateLineOne.visibility = View.GONE
                _frgBind.referralEnterSearch.visibility = View.GONE
            }
        }
    }

    private fun addResourceLogo(resource: Int){
        _resILogo = resource
    }

    private fun addFragmentListeners(listener: FragmentListener){
        _frgListener = listener
    }

    private fun addAdapter(adapter: ControllerWrapper.AdapterContact){
        _adapter = adapter
    }

}