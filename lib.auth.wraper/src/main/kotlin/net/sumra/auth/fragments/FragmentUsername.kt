package net.sumra.auth.fragments

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.StateListDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import net.sumra.auth.ControllerWrapper
import net.sumra.auth.FragmentAction
import net.sumra.auth.FragmentListener
import net.sumra.auth.R
import net.sumra.auth.databinding.FragmentUsernameBinding
import net.sumra.auth.helpers.*
import net.sumra.auth.states.ActionEvents
import net.sumra.auth.states.OperationResultEvents

//@Parcelize
//data class AddReceiverCode(val code: String): Parcelable

class FragmentUsername: FragmentBase(), FragmentAction {

    companion object{
        fun getInstance(resId: Int, authCode: String, sidCode: String, frgHandler: FragmentListener): Fragment {
            return Builder().apply {
                this.resId = resId
                this.authCode = authCode
                this.sidCode = sidCode
                this.handler = frgHandler
            }.build()
        }
    }

    internal class Builder{
        internal var resId: Int? = null
        internal var authCode: String? = null
        internal var sidCode: String? = null
        internal var handler: FragmentListener? = null

        internal fun build(): FragmentUsername {
            val fragment = FragmentUsername()
            fragment.addResourceLogo(resId!!)
            fragment.addAuthCode(authCode!!)
            fragment.addSidCode(sidCode!!)
            fragment.addListener    (handler!!)
            return fragment
        }
    }

    private var _resId                : Int? = null
    private lateinit var _frgListener : FragmentListener
    private lateinit var _frgBind     : FragmentUsernameBinding
    private val usernameOk            = false

    private lateinit var _authCode: String      // , AddReceiverCode by libBundle.libraryArgs()
    private lateinit var _sidCode : String      // , AddReceiverCode by libBundle.libraryArgs()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is FragmentListener){
            logI("Inject FragmentListener SUCCESS")
        }else{
            logE("Inject FragmentListener FAILURE")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _frgBind = FragmentUsernameBinding.inflate(layoutInflater)
        setupFont()
        setupUI()
        addListeners()
        return _frgBind.root
    }

    private fun setupUI(){

    /** Global setup views and buttons ========================================================= **/

        _frgBind.enteredUsername.text?.run{
            if(length >= 8) {
                switchButtonChecked(true)
            }else{
                switchButtonChecked(false)
            }
        }

        _resId?.run {
            _frgBind.userNameEnterLogo.setImageResource(this)
        }?:{
            _frgBind.userNameEnterLogo.visibility = View.GONE
        }

        _frgBind.userNameButtonIcon.visibility = View.GONE

        if(!usernameOk)_frgBind.userNameButtonConfirmation.isEnabled = false
        else           _frgBind.userNameButtonConfirmation.isEnabled = true

    /** Setup texts block for insert texts, weight and color setup============================== **/

        _frgBind.userNameLabel.text = HtmlCompat.fromHtml("""<span>
            |<b><font color = ${String.format("#%06X", (0xffffff and ControllerWrapper.textColPrimDark))}>
            |${resources.getString(R.string.username_label)}
            |</font></b>
            |</span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)

        _frgBind.userNameLabelHelp.text = HtmlCompat.fromHtml("""<span>
            |<font color = ${String.format("#%06X", (0xffffff and ControllerWrapper.textColPrimary))}>
            |${resources.getString(R.string.username_help)}
            |</font>
            |</span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)

        _frgBind.userNameEntered.apply {
            setHintTextColor ( ControllerWrapper.textColPrimary )
            setTextColor     ( ControllerWrapper.textColPrimary )
            hint = resources.getString(R.string.username_hint)

            background = setupSelectorEditUsername()
        }

        _frgBind.userNameButtonText.text = HtmlCompat.fromHtml("""<span>
            |<b><font>
            |${resources.getString(R.string.button_confirmation_username)}
            |</font></b>
            |</span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)

        _frgBind.userNameTermsPolicy.text = HtmlCompat.fromHtml("""<span>
            |<p><b><font color=${String.format("#%06X", (0xffffff and ControllerWrapper.textColLight))}>
            |${resources.getString(R.string.therms_private_policy_all)}
            |</font></b></p>
            |<p><b><font color=${String.format("#%06X", (0xffffff and ControllerWrapper.textColSelect))}>
            |${resources.getString(R.string.therms_private_policy)}
            |</font></b></p>
            |</span>""".trimMargin(), HtmlCompat.FROM_HTML_MODE_COMPACT)

        _frgBind.userNameButtonConfirmation.apply {
            radius = resources.getDimension(R.dimen.sign_process_radius_r07)
            elevation = resources.getDimension(R.dimen.button_elevation_e00)
            background = setupSelectorButtonSignIn()
        }
        _frgBind.userNameBackTheButton.apply {
            isFocusableInTouchMode = true
            imageTintList = ControllerWrapper.setupBackButtonTint()
            requestFocus()
        }
    }

    private fun addListeners(){
        _frgBind.userNameBackTheButton.setOnClickListener { btn ->
            btn.hideKeyboard()
            toBackStack()
        }

        _frgBind.userNameButtonConfirmation.setOnClickListener { btn ->
            btn.hideKeyboard()
            _frgListener.resultActionListener(OperationResultEvents.EventAuthUp(
                candidateName = _frgBind.userNameEntered.text.toString(),
                authCode  = _authCode,
                sidCode   = _sidCode))
        }

        _frgBind.userNameEntered.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {/*TODO("Not yet implemented"*/}
            override fun afterTextChanged(s: Editable?) { /*TODO("Not yet implemented")*/ }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                _frgBind.usernamePictureFailure.visibility = View.GONE
                _frgBind.usernamePictureSuccess.visibility = View.GONE
                if(count >= Finals.min_username_symbols) {
                    switchButtonChecked(true)
                }else {
                    switchButtonChecked(false)
                }
            }
        })
    }

    private fun setupFont(){
        context?.run {
            ControllerWrapper.fontAssetsPath?.let { font ->
                val typeFace = Typeface.createFromAsset(this.assets, font)
                _frgBind.userNameLabel.typeface = typeFace
                _frgBind.userNameLabelHelp.typeface = typeFace
                _frgBind.userNameEntered.typeface = typeFace
                _frgBind.userNameButtonText.typeface = typeFace
                _frgBind.userNameTermsPolicy.typeface = typeFace
            }
        }
    }

    private fun setupSelectorEditUsername(): StateListDrawable {
        val gradientUnFocusedColors = intArrayOf(Color.parseColor("#ffffff"), Color.parseColor("#ffffff"))
        val gradientUnFocused = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientUnFocusedColors)

        val gradientOkFocusedColors = intArrayOf(ControllerWrapper.lightColor, ControllerWrapper.lightColor)
        val gradientOkFocused = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientOkFocusedColors)

        val figureRadius = context?.run{
            resources.getDimension(R.dimen.sign_process_radius_r07)
        }?: 0F

        gradientUnFocused.cornerRadius = figureRadius
        gradientOkFocused.cornerRadius = figureRadius
        gradientOkFocused.setStroke(resources.getDimension(R.dimen.phone_border_weight).toInt(), ControllerWrapper.darkColor)

        val stateList = StateListDrawable()
        stateList.run {
            addState(
                intArrayOf(-android.R.attr.state_focused),
                gradientUnFocused
            )
            addState(
                intArrayOf(android.R.attr.state_focused),
                gradientOkFocused
            )
        }
        return stateList
    }

    private fun setupSelectorButtonSignIn(): StateListDrawable {
        val gradientUnPressedColors = intArrayOf(ControllerWrapper.darkColor, ControllerWrapper.lightColor)
        val gradientUnPressed = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientUnPressedColors)

        val gradientOkPressedColors = intArrayOf(ControllerWrapper.lightColor, ControllerWrapper.darkColor)
        val gradientOkPressed = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientOkPressedColors)

        val figureRadius = context?.run{
            resources.getDimension(R.dimen.sign_process_radius_r07)
        }?: 0F

        gradientUnPressed.cornerRadius = figureRadius
        gradientOkPressed.cornerRadius = figureRadius

        val stateList = StateListDrawable()
        stateList.run {
            addState(
                intArrayOf(android.R.attr.state_pressed),
                gradientUnPressed
            )
            addState(
                intArrayOf(-android.R.attr.state_pressed),
                gradientOkPressed
            )
        }
        return stateList
    }

    override fun onBackPressed() {
        toBackStack()
    }

    private fun toBackStack() {
        _frgListener.resultActionListener(OperationResultEvents.EventPopBackStack)
    }

    private fun addResourceLogo(resId: Int){
        _resId = resId
    }

    private fun addListener(handler: FragmentListener){
        _frgListener = handler
    }

    private fun addSidCode(sidCode: String){
        _sidCode = sidCode
    }

    private fun addAuthCode(authCode: String){
        _authCode = authCode
    }

    private fun switchButtonChecked(isEnable: Boolean){
        when(isEnable){
            true -> _frgBind.userNameButtonConfirmation.isEnabled = true
            else -> _frgBind.userNameButtonConfirmation.isEnabled = false
        }
    }

    override fun updateFragmentByAction(result: ActionEvents) {
        if(result == ActionEvents.ActionEventsSuccess){
            _frgBind.userNameButtonConfirmation.isEnabled = true
        }else{
            _frgBind.usernamePictureFailure.visibility = View.VISIBLE
            _frgBind.usernamePictureSuccess.visibility = View.GONE
        }
    }
}
