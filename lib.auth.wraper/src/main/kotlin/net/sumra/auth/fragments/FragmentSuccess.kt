package net.sumra.auth.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.coroutines.*
import net.sumra.auth.FragmentListener
import net.sumra.auth.databinding.FragmentSignSuccessBinding
import net.sumra.auth.databinding.FragmentSumraIdBinding
import net.sumra.auth.states.OperationResultEvents

class FragmentSuccess: Fragment() {

    class Builder {
        lateinit var bindListener: FragmentListener
        fun build(): FragmentSuccess {
            val fragment = FragmentSuccess()
            fragment._frgListener = bindListener
            return fragment
        }
    }

    private lateinit var _frgListener: FragmentListener

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return FragmentSignSuccessBinding.inflate(layoutInflater).root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        CoroutineScope(Dispatchers.IO).launch{
            delay(1759)
            withContext(Dispatchers.Main){
                _frgListener.resultActionListener(OperationResultEvents.EventReferralsSendSuccess)
            }
        }

    }

}