package net.sumra.auth

import android.content.ContentUris
import android.content.Context
import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.StateListDrawable
import android.net.Uri
import android.os.Parcelable
import android.provider.ContactsContract
import android.provider.Settings
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.*
import kotlinx.parcelize.Parcelize
import net.app.sph.tst.scs.convertors.Convertor
import net.sumra.auth.data.remote.*
import net.sumra.auth.data.remote.failure.WrapperError
import net.sumra.auth.data.remote.models.*

import net.sumra.auth.databinding.ReferralContactCellBinding
import net.sumra.auth.fragments.*
import net.sumra.auth.helpers.*
import net.sumra.auth.models.AuthParameter
import net.sumra.auth.models.MappedContact
import net.sumra.auth.states.ActionEvents
import net.sumra.auth.states.MessageTypeEvents
import net.sumra.auth.states.OperationResultEvents
import okhttp3.OkHttpClient
import timber.log.Timber
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.lang.RuntimeException
import java.lang.StringBuilder
import java.util.*
import java.util.concurrent.CancellationException

class ControllerWrapper
    /* @AssistedInject constructor(
    *  private val defaultConnectService: ConnectService,
    *  private val context: Context)
    */
    : FragmentListener {

    class Builder() {
        var _logoResId       : Int?    = null
        var _congratsIconId  : Int?    = null
        var _identifyApp     : String? = null
        var _lightColor      : Int?    = null
        var _darkColor       : Int?    = null
        var _trackEnable     : Int?    = null
        var _trackDisable    : Int?    = null
        var _thumbEnable     : Int?    = null
        var _thumbDisable    : Int?    = null
        var _fontAssetsPath  : String? = null
        var _textColPrimDark : Int?    = null
        var _textColNormDark : Int?    = null
        var _textColPrimary  : Int?    = null
        var _textColLight    : Int?    = null
        var _textColSelect   : Int?    = null
        var _codeColorFocusNo: Int?    = null
        var _codeColorFocusOk: Int?    = null
        var _checkedFigure   : Int?    = null
        var _backgroundColor : Int?    = null
        var context          : Context?= null

        fun build(): ControllerWrapper {
            val controller = ControllerWrapper()
            _logoResId?.run        { logoResId = this }
            _congratsIconId?.run   { congratsIcon = this }
            _lightColor?.run       { lightColor = this }
            _darkColor?.run        { darkColor = this }
            _trackEnable?.run      { trackEnable = this }
            _trackDisable?.run     { trackDisable = this }
            _thumbEnable?.run      { thumbEnable = this }
            _thumbDisable?.run     { thumbDisable = this }
            _fontAssetsPath?.run   { fontAssetsPath = this }
            _textColPrimDark?.run  { textColPrimDark = this }
            _textColNormDark?.run  { textColNormDark = this }
            _textColPrimary?.run   { textColPrimary = this }
            _textColLight?.run     { textColLight = this }
            _textColSelect?.run    { textColSelect = this }
            _codeColorFocusNo?.run { codeColorFocusNo = this }
            _codeColorFocusOk?.run { codeColorFocusOk = this }
            _checkedFigure?.run {
                checked = this
                _backgroundColor?.run { backgroundColor = this }
            }
            _identifyApp?.run     { controller.identifyApp = this }
            context?.run {
                controller.context = this
                controller.preferences = getSharedPreferences(PREF, Context.MODE_PRIVATE)
                controller.defaultConnectService = DefaultConnectService(
                    okHttpClient = OkHttpClient(),
                    retrofitFactory = RetrofitFactory(Gson())
                )
                controller.deviceId =
                    Settings.Secure.getString(this.contentResolver!!, Settings.Secure.ANDROID_ID)
                if (context is ControllerListener) {
                    controller.updateState = this as ControllerListener
                } else {
                    throw ClassCastException("Not implement ControllerListener...")
                }
            }
            return controller
        }
    }

    companion object {
        internal val PREF: String = "pref-meet-private-saved-procedure"
        internal var checked          = -1
        internal var logoResId        = 0
        internal var lightColor       = Color.parseColor("#FFCA8F31")
        internal var darkColor        = Color.parseColor("#FFFFAF1A")
        internal var trackEnable      = Color.parseColor("#FFCA8F31")
        internal var trackDisable     = Color.parseColor("#FFCA8F31")
        internal var thumbEnable      = Color.parseColor("#FFCA8F31")
        internal var thumbDisable     = Color.parseColor("#FFCA8F31")
        internal var textColPrimDark  = Color.parseColor("#FF4C4E5A")
        internal var textColNormDark  = Color.parseColor("#FF4C4E5A")
        internal var textColPrimary   = Color.parseColor("#FF4C4E5A")
        internal var textColLight     = Color.parseColor("#FF4C4E5A")
        internal var textColSelect    = Color.parseColor("#FF4C4E5A")
        internal var codeColorFocusNo = Color.parseColor("#FFFFFFFF")
        internal var codeColorFocusOk = Color.parseColor("#FFAAAAAA")
        internal var congratsIcon     : Int? = null
        internal var fontAssetsPath   : String = Finals.FONT_FACE
        internal var backgroundColor  = Color.parseColor("#FFFFFFFF")

        internal fun setupSearchButtonTint() : ColorStateList {
            return ColorStateList(
                arrayOf(intArrayOf(android.R.attr.state_enabled, -android.R.attr.state_checked), intArrayOf(android.R.attr.state_enabled, android.R.attr.state_checked)),
                intArrayOf(textColLight, textColPrimDark))
        }

        internal fun setupBackButtonTint() : ColorStateList {
            return ColorStateList(
                arrayOf(intArrayOf(android.R.attr.state_enabled, -android.R.attr.state_pressed), intArrayOf(android.R.attr.state_enabled, android.R.attr.state_pressed)),
                intArrayOf(textColPrimary, textColNormDark))
        }

        internal fun setupCheckBoxTint() : ColorStateList {
            return ColorStateList(
                arrayOf(intArrayOf(android.R.attr.state_enabled, -android.R.attr.state_pressed), intArrayOf(android.R.attr.state_enabled, android.R.attr.state_pressed)),
                intArrayOf(darkColor, darkColor))
        }

        internal fun setupSelectorButtonContinue(context: Context): StateListDrawable {
            val stateList = StateListDrawable()
                val gradientUnPressedColors = intArrayOf(darkColor, lightColor)
                val gradientUnPressed =
                    GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientUnPressedColors)
                val gradientOkPressedColors = intArrayOf(lightColor, darkColor)
                val gradientOkPressed =
                    GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientOkPressedColors)
                val figureRadius = context.resources.getDimension(R.dimen.sign_process_radius_r07)
                gradientUnPressed.cornerRadius = figureRadius
                gradientOkPressed.cornerRadius = figureRadius
                stateList.run {
                    addState(intArrayOf(android.R.attr.state_pressed), gradientUnPressed)
                    addState(intArrayOf(-android.R.attr.state_pressed), gradientOkPressed)
                }
            return stateList
        }

        internal fun setupSelectorButtonNotRadius(
                                                   context: Context,
                                                   gradientLtoR: Boolean = false): StateListDrawable {
            val stateList = StateListDrawable()
            if ( gradientLtoR ) {
                val gradientUnPressedColors = intArrayOf(darkColor, lightColor)
                val gradientUnPressed =
                    GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientUnPressedColors)
                val gradientOkPressedColors = intArrayOf(lightColor, darkColor)
                val gradientOkPressed =
                    GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientOkPressedColors)
                val figureRadius = context.resources.getDimension(R.dimen.sign_process_radius_r07)
//                gradientUnPressed.cornerRadius = figureRadius
//                gradientOkPressed.cornerRadius = figureRadius
                stateList.run {
                    addState(intArrayOf(android.R.attr.state_pressed), gradientUnPressed)
                    addState(intArrayOf(-android.R.attr.state_pressed), gradientOkPressed)
                }
            } else {
                val gradientUnPressedColors = intArrayOf(lightColor, darkColor)
                val gradientUnPressed =
                    GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientUnPressedColors)
                val gradientOkPressedColors = intArrayOf(darkColor, lightColor)
                val gradientOkPressed =
                    GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientOkPressedColors)
                val figureRadius = context.resources.getDimension(R.dimen.sign_process_radius_r07)
//                gradientUnPressed.cornerRadius = figureRadius
//                gradientOkPressed.cornerRadius = figureRadius
                stateList.run {
                    addState(intArrayOf(android.R.attr.state_pressed), gradientUnPressed)
                    addState(intArrayOf(-android.R.attr.state_pressed), gradientOkPressed)
                }
            }
            return stateList
        }
    }

    private var deviceId            : String = ""
    private lateinit var context    : Context
    private lateinit var updateState: ControllerListener
    private lateinit var identifyApp: String

    private var action: FragmentAction? = null

    private lateinit var defaultConnectService: ConnectService
    private lateinit var preferences: SharedPreferences

    private lateinit var session : Session

    private var currOpt: Fragment? = null

    private val currentAuthConnect: ConnectAuthWizard?
            get() = defaultConnectService.getConnectAuthService()
    private val currentContactConnect: ConnectContactWizard?
            get() = defaultConnectService.getConnectContactService()

//    fun checkToken(): String{
//        //todo
//        //chek of saved token
//        return "5"
//    }

    fun checkFirstPage(): Fragment {
        if(!extReadThermsContactsState(preferences) && !extReadThermsUpdatesState(preferences))
            return FragmentTherms.getInstance(logoResId,
                checked = checked,
                this,
                lightColor = lightColor,
                darkColor  = darkColor,

                trackEnable  = trackEnable,
                trackDisable = trackDisable,
                thumbEnable  = thumbEnable,
                thumbDisable = thumbDisable,

                textColPrimDark = textColPrimDark,
                textColNormDark = textColNormDark,
                textColPrimary  = textColPrimary,
                textColLight    = textColLight,
                textColSelect   = textColSelect,
                fontAssetsPath  = fontAssetsPath,
                backgroundColor = backgroundColor)
        else {
            this.currOpt = FragmentOptions.getInstance(
                logoResId,
                this,

                lightColor = lightColor,
                darkColor = darkColor,

                textColPrimDark = textColPrimDark,
                textColNormDark = textColNormDark,
                textColPrimary = textColPrimary,
                textColLight = textColLight,
                textColSelect = textColSelect,
                fontAssetsPath = fontAssetsPath,
                backgroundColor = backgroundColor
            )
            return this.currOpt!!
        }
    }




    fun updateLogo(resId: Int){
        logoResId = resId
    }

    override fun resultActionListener(events: OperationResultEvents) {
        Timber.i("controller listener result :: fragment loaded")
        when(events){
            is OperationResultEvents.EventPopBackStack      -> {updateState.updateControllerState(
                fragment = null,
                addBack  = false,
                isBack   = true)}

            is OperationResultEvents.EventThermsSuccess     -> {
                extWriteThermsContactsState(true, preferences)
                extWriteThermsUpdatesState (true, preferences)

                if(currOpt == null){
                    currOpt = FragmentOptions.getInstance(
                        logoResId,
                        this,

                        lightColor = lightColor,
                        darkColor = darkColor,

                        textColPrimDark = textColPrimDark,
                        textColNormDark = textColNormDark,
                        textColPrimary = textColPrimary,
                        textColLight = textColLight,
                        textColSelect = textColSelect,
                        fontAssetsPath = fontAssetsPath,
                        backgroundColor = backgroundColor
                    )
                }

                updateState.updateControllerState(
                fragment = currOpt,
                addBack  = false,
                isBack   = false)
            }

            is OperationResultEvents.EventSelectedMessengers -> {
                Timber.i("${events.type.`package`}")
                updatePageByOptions(null)
            }
            is OperationResultEvents.EventSelectedPhone     -> {

                updateState.checkProgressState(true)
                CoroutineScope(Dispatchers.IO).launch {
                    try {
                        currentAuthConnect?.actionSharePhone(events.phoneNumber, identifyApp)
                    }catch (failure: Throwable){
                        if (failure !is CancellationException) {
                            Timber.e(failure)
                            withContext(Dispatchers.Main) {
                                updateState.sendMessageToSystem(
                                    type = MessageTypeEvents.MessageSimple(failure.message!!))}
                        }
                        null
                    }?.let { data ->
                        when ( data ) {
                            is RemoteResponse.Data -> {
                                withContext(Dispatchers.Main){
                                    /** show dialog window **/
                                    val dt = data.body as objectDataPhone.RSPPhone
                                    when (dt.success) {
                                        "success" -> {
                                            (currOpt as FragmentOptions).updateDialog(article = dt.message)
//                                    isPhoneInfo.value = true
                                            updateState.sendMessageToSystem(
                                                type = MessageTypeEvents.MessageSimple (dt.message))
                                        }
                                        "danger" -> {
                                            updateState.sendMessageToSystem(
                                            type = MessageTypeEvents.MessageSimple (dt.message))
                                        }
                                    }
                                }
                            }
                            is RemoteResponse.Fail -> {
                                withContext(Dispatchers.Main) {
                                    /** show error message **/
                                    updateState.sendMessageToSystem(
                                        type = MessageTypeEvents.MessageSimple(data.fail.toString())
                                    )
                                }
                            }
                        }
                    }
                    withContext(Dispatchers.Main) {
                        updateState.checkProgressState(false)
                    }
                }
//                updatePageByOptions(events.phoneNumber)
            }
            is OperationResultEvents.EventConfirmPhone      -> {
                updateState.checkProgressState(true)
                CoroutineScope(Dispatchers.IO).launch {
                    try {
                        currentAuthConnect?.actionSendSMS(events.phoneNumber, identifyApp)
                    }catch (failure: Throwable){
                        if (failure !is CancellationException) {
                            Timber.e(failure)
                            withContext(Dispatchers.Main) {
                                updateState.sendMessageToSystem(
                                    type = MessageTypeEvents.MessageSimple(failure.message!!))}
                        }
                        null
                    }?.let { data ->
                        when ( data ) {
                            is RemoteResponse.Data -> {
                                withContext(Dispatchers.Main){
                                    /** show dialog window **/
                                    val dt = data.body as objectDataPhone.RSP_SMS
                                    when (dt.type) {
                                        "success" -> {
//                                            isPhoneInfo.value = true
                                            updatePageByOptions(events.phoneNumber)

//                                            updateState.sendMessageToSystem(
//                                                type = MessageTypeEvents.MessageSimple (dt.message))
                                        }
                                        "danger" -> {
                                            updateState.sendMessageToSystem(
                                                type = MessageTypeEvents.MessageSimple (dt.message))
                                        }
                                    }
                                }
                            }
                            is RemoteResponse.Fail -> {
                                withContext(Dispatchers.Main) {
                                    /** show error message **/
                                    updateState.sendMessageToSystem(
                                        type = MessageTypeEvents.MessageSimple(data.fail.toString())
                                    )
                                }
                            }
                        }
                    }
                    withContext(Dispatchers.Main) {
                        updateState.checkProgressState(false)
                    }
                }
            }
            is OperationResultEvents.EventSelectedSumraId   -> {
                val temp = FragmentSumraID.Builder().also {
                    it.buildLogo = null //logoResId
                    it.buildListener = this
                }.build()
                action = temp
                updateState.updateControllerState(
                    fragment = temp,
                    addBack = true,
                    isBack = false
                )
            }

            is OperationResultEvents.EventSumraIdUsernameCheck ->{
                action?.run {
                    val temp = Convertor(context)
                    when(temp.authUserCheck(events.username.toString())) {
                        true -> updateFragmentByAction(ActionEvents.ActionEventsSuccess)
                        else -> updateFragmentByAction(ActionEvents.ActionEventsFailure)
                    }
                }
            }

            is OperationResultEvents.EventSumraIdLogin ->{
//                userAuthInProcess(userAuthReadParams())
                userAuthBySumraId(events)
            }

            is OperationResultEvents.EventEnterReceiverCode -> {
                actionSendCodeValidation(authCode = events.code)
//                val frg = FragmentUsername.getInstance(logoResId, events.code, this)
//                action = frg as FragmentAction
//                updateState.updateControllerState(
//                    fragment = frg,
////                    params = AddReceiverCode(result.code),
//                    addBack = true,
//                    isBack = false
//                )
            }

            is OperationResultEvents.EventCheckUsername     -> {
                updateState.checkProgressState(true)
                CoroutineScope(Dispatchers.IO).launch {
//                    val response = connect.checkUsername(events.candidate)
                    delay(2000)
//                    withContext(Dispatchers.Main){
//                        updateState.sendMessageToSystem(
//                            type = MessageTypeEvents.MessageSimple (response.toString()))
//                        action?.run{
//                            if(true) updateFragmentByAction(ActionEvents.ActionEventsSuccess)
//                            else     updateFragmentByAction(ActionEvents.ActionEventsFailure)
//                        }
//                        updateState.checkProgressState(false)
//                    }
                }
            }

            is OperationResultEvents.EventAuthUp -> {
                userAuthUpProcess(candidateName = events.candidateName, authCode = events.authCode, sidCode = events.sidCode)
            }

            is OperationResultEvents.EventSendReferralContacts     -> {
//                 after created fragment necessary few steps
//                 01. Request to server for registration user...
//                 02. After response registration user save and encrypt data response
//                 03. After save - request for login (authentification) user of server Sumra
//                 04. Response of login - token for used in system...
//                 05. Request permission user contacts for take participation referral program
//                 06. if permission great - fragment selected contacts... else run fragment all wallets user...
//                updateState.checkPermissionContacts()
            }

            is OperationResultEvents.EventSubmitContacts     -> {
                updateState.checkProgressState(true)
                CoroutineScope(Dispatchers.IO).launch {
                    try{
                        val contact = mutableListOf<ShareContact>()
                        val imageString = StringBuilder()
                        events.contacts.forEach { item ->
                            val contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, item.id)
                            val photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY)
                            item.photoURI?.let {
                                var baos: InputStream? = null

                                val cursor: Cursor? = context.getContentResolver().query(
                                        photoUri,
                                        arrayOf(ContactsContract.Contacts.Photo.PHOTO),
                                        null,
                                        null,
                                        null)
                                try {
                                    cursor?.let {
                                        if (it.moveToFirst()) {
                                            val data = it.getBlob(0)
                                            if (data != null) {
                                                baos = ByteArrayInputStream(data)
                                            }
                                        }
                                    }
                                } finally {
                                    cursor?.close()
                                }
                                baos?.let { stream ->
                                    val bitmap = BitmapFactory.decodeStream(stream)
                                    val out = ByteArrayOutputStream()
                                    bitmap?.run {
                                        compress(Bitmap.CompressFormat.JPEG, 75, out)
                                        val imageBytes: ByteArray = out.toByteArray()
                                        imageString.append(Base64.encodeToString(imageBytes, Base64.DEFAULT))
                                    }
                                }
                            }

                            // добавить проверку по отправленым на сервер контактам
                            // 1. Сделать базу для записи id отправленных контактов
                            // 2. Сверять уже отправленые записи с новыми отправляемыми

                            contact.add(
                                ShareContact(
//                                    userId   = item.id,
                                    displayName = item.displayName,
                                    avatar   = imageString.toString(),
                                    phones   = item.msisdns,
                                    emails   = item.emails,
                                    isShared = true)
                            )
                        }
                        currentContactConnect?.let{
                            it.actionContactsShare(
                                                    authValid = session.homeUserId,
                                                    appUID = identifyApp,
                                                    contacts = EnterShareContacts(
                                                    groupId = null,
                                                    listsContact = contact))
                        }
                    } catch (failure: Throwable ){
                        if (failure !is CancellationException) {
                            Timber.e(failure)
                            withContext(Dispatchers.Main) {
                                updateState.sendMessageToSystem(
                                    type = MessageTypeEvents.MessageSimple(failure.message!!)
                                )
                            }
                        }
                        null
                    }?.let{ result ->
//                        val type =
                        when(result.body.type.equals("success")){
                            true -> {
                                withContext(Dispatchers.Main) {

                                }
                            }
                            else -> {
                                withContext(Dispatchers.Main) {
                                    updateState.sendMessageToSystem(
                                        type = MessageTypeEvents.MessageSimple("ERROR!..")
                                    )
                                }
                            }
                        }
                    }
                    withContext(Dispatchers.Main) {
                        updateState.checkProgressState(false)
                        updateState.updateControllerState(
                            fragment = null,
                            params = session,
                            addBack = true,
                            isBack = false
                        )
                    }
                }
            }

            is OperationResultEvents.EventReferralsSendSuccess -> updateState.updateControllerState(
                fragment = null,
                addBack  = false,
                isBack   = false)

            is OperationResultEvents.EventPushSimple  -> updateState.sendMessageToSystem(
                type = MessageTypeEvents.MessageSimple (events.message))
            is OperationResultEvents.EventPushSuccess -> updateState.sendMessageToSystem(
                type = MessageTypeEvents.MessageSuccess(events.message))
            is OperationResultEvents.EventPushFailure -> updateState.sendMessageToSystem(
                type = MessageTypeEvents.MessageFailure(events.message))
            is OperationResultEvents.EventCongrats    -> updateState.checkPermissionContacts()
        }
    }

    /***  sending received code with messengers or sms to validation   ***/
    private fun actionSendCodeValidation(authCode: String){
        updateState.checkProgressState(true)
        CoroutineScope(Dispatchers.IO).launch {
            try {
                currentAuthConnect?.actionSendCode(appUID = identifyApp, authCode = authCode)
            } catch (failure: Throwable){
                if (failure !is CancellationException) {
                    withContext(Dispatchers.Main) {
                        updateState.checkProgressState(false)
                        updateState.sendMessageToSystem(
                            type = MessageTypeEvents.MessageFailure(failure.message!!)
                        )
                    }
                }
                null
            }?.let { result ->
                when(result){
                    is RemoteResponse.Data -> {
                        val sel = result.body as objectDataCode.RSPCode
                        withContext(Dispatchers.Main) {
                            /**  this next step - start fragment username  **/
                            nextStepsShowEnterUsername(authCode, sel.sidCode)
                        }
                    }
                    is RemoteResponse.Fail -> {
                        val fail = result.fail as WrapperError
                        withContext(Dispatchers.Main) {
                            updateState.sendMessageToSystem(
                                MessageTypeEvents.MessageFailure("Sorry. ${fail.message}")
                            )
                        }
                    }
                }
                withContext(Dispatchers.Main){
                    updateState.checkProgressState(false)
                }
            }
        }
    }


    // 01. Request to server for registration user...
    private fun userAuthUpProcess(candidateName: String, authCode: String, sidCode: String){
        updateState.checkProgressState(true)
        CoroutineScope(Dispatchers.IO).launch {
            try {
                currentAuthConnect?.actionUserName(
                    sidCode  = sidCode,
                    username = candidateName,
                    appUID   = identifyApp )
            } catch (failure: Throwable){
                if (failure !is CancellationException) {
                    withContext(Dispatchers.Main) {
                        updateState.checkProgressState(false)
                        updateState.sendMessageToSystem(
                            type = MessageTypeEvents.MessageFailure(failure.message!!)
                        )
                    }
                }
                null
            }?.let { result ->
                when(result){
                    is RemoteResponse.Data -> {
                        val sel = result.body as objectDataUser.RSPUserName
                        withContext(Dispatchers.Main) {
//                            session = Session(token = UUID.randomUUID().toString(),
//                                homeUserId = UUID.randomUUID().toString(),
//                                homeServer = "https://env.meet.net/",
//                                userName = candidateName,
//                                code = candidateCode)

                            session = Session(
                                token = if(sel.token.isNullOrEmpty()) "" else sel.token,
                                homeUserId = sel.userID, // sel.content.userId,
                                homeServer = "", // sel.content.homeServer,
                                userName = candidateName,
                                code = sidCode)
                            nextStepsShowCongrats()
                        }
                    }
                    is RemoteResponse.Fail -> {
                        val fail = result.fail as WrapperError
                        withContext(Dispatchers.Main) {
                            updateState.sendMessageToSystem(
                                MessageTypeEvents.MessageFailure("Sorry. ${fail.message}")
                            )
//                            session = Session(token = result.content.accessTokenSso,
//                                homeUserId = result.content.userId,
//                                homeServer = result.content.homeServer,
//                                userName = candidateName,
//                                code = candidateCode)
//                            nextStepsShowCongrats()
                        }
                    }
                }
                withContext(Dispatchers.Main){
                    updateState.checkProgressState(false)
                }
            }
        }
    }

    // 02. After response registration user save and encrypt data response
    private fun userAuthInProcess(parameter: AuthParameter){
        CoroutineScope(Dispatchers.IO).launch {
//            val respAuthIn = connect.userAuthInNet(
//                username = parameter.authUsername,
//                password = parameter.authPassword).await()
//            withContext(Dispatchers.Main) {
//                if (respAuthIn.code != 200) {
//                    respAuthIn.content?.let {
//                        // parse token from data class and saving
//                        updateState.checkPermissionContacts()
//                    }
//                } else {
//                    updateState.sendMessageToSystem(
//                        MessageTypeEvents
//                            .MessageFailure(message = LibraryError(respAuthIn.code).errorParse())
//                    )
//                }
//                updateState.checkProgressState(false)
//
//                //todo remove this request
//                updateState.checkPermissionContacts()
//            }
        }
    }

    private fun userAuthBySumraId(events: OperationResultEvents.EventSumraIdLogin){
        updateState.checkProgressState(true)
        CoroutineScope(Dispatchers.IO).launch {
            try{
                currentAuthConnect?.run{
                    delay(2100)
                    actionAuthBySumraId(password = events.password.toString(),
                                        username = events.username.toString(),
                                        appUID   = identifyApp)
                }
            }catch (ex: RuntimeException){
                /** This place connect Error... */
                withContext(Dispatchers.Main) {
                    updateState.checkProgressState(false)
                    updateState.sendMessageToSystem(MessageTypeEvents.MessageFailure(ex.message!!))
                }
            }?.let { response ->
                /** This place server response success/error... */
//                when(response) {
//                    val content = response as UserRegisteredParams
                    withContext(Dispatchers.Main) {
                        updateState.checkProgressState(false)
//                        updateState.sendMessageToSystem(MessageTypeEvents.MessageFailure("${content.success}, ${content.content.accessTokenSso}"))
                    }
//                }
            }
        }
    }

    // 03. After response registration user save and encrypt data response
    private fun userAuthSaveParams(params: AuthParameter) {
        //parse and serialization and encription process...
        // Invariant - exactly one of mMap / mParcelledData will be nullInt
        // (except inside a call to unparcel)
//        val mMap: ArrayMap<String, Any>? = null
        val _convertor = Convertor(context = context)
        _convertor.authUserData(params)
//        val data = _convertor.authUserData()
//        Timber.i("$TAG :: userAuthSaveParams: ")
    }

    // 04. After response registration user save and encrypt data response
    private fun userAuthReadParams(): AuthParameter{
        val _convertor = Convertor(context = context)
        return _convertor.authUserData()
//        return AuthParameter(authUsername = "UserName", authPassword = "PaSsWoRd")
    }

    private fun updatePageByOptions(phoneNumber: String?) {
        updateState.updateControllerState(
           fragment = FragmentCode.getInstance(
               resId            = logoResId,
               frgListener      = this,
               lightColor       = lightColor,
               darkColor        = darkColor,
               textColPrimDark  = textColPrimDark,
               textColNormDark  = textColNormDark,
               textColPrimary   = textColPrimary,
               textColLight     = textColLight,
               textColSelect    = textColSelect,
               fontAssetsPath   = fontAssetsPath,
               backgroundColor  = backgroundColor,
               codeColorFocusNo = codeColorFocusNo,
               codeColorFocusOk = codeColorFocusOk,
               phoneSending     = phoneNumber
           ),
           addBack = true,
           isBack = false
        )
    }

    fun nextStepsReferralContacts(){
        context.let{
            CoroutineScope(Dispatchers.IO).launch {                 // load email        // load phone
                val contacts = ContactsDataSource(it).getContacts(config.isLoadEmail, config.isLoadPhone)
                delay(1000)
                withContext(Dispatchers.Main){
                    startReferralFragment(contacts)
                    action?.run{
                        updateFragmentByAction(ActionEvents.ActionEventsContacts(adapter = AdapterContact(contacts as MutableList<MappedContact>)))
                    }
                }
            }
        }
    }

    fun nextStepsShowEnterUsername(authCode: String, sidCode: String){
        val frg = FragmentUsername.getInstance(logoResId, authCode, sidCode,this)
            action = frg as FragmentAction
            updateState.updateControllerState(
                fragment = frg,
//                    params = AddReceiverCode(result.code),
                addBack = true,
                isBack = false
            )
    }

    fun nextStepsShowCongrats(){
        updateState.updateControllerState(FragmentCongrats.getInstance(context, this), null, false, false)
    }

    private fun startReferralFragment(contacts: List<MappedContact>){
        val referral = FragmentReferral.getInstance(resId = logoResId,
            frgListener = this,
            adapter = AdapterContact(contacts as MutableList<MappedContact>))
        updateState.updateControllerState(
            fragment = referral,
            addBack = false,
            isBack = false
        )
    }

    // adapters ...

    interface ReferralCellListener {
        fun contactSelect(contact: MappedContact) }

    class AdapterContact(val contacts: MutableList<MappedContact>)
        : RecyclerView.Adapter<AdapterContact.HolderContact>()
        , ReferralCellListener {

        class HolderContact(cell: View): RecyclerView.ViewHolder(cell){
            private val bind = ReferralContactCellBinding.bind(cell)
            private var itemState: Boolean = false
            init {
                bind.referralSelectedShow.imageTintList = setupCheckBoxTint()
            }
            fun bind(contact: MappedContact, select: ReferralCellListener, state: Boolean){
                itemState = state
                bind.referralSelectedPhoto.setImageURI(contact.photoURI)
                bind.referralSelectedUsername.text = contact.displayName
                contactStateChecked(itemState)
                bind.referralCell.setOnClickListener { view ->
                    itemState = !itemState
                    select.contactSelect(contact)
                    contactStateChecked(state = itemState)
                }
            }
            private fun contactStateChecked(state: Boolean){
                when(state){
                    true -> {
                        bind.referralSelectedShow
                            .setImageResource(R.drawable.ic_referral_contact_ok)
                    }
                    else -> {
                        bind.referralSelectedShow
                            .setImageResource(R.drawable.ic_referral_contact_no)
                    }
                }
            }
        }
//--------------------------------------------------------------------------------------------------
        private val selected = mutableListOf<MappedContact>()
        private val filtered = mutableListOf<MappedContact>()

        init {
            filtered.addAll(contacts)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderContact {
            return HolderContact(LayoutInflater.from(parent.context)
                .inflate(R.layout.referral_contact_cell, parent, false))
        }

        override fun onBindViewHolder(holder: HolderContact, position: Int) {
            holder.bind(filtered[position], this, checkContactSelect(filtered[position]))
        }

        override fun getItemCount(): Int {
            return filtered.size
        }

        override fun contactSelect(contact: MappedContact) {
            if(!selected.contains(contact)) {
                selected.add(contact)
            }else {
                selected.remove(contact)
            }
        }

        private fun checkContactSelect(contact: MappedContact): Boolean {
            return selected.contains(contact)
        }

        fun getContactsSelected() = selected

        fun getContactsAll() = contacts

        fun updateListByFilter(textFilter: String){
            if(textFilter.length > 0){
                filtered.clear()
                contacts.forEach { item ->
                    if(item.displayName.length>=textFilter.length) {
                        val names = item.displayName.split(" ")
                        val count = names.size
                        var symbol = 0
                        while (symbol < count){
                            if(names[symbol].length >= textFilter.length){
                                val tmp: String = names[symbol].substring(0, textFilter.length).lowercase()
                                if (tmp.equals(textFilter.lowercase())) {
                                    filtered.add(item)
                                    symbol = count
                                }
                            }
                            symbol ++
                        }
                    }
                }
            }else{
                filtered.clear()
                filtered.addAll(contacts)
            }
            notifyDataSetChanged()
        }
    }
}

interface FragmentListener{
    fun resultActionListener(result: OperationResultEvents)
}

interface FragmentAction{
    fun updateFragmentByAction(result: ActionEvents)
}

interface ControllerListener{
    fun updateControllerState(fragment: Fragment?, params: Parcelable? = null, addBack: Boolean, isBack: Boolean)
    fun sendMessageToSystem(type: MessageTypeEvents)
    fun checkProgressState (state: Boolean)
    fun checkPermissionContacts ()
}

@Parcelize
data class Session(
    @SerializedName("token_sso")
    val token: String = "",
    @SerializedName("username")
    val userName: String = "",
    @SerializedName("security")
    val code: String = "",
    @SerializedName("home_server")
    val homeServer: String = "",
    @SerializedName("home_user_id")
    val homeUserId: String = "",
    @SerializedName("user_avatar")
    val avatar: String? = null
): Parcelable