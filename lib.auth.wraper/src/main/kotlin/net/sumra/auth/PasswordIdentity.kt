package net.sumra.auth

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView

class PasswordIdentity @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0): AppCompatImageView(context, attrs, defStyleAttr) {

    init {
        renderState(false)
    }

    fun colorTintUpdate(color: ColorStateList){
        this.imageTintList = color
    }

    fun renderState(boolean: Boolean){
        when(boolean){
            true -> this.setImageResource(R.drawable.ic_eye_show)
            else -> this.setImageResource(R.drawable.ic_eye_hide)
        }
    }

}