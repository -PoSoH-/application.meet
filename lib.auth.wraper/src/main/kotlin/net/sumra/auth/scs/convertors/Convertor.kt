package net.app.sph.tst.scs.convertors

import android.app.Activity
import android.content.Context
import android.provider.SyncStateContract
import android.util.Base64
import com.google.gson.Gson
import net.sumra.auth.models.AuthParameter
import okhttp3.internal.trimSubstring
import java.io.*
import java.lang.StringBuilder
import kotlin.random.Random

class Convertor(val context: Context) {

    private val _data      = DataSymbols()
    private val _generator = Generator()
    private var _settings  : Settings
    private val _preference= PreferenceSaving(context)

    init {
        _data.initConstants(_generator)
        _settings = Settings()
        _settings.initGroup(_data)
    }

    fun authUserData(auth: AuthParameter){
        _settings.updateAuthData(authData = auth, _data)
        _preference.savingSetup(convertObjForSaving())
    }

    fun authUserData(): AuthParameter{
        _settings = _preference.loadingSetup()
        return _settings.updateAuthData(_data)
    }

    fun authUserCheck(authName: String): Boolean{
        _settings = _preference.loadingSetup()
        val temp = _settings.updateAuthData(_data)
        if(authName.hashCode() == temp.authUsername.hashCode()
            && authName.equals(temp.authUsername)) {
            return true
        }else {
            return false
        }
    }

//    fun settings(): Settings{
//        return _settings
//    }

    private fun convertObjForSaving(): String {
        val res = StringBuilder()
        try {
            val gsonResult = Gson().toJson(_settings)
            res.append(Base64.encodeToString(gsonResult.toByteArray(), Base64.DEFAULT))
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        } catch (e: ClassCastException) {
            e.printStackTrace();
        }
        return res.toString()
    }

    private fun convertLoadingForObjUse(data: String): Settings {
        lateinit var res: Settings //StringBuilder()
        try {
            val from: String = Base64.decode(data, Base64.DEFAULT).decodeToString()
            res = Gson().fromJson(from, Settings::class.java)
        } catch (e: IOException) {
            e.printStackTrace();
        } catch (e: ClassNotFoundException) {
            e.printStackTrace();
        } catch (e: ClassCastException) {
            e.printStackTrace();
        }
        return res
    }

    inner class Generator{
        fun generatorCode(countSymbol: Int = 12, symbols: HashMap<Int, List<String>>): String {
            val password = StringBuilder()
            var counter = 0
            while (counter < countSymbol) {
                counter++
                val temp = symbols[Random.nextInt(symbols.size - 1)]
                password.append(temp!!.get(Random.nextInt(temp.size - 1)))
            }
            return password.toString()
        }
    }

    inner class DataSymbols{

        private val _num_a = 64
        private val _num_b = 64 //1024

        private val SYMBOLS = hashMapOf<Int, List<String>>(
            0 to listOf("q", "a", "z", "w", "s", "x", "e", "d", "c", "r", "f", "v", "t", "g", "b", "y", "h", "n", "u", "j", "m", "i", "k", "o", "l", "p"),
            1 to listOf("Q", "A", "Z", "W", "S", "X", "E", "D", "C", "R", "F", "V", "T", "G", "B", "Y", "H", "N", "U", "J", "M", "I", "K", "O", "L", "P"),
            2 to listOf("0", "7", "2", "8", "4", "6", "5", "3", "1", "9"),
            3 to listOf("!", "@", "#", "$", "%", "^", "&", "*")
        )
        val FILE_NAME  = "default.app.cash"

        lateinit var SETUP_PRD: String  // value city of default
        lateinit var SETUP_UNM: String  // value city of default
        lateinit var SETUP_CRY: String  // value currency of default
        lateinit var SETUP_CTY: String  // value city of default
        lateinit var SETUP_THR: String  // value city of default
        lateinit var NIL_SETUP: String  // value data of not default
        lateinit var LOG_SETUP: String  // value login of default

        fun initConstants (generator: Generator){
            SETUP_PRD = generator.generatorCode(_num_b, SYMBOLS)
            SETUP_UNM = generator.generatorCode(_num_b, SYMBOLS)
            SETUP_CRY = generator.generatorCode(_num_a, SYMBOLS)
            SETUP_CTY = generator.generatorCode(_num_a, SYMBOLS)
            SETUP_THR = generator.generatorCode(_num_a, SYMBOLS)
            NIL_SETUP = generator.generatorCode(_num_a, SYMBOLS)
            LOG_SETUP = generator.generatorCode(_num_a, SYMBOLS)
        }

        fun getNum() = _num_b
    }

    inner class Settings(): Serializable {

        private lateinit var settingsPrd: String // = _symbols.SETUP_PRD
        private lateinit var settingsUnm: String // = _symbols.SETUP_UNM
        private lateinit var settingsCry: String // = _symbols.SETUP_CRY
        private lateinit var settingsCty: String // = _symbols.SETUP_CTY
        private lateinit var settingsOth: String // = _symbols.SETUP_THR

        fun initGroup(symbols: DataSymbols) {
            settingsPrd = symbols.SETUP_PRD
            settingsUnm = symbols.SETUP_UNM
            settingsCry = symbols.SETUP_CRY
            settingsCty = symbols.SETUP_CTY
            settingsOth = symbols.SETUP_THR
        }

        fun updateAuthData(authData: AuthParameter, symbols: DataSymbols){
            if(settingsPrd.length == symbols.getNum()){
                val tmpS1 = settingsPrd.chunked(symbols.getNum()/2)
                settingsPrd = "${tmpS1[0]}${authData.authPassword}${tmpS1[1]}"
            }else{
                settingsPrd = _data.SETUP_PRD
                val tmpS1 = settingsPrd.chunked(symbols.getNum()/2)
                settingsPrd = "${tmpS1[0]}${authData.authPassword}${tmpS1[1]}"
            }
            if(settingsUnm.length == symbols.getNum()){
                val tmpS1 = settingsUnm.chunked(symbols.getNum()/2)
                settingsUnm = "${tmpS1[0]}${authData.authUsername}${tmpS1[1]}"
            }else{
                settingsUnm = _data.SETUP_PRD
                val tmpS1 = settingsUnm.chunked(symbols.getNum()/2)
                settingsUnm = "${tmpS1[0]}${authData.authUsername}${tmpS1[1]}"
            }
        }

        fun updateAuthData(symbols: DataSymbols):AuthParameter{
            if(settingsPrd.length == symbols.getNum()&&settingsUnm.length == symbols.getNum()){
                return AuthParameter("", "")
            }else if(settingsPrd.length == symbols.getNum() && settingsUnm.length != symbols.getNum()){
                return AuthParameter(authUsername = parseUnm(symbols), "")
            }else if(settingsPrd.length != symbols.getNum()&&settingsUnm.length != symbols.getNum()){
                return AuthParameter(authUsername = parseUnm(symbols), authPassword = parsePrd(symbols))
            } else {
                // maybe exception
                return AuthParameter("", "")
            }
        }

        private fun parsePrd(symbols: DataSymbols):String{
            val st1 = settingsPrd.chunked(symbols.getNum()/2)
            val st2 = st1[1].chunked(settingsPrd.length - symbols.getNum())
            return st2[0]
        }


        private fun parseUnm(symbols: DataSymbols):String{
            val st1 = settingsUnm.chunked(symbols.getNum()/2)
            val st2 = st1[1].chunked(settingsUnm.length - symbols.getNum())
            return st2[0]
        }
    }

    inner class PreferenceSaving(private val context: Context){

        fun savingSetup(scs: String){
            lateinit var stream : ObjectOutputStream
            try {
                val fileOut: FileOutputStream = context.openFileOutput(_data.FILE_NAME, Activity.MODE_PRIVATE)
                stream = ObjectOutputStream(fileOut)
                stream.writeObject(scs)
                fileOut.getFD().sync()
            } catch (e: IOException) {
                e.printStackTrace()
            } finally {
                try {
                    stream.close()
                } catch (e: IOException) {
                    // do nowt
                }
            }
        }

        fun loadingSetup() : Settings{
            var loadedObj: String? = null
            var stream: ObjectInputStream? = null
            try {
                val fileIn : FileInputStream = //ctx.openFileInput(Constants.FILE_NAME)
                    context.getApplicationContext()
                        .openFileInput(_data.FILE_NAME);
                stream = ObjectInputStream(fileIn);
                loadedObj = stream.readObject() as String

            } catch (e: FileNotFoundException) {
                // Do nothing
            } catch (e: IOException) {
                e.printStackTrace();
            } catch (e: ClassNotFoundException) {
                e.printStackTrace();
            } finally {
                try {
                    if(stream!=null) {
                        stream.close();
                    }
                } catch (e: IOException) {
                    // do nowt
                }
            }
            if(loadedObj == null){
                _settings.initGroup(_data)
                savingSetup(convertObjForSaving())
                return _settings
            }else {
                return convertLoadingForObjUse(loadedObj)
            }

        }

//        fun checkDataPrd(context: Context) :Boolean {
//            val data = loadingSetup(context).settingsPrd
//            val resultat = data.equals(Constants.NIL_SETUP)
//            return resultat
//        }
//
//        fun checkDataUnm(data: String, context: Context) = !loadingSetup(context).settingsUnm.equals(data)
//
//        fun checkLogSetup(context: Context) : Boolean {
//            val data = loadingSetup(context).settingsPrd
//            val resultat = !data.equals(Constants.LOG_SETUP)
//            return resultat
//        }
    }
}
