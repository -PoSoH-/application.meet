package net.sumra.auth

import android.app.Application
import android.content.Context
import android.content.SharedPreferences

class AuthWrapperApp: Application() {

    companion object {
        private lateinit var appContext: Context
        private lateinit var prefPrivate: SharedPreferences

        fun getAppContext() = appContext
        fun getPref() = prefPrivate
    }

    override fun onCreate() {
        super.onCreate()

        appContext = this
        prefPrivate= getSharedPreferences("MEET.SUMRA.APP", MODE_PRIVATE)

    }


}