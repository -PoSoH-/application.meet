package net.sumra.auth.auth

import okhttp3.OkHttpClient
import timber.log.Timber

//internal fun OkHttpClient.Builder.addSocketFactory(serverConnectionConfig: ServerConnectionConfig): OkHttpClient.Builder {
//    try {
//        val pair = CertUtil.newPinnedSSLSocketFactory(serverConnectionConfig)
//        sslSocketFactory(pair.sslSocketFactory, pair.x509TrustManager)
//        hostnameVerifier(CertUtil.newHostnameVerifier(serverConnectionConfig))
//        connectionSpecs(CertUtil.newConnectionSpecs(serverConnectionConfig))
//    } catch (e: Exception) {
//        Timber.e(e, "addSocketFactory failed")
//    }
//
//    return this
//}
//
//fun newHostnameVerifier(hsConfig: HomeServerConnectionConfig): HostnameVerifier {
//    val defaultVerifier: HostnameVerifier = OkHostnameVerifier // HttpsURLConnection.getDefaultHostnameVerifier()
//    val trustedFingerprints = hsConfig.allowedFingerprints
//
//    return HostnameVerifier { hostname, session ->
//        if (USE_DEFAULT_HOSTNAME_VERIFIER) {
//            if (defaultVerifier.verify(hostname, session)) return@HostnameVerifier true
//        }
//        // TODO How to recover from this error?
//        if (trustedFingerprints.isEmpty()) return@HostnameVerifier false
//
//        // If remote cert matches an allowed fingerprint, just accept it.
//        try {
//            for (cert in session.peerCertificates) {
//                for (allowedFingerprint in trustedFingerprints) {
//                    if (cert is X509Certificate && allowedFingerprint.matchesCert(cert)) {
//                        return@HostnameVerifier true
//                    }
//                }
//            }
//        } catch (e: SSLPeerUnverifiedException) {
//            return@HostnameVerifier false
//        } catch (e: CertificateException) {
//            return@HostnameVerifier false
//        }
//
//        false
//    }
//}
//
///**
// * Create a list of accepted TLS specifications for a hs config.
// *
// * @param hsConfig the hs config.
// * @return a list of accepted TLS specifications.
// */
//fun newConnectionSpecs(hsConfig: HomeServerConnectionConfig): List<ConnectionSpec> {
//    val builder = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
//    val tlsVersions = hsConfig.tlsVersions
//    if (null != tlsVersions && tlsVersions.isNotEmpty()) {
//        builder.tlsVersions(*tlsVersions.toTypedArray())
//    }
//
//    val tlsCipherSuites = hsConfig.tlsCipherSuites
//    if (null != tlsCipherSuites && tlsCipherSuites.isNotEmpty()) {
//        builder.cipherSuites(*tlsCipherSuites.toTypedArray())
//    }
//
//    @Suppress("DEPRECATION")
//    builder.supportsTlsExtensions(hsConfig.shouldAcceptTlsExtensions)
//    val list = ArrayList<ConnectionSpec>()
//    list.add(builder.build())
//    // TODO: we should display a warning if user enter an http url
//    if (hsConfig.allowHttpExtension || hsConfig.homeServerUri.toString().startsWith("http://")) {
//        list.add(ConnectionSpec.CLEARTEXT)
//    }
//    return list
//}