package net.sumra.auth.auth

interface PendingSessionStore {
    suspend fun savePendingSessionData(pendingSessionData: PendingSessionData)
    fun getPendingSessionData(): PendingSessionData?
    suspend fun delete()
}