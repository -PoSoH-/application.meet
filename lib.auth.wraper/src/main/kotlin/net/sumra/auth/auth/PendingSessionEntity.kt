package net.sumra.auth.auth

internal open class PendingSessionEntity (
    var homeServerConnectionConfigJson: String = "",
    var clientSecret: String = "",
    var sendAttempt: Int = 0,
    var resetPasswordDataJson: String? = null,
    var currentSession: String? = null,
    var isRegistrationStarted: Boolean = false,
    var currentThreePidDataJson: String? = null
)