package net.sumra.auth.auth

import okhttp3.ConnectionSpec
import okhttp3.internal.tls.OkHostnameVerifier
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLPeerUnverifiedException

internal object CertUtil {

    private const val USE_DEFAULT_HOSTNAME_VERIFIER = true

//    fun newHostnameVerifier(hsConfig: ServerConnectionConfig): HostnameVerifier {
//        val defaultVerifier: HostnameVerifier =
//            OkHostnameVerifier // HttpsURLConnection.getDefaultHostnameVerifier()
//
//        return HostnameVerifier { hostname, session ->
//            if (USE_DEFAULT_HOSTNAME_VERIFIER) {
//                if (defaultVerifier.verify(hostname, session)) return@HostnameVerifier true
//            }
//            // TODO How to recover from this error?
//
//            // If remote cert matches an allowed fingerprint, just accept it.
//            try {
//                for (cert in session.peerCertificates) {
//                    for (allowedFingerprint in trustedFingerprints) {
//                        if (cert is X509Certificate && allowedFingerprint.matchesCert(cert)) {
//                            return@HostnameVerifier true
//                        }
//                    }
//                }
//            } catch (e: SSLPeerUnverifiedException) {
//                return@HostnameVerifier false
//            } catch (e: CertificateException) {
//                return@HostnameVerifier false
//            }
//
//            false
//        }
//    }
//
//    /**
//     * Create a list of accepted TLS specifications for a hs config.
//     *
//     * @param hsConfig the hs config.
//     * @return a list of accepted TLS specifications.
//     */
//    fun newConnectionSpecs(hsConfig: ServerConnectionConfig): List<ConnectionSpec> {
//        val builder = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
//        val tlsVersions = hsConfig.tlsVersions
//        if (null != tlsVersions && tlsVersions.isNotEmpty()) {
//            builder.tlsVersions(*tlsVersions.toTypedArray())
//        }
//
//        val tlsCipherSuites = hsConfig.tlsCipherSuites
//        if (null != tlsCipherSuites && tlsCipherSuites.isNotEmpty()) {
//            builder.cipherSuites(*tlsCipherSuites.toTypedArray())
//        }
//
//        @Suppress("DEPRECATION")
//        builder.supportsTlsExtensions(hsConfig.shouldAcceptTlsExtensions)
//        val list = ArrayList<ConnectionSpec>()
//        list.add(builder.build())
//        // TODO: we should display a warning if user enter an http url
//        if (hsConfig.allowHttpExtension || hsConfig.homeServerUri.toString()
//                .startsWith("http://")
//        ) {
//            list.add(ConnectionSpec.CLEARTEXT)
//        }
//        return list
//    }
}