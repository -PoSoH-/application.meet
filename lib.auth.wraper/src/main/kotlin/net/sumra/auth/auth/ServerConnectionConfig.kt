package net.sumra.auth.auth

import android.net.Uri
import net.sumra.auth.helpers.ensureTrailingSlash

data class ServerConnectionConfig(
    val homeServerUri: Uri
) {
    class Builder {

        private lateinit var homeServerUri: Uri

        fun withHomeServerUri(hsUriString: String): Builder {
            return withHomeServerUri(Uri.parse(hsUriString))
        }

        private fun withHomeServerUri(hsUri: Uri): Builder {
            if (hsUri.scheme != "http" && hsUri.scheme != "https") {
                throw RuntimeException("Invalid home server URI: $hsUri")
            }
            // ensure trailing /
            val hsString = hsUri.toString().ensureTrailingSlash()
            homeServerUri = try {
                Uri.parse(hsString)
            } catch (e: Exception) {
                throw RuntimeException("Invalid home server URI: $hsUri")
            }
            return this
        }

        fun build(): ServerConnectionConfig {
            return ServerConnectionConfig(
                homeServerUri
            )
        }

    }
}
