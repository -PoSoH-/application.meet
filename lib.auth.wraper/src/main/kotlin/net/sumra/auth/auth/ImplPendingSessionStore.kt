package net.sumra.auth.auth

import android.content.SharedPreferences
import com.google.gson.Gson
import javax.inject.Inject

class ImplPendingSessionStore @Inject constructor(
    private val pref: SharedPreferences): PendingSessionStore{

    override suspend fun savePendingSessionData(pendingSessionData: PendingSessionData) {
        pref.edit().putString("data", Gson().toJson(pendingSessionData)).apply()
    }

    override fun getPendingSessionData(): PendingSessionData? {
        return Gson().fromJson(pref.getString("data",""), PendingSessionData::class.java)
    }

    override suspend fun delete() {
        pref.edit().remove("data").apply()
    }
}