package chat.sumra.app.features.login.sumra.messengers

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.view.View
import androidx.appcompat.widget.AppCompatImageView

class MessengerImpl(override val messengerPackName: Messengers, override val context: Context) : IMessenger {

    private val iconButton = AppCompatImageView(context)
    private lateinit var intent: Intent
    private var programChecked = false

    private val STORE_BASE = "https://play.google.com/store/apps/details?id="

    init{
//        iconButton.showImageRound(messengerPackName.resource)
        iconButton.setImageResource(messengerPackName.resource)
    }
    fun getButton(): View {
        return iconButton
    }
    override fun checkPackageNameResult(manager: PackageManager) : Boolean {
        try{
            programChecked = manager.getPackageInfo(messengerPackName.`package`, 0) != null
        } catch (er: PackageManager.NameNotFoundException){
            programChecked = false
        }
        return programChecked
    }

    override fun getIntentResult(): Intent {
        if(programChecked){
            intent = Intent(
                messengerPackName.action, messengerPackName.uri
            )
            intent.setPackage(messengerPackName.`package`)
        }else{
            intent = Intent(messengerPackName.action, Uri.parse("$STORE_BASE${messengerPackName.`package`}"))
        }
        return intent
    }
}
