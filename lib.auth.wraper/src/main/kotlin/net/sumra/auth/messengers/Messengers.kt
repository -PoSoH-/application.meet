/*
 * Copyright 2021 INVESTORS IN IDEAS LTD
 */

package chat.sumra.app.features.login.sumra.messengers

import android.content.Intent
import android.net.Uri
import net.sumra.auth.R

enum class Messengers (val `package`: String, val resource: Int, val action: String, var uri: Uri) {
    TELEGRAM  (`package` = "org.telegram.messenger" //:org.telegram.plus"
        , resource = R.drawable.ic_app_sumra_icon_telegram
        , action = "android.intent.action.VIEW"
        , uri = Uri.parse("tg://resolve?domain=SumraBot")),
    VIBER     (`package` = "com.viber.voip"
        , resource = R.drawable.ic_app_sumra_icon_viber
        , action = "android.intent.action.VIEW"
        , uri = Uri.parse("viber://pa?chatURI=sumrabot")),
    WHATSAPP  (`package` = "com.whatsapp"
        , resource = R.drawable.ic_app_sumra_icon_whatsapp
        , action = Intent.ACTION_VIEW // only ACTION_VIEW
//        , uri = Uri.parse("https://wa.me/send?phone=+14155238886&text=join stage-nine")),  // not working
        , uri = Uri.parse("https://api.whatsapp.com/send/?phone=14155238886&text=join stage-nine&app_absent=0")), // this link working
//        , uri = Uri.parse("https://wa.me/send/?phone=14155238886&text=join stage-nine&app_absent=0")),  // not working
    SIGNAL    (`package` = "org.thoughtcrime.securesms"
        , resource = R.drawable.ic_app_sumra_icon_signal
        , action = Intent.ACTION_VIEW
//        , uri = Uri.parse("https://org.thoughtcrime.securesms/send/?phone=380954410723&text=hello")),
//        , uri = Uri.parse("sgnl://signal.group/?phone=380954410723&text=hello")),
//        , uri = Uri.parse("https://sms?ssp=380954410723&text=join stage-nine")),
        , uri = Uri.parse("smsto:+380954410723?body=Send me security code please!")), ///380954410723")),
    MESSENGER (`package` = "com.facebook.orca"
        , resource = R.drawable.ic_app_sumra_icon_messenger
        , action = "android.intent.action.VIEW"
        , uri = Uri.parse("nil")),
    DISCORD   (`package` = "com.discord"
        , resource = R.drawable.ic_app_sumra_icon_discord
        , action = "android.intent.action.VIEW"
        , uri = Uri.parse("https://discord.gg/75xbhgmbvP")), //?client_id=843810660324474890&permissions=210944&scope=bot")),
    LINE      (`package` = "jp.naver.line.android"
        , resource = R.drawable.ic_app_sumra_icon_line
        , action = "android.intent.action.VIEW"
        , uri = Uri.parse("line://ti/p/@772dmcwu")),
//    LINELITE  (`package` = "com.linecorp.linelite", resource = R.drawable.ic_app_sumra_icon_messenger),
    SNAPCHAT  (`package` = "com.snapchat.android"
    , resource = R.drawable.ic_app_sumra_icon_snapchat
    , action = "android.intent.action.VIEW"
    , uri = Uri.parse("nil")),
//    LINKEDIN  (`package` = "com.linkedin.android", resource = R.drawable.ic_app_sumra_icon_messenger),
//    PINTEREST (`package` = "com.pinterest", resource = R.drawable.ic_app_sumra_icon_messenger),
//    REDDIT    (`package` = "com.reddit.frontpage", resource = R.drawable.ic_app_sumra_icon_messenger),
//    TWITCH    (`package` = "tv.twitch.android.app", resource = R.drawable.ic_app_sumra_icon_messenger),
//    TIKTOK    (`package` = "com.zhiliaoapp.musically", resource = R.drawable.ic_app_sumra_icon_messenger),
//    INSTAGRAM (`package` = "com.instagram.android", resource = R.drawable.ic_app_sumra_icon_messenger),
    UNDEFINED (`package` = "nil", resource = -1, action = "nil", uri = Uri.parse("nil"));

    companion object{

        fun getListMessangers(): List<Messengers>{
            return mutableListOf(
                TELEGRAM,
                VIBER,
                LINE,
                WHATSAPP,
                DISCORD,
                SIGNAL,
                MESSENGER,
                SNAPCHAT
            )
        }
    }
}
