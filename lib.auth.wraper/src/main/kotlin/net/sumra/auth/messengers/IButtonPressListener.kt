package chat.sumra.app.features.login.sumra.messengers

import android.content.Intent

interface IButtonPressListener {
    fun pressed(mesOk: Boolean, type: Messengers, intent: Intent)
}
