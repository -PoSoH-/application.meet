package chat.sumra.app.features.login.sumra.messengers

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager

interface IMessenger {
    val messengerPackName: Messengers
    val context: Context

    fun checkPackageNameResult(manager: PackageManager) : Boolean
    fun getIntentResult(): Intent
}
