package chat.sumra.app.features.login.sumra.messengers

import android.app.Activity
import android.content.Context
import android.graphics.Point
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import net.sumra.auth.R
import timber.log.Timber


class ControllerMessengers(val context: Context) {
    private val messengers = mutableListOf<MessengerImpl>()
    init{
        Messengers.getListMessangers().forEach {
            messengers.add(MessengerImpl(it, context))
        }
    }
    fun updateButtonView(view: LinearLayout){
        var realWeight = 0
        var scale = 0.0f
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
//            val temp = (context as Activity).windowManager.currentWindowMetrics
//            val rect : Rect = temp.bounds
//            realWeight = rect.width()
////            scale = temp
//            Timber.i("${realWeight}")
//        }else{
            val temp = (context as Activity).getWindowManager().defaultDisplay
            val rect = Point()
            val metric = DisplayMetrics()
            temp.getRealSize(rect)
            realWeight = rect.x
            temp.getRealMetrics(metric)
            scale = metric.scaledDensity
            Timber.i("${realWeight}")
//        }

        val iconSize = messengers.size * context.resources.getDimension(R.dimen.messenger_button).toInt()
        var basePadd = context.resources.getDimension(R.dimen.sumra_sign_separate_messenger_button).toInt()

        if((iconSize + (basePadd * (messengers.size - 1))) < realWeight/scale){
            basePadd = (realWeight - iconSize) / (messengers.size)
        }

        messengers.forEachIndexed { index, button ->
            val btn = button.getButton()
            btn.layoutParams = ViewGroup.LayoutParams(
                context.resources.getDimension(R.dimen.messenger_button).toInt(),
                context.resources.getDimension(R.dimen.messenger_button).toInt())
            view.addView(btn)
            if(index < messengers.size-1){
                val separate = View(context)
                separate.layoutParams = ViewGroup.LayoutParams(
                    basePadd, //context.resources.getDimension(R.dimen.sumra_sign_separate_messenger_button).toInt(),
                    ViewGroup.LayoutParams.MATCH_PARENT)
                view.addView(separate)
            }
        }
    }

    fun updateButtonListener(buttonListener: IButtonPressListener){
        messengers.forEach { button ->
            button.getButton().setOnClickListener {
                buttonListener.pressed(button.checkPackageNameResult(context.packageManager), button.messengerPackName, button.getIntentResult())
            }
        }
    }
}
