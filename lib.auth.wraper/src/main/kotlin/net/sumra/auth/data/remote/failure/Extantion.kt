package net.sumra.auth.data.remote.failure

import com.google.gson.Gson
import net.sumra.auth.helpers.LogUtil
import retrofit2.HttpException
import java.nio.charset.Charset
import javax.net.ssl.HttpsURLConnection

fun Throwable.toErrorResponse(): WrapperError? {

    return if ( this is HttpException
        && code() == HttpsURLConnection.HTTP_BAD_REQUEST /* 400 */) {

        val temp = response()!!.errorBody() as okhttp3.ResponseBody
        LogUtil.info("resp", "${temp.source().buffer}")
        val buffer = temp.source().buffer
        val responseBodyString = buffer.clone().readString(Charset.forName("UTF-8"))

        val phone = Gson().fromJson(responseBodyString, WrapperError::class.java)
        val code = Gson().fromJson(responseBodyString, WrapperError::class.java)
        val username = Gson().fromJson(responseBodyString, WrapperError::class.java)
        val oder = Gson().fromJson(responseBodyString, WrapperError::class.java)

        oder
    } else if ( this is Failure.ServerError ) {
        this.error
    } else {
        null
    }
}



fun Throwable.toPhoneSendResponse(): WrapperError? {

    return if ( this is HttpException
        && code() == HttpsURLConnection.HTTP_BAD_REQUEST /* 400 */) {

        val temp = response()!!.errorBody() as okhttp3.ResponseBody
        LogUtil.info("resp", "${temp.source().buffer}")
        val buffer = temp.source().buffer
        val responseBodyString = buffer.clone().readString(Charset.forName("UTF-8"))
        val data = Gson().fromJson(responseBodyString, WrapperError::class.java)
        data
    } else if ( this is Failure.ServerError ) {
        this.error
    } else {
        null
    }
}

fun Throwable.toContactSendResponse(): ContactError? {

    return if ( this is HttpException
        && code() == HttpsURLConnection.HTTP_BAD_REQUEST
    ) {

        val temp = response()!!.errorBody() as okhttp3.ResponseBody
        LogUtil.info("resp", "${temp.source().buffer}")
        val buffer = temp.source().buffer
        val responseBodyString = buffer.clone().readString(Charset.forName("UTF-8"))
        val data = Gson().fromJson(responseBodyString, ContactError::class.java)
        data
    } else {
        if ( this is ServerError ) {
            Gson().fromJson(this.message, ContactError::class.java)
        } else {
            null
        }
    }
}