package net.sumra.auth.data.remote

import okhttp3.OkHttpClient
import javax.inject.Inject

class DefaultConnectService @Inject constructor(
    val okHttpClient: OkHttpClient,
    val retrofitFactory: RetrofitFactory): ConnectService{

    private var currentWrapperWizard : ConnectAuthWizard? = null
    private var currentContactWizard : ConnectContactWizard? = null

    private fun buildWrapperAPI(): AuthAPI {
        val retrofit = retrofitFactory.createForAuth(buildClient(), rout.fullAuth)
        return retrofit.create(AuthAPI::class.java)
    }

    private fun buildContactAPI(): AuthAPI {
        val retrofit = retrofitFactory.createForContact(buildClient(), rout.fullContact)
        return retrofit.create(AuthAPI::class.java)
    }

    private fun buildClient(): OkHttpClient {
        return okHttpClient
            .newBuilder()
            .build()
    }

    // setup endpoint for auth wrapper microservices
    override fun getConnectAuthService(): ConnectAuthWizard {
        val temp =  currentWrapperWizard
            ?: let {
                    DefaultWraperWizard(
                        buildWrapperAPI(),
                    ).also {
                        currentWrapperWizard = it
                    }
            }
        return temp
    }

    // setup endpoint for contact microservices
    override fun getConnectContactService(): ConnectContactWizard {
        val temp =  currentContactWizard
            ?: let {
                DefaultContactWizard(
                    buildContactAPI(),
                ).also {
                    currentContactWizard = it
                }
            }
        return temp
    }

}