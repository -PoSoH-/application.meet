package net.sumra.auth.data.remote

import net.sumra.auth.data.remote.failure.toErrorResponse
import net.sumra.auth.data.remote.models.objectDataPhone

internal interface TaskSendPhone: Task<TaskSendPhone.Params, RemoteResponse> {
    data class Params(
        val content: objectDataPhone.RQTPhone
    )
}

internal class DefPhoneSendExec (private val authAPI: AuthAPI): TaskSendPhone {
    override suspend fun execute(params: TaskSendPhone.Params): RemoteResponse {
        try {
            val dataResult = authAPI.actionSharePhone(content = params.content)
            return RemoteResponse.Data(body = dataResult)
        } catch (exception: Throwable) {
            throw exception.toErrorResponse()
                ?.let{
                    return RemoteResponse.Fail(fail = it)
                }?: exception
        }
    }
}