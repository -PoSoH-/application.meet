package net.sumra.auth.data.remote

object rout {
    internal const val fullAuth      = "https://onestepid.com/"
                                    /* "https://api.sumra.net/" */

    internal const val verAuth       = "api/v1/"

    internal const val sendCode      = "send-code"
    internal const val sendUsername  = "send-username"

    internal const val smsSendPhone  = "sms/send-phone"
    internal const val smsSendSms    = "sms/send-sms"

    internal const val setWebhook    = "messenger/set-webhook"
    internal const val telWebhook    = "messenger/webhook/telegram"

    internal const val registration  = "registration"

    /***   ***   ***   ***   |||   ***   ***   ***   ***/

    internal const val fullContact = "http://ec2-54-186-224-210.us-west-2.compute.amazonaws.com:8107"
                                  // "http://ec2-54-212-236-7.us-west-2.compute.amazonaws.com:8107"
    internal const val verContact = "/v1/contacts"
}