package net.sumra.auth.data.remote

sealed class RemoteResponse{
    data class Data(val body: Any): RemoteResponse()
    data class Fail(val fail: Any): RemoteResponse()
}
