package net.sumra.auth.data.remote

import net.sumra.auth.data.remote.failure.Failure
import net.sumra.auth.data.remote.failure.toErrorResponse
import net.sumra.auth.data.remote.models.objectDataCode

internal interface TaskSendCode: Task<TaskSendCode.Params, RemoteResponse> {
    data class Params(
        val content: objectDataCode.RQTCode
    )
}

internal class DefSendCodeExec (private val authAPI: AuthAPI): TaskSendCode {
    override suspend fun execute(params: TaskSendCode.Params): RemoteResponse {
        try{
            val dataResult = authAPI.actionSendCode (content = params.content)
            return RemoteResponse.Data(dataResult)
        }catch (exception: Throwable){
            throw exception.toErrorResponse()
                ?.let{
                    return RemoteResponse.Fail(it)
                }?: exception
        }
    }
}
