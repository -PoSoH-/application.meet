/*
 * Copyright (c) 2021 BGC Global Partners
 */

package org.matrix.android.sdk.internal.auth.data.sumra

import com.google.gson.annotations.SerializedName
import net.sumra.auth.data.remote.failure.Failure
import net.sumra.auth.data.remote.failure.WrapperError

sealed class ShareResult{
    data class ShareAvailable   (val success: JsonContactSend) : ShareResult()
    data class NotShareAvailable(val failure: WrapperError)    : ShareResult()
}

data class SendContactAvailable(
    @SerializedName("data")
    val body: JsonContactSend,
)

data class JsonContactSend(
    @SerializedName("type")
    val type: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("message")
    val message: String
)


/**
 *
{
    "data": {
        "type": "success",
        "title": "Bulk import of contacts",
        "message": "Contacts was imported successfully"
    }
}
 * */