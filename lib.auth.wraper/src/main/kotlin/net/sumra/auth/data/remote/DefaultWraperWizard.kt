package net.sumra.auth.data.remote

import net.sumra.auth.data.remote.models.objectDataCode
import net.sumra.auth.data.remote.models.objectDataPhone
import net.sumra.auth.data.remote.models.objectDataUser
import org.matrix.android.sdk.internal.auth.data.sumra.*

internal class DefaultWraperWizard (
    authApi: AuthAPI): ConnectAuthWizard {

    private val execSharePhone = DefPhoneSendExec(authApi)
    private val execPhoneSMS   = DefPhoneSMSExec(authApi)
    private val execSendCode   = DefSendCodeExec(authApi)
    private val execUsername   = DefUsernameExec(authApi)

    private val execWebhookSet = DefWebhookSetExec(authApi)
    private val execWebhookTel = DefWebhookTelExec(authApi)

    private val executeAuthBySumraId = DefaultAuthBySumraId(authApi)

    override suspend fun actionSharePhone(phone: String, appUID: String): RemoteResponse {
        return execSharePhone.execute(TaskSendPhone.Params(
            content = objectDataPhone.RQTPhone(
//                appUID = appUID,
                phoneNumber = phone
            )
        ))
    }

    override suspend fun actionSendSMS(phoneNumber: String, appUID: String): RemoteResponse {
        return execPhoneSMS.execute(params = TaskPhoneSMS.Params(
            content = objectDataPhone.RQTPhone(phoneNumber = phoneNumber)))
    }

    override suspend fun actionSendCode(authCode: String, appUID: String): RemoteResponse {
        return execSendCode.execute(params = TaskSendCode.Params(
            content = objectDataCode.RQTCode(authCode = authCode)))
    }

    override suspend fun actionUserName(sidCode: String,
                                        username: String,
                                        appUID: String): RemoteResponse {
        val r = execUsername.execute(TaskSendUsername.Params(
            objectDataUser.RQTUsername(
                sidCode = sidCode,
                username = username))
//                appUID   = appUID))
        )
        return r
    }

    override suspend fun actionWebhookSet(): RemoteResponse {
        return execWebhookSet.execute(params = TaskWebhookSet.Params(content = ""))
    }

    override suspend fun actionWebhookTel(): RemoteResponse {
        return execWebhookTel.execute(params = TaskWebhookTel.Params(content = ""))
    }

//    override suspend fun actionAuthenticate(
//        security: String,
//        username: String,
//        appUID  : String
//    ): RemoteResponse { //UserRegisteredParams {
//        val result = executeAuthenticate.execute(TaskSendUsername.Params(
//            UserEnterParams(
//                security = security,
//                userName = username,
//                appUID   = appUID))
//        )
//        if(result.success){
//            return RemoteResponse.Data(success = UserRegisteredParams(
//                content = AppInformation(accessTokenSso = result.sso.accessToken,
//                    deviceId = "null",
//                    homeServer = rout.fullAuth,
//                    userId = result.sso.accessToken,
//                    appErr = "null"),
//                success = result.success))
//        }else{
//            return RemoteResponse.Fail(failure = result)
//        }
//    }

    override suspend fun actionAuthBySumraId(
        password: String,
        username: String,
        appUID: String
    ): RemoteResponse { //UserRegisteredParams {
        return  executeAuthBySumraId.execute(
            params = TaskSendAuthBySumraId.Params(
                objectDataSumraID.SumraIDEnterParams(
                    password = password,
                    username = username,
                    appUID   = appUID)))
//        if(result.success){
//            return RemoteResponse.Data(result)
//        }else{
//            return RemoteResponse.Fail(result)
//        }
    }
}
