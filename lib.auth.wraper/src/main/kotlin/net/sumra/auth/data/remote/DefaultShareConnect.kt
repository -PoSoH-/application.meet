package net.sumra.auth.data.remote

import net.sumra.auth.data.remote.failure.Failure
import net.sumra.auth.data.remote.failure.toPhoneSendResponse
import net.sumra.auth.data.remote.models.EnterShareContacts
import org.matrix.android.sdk.internal.auth.data.sumra.SendContactAvailable

internal interface TaskShareConnect: Task<TaskShareConnect.Params, SendContactAvailable> {
    data class Params(
        val authValid: String,
        val content: EnterShareContacts
    )
}

internal class DefaultShareConnect (private val authAPI: AuthAPI): TaskShareConnect {
    override suspend fun execute(params: TaskShareConnect.Params): SendContactAvailable {
        try {
            val dataResult = authAPI.actionShareContacts(authHeader = params.authValid, content = params.content)
            return dataResult
        } catch (exception: Throwable){
            throw exception.toPhoneSendResponse()
                ?.let{
                    Failure.SendPhoneError(it)
                }?: exception
        }
    }
}