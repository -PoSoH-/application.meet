/*
 * Copyright (c) 2021 BGC Global Partners
 */

package net.sumra.auth.data.remote.models

import com.google.gson.annotations.SerializedName

object objectDataPhone {

    data class RQTPhone(
        @SerializedName("phone_number")
        val phoneNumber: String,
    )

    data class RSPPhone(
        @SerializedName("code")
        val code    : Int,     /* 200, */
        @SerializedName("exist")
        val exist   : Boolean, /* false, */
        @SerializedName("message")
        val message : String,  /* "This phone number not exists. SMS sent", */
        @SerializedName("title")
        val title   : String,  /* "Validation for number", */
        @SerializedName("type")
        val success : String   /* "success" */
    )

    data class RSP_SMS(
        @SerializedName("code")
        val code      : Int,     /* 200, */
        @SerializedName("message")
        val message   : String,  /* "This phone number not exists. SMS sent", */
        @SerializedName("phone_exist")
        val phoneExist: Boolean, /* false, */
        @SerializedName("user_status")
        val userStatus: Int,     /* "Validation for number", */
        @SerializedName("type")
        val type      : String   /* "success" */
    )
/*
  "code": 201,
  "message": "You are a new user. You need to go through verification. SMS with code sent",
  "phone_exist": false,
  "user_status": 0,
  "type": "success"
*/
}