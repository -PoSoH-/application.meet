///*
// * Copyright (c) 2021 BGC Global Partners
// */
//
//package org.matrix.android.sdk.internal.auth.data.sumra
//
//import com.google.gson.annotations.SerializedName
//
//data class PhoneEnter(
//    @SerializedName("app_uid")
//    val appUID: String,
//    @SerializedName("phone_number")
//    val phoneNumber: String
//)
