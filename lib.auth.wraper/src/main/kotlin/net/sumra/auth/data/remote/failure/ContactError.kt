package net.sumra.auth.data.remote.failure

import com.google.gson.annotations.SerializedName

data class ContactError(
    @SerializedName("type")
    val type: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("message")
    val message: String
)

