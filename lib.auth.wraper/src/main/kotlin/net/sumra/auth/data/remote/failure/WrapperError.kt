package net.sumra.auth.data.remote.failure

data class WrapperError(
    val message : String,
    val code    : Int = 0
)
