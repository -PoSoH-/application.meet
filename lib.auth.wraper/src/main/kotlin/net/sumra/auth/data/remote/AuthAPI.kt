package net.sumra.auth.data.remote

import net.sumra.auth.data.remote.models.EnterShareContacts
import net.sumra.auth.data.remote.models.objectDataCode
import net.sumra.auth.data.remote.models.objectDataPhone
import net.sumra.auth.data.remote.models.objectDataUser
import org.matrix.android.sdk.internal.auth.data.sumra.*
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface AuthAPI {

    /**
    *   Sumra block endpoints
    *   Checks to see if a username is available, and valid, for the server.
    */

    @POST("${rout.verAuth}${rout.sendCode}")
    suspend fun actionSendCode(@Body content: objectDataCode.RQTCode): objectDataCode.RSPCode

    @POST("${rout.verAuth}${rout.sendUsername}")
    suspend fun actionUserName(@Body content: objectDataUser.RQTUsername): objectDataUser.RSPUserName

    @POST("${rout.verAuth}${rout.smsSendPhone}")
    suspend fun actionSharePhone(@Body content: objectDataPhone.RQTPhone): objectDataPhone.RSPPhone

    @POST("${rout.verAuth}${rout.smsSendSms}")
    suspend fun actionSendSMS(@Body content: objectDataPhone.RQTPhone): objectDataPhone.RSP_SMS

    @POST("${rout.verAuth}${rout.setWebhook}")
    suspend fun actionWebhookSet(@Body content: String): String

    @POST("${rout.verAuth}${rout.telWebhook}")
    suspend fun actionWebhookTel(@Body content: String): String

//    @POST("${rout.verAuth}${rout.registration}")
//    suspend fun actionAuthenticate(@Body params: UserEnterParams): PhoneSendAvailable


    @POST("${rout.verContact}/import/json")
    suspend fun actionShareContacts(
        @Header("user-id") authHeader: String,  // use user_id
//        @Header("Authorization") authHeader: String,  // use token
        @Body content: EnterShareContacts): SendContactAvailable

    @POST("${rout.verAuth}loginWithSumraId")
    suspend fun actionAuthBySumraId(@Body params: objectDataSumraID.SumraIDEnterParams): String

}