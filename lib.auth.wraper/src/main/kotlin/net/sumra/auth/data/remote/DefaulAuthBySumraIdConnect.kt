package net.sumra.auth.data.remote

import net.sumra.auth.data.remote.failure.toPhoneSendResponse
import org.matrix.android.sdk.internal.auth.data.sumra.*

internal interface TaskSendAuthBySumraId: Task<TaskSendAuthBySumraId.Params, RemoteResponse> {
    data class Params(
        val content: objectDataSumraID.SumraIDEnterParams
    )
}

internal class DefaultAuthBySumraId (private val authAPI: AuthAPI): TaskSendAuthBySumraId {
    override suspend fun execute(params: TaskSendAuthBySumraId.Params): RemoteResponse {
        try{
            val dataResult = authAPI.actionAuthBySumraId(params = params.content)
            return RemoteResponse.Data(body = dataResult)
        }catch (exception: Throwable){
            throw exception.toPhoneSendResponse()
                ?.let{
                    return RemoteResponse.Fail(fail = it)
                }?: exception
        }
    }
}