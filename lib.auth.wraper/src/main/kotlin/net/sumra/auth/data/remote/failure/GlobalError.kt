package net.sumra.auth.data.remote.failure

sealed class GlobalError{
    data class InvalidToken(val softLogout: Boolean) : GlobalError()
}
