package net.sumra.auth.data.remote

import net.sumra.auth.data.remote.failure.Failure
import net.sumra.auth.data.remote.failure.toErrorResponse
import net.sumra.auth.data.remote.failure.toPhoneSendResponse
import net.sumra.auth.data.remote.models.objectDataUser

internal interface TaskWebhookTel: Task<TaskWebhookTel.Params, RemoteResponse> {
    data class Params(
        val content: String
    )
}

internal class DefWebhookTelExec (private val authAPI: AuthAPI): TaskWebhookTel {
    override suspend fun execute(params: TaskWebhookTel.Params): RemoteResponse {
        try{
            val dataResult = authAPI.actionWebhookTel (content = params.content)
            return RemoteResponse.Data(dataResult)
        }catch (exception: Throwable){
            throw exception.toErrorResponse()
                ?.let{
                    Failure.SendPhoneError(it)
                }?: exception
        }
    }
}
