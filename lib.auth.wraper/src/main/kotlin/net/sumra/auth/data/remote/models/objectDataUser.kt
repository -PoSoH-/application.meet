/*
 * Copyright (c) 2021 BGC Global Partners
 */

package net.sumra.auth.data.remote.models

import com.google.gson.annotations.SerializedName

object objectDataUser {

    data class RQTUsername ( /* request */
        @SerializedName("username")
        val username: String,/* "AlligatorCool", */
        @SerializedName("sid")
        val sidCode : String /* "SM9ea5e4ff476248cd893d622dec48492b" */
    )


    data class RSPUserName (     /* response */
        @SerializedName("code")
        val code       : Int,    /* 200, */
        @SerializedName("message")
        val message    : String, /* "User was successfully activated.", */
        @SerializedName("title")
        val user_status: Int,    /* 1, */
        @SerializedName("type")
        val type       : String, /* "success" */
        @SerializedName("token")
        val token      : String, /* "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzaWQiOiI0MzI3NjU3MTMiLCJ1c2VybmFtZSI6IkFsbGlnYXRvckJMVUUiLCJleHAiOjE2MzI5OTI0NTB9.1XQar4BOsNFg9k34yzD3P-jB0HD2Du8ZhWojUw8_d0E", */
        @SerializedName("user_id")
        val userID     : String
    )

    /*
    {
  "code": 200,
  "message": "User was successfully activated.",
  "user_status": 1,
  "type": "success",
  "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzaWQiOiJTTTllYTVlNGZmNDc2MjQ4Y2Q4OTNkNjIyZGVjNDg0OTJiIiwidXNlcm5hbWUiOiJBbGxpZ2F0b3JDb29sIiwiZXhwIjoxNjMyOTk2OTA3fQ.QxAXV6XL2BgOcNWGunERY8aM3pl2uJblwSV3q42QB-E"
}
    */

}