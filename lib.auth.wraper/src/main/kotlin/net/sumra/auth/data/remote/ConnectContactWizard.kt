package net.sumra.auth.data.remote

import net.sumra.auth.data.remote.models.EnterShareContacts
import org.matrix.android.sdk.internal.auth.data.sumra.*

interface ConnectContactWizard {

    suspend fun actionContactsShare(authValid: String,
                                    appUID   : String,
                                    contacts : EnterShareContacts): SendContactAvailable

}