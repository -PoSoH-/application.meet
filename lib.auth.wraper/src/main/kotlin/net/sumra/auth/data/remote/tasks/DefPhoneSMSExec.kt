package net.sumra.auth.data.remote

import net.sumra.auth.data.remote.failure.toErrorResponse
import net.sumra.auth.data.remote.models.objectDataPhone

internal interface TaskPhoneSMS: Task<TaskPhoneSMS.Params, RemoteResponse> {
    data class Params(
        val content: objectDataPhone.RQTPhone
    )
}

internal class DefPhoneSMSExec (private val authAPI: AuthAPI): TaskPhoneSMS {
    override suspend fun execute(params: TaskPhoneSMS.Params): RemoteResponse {
        try {
            val dataResult = authAPI.actionSendSMS(content = params.content)
            return RemoteResponse.Data(body = dataResult)
        } catch (exception: Throwable) {
            throw exception.toErrorResponse()
                ?.let{
                    return RemoteResponse.Fail(fail = it)
                }?: exception
        }
    }
}