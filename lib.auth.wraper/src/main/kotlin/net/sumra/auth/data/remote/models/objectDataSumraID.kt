/*
 * Copyright 2021 BGC Global Partners
 */

package org.matrix.android.sdk.internal.auth.data.sumra

import com.google.gson.annotations.SerializedName

object objectDataSumraID {

        data class SumraIDEnterParams(
                @SerializedName("password")
                val password: String,
                @SerializedName("username")
                val username: String,
                @SerializedName("app_uid")
                val appUID: String
                )

}
