package net.sumra.auth.data.remote.failure

import java.io.IOException

sealed class Failure (cause: Throwable? = null): Throwable(cause = cause){
    data class Unknown(val throwable: Throwable? = null) : Failure(throwable)
    data class Cancelled(val throwable: Throwable? = null) : Failure(throwable)
    data class NetworkConnection(val ioException: IOException? = null) : Failure(ioException)

    data class ServerError(val error: WrapperError, val httpCode: Int) : Failure(RuntimeException(error.toString()))
    data class ContactError(val error: ContactError, val httpCode: Int): Failure(RuntimeException(error.toString()))

    data class SendPhoneError(val error: WrapperError) : Failure(RuntimeException(error.toString()))

    data class OtherServerError(val errorBody: String, val httpCode: Int) : Failure(RuntimeException("HTTP $httpCode: $errorBody"))
}

