package net.sumra.auth.data.remote.failure

sealed class ServerError{
    data class InvalidToken(val softLogout: Boolean) : ServerError()
}
