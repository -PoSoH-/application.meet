package net.sumra.auth.data.remote

interface ConnectAuthWizard {
    suspend fun actionSharePhone(phoneNumber: String,
                                 appUID     : String): RemoteResponse //objectDataPhone.RSPPhone

    suspend fun actionSendSMS(phoneNumber: String,
                              appUID     : String): RemoteResponse //UserRegisteredParams

    suspend fun actionSendCode(authCode: String,
                               appUID      : String): RemoteResponse //UserRegisteredParams

    suspend fun actionUserName(sidCode : String,
                               username: String,
                                appUID : String): RemoteResponse //UserRegisteredParams


    suspend fun actionWebhookSet(): RemoteResponse //UserRegisteredParams
    suspend fun actionWebhookTel(): RemoteResponse //UserRegisteredParams

    suspend fun actionAuthBySumraId(password: String,
                                    username: String,
                                    appUID : String): RemoteResponse //UserRegisteredParams
}