package net.sumra.auth.data.remote

import com.google.gson.Gson
import dagger.Lazy
import net.sumra.auth.helpers.ensureTrailingSlash
import okhttp3.Call
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class RetrofitFactory @Inject constructor(private val gson: Gson) {

    fun createForAuth(okHttpClient: OkHttpClient, baseUrl: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl.ensureTrailingSlash())
            .callFactory(object : Call.Factory {
                override fun newCall(request: Request): Call {
                    return okHttpClient.newCall(request)
                }
            })
            .addConverterFactory(UnitConverterFactory)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    fun createForContact(okHttpClient: OkHttpClient, baseUrl: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl.ensureTrailingSlash())
            .callFactory(object : Call.Factory {
                override fun newCall(request: Request): Call {
                    return okHttpClient.newCall(request)
                }
            })
            .addConverterFactory(UnitConverterFactory)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }
}
