package net.sumra.auth.data.remote.failure

import com.google.gson.Gson
import com.google.gson.GsonBuilder

object GsonProvider {
    private val gson: Gson = GsonBuilder()
        .create()

    fun providesGson(): Gson {
        return gson
    }
}