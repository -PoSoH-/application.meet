package net.sumra.auth.data.remote.models

import com.google.gson.annotations.SerializedName

data class EnterShareContacts(
    @SerializedName("group_id")
    val groupId: String?,
    @SerializedName("contacts")
    val listsContact: List<ShareContact>
)

data class ShareContact(
//    @SerializedName("id")           val userId: Long? = null,
    @SerializedName("display_name") val displayName: String,
    @SerializedName("avatar")       val avatar: String?, // this field for uploaded contact avatar into Base64 format...
    @SerializedName("phones")       val phones: List<String>,
    @SerializedName("emails")       val emails: List<String>,
    @SerializedName("is_shared")    var isShared: Boolean = false  // Используется для отметки контактов которые можна отдавать пользователю на стороне сервера
)
/***
{
    "group_id": "3d9319c9-59ee-3efc-900f-eec98811c96b",
    "contacts": [
        {
            "display_name": "",
            "avatar": "",
            "phones": [
                "+3521234562545"
            ],
            "emails": [
                "client1@client.com"
            ],
            "is_shared": false
        }
    ]
}
***/