package net.sumra.auth.data.remote

import net.sumra.auth.data.remote.models.EnterShareContacts
import org.matrix.android.sdk.internal.auth.data.sumra.*

internal class DefaultContactWizard (
    authApi: AuthAPI): ConnectContactWizard {

    private val executeShareContact  = DefaultShareConnect(authApi)

    override suspend fun actionContactsShare(authValid: String, appUID: String, contacts: EnterShareContacts): SendContactAvailable {
        return executeShareContact.execute(TaskShareConnect.Params(authValid = authValid, content = contacts))
    }
}
