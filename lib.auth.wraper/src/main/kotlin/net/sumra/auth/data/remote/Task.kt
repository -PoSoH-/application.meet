package net.sumra.auth.data.remote

internal interface Task <PARAMS, RESULT> {
    suspend fun execute(params: PARAMS): RESULT
}