package net.sumra.auth.data.remote

import net.sumra.auth.data.remote.failure.toErrorResponse
import net.sumra.auth.data.remote.models.objectDataUser

internal interface TaskSendUsername: Task<TaskSendUsername.Params, RemoteResponse> {
    data class Params(
        val content: objectDataUser.RQTUsername
    )
}

internal class DefUsernameExec (private val authAPI: AuthAPI): TaskSendUsername {
    override suspend fun execute(params: TaskSendUsername.Params): RemoteResponse {
        try{
            val dataResult = authAPI.actionUserName (content = params.content)
            return RemoteResponse.Data(dataResult)
        }catch (exception: Throwable){
            throw exception.toErrorResponse()
                ?.let{
                    return RemoteResponse.Fail(it)
                }?: exception
        }
    }
}
