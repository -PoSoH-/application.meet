///*
// * Copyright (c) 2021 BGC Global Partners
// */
//
//package net.sumra.auth.auth.data.remote.models
//
//import net.sumra.auth.data.remote.failure.ContactError
//import net.sumra.auth.data.remote.models.objectDataPhone
//
//sealed class PhoneResult {
//    data class Data(val result: objectDataPhone.RSPPhone): PhoneResult()
//    data class Fail(val result: ContactError)      : PhoneResult()
//}
//
////data class PhoneSendAvailable(
////    @SerializedName("data")
////    val sso: AccessToken,
////    @SerializedName("success")
////    val success: Boolean
////    ){
////    data class AccessToken(
////        @SerializedName("access_token")
////        val accessToken: String
////    )
////}


