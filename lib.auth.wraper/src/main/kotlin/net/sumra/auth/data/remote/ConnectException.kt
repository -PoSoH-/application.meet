package net.sumra.auth.data.remote

import okhttp3.Response
import java.util.*

class ConnectException(val response: Response): RuntimeException() {

    private var code: Int = 0
    private var other: String? = null

    init {
        getMessage(this.response)
        code = this.response.code
        other = this.response.message
    }

    private fun getMessage(response: Response): String? {
        Objects.requireNonNull(response, "response == null")
        return "HTTP " + response.code + " " + response.message
    }

    fun code(): Int {
        return this.code
    }

    fun message(): String? {
        return this.message
    }

    fun response(): Response {
        return this.response
    }
}