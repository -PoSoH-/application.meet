///*
// * Copyright 2021 BGC Global Partners
// */
//
//package org.matrix.android.sdk.internal.auth.data.sumra
//
//import com.google.gson.annotations.SerializedName
//
//data class UserRegisteredParams(
//        @SerializedName("data")
//        val content: AppInformation,
//        @SerializedName("success")
//        val success: Boolean
//){
//
//}
//
//data class AppInformation(
//        @SerializedName("access_token")
//        val accessTokenSso: String,
//        @SerializedName("device_id")
//        val deviceId: String,
//        @SerializedName("home_server")
//        val homeServer: String,
//        @SerializedName("user_id")
//        val userId: String,
//        @SerializedName("app_err")
//        val appErr: String?
//)


