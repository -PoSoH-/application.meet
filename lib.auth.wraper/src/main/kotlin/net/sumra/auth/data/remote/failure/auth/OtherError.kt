package net.sumra.auth.data.remote.failure.auth

import com.google.gson.annotations.SerializedName

data class OtherError(
    @SerializedName("error")
    val error: ArrayList<String>,
    @SerializedName("code")
    val code        : Int = 0
)

/*
    {
        "validate_auth_code": false,
        "type": "danger",
        "message": "Code validation not successful"
    }
*/
