package net.sumra.auth.data.remote.failure.auth

import com.google.gson.annotations.SerializedName

data class PhoneError(
    @SerializedName("validate_auth_code")
    val authCode: Boolean,
    @SerializedName("type")
    val type    : String,
    @SerializedName("message")
    val message : String,
    @SerializedName("code")
    val code    : Int = 0
)

/*
    {
        "validate_auth_code": false,
        "type": "danger",
        "message": "Code validation not successful"
    }
*/
