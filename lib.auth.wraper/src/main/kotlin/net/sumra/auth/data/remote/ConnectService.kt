package net.sumra.auth.data.remote

interface ConnectService {
    fun getConnectAuthService()   : ConnectAuthWizard
    fun getConnectContactService(): ConnectContactWizard
}