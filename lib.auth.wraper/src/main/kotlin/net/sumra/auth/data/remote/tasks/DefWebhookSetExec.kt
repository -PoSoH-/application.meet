package net.sumra.auth.data.remote

import net.sumra.auth.data.remote.failure.toErrorResponse

internal interface TaskWebhookSet: Task<TaskWebhookSet.Params, RemoteResponse> {
    data class Params(
        val content: String
    )
}

internal class DefWebhookSetExec (private val authAPI: AuthAPI): TaskWebhookSet {
    override suspend fun execute(params: TaskWebhookSet.Params): RemoteResponse {
        try{
            val dataResult = authAPI.actionWebhookSet (content = params.content)
            return RemoteResponse.Data(dataResult)
        }catch (exception: Throwable){
            throw exception.toErrorResponse()
                ?.let{
                    return RemoteResponse.Fail(it)
                }?: exception
        }
    }
}
