/*
 * Copyright (c) 2021 BGC Global Partners
 */

package net.sumra.auth.data.remote.models

import com.google.gson.annotations.SerializedName

object objectDataCode {

    data class RQTCode(
        @SerializedName("auth_code_from_user")
        val authCode: String,
    )

    data class RSPCode(
        @SerializedName("code")
        val code       : Int,         /* 200 */
        @SerializedName("message")
        val message    : String,      /* Code validation successful */
        @SerializedName("user_status")
        val userStatus : String,      /* Code validation successful */
        @SerializedName("type")
        val type       : String,      /* "success", */
        @SerializedName("sid")
        val sidCode    : String,      /* SM9ea5e4ff476248cd893d622dec48492b */
        @SerializedName("validate_auth_code")
        val validateAuthCode: Boolean /* true */
    )

    /*
    {
  "code": 200,
  "message": "Code validation successful",
  "user_status": 0,
  "type": "success",
  "sid": "SM9ea5e4ff476248cd893d622dec48492b",
  "validate_auth_code": true
}
    */
}