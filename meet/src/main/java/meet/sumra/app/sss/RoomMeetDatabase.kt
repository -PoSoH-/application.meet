//package meet.sumra.app.sss
//
//import androidx.room.Database
//import androidx.room.RoomDatabase
//import meet.sumra.app.sss.dao.HistoryDao
//import meet.sumra.app.sss.dao.MeetChatDao
//import meet.sumra.app.sss.dao.MeetParameterDao
//import meet.sumra.app.sss.dao.SessionDao
//import meet.sumra.app.sss.models.HistoryEntity
//import meet.sumra.app.sss.models.MeetChatEntity
//import meet.sumra.app.sss.models.ParametersEntity
//import meet.sumra.app.sss.models.SessionEntity
//
//@Database(
//    entities = [
//        SessionEntity::class,
//        HistoryEntity::class,
//        ParametersEntity::class,
//        MeetChatEntity::class]
//    , version = 1
//    , exportSchema = false)
//abstract class RoomMeetDatabase : RoomDatabase() {
//    abstract fun sessionDao()   : SessionDao
//    abstract fun historyDao()   : HistoryDao
//    abstract fun meetParametersDao(): MeetParameterDao
//    abstract fun meetСhatDao()  : MeetChatDao

//    companion object {
//
//        private val databaseCallback = object : RoomDatabase.Callback() {
//            override fun onCreate(db: SupportSQLiteDatabase) {
//                super.onCreate(db)
//                Log.d("RoomMeetDatabase", "onCreate")
////            CoroutineScope(Dispatchers.IO).launch {
////                addSampleBooksToDatabase()
////            }
//            }
//        }
//
//        fun buildDataSource(context: Context): RoomMeetDatabase = Room.databaseBuilder(
//            context
//            , RoomMeetDatabase::class.kotlin
//            , ContractTableRoom.database
//        )
//            .fallbackToDestructiveMigration()
////            .allowMainThreadQueries()  //
//            .addCallback(databaseCallback)
//            .build()
//    }
//}