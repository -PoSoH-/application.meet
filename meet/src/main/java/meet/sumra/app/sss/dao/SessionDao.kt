//package meet.sumra.app.sss.dao
//
//import androidx.room.*
//import meet.sumra.app.sss.contracts.SessionContract
//import meet.sumra.app.sss.models.SessionEntity
//
//@Dao
//interface SessionDao {
//
//    @Query(SessionContract.fetch)
//    fun fetchSession(): List<SessionEntity>
//
//    @Insert(entity = SessionEntity::class, onConflict = OnConflictStrategy.REPLACE)
//    fun insertSession(cardEntity: SessionEntity)
//
//    @Delete(entity = SessionEntity::class)
//    fun deleteSession(cardEntity: SessionEntity)
//
//}