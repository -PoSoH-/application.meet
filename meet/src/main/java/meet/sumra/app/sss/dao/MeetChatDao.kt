//package meet.sumra.app.sss.dao
//
//import androidx.room.Dao
//import androidx.room.Insert
//import androidx.room.OnConflictStrategy
//import androidx.room.Query
//import meet.sumra.app.sss.contracts.MeetChatContract
//import meet.sumra.app.sss.models.MeetChatEntity
//
//@Dao
//interface MeetChatDao {
//
//    @Query(MeetChatContract.fetchAllChatWithMeet)
//    fun fetchAllChatWithMeet(meetId: Long): List<MeetChatEntity>
//
//    @Query(MeetChatContract.fetchChat)
//    fun fetchWithId(chatId: Long): MeetChatEntity
//
//    @Insert(entity = MeetChatEntity::class, onConflict = OnConflictStrategy.IGNORE)
//    fun insertChat(chat: MeetChatEntity)
//}