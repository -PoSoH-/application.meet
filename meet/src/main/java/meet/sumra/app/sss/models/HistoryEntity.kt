//package meet.sumra.app.sss.models
//
//import androidx.room.Entity
//import androidx.room.PrimaryKey
//import meet.sumra.app.sss.contracts.ContractTableRoom
//
//@Entity(tableName = ContractTableRoom.meet_tbl_history)
//data class HistoryEntity (
//    @PrimaryKey ( autoGenerate = true)
//    val historyId   : Long,
//    val meetName    : String,
//    val sessionMeet : String,
//
//    val startTimeMeet : Long,
//    val finalTimeMeet : Long,
//
//    val meetParameterId : Long,
//    val meetTextChatId  : Long
//)