//package meet.sumra.app.sss.dao
//
//import androidx.room.*
//import meet.sumra.app.sss.contracts.HistoryContract
//import meet.sumra.app.sss.models.HistoryEntity
//
//
//@Dao
//interface HistoryDao {
//
//    @Query(HistoryContract.fetchAll)
//    fun fetchAllHistory(): List<HistoryEntity>
//
//    @Query(HistoryContract.fetch)
//    fun fetchHistory(hId: Long): HistoryEntity
//
//    @Insert(entity = HistoryEntity::class, onConflict = OnConflictStrategy.REPLACE)
//    fun insertHistory(cardEntity: HistoryEntity)
//
//    @Update(entity = HistoryEntity::class)
//    fun updateHistory(cardEntity: HistoryEntity)
//
//    @Delete(entity = HistoryEntity::class)
//    fun deleteHistory(cardEntity: HistoryEntity)
//
//    @Query(HistoryContract.deleteWithId)
//    fun deleteHistoryWithId(hId: Long)
//
//}
