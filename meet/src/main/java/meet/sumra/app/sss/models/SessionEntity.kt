//package meet.sumra.app.sss.models
//
//import androidx.room.Entity
//import androidx.room.PrimaryKey
//import meet.sumra.app.sss.contracts.ContractTableRoom
//
//@Entity(tableName = ContractTableRoom.meet_tbl_session)
//data class SessionEntity (
//    @PrimaryKey (autoGenerate = false)
//    val id: Long? = null,  // this place set id user
//    var session: String,
//    var code: String,
//    var userName: String,
//    var homeServer: String,
//    var homeUserId: String,
//    var avatar: String? // form Base64 format
//)