//package meet.sumra.app.sss.models
//
//import androidx.room.Entity
//import androidx.room.PrimaryKey
//import meet.sumra.app.sss.contracts.ContractTableRoom
//
//@Entity(tableName = ContractTableRoom.meet_tbl_meet_chat)
//data class MeetChatEntity (
//    @PrimaryKey (autoGenerate = true)
//    val meetChatId : Long,
//    val linkMeetId : Long,
//    val startChatTime : Long,
//    val finalChatTime : Long,
//    val messageChat: String
//)