//package meet.sumra.app.sss.dao
//
//import androidx.room.Dao
//import androidx.room.Insert
//import androidx.room.OnConflictStrategy
//import androidx.room.Query
//import meet.sumra.app.sss.contracts.MeetParamsContract
//import meet.sumra.app.sss.models.ParametersEntity
//
//@Dao
//interface MeetParameterDao {
//    @Query(MeetParamsContract.fetchParams)
//    fun fetchWithId(parameterId: Long): ParametersEntity
//
//    @Insert(entity = ParametersEntity::class, onConflict = OnConflictStrategy.REPLACE)
//    fun insertParameter(parameterId: ParametersEntity)
//}