package meet.sumra.app

import android.app.Application
import meet.sumra.app.di.component.*
import meet.sumra.app.di.module.*
import meet.sumra.app.utils.setup.ThemeUtils
import sdk.net.meet.net.di.components.DaggerMeetComponent
import sdk.net.meet.net.di.components.MeetComponent
import xyn.meet.storange.RoomMeetDatabase

class AppMeet: Application() {

    private var netComponent : MeetComponent? = null
    private var meetComponent: ComponentViewModel? = null
//    private var statisticComponent: ComponentViewStatistic? = null
    private var meetDatabase : RoomMeetDatabase? = null


//    @Inject lateinit var networkServices: NetworkServices

    companion object {
        private lateinit var instance : AppMeet

        fun getInstance() = instance
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        initRoom()
        initDagger()
        ThemeUtils.init(this)
    }

    private fun initRoom() {
        meetDatabase = RoomMeetDatabase.buildDataSource(this)
    }

    private fun initDagger() {

        val netComponent = DaggerMeetComponent.factory()

        val apiComponent = DaggerComponentServerApi.builder()
            .moduleServerApi(ModuleServerApi(netComponent.create(this).getNetworkServices()))
            .build()

        val databaseComponent = DaggerComponentDatabase.builder()
            .roomDatabaseModule(RoomDatabaseModule(this.meetDatabase!!))
            .build()

        val repositoryComponent = DaggerComponentRepository.builder()
            .componentServerApi(apiComponent)
            .componentDatabase(databaseComponent)
            .repositoryModule(RepositoryModule())
            .build()

        meetComponent = DaggerComponentViewModel.builder()
            .componentRepository(repositoryComponent)
            .moduleViewModel(ModuleViewModel(this))
            .build()
//
//        statisticComponent = DaggerComponentViewStatistic.buider()
//            .componentRepository(repositoryComponent)
//            .moduleViewModelStatistic(ModuleViewModelStatistic(this))
//            .build()
    }

    fun getViewModelComponent(): ComponentViewModel {
        return this.meetComponent!!
    }
}