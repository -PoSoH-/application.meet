package meet.sumra.app.check_ref

import android.annotation.SuppressLint
import android.content.Context
import android.provider.Settings
import android.os.Build
import com.google.gson.Gson
import meet.sumra.app.BuildConfig
import meet.sumra.app.R
import meet.sumra.app.utils.LogUtil
import okio.BufferedSource
import okio.buffer
import okio.source
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec

data class RefBody(val data: String, val `val`: String)

data class REF constructor(
    var referrerUrl: String? = null,
    var referrerClickTime: Long?  = null,
    var appInstallTime: Long?  = null,
    var instantExperienceLaunched: Boolean? = null){

    private val KEY_PREF_DATA           = "Sumra:1ad49bc1-3dc9-48bb-9fd5-aae4971a136a#"
    private val KEY_PREF_REF_URL        = "Refer:a673b76e-d173-4bcb-b7b4-b3a0a454ba69#"
    private val KEY_PREF_REF_CLICK_TIME = "Refer:cbfd4fb5-c213-419f-9910-caf4d0ae31bf#"
    private val KEY_PREF_REF_ALL_TIME   = "Refer:fabb7cef-870e-417f-af45-f9711335dcd8#"
    private val KEY_PREF_REF_EXP        = "Refer:6711ebae-d672-4f2d-9c1e-5c82d52c4dc9#"

    fun write(ctx: Context){
        val pref = ctx.getSharedPreferences(KEY_PREF_DATA, Context.MODE_PRIVATE)
        val editable = pref.edit()
        editable.putString(KEY_PREF_REF_URL, referrerUrl!!)
        editable.putLong(KEY_PREF_REF_CLICK_TIME, referrerClickTime!!)
        editable.putLong(KEY_PREF_REF_ALL_TIME, appInstallTime!!)
        editable.putBoolean(KEY_PREF_REF_EXP, instantExperienceLaunched!!)
        editable.apply()
    }

    fun read(ctx: Context){
        val pref = ctx.getSharedPreferences(KEY_PREF_DATA, Context.MODE_PRIVATE)
        referrerUrl = pref.getString(KEY_PREF_REF_URL, null)
        referrerClickTime = pref.getLong(KEY_PREF_REF_CLICK_TIME, 0L)
        appInstallTime = pref.getLong(KEY_PREF_REF_ALL_TIME, 0L)
        instantExperienceLaunched = pref.getBoolean(KEY_PREF_REF_EXP, false)
    }
}

data class DEV(
    var androidId: String? = null            // Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    , var deviceSerialNumber: String? = null // : String = Build.SERIAL
    , var deviceManufactured: String? = null // : String = Build.MANUFACTURER //getString("ro.product.manufacturer")
    , var deviceBrand: String? = null        // : String = Build.BRAND  ///: String = Build.getString("ro.product.brand")
    , var deviceModel: String? = null        // : String = Build.MODEL
    , var deviceBootloader: String? = null   // : String = Build.BOOTLOADER
    , var packageName: String? = null        // : String = getPackageName()
    , var applicationID: String? = null      // : String = BuildConfig.APPLICATION_ID
    , var versionName: String? = null        // : String = BuildConfig.VERSION_NAME
    , var versionCode: String? = null        // : String = BuildConfig.VERSION_CODE.toString()
    , var versionAndroidSDK : Int?  = null ) {

    fun read(ctx: Context){
        androidId = Settings.Secure.getString(ctx.contentResolver, Settings.Secure.ANDROID_ID);
        deviceSerialNumber = Build.SERIAL
        deviceManufactured = Build.MANUFACTURER //getString("ro.product.manufacturer")
        deviceBrand = Build.BRAND  ///: String = Build.getString("ro.product.brand")
        deviceModel = Build.MODEL
        deviceBootloader = Build.BOOTLOADER
        packageName = ctx.getPackageName()
        applicationID = BuildConfig.APPLICATION_ID
        versionName = BuildConfig.VERSION_NAME
        versionCode = BuildConfig.VERSION_CODE.toString()
        versionAndroidSDK = Build.VERSION.SDK_INT
    }
}

data class VAL(
    var referrerUrl: String? = null,
    var referrerClickTime: Long? = null,
    var appInstallTime: Long? = null,
    var instantExperienceLaunched: Boolean? = null,
    var androidId: String? = null,
    var deviceSerialNumber: String? = null,
    var deviceManufactured: String? = null,
    var deviceBrand: String? = null,
    var deviceModel: String? = null,
    var deviceBootloader: String? = null,
    var packageName: String? = null,
    var applicationID: String? = null,
    var versionName: String? = null,
    var versionCode: String? = null,
    var versionAndroidSDK : Int? = null
) {
    constructor(ref: REF, dev: DEV) : this(){
        referrerUrl = ref.referrerUrl
        referrerClickTime = ref.referrerClickTime
        appInstallTime = ref.appInstallTime
        instantExperienceLaunched = ref.instantExperienceLaunched
        androidId = dev.androidId
        deviceSerialNumber = dev.deviceSerialNumber
        deviceManufactured = dev.deviceManufactured
        deviceBrand = dev.deviceBrand
        deviceModel = dev.deviceModel
        deviceBootloader = dev.deviceBootloader
        packageName = dev.packageName
        applicationID = dev.applicationID
        versionName = dev.versionName
        versionCode = dev.versionCode
        versionAndroidSDK = dev.versionAndroidSDK
    }

    private fun toJson(): String { return "" /*** Gson().toJson(this) ***/ }

    @SuppressLint("GetInstance")
    fun tp(ctx: Context) : RefBody {
        val ALGORITHM = "AES/ECB/PKCS5PADDING"
        val _t = readFile(ctx)
        val BYTE_KEY = _t.yek.toByteArray()
        val BYTE_PORNO = toJson().toByteArray()
        val cipher = Cipher.getInstance(ALGORITHM)
        cipher.init(Cipher.ENCRYPT_MODE, SecretKeySpec(BYTE_KEY, ALGORITHM))
        val encodedValue = cipher.doFinal(BYTE_PORNO)
        val encodeJ = android.util.Base64.encodeToString(encodedValue
            , android.util.Base64.DEFAULT)
        LogUtil.error("encode result:", encodeJ)
        return RefBody(_t.rev, encodeJ)
    }

    private fun readFile(ctx: Context): IN{
        val source: BufferedSource = ctx.resources.openRawResource(R.raw.sumrameet_byte).source().buffer()
        val result = source.readUtf8()
        source.close()
        val _R = Gson().fromJson<IN>(result, IN::class.java)
        return _R
    }
}

data class IN(val rev: String, val yek: String)
