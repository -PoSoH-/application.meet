package meet.sumra.app.check_ref

import android.app.Application
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.android.installreferrer.api.InstallReferrerClient
import com.android.installreferrer.api.InstallReferrerStateListener
import meet.sumra.app.utils.LogUtil
import net.sumra.auth.helpers.Finals

class CheckReferral constructor(val application: Application){

    val mPrefs : SharedPreferences
    val mDeviceInfo : Boolean
    var mVal: VAL? = null; get() {return field}

    init {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(application)
        mDeviceInfo = mPrefs.getBoolean(Finals.KEY_REFERRER_PREF, false)

        if (!mDeviceInfo) {
            // here you can launch another activity if you like

            val editor: SharedPreferences.Editor = mPrefs.edit();
            editor.putBoolean(Finals.KEY_REFERRER_PREF, true);
            editor.apply(); // Very important to save the preference

            val referrerClient: InstallReferrerClient =
                InstallReferrerClient.newBuilder(application).build();
            referrerClient.startConnection(object : InstallReferrerStateListener {

                override fun onInstallReferrerSetupFinished(responseCode: Int) {
                    when (responseCode) {
                        InstallReferrerClient.InstallReferrerResponse.OK -> {
                            mVal = rRefCell(referrerClient)
                        }
                        InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED -> {
                            LogUtil.info("IEL: ", "FEATURE_NOT_SUPPORTED")
                        }
                        InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE -> {
                            LogUtil.info("IEL: ", "SERVICE_UNAVAILABLE")
                        }
                    }
                }

                override fun onInstallReferrerServiceDisconnected() {
                    // Try to restart the connection on the next request to
                    // Google Play by calling the startConnection() method.
                }
            })
        }
    }

    private fun rRefCell(referrerClient: InstallReferrerClient?): VAL{
        val response = referrerClient?.getInstallReferrer();
        val referrerUrl = response?.getInstallReferrer ();
        val referrerClickTime = response?.getReferrerClickTimestampSeconds();
        val appInstallTime = response?.getInstallBeginTimestampSeconds();
        val instantExperienceLaunched = response?.getGooglePlayInstantParam();
        val ref = REF(
            referrerUrl =  referrerUrl
            , referrerClickTime = referrerClickTime
            , appInstallTime = appInstallTime
            , instantExperienceLaunched = instantExperienceLaunched
        )
        ref.write(ctx = application)
        val dev = DEV()
        dev.read(application)
        val neo = VAL(ref, dev)
        return neo
//        mPresenter.sendReferrals(neo.tp(application))
    }

}