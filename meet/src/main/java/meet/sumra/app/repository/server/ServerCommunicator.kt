package meet.sumra.app.repository.server

import dagger.assisted.AssistedInject
import sdk.net.meet.api.NetworkServices
import sdk.net.meet.api.contacts.ContactWizard
import sdk.net.meet.api.meet_calendar.IWizardMeetCalendar
import sdk.net.meet.api.meet_profile.IWizardMeetProfile
import sdk.net.meet.api.meet_rooms.IWizardMeetRooms
import sdk.net.meet.api.meet_session.IWizardMeetSession
import sdk.net.meet.api.referral.ReferralWizard
import sdk.net.meet.net.meet_calendar.WizardMeetCalendar
import sdk.net.meet.net.meet_rooms.WizardMeetRooms
import sdk.net.meet.net.meet_session.WizardMeetSession
import javax.inject.Inject

class ServerCommunicator @Inject constructor(
    private val networkServices: NetworkServices) {

    val currentReferralConnect: ReferralWizard?
        get() = networkServices.getReferralWizard()

    val currentContactConnect: ContactWizard?
        get() = networkServices.getContactWizard()

    val currentRoomConnect: IWizardMeetRooms?
        get() = networkServices.getWizardRooms()

    val currentSessionConnect: IWizardMeetSession?
        get() = networkServices.getWizardSession()

    val currentCalendarConnect: IWizardMeetCalendar?
        get() = networkServices.getWizardCalendar()

    val currentProfileConnect: IWizardMeetProfile?
        get() = networkServices.getWizardProfile()

    companion object {
        private val DEFAULT_TIMEOUT = 10
        private val DEFAULT_RETRY_ATTEMPTS = 4L
    }

    suspend fun refresh(tokenSSO: String): String {
//        SessionEntity()
//        mService.refresh()
//        tokenSSO = UUID.randomUUID().toString()
        return ""
    }

    /***  referrals functions  ***/
}