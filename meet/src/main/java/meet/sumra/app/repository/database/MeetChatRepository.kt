package meet.sumra.app.repository.database

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
//import meet.sumra.app.sss.RoomMeetDatabase
//import meet.sumra.app.sss.dao.MeetChatDao
//import meet.sumra.app.sss.models.MeetChatEntity
import xyn.meet.storange.RoomMeetDatabase
import xyn.meet.storange.dao.MeetChatDao
import xyn.meet.storange.models.MeetChatEntity
import javax.inject.Inject

class MeetChatRepository @Inject constructor(meetDatabase: RoomMeetDatabase) {

    private val meetChatDAO: MeetChatDao

    init {
        meetChatDAO  = meetDatabase.meetСhatDao()
    }

    fun fetchMeetChat(chatId: Long): MeetChatEntity {
        return meetChatDAO.fetchWithId(chatId)
    }

    fun fetchAllChatWithMeet(meetId: Long): List<MeetChatEntity> {
        return meetChatDAO.fetchAllChatWithMeet(meetId)
    }

    fun insertSession(current: MeetChatEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            meetChatDAO.insertChat(current)
        }
    }
}