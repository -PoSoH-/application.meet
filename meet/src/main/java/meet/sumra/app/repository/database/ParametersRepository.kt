package meet.sumra.app.repository.database

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
//import meet.sumra.app.sss.RoomMeetDatabase
//import meet.sumra.app.sss.dao.MeetParameterDao
//import meet.sumra.app.sss.models.ParametersEntity
import xyn.meet.storange.RoomMeetDatabase
import xyn.meet.storange.dao.MeetParameterDao
import xyn.meet.storange.models.ParametersEntity

import javax.inject.Inject

class ParametersRepository @Inject constructor (meetDatabase: RoomMeetDatabase) {

    private val parametersDAO: MeetParameterDao

    init {
        parametersDAO  = meetDatabase.meetParametersDao()
    }

    fun fetchParameters(parameterId: Long): ParametersEntity {
        return parametersDAO.fetchWithId(parameterId)
    }

    fun insertParameters(parameter: ParametersEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            parametersDAO.insertParameter(parameter)
        }
    }

//    fun deleteBook(book: BookEntity) {
//        CoroutineScope(Dispatchers.IO).launch {
//            bookDAO.deleteBook(book)
//        }
//    }
//
//    fun updateBook(book: BookEntity) {
//        CoroutineScope(Dispatchers.IO).launch {
//            bookDAO.updateBook(book)
//        }
//    }
}