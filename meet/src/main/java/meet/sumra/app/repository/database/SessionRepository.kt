package meet.sumra.app.repository.database

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import xyn.meet.storange.RoomMeetDatabase
import xyn.meet.storange.dao.SessionDao
import xyn.meet.storange.models.SessionEntity
import javax.inject.Inject

class SessionRepository @Inject constructor (meetDatabase: RoomMeetDatabase) {

    private val jobs = CoroutineScope(Dispatchers.IO)
    private val sessionDAO: SessionDao

    init {
        sessionDAO  = meetDatabase.sessionDao()
    }

    fun fetchSession(): SessionEntity? {
        return sessionDAO.fetchSession()[0]
    }

    fun insertSession(session: SessionEntity) {
        sessionDAO.insertSession(session)
    }
}