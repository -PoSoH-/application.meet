package meet.sumra.app.repository

import android.graphics.Color
import android.net.Uri
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.PercentFormatter
import com.google.gson.Gson
import kotlinx.coroutines.*
import meet.sumra.app.CONST
import meet.sumra.app.check_ref.VAL
import meet.sumra.app.data.mdl.*
import meet.sumra.app.data.mdl.MockDataBase.readDataBaseCalendar
import meet.sumra.app.data.mdl.MockDataBase.searchDataBase
import meet.sumra.app.data.mdl.earnings.cards
import meet.sumra.app.data.mdl.plaza.*
import meet.sumra.app.data.mdl.plaza.mock.Emulation
import meet.sumra.app.data.configs.modelDivitsRewardsBonus.convertToBarData
import meet.sumra.app.data.configs.modelDivitsRewardsBonus.getDivitsRewardStatistic
import meet.sumra.app.data.configs.setupStatisticCharts.fetchBarData
import meet.sumra.app.data.configs.setupStatisticCharts.fetchBarOverviewData
import meet.sumra.app.data.configs.setupStatisticCharts.fetchLineData
import meet.sumra.app.data.mdl.statistic.*
import meet.sumra.app.data.pojo.JsonPojoBanner
import meet.sumra.app.data.pojo.RespStatistic
import meet.sumra.app.data.pojo.ResponseRepository
import meet.sumra.app.data.pojo.contacts.UserContactCreate
import meet.sumra.app.data.pojo.plaza.MockJsonPlaza
import meet.sumra.app.ext.ClrExt
import meet.sumra.app.interfaces.sealeds.SChartInterval
import meet.sumra.app.repository.server.ServerCommunicator
import meet.sumra.app.utils.ContactConfig
import meet.sumra.app.vm.BaseViewModel
import net.sumra.auth.Session
import sdk.net.meet.BuildConfig
import sdk.net.meet.LogUtil
import sdk.net.meet.api.contacts.ApiGetConfig
import sdk.net.meet.api.contacts._ResponseContacts
import sdk.net.meet.api.contacts.request.FromAddContact
import sdk.net.meet.api.contacts.response.*
import sdk.net.meet.api.contacts.tasks.TaskAllContacts
import sdk.net.meet.api.contacts.tasks.TaskCategory
import sdk.net.meet.api.failure.Failure
import sdk.net.meet.api.meet_calendar.TaskRespCalendar
import sdk.net.meet.api.meet_calendar.request.FromCreateCalendar
import sdk.net.meet.api.meet_calendar.response.FetchMeetCalendar
import sdk.net.meet.api.meet_calendar.response.MeetCalendar
import sdk.net.meet.api.meet_calendar.response.MeetCalendarAttr
import sdk.net.meet.api.meet_profile.TaskProfileResult
import sdk.net.meet.api.meet_profile.response.FetchMeetProfile
import sdk.net.meet.api.meet_profile.response.MeetUserInfo
import sdk.net.meet.api.meet_rooms.TaskRespMeetRoom
import sdk.net.meet.api.meet_rooms.request.FromMeetByInvite
import sdk.net.meet.api.meet_rooms.request.FromMeetRooms
import sdk.net.meet.api.meet_rooms.response.*
import sdk.net.meet.api.meet_session.TaskRespMeetSession
import sdk.net.meet.api.meet_session.request.FromMeetSession
import sdk.net.meet.api.meet_session.response.FetchMeetSessionCrt
import sdk.net.meet.api.meet_session.response.FetchMeetSessionUpd
import sdk.net.meet.api.referral.objModelReferralCode
import sdk.net.meet.api.referral.responses.*
import xyn.meet.storange.RoomMeetDatabase
import xyn.meet.storange.models.HistoryEntity
import xyn.meet.storange.models.SessionEntity
import java.lang.RuntimeException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.CancellationException
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.random.Random


class Repository @Inject constructor( private val networkServices: ServerCommunicator,
                                      private val meetDatabase: RoomMeetDatabase,
                                      val mRepoState: MutableLiveData<ResponseRepository> = MutableLiveData<ResponseRepository>()
){

    internal interface BackListener {
        fun toResult(r: ResponseRepository)
    }

//    private lateinit var listener: BackListener
//    private val baseUserID = "00000000-1000-1000-1000-000000000000"
//    private val baseUserID = "00000000-2000-2000-2000-000000000000"
    private val baseUserID = "00000000-3000-3000-3000-000000000000"
//    private val baseUserID = "00000000-4000-4000-4000-000000000000"

    private val authParams = "1000"
    private val strRoom = baseUserID  // 00000000-1000-1000-1000-000000000000 // 00000000-2000-2000-2000-000000000000
    private val strCont = baseUserID  // "00000000-2000-2000-2000-000000000000"
    private val strClnr = baseUserID  // "00000000-2000-2000-2000-000000000000"
    private val strRefr = baseUserID  // "00000000-2000-2000-2000-000000000000"
    private val strUser = baseUserID  // "00000000-2000-2000-2000-000000000000"

    private lateinit var data: List<SessionEntity> // meetDatabase.sessionDao().fetchSession()
    private lateinit var earnings: _AppEarning

    private var dataCodes: MutableList<objModelReferralCode.Code>? = null

    private var cContacts = mutableListOf<FetchContact>() //? = null
    private var cLetters  = mutableListOf<String>()
    private val cContactState = mutableStateListOf<FetchContact>()
    private var cCategories= mutableListOf<ContactCategory>() //MutableList<ContactCategory>? = null

    private val rCalendars = mutableListOf<DtMeetCalendar>()

    private val profile = mutableListOf<MeetUserInfo>()
    private val rRoom   = mutableListOf<roomModels.RoomByInvite>()

    /**  data for Divits Rewards Bonus  **/
    private val pageRewardBonus = PageRewardsBonus()

    private val pageBonusChart  = PageDataBonusChart()
    private val pageTokenChart  = PageTokenChart()
    private val pageHowToReward = PageHowToRewards()

//    init {
//        loadingUserProfile()
//    }

//    private suspend fun loadingUserProfile(){
//        if(profile.isNullOrEmpty()){
//            profile.clear()
//            profile.add(
//                MeetUserInfo(
//                    id = UUID.randomUUID().toString(),
//                    avatar = "https://images.unsplash.com/photo-1522075469751-3a6694fb2f61?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80",
//                    displayName = "Display name vnature",
//                    phone = "+38.069.365.21.54",
//                    email = "display.name.vnature@name.info",
//                    totalTime = 2548f,
//                    slug = "Elementary for users!"
//            ))
//        }
//    }

//    internal fun addBackListener(l: BackListener){
//        this.listener = l
//    }

    internal fun fetchOwnerId() = profile[0].id

    suspend internal fun sendReferralData(dt: VAL): ResponseRepository{
        return ResponseRepository.Success
//        try {
//            networkServices.currentReferralConnect!!.sendReferralsInformation(data[0].homeUserId, Gson().toJson(dt))
//        }catch (e: RuntimeException){
//
//        }.let{ result ->
//            return when(result){
//                is _POST_RefCodeInfoSend.Success -> ResponseRepository.Success
//                is _POST_RefCodeInfoSend.Failure -> ResponseRepository.Failure
//                else -> ResponseRepository.Nothing
//            }
//        }
    }

    internal suspend fun featchUserProfile(): ResponseRepository {
        return if(profile.isNullOrEmpty()){
            try{
                networkServices.currentProfileConnect?.userProfileGet(
                    authHeader =  data[0].homeUserId,
                    appId = BuildConfig.APP_ID)
            } catch (r: RuntimeException) {
                ResponseRepository.RequestErrorResult(
                    message = BaseViewModel.MessageState.MessageDanger(
                        content = BaseViewModel.AlertText(
                            title = r.message.toString(),
                            texts = r.message.toString() )))
            }.let { loaded ->
                when(loaded){
                    is TaskProfileResult.Body -> {
                        val tmp = loaded.body as FetchMeetProfile
                        profile.clear()
                        profile.add(tmp.body)
                        if(tmp.body.userRooms.size>0){
                            tmp.body.userRooms.forEachIndexed() { index, item ->
                                val vt = Emulation.emulationDate()
                                meetDatabase.historyDao().insertHistory(
                                    HistoryEntity (
                                        historyId       = index + 999999L,
                                        meetName        = item.name,
                                        sessionMeet     = UUID.randomUUID().toString(),
                                        startTimeMeet   = vt[0].timeInMillis,
                                        finalTimeMeet   = vt[1].timeInMillis,
                                        meetParameterId = System.currentTimeMillis() - Random.nextInt(30000, 40000),
                                        meetTextChatId  = System.currentTimeMillis() - Random.nextInt(40000, 50000)
                                    )
                                )
                            }
                        }
                        ResponseRepository.PrflSuccess(
                            message = show(
                                type = tmp.status,
                                message = BaseViewModel.AlertText(
                                    title = tmp.title,
                                    texts = tmp.message))
                          , content = tmp.body)
                    }
                    is TaskProfileResult.Fail -> {
                        ResponseRepository.RequestErrorResult(
                            message = show(
                                type = loaded.fail.status,
                                message = BaseViewModel.AlertText(
                                        title = loaded.fail.title,
                                        texts = loaded.fail.message)))
                    }
                    else -> {ResponseRepository.Nothing}
                }
            }
        }else{
           ResponseRepository.PrflSuccess(
                message = BaseViewModel.MessageState.MessageNothing,
                content = profile[0])
        }
    }

    internal fun dataReferralClear(){ dataCodes?.clear() }

    suspend fun fetchSession(): ResponseRepository {
        data = meetDatabase.sessionDao().fetchSession()
        if ( data.size > 0 ){
            return ResponseRepository.ResponseRefresh()
        }
        return ResponseRepository.RespFailureFetchSession() // "Don`t issues session!...")
    }

    suspend fun refreshToken(): ResponseRepository {
        try{
//            serverCommunicator.refresh(data[0].session)
        } catch (ex : CancellationException){
            return ResponseRepository.RequestErrorResult(BaseViewModel.MessageState
                .MessageDanger(content = BaseViewModel.AlertText(
                    title = "Error...",
                    texts = ex.message.toString())))
        }.let {
            return ResponseRepository.ResponseRefreshSuccess()
        }
    }

    suspend fun updateSession(session: String): ResponseRepository {
        try {
            val session = Gson().fromJson<Session>(session, Session::class.java)
            val sessionEntity: SessionEntity
            data = meetDatabase.sessionDao().fetchSession()
            if (data.size > 0) {
                sessionEntity = data[0]
                sessionEntity.session = session.token
            } else {
                sessionEntity = SessionEntity(
                    id         = 0,
                    session    = session.token,
                    code       = session.code,
                    userName   = session.userName,
                    homeServer = session.homeServer,
                    homeUserId = session.homeUserId,
                    avatar     = session.avatar)
            }
            meetDatabase.sessionDao().insertSession(sessionEntity)
        } catch (ex: CancellationException) {
            return ResponseRepository.RequestErrorResult(BaseViewModel.MessageState
                .MessageDanger(content = BaseViewModel.AlertText(
                    title = "Error...",
                    texts = ex.message.toString())))
        }.let { resp ->
            return ResponseRepository.ResponseRefreshSuccess()
        }
    }

    suspend fun deleteSession(): ResponseRepository {
        try{
            data = meetDatabase.sessionDao().fetchSession()
            if (data.size > 0) {
                meetDatabase.sessionDao().deleteSession(data[0])
            }else{
                return ResponseRepository.ResponseRefreshSuccess()
            }
        }catch (ex: CancellationException){
            return ResponseRepository.RequestErrorResult(BaseViewModel.MessageState
                .MessageDanger(content = BaseViewModel.AlertText(
                    title = "Error...",
                    texts = ex.message.toString())))
        }.let{
            return ResponseRepository.ResponseRefreshSuccess()
        }
    }

    /***   referrals functions   ***/

    suspend fun updateDataEarning(): _AppEarning {
        val format = SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z")
        val bonus = Random.nextInt(3, 8)
        earnings = _AppEarning(
            userID           = data[0].homeUserId,
            membershipPlanID = UUID.randomUUID().toString(),
            currency         = "USD",
            /** ******||||---|---||||****** **/
            bonuses          = mutableListOf(
                _AppBonuses(
                    bonus     = bonus,
                    bonusID   = UUID.randomUUID().toString(),
                    count     = Random.nextInt(14, 1378),
                    createdAt = format.format(Date(System.currentTimeMillis() - (1000*60*60*24)*Random.nextInt(11, 16))),
                    updatedAt = format.format(Date(System.currentTimeMillis() - (1000*60*60*24)*Random.nextInt( 5, 10)))
                ),
                _AppBonuses(
                    bonus     = bonus,
                    bonusID   = UUID.randomUUID().toString(),
                    count     = Random.nextInt(34, 168),
                    createdAt = format.format(Date(System.currentTimeMillis() - (1000*60*60*24)*Random.nextInt(11, 16))),
                    updatedAt = format.format(Date(System.currentTimeMillis() - (1000*60*60*24)*Random.nextInt( 5, 10)))
                ),
                _AppBonuses(
                    bonus     = bonus,
                    bonusID   = UUID.randomUUID().toString(),
                    count     = Random.nextInt(2, 78),
                    createdAt = format.format(Date(System.currentTimeMillis() - (1000*60*60*24)*Random.nextInt(11, 16))),
                    updatedAt = format.format(Date(System.currentTimeMillis() - (1000*60*60*24)*Random.nextInt( 5, 10)))
                ),
                _AppBonuses(
                    bonus     = bonus,
                    bonusID   = UUID.randomUUID().toString(),
                    count     = Random.nextInt(4, 18),
                    createdAt = format.format(
                        Date(System.currentTimeMillis()-(1000*60*60*24)*Random.nextInt(11,16))),
                    updatedAt = format.format(
                        Date(System.currentTimeMillis()-(1000*60*60*24)*Random.nextInt(5,10)))
                )
            )
        )
        return earnings
    }

    internal suspend fun updateDataReferrals(): ResponseRepository {
        if(dataCodes.isNullOrEmpty()){
            try {
                networkServices.currentReferralConnect?.fetchReferralCodes(
                    authHeader =  data[0].homeUserId, //meetDatabase.sessionDao().fetchSession()[0].session,
                    appID = BuildConfig.APP_ID)!! // change user id to token
            } catch (th: Throwable) {
                return ResponseRepository.RequestErrorResult(BaseViewModel.MessageState
                    .MessageDanger(content = BaseViewModel.AlertText(
                        title = "Error...",
                        texts = th.message.toString())))
            }.let { success ->
                when (success) {
                    is _GetReferralCodes.Success -> {
                        dataCodes?.clear()
                        val resultat = success.success as objModelReferralCode.ResultGetReferralCode
                        dataCodes = resultat.refCodes as MutableList<objModelReferralCode.Code>
                        return ResponseRepository.RespFetchRefCodes(dataCodes!!)
                    }
                    is _GetReferralCodes.Failure -> {
                        val resultat = success.failure as ReferralsFailureObject
                        return ResponseRepository.RequestErrorResult(BaseViewModel.MessageState
                            .MessageDanger(content = BaseViewModel.AlertText(
                                title = "Error...",
                                texts = resultat.errorObject.message)))
                    }
                    else -> return ResponseRepository.RequestErrorResult(
                                                message = BaseViewModel.MessageState.MessageNothing)
                }
            }
        }else{
            return ResponseRepository.RespFetchRefCodes(dataCodes!!)
        }
    }

    internal suspend fun onCreateNewReferralCode(notes: String, isDefault: Boolean): ResponseRepository {
        try {
            networkServices.currentReferralConnect?.postReferralCodes(
            authHeader =  data[0].homeUserId,
            body = objModelReferralCode.ReferralCodeParamsPst(
                    appID = CONST.APP_PACKAGE,
                    isDefault = isDefault,
                    note = notes ))
        } catch (th: Throwable){
            return ResponseRepository.RequestErrorResult(BaseViewModel.MessageState
                .MessageDanger(content = BaseViewModel.AlertText(
                    title = "Error...",
                    texts = th.message.toString())))
        }.let{
            when (it) {
                is _POST_ReferralUpdate.Success -> {
                    return ResponseRepository.TaskCreateReferralCode(message = show(
                        type = "success", //temp.type,
                        message = BaseViewModel.AlertText(
                            title = "Update referral link...", //temp.title,
                            texts = "Update referral link success")), // temp.message)),
                        code = it.success.code)
                }
                is _POST_ReferralUpdate.Failure -> {
                    return ResponseRepository.RequestErrorResult(BaseViewModel.MessageState
                        .MessageDanger(content = BaseViewModel.AlertText(
                            title = "Error...",
                            texts = it.failure.errorObject.message)))
                }
                else -> return ResponseRepository.RequestErrorResult(BaseViewModel.MessageState.MessageNothing)
            }
        }
    }

    internal suspend fun onUpdateReferralCode(referralID: String, notes: String, isDefault: Boolean): ResponseRepository{ // _PUT_ReferralUpdate?{
        try {
            networkServices.currentReferralConnect?.updateReferralCodeById(
                authHeader =  data[0].homeUserId,
                id = referralID,
                body = objModelReferralCode.ReferralCodeParamsPut(
                    isDefault = isDefault,
                    note = notes
                ))
        }catch (th: Throwable){
            return ResponseRepository.RequestErrorResult(BaseViewModel.MessageState
                .MessageDanger(content = BaseViewModel.AlertText(
                    title = "Error...",
                    texts = th.message.toString())))
        }.let{
            when (it) {
                is _PUT_ReferralUpdate.Success -> {
                    return  ResponseRepository.TaskUpdateReferralCode(
                        message = show(type = "success", message = BaseViewModel
                            .AlertText(title = "Update referral link", texts = "Update referral link success...")),
                        code    = it.success
                    )
                }
                is _PUT_ReferralUpdate.Failure -> {
                    return ResponseRepository.RequestErrorResult(BaseViewModel.MessageState
                        .MessageDanger(content = BaseViewModel.AlertText(
                            title = "Error...",
                            texts = it.failure.errorObject.message)))
                }
                else -> return ResponseRepository.RequestErrorResult(
                                                message = BaseViewModel.MessageState.MessageNothing)
            }
        }
    }

    internal suspend fun onRemoveReferralCode(referralID: String): ResponseRepository { //
//            _DEL_ReferralUpdate?{
        try{
            networkServices.currentReferralConnect?.deleteReferralCodeById( data[0].homeUserId, referralID)!!
        }catch (th: Throwable){
            return ResponseRepository.RequestErrorResult(BaseViewModel.MessageState
                .MessageDanger(content = BaseViewModel.AlertText(
                    title = "Error...",
                    texts = th.message.toString())))
        }.let{
            when(it){
                is _DEL_ReferralUpdate.Success -> {
                    return ResponseRepository.RespDeleteRefCodes(referralID, it.success.content.message)
                }
                is _DEL_ReferralUpdate.Failure -> {
                    return ResponseRepository.RequestErrorResult(BaseViewModel.MessageState
                        .MessageDanger(content = BaseViewModel.AlertText(
                            title = "Error...",
                            texts = it.failure.errorObject.message)))
                }
            }
        }
    }

/**        Block contact job        */
    internal suspend fun fetchContactCategories(): ResponseRepository{
        if(cCategories.isNullOrEmpty()){
            try {
                networkServices.currentContactConnect?.fetchContactCategories(authHeader =  data[0].homeUserId)
            } catch (th: Throwable) {
                ResponseRepository.RequestErrorResult(message = BaseViewModel.MessageState
                    .MessageDanger(content = BaseViewModel.AlertText(
                        title = "Error",
                        texts = th.message!!)))
            }.let {
                when(it){
                    is TaskCategory.Success -> {
                        cCategories?.clear()
                        cCategories?.addAll(it.body.content)
                        return ResponseRepository.TaskContactCategory(
                            message = BaseViewModel.MessageState.MessageNothing,
                            content = cCategories!!
                        )
                    }
                    is TaskCategory.Failure -> {
                        return ResponseRepository.RequestErrorResult(
                            message = show(it.body.status, BaseViewModel.AlertText(
                                title = it.body.title,
                                texts = it.body.message))
                        )
                    }
                    else -> {
                        return ResponseRepository
                            .RequestErrorResult(message = BaseViewModel.MessageState.MessageNothing)
                    }
                }
            }
        }else{
            return ResponseRepository.TaskContactCategory(
                message = BaseViewModel.MessageState.MessageNothing,
                content = cCategories!!)
        }
    }

    internal fun preparyContact() = cContactState

    internal suspend fun fetchAllContacts(isSearchMode: Boolean, config: ContactConfig): ResponseRepository {
        if (cContacts.isNullOrEmpty() /*|| config.currentPage!! < config.lastPage!!*/) {
            LogUtil.info(txt = "${config.currentPage}")
            try {
                networkServices.currentContactConnect
                    ?.fetchAllContacts( data[0].homeUserId, CONST.APP_PACKAGE, convertToApi(config))!! // change user id to token
            } catch (th: Throwable) {
                return ResponseRepository.RequestErrorResult(message = BaseViewModel.MessageState
                    .MessageDanger(content = BaseViewModel.AlertText(
                        title = "Error",
                        texts = th.message!!)))
            }.let { success ->
                val resultat = success as TaskAllContacts
                when (resultat) {
                    is TaskAllContacts.Success -> {
                        config.search?.let {
                            cContacts.clear()
                            cContacts.addAll(resultat.body.contacts)
                            return ResponseRepository.TaskContactsUpdate(
                                contacts = cContacts,
                                letters  = resultat.body.letters,
                                pagination = converPaginationData(resultat.body))
                        }.also {
                            config.isFavorite?.let {
                                cContacts.clear()
                                cContacts.addAll(resultat.body.contacts) //
                                return ResponseRepository.TaskContactsUpdate(
                                    contacts = cContacts,
                                    letters  = resultat.body.letters,
                                    pagination = converPaginationData(resultat.body))
                            }.also {
                                if(isSearchMode){
                                    cContacts.clear()
                                }
                                cContacts.addAll(resultat.body.contacts)
                                return ResponseRepository.TaskContactsUpdate(
                                    contacts = cContacts,
                                    letters  = resultat.body.letters,
                                    pagination = converPaginationData(resultat.body))
                            }
                        }
                    }
                    is TaskAllContacts.Failure -> {
                        return ResponseRepository.RequestErrorResult(
                            message = show(
                                type = resultat.body.status,
                                message = BaseViewModel.AlertText(
                                    title = resultat.body.title,
                                    texts = resultat.body.message)))

                    }
                }
            }
        } else {
            return ResponseRepository.TaskContactsUpdate(cContacts, cLetters, null)
        }
    }

    internal suspend fun fetchAllParticipants(config: ContactConfig): ResponseRepository {
        if (cContacts.isNullOrEmpty()) {
            LogUtil.info(txt = "${config.currentPage}")
            try {
                networkServices.currentContactConnect
                    ?.fetchAllContacts( data[0].homeUserId, CONST.APP_PACKAGE, convertToApi(config))!! // change user id to token
            } catch (th: Throwable) {
                ResponseRepository.RequestErrorResult(message = BaseViewModel.MessageState
                    .MessageDanger(content = BaseViewModel.AlertText(
                        title = "Error",
                        texts = th.message!!)))
            }.let { success ->
                val resultat = success as TaskAllContacts
                when (resultat) {
                    is TaskAllContacts.Success -> {
                        cContacts.addAll(resultat.body.contacts)
                        return ResponseRepository.TaskMeetGet(
                            contacts   = cContacts,
                            pagination = converPaginationData(resultat.body))
                    }
                    is TaskAllContacts.Failure -> {
                        return ResponseRepository.RequestErrorResult(
                            message = show(
                                type = resultat.body.status,
                                message = BaseViewModel.AlertText(
                                    title = resultat.body.title,
                                    texts = resultat.body.message)))
                    }
                }
            }
        } else {
            return ResponseRepository.TaskMeetGet(contacts = cContacts, null)
        }
    }

    internal suspend fun fetchSearchContacts(config: ContactConfig): ResponseRepository {

        try {
            networkServices.currentContactConnect
                ?.fetchAllContacts( data[0].homeUserId, CONST.APP_PACKAGE, convertToApi(config))!! // change user id to token
        } catch (th: Throwable) {
            ResponseRepository.RequestErrorResult(message = BaseViewModel.MessageState
                .MessageDanger(content = BaseViewModel.AlertText(
                    title = "Error",
                    texts = th.message!!)))
        }.let { success ->
            val resultat = success as TaskAllContacts
            when (resultat) {
                is TaskAllContacts.Success -> {
                    cLetters.clear()
                    cLetters.addAll(resultat.body.letters)
                    cContacts.clear()
                    cContacts.addAll(resultat.body.contacts)
                    return ResponseRepository.TaskContactsUpdate(
                        contacts   = cContacts,
                        cLetters,
                        pagination = converPaginationData(resultat.body))
                }
                is TaskAllContacts.Failure -> {
                    return ResponseRepository.RequestErrorResult(
                        message = show(
                            type = resultat.body.status,
                            message = BaseViewModel.AlertText(
                                title = resultat.body.title,
                                texts = resultat.body.message)))

                }
            }
        }
    }

    private fun convertToApi(config: ContactConfig): ApiGetConfig{
        return ApiGetConfig(
            limit = config.limit,
            page  =  config.nextPage,
            search = config.search,
            isFavorite = config.isFavorite,
            isRecently = config.isRecently,
            byLetter = config.byLetter,
            groupId = config.groupId,
            sortBy = config.sortBy,
            sortOrder = config.sortOrder
        )
    }

    private fun converPaginationData(pagination: FetchAllUserContact): ContactDataPagination {
        pagination.apply {
            val temp = mutableListOf<ContactDataPagination.Link>()
            links.forEach { item ->
                temp.add(ContactDataPagination
                    .Link(active = item.active, label = item.label, url = item.url))
            }
            return ContactDataPagination(currentPage = currentPage,
                firstPageUrl = firstPageUrl,
                from = from,
                lastPage = lastPage,
                lastPageUrl = lastPageUrl,
                nextPageUrl = nextPageUrl,
                path = path,
                perPage = perPage,
                prevPageUrl = prevPageUrl,
                to = to,
                total = total ,
                letters = letters,
                links = temp )
        }
    }

    internal suspend fun createNewContact(tempUri: Uri, create: UserContactCreate): ResponseRepository{
        try {
            networkServices.currentContactConnect?.createContact(
                authHeader =  data[0].homeUserId, //data[0].session,
                modelContact = convertDataFromSend(create))
        } catch (ex:Throwable){
            return ResponseRepository.RequestErrorResult(BaseViewModel.MessageState
                .MessageDanger(content = BaseViewModel.AlertText(title = "Error...", texts = ex.message.toString())))
        }.let{
            when(it){
                is _ResponseContacts._ResultSuccess -> {
                    val temp = it.content as FetchContactCreate
                    cContacts.add(0, FetchContact(
                        avatar = tempUri.toString(),
                        birthday = temp.saveResult.birthday,
                        displayName = temp.saveResult.displayName,
                        email = create.uEmails[0],
                        firstName = temp.saveResult.firstName,
                        id = temp.saveResult.id,
                        isFavorite = temp.saveResult.isFavorite,
                        lastName = temp.saveResult.lastName,
                        middleName = temp.saveResult.midleName,
                        nickname = temp.saveResult.nickname,
                        note = temp.saveResult.note,
                        phone = create.uPhones[0],
                        prefixName = temp.saveResult.prefixName,
                        suffixName = temp.saveResult.suffixName,
                        userId = temp.saveResult.userId,
                        writeAsName = temp.saveResult.writeAsName,
                        groups = listOf()
                    ))
                    return ResponseRepository.TaskContactCreate(message = show(
                            type = temp.type,
                            message = BaseViewModel.AlertText(
                                title = temp.title,
                                texts = temp.message)),
                        content = temp.saveResult)
                }
                is _ResponseContacts._ResultFailure -> {
                    val temp = it.content as Failure
                    return ResponseRepository.RequestErrorResult(BaseViewModel.MessageState
                        .MessageDanger(content = BaseViewModel.AlertText(
                            title = "Error...",
                            texts = temp.message.toString())))
                }
                else -> {
                    return ResponseRepository.RequestErrorResult(BaseViewModel.MessageState
                        .MessageDanger(content = BaseViewModel.AlertText(
                            title = "Error...",
                            texts = "Polnaja Laga of request...")))
                }
            }
        }
    }

    /**  **/

    internal fun checkCurrentUser(): ResponseRepository{
        return if(profile.isNullOrEmpty()){
            ResponseRepository.TaskNotRoom
        }else{
            ResponseRepository.PrflUpdate(content = profile[0])
        }
    }

    internal fun checkCurrentRoom(): ResponseRepository{
        return if(rRoom.isNullOrEmpty()){
            ResponseRepository.TaskNotRoom
        }else{
            ResponseRepository.TaskRoomUpdate(content = rRoom[0])
        }
    }

    internal suspend fun roomCreateByRoomName(room: RoomData): ResponseRepository {
        return try {
            networkServices.currentRoomConnect?.roomPst(
                authHeader = data[0].homeUserId,
                appId = CONST.APP_PACKAGE,
                body = convertDataRoom(room))
        } catch (th: Throwable) {
            ResponseRepository.RequestErrorResult(message = show(
                type = BaseViewModel.DANGER,
                message = BaseViewModel.AlertText(th.message!!, th.message!!)))
        }.let {
            when(it){
                is TaskRespMeetRoom.Data -> {
                    val tmp = it.body as FetchOwnerRoom
                    rRoom.clear()
                    rRoom.add(convertToDtRoom(tmp.body!!))
                    ResponseRepository
                        .TaskRoomCreate(message = show(type = tmp.type,
                            message = BaseViewModel.AlertText(
                                title = tmp.title,
                                texts = tmp.message)),
                            content = rRoom[0])
                }
                is TaskRespMeetRoom.Fail -> {
                    ResponseRepository.RequestErrorResult(
                        message = show(
                        type = it.fail.status,
                        message = BaseViewModel.AlertText(
                            title = it.fail.title,
                            texts = it.fail.message)))
                }
                else -> {ResponseRepository.Nothing}
            }
        }
    }

    internal suspend fun roomFetchByInvite(invite: String): ResponseRepository{
        return if(rRoom.isNullOrEmpty()) {
            try {
                networkServices.currentProfileConnect?.userMeetRoomInvitePos(
                    authHeader =  data[0].homeUserId,
                    appId = CONST.APP_PACKAGE,
                    body = FromMeetByInvite(invite = invite)
                )
            } catch (th: Throwable) {
                    ResponseRepository.RequestErrorResult(
                        message = show(
                            type = BaseViewModel.DANGER,
                            message = BaseViewModel.AlertText(th.message!!, th.message!!)
                        ))
            }.let {
                when (it) {
                    is TaskRespMeetRoom.Data -> {
                        /***   convertToInviteRoom(it)   ***/
                        val tmp = it.body as respDataInviteRoom.FetchRoomByInvite
                            rRoom.clear()
                        rRoom.add(convertToDtRoom(tmp.data))
                            ResponseRepository
                                .TaskRoomCreate(
                                    message = show(
                                        type = tmp.type,
                                        message = BaseViewModel.AlertText(
                                            title = tmp.title,
                                            texts = tmp.message
                                        )
                                    ),
                                    content = rRoom[0]
                                )

                    }
                    is TaskRespMeetRoom.Fail -> {
                            ResponseRepository.RequestErrorResult(
                                message = show(
                                    type = it.fail.status,
                                    message = BaseViewModel.AlertText(
                                        title = it.fail.title,
                                        texts = it.fail.message
                                    )
                                )
                            )

                    }
                    else ->{ResponseRepository.Nothing}
                }
            }
        } else {
            ResponseRepository.TaskRoomCreate(
                message = BaseViewModel.MessageState.MessageNothing,
                content = rRoom[0])
        }
    }

    private fun convertToDtRoom(room: Any): roomModels.RoomByInvite{
        var tmp: roomModels.RoomByInvite? = null
        when(room){
            is MeetRoom -> {
                val t = room
                tmp = roomModels.RoomByInvite(
                      rRoomID = t.id
                    , rRoomName = t.name
                    , rRoomStatus = t.status
                    , rRoomInvite = t.invite
                    , rRoomParticipants = convertRoomUsersCreate(t.roomUsers))
            }
            is respDataInviteRoom.RoomByInvite -> {
                val t = room
                tmp = roomModels.RoomByInvite(
                      rRoomID = t.id
                    , rRoomName = t.name
                    , rRoomStatus = if(t.status == 1) { true } else { false }
                    , rRoomInvite = t.invite
                    , rRoomParticipants = convertRoomUsersInvite(t.roomUsers))
            }
        }
        return tmp!!
    }

    private fun convertRoomUsersCreate(l: List<InviteRoomUsers>?): List<roomModels.ParticipantsRoom> {
        val ur = mutableListOf<roomModels.ParticipantsRoom>()
        l?.run {
            l.forEach() { users ->
                ur.add(
                    roomModels.ParticipantsRoom(
                        pUserID = users.id,
                        pDisplayName = users.mDisplayName,
                        pUserPhone = users.mPhone,
                        pUserEmail = users.mEmail,
                    )
                )
            }
        }
        return ur
    }

    private fun convertRoomUsersInvite(l: List<respDataInviteRoom.RoomByInviteParticipants>?): List<roomModels.ParticipantsRoom> {
        val ur = mutableListOf<roomModels.ParticipantsRoom>()
        l?.run {
            l.forEach() { users ->
                ur += roomModels.ParticipantsRoom(
                    pUserID = users.id,
                    pDisplayName = if(users.displayName.isNullOrEmpty()){""}else(users.displayName),
                    pUserPhone = if(users.phone.isNullOrEmpty()){""}else(users.phone),
                    pUserEmail = if(users.email.isNullOrEmpty()){""}else(users.email),
                )
            }
        }
        return ur
    }

    private fun convertDataRoom(room: RoomData): FromMeetRooms{
        return FromMeetRooms(name = room.name, status = room.status, password = room.password)
    }

    private fun convertInviteLink(invite: String): FromMeetByInvite {
        return FromMeetByInvite(invite = invite)
    }

    internal suspend fun createRoomSession(session: ToSession): ResponseRepository {
        try {
            networkServices.currentSessionConnect?.roomSessionPst(authHeader =  data[0].homeUserId
                , appId = CONST.APP_PACKAGE
                , body = convertDataSession(session))
        } catch (th: Throwable) {
            return ResponseRepository.RequestErrorResult(show(type = BaseViewModel.DANGER,
                message = BaseViewModel.AlertText(title = th.message!!,
                    texts = th.message!!)))
        }.let {
            when (it){
                is TaskRespMeetSession.Data -> {
                    val tmp = it.body as FetchMeetSessionCrt
                    return ResponseRepository.TaskSessionCreate(message = show(type = tmp.type
                        , message = BaseViewModel.AlertText(title = tmp.title
                            , texts = tmp.message))
                        , content = tmp.body)}
                is TaskRespMeetSession.Fail -> {
                    return ResponseRepository.RequestErrorResult(message = show(type = it.fail.status
                        , message = BaseViewModel.AlertText(title = it.fail.title
                            , texts = it.fail.message)))
                }
                else -> return ResponseRepository.RequestErrorResult(show(type = BaseViewModel.DANGER,
                    message = BaseViewModel.AlertText(title = "",
                        texts = "")))
            }
        }
    }

    internal suspend fun updateRoomSession(session: ToSession, sessionId: String): ResponseRepository {
        try {
            networkServices.currentSessionConnect?.roomSessionPut(authHeader = data[0].homeUserId
                , appId = CONST.APP_PACKAGE
                , sessionId = session.roomId
                , body = convertDataSession(session))
        } catch (th: Throwable) {
            return ResponseRepository.RequestErrorResult(show(type = BaseViewModel.DANGER,
                message = BaseViewModel.AlertText(title = th.message!!,
                    texts = th.message!!)))
        }.let {
            when (it){
                is TaskRespMeetSession.Data -> {
                    val tmp = it.body as FetchMeetSessionUpd
                    return ResponseRepository.TaskSessionUpdate(message = show(type = tmp.type
                        , message = BaseViewModel.AlertText(title = tmp.title
                            , texts = tmp.message))
                        , content = tmp.body)}
                is TaskRespMeetSession.Fail -> {
                    return ResponseRepository.RequestErrorResult(message = show(type = it.fail.status
                        , message = BaseViewModel.AlertText(title = it.fail.title
                            , texts = it.fail.message)))
                }
                else -> return ResponseRepository.RequestErrorResult(show(type = BaseViewModel.DANGER,
                    message = BaseViewModel.AlertText(title = "",
                        texts = "")))
            }
        }
    }

    private fun convertDataSession(session: ToSession): FromMeetSession{
        return FromMeetSession(roomId = session.roomId)
    }

    internal fun updateDataLineChart(params: SChartInterval) : ResponseRepository{
        try {

            val interval: String?
            when(params){
                is SChartInterval.Days   -> {
                    interval = params.interval
                    return ResponseRepository.TaskChartLinearUdt(
                        message =  show(type = BaseViewModel.SUCCESS
                            , message = BaseViewModel.AlertText(title = "Linear chart"
                                , texts = "Updates succes")),
                        content = setDataForLineChart(count = 30, range = 100F))
                }
                is SChartInterval.Weeks  -> {
                    interval = params.interval
                    return ResponseRepository.TaskChartLinearUdt(
                        message =  show(type = BaseViewModel.SUCCESS
                            , message = BaseViewModel.AlertText(title = "Linear chart"
                                , texts = "Updates succes")),
                        content = setDataForLineChart(count = 7, range = 100F))
                }
                is SChartInterval.Months -> {
                    interval = params.interval
                    return ResponseRepository.TaskChartLinearUdt(
                        message =  show(type = BaseViewModel.SUCCESS
                            , message = BaseViewModel.AlertText(title = "Linear chart"
                                , texts = "Updates succes")),
                        content = setDataForLineChart(count = 30, range = 100F))
                }
                is SChartInterval.Years  -> {
                    interval = params.interval
                    return ResponseRepository.TaskChartLinearUdt(
                        message =  show(type = BaseViewModel.SUCCESS
                            , message = BaseViewModel.AlertText(title = "Linear chart"
                                , texts = "Updates succes")),
                        content = setDataForLineChart(count = 12, range = 100F))
                }
            }
        } catch (th: Throwable) {
            return ResponseRepository.RequestErrorResult(show(type = BaseViewModel.DANGER,
                message = BaseViewModel.AlertText(title = th.message!!,
                    texts = th.message!!)))
        }
    }

    internal fun updateDataPieChart(params: SChartInterval) : ResponseRepository{
        try {
            val interval: String?
            when(params){
                is SChartInterval.Days   -> {
                    interval = params.interval
                    return ResponseRepository.TaskChartHalfPieUdt(
                        message =  show(type = BaseViewModel.SUCCESS
                            , message = BaseViewModel.AlertText(title = "Pie chart"
                                , texts = "Updates succes")),
                        content = setDataForPieChart(count = 2, range = 125.5f))
                }
                is SChartInterval.Weeks  -> {
                    interval = params.interval
                    return ResponseRepository.TaskChartHalfPieUdt(
                        message =  show(type = BaseViewModel.SUCCESS
                            , message = BaseViewModel.AlertText(title = "Pie chart"
                                , texts = "Updates succes")),
                        content = setDataForPieChart(count = 2, range = 2654.5f))
                }
                is SChartInterval.Months -> {
                    interval = params.interval
                    return ResponseRepository.TaskChartHalfPieUdt(
                        message =  show(type = BaseViewModel.SUCCESS
                            , message = BaseViewModel.AlertText(title = "Pie chart"
                                , texts = "Updates succes")),
                        content = setDataForPieChart(count = 2, range = 12468.5f))
                }
                is SChartInterval.Years  -> {
                    interval = params.interval
                    return ResponseRepository.TaskChartHalfPieUdt(
                        message =  show(type = BaseViewModel.SUCCESS
                            , message = BaseViewModel.AlertText(title = "Pie chart"
                                , texts = "Updates succes")),
                        content = setDataForPieChart(count = 2, range = 99648.5f))
                }
            }
        } catch (th: Throwable) {
            return ResponseRepository.RequestErrorResult(show(type = BaseViewModel.DANGER,
                message = BaseViewModel.AlertText(title = th.message!!,
                    texts = th.message!!)))
        }
    }

    internal fun updateDataBarChart(params: SChartInterval) : ResponseRepository{
        try {

            val interval: String?
            when(params){
                is SChartInterval.Days   -> {
                    interval = params.interval
                    return ResponseRepository.TaskChartBarUpdate(
                        message =  show(type = BaseViewModel.SUCCESS
                            , message = BaseViewModel.AlertText(title = "Bar chart"
                                , texts = "Updates succes")),
                        content = setDataForBarChart(count = 30, range = 88.5F))
                }
                is SChartInterval.Weeks  -> {
                    interval = params.interval
                    return ResponseRepository.TaskChartBarUpdate(
                        message =  show(type = BaseViewModel.SUCCESS
                            , message = BaseViewModel.AlertText(title = "Bar chart"
                                , texts = "Updates succes")),
                        content = setDataForBarChart(count = 7, range = 845.9F))
                }
                is SChartInterval.Months -> {
                    interval = params.interval
                    return ResponseRepository.TaskChartBarUpdate(
                        message =  show(type = BaseViewModel.SUCCESS
                            , message = BaseViewModel.AlertText(title = "Bar chart"
                                , texts = "Updates succes")),
                        content = setDataForBarChart(count = 4, range = 3654.5F))
                }
                is SChartInterval.Years  -> {
                    interval = params.interval
                    return ResponseRepository.TaskChartBarUpdate(
                        message =  show(type = BaseViewModel.SUCCESS
                            , message = BaseViewModel.AlertText(title = "Bar chart"
                                , texts = "Updates succes")),
                        content = setDataForBarChart(count = 12, range = 51349.8F))
                }
            }
        } catch (th: Throwable) {
            return ResponseRepository.RequestErrorResult(show(type = BaseViewModel.DANGER,
                message = BaseViewModel.AlertText(title = th.message!!,
                    texts = th.message!!)))
        }
    }

    /**      **/
    private fun show(type: String, message: BaseViewModel.AlertText): BaseViewModel.MessageState {
        return when(type){
            BaseViewModel.MessageState.MessagePrimary.TAG   -> {
                BaseViewModel.MessageState.MessagePrimary(content = message)}
            BaseViewModel.MessageState.MessageSecondary.TAG -> {
                BaseViewModel.MessageState.MessageSecondary(content = message)}
            BaseViewModel.MessageState.MessageSuccess.TAG   -> {
                BaseViewModel.MessageState.MessageSuccess(content = message)}
            BaseViewModel.MessageState.MessageInfo.TAG      -> {
                BaseViewModel.MessageState.MessageInfo(content = message)}
            BaseViewModel.MessageState.MessageWarning.TAG   -> {
                BaseViewModel.MessageState.MessageWarning(content = message)}
            BaseViewModel.MessageState.MessageDanger.TAG    -> {
                BaseViewModel.MessageState.MessageDanger(content = message)}
            BaseViewModel.MessageState.MessageLight.TAG     -> {
                BaseViewModel.MessageState.MessageLight(content = message)}
            BaseViewModel.MessageState.MessageDark.TAG      -> {
                BaseViewModel.MessageState.MessageDark(content = message)}
            else -> BaseViewModel.MessageState.MessageNothing
        }
    }

    private fun convertDataFromSend(create: UserContactCreate): FromAddContact {
        return FromAddContact(
            avatar = create.uAvatar,
            prefixName = create.uNameP,
            firstName = create.uNameF,
            middleName = create.uNameM,
            lastName = create.uNameL,
            suffixName = create.uNameS,
            displayName = create.uNameD,
            nickname = create.uNameN,
            birthday = create.uBirthday,
            phones = listOf(FromAddContact
                .UserPhone(phone = create.uPhones[0], type = "", isDefault = true)),
            emails = listOf(FromAddContact
                .UserEmail(email = create.uPhones[0], type = "", isDefault = true)),
            note = create.uNote
        )
    }
    /** Setup for line chart **/
    private fun setDataForLineChart (count: Int, range: Float):LineDataSet {
        val values = ArrayList<Entry>()
        for (i in 0 until count) {
            val `val` = (Math.random() * (range + 1)).toFloat() + 20
            values.add(Entry(i.toFloat(), `val`))
        }
        val set1: LineDataSet
            set1 = LineDataSet(values, "Rewards of week")
        return set1
    }

    private fun setDataForPieChart(count: Int, range: Float): PieDataSet {
        val values = java.util.ArrayList<PieEntry>()

        val parties = arrayOf(
            "", "", "Party C", "Party D", "Party E", "Party F", "Party G", "Party H",
            "Party I", "Party J", "Party K", "Party L", "Party M", "Party N", "Party O", "Party P",
            "Party Q", "Party R", "Party S", "Party T", "Party U", "Party V", "Party W", "Party X",
            "Party Y", "Party Z")

        for (i in 0 until count) {
            values.add(
                PieEntry((Math.random() * range + range/5).toFloat(),
                    parties.get(i % parties.size)
                )
            )
        }
        val dataSet = PieDataSet(values, null) //"Election Results")
        dataSet.sliceSpace     = 3f
        dataSet.selectionShift = 5f

        dataSet.setColors(ClrExt.color_gold_l, ClrExt.color_blue_500_op20)
        dataSet.setSelectionShift(0f);
        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter())
        data.setValueTextSize(0f)
        data.setValueTextColor(Color.WHITE)

        return dataSet

    }

    private fun setDataForBarChart(count: Int, range: Float):BarDataSet {
        val start = 1f
        val values = ArrayList<BarEntry>()
        var i = start //.toInt()
        while (i < start + count) {
            val `val` = (Math.random() * (range + 1)).toFloat()
                values.add(BarEntry(i, `val`))
            i++
        }
        val set1: BarDataSet

        set1 = BarDataSet(values, "The year 2017")
        set1.setDrawIcons(false)
        set1.setDrawValues(false)

        set1.highLightColor = ClrExt.color_blue_400

        set1.setValueTextColor(ClrExt.color_gray_200)
        set1.setGradientColor(ClrExt.color_blue_500_op20, ClrExt.color_blue_500_op20)
        return set1
    }

    internal fun fetchCalendar(): ResponseRepository{
        return if(rCalendars.isNullOrEmpty()){
            val _r = readDataBaseCalendar()
            if (_r.type.equals("success")){
                rCalendars.clear()
                rCalendars.addAll(convertDtCalendar(_r.body))
//                updateUserCalendar()
                ResponseRepository.RequestCalendarGet(
                    message = BaseViewModel.MessageState.MessageNothing,
                    content = rCalendars)
            }else{
                ResponseRepository.RequestErrorResult(
                    message = BaseViewModel.MessageState.MessageDanger(
                        color = BaseViewModel.AlertColor(
                            back = Color.RED,
                            text = Color.BLACK),
                        content = BaseViewModel.AlertText(
                            title = "Error!",
                            texts = "Error calendar loaded!..")))
            }
        }else{
            ResponseRepository.RequestCalendarGet(
                message = BaseViewModel.MessageState.MessageNothing,
                content = rCalendars)
        }
    }

    internal suspend fun newMeetGET(c: MeetCalendarAttr): ResponseRepository{
        return try {
            networkServices.currentCalendarConnect?.calendarPst(
                    authHeader = authParams,
                    appId = meet.sumra.app.BuildConfig.APP_ID,
                    body = FromCreateCalendar(
                        id = c.id,
                        title = c.title,
                        note = c.note,
                        startDate = c.startDate,
                        finalDate = c.finalDate,
                        password = c.password,
                        ownerId = c.ownerId,
                        participants = c.participants,
                        isLobby = c.isLobby,
                        liveStream = c.liveStream,
                        updated_at = c.updated_at,
                        created_at = c.created_at
                    ))
        } catch (th: RuntimeException){
            ResponseRepository.RequestErrorResult(show(type = BaseViewModel.DANGER,
                message = BaseViewModel.AlertText(title = th.message!!,
                    texts = th.message!!)))
        }.let{ tmp ->
            when(tmp){
                is TaskRespCalendar.Data ->{
                    val t = tmp as FetchMeetCalendar
                    val r = convertDtCalendar(mutableListOf(t.body as MeetCalendar))
                    rCalendars.add(0, r[0])
                    ResponseRepository.RequestCalendarGet(
                          message = show(
                              type = tmp.type
                            , message = BaseViewModel.AlertText(
                                  title = tmp.title
                                , texts = tmp.message))
                        , content = rCalendars)
                }
                is TaskRespCalendar.Fail ->{
                    ResponseRepository.RequestErrorResult(message = show(type = tmp.fail.status
                        , message = BaseViewModel.AlertText(title = tmp.fail.title
                            , texts = tmp.fail.message)))
                }
                else -> {
                    ResponseRepository.RequestErrorResult(show(type = BaseViewModel.DANGER,
                        message = BaseViewModel.AlertText(title = "",
                            texts = "")))
                }
            }
        }
    }

    private fun convertDtCalendar(mc: List<MeetCalendar>): MutableList<DtMeetCalendar> {
        val d = mutableListOf<DtMeetCalendar>()
        mc.forEach {
            d.add(
                DtMeetCalendar(
                    dMeetId = it.attributes.id,
                    dTitle = it.attributes.title,
                    dNote = it.attributes.note,
                    dStartDate = it.attributes.startDate,
                    dFinalDate = it.attributes.finalDate,
                    dOwnerId = it.attributes.ownerId,
                    dPassword = it.attributes.password,
                    dIsLobby = it.attributes.isLobby,
                    dCreated_at = it.attributes.created_at,
                    dUpdated_at = it.attributes.updated_at,
                    dParticipants = searchDataBase(it.attributes.participants),
                    dLiveStream = meet.sumra.app.data.mdl.DtMeetStream(
                        dStreamKey = it.attributes.liveStream?.streamKey,
                        dStreamLink = it.attributes.liveStream?.streamLink)
                ))
        }
        return d
    }

    suspend internal fun fethRewardBonus(): ResponseRepository {
        if(!pageRewardBonus.plazaRewardBonus.isNullOrEmpty()){
            return ResponseRepository.RewardBonusGet(
                message = show(type = "success", message = BaseViewModel
                    .AlertText("Reward bonus", "Loaded success!..") ),
                body = pageRewardBonus.plazaRewardBonus[0])
        }else{
            delay(Random.nextLong(524, 2240))
            val t = Gson().fromJson<PlazaRewardBonus>(MockJsonPlaza.fetchRewardBonus(), PlazaRewardBonus::class.java)
            pageRewardBonus.plazaRewardBonus.clear()
            pageRewardBonus.plazaRewardBonus.add(t)
            return ResponseRepository.RewardBonusGet(
                message = show(type = "success", message = BaseViewModel
                    .AlertText("Reward bonus", "Loaded success!..") ),
                body = pageRewardBonus.plazaRewardBonus[0])
        }
    }

    suspend internal fun fethDivitsTotalBalance(): ResponseRepository {
        if(!pageRewardBonus.plazaRewardBonus.isNullOrEmpty()){
            return ResponseRepository.RewardBonusGet(
                message = show(type = "success", message = BaseViewModel
                    .AlertText("Reward bonus", "Loaded success!..") ),
                body = pageRewardBonus.plazaRewardBonus[0])
        }else{
            delay(Random.nextLong(524, 2240))
            val t = Gson().fromJson<PlazaRewardBonus>(MockJsonPlaza.fetchRewardBonus(), PlazaRewardBonus::class.java)
            pageRewardBonus.plazaRewardBonus.clear()
            pageRewardBonus.plazaRewardBonus.add(t)
            return ResponseRepository.RewardBonusGet(
                message = show(type = "success", message = BaseViewModel
                    .AlertText("Reward bonus", "Loaded success!..") ),
                body = pageRewardBonus.plazaRewardBonus[0])
        }
    }

    suspend internal fun fethRewardChart(type: cards.EPeriod): ResponseRepository {
        delay(Random.nextInt(500, 2500).toLong())
        val tmp = getDivitsRewardStatistic(type)
        pageRewardBonus.plazaRewardChart.clear()
        pageRewardBonus.plazaRewardChart.add(convertToBarData(tmp))
        pageRewardBonus.plazaRewardStatistic.clear()
        pageRewardBonus.plazaRewardStatistic.add(tmp.totalBalance)
            return  ResponseRepository.RewardChartGet(
            message = show(type = "success", message = BaseViewModel
                .AlertText("Reward chart", "Loaded success!..") ),
            body = PlazaRewardChart(pageRewardBonus.plazaRewardChart[0]),
            statistic = pageRewardBonus.plazaRewardStatistic[0])
    }

    suspend internal fun fethRewardHowTo(): ResponseRepository {
        if(!pageHowToReward.howToRewards.isNullOrEmpty()){
            return  ResponseRepository.RewardHowToGet(
                message = show(type = "success", message = BaseViewModel
                    .AlertText("Reward chart", "Loaded success!..")),
                body = pageHowToReward.howToRewards[0])
        }else{
            delay(1834)
            val t = Gson().fromJson<PlazaHowToReward>(MockJsonPlaza.fetchRewardHowTo()
                , PlazaHowToReward::class.java)
            pageHowToReward.howToRewards.clear()
            pageHowToReward.howToRewards.add(t)
            return  ResponseRepository.RewardHowToGet(
                message = show(type = "success", message = BaseViewModel
                    .AlertText("Reward chart", "Loaded success!..")),
                body = pageHowToReward.howToRewards[0])
        }
    }

    suspend internal fun fethBannerInformation(): ResponseRepository{
        delay(1245)
        return try {
            Gson().fromJson(JsonPojoBanner.fetchInfoBanner, BannerLoaded::class.java)
        }catch (t: RuntimeException){
            ResponseRepository.RequestErrorResult(message = BaseViewModel
                .MessageState.MessageDanger(BaseViewModel.AlertText(
                    title = "Runtime error exception!",
                    texts = t.message.toString())))
        }.let { it ->
            val t = it as BannerLoaded
            ResponseRepository.RecentToGet(
                message = show(t.type, BaseViewModel.AlertText(t.title, t.message)),
                body = t.banner)
        }
    }

    suspend internal fun fetchHistoryInformation(): ResponseRepository{
        return try {
            meetDatabase.historyDao().fetchAllHistory()
        }catch (t: RuntimeException){
            ResponseRepository.RequestErrorResult(message = BaseViewModel
                .MessageState.MessageDanger(BaseViewModel.AlertText(
                    title = "Runtime error exception!",
                    texts = t.message.toString())))
        }.let { it ->
            val t = it as List<HistoryEntity>
            val r = mutableListOf<DtMeetHistory>()
            val c = Calendar.getInstance()
            t.forEach {
                c.timeInMillis = it.startTimeMeet
                val sY = c.get(Calendar.YEAR)
                val sM = c.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) // get(Calendar.MONTH)
                val sD = c.get(Calendar.DATE)
                val sHour = c.get(Calendar.HOUR_OF_DAY)
                val sMin = c.get(Calendar.MINUTE)
                c.timeInMillis = it.finalTimeMeet
                val fHour = c.get(Calendar.HOUR_OF_DAY)
                val fMin = c.get(Calendar.MINUTE)

                r.add(DtMeetHistory(
                      historyId = it.historyId
                    , sessionMeet = it.sessionMeet
                    , meetName = it.meetName
                    , meetDate = "$sD $sM $sY"
                    , meetStartTime = "$sHour:$sMin"
                    , meetFinalTime = "$fHour:$fMin"
                ))
            }

            ResponseRepository.HistoryToGet(
                message = BaseViewModel.MessageState.MessageNothing,
                body = r)
        }
    }

    suspend internal fun insertHistoryInformation(history: HistoryEntity): ResponseRepository{
        return try {
            meetDatabase.historyDao().insertHistory(history)
            Gson().fromJson(JsonPojoBanner.fetchInfoBanner, BannerLoaded::class.java)
        }catch (t: RuntimeException){
            ResponseRepository.RequestErrorResult(message = BaseViewModel
                .MessageState.MessageDanger(BaseViewModel.AlertText(
                    title = "Runtime error exception!",
                    texts = t.message.toString())))
        }.let { it ->
            val t = it as BannerLoaded
            ResponseRepository.RecentToGet(
                message = show(t.type, BaseViewModel.AlertText(t.title, t.message)),
                body = t.banner)
        }
    }

    suspend fun updateCurrentBalances(): RespStatistic {
        delay(Random.nextInt(569, 2468).toLong())
        return RespStatistic.Balances(body = Balances(
            cardManyAll = "2548",
            cardManyUpVal = "245",
            cardManyUpPer = "24",
            cardManyDownVal = "45",
            cardManyDownPer = "2",
            cardAvailable = "6835"
        ))
    }

    suspend fun updateCurrentEarnings(): RespStatistic {
        delay(Random.nextInt(569, 2567).toLong())

        return RespStatistic.Earnings(body = Earnings(
            cardManyAll = 6854.7F,
            cardLineChart = fetchLineData(),
            cardLabel = 25,
            cardManyMonth = 254.51F ,
            cardPercent = 7
        ))
    }

    suspend fun updateCurrentReferrals(): RespStatistic {
        delay(Random.nextInt(569, 2567).toLong())

        return RespStatistic.Referrals(body = Referrals(
            cardReferrals = 568.58F,
            cardGrOrFall = 12,
            cardInvite = 125F,
            cardBarChart = fetchBarData()))
//        BarDataSet(entry, "Referrals"))
    }

    suspend fun updateCurrentContacts(): RespStatistic {
        delay(Random.nextInt(569, 2567).toLong())
        return RespStatistic.Contacts(body = Contacts(
            cardBookAll = 658,
            cardBookInv = 354,
            cardBookPer = 12.5F,
            cardConversionRate = 55))
    }

    suspend fun updateCurrentRewards(): RespStatistic {
        delay(Random.nextInt(569, 2567).toLong())
        return RespStatistic.Rewards(body = Rewards(
            cardRewardsAll = 16.500f,
            cardRewardsUSD = 1254.21f,
            cardRewardsTotal = 2568,
            cardRewardsSpendVal = 96,
            cardRewardsSpendPer = -2,
            cardRewardsEarnedDiv = 900,
            cardRewardsEarnedPer = -3))
    }

    suspend fun updateCurrentCashBack(): RespStatistic {
        delay(Random.nextInt(569, 2567).toLong())
        return RespStatistic.Cashback( body = meet.sumra.app.data.mdl.statistic.CashBack(
            cardManyAll = 2549.14F,
            cardLineChart = fetchLineData() ,
            cardManyMonth = 1245.25F,
            cardPercent = 12)
        )
    }

    suspend fun updateCurrentRentpayments(): RespStatistic {
        delay(Random.nextInt(569, 2567).toLong())
        return RespStatistic.RentPay( body = RentPay(
            cardManyAll = 2549.14F,
            cardLineChart = fetchLineData() ,
            cardManyMonth = 1245.25F,
            cardPercent = 12)
        )
    }

    suspend fun updateCurrentTransfers(): RespStatistic {
        delay(Random.nextInt(569, 2567).toLong())
        return RespStatistic.Transfer(body = Transfers(
            cardTransfers = 568.58F,
            cardGrOrFall = 12,
            cardTransMnth = 125F,
            cardBarChart = fetchBarData()))
    }

    suspend fun updateCurrentOverview(period: cards.EPeriod): RespStatistic {
        delay(Random.nextInt(569, 2567).toLong())
        return RespStatistic.Overview(body = Overview(
            overBarChart   = fetchBarOverviewData(cards.EPeriod.YEAR),
            overGlEarnings = 326,
            overRewards    = 1258,
            overCashbacks  = 226,
            overIvdUsers   = 36,
            overConvRate   = 60
        ))
    }

    data class DtMeetStream(
        val streamKey :String? = null,
        val streamLink: String? = null)

}