package meet.sumra.app.repository.database

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
//import meet.sumra.app.sss.RoomMeetDatabase
//import meet.sumra.app.sss.dao.SessionDao
//import meet.sumra.app.sss.models.SessionEntity
import xyn.meet.storange.RoomMeetDatabase
import xyn.meet.storange.dao.SessionDao
import xyn.meet.storange.models.SessionEntity
import javax.inject.Inject

class HistoryRepository @Inject constructor (meetDatabase: RoomMeetDatabase) {

    private val sessionDAO: SessionDao

    init {
        sessionDAO  = meetDatabase.sessionDao()
    }

    fun fetchSession(): SessionEntity {
        return sessionDAO.fetchSession()[0]
    }

    fun insertSession(session: SessionEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            sessionDAO.insertSession(session)
        }
    }

//    fun deleteBook(book: BookEntity) {
//        CoroutineScope(Dispatchers.IO).launch {
//            bookDAO.deleteBook(book)
//        }
//    }
//
//    fun updateBook(book: BookEntity) {
//        CoroutineScope(Dispatchers.IO).launch {
//            bookDAO.updateBook(book)
//        }
//    }
}