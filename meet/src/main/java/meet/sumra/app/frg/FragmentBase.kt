package meet.sumra.app.frg

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import meet.sumra.app.AppMeet
import meet.sumra.app.R
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.interfaces.IOnBackPressed
import meet.sumra.app.interfaces.UrlListener
import meet.sumra.app.utils.LogUtil

abstract class FragmentBase<VB: ViewBinding>: Fragment(), IOnBackPressed, UrlListener {

    protected lateinit var viewFragment: VB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createDaggerDependencies()

        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true /* enabled by default */) {
                override fun handleOnBackPressed() {
                    LogUtil.info(tag = "Base fragment...", "BackPressed -> Click")
                    onBackPressed()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewFragment = getBinding()
        return viewFragment.root
    }

    abstract fun getBinding(): VB

    protected abstract fun injectDependency(component: ComponentViewModel)
//    protected abstract fun injectDependency(component: ComponentViewStatistic)

    private fun createDaggerDependencies() {
        injectDependency(AppMeet.getInstance().getViewModelComponent())
    }

    override fun onImageReady(uri: Uri?) {
        if (uri != null) {
            uploadAvatar(uri)
        } else {
            Toast.makeText(requireContext(), "Cannot retrieve cropped value", Toast.LENGTH_SHORT).show()
        }
    }

    private fun uploadAvatar(uri: Uri) {
//        displayLoadingView()
//        lifecycleScope.launch {
//            val result = runCatching {
//                session.updateAvatar(session.myUserId, uri, getFilenameFromUri(context, uri) ?: UUID.randomUUID().toString())
//            }
//            if (!isAdded) return@launch
//            onCommonDone(result.fold({ null }, { it.localizedMessage }))
//        }
    }

    open fun getEnterAnimation() = R.anim.anim_show_r_to_l
    open fun getExitAnimation()  = R.anim.anim_hide_r_to_l

}
