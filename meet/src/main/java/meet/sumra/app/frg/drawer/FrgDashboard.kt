package meet.sumra.app.frg.drawer

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import meet.sumra.app.databinding.FrgDashboardBinding
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.frg.FragmentBase
import meet.sumra.app.vm.vm_home.ViewModelHome
import javax.inject.Inject

class FrgDashboard: FragmentBase<FrgDashboardBinding>() {

    companion object{
        val TAG = FrgDashboard::class.java.simpleName
        fun getInstance(): Fragment{
            return FrgDashboard()
        }
    }

    internal lateinit var viewModel: ViewModelHome
    @Inject set

    private fun setupUI(){

    }

    private fun addListener(){
        viewFragment.frgDashboardBackButton.setOnClickListener { btn -> viewModel.onBackClickListener()}
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        addListener()
    }

    override fun onBackPressed(): Boolean {
        return true
    }

    override fun getBinding(): FrgDashboardBinding {
        return FrgDashboardBinding.inflate(layoutInflater)
    }

    override fun injectDependency(component: ComponentViewModel) {
        component.inject(this)
    }

}