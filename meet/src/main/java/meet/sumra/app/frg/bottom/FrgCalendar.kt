package meet.sumra.app.frg.bottom

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import meet.sumra.app.R
import meet.sumra.app.data.enums.EAvatarType
import meet.sumra.app.databinding.FrgCalendarBinding
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.ext.loadRes
import meet.sumra.app.frg.FragmentBase
import meet.sumra.app.vm.vm_home.ViewModelHome
import javax.inject.Inject

class FrgCalendar: FragmentBase<FrgCalendarBinding>() {

    companion object{
        val TAG = FrgCalendar::class.java.simpleName
        fun getInstance(): FragmentBase<*>{
            return FrgCalendar()
        }
    }

    internal lateinit var homeViewModel: ViewModelHome
    @Inject set

    private fun setupUI(){
        viewFragment.root.background = homeViewModel.fetchGradient()
        val divider = DividerItemDecoration(requireContext(), RecyclerView.VERTICAL)
        divider.setDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.divider_calendar_card)!!)
        viewFragment.frgContactAllList.apply {
            adapter = homeViewModel.adapterCldr()
            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false)
            addItemDecoration(divider)
        }
//        viewFragment.frgCalendarCreateMeet.loadRes(R.drawable.ic_button_add, EAvatarType.CIRCLE)
    }

    private fun addListener(){
        viewFragment.frgCalendarCreateMeet.setOnClickListener {
            showDialogMeetScheduler()
        }
    }

    private fun showDialogMeetScheduler(){
        homeViewModel.onShowDialogEditNewMeet()
    }

    private fun stateCalendar() = Observer<ViewModelHome.StateCalendar> { calendar ->
        when(calendar){
            is ViewModelHome.StateCalendar.CalendarDft -> {

            }
            is ViewModelHome.StateCalendar.CalendarGet -> {
//                homeViewModel.updateCalendarList()
            }
            is ViewModelHome.StateCalendar.CalendarPst -> {

            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeViewModel.initCalendar()
        homeViewModel.viewStateCldr.observe(viewLifecycleOwner, stateCalendar())
        setupUI()
        addListener()
    }

    override fun onBackPressed(): Boolean {
        return true
    }

    override fun getBinding(): FrgCalendarBinding {
        return FrgCalendarBinding.inflate(layoutInflater)
    }

    override fun injectDependency(component: ComponentViewModel) {
        component.inject(this)
    }

}