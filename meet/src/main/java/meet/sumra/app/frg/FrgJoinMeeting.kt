package meet.sumra.app.frg

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import meet.sumra.app.act.ActivityHome
import meet.sumra.app.databinding.FrgNewMeetBinding
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.utils.LogUtil
import java.lang.ref.WeakReference
import java.util.*

class FrgJoinMeeting: FragmentBase<FrgNewMeetBinding>() {

    private lateinit var bind   : FrgNewMeetBinding
    private lateinit var actHome: WeakReference<ActivityHome>

    companion object{

        val TAG = UUID.randomUUID().toString()

        fun getInstance(): FragmentBase<*>{
            return FrgJoinMeeting()
        }
    }

//    @Inject
//    lateinit var repository: MeetChatRepository
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        AppMeet.getInstance().meetComponent.inject(this)
//    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        actHome = WeakReference(requireActivity() as ActivityHome)
        bind = FrgNewMeetBinding.inflate(inflater)
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
    }

    override fun onBackPressed(): Boolean {
        return true
    }

    private fun setupUI(){
        bind.btnMeetCreate.setOnClickListener { actionCreateMeet() }
        bind.btnCloseMeet.setOnClickListener { actionCloseMeet() }
    }

    private fun actionCreateMeet(){
        LogUtil.info(txt = "action create new meet...")
    }

    private fun actionCloseMeet(){
        LogUtil.info(txt = "action close meet...")
        actHome.get()?.run {
            helpFragmentHide()
        }
    }

    override fun getBinding(): FrgNewMeetBinding {
        TODO("Not yet implemented")
    }

    override fun injectDependency(component: ComponentViewModel) {
        TODO("Not yet implemented")
    }
}