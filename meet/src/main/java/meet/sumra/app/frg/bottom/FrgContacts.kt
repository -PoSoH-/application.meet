package meet.sumra.app.frg.bottom

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import meet.sumra.app.data.pojo.contacts.UserContactCreate
import meet.sumra.app.databinding.FrgContactsBinding
import meet.sumra.app.ext.*
import meet.sumra.app.ext.ExtAnimation.hideDtoU
import meet.sumra.app.ext.ExtAnimation.hideUtoD
import meet.sumra.app.ext.ExtAnimation.showDtoU
import meet.sumra.app.ext.ExtAnimation.showUtoD
import meet.sumra.app.ext.ExtAnimation.switchView
import meet.sumra.app.ext.ExtAnimation.textTrim
import meet.sumra.app.ext.convertToBase64
import meet.sumra.app.ext.editEmpty
import meet.sumra.app.frg.FragmentBase
import meet.sumra.app.utils.registerForPermissionsResult
import java.io.File
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import chat.sumra.lib.multipicker.MultiPicker
import chat.sumra.lib.multipicker.entity.MultiPickerImageType
import com.google.accompanist.pager.ExperimentalPagerApi
import com.skydoves.landscapist.glide.GlideImage
import com.yalantis.ucrop.UCrop
import meet.sumra.app.R
import meet.sumra.app.data.pojo.contacts.ContactHoldCompose
import meet.sumra.app.data.pojo.contacts.ContactHoldState
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.interfaces.UrlListener
import meet.sumra.app.utils.ContactConfig
import meet.sumra.app.utils.PERMISSIONS_FOR_TAKING_PHOTO
import meet.sumra.app.utils.checkPermissions
import meet.sumra.app.views.compose.ui_theme.StyleText
import meet.sumra.app.vm.BaseViewModel
import meet.sumra.app.vm.CustomScrollerViewProvider
import meet.sumra.app.vm.vm_home.ViewModelHome
import sdk.net.meet.LogUtil
import sdk.net.meet.api.contacts.response.FetchContact
import javax.inject.Inject

class FrgContacts: FragmentBase<FrgContactsBinding>() {

    companion object{
        val TAG = FrgContacts::class.java.simpleName
        fun getInstance(): FragmentBase<*> {
            return FrgContacts()
        }
    }

    internal lateinit var viewModel: ViewModelHome
    @Inject set

    @ExperimentalMaterialApi
    @ExperimentalFoundationApi
    @ExperimentalPagerApi
    private fun setupUI(){
        viewFragment.root.background = viewModel.fetchGradient()
        setupFont()

        viewFragment.frgContactAllField.setImageResource(viewModel.isCntcAddExpMode.getInfoButtonState().icon)

//        viewFragment.frgContactAllList.apply {
//            adapter = viewModel.getAdapterContact()
//            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
////            val decor = DividerItemDecoration(context, LinearLayoutManager.HORIZONTAL)
////            decor.setDrawable(resources.getDrawable(R.drawable.divider_horizontal_contact_list))
////            addItemDecoration(decor)
//            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL).apply {
//                ContextCompat.getDrawable(
//                    context,
//                    R.drawable.divider_horizontal_contact_list)?.let {
//                    setDrawable(it)
//                }
//            })
//        }
//
//        viewFragment.frgContactFastScroll.setRecyclerView(viewFragment.frgContactAllList)
//        viewFragment.frgContactFastScroll.
//        val myViewProvider = CustomScrollerViewProvider()
//        viewFragment.frgContactFastScroll.setViewProvider(myViewProvider)

//        viewFragment.frgContactInviteList.apply {
//            adapter = viewModel.getAdapterInvite()
//            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
////            val decor = DividerItemDecoration(context, LinearLayoutManager.VERTICAL)
////            decor.setDrawable(resources.getDrawable(R.drawable.divider_vertical_invite_list))
////            addItemDecoration(decor)
//            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL).apply {
//                ContextCompat.getDrawable(
//                    context,
//                    R.drawable.divider_vertical_invite_list)?.let {
//                    setDrawable(it)
//                }
//            })
//        }

        viewModel.updateTypeFace().run {

        }

        viewFragment.frgContactSelectYear.setContent { viewModel.fetchYearsSetup() }
        viewFragment.frgContactSelectMonth.setContent { viewModel.fetchMonthsSetup() }
        viewFragment.frgContactSelectDay.setContent { viewModel.fetchDaysSetup() }

        viewFragment.frgContactShowCompose.setContent {
            ShowListContacts()
        }

        viewFragment.frgContactInviteCompose.setContent {
            ShowInviteList()
        }
    }

    private fun addListener(){

        viewFragment.frgContactSearchButton.setOnClickListener    { search ->
            if (viewFragment.frgContactSearchEnter.text.isNullOrEmpty()){
                viewModel.fetchAllContacts()
            }else{
                viewModel.fetchSearchContacts(
                    config = ContactConfig.getSearch(
                        text = viewFragment
                            .frgContactSearchEnter.text.toString())
                )
            }
        }

        viewFragment.frgContactAddInviteButton.setOnClickListener { addCon ->
            viewFragment.frgContactUserAddView.showDtoU()
        }

        viewFragment.frgContactSearchEnter.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s?.length == 0) viewModel.fetchAllContacts()
//                LogUtil.info(TAG, "cPhanged -> $s")
//                viewModel.getAdapterContact().updateListByFilter(s.toString())
            }
            override fun afterTextChanged(s: Editable?) {}
        })

        viewFragment.frgContactSubmitContact.setOnClickListener { submit ->
            if(checkValidateField()) {
                submit.hideKeyboard()
                viewFragment.frgContactUserAddView.hideUtoD()
                viewFragment.apply {
                    viewModel.createNewContact(
                        UserContactCreate(
                            uNameP = frgContactAddEnterPrefix.textTrim(),
                            uNameF = frgContactAddEnterFirst.textTrim(),
                            uNameM = frgContactAddEnterMiddle.textTrim(),
                            uNameL = frgContactAddEnterLast.textTrim(),
                            uNameS = frgContactAddEnterSuffix.textTrim(),
//                        if(frgContactAddDisplayName.text.isNullOrEmpty()){
//                            uNameD = "${viewFragment.frgContactAddEnterFirst.text.toString()} ${viewFragment.frgContactAddEnterLast.text.toString()}",
//                        }else{
                            uNameD = frgContactAddDisplayName.textTrim(),
//                        }
                            uNameN = frgContactAddEnterNickName.textTrim(),
                            uPhones = listOf(viewFragment.frgContactAddEnterPhone.textTrim()),
                            uEmails = listOf(viewFragment.frgContactAddEnterEmail.textTrim()),
                            uAvatar = viewFragment.frgContactUserAvatar.convertToBase64(),
                            uBirthday = viewModel.getBirthDay(),
                            uNote = frgContactAddEnterNote.textTrim()
                        )
                    )
                }
                emptyContactField()
            }
        }

        viewFragment.frgContactCloseContact.setOnClickListener { close ->
            close.hideKeyboard()
            viewFragment.frgContactUserAddView.hideUtoD()
            emptyContactField()
        }

        viewFragment.let { v ->

            v.frgContactAllField.setOnClickListener       { check ->
//                switchExtention((check as CheckBox).isChecked)
                v.frgContactAllField.setImageResource(viewModel.isCntcAddExpMode.switchStateGetParams().icon)
                switchExtention(viewModel.isCntcAddExpMode.getState())
            }

            v.frgContactUserAvatarBox.setOnClickListener  { avt ->
                if (v.frgContactShowTakeOrCheckPhoto.isVisible) {
                    v.frgContactShowTakeOrCheckPhoto.hideUtoD()
                } else {
                    v.frgContactShowTakeOrCheckPhoto.showDtoU()
                }
            }

            v.frgContactSelectPhotoBtn.setOnClickListener { avt ->
//                intentSelectPhoto()
                v.frgContactShowTakeOrCheckPhoto.hideUtoD()
                onAvatarTypeSelected(Type.Gallery)
//                pickImageActivityResultLauncher
            }

            v.frgContactTakePhotoBtn.setOnClickListener   { avt ->
//                intentTakePhoto()
                v.frgContactShowTakeOrCheckPhoto.hideUtoD()
                onAvatarTypeSelected(Type.Camera)
//                takePhotoPermissionActivityResultLauncher
            }

            v.frgContactCancelPhotoBtn.setOnClickListener { avt ->
                v.frgContactShowTakeOrCheckPhoto.hideUtoD()
            }

            v.frgContactUserAddView.setOnClickListener    { back -> Unit

            }
        }
    }

    private fun setupFont(){
        viewModel.updateTypeFace().let { face ->
            viewFragment.run{
                frgContactAddLabelPrefix  .typeface = face
                frgContactAddEnterPrefix  .typeface = face
                frgContactAddLabelFirst   .typeface = face
                frgContactAddEnterFirst   .typeface = face
                frgContactAddLabelMiddle  .typeface = face
                frgContactAddEnterMiddle  .typeface = face
                frgContactAddLabelLast    .typeface = face
                frgContactAddEnterLast    .typeface = face
                frgContactAddLabelSuffix  .typeface = face
                frgContactAddEnterSuffix  .typeface = face
                frgContactAddLabelDisplay .typeface = face
                frgContactAddDisplayName  .typeface = face
                frgContactAddLabelNickName.typeface = face
                frgContactAddEnterNickName.typeface = face
                frgContactAddLabelPhone   .typeface = face
                frgContactAddEnterPhone   .typeface = face
                frgContactAddLabelEmail   .typeface = face
                frgContactAddEnterEmail   .typeface = face
                frgContactAddLabelNote    .typeface = face
                frgContactAddEnterNote    .typeface = face

                frgContactSelectPhotoLabel.typeface = face
                frgContactTakePhotoLabel  .typeface = face
                frgContactCancelPhotoLabel.typeface = face
            }
        }
    }

    private fun switchExtention(type: Boolean){
        viewFragment.let {
            type.run {
                it.frgContactAddPrefixBox.switchView(this)
                it.frgContactAddMiddleBox.switchView(this)
                it.frgContactAddSuffixBox.switchView(this)
                it.frgContactAddDisplayNameBox.switchView(this)
                it.frgContactAddNicknameBox.switchView(this)
                it.frgContactAddBirthdayBox.switchView(this)
                it.frgContactAddNoteBox.switchView(this)
            }
        }
    }

    private fun stateContact() = Observer<ViewModelHome.StateContacts> { stateContact ->
        when(stateContact){
            is ViewModelHome.StateContacts.ShowInvite -> {
                viewFragment.frgContactInviteBox.showUtoD()
            }
            is ViewModelHome.StateContacts.HideInvite -> {
                viewFragment.frgContactInviteBox.hideDtoU()
            }
            is ViewModelHome.StateContacts.Updates -> {


//                contactState += stateContact.contacts
//                contactState += stateContact.contacts
//                contactState += stateContact.contacts
//                contactState += stateContact.contacts
//                contactState += stateContact.contacts



//                viewFragment.frgContactInviteBox.hideDtoU()
            }
        }
    }

    private fun emptyContactField() {
        viewFragment.frgContactAddEnterFirst.editEmpty()
        viewFragment.frgContactAddEnterLast.editEmpty()
        viewFragment.frgContactAddEnterPhone.editEmpty()
        viewFragment.frgContactAddEnterEmail.editEmpty()
    }

    @ExperimentalMaterialApi
    @ExperimentalFoundationApi
    @ExperimentalPagerApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.initContact()
        viewModel.stateContacts.observe(viewLifecycleOwner, stateContact())
//
        setupUI()
        addListener()
    }

    override fun onStart() {
        super.onStart()
//        viewModel.updateDataReferral()
    }

    override fun onStop() {
        super.onStop()
//        viewModel.dataReferralClear()
    }

    override fun onBackPressed(): Boolean {
        if(viewFragment.frgContactUserAddView.isVisible){
            if(viewFragment.frgContactShowTakeOrCheckPhoto.isVisible){
                viewFragment.frgContactShowTakeOrCheckPhoto.hideUtoD()
                return false
            }else {
                viewFragment.frgContactUserAddView.hideUtoD()
                return false
            }
        }else {
            return true
        }
    }

    override fun getBinding(): FrgContactsBinding {
        return FrgContactsBinding.inflate(layoutInflater)
    }

    override fun injectDependency(component: ComponentViewModel) {
        component.inject(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        data?.apply {
            this.data.let{
                LogUtil.info(txt = it!!.path.toString())
            }.run {
                LogUtil.info(txt = "Empty data")
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    /***--- Take photo Multipicker ---***/

    private fun checkValidateField(): Boolean{
        if(viewFragment.frgContactAddEnterFirst.text.isNullOrEmpty()){
            viewModel.updateMessageSate(
                BaseViewModel.MessageState.MessageWarning(
                content = BaseViewModel.AlertText(
                    title = resources.getString(R.string.addNewContactFirstFail),
                    texts = resources.getString(R.string.addNewContactFirstText))))
            return false
        }
        if(viewFragment.frgContactAddEnterLast.text.isNullOrEmpty()){
            viewModel.updateMessageSate(BaseViewModel.MessageState.MessageWarning(
                content = BaseViewModel.AlertText(
                    title = resources.getString(R.string.addNewContactLastFail),
                    texts = resources.getString(R.string.addNewContactLastText))))
            return false
        }
        if(viewFragment.frgContactAddEnterPhone.text.isNullOrEmpty()){
            viewModel.updateMessageSate(BaseViewModel.MessageState.MessageWarning(
                content = BaseViewModel.AlertText(
                    title = resources.getString(R.string.addNewContactPhoneFail),
                    texts = resources.getString(R.string.addNewContactPhoneText))))
            return false
        }
        if(viewFragment.frgContactAddEnterEmail.text.isNullOrEmpty()){
            viewModel.updateMessageSate(BaseViewModel.MessageState.MessageWarning(
                content = BaseViewModel.AlertText(
                    title = resources.getString(R.string.addNewContactEmailFail),
                    texts = resources.getString(R.string.addNewContactEmailText))))
            return false
        }
        return true
    }

    private enum class Type {
        Camera,
        Gallery
    }

    private fun onAvatarTypeSelected(type: Type) {
        when (type) {
            Type.Camera ->
                if (checkPermissions(PERMISSIONS_FOR_TAKING_PHOTO,
                        requireActivity(),
                        takePhotoPermissionActivityResultLauncher)) {
                    doOpenCamera()
                }
            Type.Gallery ->
                MultiPicker.get(MultiPicker.IMAGE).single().startWith(pickImageActivityResultLauncher)
        }
    }

    private val takePhotoPermissionActivityResultLauncher = registerForPermissionsResult { allGranted ->
        if (allGranted) {
            doOpenCamera()
        }
    }

    private val takePhotoActivityResultLauncher = registerStartForActivityResult { activityResult ->
        if (activityResult.resultCode == Activity.RESULT_OK) {
            avatarCameraUri?.let { uri ->
                MultiPicker.get(MultiPicker.CAMERA)
                    .getTakenPhoto(requireActivity(), uri)
                    ?.let { startUCrop(it) }
            }
        }
    }

    private val pickImageActivityResultLauncher = registerStartForActivityResult { activityResult ->
        if (activityResult.resultCode == Activity.RESULT_OK) {
            MultiPicker
                .get(MultiPicker.IMAGE)
                .getSelectedFiles(requireActivity(), activityResult.data)
                .firstOrNull()
                ?.let { startUCrop(it) }
        }
    }

    private val uCropActivityResultLauncher = registerStartForActivityResult { activityResult ->
        if (activityResult.resultCode == Activity.RESULT_OK) {
            activityResult.data?.let {
                viewModel.urlTempAvatar = UCrop.getOutput(it)
                viewFragment.frgContactUserAvatar.loadUrl(viewModel.urlTempAvatar.toString())
//                listener.onImageReady(UCrop.getOutput(it))
            }
        }
    }

    private fun startUCrop(image: MultiPickerImageType) {
        val destinationFile = File(requireActivity().cacheDir, image.displayName.insertBeforeLast("_e_${System.currentTimeMillis()}"))
        val uri = image.contentUri

        createUCropWithDefaultSettings(context = requireContext(), uri, destinationFile.toUri(), getString(R.string.rotate_and_crop_screen_title))
            .withAspectRatio(1f, 1f)
            .getIntent(requireActivity())
            .let {
                uCropActivityResultLauncher.launch(it)
            }
    }

    private val listener = this as? UrlListener ?: error("Fragment must implement GalleryOrCameraDialogHelper.Listener")

    private var avatarCameraUri: Uri? = null
    private fun doOpenCamera() {
        avatarCameraUri = MultiPicker.get(MultiPicker.CAMERA).startWithExpectingFile(requireActivity(), takePhotoActivityResultLauncher)
    }

    /***   ***   ***   ***   ***   ***   ***   ***   ***   ***   ***   ***   ***   ***   ***/
//    private val contactState = mutableStateListOf<FetchContact>()
    lateinit var stateList: LazyListState
//    stateList.animateScrollToItem(10, 0)

//    private val charactersState = mutableStateListOf<String>()
    private val symbolHeight = 40

    @ExperimentalMaterialApi
    @ExperimentalFoundationApi
    @Composable
    fun ShowListContacts(){
        Box(modifier = Modifier
            .fillMaxSize()
            .background(color = Color.Transparent) //(red = 55, green = 55, blue = 55, alpha = 50))
        ) {
            Row(modifier = Modifier.fillMaxSize(1f),
                verticalAlignment = Alignment.CenterVertically
            ) {
                stateList = rememberLazyListState(viewModel.contactFilter.size)
//                    initialFirstVisibleItemIndex = 0,
//                    initialFirstVisibleItemScrollOffset = 0)
                val stateScroll = rememberScrollState()
                LazyColumn(
//                    state =  stateList,
                    modifier = Modifier
                        .fillMaxHeight(1f)
                        .weight(1f)
//                        .verticalScroll(ScrollState(0))
                ) {
                    val grTime = viewModel.contactFilter.groupBy { it.contact.displayName[0].toString() }
                    grTime.forEach { (it, history) ->
                        stickyHeader {
//                            charactersState.add(it)
                            AlphabetPeriodHeader(it)
                            Divider(modifier = Modifier
                                .height(1.dp)
                                .fillMaxWidth(1f),
                                color = Color(0xff989BA5))
                        }
                        itemsIndexed(history) { position, participant ->
                            Column(
//                                    modifier = Modifier.height(height = height.dp)
                            ) {
                                ItemContact(participant)
                            }
                        }
                    }
                }

//                LazyColumn(
//                    state = stateList,
//                    modifier = Modifier
//                        .width(symbolHeight.dp)
//                ) {
//                    itemsIndexed(viewModel.letter) { position, participant ->
//                        Column(
//                            modifier = Modifier
//                                .height(symbolHeight.dp)
//                                .width(symbolHeight.dp),
//                            horizontalAlignment = Alignment.CenterHorizontally
//                        ) {
//                            Surface(
//                                onClick = {
//                                    Log.i("ITEM CLICK", "ITEM POSITION ${position}")
//                                },
//                                modifier = Modifier.fillMaxSize(1f),
//                            ) {
//                                Row(
//                                    modifier = Modifier.fillMaxSize(1f),
//                                    horizontalArrangement = Arrangement.Start,
//                                    verticalAlignment = Alignment.CenterVertically
//                                ) {
//                                    Text(
//                                        text = participant,
//                                        style = StyleText.txtPgTkn(),
//                                        textAlign = TextAlign.End,
//                                        modifier = Modifier.fillMaxWidth(1f)
//                                    )
//                                }
//                            }
//                        }
//                    }
//                }
            }
        }
    }

    @Composable
    private fun AlphabetPeriodHeader(period: String){
        Surface(
            modifier = Modifier.height(symbolHeight.dp)
            , color = Color.Transparent //(0xBFDBEDFF)
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = period,
                    style = StyleText.txtPgTkn(),
                    textAlign = TextAlign.Center
//                    modifier = Modifier.fillMaxSize(1f)
                )
            }
        }
    }

    private val height = 45
    @ExperimentalMaterialApi
    @Composable
    fun ItemContact(entity: ContactHoldCompose){
        Surface (
            onClick = {
                Log.i("ITEM CLICK", "ITEM POSITION ${entity.isInvite}")
                entity.isInvite.value = !entity.isInvite.value
                if(!entity.isInvite.value){
                    viewModel.isInvitedContacts.remove(entity)
                }else {
                    viewModel.isInvitedContacts.add(entity)
                }

                if(viewModel.isInvitedContacts.size >= 1 && !viewModel.isInvite.value)
                    {viewModel.isInvite.value = true}
                if(viewModel.isInvitedContacts.size == 0 && viewModel.isInvite.value)
                    {viewModel.isInvite.value = false}

            }
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .padding(vertical = 10.dp)
//                .height(height = height.dp)
            ) {
                Row(
                    modifier = Modifier
                        .height(height.dp)
                        .fillMaxWidth(1f),
                    verticalAlignment = Alignment.CenterVertically
                ) {
//                Image(imageVector = , contentDescription = "")
                    Box(
                        modifier = Modifier
                            .height(height.dp)
                            .width (height.dp),
                        contentAlignment = Alignment.Center
                    ) {
                        if (entity.contact.avatar.isNullOrEmpty()) {
                            Image(
                                painter = painterResource(R.drawable.ic_placeholder),
                                contentDescription = "Placeholder",
                                modifier = Modifier.fillMaxSize()
                            )
                        } else {
                            GlideImage(
                                imageModel = entity.contact.avatar!!,
                                modifier = Modifier
                                    .height(height.dp)
                                    .width (height.dp)
                                    .clip(RoundedCornerShape(12.dp)),
                                contentScale = ContentScale.Crop,
//                    circularRevealedEnabled = true,
                                placeHolder = painterResource(id = R.drawable.ic_placeholder),
                                error = painterResource(id = R.drawable.ic_check_failure),
                            )
                        }
                    }
                    Row(
                        modifier = Modifier.fillMaxSize().padding(start = 8.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Column(
                            modifier = Modifier.weight(1f)
                        ) {
                            Text(
                                text = entity.contact.displayName, style = StyleText.txtListName()
                            )
                            val st = StringBuilder()
                            if (entity.contact.email.isNullOrEmpty()) {
                                if (entity.contact.phone.isNullOrEmpty()) {
                                    st.append("Not entered user email and phone...")
                                } else { st.append(entity.contact.email) }
                            } else { st.append(entity.contact.email) }
                            Text( text = st.toString(), style = StyleText.txtListDate() )
                        }
                        if (entity.isInvite.value) {
                            Image(
                                painter = painterResource(id = R.drawable.ic_check_success),
                                contentDescription = "showEnable",
                                modifier = Modifier.width(20.dp).height(20.dp),
                            )
                        } else {
                            Image(
                                painter = painterResource(id = R.drawable.ic_checkbox_emty),
                                contentDescription = "showDisable",
                                modifier = Modifier.width(20.dp).height(20.dp),
                            )
                        }
                    }
                }
            }
        }
    }

    @ExperimentalMaterialApi
    @Composable
    fun ShowInviteList(){
        if(viewModel.isInvite.value){
            viewFragment.frgContactInviteBox.showUtoD()
        }else{
            viewFragment.frgContactInviteBox.hideDtoU()
        }
        LazyRow(
            modifier = Modifier.fillMaxSize(1f)
        ){
            itemsIndexed(viewModel.isInvitedContacts){ indx, data ->
                ShowInviteItem(data)
                if(indx < viewModel.isInvitedContacts.size) Spacer(modifier = Modifier.width(7.dp))
            }
        }
    }

    private val sizeInv = 45

    @ExperimentalMaterialApi
    @Composable
    fun ShowInviteItem(invited: ContactHoldCompose){
        Box(
            Modifier
                .height((sizeInv + 7).dp)
                .width ((sizeInv + 7).dp),
        ) {
            Box(
                Modifier
                    .fillMaxSize(1f)
                    .height((sizeInv + 7).dp)
                    .width ((sizeInv + 7).dp),
                contentAlignment = Alignment.TopStart
            ) {
                if (invited.contact.avatar.isNullOrEmpty()) {
                    Image(
                        painter = painterResource(R.drawable.ic_placeholder),
                        contentDescription = "Placeholder",
                        modifier = Modifier
                            .height(sizeInv.dp)
                            .width (sizeInv.dp)
                    )
                } else {
                    GlideImage(
                        imageModel = invited.contact.avatar,
                        modifier = Modifier
                            .height(sizeInv.dp)
                            .width (sizeInv.dp)
                            .clip(RoundedCornerShape(12.dp)),
                        contentScale = ContentScale.Crop,
//                    circularRevealedEnabled = true,
                        placeHolder = painterResource(id = R.drawable.ic_placeholder),
                        error = painterResource(id = R.drawable.ic_check_failure),
                    )
                }
            }
            Box(
                Modifier
                    .fillMaxSize(1f)
                    .height((sizeInv + 7).dp)
                    .width ((sizeInv + 7).dp),
                contentAlignment = Alignment.BottomEnd
            ) {
                Surface(onClick = {
                    invited.isInvite.value = !invited.isInvite.value
                    viewModel.isInvitedContacts.remove(invited)},
                    modifier = Modifier
                        .background(color = Color.Transparent)
                        .clip(CircleShape)
                ) {
                    Image(
                        painter = painterResource(R.drawable.ic_invite_close),
                        contentDescription = "Placeholder",
                        modifier = Modifier
                            .height(16.dp)
                            .width (16.dp)
                            .clip(CircleShape)
                            .background(color = Color.Transparent)
                    )
                }
            }
        }
    }

}


