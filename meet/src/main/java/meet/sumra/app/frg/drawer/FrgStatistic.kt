package meet.sumra.app.frg.drawer

import android.os.Bundle
import android.view.View
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Observer
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.interfaces.datasets.IDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import meet.sumra.app.databinding.FrgStatisticBinding
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.frg.FragmentBase
import meet.sumra.app.views.compose.ui_theme.StyleText
import meet.sumra.app.vm.vm_home.ViewModelHome
import meet.sumra.app.R
import meet.sumra.app.data.configs.setupStatisticCharts
import meet.sumra.app.data.mdl.earnings.cards
import meet.sumra.app.data.mdl.earnings.cards.CardCashBacks
import meet.sumra.app.data.mdl.earnings.cards.CardContactBooks
import meet.sumra.app.data.mdl.earnings.cards.CardCurrentBalances
import meet.sumra.app.data.mdl.earnings.cards.CardGlobalEarnings
import meet.sumra.app.data.mdl.earnings.cards.CardOverview
import meet.sumra.app.data.mdl.earnings.cards.CardReferrals
import meet.sumra.app.data.mdl.earnings.cards.CardRentPayments
import meet.sumra.app.data.mdl.earnings.cards.CardReward
import meet.sumra.app.data.mdl.earnings.cards.CardTransfers
import meet.sumra.app.data.mdl.statistic.Overview
import meet.sumra.app.utils.LogUtil
import meet.sumra.app.vm.vm_home.StateHome
import meet.sumra.app.vm.vm_statistic.StateStatistic
import meet.sumra.app.vm.vm_statistic.ViewModelStatistic
import javax.inject.Inject

class FrgStatistic: FragmentBase<FrgStatisticBinding>() {

    companion object {
        val TAG = FrgStatistic::class.java.simpleName
        fun getInstance(): FragmentBase<*> {
            return FrgStatistic()
        }
    }

    internal lateinit var viewModel: ViewModelHome
        @Inject set
    internal lateinit var statistic: ViewModelStatistic
        @Inject set

//    private lateinit var earnings : LineChart
//    private lateinit var referrals: BarChart
//    private lateinit var cashBacks: LineChart
//    private lateinit var rentPay  : LineChart
//    private lateinit var transfers: BarChart
//    private lateinit var overview : BarChart

//    private fun addListener() {
//        viewFragment.frgStatisticBackButton.setOnClickListener { btn -> viewModel.onBackClickListener() }
//    }

    @ExperimentalMaterialApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.viewState.observe(requireActivity(), initObsHome())
        statistic.stateStatistic.observe(requireActivity(), initObsThis())

//        earnings = LineChart(requireContext())
//        earnings.data = LineData(statistic.globalEarnings.value)
//        setupStatisticCharts.setupLineChart(
//            earnings,
//            android.graphics.Color.parseColor("#FF4A7A")
//        )

//        referrals= BarChart(requireContext())
//        cashBacks= LineChart(requireContext())
//        rentPay  = LineChart(requireContext())
//        transfers= BarChart(requireContext())
//        overview = BarChart(requireContext())

        viewFragment.root.setContent { ShowStatistic() }

    }

    override fun onBackPressed(): Boolean {
        return true
    }

    override fun getBinding(): FrgStatisticBinding {
        return FrgStatisticBinding.inflate(layoutInflater)
    }

    override fun injectDependency(component: ComponentViewModel) {
        component.inject(this)
    }
//    override fun injectDependency(component: ComponentViewStatistic) {
//        component.inject(this)
//    }

    fun initObsHome()= Observer<StateHome> { meet ->

    }

    fun initObsThis()= Observer<StateStatistic> { statistic ->
        when(statistic){
            is StateStatistic.UpdateBalances -> {

            }
            is StateStatistic.UpdateEarnings -> {

            }
            is StateStatistic.UpdateReferrals -> {

            }
            is StateStatistic.UpdateContacts -> {

            }
            is StateStatistic.UpdateRewards -> {

            }
            is StateStatistic.UpdateCashback -> {

            }
            is StateStatistic.UpdateRentPay -> {

            }
            is StateStatistic.UpdateTransfer -> {

            }
            is StateStatistic.UpdateOverview -> {

            }
        }
    }

    @Preview
    @Composable
    @ExperimentalMaterialApi
    private fun ShowStatistic() {
        val rootBrush = Brush
            .horizontalGradient(listOf(Color(0xff02C2FF), Color(0xff0E6AE3)))
        Column(
            modifier = Modifier
                .fillMaxSize(1f)
                .background(brush = rootBrush, shape = RoundedCornerShape(size = 0.dp))
        ) {
            StatisticToolBar(stringResource(id = R.string.frg_appbar_statistic_label))
            PageAllList()
        }
    }

    @ExperimentalMaterialApi
    @Composable
    private fun PageAllList(){
        val rootTopCorner = 28.dp
        val rootPadding = 20.dp
        val separateCard = 12.dp
        val rootScrollState = rememberScrollState()
        Surface(
            modifier = Modifier.fillMaxSize(1f),
            color = Color(0xffFAF9FC),
            shape = RoundedCornerShape(
                topStart = rootTopCorner,
                topEnd = rootTopCorner)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize(1f)
                    .verticalScroll(rootScrollState)
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxSize(1f)
                        .padding(
                            top = 30.dp, bottom = separateCard,
                            start = rootPadding, end = rootPadding
                        )
                ) {
                    Text(
                        text = stringResource(id = R.string.frgStatisticBySection),
                        style = StyleText.txtStatisticLbl()
                    )
                }
                CardCurrentBalances(
                    balances = statistic.currentBalances,
                    loading = statistic.prgBalances
                )
                CardGlobalEarnings(
//                    dataLine = earnings,
                    earnings = statistic.globalEarnings,
                    loading  = statistic.prgEarnings
                )
                CardReferrals(
                    referrals = statistic.currentReferrals,
                    loading   = statistic.prgReferrals
                )
                CardContactBooks(
                    contacts = statistic.contactBooks,
                    loading  = statistic.prgBooks
                )
                CardReward(
                    rewRewards = statistic.currentRewards,
                    rewLoading = statistic.prgRewards
                )
                CardCashBacks(
                    cashback = statistic.currentCashBack,
                    loading  = statistic.prgCashBack
                )
                CardRentPayments(
                    cashback = statistic.currentRentPay,
                    loading  = statistic.prgRentPay
                )
                CardTransfers(
                    barTransfers = statistic.currentTransfers,
                    barLoading   = statistic.prgTransfers
                )
                CardOverview(
                    barOverview = statistic.currentOverview,
                    barOverLoad = statistic.prgOverview,
                    callSelect   = object : cards.PeriodValue {
                        override fun period(period: cards.EPeriod) {
                            LogUtil.info(txt = period.label)
                            statistic.fetchUpdateOverviewPeriod(period)
//                        viewModel.fetchNewSelectedPeriod(period)
                        }
                    },
//                    menuItems = cards.EPeriod.getPeriods()
                )
            }
        }
    }

    private fun periodSelect(p: Int){
//        viewModel.fetchNewSelectedPeriod(p)
    }

    @ExperimentalMaterialApi
    @Composable
    private fun StatisticToolBar(label: String){
        Row(
            modifier = Modifier
                .fillMaxWidth(1f)
                .height(dimensionResource(id = R.dimen.toolBarHeight))
                .background(color = Color.Transparent),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Surface(
                onClick = { viewModel.onBackClickListener() },
                modifier = Modifier
                    .height(dimensionResource(id = meet.sumra.app.R.dimen.toolBarHeight))
                    .width(dimensionResource(id = meet.sumra.app.R.dimen.toolBarHeight)),
                color = Color.Transparent
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(start = 8.dp),
                    contentAlignment = Alignment.Center
                ) {
                    Image(
                        modifier = Modifier
                            .height(24.dp)
                            .width(24.dp),
                        painter = painterResource(id = meet.sumra.app.R.drawable.ic_arrow_back_ios),
                        contentDescription = "Button back",
                        colorFilter = ColorFilter.tint(color = Color(0xffFFFFFF)) //, blendMode = BlendMode.Src)
                    )
                }
            }
            val txtPad = 16
            Text(
                modifier = Modifier
                    .weight(1f)
                    .padding(start = txtPad.dp, end = txtPad.dp),
                text = label,
                style = StyleText.txtEarnCardLbl()
            )
        }
    }

    @Preview
    @Composable
    @ExperimentalMaterialApi
    private fun ShowPreview() {
        ShowStatistic()
    }

    interface DataLineChartUpdates {
        fun updates(obj: ILineDataSet)
    }

    interface DataBarChartUpdates {
        fun updates(obj: IBarDataSet)
    }
}
