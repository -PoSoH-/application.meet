package meet.sumra.app.frg.bottom

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import meet.sumra.app.BuildConfig
import meet.sumra.app.R
import meet.sumra.app.databinding.FrgSettingsBinding
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.ext.restart
import meet.sumra.app.frg.FragmentBase
import meet.sumra.app.utils.setup.ThemeUtils
import meet.sumra.app.vm.vm_home.ViewModelHome
import javax.inject.Inject

class FrgSettings: FragmentBase<FrgSettingsBinding>() {

    companion object{
        val TAG = FrgSettings::class.java.simpleName
        fun getInstance(): FragmentBase<*>{
            return FrgSettings()
        }
    }

    internal lateinit var homeViewModel: ViewModelHome
        @Inject set

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeViewModel.viewStateSetup.observe(viewLifecycleOwner, stateSettings())
        setupUI()
        addListener()
    }

    override fun onBackPressed(): Boolean {
        return true
    }

    override fun getBinding(): FrgSettingsBinding {
        return FrgSettingsBinding.inflate(layoutInflater)
    }

    override fun injectDependency(component: ComponentViewModel) {
        component.inject(this)
    }

    private fun stateSettings() = Observer<ViewModelHome.StateAppSetup> { stateSetup ->
        when(stateSetup){
            is ViewModelHome.StateAppSetup.SetupDefault -> {}
            is ViewModelHome.StateAppSetup.UpdateVideo -> {
                viewFragment.settingsSectionVideoSwitch.setChecked(homeViewModel.isVideoSetup())
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupUI(){
        viewFragment.root.background = homeViewModel.fetchGradient()
        homeViewModel.updateTypeFace().run{
            viewFragment.settingsSectionDials.typeface = this
            viewFragment.settingsSectionSoundLabel.typeface = this
            viewFragment.settingsSectionAudioState.typeface = this
            viewFragment.settingsSectionVideoLabel.typeface = this
            viewFragment.settingsSectionVideoState.typeface = this
            viewFragment.settingsSectionThemeLabel.typeface = this
            viewFragment.settingsSectionThemeState.typeface = this

            viewFragment.settingsSectionAdditional.typeface = this
            viewFragment.settingsSectionAdditionalShow.typeface = this
            viewFragment.settingsSectionServerName.typeface = this
            viewFragment.settingsSectionServerLabel.typeface = this
            viewFragment.settingsSectionBuilding.typeface = this
            viewFragment.settingsSectionVersionLabel.typeface = this
            viewFragment.settingsSectionVersionShow.typeface = this
        }
        viewFragment.settingsSectionVideoSwitch.setChecked(homeViewModel.isVideoSetup())
        viewFragment.settingsSectionVoiceSwitch.setChecked(homeViewModel.isVoiceSetup())
        viewFragment.settingsSectionThemeSwitch.setChecked(homeViewModel.isDarkModeSetup())
        viewFragment.settingsSectionAdditionalSwitch.setChecked(homeViewModel.isAdditionalSetup())

        viewFragment.settingsSectionVersionShow
            .text = """${BuildConfig.VERSION_NAME} ${resources
                .getString(R.string.setupNameBuild)} ${BuildConfig.VERSION_CODE}"""

        if(homeViewModel.isVideoSetup()) {
            viewFragment.settingsSectionVideoState.text = resources
                .getString(R.string.frgSettingsParamsOn)
        } else {
            viewFragment.settingsSectionVideoState.text = resources
                .getString(R.string.frgSettingsParamsOff)
        }

        if(homeViewModel.isVoiceSetup()) {
            viewFragment.settingsSectionAudioState.text = resources
                .getString(R.string.frgSettingsParamsOn)
        } else {
            viewFragment.settingsSectionAudioState.text = resources
                .getString(R.string.frgSettingsParamsOff)
        }

        if(homeViewModel.isDarkModeSetup()) {
            viewFragment.settingsSectionThemeState.text = resources
                .getString(R.string.frgSettingsParamsOn)
        } else {
            viewFragment.settingsSectionThemeState.text = resources
                .getString(R.string.frgSettingsParamsOff)
        }

        if(homeViewModel.isAdditionalSetup()) {
            viewFragment.settingsSectionAdditionalShow.text = resources
                .getString(R.string.frgSettingsParamsOn)
        } else {
            viewFragment.settingsSectionAdditionalShow.text = resources
                .getString(R.string.frgSettingsParamsOff)
        }

    }

    private fun addListener(){
        viewFragment.settingsSectionVideoSwitch.setOnCheckedListener {sw ->
            homeViewModel.inVideoSetup(sw)
            if(sw) {
                viewFragment.settingsSectionVideoState.text = resources
                    .getString(R.string.frgSettingsParamsOn)
            } else {
                viewFragment.settingsSectionVideoState.text = resources
                    .getString(R.string.frgSettingsParamsOff)
            }
        }
        viewFragment.settingsSectionVoiceSwitch.setOnCheckedListener {sw ->
            homeViewModel.inVoiceSetup(sw)
            if(sw) {
                viewFragment.settingsSectionAudioState.text = resources
                    .getString(R.string.frgSettingsParamsOn)
            } else {
                viewFragment.settingsSectionAudioState.text = resources
                    .getString(R.string.frgSettingsParamsOff)
            }
        }
        viewFragment.settingsSectionThemeSwitch.setOnCheckedListener {sw ->
            homeViewModel.inDarkModeSetup(sw)
            if(sw) {
                viewFragment.settingsSectionThemeState.text = resources
                    .getString(R.string.frgSettingsParamsOn)
            } else {
                viewFragment.settingsSectionThemeState.text = resources
                    .getString(R.string.frgSettingsParamsOff)
            }
            ThemeUtils.setApplicationTheme(requireContext().applicationContext, sw)
            // Restart the Activity
            activity?.restart()
        }
        viewFragment.settingsSectionAdditionalSwitch.setOnCheckedListener {sw ->
            homeViewModel.inAdditionalSetup(sw)
            if(sw) {
                viewFragment.settingsSectionAdditionalShow.text = resources
                    .getString(R.string.frgSettingsParamsOn)
                viewFragment.frgSettingAdditionalBoxA.isVisible = true // showScaleWithCenterToOut()
                viewFragment.frgSettingAdditionalBoxB.isVisible = true //.showScaleWithCenterToOut()
            } else {
                viewFragment.settingsSectionAdditionalShow.text = resources
                    .getString(R.string.frgSettingsParamsOff)
                viewFragment.frgSettingAdditionalBoxA.isVisible = false //.hideScaleWithOutToCenter()
                viewFragment.frgSettingAdditionalBoxB.isVisible = false //.hideScaleWithOutToCenter()
            }
        }
    }
}