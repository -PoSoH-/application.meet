package meet.sumra.app.frg.drawer

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.forEachIndexed
import androidx.lifecycle.Observer
import meet.sumra.app.CONST
import meet.sumra.app.R
import meet.sumra.app.databinding.BoxItemReferralLinkItemBinding
import meet.sumra.app.databinding.FrgReferralsBinding
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.ext.ExtAnimation.hideScaleWithOutToCenter
import meet.sumra.app.ext.ExtAnimation.hideUtoD
import meet.sumra.app.ext.ExtAnimation.showDtoU
import meet.sumra.app.ext.ExtAnimation.showScaleWithCenterToOut
import meet.sumra.app.ext.editEmpty
import meet.sumra.app.ext.hideKeyboard
import meet.sumra.app.ext.putString
import meet.sumra.app.frg.FragmentBase
import meet.sumra.app.utils.LogUtil
import meet.sumra.app.vm.BaseViewModel
import meet.sumra.app.vm.vm_home.ViewModelHome
import sdk.net.meet.api.referral.objModelReferralCode
import java.lang.RuntimeException
import javax.inject.Inject
import kotlin.random.Random

class FrgReferrals: FragmentBase<FrgReferralsBinding>() {

    companion object{
        val TAG = FrgReferrals::class.java.simpleName
        fun getInstance(): FragmentBase<*>{
            return FrgReferrals()
        }
    }

    internal lateinit var viewModel: ViewModelHome
    @Inject set

    private var listCodes: MutableList<View>? = null

    private fun setupUI(){
        viewFragment.root.background = viewModel.fetchGradient()
        viewModel.updateTypeFace().run {
            viewFragment.frgReferralToolBarLabel.typeface = this
            viewFragment.frgReferralsMembershipName.typeface = this
            viewFragment.frgReferralsMembershipLabel.typeface = this
            viewFragment.frgReferralsUpgrade.typeface = this
            viewFragment.frgReferralsYouEarningsLabel.typeface = this
            viewFragment.frgReferralsHelpInvited.typeface = this
            viewFragment.frgReferralsHelpBonus.typeface = this
            viewFragment.frgReferralsTotalHelp.typeface = this
            viewFragment.frgReferralsShowInvited.typeface = this
            viewFragment.frgReferralsMultiple.typeface = this
            viewFragment.frgReferralsBonusCurrency.typeface = this
            viewFragment.frgReferralsShowBonus.typeface = this
            viewFragment.frgReferralsEquals.typeface = this
            viewFragment.frgReferralsShowCurrency.typeface = this
            viewFragment.frgReferralsShowTotal.typeface = this
            viewFragment.frgReferralsCalculateLabel.typeface = this
            viewFragment.frgReferralsInfoLabel.typeface = this
            viewFragment.frgReferralCodeLink.typeface = this
            viewFragment.frgReferralsTxtLblCode.typeface = this
            viewFragment.frgReferralInfoLink.typeface = this
            viewFragment.frgReferralsTxtLblLink.typeface = this
            viewFragment.frgReferralsShareSocialLabel.typeface = this
            viewFragment.frgReferralListLinksLabel.typeface = this
        }
    }

    private fun addListener(){
        viewFragment.frgReferralBackButton.setOnClickListener{btn->viewModel.onBackClickListener()}

        viewFragment.frgReferralAddButton.setOnClickListener{ btn->
            viewModel.onShowDialogEditNewCode()
        }

        viewFragment.frgDialogEditedCodeClosed.setOnClickListener { btn->
            viewFragment.frgDialogEditedCode.hideScaleWithOutToCenter()
        }

        viewFragment.frgDialogEditedCodeSubmit.setOnClickListener { btn->
            viewFragment.frgDialogEditedCode.hideScaleWithOutToCenter()
            btn.hideKeyboard()
            var id: String? = null
            if(viewFragment.frgDialogEditDefaultNoteEdit.tag != null) {
                id = viewFragment.frgDialogEditDefaultNoteEdit.tag.toString()
            }
            viewFragment.frgDialogEditDefaultMakeSelect.apply {
                viewModel.onClickButtonSubmit(
                    notes = viewFragment.frgDialogEditDefaultNoteEdit.text.toString(),
                    isDefault = isChecked,
                    id
                )
            }
        }

        viewFragment.frgReferralsUpgradeBtn.setOnClickListener{btn->
            LogUtil.error("REFERRALS", "Button upgrade... pressed...")
        }

        viewFragment.frgReferralsEarningsCalculate.setOnClickListener{btn->
            LogUtil.error("REFERRALS", "Button recalculate earnings... pressed...")
            viewModel.updateCalculateEarning()
        }

        viewFragment.frgReferralsCodeCopyBtn.setOnClickListener{btn->
            LogUtil.error("REFERRALS", "Button copy referral code... pressed...")
            copyToClipBoard(viewFragment.frgReferralsTxtLblCode, R.string.success_text_code_copies)
        }

        viewFragment.frgReferralsLinkCopyBtn.setOnClickListener{btn->
            LogUtil.error("REFERRALS", "Button copy referral link... pressed...")
            copyToClipBoard(viewFragment.frgReferralsTxtLblCode, R.string.success_text_link_copies)
        }

        viewFragment.frgReferralsShareSocialLabel.setOnClickListener{btn->
            LogUtil.error("REFERRALS", "Button button send of social network... pressed...")
            viewModel.onShareSocial(code = viewFragment.frgReferralsTxtLblCode.text.toString(),
                                    link = viewFragment.frgReferralsTxtLblLink.text.toString())
        }

        viewFragment.frgDialogBtnQuestionClosed.setOnClickListener { click ->
            viewFragment.frgDialogQuestionRemove.hideUtoD()
            viewFragment.frgDialogQuestionsShowCode.text = ""
            viewFragment.frgDialogQuestionsShowLink.text = ""
        }
        viewFragment.frgDialogBtnQuestionRemove.setOnClickListener { click ->
            viewFragment.frgDialogQuestionRemove.hideUtoD()
            viewModel.onRemoveReferralCode(viewFragment.frgDialogQuestionRemove.tag.toString())
        }

        viewFragment.frgDialogEditedCode.setOnClickListener { btn-> Unit }
        viewFragment.frgDialogQuestionRemove.setOnClickListener { btn-> Unit }
    }

    private fun updateReferralsCode(data: MutableList<objModelReferralCode.Code>){
        if ( listCodes == null ) { listCodes = mutableListOf() }
        else { listCodes!!.clear() }
        viewFragment.frgReferralsListLinks.removeAllViews()
        data.forEachIndexed { index, code ->
            val itemBox = getItem()
            itemBox.apply {
                itemBox.setOnLongClickListener { long ->
                    viewFragment.frgDialogQuestionsShowCode.text = code.code
                    viewFragment.frgDialogQuestionsShowLink.text = code.referralLink
                    viewFragment.frgDialogQuestionRemove.showDtoU()
                    viewFragment.frgDialogQuestionRemove.tag = code.id
                    return@setOnLongClickListener false
                }

                val layout = ConstraintLayout.LayoutParams(
                    ConstraintLayout.LayoutParams.MATCH_PARENT,
                    resources.getDimension(R.dimen.linksReferralHeight).toInt()
                )
                tag = code.id
                layout.setMargins(0, 4, 0, 4)
                layoutParams = layout
                findViewById<TextView>(R.id.referralsLinkShow      ).text = code.code
                findViewById<TextView>(R.id.linksReferralsCodeCount).text = "${code.id.length - Random.nextInt(1, code.id.length-1)} "

                findViewById<ImageView>(R.id.linksReferralsCodeCopy)      // copy ref code
                findViewById<ImageView>(R.id.linksReferralsAddReferral)   // add user of code

                findViewById<ImageView>(R.id.linksReferralsEditReferral).setOnClickListener { btn ->
                    onUpdateRefPosById(id = tag.toString())
                }  // edit ref code

                if (index == 0) {
                    findViewById<TextView>(R.id.linksReferralsDefault).visibility = View.VISIBLE

                    viewFragment.frgReferralsTxtLblCode.text = code.code
                    viewFragment.frgReferralsTxtLblLink.text = code.referralLink
                }
                viewModel.updateTypeFace().apply {
                    findViewById<TextView>(R.id.referralsLinkShow      ).typeface = this
                    findViewById<TextView>(R.id.linksReferralsDefault  ).typeface = this
                    findViewById<TextView>(R.id.linksReferralsCodeCount).typeface = this
                }

                findViewById<ImageView>(R.id.linksReferralsCodeCopy).setOnClickListener { btn ->
                    copyToClipBoard(
                        findViewById<TextView>(R.id.referralsLinkShow),
                        R.string.success_text_code_copies
                    )
                }
            }
            listCodes?.run{ add(itemBox) }
            viewFragment.frgReferralsListLinks.addView(itemBox)
        }
    }

    private fun onUpdateRefPosById(id: String){ //, note: String, isDefault: String){
        viewModel.showEditCurrentRefCode(id) //, note, isDefault)
    }

    private fun updateDialogLabel(txtHeader: String, txtBase: String){
        viewFragment.frgDialogEditCodeLabel.text = txtHeader
        viewFragment.frgDialogEditDefaultNoteLabel.text = txtBase
    }

    private fun clearEditedDialogField(){
        viewFragment.frgDialogEditCodeLabel.text = ""
        viewFragment.frgDialogEditDefaultNoteLabel.text = ""
        viewFragment.frgDialogEditDefaultNoteEdit.editEmpty()
        viewFragment.frgDialogEditDefaultMakeSelect.isChecked = false
    }

    private fun updateRefCodeAfterUpdate(data: objModelReferralCode.Code){
        viewFragment.frgReferralsListLinks.forEachIndexed { i, v ->
            if (v.getTag(0).equals(data.id)) {
                v.findViewById<TextView>(R.id.referralsLinkShow      ).text = data.code
                v.findViewById<TextView>(R.id.linksReferralsCodeCount).text = data.userID
                if (i == 0) {
                    v.findViewById<TextView>(R.id.linksReferralsDefault).visibility = View.VISIBLE
                }
            }
        }
    }

    private fun getItem(): ConstraintLayout {
        return BoxItemReferralLinkItemBinding.inflate(layoutInflater).root
    }

    private fun copyToClipBoard(view: TextView, resText: Int){
        try {
            val clipboard = requireActivity().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText(CONST.APP_PACKAGE, view.text)
            clipboard.setPrimaryClip(clip)
            viewModel.showSuccessMessage(BaseViewModel.AlertText(title = "",
                texts = resources.getString(resText)))
        } catch (ex: RuntimeException) {
            viewModel.showFailureMessage(BaseViewModel.AlertText(title = "",
                texts = resources.getString(R.string.frgErrorFailureTextCopies)))
        }
    }

    private fun referralsState() = Observer<ViewModelHome.StateReferral> { state ->
        when(state) {
            is ViewModelHome.StateReferral.UpdatesReferralCodes -> {
                LogUtil.error("TAG REFERRALS", "Update Referrals codes...")
                updateReferralsCode(state.data)
            }
            is ViewModelHome.StateReferral.EditedReferralCode  -> {
                viewFragment.frgDialogEditedCode.showScaleWithCenterToOut()
                when(state.isEdited){
                    is ViewModelHome.TypEdCode.CreateCode -> {
                        clearEditedDialogField()
                        updateDialogLabel(txtHeader = resources.getString(R.string.frgRefDialogHeaderCreateLabel),
                                          txtBase   = resources.getString(R.string.frgRefDialogBaseCreateLabel))
                        viewFragment.frgDialogEditDefaultNoteEdit.tag = ""
                    }
                    is ViewModelHome.TypEdCode.EditedCode -> {
                        clearEditedDialogField()
                        updateDialogLabel(txtHeader = resources.getString(R.string.frgRefDialogHeaderEditedLabel),
                                          txtBase   = resources.getString(R.string.frgRefDialogBaseEditedLabel))
                        state.id?.let {
                            viewFragment.frgDialogEditDefaultNoteEdit.tag = it
                        }
                        state.note?.let {
                            viewFragment.frgDialogEditDefaultNoteEdit.putString(it)
                        }
                        state.isDefault?.let {
                            viewFragment.frgDialogEditDefaultMakeSelect.isChecked = it
                        }
                    }
                    else -> Unit
                }
            }
            is ViewModelHome.StateReferral.UpdateEarningsCalc  -> {
                state.apply {
                    when(cur) {
                        "USD" -> { viewFragment.frgReferralsBonusCurrency.text = "$"
                            viewFragment.frgReferralsShowCurrency.text = "$" }
                        else  -> { viewFragment.frgReferralsBonusCurrency.text = "E"
                            viewFragment.frgReferralsShowCurrency.text = "E" }
                    }
                    viewFragment.frgReferralsShowInvited.text = invoited
                    viewFragment.frgReferralsShowBonus.text   = bonus
                    viewFragment.frgReferralsShowTotal.text   = result
                }
            }
            is ViewModelHome.StateReferral.UpdateMemberShip    -> {
                state.apply{
                    viewFragment.frgReferralsMembershipName.text = ""
                    when(state.data.membership.memberShipPlan){
                        0->{viewFragment.frgReferralsCenterImg.setImageResource(R.drawable.ic_referral_medal_platinum)}
                        1->{viewFragment.frgReferralsCenterImg.setImageResource(R.drawable.ic_referral_medal_gold)}
                        2->{viewFragment.frgReferralsCenterImg.setImageResource(R.drawable.ic_referral_medal_silver)}
                        3->{viewFragment.frgReferralsCenterImg.setImageResource(R.drawable.ic_referral_medal_bronze)}
                    }
                }
            }
            is ViewModelHome.StateReferral.UpdateAfterDelete -> {
                viewModel.updateReferralsAfterDelete(state.id)
            }
            else -> Unit
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.initReferral()
        viewModel.stateReferrals.observe(viewLifecycleOwner, referralsState())

        setupUI()
        addListener()
    }

    override fun onStart() {
        super.onStart()
        viewModel.updateDataReferral()
    }

    override fun onStop() {
        super.onStop()
        viewModel.dataReferralClear()
    }

    override fun onBackPressed(): Boolean {
        return true
    }

    override fun getBinding(): FrgReferralsBinding {
        return FrgReferralsBinding.inflate(layoutInflater)
    }

    override fun injectDependency(component: ComponentViewModel) {
        component.inject(this)
    }
}