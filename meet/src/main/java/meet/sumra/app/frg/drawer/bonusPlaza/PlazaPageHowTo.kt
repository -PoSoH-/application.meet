package meet.sumra.app.frg.drawer.bonusPlaza

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import meet.sumra.app.R
import meet.sumra.app.data.mdl.plaza.HowToSection
import meet.sumra.app.data.mdl.plaza.ObjData
import meet.sumra.app.views.compose.ui_theme.StyleText

@Preview
@Composable
fun PageHowToPreview() {
    GetPageHowTo(
        remember {mutableStateOf(
            HowToSection("method_a"
                ,"Modifier 01"
                ,"jksjd sla klsak dj slakdjsa l kd j a ljdkasj dksa jdksja djsa kldjslak jdlsajd lksasjd lasjd lask jdklsajd klasjd lk"
                , reward = ObjData(value = "245", currency = "DR")
                , limit  = ObjData(value = "Unlimited", currency = ""))
        )}
    )
}

@Composable
fun GetPageHowTo(item: MutableState<HowToSection>) {
    val circleSize = 68
    val paintSize = 32
    var imageType = 0
    var circleColor = Color.Transparent
    var textColor   = Color.Transparent

    Card(
        modifier = Modifier
            .padding(12.dp)
//            .fillMaxSize()
        , backgroundColor = Color.White
        , shape =  RoundedCornerShape(12.dp)
    ){
        when(item.value.typeSection){
            "method_a" -> {
                imageType = R.drawable.ic_plaza_daily_bonus
                circleColor = Color(0xffE5F6EF)
                textColor = Color(0xff38CB89)
            }
            "method_b" -> {
                imageType = R.drawable.ic_plaza_commercial_shield
                circleColor = Color(0xffFFFBF1)
                textColor = Color(0xff38CB89)
            }
            "method_c" -> {
                imageType = R.drawable.ic_plaza_sumra_id
                circleColor = Color(0xffF4F8FF)
                textColor = Color(0xff38CB89)
            }
            "method_d" -> {
                imageType = R.drawable.ic_plaza_services_rewiews
                circleColor = Color(0xffFFF6F4)
                textColor = Color(0xff38CB89)}
            "method_e" -> {
                imageType = R.drawable.ic_plaza_token_share
                circleColor = Color(0xffE5F6EF)
                textColor = Color(0xff38CB89)
            }
            "method_f" -> {
                imageType = R.drawable.ic_plaza_token_chart_arrow
                circleColor = Color(0xffFDF4E0)
                textColor = Color(0xff38CB89)
            }
            "method_g" -> {
                imageType = R.drawable.ic_plaza_how_to_surveys_our
                circleColor = Color(0xffEBF2FF)
                textColor = Color(0xff38CB89)
            }
            "method_i" -> {
                imageType = R.drawable.ic_plaza_how_to_review_link
                circleColor = Color(0xffFFF6F4)
                textColor = Color(0xff38CB89)
            }
            else       -> {
                imageType = R.drawable.ic_plaza_commercial_shield
                circleColor = Color(0xffE5F6EF)
                textColor = Color(0xff38CB89)
            }
        }

        Row(){
            Column(
                modifier = Modifier.padding(8.dp)
            ) {
                Box(
                    modifier = Modifier
                        .width(circleSize.dp)
                        .height(circleSize.dp)
                    , contentAlignment = Alignment.Center
                ) {
                    CircleIndicator(size = circleSize.dp, color = circleColor)
                    Image(
                        modifier = Modifier
                            .width(paintSize.dp)
                            .height(paintSize.dp),
                        painter = painterResource(id = imageType),
                        contentDescription = null
                    )
                }
            }
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        start = 4.dp, top = 8.dp, end = 8.dp, bottom = 8.dp
                    )
            ) {
                Text(
                    text = item.value.label, style = StyleText.txtCrdLbl()
                )
                Text(
                    text = item.value.message, style = StyleText.txtCrdTxt()
                )

//                Column(
//                    modifier = Modifier
//                        .fillMaxWidth()
//                ){
                    Surface(
                        modifier = Modifier
//                            .background(Color(0xFFF8F9FA), RoundedCornerShape(5.dp))
                            .fillMaxWidth()
                            .padding(4.dp),
                        color = Color(0xFFF8F9FA),
                        shape = RoundedCornerShape(5.dp)
                    ) {
                        Row(
                            modifier = Modifier
                                .background(color = Color.Transparent)
                                .padding(vertical = 5.dp, horizontal = 8.dp)
                        ) {
                            Column(
                                modifier = Modifier.background(color = Color.Transparent)
                            ) {
                                Row(
                                    modifier = Modifier.background(color = Color.Transparent)
                                ) {
                                    Text(
                                        text = stringResource(R.string.frgPlazaReward),
                                        style = StyleText.txtCrdLim()
                                    )
                                    Text(
                                        modifier = Modifier.padding(start = 8.dp),
                                        text = "+ ${item.value.reward.getData()}",
                                        style = StyleText.txtCrdCur(textColor)
                                    )
                                }
                            }
                            Column(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .background(color = Color.Transparent),
                                horizontalAlignment = Alignment.End
                            ) {
                                Row(
                                    modifier = Modifier.background(color = Color.Transparent)
                                ) {
                                    Text(
                                        text = stringResource(R.string.frgPlazaLimit),
                                        style = StyleText.txtCrdLim()
                                    )
                                    Text(
                                        modifier = Modifier.padding(start = 8.dp),
                                        text = item.value.limit.getData(),
                                        style = StyleText.txtCrdUlt()
                                    )
                                }
                            }
                        }
                    }
//                }
            }
        }
    }
}

@Composable
fun CircleIndicator(size: Dp, color: Color){
    Canvas(modifier = Modifier.size(size), onDraw = {
        drawCircle(color = color)
    })
}

//fun updateType(type: String){
//
//}