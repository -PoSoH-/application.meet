package meet.sumra.app.frg.drawer

import android.app.Activity
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import chat.sumra.lib.multipicker.MultiPicker
import chat.sumra.lib.multipicker.entity.MultiPickerImageType
import com.yalantis.ucrop.UCrop
import meet.sumra.app.R
import meet.sumra.app.databinding.FrgProfileBinding
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.ext.ExtAnimation.hideDtoU
import meet.sumra.app.ext.ExtAnimation.hideUtoD
import meet.sumra.app.ext.ExtAnimation.showDtoU
import meet.sumra.app.ext.ExtAnimation.showUtoD
import meet.sumra.app.ext.createUCropWithDefaultSettings
import meet.sumra.app.ext.insertBeforeLast
import meet.sumra.app.ext.loadUrl
import meet.sumra.app.ext.registerStartForActivityResult
import meet.sumra.app.frg.FragmentBase
import meet.sumra.app.interfaces.UrlListener
import meet.sumra.app.utils.PERMISSIONS_FOR_TAKING_PHOTO
import meet.sumra.app.utils.checkPermissions
import meet.sumra.app.utils.registerForPermissionsResult
import meet.sumra.app.vm.vm_home.ViewModelHome
import java.io.File
import javax.inject.Inject

class FrgProfile: FragmentBase<FrgProfileBinding>() {

    companion object{
        val TAG = FrgProfile::class.java.simpleName
        fun getInstance(): FragmentBase<*>{
            return FrgProfile()
        }
    }

    private var avatarCameraUri: Uri? = null

    internal lateinit var viewModel: ViewModelHome
    @Inject set

    private fun setupUI() {
        viewFragment.root.background = viewModel.fetchGradient()
        viewFragment.profileToolbarContainer.background = viewModel.fetchGradient()
        viewModel.updateTypeFace().run {
            viewFragment.meetProfileLabel.typeface = this
            viewFragment.settingsSectionDisplayName.typeface = this
            viewFragment.settingsSectionFirstEnter.typeface = this
            viewFragment.settingsSectionEmail.typeface = this
            viewFragment.settingsSectionLastEnter.typeface = this
        }
    }

    private fun addListener(){
        viewFragment.frgProfileBackButton.setOnClickListener {
            viewModel.onBackClickListener()}
        viewFragment.frgProfileEditButton.setOnClickListener {
            if(viewFragment.frgProfileEditView.isShown){
                viewFragment.frgProfileEditView.hideUtoD()
            }else {
                viewFragment.frgProfileEditView.showDtoU()
            }
        }

        viewFragment.frgProfileSubmitContact.setOnClickListener {
            // listener for send new userprofile data
            validationAndSend()
        }
        viewFragment.frgProfileCloseContact.setOnClickListener {
            // listener for close userprofile data
            viewFragment.frgProfileEditView.hideUtoD()
        }
        viewFragment.frgProfileUserAvatarBox.setOnClickListener {
            // listener for show menu for add new user photo
            viewFragment.frgProfileShowTakeOrCheckPhoto.showDtoU()
        }

        viewFragment.frgProfileSelectPhotoBtn.setOnClickListener {
            // listener for cancel dialog menu choice photo
            onAvatarTypeSelected(Type.Gallery)
        }
        viewFragment.frgProfileTakePhotoBtn.setOnClickListener {
            // listener for take photo
            onAvatarTypeSelected(Type.Camera)
        }
        viewFragment.frgProfileCancelPhotoBtn.setOnClickListener {
            // listener for select photo
            viewFragment.frgProfileShowTakeOrCheckPhoto.hideUtoD()
        }
    }

    private fun stateProfile() = Observer<ViewModelHome.StatePrfl> { prfl ->
        when(prfl){
            is ViewModelHome.StatePrfl.Default -> {

            }
            is ViewModelHome.StatePrfl.Success -> {
                viewFragment.profileUserAvatar.loadUrl(prfl.dt.avatar)
                viewFragment.settingsSectionFirstEnter.text = prfl.dt.displayName
//                viewFragment.settingsSectionLastEnter.text  = prfl.dt.lastName
            }
        }
    }

    private fun validationAndSend(){

    }

    private fun clearAllField(){

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.initProfile()
        viewModel.viewStatePrfl.observe(viewLifecycleOwner, stateProfile())
        setupUI()
        addListener()
    }

    override fun onBackPressed(): Boolean {
        return true
    }

    override fun getBinding(): FrgProfileBinding {
        return FrgProfileBinding.inflate(layoutInflater)
    }

    override fun injectDependency(component: ComponentViewModel) {
        component.inject(this)
    }


    private enum class Type {
        Camera,
        Gallery
    }

    private fun onAvatarTypeSelected(type: Type) {
        when (type) {
            Type.Camera ->
                if (checkPermissions(
                        PERMISSIONS_FOR_TAKING_PHOTO,
                        requireActivity(),
                        takePhotoPermissionActivityResultLauncher)
                ) {
                    doOpenCamera()
                }
            Type.Gallery ->
                MultiPicker.get(MultiPicker.IMAGE).single().startWith(pickImageActivityResultLauncher)
        }
    }

    private val takePhotoPermissionActivityResultLauncher = registerForPermissionsResult { allGranted ->
        if (allGranted) {
            doOpenCamera()
        }
    }

    private val takePhotoActivityResultLauncher = registerStartForActivityResult { activityResult ->
        if (activityResult.resultCode == Activity.RESULT_OK) {
            avatarCameraUri?.let { uri ->
                MultiPicker.get(MultiPicker.CAMERA)
                    .getTakenPhoto(requireActivity(), uri)
                    ?.let { startUCrop(it) }
            }
        }
    }

    private val pickImageActivityResultLauncher = registerStartForActivityResult { activityResult ->
        if (activityResult.resultCode == Activity.RESULT_OK) {
            MultiPicker
                .get(MultiPicker.IMAGE)
                .getSelectedFiles(requireActivity(), activityResult.data)
                .firstOrNull()
                ?.let { startUCrop(it) }
        }
    }

    private val uCropActivityResultLauncher = registerStartForActivityResult { activityResult ->
        if (activityResult.resultCode == Activity.RESULT_OK) {
            activityResult.data?.let {
                viewModel.urlTempAvatar = UCrop.getOutput(it)
                viewFragment.frgProfileUserAvatar.loadUrl(viewModel.urlTempAvatar.toString())
                viewFragment.profileUserAvatar.loadUrl(viewModel.urlTempAvatar.toString())
                viewFragment.frgProfileShowTakeOrCheckPhoto.hideUtoD()
            }
        }
    }

    private fun startUCrop(image: MultiPickerImageType) {
        val destinationFile = File (
            requireActivity().cacheDir,
            image.displayName.insertBeforeLast("_e_${System.currentTimeMillis()}")
        )
        val uri = image.contentUri

        createUCropWithDefaultSettings(context = requireContext(), uri, destinationFile.toUri(), getString(
            R.string.rotate_and_crop_screen_title))
            .withAspectRatio(1f, 1f)
            .getIntent(requireActivity())
            .let {
                uCropActivityResultLauncher.launch(it)
            }
    }

    private fun doOpenCamera() {
        avatarCameraUri = MultiPicker.get(MultiPicker.CAMERA).startWithExpectingFile(requireActivity(), takePhotoActivityResultLauncher)
    }

}