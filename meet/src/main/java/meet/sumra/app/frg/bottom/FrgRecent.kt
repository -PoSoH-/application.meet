package meet.sumra.app.frg.bottom

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.ComposeCompilerApi
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Observer
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.PagerState
import com.google.accompanist.pager.rememberPagerState
import meet.sumra.app.R
import meet.sumra.app.act.ActivityHome
import meet.sumra.app.data.mdl.Banner
import meet.sumra.app.data.mdl.DtMeetHistory
import meet.sumra.app.databinding.FrgRecentBinding
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.ext.hideKeyboard
import meet.sumra.app.frg.FragmentBase
import meet.sumra.app.utils.LogUtil
import meet.sumra.app.views.compose.ui_theme.StyleText
import meet.sumra.app.views.compose.ui_theme.ThemeSetup
import meet.sumra.app.vm.BaseViewModel
import meet.sumra.app.vm.RecentHome
import meet.sumra.app.vm.vm_home.ViewModelHome
import org.jitsi.meet.sdk.*
import xyn.meet.storange.models.HistoryEntity
import java.lang.ref.WeakReference
import javax.inject.Inject

class FrgRecent: FragmentBase<FrgRecentBinding>() {

    private lateinit var activity: WeakReference<ActivityHome>

    private val viewStateBanner = mutableStateListOf<Banner>()

    internal lateinit var viewModel: ViewModelHome
    @Inject set

    companion object{
        val TAG = FrgRecent::class.java.simpleName
        fun getInstance(): FrgRecent{
            return FrgRecent()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = WeakReference(requireActivity() as ActivityHome)
    }

    @ExperimentalPagerApi
    @ExperimentalMaterialApi
    @ExperimentalFoundationApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.initRecent()
        viewModel.stateRecent.observe(viewLifecycleOwner, stateRecent())
        setupUI()
        addListener()
    }

    override fun onDetach() {
        super.onDetach()
        activity.clear()
    }

    override fun onBackPressed(): Boolean {
        return true
    }

    @ExperimentalPagerApi
    @ExperimentalMaterialApi
    private fun setupUI() {
        viewModel.updateTypeFace().run {
            viewFragment.frgRecentTextHeader.typeface = this
            viewFragment.frgRecentRoomName.typeface = this
            viewFragment.frgRecentConversation.typeface = this
            viewFragment.frgRecentButtonName.typeface = this
            viewFragment.frgRecentButtonJoinMeetName.typeface = this
        }
        viewFragment.frgRecentTextHeader.isFocusable = true
        viewFragment.frgRecentTextHeader.requestFocus()
        viewFragment.frgRecentBannerBox.setContent { BannerShow() }
    }

    private fun addListener(){
//        viewFragment.frgRecentButton.setOnClickListener{btn -> viewModel.onBackClickListener()}
        viewFragment.frgRecentCreateMeet.setOnClickListener {btn->
            viewModel.showDialogCreateMeet()
//            startValidyParams()
        }
        viewFragment.frgRecentJoinMeet.setOnClickListener {btn->
            viewModel.showDialogJoinMeet()
        }
    }

/**
    private fun startValidyParams(){
        viewFragment.frgRecentRoomName.hideKeyboard()
        when(viewFragment.frgRecentRoomName.text?.length){
            in 6..100 -> {
//                viewModel.switchToInvite(viewFragment.frgRecentRoomName.text.toString())
            }
            else -> {
                val t = StringBuilder()
                if(viewFragment.frgRecentRoomName.text.isNullOrEmpty()){
                    t.append(resources.getString(R.string.recentNewRoomTitleFailZeroText))
                }else if(viewFragment.frgRecentRoomName.text?.length!! < 6){
                    t.append(resources.getString(R.string.recentNewRoomTitleFailMinSizeText))
                }else if(viewFragment.frgRecentRoomName.text?.length!! > 100) {
                    t.append(resources.getString(R.string.recentNewRoomTitleFailMaxSizeText))
                }
                viewModel.updateMessageSate(
                    BaseViewModel.MessageState.MessageWarning(
                        content = BaseViewModel.AlertText(
                            title = resources.getString(R.string.recentNewRoomTitleFail),
                            texts = t.toString())))
            }
        }
    }
**/

    @ExperimentalFoundationApi
    private fun stateRecent() = Observer<RecentHome> { state ->
        when(state) {
            is RecentHome.Default -> {
                viewModel.initBanner()
                viewModel.initHistory()
            }
            is RecentHome.BannerGet -> {
                viewStateBanner.clear()
                viewStateBanner.addAll(state.value)
            }
            is RecentHome.HistoryGet -> {
                historyList.clear()
                historyList.addAll(state.value)
                viewFragment.recentListHistoryView
                    .setContent { ShowListHistory() }
            }
        }
    }

/**
    private fun hideSetupElement(){
        viewFragment.meetSetupContainer.run{
            visibility = View.GONE
            hideKeyboard()
        }
        viewFragment.frgConferency.visibility = View.VISIBLE
    }
**/

    private fun showDialogDetails(dBoxID: Int, dShow: Boolean) {
        LogUtil.info(tag = "dialog call...", txt = "dialog id show ... ${dBoxID}")
        viewModel.switchBannerDialogue(dBoxID)
    }

    override fun getBinding(): FrgRecentBinding {
        return FrgRecentBinding.inflate(layoutInflater)
    }

    override fun injectDependency(component: ComponentViewModel) {
        component.inject(this@FrgRecent)
    }

    /***   ***   ***   ***   ***   ***   ***    |||    ***   ***   ***   ***   ***   ***   ***/

    @Composable
    @ExperimentalPagerApi
    @ExperimentalMaterialApi
    fun BannerShow(){

        Column(
            modifier = Modifier
                .fillMaxWidth(1f),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            val pagerState = PagerState(
                pageCount = viewStateBanner.size,
                currentPage = 0,
                currentPageOffset = 0f,
                infiniteLoop = true
            )

            HorizontalPager(
                state = pagerState,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(80.dp)
            ) { page ->
                // Our content for each page
                BannerItem(
                    title = viewStateBanner[page].title,
                    article = viewStateBanner[page].article,
                    imageRes = viewStateBanner[page].sectionIcon,
                    buttonName = viewStateBanner[page].buttonName,
                    buttonColor = viewStateBanner[page].buttonColor,
                    modifier = Modifier.fillMaxWidth(1f)
                )
            }

            HorizontalPagerIndicator(
                pagerState = pagerState,
                modifier = Modifier
                    .padding(top = 16.dp)
                    .height(20.dp),
            )
        }
    }

    @Composable
    @ExperimentalPagerApi
    @ExperimentalMaterialApi
    fun HorizontalPagerIndicator(
        pagerState: PagerState,
        modifier: Modifier = Modifier,
        activeColor: Color = Color(0xff377DFF), // LocalContentColor.current.copy(alpha = LocalContentAlpha.current),
        inactiveColor: Color = Color(0xffF4F4F4), // activeColor.copy(ContentAlpha.disabled),
        indicatorWidth: Dp = 8.dp,
        indicatorHeight: Dp = indicatorWidth,
        spacing: Dp = indicatorWidth,
        indicatorShape: Shape = CircleShape,
    ) {

        val indicatorWidthPx = LocalDensity.current.run { indicatorWidth.roundToPx() }
        val spacingPx = LocalDensity.current.run { spacing.roundToPx() }

        Box(
            modifier = modifier,
            contentAlignment = Alignment.CenterStart
        ) {
            Row(
                horizontalArrangement = Arrangement.spacedBy(spacing),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                val indicatorModifier = Modifier
                    .size(width = indicatorWidth, height = indicatorHeight)
                    .background(color = inactiveColor, shape = indicatorShape)

                repeat(pagerState.pageCount) {
                    Box(indicatorModifier)
                }
            }

            if(viewStateBanner.size >= 1) {
                Box(
                    Modifier
                        .offset {
                            val scrollPosition =
                                (pagerState.currentPage + pagerState.currentPageOffset)
                                    .coerceIn(
                                        0f,
                                        (pagerState.pageCount - 1)
                                            .coerceAtLeast(0)
                                            .toFloat()
                                    )
                            IntOffset(
                                x = ((spacingPx + indicatorWidthPx) * scrollPosition).toInt(),
                                y = 0
                            )
                        }
                        .size(width = indicatorWidth, height = indicatorHeight)
                        .background(
                            color = activeColor,
                            shape = indicatorShape,
                        )
                )
            }
        }
    }


    @Composable
    @ExperimentalMaterialApi
    fun BannerItem(
          title: String
        , article: String
        , imageRes: String
        , buttonName: String
        , buttonColor: String
        , modifier: Modifier
    ){
        Surface(
            modifier = modifier
            , color = Color(0xffFFF6F4)
            , shape = RoundedCornerShape(10.dp)
        ) {
            Row (modifier = Modifier
                .fillMaxWidth()
            ) {
                val tmp = when(imageRes){
                    "referral"  -> {R.drawable.ic_banner_referral}
                    "rewards"   -> {R.drawable.ic_banner_rewards}
                    "free_plan" -> {R.drawable.ic_banner_free_plan}
                    else -> {R.drawable.ic_meet_nit}
                }
                val col = when(buttonColor){
                    "referral"  -> {Color(0xffF16868)}
                    "rewards"   -> {Color(0xff38CB89)}
                    "free_plan" -> {Color(0xffFFAB00)}
                    else -> {Color.Transparent}
                }
                Image(
                    painter = painterResource(id = tmp),
                    contentDescription = "...show banner image...",
                    modifier = Modifier
                        .fillMaxHeight()
                        .padding(start = 20.dp, top = 0.dp, bottom = 0.dp, end = 5.dp),
                    alignment = Alignment.Center
                )
                Row(
                    modifier = Modifier
                        .padding(all = 5.dp)
                        .fillMaxSize()
//                        .background(color = Color.Green)
                    , verticalAlignment = Alignment.CenterVertically
                    , horizontalArrangement = Arrangement.SpaceEvenly
                ) {

                    Column(
                          modifier = Modifier
                              .fillMaxHeight(1.00f)
                              .fillMaxWidth(0.60f)
                        , verticalArrangement = Arrangement.Center

                    ) {
                        Text(
                            text = title,
                            style = StyleText.txtWaitBBRL()
                        )
                        Text(
                              text = article
                            , style = StyleText.txtWaitBBRV()
                            , maxLines = 2
                            , overflow = TextOverflow.Ellipsis
                        )
                    }
                    Column(
                        modifier = Modifier
                            .fillMaxHeight(1f)
                            .fillMaxWidth(1f)
                            .padding(start = 5.dp, top = 5.dp, bottom = 5.dp, end = 20.dp)
                        , verticalArrangement = Arrangement.Center
                        , horizontalAlignment = Alignment.End
                    ) {
                        Surface(
                            onClick = { showDialogDetails(8794648, true) }
                            , modifier = Modifier
                                .height(ThemeSetup.bbd.bBH)
                                .width(ThemeSetup.bbd.bBW)
                            , color = col // ThemeSetup.bbd.bBBC
                            , shape = RoundedCornerShape(ThemeSetup.bbd.bBBR)
                        ) {
                            Column(
                                modifier = Modifier
                                    .fillMaxSize()
                                , verticalArrangement = Arrangement.Center
                                , horizontalAlignment = Alignment.CenterHorizontally
                            ) {
                                Text(
                                    text = buttonName,  //  stringResource(id = R.string.bannerReferral),
                                    style = StyleText.txtWaitBBT()
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    private val historyList = mutableStateListOf<DtMeetHistory>()

    @ExperimentalFoundationApi
    @Composable
    fun ShowListHistory(){
        Box(modifier = Modifier.fillMaxSize()
        ) {
            val stateList = rememberLazyListState()
            LazyColumn(
                state = stateList,
                modifier = Modifier
                    .fillMaxSize(1f)
            ) {
                val grTime = historyList.groupBy { it.meetDate }
                grTime.forEach { (it, history) ->
                    stickyHeader {
                        TimePeriodHeader(it)
                    }
                    itemsIndexed(history) {position, participant ->
                        Column(
                            modifier = Modifier
                                .height(48.dp)
                        ) {
                            ItemHistory(participant)
                        }
                    }
//                    Divider(modifier = Modifier
//                        .height(1.dp),
//                        color = Color(0xffffffff)
//                    )
                }
            }
        }
    }

    @Composable
    fun ItemHistory(entity: DtMeetHistory){
        Box(
            modifier = Modifier.fillMaxSize().height(40.dp)
        ) {
            Column(modifier = Modifier.fillMaxSize()) {
                Text( text = entity.meetName
                    , style = StyleText.txtListName())

                Row() {
                    Text(
                          text = entity.meetDate
                        , style = StyleText.txtListDate())
                    Text(
                          text = ","
                        , style = StyleText.txtListDate())
                    Text(
                          text = entity.meetStartTime
                        , style = StyleText.txtListDate())
                    Text(
                          text = "-"
                        , style = StyleText.txtListDate())
                    Text(
                          text = entity.meetFinalTime
                        , style = StyleText.txtListDate())
                }
            }
        }
    }

    private val txtHeadSize = 20

    @Composable
    fun TimePeriodHeader(period: String){
        Surface(
              modifier = Modifier.height(txtHeadSize.dp)
//                  .padding(horizontal = 10.dp, vertical = 2.dp)
            , color = Color(0xBFDBEDFF)
            , shape = RoundedCornerShape((txtHeadSize/2).dp)
        ) {
            Box(
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = period,
                    style = StyleText.txtListHead(),
                    modifier = Modifier.padding(horizontal = 10.dp, vertical = 2.dp)
                )
            }
        }
    }

    @Preview
    @Composable
    @ExperimentalPagerApi
    @ExperimentalMaterialApi
    @ExperimentalFoundationApi
    fun PreviewBanner(){
        BannerShow()
        ShowListHistory()
    }
}
