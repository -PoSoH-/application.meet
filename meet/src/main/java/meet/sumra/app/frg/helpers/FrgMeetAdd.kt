package meet.sumra.app.frg.helpers

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.accompanist.pager.ExperimentalPagerApi
import meet.sumra.app.R
import meet.sumra.app.conf.MeetConfig
import meet.sumra.app.databinding.FrgMeetAddBinding
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.ext.editEmpty
import meet.sumra.app.frg.FragmentBase
import meet.sumra.app.vm.BaseViewModel
import meet.sumra.app.vm.vm_home.ViewModelHome
import sdk.net.meet.api.meet_calendar.response.MeetCalendarAttr
import sdk.net.meet.api.meet_calendar.response.MeetStream
import java.util.*
import javax.inject.Inject

class FrgMeetAdd: FragmentBase<FrgMeetAddBinding>() {

    companion object{
        val TAG = FrgMeetAdd::class.java.simpleName
        fun getInstance(): FragmentBase<*>{
            return FrgMeetAdd()
        }
    }

    internal lateinit var viewModel: ViewModelHome
    @Inject set

    @ExperimentalPagerApi
    private fun setupUI(){
        viewFragment.root.background = viewModel.fetchGradient()
        viewModel.updateTypeFace().run {
            viewFragment.frgContactMeetBar.typeface = this
            viewFragment.frgAddMeetLabelTitle.typeface = this
            viewFragment.frgAddMeetEditTitle.typeface = this
            viewFragment.frgAddMeetLabelParticipants.typeface = this
            viewFragment.frgAddMeetParticipantsLabel.typeface = this
            viewFragment.frgAddMeetEmptyListLabel.typeface = this
            viewFragment.frgAddMeetLabelLobby.typeface = this
            viewFragment.frgAddMeetLabelDescription.typeface = this
            viewFragment.frgAddMeetLabelPassword.typeface = this
            viewFragment.frgAddMeetEnterPassword.typeface = this
            viewFragment.frgAddMeetLabelSelectDateTime.typeface = this
            viewFragment.frgAddMeetLabelTranslation.typeface = this
            viewFragment.frgAddMeetLabelKey.typeface = this
            viewFragment.frgAddMeetEditTranslationKey.typeface = this
            viewFragment.frgAddMeetLabelNote.typeface = this
            viewFragment.frgAddMeetEnterNote.typeface = this
        }
        /** recycler lists of participants **/
        viewFragment.frgAddMeetShowListCandidate.apply {
            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false)
            adapter = viewModel.getParticipantAdapter()
        }
        val dV = DividerItemDecoration(requireContext(), RecyclerView.HORIZONTAL)
        dV.setDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.divider_new_meet_participant)!!)
        viewFragment.frgAddMeetParticipants.apply {
            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.HORIZONTAL,
                true)
            adapter = viewModel.getInviteParticipantAdapter()
            addItemDecoration(dV)
        }
        viewFragment.frgAddMeetShowYear.setContent{viewModel.fetchTestShowYAdptr()}
        viewFragment.frgAddMeetSelectMonth.setContent{viewModel.fetchTestShowMAdptr()}
        viewFragment.frgAddMeetSelectDay.setContent{viewModel.fetchTestShowDAdptr()}
        viewFragment.frgAddMeetSelectHour.setContent { viewModel.fetchTestShowHourAdptr() }
        viewFragment.frgAppMeetSelectMin.setContent { viewModel.fetchTestShowMinAdptr() }

        viewFragment.frgAddMeetSelectDurationHour.setContent{viewModel.fetchTestShowTermHourAdptr()}
        viewFragment.frgAppMeetSelectDurationMinutes.setContent{viewModel.fetchTestShowTermMinAdptr()}
    }

    private fun addListener(){
        viewFragment.meetCalendarShowParticipants.setOnClickListener {
            viewFragment.calendarNewMeetAddParticipantsBox.isVisible = !viewFragment
                .calendarNewMeetAddParticipantsBox.isVisible
            if(viewFragment.calendarNewMeetAddParticipantsBox.isVisible) {
                if(viewModel.getParticipantAdapter().itemCount == 0) {
                    viewModel.fetchParticipants()
                }
            }
        }
        viewFragment.frgAppMeetAdd.setOnClickListener {
            viewFragment.calendarNewMeetAddParticipantsBox.isVisible = !viewFragment
                .calendarNewMeetAddParticipantsBox.isVisible
        }
        viewFragment.frgAppMeetClose.setOnClickListener {
            viewFragment.calendarNewMeetAddParticipantsBox.isVisible = !viewFragment
                .calendarNewMeetAddParticipantsBox.isVisible
            viewModel.participantClear()
        }
        viewFragment.frgAddMeetSelectLobby.setOnCheckedListener { state ->

        }
        viewFragment.frgAddMeetShowHidePassword.setOnClickListener {
            if(!viewModel.isNewMeetPassShow[0]){
                showPassword()
            }else{
                hidePassword()
            }
        }
        viewFragment.frgAddMeetEnablePassword.setOnCheckedListener { state ->
            viewFragment.frgAddMeetPassword.isVisible = state
        }
        viewFragment.frgAddMeetEnableTranslation.setOnCheckedListener { state ->
            viewFragment.frgAddMeetContainerEnterKey.isVisible = state
        }
        viewFragment.frgContactSubmitContact.setOnClickListener{
            if(checkValidationField()) {
                viewModel.createNewMeetParticipants(preparingDataForSend())
            }
        }
        viewFragment.frgContactCloseContact.setOnClickListener{
            clearAllView()
            viewModel.onBackClickListener()
        }
    }

    private fun showPassword(){
        viewFragment.frgAddMeetEnterPassword
            .setTransformationMethod(HideReturnsTransformationMethod())
        viewModel.isNewMeetPassShow.clear()
        viewModel.isNewMeetPassShow.add(true)
    }

    private fun hidePassword(){
        viewFragment.frgAddMeetEnterPassword
            .setTransformationMethod(PasswordTransformationMethod())
        viewModel.isNewMeetPassShow.clear()
        viewModel.isNewMeetPassShow.add(false)
    }

    private fun stateMeetAdd() = Observer<ViewModelHome.StateMeetAdd> { newMeet ->
        when(newMeet){
            is ViewModelHome.StateMeetAdd.MeetAddDft -> {
                //update ap-mp installation
                viewFragment.frgAddMeetAmSwitchPm.setChecked(newMeet.isStatePM)
            }
            is ViewModelHome.StateMeetAdd.MeetAddPst -> {
                clearAllView()
                onBackPressed()
            }
            is ViewModelHome.StateMeetAdd.MeetParticipantCount -> {
                if(newMeet.c > 0) {
                    if(viewFragment.frgAddMeetEmptyListLabel.isVisible) {
                        viewFragment.frgAddMeetEmptyListLabel.isVisible = false
                    }
                }else{
                    viewFragment.frgAddMeetEmptyListLabel.isVisible = true
                }
            }
        }
    }

    private fun clearAllView(){
        viewFragment.frgAddMeetEditTitle.editEmpty()
        viewFragment.frgAddMeetEnterPassword.editEmpty()
        viewFragment.frgAddMeetEditTranslationKey.editEmpty()
        viewFragment.frgAddMeetEnterNote.editEmpty()
        hidePassword()
    }

    private fun preparingDataForSend(): MeetCalendarAttr {
        return MeetCalendarAttr(
            id = UUID.randomUUID().toString(),
            title = viewFragment.frgAddMeetEditTitle.text.toString(),
            note =  viewFragment.frgAddMeetEnterNote.text.toString(),
            startDate = viewModel.fetchStartDateTime(),
            finalDate = viewModel.fetchFinalDateTime(),
            password = viewFragment.frgAddMeetEnterPassword.text.toString(),
            ownerId = viewModel.fetchOwnerId(),
            participants = viewModel.fetchMeetParticipants(),
            liveStream = MeetStream(streamKey = null, streamLink = null),
            updated_at = viewModel.fetchCreateDateTime(),
            created_at = viewModel.fetchCreateDateTime(),
            isLobby = viewFragment.frgAddMeetSelectLobby.isChecked()
        )
    }

    private fun checkValidationField(): Boolean{
        return if(viewFragment.frgAddMeetEditTitle.text.isNullOrEmpty()){
            viewModel.updateMessageSate(BaseViewModel.MessageState.MessageWarning(
                content = BaseViewModel.AlertText(
                    title = resources.getString(R.string.addNewMeetTitleFail),
                    texts = resources.getString(R.string.addNewMeetTitleFailText))))
            false
        }else if(viewModel.fetchMeetParticipants().isNullOrEmpty()){
            viewModel.updateMessageSate(BaseViewModel.MessageState.MessageWarning(
                content = BaseViewModel.AlertText(
                    title = resources.getString(R.string.addNewMeetParticipantTitle),
                    texts = resources.getString(R.string.addNewMeetParticipantTitleText))))
            false
        } else{
            true
        }
    }

    @ExperimentalPagerApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.initMeetAdd()
        viewModel.viewMeetAdd.observe(viewLifecycleOwner, stateMeetAdd())
        setupUI()
        addListener()
    }

    override fun onBackPressed(): Boolean {
        return true
    }

    override fun getBinding(): FrgMeetAddBinding {
        return FrgMeetAddBinding.inflate(layoutInflater)
    }

    override fun injectDependency(component: ComponentViewModel) {
        component.inject(this)
    }

    override fun getEnterAnimation() = R.anim.anim_show_c_to_o
    override fun getExitAnimation()  = R.anim.anim_hide_o_to_c

}