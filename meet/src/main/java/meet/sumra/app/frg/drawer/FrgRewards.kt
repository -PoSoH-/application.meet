package meet.sumra.app.frg.drawer

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import meet.sumra.app.CONST
import meet.sumra.app.R
import meet.sumra.app.databinding.FrgRewardsBinding
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.frg.FragmentBase
import meet.sumra.app.vm.BaseViewModel
import meet.sumra.app.vm.vm_home.ViewModelHome
import java.lang.RuntimeException
import javax.inject.Inject
import android.widget.Toast
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IFillFormatter
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.utils.ColorTemplate

import com.hadiidbouk.charts.BarData
import com.hadiidbouk.charts.OnBarClickedListener
import meet.sumra.app.ext.ExtAnimation.hideScaleBottomLTopR
import meet.sumra.app.ext.ExtAnimation.showScaleTopRBottomL
import meet.sumra.app.utils.charts.RewardBarConfig
import meet.sumra.app.utils.charts.RewardLinearConfig
import meet.sumra.app.utils.charts.RewardPieConfig

class FrgRewards: FragmentBase<FrgRewardsBinding>() {

    companion object{
        val TAG = FrgRewards::class.java.simpleName
        fun getInstance(): FragmentBase<*>{
            return FrgRewards()
        }
    }

    internal lateinit var viewModel: ViewModelHome
    @Inject set

    private var listCodes: MutableList<View>? = null

    private fun setupUI(){
        viewFragment.frgRewardsMenuLinearList.apply {
            adapter = viewModel.getChartLineAdapter()
            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false)
        }
        viewFragment.frgRewardsMenuOtherList.apply {
            adapter = viewModel.getChartOtherAdapter()
            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false)
        }

//        viewFragment.frgRewardsVerticalBar.animation.duration = 500
//        viewFragment.frgRewardsVerticalBar.animate(barSet)

        setupLinearChart()
        setupBarChart()
        setupPieChart()

        viewFragment.frgRewardsVerticalBar.let { chart ->
            val dataList = generate()
            chart.setDataList(ArrayList(dataList))
            chart.build()
            chart.setOnBarClickedListener(OnBarClickedListener { item ->
                Toast.makeText(requireContext(), item.toString(), Toast.LENGTH_SHORT).show();
            })
            chart.disableBar(dataList.size - 1)
        }




//        viewFragment.root.setBackgroundColor(resources.getColor(R.color.blue_300))
//        viewFragment.frgTopBarContainer.setBackgroundColor(resources.getColor(R.color.blue_300))
        viewModel.updateTypeFace().run {
//            viewFragment.frgReferralToolBarLabel.typeface = this
//            viewFragment.frgReferralsMembershipName.typeface = this
//            viewFragment.frgReferralsMembershipLabel.typeface = this
//            viewFragment.frgReferralsUpgrade.typeface = this
//            viewFragment.frgReferralsYouEarningsLabel.typeface = this
//            viewFragment.frgReferralsHelpInvited.typeface = this
//            viewFragment.frgReferralsHelpBonus.typeface = this
//            viewFragment.frgReferralsTotalHelp.typeface = this
//            viewFragment.frgReferralsShowInvited.typeface = this
//            viewFragment.frgReferralsMultiple.typeface = this
//            viewFragment.frgReferralsBonusCurrency.typeface = this
//            viewFragment.frgReferralsShowBonus.typeface = this
//            viewFragment.frgReferralsEquals.typeface = this
//            viewFragment.frgReferralsShowCurrency.typeface = this
//            viewFragment.frgReferralsShowTotal.typeface = this
//            viewFragment.frgReferralsCalculateLabel.typeface = this
//            viewFragment.frgReferralsInfoLabel.typeface = this
//            viewFragment.frgReferralCodeLink.typeface = this
//            viewFragment.frgReferralsTxtLblCode.typeface = this
//            viewFragment.frgReferralInfoLink.typeface = this
//            viewFragment.frgReferralsTxtLblLink.typeface = this
//            viewFragment.frgReferralsShareSocialLabel.typeface = this
//            viewFragment.frgReferralListLinksLabel.typeface = this
        }
    }

    private fun addListener(){
        viewFragment.frgRewardsLinearMenuButtons.setOnClickListener { btn ->
            if(!viewFragment.frgRewardsMenuLinearList.isVisible) {
                viewFragment.frgRewardsMenuLinearList.showScaleTopRBottomL()
            }else{
                viewFragment.frgRewardsMenuLinearList.hideScaleBottomLTopR()
            }
        }

        viewFragment.frgRewardTimeDiagramMenuButtons.setOnClickListener { btn ->
            if(!viewFragment.frgRewardsMenuOtherList.isVisible) {
                viewFragment.frgRewardsMenuOtherList.showScaleTopRBottomL()
            }else{
                viewFragment.frgRewardsMenuOtherList.hideScaleBottomLTopR()
            }
        }

        viewFragment.frgRewardForDiagramShow.apply {
            setOnClickListener { btn ->
                if(isVisible) {
                    viewFragment.frgRewardsMenuOtherList.hideScaleBottomLTopR()
                }
            }
        }

        viewFragment.frgRewardsChartBox.apply {
            setOnClickListener { btn ->
                if(isVisible) {
                    viewFragment.frgRewardsMenuLinearList.hideScaleBottomLTopR()
                }
            }
        }

        viewFragment.frgReferralBackButton.setOnClickListener {
            viewModel.onBackClickListener()
        }
    }

//    private fun moveOffScreen() {
//        val displayMetrics = DisplayMetrics()
//        requireActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics)
//        val height = displayMetrics.heightPixels
//        val offset = (height * 0.65).toInt() /* percent to move */
//        val rlParams = viewFragment.frgRewardsDiagram.getLayoutParams() as ConstraintLayout.LayoutParams
//        rlParams.setMargins(0, 0, 0, -offset)
//        viewFragment.frgRewardsDiagram.setLayoutParams(rlParams)
//    }

    private fun setData(count: Int, range: Float) {
        val values = java.util.ArrayList<PieEntry>()

        val parties = arrayOf(
            "Party A", "Party B", "Party C", "Party D", "Party E", "Party F", "Party G", "Party H",
            "Party I", "Party J", "Party K", "Party L", "Party M", "Party N", "Party O", "Party P",
            "Party Q", "Party R", "Party S", "Party T", "Party U", "Party V", "Party W", "Party X",
            "Party Y", "Party Z"
        )

        for (i in 0 until count) {
            values.add(
                PieEntry((Math.random() * range + range/5).toFloat(),
                    parties.get(i % parties.size)
                )
            )
        }
        val dataSet = PieDataSet(values, null) //"Election Results")
        dataSet.sliceSpace     = 3f
        dataSet.selectionShift = 5f
        dataSet.setColors(*ColorTemplate.MATERIAL_COLORS)
        dataSet.setSelectionShift(0f);
        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter())
        data.setValueTextSize(11f)
        data.setValueTextColor(Color.WHITE)

        data.setValueTypeface(viewModel.updateTypeFace())

//        viewFragment.frgRewardsDiagram.s
        viewFragment.frgRewardsPieChartView.setData(data)
        viewFragment.frgRewardsPieChartView.invalidate()
    }

    private fun generateCenterSpannableText(): SpannableString? {
        val s = SpannableString("Time spend")
        s.setSpan(RelativeSizeSpan(1.7f), 0, 14, 0)
        s.setSpan(StyleSpan(Typeface.NORMAL), 14, s.length - 15, 0)
        s.setSpan(ForegroundColorSpan(Color.GRAY), 14, s.length - 15, 0)
        s.setSpan(RelativeSizeSpan(.8f), 14, s.length - 15, 0)
        s.setSpan(StyleSpan(Typeface.ITALIC), s.length - 14, s.length, 0)
        s.setSpan(ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length - 14, s.length, 0)
        return s
    }

    private fun onUpdateRefPosById(id: String){ //, note: String, isDefault: String){
        viewModel.showEditCurrentRefCode(id) //, note, isDefault)
    }

    private fun updateDialogLabel(txtHeader: String, txtBase: String){
//        viewFragment.frgDialogEditCodeLabel.text = txtHeader
//        viewFragment.frgDialogEditDefaultNoteLabel.text = txtBase
    }

    private fun copyToClipBoard(view: TextView, resText: Int){
        try {
            val clipboard = requireActivity().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText(CONST.APP_PACKAGE, view.text)
            clipboard.setPrimaryClip(clip)
            viewModel.showSuccessMessage(BaseViewModel.AlertText(title = "",
                texts = resources.getString(resText)))
        } catch (ex: RuntimeException) {
            viewModel.showFailureMessage(BaseViewModel.AlertText(title = "",
                texts = resources.getString(R.string.frgErrorFailureTextCopies)))
        }
    }

    private fun setupLinearChart(){
        RewardLinearConfig(viewFragment.frgRewardsChartsShow)
    }

    private fun setupBarChart(){
//        val config =
        RewardBarConfig(viewFragment.frgRewardBarChartView)
    }

    private fun setupPieChart(){
//        val config =
        RewardPieConfig(viewFragment.frgRewardsPieChartView)
//        setData(2, 12.6f)
    }

    private fun rewardsState() = Observer<ViewModelHome.StateRewards> { state ->
        when(state) {
            is ViewModelHome.StateRewards.Default -> {
                viewModel.chartDataLinearUpd()
                viewModel.chartDataBoxPieUpd()
            }
            is ViewModelHome.StateRewards.UpdateLines -> {
//                state.data
                state.data.fillFormatter =
                IFillFormatter { dataSet, dataProvider ->
                    viewFragment.frgRewardsChartsShow.getAxisLeft().getAxisMinimum()
                }
                val data = LineData(state.data)
                data.setValueTypeface(viewModel.updateTypeFace())
                data.setValueTextSize(7f)
                data.setDrawValues(false)

                state.data.mode = LineDataSet.Mode.HORIZONTAL_BEZIER

                state.data.setDrawFilled(true)
                state.data.setDrawCircles(false)
                state.data.cubicIntensity = 0.2f
                state.data.lineWidth    = 1.8f
                state.data.circleRadius = 4f
                state.data.setCircleColor(ResourcesCompat.getColor(resources, R.color.blue_400, null)) //  (Color.WHITE)

                state.data.highLightColor = ContextCompat.getColor(requireContext(), R.color.danger)  //ResourcesCompat.getColor(resources, R.color.blue_400, null) // resources.getColor(R.color.blue_400) //ColorCompat()
                state.data.color = ResourcesCompat.getColor(resources, R.color.blue_400, null) // resources.getColor(R.color.blue_400) //Color.WHITE

                state.data.fillColor = ContextCompat.getColor(requireContext(), R.color.transparent) // ResourcesCompat.getColor(resources, R.color.gray_500, null) // resources.getColor(R.color.gray_500) //Color.WHITE
                state.data.fillDrawable = AppCompatResources.getDrawable(requireContext(), R.drawable.reward_background_linear_chart_gradient)

                state.data.fillAlpha = 0
                state.data.setDrawHorizontalHighlightIndicator(false)

                viewFragment.frgRewardsChartsShow.setData(data)
                viewFragment.frgRewardsChartsShow.invalidate()
            }
            is ViewModelHome.StateRewards.UpdateBars -> {
                viewFragment.frgRewardBarChartView.setData(BarData(state.data))
                viewFragment.frgRewardBarChartView.invalidate()
            }
            is ViewModelHome.StateRewards.UpdatePies -> {
                viewFragment.frgRewardsPieChartView.setData(PieData(state.data))
                viewFragment.frgRewardsPieChartView.invalidate()
            }
            is ViewModelHome.StateRewards.HideLinearMenu -> {
                viewFragment.frgRewardsMenuLabel.text = state.menuItemName
                viewFragment.frgRewardsMenuLinearList.hideScaleBottomLTopR()
            }
            is ViewModelHome.StateRewards.HideBoxPieMenu -> {
                viewFragment.frgRewardsTimeDiagramMenuLabel.text = state.menuItemName
                viewFragment.frgRewardsMenuOtherList.hideScaleBottomLTopR()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.initRewards()
        viewModel.stateRewards.observe(viewLifecycleOwner, rewardsState())
        setupUI()
        addListener()
        setHasOptionsMenu(true);
    }

    override fun onBackPressed(): Boolean {
        return true
    }

    override fun getBinding(): FrgRewardsBinding {
        return FrgRewardsBinding.inflate(layoutInflater)
    }

    override fun injectDependency(component: ComponentViewModel) {
        component.inject(this)
    }

    fun generate(): List<BarData>{
        val dataList = mutableListOf<BarData>(
            BarData("Sun", 30.40f, "30 min 40 sec"),
            BarData("Mon", 80.07f, "80 min 07 sec"),
            BarData("Tue", 11.27f, "11 min 27 sec"),
            BarData("Wed", 71.34f, "71 min 34 sec"),
            BarData("The", 64.22f, "64 min 22 sec"),
            BarData("Fri", 33.33f, "33 min 33 sec"),
            BarData("Sat", 41.68f, "41 min 68 sec")
        )
        return dataList
    }

}