package meet.sumra.app.frg

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import meet.sumra.app.R
import meet.sumra.app.act.ActivityHome
import meet.sumra.app.databinding.FrgNewMeetBinding
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.utils.LogUtil
import java.lang.ref.WeakReference
import java.util.*

class FrgNewMeeting: FragmentBase<FrgNewMeetBinding>() {

    private lateinit var bind: FrgNewMeetBinding
    private lateinit var actHome: WeakReference<ActivityHome>

    companion object{

        val TAG = UUID.randomUUID().toString() //UUID.fromString("Fragment settings").leastSignificantBits

        fun getInstance(): FragmentBase<*>{
            return FrgNewMeeting()
        }
    }

//    @Inject
//    lateinit var repository: MeetChatRepository
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        AppMeet.getInstance().meetComponent.inject(this)
//    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        actHome = WeakReference(requireActivity() as ActivityHome)
        bind = FrgNewMeetBinding.inflate(inflater)
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
    }

    override fun onBackPressed(): Boolean {
        return true
    }

    private fun setupUI(){
        bind.btnMeetCreate.setOnClickListener { actionCreateMeet() }
        bind.btnCloseMeet.setOnClickListener { actionCloseMeet() }

        bind.cellItemVideoSetup.setOnClickListener { actionSwitchVideoStatus() }
    }

    private fun actionCreateMeet(){
        LogUtil.info(txt = "action create new meet...")
        bind.editNameConference.text.let{
            if(it.isNullOrEmpty()){
                Toast.makeText(
                    actHome.get(),
                    "Enter Conference Name",
                    Toast.LENGTH_SHORT).show()
            }else{
//                ActivityMeet.getInstance(actHome.get()!!, viewModelStore)
            }
        }
    }

    private fun actionCloseMeet(){
        LogUtil.info(txt = "action close meet...")
        actHome.get()?.run {
            helpFragmentHide()
        }
    }

    private fun actionSwitchVideoStatus() {
        val preferences = actHome.get()?.getPreferences(Context.MODE_PRIVATE)
        val res = preferences?.getBoolean("VIDEO_ON", false)
        if(res!!){
            bind.btnOnOffVideoShow.setBackgroundResource(R.drawable.flag_indonesia)
        } else {
            bind.btnOnOffVideoShow.setBackgroundResource(R.drawable.flag_poland)
        }
        val edit = preferences.edit()
        edit.putBoolean("VIDEO_ON", !res)
        edit.apply()
    }

    override fun getBinding(): FrgNewMeetBinding {
        TODO("Not yet implemented")
    }

    override fun injectDependency(component: ComponentViewModel) {
//        TODO("Not yet implemented")
    }
}