package meet.sumra.app.frg.drawer

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.*
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.Observer
import com.github.mikephil.charting.charts.LineChart
import meet.sumra.app.R
import meet.sumra.app.data.mdl.earnings.ByDateLineChartConfigure
import meet.sumra.app.data.mdl.earnings.EarningPlan
import meet.sumra.app.data.mdl.earnings.PeriodState
import meet.sumra.app.data.mdl.earnings.modelEarnings
import meet.sumra.app.databinding.FrgEarningsBinding
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.frg.FragmentBase
import meet.sumra.app.interfaces.sealeds.SChartInterval
import meet.sumra.app.views.compose.ui_theme.StyleText
import meet.sumra.app.vm.vm_home.StateEarnings
import meet.sumra.app.vm.vm_home.ViewModelHome
import javax.inject.Inject

class FrgEarnings: FragmentBase<FrgEarningsBinding>() {

    companion object {
        val TAG = FrgEarnings::class.java.simpleName
        fun getInstance(): FragmentBase<*> {
            return FrgEarnings()
        }
    }

    internal lateinit var viewModel: ViewModelHome
        @Inject set

    private lateinit var linear: LineChart // = LineChart(context)
    private lateinit var debora: ByDateLineChartConfigure //ByDateLineChartConfigure(linear, viewModel)

    @ExperimentalMaterialApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.initEarnings()
        viewModel.stateEarnings.observe(viewLifecycleOwner, earningsState())

        linear = LineChart(context)
        debora = ByDateLineChartConfigure(linear, viewModel)
        viewFragment.frgMeetRootEarnings.setContent { ShowEarnings() }
        switchView.value = true
    }

    override fun onBackPressed(): Boolean {
        viewModel.onBackClickListener()
        return true
    }

    override fun getBinding(): FrgEarningsBinding {
        return FrgEarningsBinding.inflate(layoutInflater)
    }

    override fun injectDependency(component: ComponentViewModel) {
        component.inject(this)
    }

    private fun earningsState() = Observer<StateEarnings> { state ->
        when (state) {
            is StateEarnings.Default -> {

            }
        }
    }

    private val isPeriod = mutableStateOf(false)
    private val menuList = mutableStateListOf<PeriodState>(
        PeriodState(period = "Day", isActive =  true),
        PeriodState(period = "Week"),
        PeriodState(period = "Month"),
        PeriodState(period = "Year" ),
    )

    private val earnings = mutableStateOf(
        mutableListOf<modelEarnings.YourEarnings>(
            modelEarnings.YourEarnings(
                title = "Total earnings",
                image = R.drawable.ic_earnings_card_your_total,
                value = "$4,000"
            ),
            modelEarnings.YourEarnings(
                title = "Membership plan",
                image = R.drawable.ic_earnings_card_your_plan,
                value = "Bronze"
            ),
            modelEarnings.YourEarnings(
                title = "Bonus per invited user",
                image = R.drawable.ic_earnings_card_your_invited,
                value = "$8"
            ),
            modelEarnings.YourEarnings(
                title = "Invited users",
                image = R.drawable.ic_earnings_invited_users,
                value = "500"
            )
        )
    )

    private val leaderBoard = mutableStateOf(
        mutableListOf<modelEarnings.LeaderBoard>(
            modelEarnings.LeaderBoard(position = 1, image = R.drawable.ic_referral_medal_gold  , displayName = "Hello World" , count = "$254812"),
            modelEarnings.LeaderBoard(position = 2, image = R.drawable.ic_referral_medal_silver, displayName = "Invite Uses" , count = "$210021"),
            modelEarnings.LeaderBoard(position = 3, image = R.drawable.ic_referral_medal_bronze, displayName = "Call Defise" , count = "$155861"),
            modelEarnings.LeaderBoard(position = 4, image = R.drawable.ic_earnings_medal_star  , displayName = "Upgrade Dell", count = "$89823")
        )
    )

    private val cardPadding = 20
    private val cardRadius = 10

    @Preview
    @Composable
    @ExperimentalMaterialApi
    private fun ShowPreview() {
        ShowEarnings()
    }

    @ExperimentalMaterialApi
    @Composable
    fun ToolBar(title: String) {
        Row(
            modifier = Modifier
                .fillMaxWidth(1f)
                .height(dimensionResource(id = R.dimen.toolBarHeight))
                .background(color = Color.Transparent),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Surface(
                onClick = { viewModel.onBackClickListener() },
                modifier = Modifier
                    .height(dimensionResource(id = R.dimen.toolBarHeight))
                    .width(dimensionResource(id = R.dimen.toolBarHeight)),
                color = Color.Transparent
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(start = 8.dp),
                    contentAlignment = Alignment.Center
                ) {
                    Image(
                        modifier = Modifier
                            .height(24.dp)
                            .width(24.dp),
                        painter = painterResource(id = R.drawable.ic_arrow_back_ios),
                        contentDescription = "Button back",
                        colorFilter = ColorFilter.tint(color = Color(0xffFFFFFF)) //, blendMode = BlendMode.Src)
                    )
                }
            }
            val txtPad = 16
            Text(
                modifier = Modifier
                    .weight(1f)
                    .padding(start = txtPad.dp, end = txtPad.dp),
                text = title,
                style = StyleText.txtEarnCardLbl()
            )
        }
    }

    @Composable
    @ExperimentalMaterialApi
    private fun BlockYourEarning() {
        val brush = Brush.linearGradient(
            colors = listOf(Color(0xff2188FF), Color(0xff0E3868)),
            start  = Offset(0f, Float.POSITIVE_INFINITY), //, 0f),
            end    = Offset(Float.POSITIVE_INFINITY, 0f)
        )
        Box(
            modifier = Modifier
                .padding(
                    start = cardPadding.dp,
                    end = cardPadding.dp,
                    top = 30.dp,
                    bottom = cardPadding.dp
                )
                .background(color = Color.Transparent)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize(1f)
                    .background(brush = brush, shape = RoundedCornerShape(cardRadius.dp))
                    .padding(all = cardPadding.dp)
//                    .height(350.dp)
            ) {
                Text(
                    text = stringResource(id = R.string.frgEarningsYouTitle),
                    style = StyleText.txtEarnCardLbl(),
                    modifier = Modifier
                        .padding(bottom = 15.dp)
                )
                earnings.value.forEachIndexed { index, value ->
                    RowYouEarnings(title = value.title, icon = value.image, value = value.value)
                }
                Row(
                    modifier = Modifier
                        .fillMaxSize(1f),
                    verticalAlignment = Alignment.Bottom
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_earnings_column_coin),
                        contentDescription = "Image",
                        modifier = Modifier
                            .height(67.dp)
                            .width(61.dp)
                    )
                    Spacer(modifier = Modifier.weight(1f))
                    Surface(
                        onClick = { Log.i("CLICK", "UPGRADE BUTTON PRESSED") },
                        modifier = Modifier
                            .height(40.dp)
                            .width(168.dp),
                        color = Color(0xff2188FF),
                        shape = RoundedCornerShape(size = 4.dp)
                    ) {
                        Box(
                            modifier = Modifier.fillMaxSize(1f),
                            contentAlignment = Alignment.Center
                        ) {
                            Row(
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                Text(
                                    text = stringResource(id = R.string.frgEarningsYourPlan),
                                    style = StyleText.txtEarnCardBtn()
                                )
                                Spacer(modifier = Modifier.width(8.dp))
                                Image(
                                    modifier = Modifier
                                        .height(12.dp)
                                        .width(12.dp),
                                    contentDescription = "Arrow to upgrade...",
                                    painter = painterResource(id = R.drawable.ic_earnings_upgrade_arrow)
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    @Composable
    fun RowYouEarnings(title: String, icon: Int, value: String) {
        val sizeImg = 18
        Row(
            modifier = Modifier
                .fillMaxSize(1f)
                .height(32.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                modifier = Modifier.weight(1f),
                text = title,
                style = StyleText.txtEarnCardTxt()
            )
            Row(
                modifier = Modifier.background(Color.Transparent)
            ) {
                Image(
                    painter = painterResource(icon),
                    contentDescription = "Placeholder",
                    modifier = Modifier
                        .height(sizeImg.dp)
                        .width(sizeImg.dp)
                )
                Spacer(modifier = Modifier.width(7.dp))
                Text(
                    text = value,
                    style = StyleText.txtEarnCardTxt()
                )
            }
        }
    }

    @Composable
    @ExperimentalMaterialApi
    private fun BlockEarningsByDate() {
        Box(
            modifier = Modifier
                .padding(all = cardPadding.dp)
                .background(color = Color.Transparent)
        ) {
            Surface(
                modifier = Modifier
                    .fillMaxWidth(1f),
//                    .padding(start = cardPadding.dp, end = cardPadding.dp),
                color = Color.Transparent
            ) {
                Column(
                    modifier = Modifier.fillMaxSize()
                ) {
                    Row(
                        modifier = Modifier.fillMaxWidth(1f)
                    ) {
                        Text(
                            text = "Earnings by date",
                            style = StyleText.txtBoardCardLbl(),
                            modifier = Modifier
                                .weight(1f)
                                .padding(start = cardPadding.dp, end = cardPadding.dp)
                        )
                        Surface(
                            onClick = {
                                Log.i("INTERVAL", "button interval...")
                                isPeriod.value = !isPeriod.value
                            },
                            modifier = Modifier.padding(start = 8.dp, end = 8.dp),
                            color = Color(0xffF4F5F7),
                            shape = RoundedCornerShape(size = 8.dp)
                        ){
                            Row(
                                modifier = Modifier
                                    .height(32.dp)
                                    .padding(start = 8.dp, end = 8.dp),
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                Text(
                                    text = "View by this year",
                                    style = StyleText.txtDateCardLst()
                                )
                                Spacer(modifier = Modifier.width(7.dp))
                                Image(
                                    painter = painterResource(R.drawable.ic_arrow_down),
                                    contentDescription = "Placeholder",
                                    modifier = Modifier
                                        .height(12.dp)
                                        .width(12.dp),
                                    colorFilter = ColorFilter.tint(color = Color(0xffB0B7C3))
                                )
                            }
                        }
                    }

                    linear.data = debora.fetchDataChartUpdate(params = SChartInterval.Weeks("WEEKS"))

                    Box(modifier = Modifier
                        .fillMaxSize()
                        .height(220.dp)
                    ){
                        AndroidView(
                            factory = { context -> linear },
                            modifier = Modifier
                                .fillMaxSize(1f)
                        )
                        if(isPeriod.value){
                            Box(
                                modifier = Modifier
                                    .fillMaxWidth(1f)
                                    .background(color = Color.Transparent)
                                    .padding(end = 8.dp, top = 2.dp),
                                contentAlignment = Alignment.TopEnd
                            ) {
                                val rad = 4
                                Column (
                                    modifier = Modifier
                                        .width(100.dp)
                                        .background(
                                            color = colorResource(id = R.color.gray_300),
                                            shape = RoundedCornerShape(rad.dp)
                                        )
                                ){
                                    menuList.forEachIndexed { ind, it ->
                                        PeriodItemMenu(position = ind, rad = rad)
                                        if (ind <= menuList.size - 2) {
                                            Divider(
                                                modifier = Modifier
                                                    .fillMaxWidth()
                                                    .height(1.dp),
                                                color = Color.White,
                                            )
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Composable
    @ExperimentalMaterialApi
    private fun PeriodItemMenu(position: Int, rad: Int) {
        Surface(
            onClick = {
                isPeriod.value = !isPeriod.value
                disableAllPeriod()
                menuList[position].isActive = true
            }
            , modifier = Modifier
                .height(40.dp)
                .width(100.dp)
            , color = Color.Transparent
//            , shape = RoundedCornerShape(size = rad.dp)
        ) {
            Row(
                modifier = Modifier
                    .fillMaxSize()
                    .background(
                        color = if (menuList[position].isActive) colorResource(id = R.color.gray_300) else Color.Transparent,
                        shape = shapeReturn(position = position, rad)
                    )
                , verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    modifier = Modifier.padding(horizontal = 8.dp),
                    text = menuList[position].period,
                )
            }
        }
    }

    private fun disableAllPeriod(){
        menuList.forEach { it.isActive = false }}

    private fun shapeReturn(position: Int, rad: Int): Shape{
        return if(position == 0){
            RoundedCornerShape(topStart = rad.dp, topEnd = rad.dp)
        }else if(position == menuList.size -1){
            RoundedCornerShape(bottomStart = rad.dp, bottomEnd = rad.dp)
        }else{
            RoundedCornerShape(size = 0.dp)
        }
    }

    @Composable
    @ExperimentalMaterialApi
    private fun BlockLeaderboard() {
        Box(
            modifier = Modifier
                .fillMaxWidth(1f)
                .padding(all = cardPadding.dp)
        ) {
            Surface(
                elevation = 2.dp,
    //                color = Color.White,
    //                shape = RoundedCornerShape(size = cardRadius.dp),
                modifier = Modifier
                    .fillMaxWidth(1f),
    //                    .padding(20.dp)
    //                    .background(
                        color = Color.White,
                        shape = RoundedCornerShape(size = cardRadius.dp)
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(color = Color.Transparent)
    //                        .padding(20.dp)
                ) {
                    Text(
                        modifier = Modifier.padding(
                            top = cardPadding.dp,
                            bottom = 15.dp,
                            start = cardPadding.dp,
                            end = cardPadding.dp),
                        text = "Leaderboard",
                        style = StyleText.txtBoardCardLbl()
                    )
                    leaderBoard.value.forEachIndexed { index, value ->
                        RowEarningsLeaderboard(
                            position = value.position.toString(),
                            iconState = value.image,
                            userName = value.displayName,
                            count = value.count
                        )
                        Divider(
                            color = Color(0xffEDEDEE),
                            modifier = Modifier
                                .padding(start = cardPadding.dp, end = cardPadding.dp)
                                .fillMaxWidth()
                                .height(1.dp)
                        )
                    }
                }
            }
        }
    }

    @Composable
    fun RowEarningsLeaderboard(position: String, iconState: Int, userName: String, count: String) {
        val sizeImg = 24
        val sizeIco = 15
        val itemHeight = 45
        Row(
            modifier = Modifier
                .fillMaxWidth(1f)
                .height(itemHeight.dp)
                .padding(start = cardPadding.dp, end = cardPadding.dp)
                .background(Color(0x00ffffff)),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Row(
                modifier = Modifier
                    .background(Color.Transparent)
                    .weight(1f),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    modifier = Modifier.padding(start = 8.dp, end = 4.dp),
                    text = position,
                    style = StyleText.txtBoardCardTxt()
                )
                Text(
                    modifier = Modifier.padding(end = 8.dp),
                    text = "st",
                    style = StyleText.txtBoardCardHnt()
                )
                Image(
                    painter = painterResource(iconState),
                    contentDescription = "Placeholder",
                    modifier = Modifier
                        .height(sizeImg.dp)
                        .width(sizeImg.dp)
                )
                Text(
                    modifier = Modifier
                        .weight(1f)
                        .padding(start = 8.dp),
                    text = userName,
                    style = StyleText.txtBoardCardTxt()
                )
            }
            Row(
                modifier = Modifier.background(Color.Transparent),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = painterResource(R.drawable.ic_earnings_show_coin),
                    contentDescription = "Placeholder",
                    modifier = Modifier
                        .height(sizeIco.dp)
                        .width(sizeIco.dp)
                        .padding(start = 8.dp)
                )
                Text(
                    modifier = Modifier.padding(start = 8.dp, end = 8.dp),
                    text = count,
                    style = StyleText.txtBoardCardVal()
                )
            }
        }
    }


    @Composable
    @ExperimentalMaterialApi
    private fun BlockProjected() {
        val brush = Brush.linearGradient(
            colors = listOf(
                Color(0xff0E3868),
                Color(0xff2188FF)),
            start = Offset(.5f, Float.POSITIVE_INFINITY),
            end   = Offset(Float.POSITIVE_INFINITY, .5f)
        )
        val radSelBtn = 10
        val radCard = 10

        Box(
            modifier = Modifier
                .fillMaxWidth(1f)
                .background(Color.Transparent),
            contentAlignment = Alignment.BottomCenter
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .background(Color.Transparent)
                    .padding(all = cardPadding.dp),
                contentAlignment = Alignment.BottomCenter
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxSize(1f)
                        .background(color = Color.Transparent)
                ) {
                    Text(
                        text = stringResource(id = R.string.earningsProjectedLabel),
                        style = StyleText.txtEarnCardBtn(),
                        modifier = Modifier
                            .fillMaxSize(1f)
                            .padding(
                                start = cardPadding.dp,
                                end = cardPadding.dp,
                                bottom = cardPadding.dp
                            )
                    )

                    Row(
                        modifier = Modifier
                            .fillMaxSize(1f)
                            .height(56.dp)
                            .background(Color.Transparent),
                        verticalAlignment = Alignment.Top
                    ) {
                        Log.i("VAL", "${switchView.value}")
//                        if(switchView.value) {
                            planState.forEachIndexed { index, value ->
                                ProjectButton(
                                    Modifier
                                        .fillMaxSize(1f)
                                        .height(56.dp)
                                        .weight(1f),
//                            isEnable = true,
                                    position = index,
                                    radius = radSelBtn.dp
                                )
                                if (index < planState.size - 1) {
                                    Spacer(
                                        modifier = Modifier
                                            .width(10.dp)
                                            .fillMaxHeight(1f)
                                    )
                                }
                                if(index == planState.size-1){
                                    switchView.value = false
                                }
                            }
//                        }
                    }

                    Spacer(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(14.dp)
                    )

                    Column(
                        modifier = Modifier
                            .fillMaxSize(2f)
                            .background(brush = brush, shape = RoundedCornerShape(radCard.dp))
                    ) {
                        Text(
                            text = stringResource(id = R.string.earningsProjectedCardLabel),
                            style = StyleText.txtProjectedCardLbl(),
                            modifier = Modifier
                                .fillMaxSize(1f)
                                .weight(1f)
                                .padding(all = cardPadding.dp)
                        )
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(all = 10.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            val textState = mutableStateOf("100")
                            // remember{ mutableStateOf(TextFieldValue()) }
//                            textState.value = TextFieldValue("100")
                            Column(
                                modifier = Modifier.weight(1f)
                            ) {
                                Text(
                                    text = stringResource(id = R.string.earningsProjectedCardUser),
                                    style = StyleText.txtProjectedCardTxt()
                                )
                                Spacer(modifier = Modifier.height(5.dp))
                                Column(
                                    modifier = Modifier
                                        .fillMaxSize(1f)
                                        .height(35.dp)
                                        .background(
                                            color = Color(0xff2188FF),
                                            shape = RoundedCornerShape(size = 4.dp)
                                        ), horizontalAlignment = Alignment.CenterHorizontally
                                ) {
                                    Row(
                                        modifier = Modifier
                                            .fillMaxHeight(1f)
                                            .height(35.dp)
                                            .padding(start = 4.dp, end = 4.dp),
//                                    .background(
//                                        color = Color(0xff2188FF),
//                                        shape = RoundedCornerShape(size = 4.dp)),
                                        verticalAlignment = Alignment.CenterVertically
                                    ) {
                                        Spacer(modifier = Modifier.fillMaxHeight().weight(1f))
                                        Image(
                                            painter = painterResource(
                                                id = R.drawable.ic_earnings_invited_users
                                            ),
                                            contentDescription = "ImageIcon"
                                        )
                                        Spacer(
                                            modifier = Modifier
                                                .fillMaxHeight()
                                                .width(8.dp)
                                        )
                                        OutlinedTextField(
                                            modifier = Modifier
                                                .fillMaxHeight()
                                                .background(Color.Transparent)
                                                .padding(all = 0.dp),
                                            value = textState.value,
                                            onValueChange = { textState.value = it },
//                                            label = { Text("Invite count") },
                                            colors = TextFieldDefaults.outlinedTextFieldColors(
                                                unfocusedBorderColor = Color.Transparent, // (0xffFFffFF),
                                                focusedBorderColor = Color.Transparent, // (0xffFFffFF),
                                                focusedLabelColor  = Color.Transparent, // (0xffFFffFF),
                                                unfocusedLabelColor = Color.Transparent, // (0xffFFffFF),
                                                backgroundColor     = Color.Transparent, // (0xffFFffFF),
                                                textColor   = Color(0xffFFffFF),
                                                cursorColor = Color(0xffFFffFF)),
                                            textStyle = StyleText.txtProjectedCrdVal(),
                                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
                                        )
                                        Spacer(modifier = Modifier.fillMaxHeight().weight(1f))
//                                        Text(
//                                            text = "10,000",
//                                            style = StyleText.txtProjectedCrdVal()
//                                        )
                                    }
                                }
                            }
                            Text(
                                modifier = Modifier.padding(top = 19.dp, start = 4.dp, end = 4.dp),
                                text = "x",
                                style = StyleText.txtProjectedOp()
                            )
                            Column(
                                modifier = Modifier
                                    .weight(1f)
                            ) {
                                Text(
                                    text = stringResource(id = R.string.earningsProjectedCardBonus),
                                    style = StyleText.txtProjectedCardTxt()
                                )
                                Spacer(modifier = Modifier.height(5.dp))
                                Column(
                                    modifier = Modifier
                                        .fillMaxSize(1f)
                                        .height(35.dp)
                                        .background(
                                            color = Color(0xff2188FF),
                                            shape = RoundedCornerShape(size = 4.dp)
                                        ), horizontalAlignment = Alignment.CenterHorizontally
                                ) {
                                    Row(
                                        modifier = Modifier
                                            .fillMaxHeight(1f)
                                            .height(35.dp),
//                                        .background(
//                                            color = Color(0xff2188FF),
//                                            shape = RoundedCornerShape(size = 4.dp)),
                                        verticalAlignment = Alignment.CenterVertically
                                    ) {
                                        Image(
                                            painter = painterResource(
                                                id = R.drawable.ic_earnings_total
                                            ),
                                            contentDescription = "ImageIcon"
                                        )
                                        Spacer(
                                            modifier = Modifier
                                                .fillMaxHeight()
                                                .width(8.dp)
                                        )
                                        Text(
                                            text = "${curPlan.value.planCount}", // "$10",
                                            style = StyleText.txtProjectedCrdVal()
                                        )
                                    }
                                }
                            }
                            Text(
                                modifier = Modifier.padding(top = 19.dp, start = 4.dp, end = 4.dp),
                                text = "=",
                                style = StyleText.txtProjectedOp()
                            )
                            Column(
                                modifier = Modifier
                                    .weight(1f)
                            ) {
                                Text(
                                    text = stringResource(id = R.string.earningsProjectedCardTotal),
                                    style = StyleText.txtProjectedCardTxt()
                                )
                                Spacer(modifier = Modifier.height(5.dp))
                                Column(
                                    modifier = Modifier
                                        .fillMaxSize(1f)
                                        .height(35.dp)
                                        .background(
                                            brush = Brush.horizontalGradient(
                                                mutableListOf(
                                                    Color(0xffFA9E4F),
                                                    Color(0xffFDB946)
                                                )
                                            ),
                                            shape = RoundedCornerShape(size = 4.dp)
                                        ), horizontalAlignment = Alignment.CenterHorizontally
                                ) {
                                    Row(
                                        modifier = Modifier
                                            .fillMaxHeight(1f)
                                            .height(35.dp),
//                                        .background(
//                                            color = Color(0xff2188FF),
//                                            shape = RoundedCornerShape(size = 4.dp)),
                                        verticalAlignment = Alignment.CenterVertically
                                    ) {
                                        val result = curPlan.value.planCount * textState.value.toFloat()
                                        Image(
                                            painter = painterResource(
                                                id = R.drawable.ic_earnings_total
                                            ),
                                            contentDescription = "ImageIcon"
                                        )
                                        Spacer(
                                            modifier = Modifier
                                                .fillMaxHeight()
                                                .width(8.dp)
                                        )
                                        Text(
                                            text = "${result}",  //"100,000",
                                            style = StyleText.txtProjectedCrdVal()
                                        )
                                    }
                                }
                            }
                        }
                        Spacer(
                            modifier = Modifier
                                .fillMaxWidth(1f)
                                .height(31.dp)
                        )

                        Box(
                            modifier = Modifier
                                .fillMaxSize()
                        ) {
                            Box(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .padding(start = 10.dp, end = 10.dp, bottom = 20.dp)
                            ) {
                                Surface(
                                    onClick = {
                                        Log.i("BUTTON UPGRADE", "GO TO UPGRADE PLAN")
                                    },
                                    color = Color.Transparent,
                                    modifier = Modifier
                                        .height(40.dp)
                                ) {
                                    Column(
                                        modifier = Modifier
                                            .fillMaxHeight()
                                            .background(
                                                color = Color(0xff2188FF),
                                                shape = RoundedCornerShape(size = 4.dp)
                                            ), horizontalAlignment = Alignment.CenterHorizontally
                                    ) {
                                        Row(
                                            modifier = Modifier
                                                .padding(start = 18.dp, end = 18.dp)
                                                .fillMaxHeight(1f),
                                            verticalAlignment = Alignment.CenterVertically
                                        ) {
                                            val temp = when(planState[activePlan.value].planName.lowercase()){
                                                "Gold".lowercase() -> planState[activePlan.value].planName
                                                "Silver".lowercase() -> planState[activePlan.value].planName
                                                else -> planState[1].planName
                                            }
                                            Text(
                                                modifier = Modifier.padding(end = 8.dp),
                                                text = "Upgrade to ${temp} plan",
                                                style = StyleText.txtProjectedBtnLbl()
                                            )
                                            Image(
                                                painter = painterResource(
                                                    id = R.drawable.ic_earnings_upgrade_arrow
                                                ),
                                                contentDescription = "Upgrade to next plan"
                                            )
                                        }
                                    }
                                }
                            }
                            Box(
                                modifier = Modifier.fillMaxSize(1f),
                                contentAlignment = Alignment.BottomEnd
                            ) {
                                Image(
//                                modifier = Modifier
//                                    .height(140.dp)
//                                    .width(66.dp)
//                                    .background(
//                                        color = Color.Transparent,
//                                        shape = RectangleShape
//                                    ),
                                    painter = painterResource(id = R.drawable.ic_bacgroung_earnings_chart_money),
                                    contentDescription = "chart",
                                    contentScale = ContentScale.FillBounds,
                                    alignment = Alignment.TopStart
                                )
                            }
                        }
                    }
                }
            }
            Row(
                modifier = Modifier
                    .fillMaxSize()
                    .background(color = Color.Transparent), //(0x33654554)),
                verticalAlignment = Alignment.Bottom
            ){
                Column(
                    modifier = Modifier.fillMaxSize(),
                    horizontalAlignment = Alignment.End
                ) {
                    Image(
                        modifier = Modifier
                            .height(60.dp)
                            .width(82.dp)
                            .padding(end = cardPadding.dp)
                            .background(
                                color = Color.Transparent,
                                shape = RectangleShape
                            ),
                        painter = painterResource(id = R.drawable.ic_background_bar_money),
                        contentDescription = "bar coin",
                        contentScale = ContentScale.FillBounds,
                        alignment = Alignment.TopStart
                    )
                }
            }
        }
    }

    private val planState = mutableStateListOf(
        EarningPlan(isActive = true,     planName = "Free",),
        EarningPlan(planName = "Bronze", planCount = 5.0F),
        EarningPlan(planName = "Silver", planCount = 8.0F),
        EarningPlan(planName = "Gold",   planCount = 10.0F))

    private val activePlan = mutableStateOf<Int>(0)
    private val curPlan = mutableStateOf(planState[0])
    private val switchView = mutableStateOf(false)

    @Composable
    @ExperimentalMaterialApi
    private fun ProjectButton(
        modifier: Modifier,
//        plan: MutableState<EarningPlan>,
        position: Int,
        radius: Dp
        ){
        Column(
            modifier = modifier
//                .fillMaxWidth(1f)
//                .height(56.dp)
            , horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Surface(
                onClick = {
                    Log.i("PLAN", "PRESS BUTTON SELECT")
                    if(position != activePlan.value) {
                        Log.i("PLAN is ACTIVE", "PRESS BUTTON SELECT")
                        disableAllPlaning()
                        curPlan.value = planState[position]
                        activePlan.value = position
                        planState[position].isActive = true
                        switchView.value = true
                    }
                },
                modifier = Modifier
                    .fillMaxSize(1f)
                    .height(46.dp)
                    .weight(1f),
                color = if (planState[position].isActive) Color(0xff2188FF) else Color.White,
                shape = RoundedCornerShape(size = radius),
//                    .background(

//                    ),
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Row(
                        modifier = Modifier.fillMaxHeight(),
                        verticalAlignment = Alignment.CenterVertically) {
                        Text(
                            text = planState[position].planName,
                            style = if (planState[position].isActive) StyleText.txtProjectedBtnOn() else StyleText.txtProjectedBtnOf(),
                            modifier = Modifier.padding(start = 4.dp, end = 4.dp)
                        )
                    }
                }
            }
            Box(
                modifier = Modifier.height(10.dp)
            ) {
                if (planState[position].isActive) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_arrow_down),
                        contentDescription = "down",
                        modifier = Modifier
//                            .shadow(elevation = 2.dp)
                            .height(10.dp),
                        colorFilter = ColorFilter.tint(color = Color(0xff2188FF))
                    )
                }
            }
        }
    }

    private fun disableAllPlaning(){ planState.forEach { it.isActive = false }}
    
    @Composable
    @ExperimentalMaterialApi
    private fun BlockGoldPlan() {
        Box(modifier = Modifier
            .fillMaxWidth(1f)
            .background(Color.Transparent)
            .padding(all = cardPadding.dp)) {
            Column(
                modifier = Modifier
                    .height(260.dp)
                    .fillMaxWidth(1f)
                    .background(
                        color = Color.White,
                        shape = RoundedCornerShape(size = cardRadius.dp)
                    )
                ) {
                Box(modifier = Modifier
                    .fillMaxWidth(1f),
                    contentAlignment = Alignment.BottomCenter
                ) {
                    Image(
                        modifier = Modifier
                            .fillMaxWidth(1f)
                            .height(151.dp)
                            .background(
                                color = Color.Transparent,
                                shape = RoundedCornerShape (
                                    bottomStart = cardRadius.dp,
                                    bottomEnd = cardRadius.dp )),
                        painter = painterResource(id = R.drawable.ic_bacground_plan_showing),
                        contentDescription = "Background images",
                        contentScale = ContentScale.Crop,
                        alignment = Alignment.TopCenter
                    )
                    Column(
                        modifier = Modifier.fillMaxSize(1f),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Row(
                            modifier = Modifier
                                .background(Color.Transparent)
                                .height((25 + 20 + 31).dp)
                                .padding(top = 25.dp, bottom = 31.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = "Block plan",
                                style = StyleText.txtPlanLabelStart(),
                                modifier = Modifier
                                    .padding(start = cardPadding.dp)
                                    .background(Color.Transparent)
                            )
                            Text(
                                text = " projected earnings",
                                style = StyleText.txtPlanLabelFinal(),
                                modifier = Modifier
                                    .padding(end = cardPadding.dp)
                                    .background(Color.Transparent)
                            )
                        }
                        Box(
                            modifier = Modifier.fillMaxSize(1f)
                        ) {
                            Column(
                                modifier = Modifier.fillMaxWidth(1f),
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {
                                Box(
                                    modifier = Modifier
                                        .height(151.dp)
                                        .width(151.dp)
                                        .background(color = Color.White, shape = CircleShape)
                                    ,
                                    contentAlignment = Alignment.Center
                                ) {
//                                    Canvas(
//                                        modifier = Modifier
//                                            .height(151.dp)
//                                            .width(151.dp),
//                                        onDraw = {
//                                            drawCircle(
//                                                color = Color.White,
//                                                rotateRad()
//                                                 StrokeCap.Round)
//                                            )
//                                        }
//                                    )
                                    Canvas(
                                        modifier = Modifier
                                            .height(151.dp)
                                            .width(151.dp),
                                    ) {
                                        val brush = Brush.radialGradient(
                                            colors = listOf(
                                                Color(0xffEDEEFF),
                                                Color(0xff2188FF)
                                            ),
                                            tileMode = TileMode.Clamp,
                                        )
                                        drawArc(
                                            brush = brush,
                                            startAngle = -25f,
                                            sweepAngle = 360f,
                                            useCenter = false,
                                            style = Stroke(width = 14f, cap = StrokeCap.Round)
                                        )
                                    }
                                    Canvas(
                                        modifier = Modifier
                                            .height(114.dp)
                                            .width(114.dp),
                                    ) {
                                        val brush = Brush.radialGradient(
                                            colors = listOf(
                                                Color(0xffFFF6ED),
                                                Color(0xffFDB946)
                                            )
                                        )
                                        drawArc(
                                            brush = brush,
                                            startAngle = -25f,
                                            sweepAngle = 360f,
                                            useCenter = false,
                                            style = Stroke(width = 14f, cap = StrokeCap.Round)
                                        )
                                    }
                                    Column(

                                    ) {
                                        Text(
                                            modifier = Modifier.fillMaxWidth(1f),
                                            text = "UP TO",
                                            style = StyleText.txtPlanCenterUp(),
                                            textAlign = TextAlign.Center
                                        )
                                        Text(
                                            modifier = Modifier.fillMaxWidth(1f),
                                            text = "$300,000",
                                            style = StyleText.txtPlanCenterDw(),
                                            textAlign = TextAlign.Center
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Composable
    @ExperimentalMaterialApi
    private fun ShowEarnings() {
        val rootBackground = Brush.horizontalGradient(
            colors = listOf(Color(0xff02C2FF), Color(0xff0E6AE3))
        )
        val backRadius = 28
        Column(
            modifier = Modifier
                .fillMaxSize(1f)
                .background(brush = rootBackground)
        ) {
            val scrollState = rememberScrollState()
            ToolBar(title = stringResource(id = R.string.frg_appbar_earnings_label))
            Column(
                modifier = Modifier
                    .background(
                        color = Color(0xffFAF9FC),
                        shape = RoundedCornerShape(
                            topStart = backRadius.dp,
                            topEnd = backRadius.dp
                        )
                    )
                    .weight(1f)
                    .fillMaxSize(1f)
                    .verticalScroll(scrollState)
            ) {
                BlockYourEarning()
                BlockEarningsByDate()
                BlockLeaderboard()
                BlockProjected()
                BlockGoldPlan()
            }
        }
    }
}