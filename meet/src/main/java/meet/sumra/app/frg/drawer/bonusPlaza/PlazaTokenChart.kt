package meet.sumra.app.frg.drawer.bonusPlaza

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.*
import meet.sumra.app.R
import meet.sumra.app.data.mdl.earnings.cards
import meet.sumra.app.data.mdl.plaza.*
import meet.sumra.app.data.configs.settingsCharts.setupBarTokenChartStatistic
import meet.sumra.app.data.configs.settingsCharts.setupLineTokenChartStatistic
import meet.sumra.app.data.configs.settingsChartsData.getDataForLineChartSecTokenChart
import meet.sumra.app.views.compose.ui_theme.StyleText
import kotlin.random.Random


@Preview
@Composable
fun TokenPreview() {

}

private val colorIndicator = mutableListOf(
    Color(0xffFFAB00),
    Color(0xff38CB89)
)

private val sizeIndicator = 6.dp
private var lineHeight = 280.dp
private var barHeight = 280.dp
private val sizePieChart = 150.dp

val rootPad = 20.dp
val cardRad = 10.dp
val cardPadHor = 10.dp
val cardPadVer = 15.dp

val isPeriodSelect = mutableStateOf(false)
val selectedPosition = mutableStateOf(3)
val selectedLabel = mutableStateOf(cards.EPeriod.YEAR.label)

@ExperimentalMaterialApi
@Composable
fun GetPageTokenChart(
    lin: MutableState<LineData>,
    pie: MutableState<SetupBar>,
    bar: MutableState<List<BarDataSet>>){

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(
                color = Color(0xffF8F9FA),
                shape = RoundedCornerShape(topStart = 25.dp, topEnd = 25.dp)
            )
    ){
        val scrollState = ScrollState(initial = 1)
        Column(modifier = Modifier
            .fillMaxSize()
            .verticalScroll(state = scrollState, enabled = true)
        ){
            FetchTokenLineChart(item = lin)
            FetchTokenPieBarChart(dataPieChart = pie, dataBarChart = bar)
//            FetchTokenPieChart (item = pie)
//            FetchTokenBarChart (item = bar)
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun FetchTokenLineChart(item: MutableState<LineData>) {
    Box(
        modifier = Modifier
            .fillMaxSize(1f)
            .padding(all = rootPad)
    ){
        Surface(
            modifier = Modifier.fillMaxSize(1f),
            color = Color(0xffF8F9FA),
            shape = RoundedCornerShape(size = cardRad)
        ) {
            Box(
                modifier = Modifier.fillMaxSize(1f)
            ) {
                Column(
                    modifier = Modifier
                        .padding(vertical = cardPadVer, horizontal = cardPadHor)
                ) {
                    Row(
                        modifier = Modifier.fillMaxWidth(1f),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(text = stringResource(id = R.string.plazaFrgTokenChartLabel))
                        Box(
                            modifier = Modifier.fillMaxWidth(1f),
                            contentAlignment = Alignment.CenterEnd
                        ) {
                            Surface(
                                modifier = Modifier.height(26.dp),
                                onClick = {
                                    isPeriodSelect.value = !isPeriodSelect.value
                                },
                                color = Color(0xffF4F5F7),
                                shape = RoundedCornerShape(7.dp)
                            ) {
                                Row(
                                    modifier = Modifier.padding(horizontal = 10.dp),
                                    verticalAlignment = Alignment.CenterVertically
                                ) {
                                    Text(text = "Show: ${selectedLabel.value}")
                                    Image(
                                        painter = (painterResource(id = R.drawable.ic_arrow_down)),
                                        contentDescription = "Arrow down"
                                    )
                                }
                            }
                        }
                    }
                    Spacer(Modifier.height(15.dp))
                    Card(
                        modifier = Modifier.fillMaxSize(),
                        backgroundColor = Color(0xffFFffFF),
                        shape = RoundedCornerShape(cardRad)
                    ) {
                        Box(
                            modifier = Modifier
                                .height(lineHeight)
                                .fillMaxWidth()
                                .padding(vertical = cardPadVer, horizontal = cardPadHor)
                        ) {
                            val color = android.graphics.Color.parseColor("#377DFF")
                            AndroidView(
                                modifier = Modifier.fillMaxSize(1f),
                                factory = {
                                    LineChart(it).apply {
                                        setupLineTokenChartStatistic(
                                            chart = this,
                                            color = color)
                                        val lists = mutableListOf(Entry())
                                        var pos = 0
                                        while (pos < 31) {
                                            lists.add(
                                                Entry(
                                                    pos.toFloat(),
                                                    Random.nextInt(10, 99).toFloat()
                                                )
                                            )
                                            pos++
                                        }
                                        data = getDataForLineChartSecTokenChart(
                                            count = selectedPosition,
                                            color = color) // LineData(dt)
                                    }
                                })
                        }
                    }
                }
                if(isPeriodSelect.value) {
                    Box(
                        modifier = Modifier
                            .fillMaxSize(1f)
                            .width(120.dp)
                            .padding(top = 6.dp, end = 8.dp),
                        contentAlignment = Alignment.TopEnd
                    ) {
                        Column() {
                            cards.EPeriod.getPeriods().forEachIndexed { i, v ->
                                Surface(
                                    modifier = Modifier.weight(1f),
                                    color = Color(0xffEBF2FF),
                                    shape = getShape(i),
                                    onClick = {
                                        selectedPosition.value = i
                                        selectedLabel.value = cards.EPeriod.getPeriods()[i]
                                        isPeriodSelect.value = false
                                    }
                                ) {
                                    Row(
                                        modifier = Modifier.padding(
                                            horizontal = 10.dp,
                                            vertical = 6.dp
                                        ),
                                        verticalAlignment = Alignment.CenterVertically
                                    ) {
                                        Image(
                                            painter = if (selectedPosition.value == i) painterResource(
                                                id = R.drawable.ic_checkbox_selected
                                            ) else painterResource(id = R.drawable.ic_checkbox_emty),
                                            contentDescription = "Selected"
                                        )
                                        Text(
                                            modifier = Modifier.padding(start = 10.dp),
                                            text = v
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

fun getShape(ind: Int): Shape{
    return when(ind){
        0 -> RoundedCornerShape(topStart = 5.dp, topEnd = 5.dp)
        cards.EPeriod.getPeriods().size -1 -> RoundedCornerShape(topStart = 5.dp, topEnd = 5.dp)
        else -> RoundedCornerShape(size = 0.dp)
    }
}

@Composable
fun FetchTokenPieBarChart(
    dataPieChart: MutableState<SetupBar>,
    dataBarChart: MutableState<List<BarDataSet>>
){
    Box(
        modifier = Modifier
            .fillMaxSize(1f)
            .padding(vertical = cardPadVer, horizontal = cardPadHor)
    ) {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = Color(0xffF8F9FA),
            shape = RoundedCornerShape(size = cardRad)
        ) {
            Column(
                Modifier.padding(all = cardPadHor)
            ) {
                ShowToolPieBarChart(dataPieChart)
                FetchTokenPieChart(dataPieChart)

                FetchTokenBarChart(dataBarChart)
            }
        }
    }
}

@Composable
fun FetchTokenPieChart(setup: MutableState<SetupBar>){
//    val sizePieChart = 120.dp
    val strokeRow = 12

    Box(
        modifier = Modifier
            .fillMaxSize(1f)
            .padding(all = rootPad)
    ) {
        Card(
            modifier = Modifier.fillMaxSize(),
            backgroundColor = Color(0xffFFFFFF),
            shape = RoundedCornerShape(12.dp)
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = cardPadVer, horizontal = cardPadHor)
            ) {
                Column() {
                    Box(
                        modifier = Modifier
                            .width(sizePieChart)
                            .height(sizePieChart),
                        contentAlignment = Alignment.Center
                    ) {
                        Canvas(
                            modifier = Modifier.fillMaxSize(1f),
                            onDraw = {
                                drawArc(
                                    color = Color(0xffF8F9FA),
                                    startAngle = 0f,
                                    sweepAngle = 360f,
                                    useCenter = false,
                                    style = Stroke(width = strokeRow.toFloat(), cap = StrokeCap.Round)
                                )
                            }
                        )
                        Canvas(
                            modifier = Modifier.fillMaxSize(1f),
                            onDraw = {
                                drawArc(
                                    color = colorIndicator[0],
                                    startAngle = 0f,
                                    sweepAngle = (360*(setup.value.statisticPeriod[Period.START]!!.percent.toFloat())/100).toFloat(),
                                    useCenter = false,
                                    style = Stroke(width = strokeRow.toFloat(), cap = StrokeCap.Round)
                                )
                            }
                        )
//                        PieChart(pieChartData = item.value[0].pieChartData)
                        Text(
                            text = setup.value.statisticPeriod[Period.START]!!.year,
                            style = StyleText.txtPieChart()
                        )
                    }
                    Text(
                        text = setup.value.statisticPeriod[Period.START]!!.value,
                        style = StyleText.txtPieLblBig()
                    )
                    Text(
                        text = setup.value.statisticPeriod[Period.START]!!.percent,
                        style = StyleText.txtPieChart()
                    )
                }
                Column(
                    modifier = Modifier
                        .background(Color.Cyan)
                        .fillMaxHeight(),
                    verticalArrangement = Arrangement.Center
                ) {
                    Text(
                        text = stringResource(id = R.string.pieDataChartText),
                        style = StyleText.txtPieChart()
                    )
                }
                Column(
                    horizontalAlignment = Alignment.End
                ) {
                    Box(
                        modifier = Modifier
                            .width(sizePieChart)
                            .height(sizePieChart),
                        contentAlignment = Alignment.Center
                    ) {
                        Canvas(
                            modifier = Modifier.fillMaxSize(1f),
                            onDraw = {
                                drawArc(
                                    color = Color(0xffF8F9FA),
                                    startAngle = 0f,
                                    sweepAngle = 360f,
                                    useCenter = false,
                                    style = Stroke(width = strokeRow.toFloat(), cap = StrokeCap.Round)
                                )
                            }
                        )
                        Canvas(
                            modifier = Modifier.fillMaxSize(1f),
                            onDraw = {
                                drawArc(
                                    color = colorIndicator[1],
                                    startAngle = 0f,
                                    sweepAngle = (360*(setup.value.statisticPeriod[Period.FINAL]!!.percent).toFloat()/100).toFloat(),
                                    useCenter = false,
                                    style = Stroke(width = strokeRow.toFloat(), cap = StrokeCap.Round)
                                )
                            }
                        )
//                        PieChart(pieChartData = item.value[1].pieChartData)
                        Text(
                            text = setup.value.statisticPeriod[Period.FINAL]!!.year,
                            style = StyleText.txtPieChart()
                        )
                    }

                    Text(
                        text = setup.value.statisticPeriod[Period.FINAL]!!.value,
                        style = StyleText.txtPieLblBig()
                    )
                    Text(
                        text = setup.value.statisticPeriod[Period.FINAL]!!.percent,
                        style = StyleText.txtPieLblLow()
                    )
                }
            }

        }
    }
}

@Composable
fun FetchTokenBarChart(item: MutableState<List<BarDataSet>>){
    Card(
        modifier = Modifier.fillMaxSize(),
        backgroundColor = Color(0xffFFFFFF),
        shape = RoundedCornerShape(12.dp)
    ){
        Box(modifier = Modifier
            .fillMaxSize(1f)
            .height(barHeight)
            .padding(all = cardRad)) {
            AndroidView(
                modifier = Modifier.fillMaxSize(1f),
                factory = {
                    BarChart(it).apply {
                        setupBarTokenChartStatistic(this)
//                        val col_a = android.graphics.Color.alpha(colorIndicator[0].value)
                        item.value[0].color = android.graphics.Color.parseColor("#FFAB00")
                        item.value[1].color = android.graphics.Color.parseColor("#38CB89")

                        data = BarData(item.value)
                    }
                }
            )
        }
    }
}

@Composable
fun ShowToolPieBarChart(setup: MutableState<SetupBar>) {
    Row(
        modifier = Modifier
            .fillMaxSize()
//            .background(Color.Blue)
            .padding(vertical = 8.dp),
    ){
        Column(
//            modifier = Modifier.background(Color.Red),
            verticalArrangement = Arrangement.Center
        ){
            Text(
                text = stringResource(id = R.string.frgPlazaTokenStatistic),
                style = StyleText.txtPgTkn(),
                modifier = Modifier.padding(bottom=4.dp))
            Text(
                text = "${setup.value.statisticPeriod[Period.START]!!.year} - ${setup.value.statisticPeriod[Period.FINAL]!!.year}",
                style = StyleText.txtCrdLim())
        }

        Box(
            modifier = Modifier
//                .background(Color.Green)
                .fillMaxWidth(),
            contentAlignment = Alignment.CenterEnd
        ){
            Column(
//                modifier = Modifier
//                    .background(Color.Yellow)
//                    .padding(start = 4.dp)
            ){
                Row(
                    modifier = Modifier
                        .background(color = Color.Transparent)
                        .padding(bottom = 2.dp)
                ){
                    Surface(
                        modifier = Modifier
                            .padding(0.dp)
                            .width(60.dp)
                            .background(color = Color.White, shape = RoundedCornerShape(5.dp)),
                        elevation = 2.dp
                    ){
                        Row(
                            modifier = Modifier
                                .padding(6.dp)
                                .background(color = Color.Transparent),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.End
                        ){
                            Box(
                                modifier = Modifier
                                    .width(sizeIndicator)
                                    .height(sizeIndicator)
                            ){
                                CircleIndicator(size = sizeIndicator, color = colorIndicator[0])
                            }
                            Text(
                                text = setup.value.statisticPeriod[Period.START]!!.year,
                                style = StyleText.txtCrdLim(),
                                modifier = Modifier.padding(start = 6.dp)
                            )
                        }
                    }
                }

                Row(
                    modifier = Modifier
                        .background(color = Color.Transparent)
                        .padding(top = 2.dp)
//                        .fillMaxWidth()
                ){
                    Surface(
                        modifier = Modifier
                            .padding(0.dp)
                            .width(60.dp)
                            .background(color = Color.White, shape = RoundedCornerShape(8.dp)),
                        elevation = 2.dp
                    ){
                        Row(
                            modifier = Modifier
                                .padding(6.dp)
                                .background(color = Color.Transparent),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.End
                        ){
                            Box(
                                modifier = Modifier
                                    .width(sizeIndicator)
                                    .height(sizeIndicator)
                            ){
                                CircleIndicator(size = sizeIndicator, color = colorIndicator[1])
                            }
                            Text(
                                text = setup.value.statisticPeriod[Period.FINAL]!!.year,
                                style = StyleText.txtCrdLim(),
                                modifier = Modifier.padding(start = 6.dp)
                            )
                        }
                    }
                }
            }
        }
    }
}


