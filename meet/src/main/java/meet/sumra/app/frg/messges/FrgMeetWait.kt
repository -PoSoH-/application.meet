//package meet.sumra.app.frg.messges
//
//import android.os.Bundle
//import android.view.View
//import android.widget.Toast
//import androidx.lifecycle.Observer
//import com.google.accompanist.pager.ExperimentalPagerApi
//import meet.sumra.app.R
//import meet.sumra.app.databinding.FrgMeetWaitBinding
//import meet.sumra.app.di.component.ComponentViewModel
//import meet.sumra.app.frg.FragmentBase
//import meet.sumra.app.interfaces.OnComposeListener
//import meet.sumra.app.vm.vm_home.StateMeet
//import meet.sumra.app.vm.vm_meet.ViewModelMeet
//import javax.inject.Inject
//import android.content.Intent
//
//
//
//
//class FrgMeetWait: FragmentBase<FrgMeetWaitBinding>() {
//
//    companion object{
//        val TAG = FrgMeetWait::class.java.simpleName
//        fun getInstance(): FragmentBase<*>{
//            return FrgMeetWait()
//        }
//    }
//
//    internal lateinit var viewModel: ViewModelMeet
//    @Inject set
//
//    @ExperimentalPagerApi
//    private fun setupUI(){
//        viewFragment.root.background = viewModel.fetchGradient()
////        viewFragment.toMeetWaitContainer.setContent { FetchWaitView(
////            onInvite = object: OnComposeListener{
////                override fun click(state: Boolean) {
//////                    viewModel.shareInviteLink()
////                    Toast.makeText(
////                        requireContext(),
////                        "Show Invite",
////                        Toast.LENGTH_SHORT).show()
////
////                    val share = Intent(Intent.ACTION_SEND)
////                    share.type = "text/plain"
////                    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
////
////                    // Add data to the intent, the receiving app will decide
////                    // what to do with it.
////
////                    // Add data to the intent, the receiving app will decide
////                    // what to do with it.
////                    share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post")
////                    share.putExtra(Intent.EXTRA_TEXT, "http://www.codeofaninja.com")
////
////                    startActivity(Intent.createChooser(share, "Share link!"))
////
////                }},
////            onMeetRun = object: OnComposeListener {
////                override fun click(state: Boolean) {
////                    viewModel.switchToMeetRun()
////                    Toast.makeText(
////                        requireContext(),
////                        "Meet Run",
////                        Toast.LENGTH_SHORT).show()
////                }},
////            context = requireContext(),
////            roomName = viewModel.fetchRoomInformation()
////        )}
//
////        viewModel.updateTypeFace().run {
////            viewFragment.frgContactMeetBar.typeface = this
////            viewFragment.frgAddMeetLabelTitle.typeface = this
////            viewFragment.frgAddMeetEditTitle.typeface = this
////            viewFragment.frgAddMeetLabelParticipants.typeface = this
////            viewFragment.frgAddMeetParticipantsLabel.typeface = this
////            viewFragment.frgAddMeetEmptyListLabel.typeface = this
////            viewFragment.frgAddMeetLabelLobby.typeface = this
////            viewFragment.frgAddMeetLabelDescription.typeface = this
////            viewFragment.frgAddMeetLabelPassword.typeface = this
////            viewFragment.frgAddMeetEnterPassword.typeface = this
////            viewFragment.frgAddMeetLabelSelectDateTime.typeface = this
////            viewFragment.frgAddMeetLabelTranslation.typeface = this
////            viewFragment.frgAddMeetLabelKey.typeface = this
////            viewFragment.frgAddMeetEditTranslationKey.typeface = this
////            viewFragment.frgAddMeetLabelNote.typeface = this
////            viewFragment.frgAddMeetEnterNote.typeface = this
////        }
//
//        /** recycler lists of participants **/
//
//    }
//
////    @Composable
////    private fun OnMeetRun(
////    onClick: (() -> Unit)? = {  }
////    ) {
////        Toast.makeText(requireContext(), "Meet Run", Toast.LENGTH_SHORT).show()
////    }
////
////    @Composable
////    fun OnInvite(
//////        onClick: (() -> Unit)? = { },
//////        consumeDownOnStart: Boolean = false,
//////        children: @Composable() () -> Unit
////    ) {
////        Toast.makeText(requireContext(), "Share Invite", Toast.LENGTH_SHORT).show()
////    }
//
//    /** View model observers... */
//    private fun stateToMeetWait() = Observer<StateMeet>{ meet ->
//        when(meet){
//            is StateMeet.Default -> {
//                viewModel.updateInitialData()
//            }
//            is StateMeet.SessionPst -> {
////                mRoomState.add(true)
//            }
//            is StateMeet.SessionPut -> {
////                mRoomState[0] = !mRoomState[0]
//            }
//            is StateMeet.RoomPut -> {}
//            is StateMeet.RoomDel -> {}
//        }
//    }
//
//    @ExperimentalPagerApi
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        viewModel.mViewState.observe(viewLifecycleOwner, stateToMeetWait())
////        viewModel.initMeetAdd()
////        viewModel.viewMeetAdd.observe(viewLifecycleOwner, stateMeetAdd())
//        setupUI()
////        addListener()
//    }
//
//    override fun onBackPressed(): Boolean {
//        return true
//    }
//
//    override fun getBinding(): FrgMeetWaitBinding {
//        return FrgMeetWaitBinding.inflate(layoutInflater)
//    }
//
//    override fun injectDependency(component: ComponentViewModel) {
//        component.inject(this)
//    }
//
//    override fun getEnterAnimation() = R.anim.anim_show_c_to_o
//    override fun getExitAnimation()  = R.anim.anim_hide_o_to_c
//
//}