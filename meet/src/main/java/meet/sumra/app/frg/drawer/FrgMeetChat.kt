package meet.sumra.app.frg.drawer

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import meet.sumra.app.databinding.FrgMeetChatBinding
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.frg.FragmentBase
import meet.sumra.app.vm.vm_home.ViewModelHome
import javax.inject.Inject

class FrgMeetChat: FragmentBase<FrgMeetChatBinding>() {

    companion object{
        val TAG = FrgMeetChat::class.java.simpleName
        fun getInstance(): Fragment{
            return FrgMeetChat()
        }
    }

    internal lateinit var viewModel: ViewModelHome
    @Inject set

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//
//    }



    private fun addListener(){
//        viewFragment.frgDashboardBackButton.setOnClickListener { button ->
//            onBackPressed()
//        }
    }

    override fun onBackPressed(): Boolean {
        return true
    }

    override fun getBinding(): FrgMeetChatBinding {
        return FrgMeetChatBinding.inflate(layoutInflater)
    }

    override fun injectDependency(component: ComponentViewModel) {
        component.inject(this)
    }
}