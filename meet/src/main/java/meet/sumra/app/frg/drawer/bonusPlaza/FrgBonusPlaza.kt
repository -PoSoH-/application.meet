package meet.sumra.app.frg.drawer.bonusPlaza

import android.os.Bundle
import android.view.View
import androidx.compose.foundation.*
import androidx.lifecycle.Observer
import meet.sumra.app.R
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.frg.FragmentBase
import meet.sumra.app.vm.vm_home.ViewModelHome
import javax.inject.Inject
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.lerp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.data.*
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.PagerState
import com.google.accompanist.pager.rememberPagerState
import kotlinx.coroutines.launch
import meet.sumra.app.data.configs.fixedContent
import meet.sumra.app.data.mdl.earnings.cards
import meet.sumra.app.data.mdl.plaza.*
import meet.sumra.app.data.configs.fixedContent.getAllRewardStatisticLegend
import meet.sumra.app.data.mdl.plaza.mock.Emulation
import meet.sumra.app.data.configs.settingsCharts.setupDivitBalanceChartSetup
import meet.sumra.app.data.configs.settingsChartsData.getDataForBarChartSecTokenChart
import meet.sumra.app.databinding.FrgBonusPlazaBinding
import meet.sumra.app.views.compose.ui_theme.StyleText
import meet.sumra.app.views.compose.ui_theme.ThemeSetup
import meet.sumra.app.views.compose.ui_theme.StyleText.txtPgLbl
import sdk.net.meet.LogUtil
import kotlin.math.absoluteValue
import kotlin.math.max

class FrgBonusPlaza: FragmentBase<FrgBonusPlazaBinding>() {

    companion object{
        val TAG = FrgBonusPlaza::class.java.simpleName
        fun getInstance(): FragmentBase<*>{
            return FrgBonusPlaza()
        }
    }

    private val txtAppBarStyle = TextStyle(
        fontSize = 12.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.SemiBold)),
        textAlign = TextAlign.Center,
        color = ThemeSetup.getTheme().cTextAddMeet )

    private val txtTabRow = TextStyle(
        fontSize = 16.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.SemiBold)),
        textAlign = TextAlign.Center,
        color = ThemeSetup.getTheme().cPlazaText )

    private val txtPageLabel = TextStyle(
        fontSize = 16.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.SemiBold)),
        textAlign = TextAlign.Center,
        color = ThemeSetup.getTheme().cTextAddMeet )

    private val txtCardLabel = TextStyle(
        fontSize = 13.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.SemiBold)),
        textAlign = TextAlign.Center,
        color = ThemeSetup.getTheme().cTextAddMeet )

    private val txtCardBase = TextStyle(
        fontSize = 30.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.SemiBold)),
        textAlign = TextAlign.Center,
        color = ThemeSetup.getTheme().cTextAddMeet )

/**  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  ** **  **/

    private val txtCardListLbl = TextStyle(
        fontSize = 14.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.SemiBold)),
        textAlign = TextAlign.Center,
        color = ThemeSetup.getTheme().cTextAddMeet )
    private val txtCardListDr = TextStyle(
        fontSize = 13.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.SemiBold)),
        textAlign = TextAlign.Center,
        color = ThemeSetup.getTheme().cTextAddMeet )
    private val txtCardListAllDr = TextStyle(
        fontSize = 30.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.SemiBold)),
        textAlign = TextAlign.Center,
        color = ThemeSetup.getTheme().cTextAddMeet )
    private val txtCardListAllType = TextStyle(
        fontSize = 13.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.SemiBold)),
        textAlign = TextAlign.Center,
        color = ThemeSetup.getTheme().cTextAddMeet )

//    private lateinit var rewardStatisticChart: BarChart
    private val totalBalance = mutableStateOf(mutableListOf(TotalBalance("0.0", "")))

    internal lateinit var viewModel: ViewModelHome
    @Inject set

    @ExperimentalMaterialApi
    @ExperimentalPagerApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.initBonusPlaza()
        viewModel.viewStatePlaza.observe(viewLifecycleOwner, stateBonusPlaza())
        setupUI()
    }

    override fun onBackPressed(): Boolean {
        return true
    }
    override fun getBinding(): FrgBonusPlazaBinding {
        return FrgBonusPlazaBinding.inflate(layoutInflater)
    }
    override fun injectDependency(component: ComponentViewModel) {
        component.inject(this)
    }
/** state and setup functions **  **  **  **  **  **  **  **/
    @ExperimentalMaterialApi
    @ExperimentalPagerApi
    private fun setupUI(){
//        rewardStatisticChart = BarChart(requireContext())
//        setupDivitBalanceChartSetup(rewardStatisticChart, viewModel.updateTypeFace())
//        rewardStatisticChart.data = barChartData.value
//        rewardStatisticChart.invalidate()
        viewFragment.baseBonusPlazaView.apply {
            setContent { GetViewPagerFirst() }
        }
    }

    private var legend: List<fixedContent.RewardStatisticLegend>? = fixedContent.getAllRewardStatisticLegend()

    private var bonusPlaza: MutableState<PlazaRewardBonus> = mutableStateOf(PlazaRewardBonus())
    private var labelDrawer  = Emulation.labelDrawer
    private var barChartData = mutableStateOf(BarData())
    private var selectedIndex = mutableStateOf(0)
    private var chartHeight  = 10

    private var tokenStatisticLin = mutableStateOf(LineData())
    private var tokenStatisticPie = mutableStateOf(SetupBar(mutableMapOf(Pair(Period.START, Statistic()), Pair(Period.FINAL, Statistic()))))
    private var tokenStatisticBar = mutableStateOf<List<BarDataSet>>(getDataForBarChartSecTokenChart())

    private var rewardHowTo = mutableStateOf(mutableListOf<HowToSection>())

    private fun stateBonusPlaza() = Observer<ViewModelHome.StateBonusPlaza> { state ->
        when(state) {
            is ViewModelHome.StateBonusPlaza.BonusPlazaDft -> {
//                barChartData.v Emulation.barChartData
            }
            is ViewModelHome.StateBonusPlaza.RewardBonusGet -> {
                bonusPlaza.value = state.body
            }
            is ViewModelHome.StateBonusPlaza.RewardChartGet -> {
                chartHeight = 300
                barChartData.value = //state.body.data

                    BarData( listOf(
                        BarDataSet(mutableListOf(BarEntry( 0F, floatArrayOf(12.5F, 1.5F, 21.5F,  5.4F))), "lbl1"),
                        BarDataSet(mutableListOf(BarEntry( 1F, floatArrayOf(14.5F, 7.5F, 15.5F, 10.4F))), "lbl2"),
                        BarDataSet(mutableListOf(BarEntry( 2F, floatArrayOf(16.5F, 6.5F, 32.5F, 15.4F))), "lbl3"),
                        BarDataSet(mutableListOf(BarEntry( 3F, floatArrayOf(11.5F, 3.5F, 16.5F,  9.4F))), "lbl4"),
                        BarDataSet(mutableListOf(BarEntry( 4F, floatArrayOf(18.5F, 5.5F, 18.5F,  2.4F))), "lbl5"),
                        BarDataSet(mutableListOf(BarEntry( 5F, floatArrayOf( 9.5F, 8.5F, 26.5F, 16.4F))), "lbl6"),
                        BarDataSet(mutableListOf(BarEntry( 6F, floatArrayOf(12.5F, 1.5F, 21.5F,  5.4F))), "lbl1"),
                        BarDataSet(mutableListOf(BarEntry( 1F, floatArrayOf(14.5F, 7.5F, 15.5F, 10.4F))), "lbl2"),
                        BarDataSet(mutableListOf(BarEntry( 7F, floatArrayOf(16.5F, 6.5F, 32.5F, 15.4F))), "lbl3"),
                        BarDataSet(mutableListOf(BarEntry( 8F, floatArrayOf(11.5F, 3.5F, 16.5F,  9.4F))), "lbl4"),
                        BarDataSet(mutableListOf(BarEntry( 9F, floatArrayOf(18.5F, 5.5F, 18.5F,  2.4F))), "lbl5"),
                        BarDataSet(mutableListOf(BarEntry(10F, floatArrayOf( 9.5F, 8.5F, 26.5F, 16.4F))), "lbl6"),
                        BarDataSet(mutableListOf(BarEntry(11F, floatArrayOf(12.5F, 1.5F, 21.5F,  5.4F))), "lbl1"),
                        BarDataSet(mutableListOf(BarEntry(12F, floatArrayOf(14.5F, 7.5F, 15.5F, 10.4F))), "lbl2"),
                        BarDataSet(mutableListOf(BarEntry(13F, floatArrayOf(16.5F, 6.5F, 32.5F, 15.4F))), "lbl3"),
                        BarDataSet(mutableListOf(BarEntry(14F, floatArrayOf(11.5F, 3.5F, 16.5F,  9.4F))), "lbl4"),
                        BarDataSet(mutableListOf(BarEntry(15F, floatArrayOf(18.5F, 5.5F, 18.5F,  2.4F))), "lbl5"),
                        BarDataSet(mutableListOf(BarEntry(16F, floatArrayOf( 9.5F, 8.5F, 26.5F, 16.4F))), "lbl6")
                    )) // state.body.data

                totalBalance.value.clear()
                totalBalance.value.add(state.stat)
//                rewardStatisticChart.invalidate()
            }
            is ViewModelHome.StateBonusPlaza.TokenChartBarGet -> {
                tokenStatisticBar.value = state.body[0]
            }
            is ViewModelHome.StateBonusPlaza.TokenChartPieGet -> {
                tokenStatisticPie.value = state.body[0]
            }
            is ViewModelHome.StateBonusPlaza.TokenChartLinGet -> {
                tokenStatisticLin.value = state.body[0]
            }
            is ViewModelHome.StateBonusPlaza.HowToRewardGet -> {
                rewardHowTo.value = state.body.characters
            }
        }
    }
/**  **  **  **  **  **  **  **  **  **    compose functions    **  **  **  **  **  **  **  **  **/
    @ExperimentalMaterialApi
    @ExperimentalPagerApi
    @Composable
    private fun GetViewPagerFirst(){
        val rootBrush = Brush.horizontalGradient(listOf(
            Color(0xff02C2FF),
            Color(0xff0E6AE3)))

        Box(
            modifier = Modifier
                .fillMaxSize(1f)
                .background(brush = rootBrush)
        ){
            Column(
                modifier = Modifier
                    .fillMaxSize(1f)
                    .background(Color.Transparent)
            ) {
                /** toolbar **/
                ToolBar(
                    label = stringResource(id = R.string.plazaAllViewToolbar),
                    brush = rootBrush )
                /** content **/
                val pages = remember { viewModel.fetchAllTabBonusPlaza() }
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(color = Color.Transparent)
                ) {
                    val coroutineScope = rememberCoroutineScope()
                    // Remember a PagerState with our tab count
                    val pagerState = rememberPagerState(pageCount = pages.size)
                    ScrollableTabRow(
                        // Our selected tab is our current page
                        selectedTabIndex = pagerState.currentPage,
                        indicator = { tabPositions ->
                            TabRowDefaults.Indicator(
                                Modifier.pagerTabIndicatorOffset(pagerState, tabPositions)
                            )
                        },
                        backgroundColor = Color.Transparent, // ThemeSetup.getTheme().cPlazaTabBackground,
                        contentColor = Color(0xff323B4B)
                    ) {
                        // Add tabs for all of our pages
                        pages.forEachIndexed { index, title ->
                            LeadingIconTab(
                                icon = {
                                    Icon(
                                        painter = painterResource(id = title.icon),
                                        contentDescription = "",
                                        tint = Color(0xff323B4B)
                                    )
                                },
                                text = {
                                    Text(
                                        text = stringResource(id = title.text),
                                        style = txtTabRow)},
                                selected = pagerState.currentPage == index,
                                onClick = {
                                    coroutineScope.launch {
                                        pagerState.animateScrollToPage(index)
                                    }
                                },
                                selectedContentColor = Color.Transparent,
                                unselectedContentColor = Color(0xff323B4B)
                            )
                        }
                    }

                    HorizontalPager(
                        state = pagerState,
                        modifier = Modifier
                            .fillMaxWidth()
                            .weight(1f)
                            .background(Color.Transparent)
                    ) { page ->
                        when (page) {
                            0 -> { GetPageDivitRewardBonus() }
                            1 -> { GetBoxRewardsBonusChart() }
                            2 -> { GetBoxDivitsTokensChart() }
                            3 -> { GetBoxHowToGetRewards() }
                        }
                    }
                }
            }
        }
    }

    private fun onClick(){
        LogUtil.info("Arrow button back")
        onBackPressed()
    }

    @ExperimentalMaterialApi
    @Composable
    private fun ToolBar(label: String, brush: Brush){
        Row(
            modifier = Modifier
                .fillMaxWidth(1f)
                .height(dimensionResource(id = R.dimen.toolBarHeight))
                .background(brush = brush),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Surface(
                onClick = { viewModel.onBackClickListener() },
                modifier = Modifier
                    .height(dimensionResource(id = R.dimen.toolBarHeight))
                    .width(dimensionResource(id = R.dimen.toolBarHeight)),
                color = Color.Transparent
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(start = 8.dp),
                    contentAlignment = Alignment.Center
                ) {
                    Image(
                        modifier = Modifier
                            .height(24.dp)
                            .width(24.dp),
                        painter = painterResource(id = meet.sumra.app.R.drawable.ic_arrow_back_ios),
                        contentDescription = "Button back",
                        colorFilter = ColorFilter.tint(color = Color(0xffFFFFFF)) //, blendMode = BlendMode.Src)
                    )
                }
            }
            val txtPad = 16
            Text(
                modifier = Modifier
                    .weight(1f)
                    .padding(start = txtPad.dp, end = txtPad.dp),
                text = label,
                style = StyleText.txtEarnCardLbl()
            )
        }
    }

    @Preview
    @Composable
    @ExperimentalPagerApi
    @ExperimentalMaterialApi
    fun PreviewFragments(){
        GetViewPagerFirst()
    }

    @ExperimentalPagerApi
    fun Modifier.pagerTabIndicatorOffset(
        pagerState: PagerState,
        tabPositions: List<TabPosition>,
    ): Modifier = composed {
        // If there are no pages, nothing to show
        if (pagerState.pageCount == 0) return@composed this

        val targetIndicatorOffset: Dp
        val indicatorWidth: Dp
        val currentTab = tabPositions[pagerState.currentPage]
        val targetPage = pagerState.targetPage
        val targetTab  = targetPage?.let { tabPositions.getOrNull(it) }

        if (targetTab != null) {
            // The distance between the target and current page. If the pager is animating over many
            // items this could be > 1
            val targetDistance = (targetPage - pagerState.currentPage).absoluteValue
            // Our normalized fraction over the target distance
            val fraction = (pagerState.currentPageOffset/max(targetDistance, 1)).absoluteValue

            targetIndicatorOffset = lerp(currentTab.left, targetTab.left, fraction)
            indicatorWidth = lerp(currentTab.width, targetTab.width, fraction).absoluteValue
        } else {
            // Otherwise we just use the current tab/page
            targetIndicatorOffset = currentTab.left
            indicatorWidth = currentTab.width
        }

        fillMaxWidth()
            .wrapContentSize(Alignment.BottomStart)
            .offset(x = targetIndicatorOffset)
            .width(indicatorWidth)
    }

    @ExperimentalMaterialApi
    @Composable
    private fun GetPageDivitRewardBonus(){
        viewModel.fetchRewardBonus()
        viewModel.fetchRewardChart(selectedIndex.value)

        val scrollState = ScrollState(initial = 1)
        Column(modifier = Modifier
            .fillMaxSize()
            .background(
                color = Color(0xffFAF9FC),
                shape = RoundedCornerShape(topStart = 25.dp, topEnd = 25.dp)
            )
            .verticalScroll(state = scrollState, enabled = true)
        ){
            Column(modifier = Modifier
                .fillMaxSize()
                .padding(start = 20.dp, end = 20.dp, top = 30.dp, bottom = 30.dp)
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(
                            color = Color(0xffFFffFF),
                            shape = RoundedCornerShape(size = 10.dp)
                        )
                        .padding(top = 15.dp, bottom = 15.dp, start = 10.dp, end = 10.dp)
                ) {
                    Text(
                        modifier = Modifier.padding(16.dp),
                        text = "Divits Reward Bonus",
                        style = txtPgLbl()
                    )
                    Card(
                        modifier = Modifier.padding(0.dp),
                        backgroundColor = ThemeSetup.getTheme().cCardPlzColorBlue,
                        shape = RoundedCornerShape(10.dp),
                        elevation = 2.dp
                    ) {
                        Column(
                            modifier = Modifier.fillMaxWidth()
                        ) {
                            Text(
                                text = "You total Divits Reward",
                                style = txtCardLabel,
                                modifier = Modifier.padding(all = 16.dp)
                            )
                            Box(
                                modifier = Modifier
                                    .fillMaxSize(),
                                contentAlignment = Alignment.Center
                            ) {
                                Row(
                                    modifier = Modifier
                                        .fillMaxSize()
                                        .padding(
                                            start = 0.dp,
                                            top = 10.dp,
                                            end = 0.dp,
                                            bottom = 14.dp
                                        ),
                                    horizontalArrangement = Arrangement.Center
                                ) {
                                    Image(
                                        modifier = Modifier
                                            .width(54.dp)
                                            .height(54.dp),
                                        painter = painterResource(id = R.drawable.ic_plaza_bonus_gradient),
                                        contentDescription = null
                                    )
                                }
                                Row(
                                    modifier = Modifier
                                        .fillMaxSize(),
                                    horizontalArrangement = Arrangement.Center,
                                    verticalAlignment = Alignment.CenterVertically
                                ) {
                                    Text(
                                        text = "${bonusPlaza.value.total.value}",
                                        style = txtCardBase,
                                        modifier = Modifier.padding(
                                            start = 0.dp,
                                            top = 0.dp,
                                            end = 0.dp,
                                            bottom = 3.dp
                                        ),
                                    )
                                    Text(
                                        text = "${bonusPlaza.value.total.currency}",
                                        style = txtCardBase,
                                        modifier = Modifier.padding(
                                            start = 3.dp,
                                            top = 0.dp,
                                            end = 0.dp,
                                            bottom = 0.dp
                                        ),
                                    )
                                }
                            }
                        }
                    }
                    Spacer(modifier = Modifier.height(5.dp))
                    bonusPlaza.value.dailyBonus?.let {
                        it.forEach { item ->
                            GetCardListShow(item)
                        }
                    }
                }
            }
            GetBoxRewardsStatisticChart()
        }
    }

    @Composable
    private fun GetCardListShow(item: DailyBonus){

        var id: Int = R.drawable.ic_placeholder
        var colorT = Color.Transparent
        var colorC = Color.Transparent
        var colorR = Color.Transparent
        when(item.type){
            EDailyBonus.type_daily_bonus.toString() -> {
                id = R.drawable.ic_plaza_daily_bonus
                colorT = ThemeSetup.getDailyBonus().cType_daily_bonus
                colorC = Color(0xffF5FCF9)
                colorR = Color(0xffE5F6EF)
            }
            EDailyBonus.type_commercial.toString() -> {
                id = R.drawable.ic_plaza_commercial_shield
                colorT = ThemeSetup.getDailyBonus().cType_commercial
                colorC = Color(0xffFFFBF1)
                colorR = Color(0xffFDF4E0)
            }
            EDailyBonus.type_sumra_id.toString() -> {
                id = R.drawable.ic_plaza_sumra_id
                colorT = ThemeSetup.getDailyBonus().cType_sumra_id
                colorC = Color(0xffF4F8FF)
                colorR = Color(0xffEBF2FF)
            }
            EDailyBonus.type_mobile_app.toString() -> {
                id = R.drawable.ic_plaza_mobile_app
                colorT = ThemeSetup.getDailyBonus().cType_mobile_app
                colorC = Color(0xffFFF6F4)
                colorR = Color(0xffFFEFEB)
            }
            EDailyBonus.type_publishing_to.toString() -> {
                id = R.drawable.ic_plaza_token_share
                colorT = ThemeSetup.getDailyBonus().cType_publishing_to
                colorC = Color(0xffF5FCF9)
                colorR = Color(0xffE5F6EF)
            }
            EDailyBonus.type_publishing_with.toString() -> {
                id = R.drawable.ic_plaza_product_tests
                colorT = ThemeSetup.getDailyBonus().cType_publishing_with
                colorC = Color(0xffF4F8FF)
                colorR = Color(0xffEBF2FF)
            }
            EDailyBonus.type_surveys.toString() -> {
                id = R.drawable.ic_plaza_token_folder
                colorT = ThemeSetup.getDailyBonus().cType_surveys
                colorC = Color(0xffFFFBF1)
                colorR = Color(0xffFDF4E0)
            }
            EDailyBonus.type_reviews.toString() -> {
                id = R.drawable.ic_plaza_services_rewiews
                colorT = ThemeSetup.getDailyBonus().cType_reviews
                colorC = Color(0xffFFF6F4)
                colorR = Color(0xffFFEFEB)
            }
        }

        Column(
            modifier = Modifier.padding(vertical = 10.dp)
        ) {
            Card(
                modifier = Modifier.padding(0.dp),
                backgroundColor = colorC, //ThemeSetup.getTheme().cCardPlzColorWhite,
                shape = RoundedCornerShape(10.dp),
                elevation = 2.dp
            ) {
                Column(
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Text(
                        text = "${item.title}", // You total Divits Reward",
                        style = txtCardListLbl,
                        modifier = Modifier.padding(16.dp, 8.dp, 16.dp, 8.dp)
                    )
                    Row(
                        modifier = Modifier
                            .fillMaxSize(),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Column(
                            modifier = Modifier.padding(
                                start = 16.dp, end = 16.dp, top = 16.dp, bottom = 16.dp
                            ),
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Row(
//                            modifier = Modifier.background(color = Color.Blue),
                                horizontalArrangement = Arrangement.Center
                            ) {
                                val sizeRound = 54
                                Box(
                                    modifier = Modifier
                                        .size(sizeRound.dp)
                                        .background(
                                            color = colorR,
                                            shape = RoundedCornerShape((sizeRound).dp)
                                        ),
                                    contentAlignment = Alignment.Center
                                ) {
                                    Image(
                                        modifier = Modifier
                                            .width(30.dp)
                                            .height(30.dp),
                                        painter = painterResource(id = id),
                                        contentDescription = null,
                                    )
                                }
                            }
                            Row(
//                            modifier = Modifier.background(color = Color.Yellow),
                                horizontalArrangement = Arrangement.Center
                            ) {
                                val style = TextStyle(
                                    fontSize = 13.sp,
                                    fontFamily = FontFamily(
                                        Font(
                                            R.font.font_dm_sans_family,
                                            FontWeight.SemiBold
                                        )
                                    ),
                                    textAlign = TextAlign.Center,
                                    color = colorT
                                )
                                Text(
                                    text = "+",
                                    style = style,
                                    modifier = Modifier.padding(
                                        start = 0.dp,
                                        top = 0.dp,
                                        end = 0.dp,
                                        bottom = 3.dp
                                    ),
                                )
                                Text(
                                    text = "${item.value}", //"250",
                                    style = style,
                                    modifier = Modifier.padding(
                                        start = 0.dp,
                                        top = 0.dp,
                                        end = 0.dp,
                                        bottom = 3.dp
                                    ),
                                )
                                Text(
                                    text = "${item.currency}", //"DR",
                                    style = style,
                                    modifier = Modifier.padding(
                                        start = 3.dp,
                                        top = 0.dp,
                                        end = 0.dp,
                                        bottom = 0.dp
                                    ),
                                )
                            }
                        }

                        Column(
                            modifier = Modifier
                                .padding(
                                    start = 16.dp,
                                    top = 16.dp,
                                    end = 16.dp,
                                    bottom = 16.dp
                                )
                                .fillMaxWidth(),
//                            .background(color = Color.Green),
                            horizontalAlignment = Alignment.End
                        ) {
//                        Column(
//                            modifier = Modifier.background(color = Color.Red),
//                            horizontalAlignment = Alignment.CenterHorizontally
//                        ){
                            Row(
//                                modifier = Modifier.background(color = Color.Cyan),
                                horizontalArrangement = Arrangement.Center
                            ) {
                                Text(
                                    text = "${item.value}", //"250",
                                    style = txtCardListAllDr,
                                    modifier = Modifier.padding(
                                        start = 0.dp,
                                        top = 0.dp,
                                        end = 0.dp,
                                        bottom = 3.dp
                                    ),
                                )
                                Text(
                                    text = "${item.currency}", //"DR",
                                    style = txtCardListAllDr,
                                    modifier = Modifier.padding(
                                        start = 3.dp,
                                        top = 0.dp,
                                        end = 0.dp,
                                        bottom = 0.dp
                                    ),
                                )
                            }
                            Row(
//                                modifier = Modifier.background(color = Color.Blue),
                                horizontalArrangement = Arrangement.Center
                            ) {
                                Text(
                                    text = "${item.message}", //"Your bonus",
                                    style = txtCardListAllType,
                                    modifier = Modifier.padding(
                                        start = 3.dp,
                                        top = 0.dp,
                                        end = 0.dp,
                                        bottom = 0.dp
                                    ),
                                )
                            }
//                        }
                        }
                    }
                }
            }
        }
    }

    @ExperimentalMaterialApi
    @Composable
    private fun GetBoxRewardsStatisticChart() {
        val list = cards.EPeriod.getPeriods()
        var expanded by remember { mutableStateOf(false) }

        val txtRewStatLbl = TextStyle(
                    fontSize = 16.sp,
                    fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.SemiBold)),
                    textAlign = TextAlign.Start,
                    color = Color(color = 0xff323B4B))
        val txtRewStatTxtGr = TextStyle(
                    fontSize = 13.sp,
                    fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.SemiBold)),
                    textAlign = TextAlign.Center,
                    color = Color(color = 0xffB0B7C3))
        val txtRewStatTxtGl = TextStyle(
                    fontSize = 13.sp,
                    fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.SemiBold)),
                    textAlign = TextAlign.Center,
                    color = Color(color = 0xff4E5D78))
        val txtRewStatTxtBl = TextStyle(
                    fontSize = 24.sp,
                    fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.SemiBold)),
                    textAlign = TextAlign.Center,
                    color = Color(color = 0xff323B4B))

        val padHor = 20.dp
        val padVer = 15.dp
        val padCardHor = 10.dp
        val padCardVer = 15.dp

        Box(modifier = Modifier
            .fillMaxSize()
//            .padding(horizontal = padHor, vertical = padVer)
        ){
            Row(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(horizontal = padHor, vertical = 0.dp)
                    .background(
                        color = Color.White,
                        shape = RoundedCornerShape(10.dp)
                    )
            ) {
                Column(
                    modifier = Modifier.fillMaxSize()
                        .padding(horizontal = padCardHor, vertical = padVer)
                ) {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(
                            text = "Divits rewards statistic",
                            style = txtRewStatLbl,
                            modifier = Modifier.fillMaxHeight(),
                        )
                        Column(
                            modifier = Modifier
                                .fillMaxWidth(1f)
                                .padding(bottom = 8.dp),
                            horizontalAlignment = Alignment.End
                        ) {
                            Surface(
                                onClick = { expanded = !expanded },
                                modifier = Modifier.height(24.dp),
                                color = if(expanded) Color(0xffF8FfFf)else(Color(0xffF4F5F7)),
                                shape = RoundedCornerShape(5.dp)
                            ) {
                                Row(
                                    modifier = Modifier
                                        .fillMaxHeight(1f)
                                        .padding(horizontal = 6.dp),
                                    verticalAlignment = Alignment.CenterVertically
                                ) {
                                    Text(
                                        text = "Show:",
                                        style = txtRewStatTxtGr,
                                        modifier = Modifier
                                            .padding(
                                                start = 0.dp, top = 0.dp,
                                                end = 5.dp, bottom = 0.dp)
                                    )
                                    Text(
                                        text = list[selectedIndex.value],
                                        style = txtRewStatTxtGl,
                                        modifier = Modifier.padding(
                                            start = 5.dp, top = 0.dp,
                                            end = 5.dp,   bottom = 0.dp
                                        ),
                                    )
                                    Image(
                                        modifier = Modifier.size(12.dp),
                                        painter = painterResource(id = R.drawable.ic_arrow_down),
                                        contentDescription = "Arrow down",
                                        colorFilter = ColorFilter.tint(Color(0xffB0B7C3)))
                                }
                            }

//                            Box(
//                                modifier = Modifier
//                                    .width(150.dp)
//                                    .wrapContentSize(Alignment.BottomEnd)
//                            ) {
//                                DropdownMenu(
//                                    expanded = expanded,
//                                    onDismissRequest = { expanded = false },
//                                    modifier = Modifier.background(Color.LightGray)
//                                ) {
//                                    list.forEachIndexed { index, s ->
//                                        DropdownMenuItem(onClick = {
//                                            selectedIndex.value = index
//                                            expanded = false
//                                            viewModel.fetchRewardChart(index)
//                                            // call function request update type lists
//                                        }) {
//                                            Text(
//                                                modifier = Modifier.padding(
//                                                    start = 8.dp,
//                                                    end = 8.dp, top = 2.dp, bottom = 2.dp
//                                                ),
//                                                text = s,
//                                                style = txtRewStatTxtGl
//                                            )
//                                        }
//                                        if (index >= 0 && index < list.size - 1) {
//                                            Divider(
//                                                modifier = Modifier.padding(
//                                                    start = 8.dp,
//                                                    end = 8.dp
//                                                ),
//                                                color = Color.DarkGray,
//                                                thickness = 1.dp
//                                            )
//                                        }
//                                    }
//                                }
//                            }
                        }
                    }
                    Box(
                        modifier = Modifier.fillMaxSize(1f)
                    ) {
                        Card(
                            modifier = Modifier.fillMaxSize(1f),
                            backgroundColor = Color(0xffF8F9FA),
                            shape = RoundedCornerShape(5.dp)
                        ) {
                            Column (
                                modifier = Modifier
                                    .padding(vertical = padCardVer, horizontal = padCardHor)
                            ){
                                Text(
                                    text = "TOTAL DIVITS BALANCE",
                                    style = txtRewStatTxtGr,
                                )
                                Row(modifier = Modifier.fillMaxWidth()) {
                                    Text(
                                        text = totalBalance.value[0].value,
                                        style = txtRewStatTxtBl,
                                        modifier = Modifier.padding(
                                            start = 0.dp,
                                            top = 0.dp,
                                            end = 5.dp,
                                            bottom = 0.dp
                                        ),
                                    )
                                    Text(
                                        text = totalBalance.value[0].currency,
                                        style = txtRewStatTxtBl,
                                        modifier = Modifier.padding(
                                            start = 5.dp,
                                            top = 0.dp,
                                            end = 0.dp,
                                            bottom = 0.dp
                                        ),
                                    )
                                }

                                Divider(
                                    modifier = Modifier.padding(top = 5.dp, bottom = 10.dp),
                                    color = Color.DarkGray,
                                    thickness = 1.dp
                                )

                                Box(
                                    modifier = Modifier
                                        .padding(top = 5.dp, bottom = 5.dp)
                                        .height(250.dp)
                                ) {
                                    AndroidView(
                                        modifier = Modifier
                                            .fillMaxSize(1f)
                                            .background(color = Color.Transparent),
                                        factory = {
                                            val dt = BarDataSet(mutableListOf(
                                                BarEntry( 0F, floatArrayOf(12.5F, 1.5F, 21.5F,  5.4F)),
                                                BarEntry( 1F, floatArrayOf(14.5F, 7.5F, 15.5F, 10.4F)),
                                                BarEntry( 2F, floatArrayOf(16.5F, 6.5F, 32.5F, 15.4F)),
                                                BarEntry( 3F, floatArrayOf(11.5F, 3.5F, 16.5F,  9.4F)),
                                                BarEntry( 4F, floatArrayOf(18.5F, 5.5F, 18.5F,  2.4F)),
                                                BarEntry( 5F, floatArrayOf( 9.5F, 8.5F, 26.5F, 16.4F)),
                                                BarEntry( 6F, floatArrayOf(12.5F, 1.5F, 21.5F,  5.4F)),
                                                BarEntry( 1F, floatArrayOf(14.5F, 7.5F, 15.5F, 10.4F)),                                                 BarEntry( 7F, floatArrayOf(16.5F, 6.5F, 32.5F, 15.4F)),
                                                BarEntry( 8F, floatArrayOf(11.5F, 3.5F, 16.5F,  9.4F)),
                                                BarEntry( 9F, floatArrayOf(18.5F, 5.5F, 18.5F,  2.4F)),
                                                BarEntry(10F, floatArrayOf( 9.5F, 8.5F, 26.5F, 16.4F)),
                                                BarEntry(11F, floatArrayOf(12.5F, 1.5F, 21.5F,  5.4F)),
                                                BarEntry(12F, floatArrayOf(14.5F, 7.5F, 15.5F, 10.4F)),
                                                BarEntry(13F, floatArrayOf(16.5F, 6.5F, 32.5F, 15.4F)),
                                                BarEntry(14F, floatArrayOf(11.5F, 3.5F, 16.5F,  9.4F)),
                                                BarEntry(15F, floatArrayOf(18.5F, 5.5F, 18.5F,  2.4F)),
                                                BarEntry(16F, floatArrayOf( 9.5F, 8.5F, 26.5F, 16.4F)),
                                            ), "")
                                            dt.colors = listOf(
                                                android.graphics.Color.parseColor("#FFAB00"),
                                                android.graphics.Color.parseColor("#38CB89"),
                                                android.graphics.Color.parseColor("#FF5630"),
                                                android.graphics.Color.parseColor("#377DFF"))
//                                            dt.setStackLabels(arrayOf("0x377DFF0","0x377DFF", "0x377DFF", "0x377DFF"))
                                            BarChart(it).apply {
                                                data = BarData(dt)
                                                setupDivitBalanceChartSetup(this)
                                            }
                                    })
                                }
                                Spacer(modifier = Modifier
                                    .fillMaxWidth(1f)
                                    .height(10.dp))
                                if(legend.isNullOrEmpty()){
                                    legend = getAllRewardStatisticLegend()
                                }
                                legend!!.forEachIndexed {i, c ->
                                    GetLegendStatistic(
                                        indicatorColor = c.color,
                                        labelLegend = stringResource(id = c.label),
                                        textStyle = txtRewStatTxtGr
                                    )
                                    if(i<legend!!.size-1){
                                        Spacer(modifier = Modifier
                                            .height(5.dp)
                                            .fillMaxWidth(1f)) }
                                }
                                /** Share on block... **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **/
                                val buttonShareHeight = 48
                                Surface(
                                    onClick = {},
                                    modifier = Modifier.height(buttonShareHeight.dp),
                                    color = Color(0xff377DFF),
                                    shape = RoundedCornerShape(size = (buttonShareHeight/2).dp)
                                ) {
                                    Row(
                                        modifier = Modifier
                                            .padding(end = 16.dp),
                                        verticalAlignment = Alignment.CenterVertically
                                    ) {
                                        Box(
                                            modifier = Modifier
                                                .width(buttonShareHeight.dp)
                                                .height(buttonShareHeight.dp),
                                            contentAlignment = Alignment.Center
                                        ) {
                                            //                            CircleIndicator(48.dp, ThemeSetup.getTheme().cCardPlzColorBlue)
                                            Image(
                                                modifier = Modifier
                                                    .width(20.dp)
                                                    .height(20.dp),
                                                painter = painterResource(id = R.drawable.ic_plaza_share_on),
                                                contentDescription = null
                                            )
                                        }
                                        Text(
                                            text = stringResource(R.string.plazaShareText),
                                            style = txtCardLabel,
                                            modifier = Modifier.padding(
                                                start = 8.dp,
                                                top = 0.dp,
                                                end = 0.dp,
                                                bottom = 0.dp
                                            )
                                        )
                                    }
                                }
                            }
                        }
                        if(expanded) {
                            Box(
                                modifier = Modifier
                                    .fillMaxWidth(1f)
                                    .padding(vertical = 8.dp, horizontal = 8.dp),
                                contentAlignment = Alignment.TopEnd
                            ) {
                                Column (
                                    modifier = Modifier
                                        .background(
                                            color = Color.White,
                                            shape = RoundedCornerShape(5.dp))
                                ) {
                                    list.forEachIndexed { i, t ->
                                        Surface(
                                            onClick = {
                                                expanded = !expanded
                                                selectedIndex.value = i
                                                viewModel.fetchRewardChart(i)
                                            }
                                        ) {
                                            Row(
                                                modifier = Modifier
                                                    .padding(vertical = 6.dp, horizontal = 12.dp),
                                                verticalAlignment = Alignment.CenterVertically
                                            ){
                                                if(i == selectedIndex.value) {
                                                    Image(
                                                        modifier = Modifier.size(12.dp),
                                                        painter = painterResource(
                                                            id = R.drawable.ic_checkbox_selected
                                                        ),
                                                        contentDescription = "Selected"
                                                    )
                                                }else{
                                                    Image(
                                                        modifier = Modifier.size(12.dp),
                                                        painter = painterResource(
                                                            id = R.drawable.ic_checkbox_emty
                                                        ),
                                                        contentDescription = "don`t Selected"
                                                    )
                                                }
                                                Text(
                                                    text = list[i],
                                                    style = txtRewStatTxtGl
                                                )
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Composable
    private fun GetLegendStatistic(
        indicatorColor: Color,
        labelLegend   : String,
        textStyle     : TextStyle
    ){
        Box(
            modifier = Modifier
                .height(22.dp)
                .fillMaxWidth(1f)
                .background(
                    color = Color(0xffFFffFF),
                    shape = RoundedCornerShape(size = 5.dp)
                )
        ) {
            Row(
                modifier = Modifier
                    .fillMaxSize(1f)
                    .padding(horizontal = 5.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                CircleIndicator(10.dp, indicatorColor)
                Spacer(modifier = Modifier.width(5.dp))
                Text(
                    text = labelLegend,
                    style = textStyle,
                    modifier = Modifier.padding(
                        start = 10.dp, top = 0.dp, end = 0.dp, bottom = 0.dp)
                )
            }
        }
    }

    @ExperimentalMaterialApi
    @Composable
    private fun GetBoxDivitsTokensChart(){
        GetPageTokenChart(lin = tokenStatisticLin,
            pie = tokenStatisticPie,
            bar = tokenStatisticBar)
    }

    @ExperimentalMaterialApi
    @Composable
    private fun GetBoxRewardsBonusChart(){

        val dt = BarDataSet(mutableListOf(
            BarEntry( 0F, floatArrayOf(12.5F, 1.5F,  5.4F)),
            BarEntry( 1F, floatArrayOf(14.5F, 7.5F, 10.4F)),
            BarEntry( 2F, floatArrayOf(16.5F, 6.5F, 15.4F)),
            BarEntry( 3F, floatArrayOf(11.5F, 3.5F,  9.4F)),
            BarEntry( 4F, floatArrayOf(18.5F, 5.5F,  2.4F)),
            BarEntry( 5F, floatArrayOf( 9.5F, 8.5F, 16.4F)),
            BarEntry( 6F, floatArrayOf(12.5F, 1.5F,  5.4F)),
            BarEntry( 1F, floatArrayOf(14.5F, 7.5F, 10.4F)),                                                 BarEntry( 7F, floatArrayOf(16.5F, 6.5F, 32.5F, 15.4F)),
            BarEntry( 8F, floatArrayOf(11.5F, 3.5F,  9.4F)),
            BarEntry( 9F, floatArrayOf(18.5F, 5.5F,  2.4F)),
            BarEntry(10F, floatArrayOf( 9.5F, 8.5F, 16.4F)),
            BarEntry(11F, floatArrayOf(12.5F, 1.5F,  5.4F)),
            BarEntry(12F, floatArrayOf(14.5F, 7.5F, 10.4F)),
            BarEntry(13F, floatArrayOf(16.5F, 6.5F, 15.4F)),
            BarEntry(14F, floatArrayOf(11.5F, 3.5F,  9.4F)),
            BarEntry(15F, floatArrayOf(18.5F, 5.5F,  2.4F)),
            BarEntry(16F, floatArrayOf( 9.5F, 8.5F, 16.4F)),
        ), "")
        dt.colors = listOf(
            android.graphics.Color.parseColor("#FFAB00"),
            android.graphics.Color.parseColor("#38CB89"),
            android.graphics.Color.parseColor("#377DFF"))

        GetPageBonusChart(
            chartBarBalance = BarData(dt),
            viewModel.updateTypeFace()
        )
    }

    @Composable
    private fun GetBoxHowToGetRewards(){
        viewModel.fetchRewardHowTo()
        val scrollState = ScrollState(initial = 1)
        Column(modifier = Modifier
            .fillMaxSize()
            .verticalScroll(state = scrollState, enabled = true)
            .background(color = Color(0xffF8F9FA))
        ){

            Text(
                text = stringResource(id = R.string.frgPlazaPageHowToLbl),
                style = txtPgLbl(),
                modifier = Modifier.padding(16.dp)
            )

            rewardHowTo.value.forEach {
                GetPageHowTo(remember{mutableStateOf(it)})
            }
        }
    }

    @Composable
    private fun GetBoxEventDetails() {
        // Our content for each page
        val scrollState = ScrollState(initial = 1)
        Column(modifier = Modifier
            .fillMaxSize()
            .verticalScroll(state = scrollState, enabled = true)
        ){
            GetPageHowTo(
                remember {mutableStateOf(
                    HowToSection("method_a"
                        ,"Modifier 01"
                        ,"jksjd sla klsak dj slakdjsa l kd j a ljdkasj dksa jdksja djsa kldjslak jdlsajd lksasjd lasjd lask jdklsajd klasjd lk"
                        , reward = ObjData(value = "245", currency = "DR")
                        , limit  = ObjData(value = "Unlimited", currency = "")
                    )
                )}
            )
        }
    }

    @Composable
    private fun GetBoxTestMeetings() {
        // Our content for each page
        Box(
            modifier = Modifier.fillMaxSize()
        ) {
            Text(
                text = "Test meetings"
            )
            Card(
                modifier = Modifier
                    .padding(16.dp)
                    .background(
                        color = ThemeSetup.getTheme().cCardPlzColorBlue,
                        shape = RoundedCornerShape(16.dp)
                    ),
            ) {
                Text(
                    text = "${bonusPlaza.value.total.value} ${bonusPlaza.value.total.currency}",
                    style = MaterialTheme.typography.h2,
                    modifier = Modifier.fillMaxWidth(),
//                                    modifier = Modifier.align(Alignment.Center)
                )
            }
        }
    }

//    @Composable
//    private fun BarChartScreenContent() {
//        Column(
//            modifier = Modifier.padding(
//                horizontal = 0.dp,
//                vertical = 0.dp
//            )
//        ) {
//            Row(
//                modifier = Modifier
//                    .fillMaxWidth()
//                    .height(chartHeight.dp)
//                    .width(500.dp)
//                    .padding(vertical = 16.dp),
//            ) {
//                BarChart(
//                    barChartData = barChartData.value,
//                    labelDrawer = labelDrawer.value
//                )
//            }
//        }
//    }

/** **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **/

    @Composable
    fun ImageResourceDemo(idRes : Int) {
        Image(
            painter = painterResource(id = idRes),
            contentDescription = null,
        )
    }

    private inline val Dp.absoluteValue: Dp
        get() = value.absoluteValue.dp

}


/***


//                Row(
//                    modifier = Modifier
//                        .padding(start = 0.dp, top = 4.dp, end = 0.dp, bottom = 4.dp)
//                        .fillMaxWidth(),
//                    verticalAlignment = Alignment.CenterVertically
//                ){
//                    CircleIndicator(10.dp, Color.Cyan)
//                    Text(
//                        text = "fdfog fdkl fkldfd;k lfdk lgj",
//                        style = txtRewStatTxtGr,
//                        modifier = Modifier.padding(
//                            start = 10.dp, top = 0.dp, end = 0.dp, bottom = 0.dp)
//                    )
//                }
//                Row(
//                    modifier = Modifier
//                        .padding(start = 0.dp, top = 4.dp, end = 0.dp, bottom = 4.dp)
//                        .fillMaxWidth(),
//                    verticalAlignment = Alignment.CenterVertically
//                ){
//                    CircleIndicator(10.dp, Color.Magenta)
//                    Text(
//                        text = "fwierjow enroc urowerpm owipo",
//                        style = txtRewStatTxtGr,
//                        modifier = Modifier.padding(
//                            start = 10.dp, top = 0.dp, end = 0.dp, bottom = 0.dp)
//                    )
//                }
//                Row(modifier = Modifier
//                    .padding(start = 0.dp, top = 4.dp, end = 0.dp, bottom = 4.dp)
//                    .fillMaxWidth(),
//                    verticalAlignment = Alignment.CenterVertically
//                ){
//                    CircleIndicator(10.dp, Color.Blue)
//                    Text(
//                        text = "fdfjio snoiio xmueosi dgljklsd jhk",
//                        style = txtRewStatTxtGr,
//                        modifier = Modifier.padding(
//                            start = 10.dp, top = 0.dp, end = 0.dp, bottom = 0.dp)
//                    )
//                }
//                Row(
//                    modifier = Modifier
//                        .padding(start = 0.dp, top = 4.dp, end = 0.dp, bottom = 4.dp)
//                        .fillMaxWidth(),
//                    verticalAlignment = Alignment.CenterVertically
//                ){
//                    CircleIndicator(10.dp, Color.Green)
//                    Text(
//                        text = "klasjf lsdmjfk sdjf lskdjf",
//                        style = txtRewStatTxtGr,
//                        modifier = Modifier.padding(
//                            start = 10.dp, top = 0.dp, end = 0.dp, bottom = 0.dp)
//                    )
//                }

***/