package meet.sumra.app.frg.drawer.bonusPlaza

import android.graphics.Typeface
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.data.*
import meet.sumra.app.AppMeet
import meet.sumra.app.R
import meet.sumra.app.R.*
import meet.sumra.app.data.mdl.earnings.cards
import meet.sumra.app.data.configs.settingsCharts.setupDivitBalanceChartSetup
import meet.sumra.app.utils.setup.ThemeUtils


fun getBonusText(): TextStyle{
    return when (ThemeUtils.getApplicationTheme(AppMeet.getInstance())) {
        true -> TextStyle(
                color = Color(0xff323B4B), fontSize = 16.sp,
                fontFamily = FontFamily(Font(font.font_dm_sans_family, FontWeight.W700)),
                textAlign = TextAlign.Justify)
        else -> TextStyle(
                color = Color(0xff323B4B), fontSize = 16.sp,
                fontFamily = FontFamily(Font(font.font_dm_sans_family, FontWeight.W700)),
                textAlign = TextAlign.Justify)
        }
}
fun getButtonShow(): TextStyle{
    return when (ThemeUtils.getApplicationTheme(AppMeet.getInstance())) {
        true -> TextStyle(
            color = Color(0xffB0B7C3), fontSize = 13.sp,
            fontFamily = FontFamily(Font(font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify)
        else -> TextStyle(
            color = Color(0xff6A737D), fontSize = 13.sp,
            fontFamily = FontFamily(Font(font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify)
    }
}
fun getButtonName(): TextStyle{
    return when (ThemeUtils.getApplicationTheme(AppMeet.getInstance())) {
        true -> TextStyle(
            color = Color(0xff4E5D78), fontSize = 13.sp,
            fontFamily = FontFamily(Font(font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify)
        else -> TextStyle(
            color = Color(0xff6A737D), fontSize = 13.sp,
            fontFamily = FontFamily(Font(font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify)
    }
}
fun getLegend(): TextStyle{
    return when (ThemeUtils.getApplicationTheme(AppMeet.getInstance())) {
        true -> TextStyle(
            color = Color(0xffB0B7C3), fontSize = 13.sp,
            fontFamily = FontFamily(Font(font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify)
        else -> TextStyle(
            color = Color(0xff6A737D), fontSize = 13.sp,
            fontFamily = FontFamily(Font(font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify)
    }
}
fun getDataValuesVal(): TextStyle{
    return when (ThemeUtils.getApplicationTheme(AppMeet.getInstance())) {
        true -> TextStyle(
            color = Color(0xff4E5D78), fontSize = 20.sp,
            fontFamily = FontFamily(Font(font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify)
        else -> TextStyle(
            color = Color(0xff6A737D), fontSize = 20.sp,
            fontFamily = FontFamily(Font(font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify)
    }
}
fun getDataValuesLbl(): TextStyle{
    return when (ThemeUtils.getApplicationTheme(AppMeet.getInstance())) {
        true -> TextStyle(
            color = Color(0xffB0B7C3), fontSize = 14.sp,
            fontFamily = FontFamily(Font(font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify)
        else -> TextStyle(
            color = Color(0xff6A737D), fontSize = 14.sp,
            fontFamily = FontFamily(Font(font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify)
    }
}
fun getBalance(): TextStyle{
    return when (ThemeUtils.getApplicationTheme(AppMeet.getInstance())) {
        true -> TextStyle(
            color = Color(0xff4E5D78), fontSize = 32.sp,
            fontFamily = FontFamily(Font(font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify)
        else -> TextStyle(
            color = Color(0xff6A737D), fontSize = 32.sp,
            fontFamily = FontFamily(Font(font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify)
    }
}
fun getBalanceLbl(): TextStyle{
    return when (ThemeUtils.getApplicationTheme(AppMeet.getInstance())) {
        true -> TextStyle(
            color = Color(0xffB0B7C3), fontSize = 16.sp,
            fontFamily = FontFamily(Font(font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify)
        else -> TextStyle(
            color = Color(0xff6A737D), fontSize = 16.sp,
            fontFamily = FontFamily(Font(font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify)
    }
}
fun getBalanceColor()= TextStyle(
            color = Color(0xff38CB89), fontSize = 16.sp,
            fontFamily = FontFamily(Font(font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify)


@ExperimentalMaterialApi
@Preview
@Composable
fun BonusChart() {
//    GetPageBonusChart(
//        chartBarBalance = BarData(
//            listOf(
//                BarDataSet(mutableListOf(BarEntry(12.4F, 12.5F)), "lbl1"),
//                BarDataSet(mutableListOf(BarEntry(16.4F, 12.5F)), "lbl2"),
//                BarDataSet(mutableListOf(BarEntry(23.4F, 12.5F)), "lbl3"),
//                BarDataSet(mutableListOf(BarEntry( 8.4F, 12.5F)), "lbl4"),
//                BarDataSet(mutableListOf(BarEntry(13.4F, 12.5F)), "lbl5"),
//                BarDataSet(mutableListOf(BarEntry(18.4F, 12.5F)), "lbl6"),
//            )
//        ),
//    )
}

@ExperimentalMaterialApi
@Composable
fun GetPageBonusChart(
    chartBarBalance: BarData,
    tf: Typeface
){
    val scrollState = rememberScrollState()
    Box(
        modifier = Modifier
            .fillMaxSize(1f)
            .background(
                color = Color(0xffFAF9FC),
                shape = RoundedCornerShape(topStart = 25.dp, topEnd = 25.dp)
            )
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize(1f)
                .verticalScroll(scrollState)
        ) {
            Column() {
                RewardsTopView(
                    periodList = cards.EPeriod.values(),
                    periodSelected = cards.EPeriod.YEAR,
                    divitsBalance = 12548.54F,
                    dataForChart = chartBarBalance,
                    tf = tf
                )
                ViewsDays(
                    dataLastDay = 45,
                    dataLastPercent = 63.4F,
                    dataThisYear =56,
                    dataThisPercent = 25.5F
                )
                ViewsBalance(
                    referralsBalance = 67,
                    tokenPublishing = 96
                )
            }
        }
    }
}

private val rootPadding = 20.dp

@ExperimentalMaterialApi
@Composable
private fun RewardsTopView(
    periodList    : Array<cards.EPeriod>,
    periodSelected: cards.EPeriod,
    divitsBalance : Float,
    dataForChart  : BarData,
    tf            : Typeface
){
    Row(modifier = Modifier
        .fillMaxSize(1f)
        .padding(top = 30.dp, bottom = 15.dp, start = rootPadding, end = rootPadding)
    ) {
        Box(
            modifier = Modifier.fillMaxSize(1f),
            contentAlignment = Alignment.TopEnd
        ) {
            Column(
                modifier = Modifier.fillMaxSize(1f)
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(1f),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        modifier = Modifier.weight(1f),
                        text = stringResource(id = R.string.plazaBonusChart),
                        style = getBonusText(),
                        maxLines = 2
                    )
                    Surface(
                        onClick = {

                        },
                        color = Color(0xffFFffFF),
                        shape = RoundedCornerShape(size = 8.dp),
//                        modifier = Modifier
//                            .padding(all = 6.dp)
                    ) {
                        Row(
                            modifier = Modifier
                                .fillMaxSize(1f)
                                .padding(all = 6.dp)
                        ) {
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = stringResource(id = R.string.plazaBonusChartShow),
                                style = getButtonShow()
                            )
                            Text(
                                modifier = Modifier.padding(end = 8.dp),
                                text = periodSelected.label,
                                style = getButtonName()
                            )
                            Image(
                                modifier = Modifier.size(10.dp),
                                painter = painterResource(id = drawable.ic_arrow_down),
                                contentDescription = "Image Arrow"
                            )
                        }
                    }
                }
                Row(
                    modifier = Modifier
                        .padding(vertical = 5.dp, horizontal = 10.dp)) {
                    Divider(color = Color(0xffDFE2E6))
                }
                AndroidView(
                    factory = {
                        BarChart(it).apply {
                            data = dataForChart
                            setupDivitBalanceChartSetup(this)
                        }
                    },
                    modifier = Modifier
                        .fillMaxWidth(1f)
                        .height(180.dp)
                )
                LegendShow(nameLegend = "Commercial activity", colorLegend = Color.Green)
                LegendShow(nameLegend = "Download our mobile apps", colorLegend = Color.Yellow)
                LegendShow(nameLegend = "Referrals balance paid-out", colorLegend = Color.Blue)
            }
        }
    }
}

@Composable
private fun LegendShow(
    nameLegend : String,
    colorLegend: Color
){
    Surface(
        modifier = Modifier
            .fillMaxWidth(1f)
            .padding(vertical = 3.dp, horizontal = 10.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth(1f)
                .height(22.dp)
                .background(color = Color(0xffFFffFF), shape = RoundedCornerShape(size = 5.dp)),
            verticalAlignment = Alignment.CenterVertically
        ) {
            val indicator = 10
            Box(
                modifier = Modifier
                    .size(indicator.dp)
                    .background(
                        color = colorLegend,
                        shape = RoundedCornerShape(size = (indicator / 2).dp)
                    )
                    .padding(start = 8.dp))
            Text(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .padding(start = 8.dp),
                text  = nameLegend,
                style = getLegend())
        }
    }
}

@Composable
private fun ViewsDays(
    dataLastDay    : Int,
    dataLastPercent: Float,
    dataThisYear   : Int,
    dataThisPercent: Float
){
    val cardPad = 10
    val sizeChart = 80
    val stroke = 12f
    Row(
        modifier = Modifier
            .fillMaxSize(1f)
            .padding(horizontal = 20.dp, vertical = 15.dp)
    ) {
        Card(
            modifier = Modifier.fillMaxSize(1f),
            backgroundColor = Color(0xffFFffFF),
            shape = RoundedCornerShape(size = 10.dp)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize(1f)
                    .padding(start = (cardPad + stroke/2).dp,
                        end = cardPad.dp,
                        top = (cardPad + stroke/2).dp,
                        bottom = (cardPad + stroke/2).dp)
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(1f),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Box(modifier = Modifier
                        .height(sizeChart.dp)
                        .width(sizeChart.dp)) {
                        Canvas(
                            modifier = Modifier
                                .height(sizeChart.dp)
                                .width(sizeChart.dp),
                        ) {
                            drawArc(
                                color = Color(0xffFAF9FC),
                                startAngle = -25f,
                                sweepAngle = 360f,
                                useCenter = false,
                                style = Stroke(width = stroke, cap = StrokeCap.Square)
                            )
                        }
                        Canvas(
                            modifier = Modifier
                                .height(sizeChart.dp)
                                .width(sizeChart.dp),
                        ) {
                            drawArc(
                                color = Color(0xff377DFF),
                                startAngle = -25f,
                                sweepAngle = (360 * dataLastDay / 100).toFloat(),
                                useCenter = false,
                                style = Stroke(width = stroke, cap = StrokeCap.Square)
                            )
                        }
                    }
//                    AndroidView(
//                        modifier = Modifier.size(100.dp),
//                        factory = {
////                            PieChart(it).apply {
////                                data = PieData(PieDataSet(
////                                    mutableListOf(
////                                        PieEntry(500F, "part b"),
////                                        PieEntry(125F, "part d"))
////                                    , "FILL"))
////                            }
//                            PieChart(it).apply {
//                                val dt = PieData(PieDataSet(
//                                    mutableListOf(
//                                        PieEntry(100F, "part b"),
//                                        PieEntry( 67F, "part d"))
//                                    , "FILL"))
//                                data = dt
//                                setupPieBarBonusChart(this)
//                            }
//                        })
                    Spacer(modifier = Modifier.width(15.dp))
                    Column(
                        horizontalAlignment = Alignment.Start
                    ) {
                        Text(
                            text = "$dataLastDay%",
                            style = getDataValuesVal()
                        )
                        Text(
                            text = stringResource(id = R.string.plazaDataLastDay),
                            style = getDataValuesLbl()
                        )
                    }
                    Spacer(modifier = Modifier.weight(1f))
                    Row(
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(
                            text = "${dataLastPercent}%",
                            style = getBalanceColor()
                        )
                        Spacer(modifier = Modifier.width(8.dp))
                        Image(
                            modifier = Modifier.size(12.dp),
                            painter = painterResource(id = drawable.ic_statistic_card_arrow_up),
                            contentDescription = "Image Up"
                        )
                    }
                }
                Row (
                    modifier = Modifier.padding(vertical = 10.dp)
                ){
                    Divider(
                        color = Color(0xffF3F3F3)
                    )
                }
                Row(
                    modifier = Modifier.fillMaxWidth(1f),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Box(modifier = Modifier
                        .height(sizeChart.dp)
                        .width(sizeChart.dp)){
                        Canvas(
                            modifier = Modifier
                                .height(sizeChart.dp)
                                .width(sizeChart.dp),
                        ) {
                            drawArc(
                                color = Color(0xffFAF9FC),
                                startAngle = -25f,
                                sweepAngle = 360f,
                                useCenter = false,
                                style = Stroke(width = stroke, cap = StrokeCap.Square)
                            )
                        }
                        Canvas(
                            modifier = Modifier
                                .height(sizeChart.dp)
                                .width(sizeChart.dp),
                        ) {
                            drawArc(
                                color = Color(0xffFF5630),
                                startAngle = -25f,
                                sweepAngle = (360 * dataThisYear / 100).toFloat(),
                                useCenter = false,
                                style = Stroke(width = stroke, cap = StrokeCap.Square)
                            )
                        }
                    }
//                    val col = listOf(
//                        android.graphics.Color.parseColor("#FAF9FC"),
//                        android.graphics.Color.parseColor("#377DFF"))
//                    AndroidView(
//                        modifier = Modifier
//                            .size(100.dp)
//                            .background(Color.LightGray),
//                        factory = {
//                            PieChart(it).apply {
//                                val ds = PieDataSet(mutableListOf(
//                                                PieEntry(100F, "part b"),
//                                                PieEntry( 83F, "part d"))
//                                            , "FILL")
//                                ds.setColors(col)
//                                val dt = PieData(ds)
//                                data = dt
//                                setupPieBarBonusChart(this)
//                            }
//                        })
                    Spacer(modifier = Modifier.width(15.dp))
                    Column(
                        horizontalAlignment = Alignment.Start
                    ) {
                        Text(
                            text = "$dataThisYear%",
                            style = getDataValuesVal()
                        )
                        Text(
                            text = stringResource(id = R.string.plazaDataThisYear),
                            style = getDataValuesLbl()
                        )
                    }
                    Spacer(modifier = Modifier
                        .weight(1f)
                        .background(Color.Yellow))
                    Row(
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(
                            text  = "${dataThisPercent}%",
                            style = getBalanceColor()
                        )
                        Spacer(modifier = Modifier.width(8.dp))
                        Image(
                            modifier = Modifier.size(12.dp),
                            painter = painterResource(id = drawable.ic_statistic_card_arrow_up),
                            contentDescription = "Image Up"
                        )
                    }
                }
            }
        }
    }
}

@Composable
private fun ViewsBalance(
    referralsBalance: Int,
    tokenPublishing : Int
){
    val cardPad = 10
    val size = 120
    val stroke = 14f
    val spaceAfterCanvas = 30
    Row(
        modifier = Modifier
            .fillMaxSize(1f)
            .padding(horizontal = 20.dp, vertical = 15.dp)
    ) {
        Card(
            modifier = Modifier.fillMaxSize(1f),
            backgroundColor = Color(0xffFFffFF),
            shape = RoundedCornerShape(size = 15.dp)
        ) {
            Row(
                modifier = Modifier
                    .fillMaxSize(1f)
                    .padding(
                        start = (cardPad + stroke/2).dp,
                        end = cardPad.dp,
                        top = (cardPad + stroke/2).dp,
                        bottom = (cardPad + stroke/2).dp)
            ) {
                Spacer(modifier = Modifier.weight(0.5f))
                Column(
                    modifier = Modifier.fillMaxHeight(1f),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Box(
                        modifier = Modifier.size(size.dp),
                        contentAlignment = Alignment.Center
                    ) {
//                        AndroidView(
//                            modifier = Modifier.size(100.dp),
//                            factory = {
//                                PieChart(it).apply {
////                                    setupPieBarBonusChart(this)
//                                }
//                            })
                        Canvas(
                            modifier = Modifier
                                .height(size.dp)
                                .width(size.dp),
                        ) {
                            drawArc(
                                color = Color(0xffFAF9FC),
                                startAngle = -25f,
                                sweepAngle = 360f,
                                useCenter = false,
                                style = Stroke(width = stroke, cap = StrokeCap.Square)
                            )
                        }
                        Canvas(
                            modifier = Modifier
                                .height(size.dp)
                                .width(size.dp),
                        ) {
                            drawArc(
                                color = Color(0xff377DFF),
                                startAngle = -25f,
                                sweepAngle = (360*referralsBalance/100).toFloat(),
                                useCenter = false,
                                style = Stroke(width = stroke, cap = StrokeCap.Square)
                            )
                        }
                        Image(
                            modifier = Modifier
                                .size(48.dp)
                                .padding(all = 6.dp),
                            painter = painterResource(id = drawable.ic_palza_token_bonus_balance)
                            , contentDescription = "USER")
                    }
                    Spacer(modifier = Modifier.width(spaceAfterCanvas.dp))
                    Text(
                        text = "$referralsBalance%",
                        style = getBalance()
                    )
                    Text(
                        text = stringResource(id = R.string.plazaReferralsBalancePaidOut),
                        style = getBalanceLbl()
                    )
                }
                Spacer(modifier = Modifier.weight(1f))
                Column(
                    modifier = Modifier.fillMaxHeight(1f),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Box(
                        modifier = Modifier.size(size.dp),
                        contentAlignment = Alignment.Center
                    ) {
//                        AndroidView(
//                            modifier = Modifier.fillMaxSize(1f),
//                            factory = {
//                                PieChart(it).apply {
////                                    setupPieBarBonusChart(this)
//                                }
//                            })
                        Canvas(
                            modifier = Modifier
                                .height(size.dp)
                                .width(size.dp),
                        ) {
                            drawArc(
                                color = Color(0xffFAF9FC),
                                startAngle = -25f,
                                sweepAngle = 360f,
                                useCenter = false,
                                style = Stroke(width = stroke, cap = StrokeCap.Square)
                            )
                        }
                        Canvas(
                            modifier = Modifier
                                .height(size.dp)
                                .width(size.dp),
                        ) {
                            drawArc(
                                color = Color(0xff38CB89),
                                startAngle = -25f,
                                sweepAngle = (360*tokenPublishing/100).toFloat(),
                                useCenter = false,
                                style = Stroke(width = stroke, cap = StrokeCap.Square)
                            )
                        }

                        Image(
                            modifier = Modifier
                                .size(48.dp)
                                .padding(all = 6.dp),
                            painter = painterResource(id = drawable.ic_palza_token_bonus_pulish)
                            , contentDescription = "USER")
                    }
                    Spacer(modifier = Modifier.width(spaceAfterCanvas.dp))
                    Text(
                        text = "$tokenPublishing%",
                        style = getBalance()
                    )
                    Text(
                        text = stringResource(id = R.string.plazaDivitsTokenPublishing),
                        style = getBalanceLbl()
                    )
                }
                Spacer(modifier = Modifier.weight(0.5f))
            }
        }
    }

//    @Composable
//    fun CircularProgress(value: Float){
////        val progress by remember { mutableStateOf(0.1f) }
////        val animatedProgress = animateFloatAsState(
////            targetValue = progress,
////            animationSpec = ProgressIndicatorDefaults.ProgressAnimationSpec
////        ).value
//        
//        CircularProgressIndicator(
//            modifier = Modifier.fillMaxSize(1f),
//            progress = value,
//            strokeWidth = 12.dp,
////            progress = animatedProgress
//        )
//    }




}





