//package meet.sumra.app.frg.messges
//
//import android.app.Activity
//import android.content.BroadcastReceiver
//import android.content.Context
//import android.content.Intent
//import android.content.IntentFilter
//import android.os.Bundle
//import android.view.MotionEvent
//import android.view.View
//import androidx.lifecycle.Observer
//import com.google.accompanist.pager.ExperimentalPagerApi
//import meet.sumra.app.R
//import meet.sumra.app.databinding.FrgMeetRunBinding
//import meet.sumra.app.di.component.ComponentViewModel
//import meet.sumra.app.ext.set
//import meet.sumra.app.frg.FragmentBase
//import meet.sumra.app.utils.LogUtil
//import meet.sumra.app.vm.vm_home.StateMeet
//import meet.sumra.app.vm.vm_meet.ViewModelMeet
//import org.jitsi.meet.sdk.*
//import javax.inject.Inject
//
//class FrgMeetRun: FragmentBase<FrgMeetRunBinding>(), View.OnTouchListener {
//
//    companion object{
//        val TAG = FrgMeetRun::class.java.simpleName
//        fun getInstance(): FragmentBase<*>{
//            return FrgMeetRun()
//        }
//    }
//
//    lateinit var viewModel: ViewModelMeet
//        @Inject set
//
//    @ExperimentalPagerApi
//    private fun setupUI(){
//        viewFragment.root.background = viewModel.fetchGradient()
//
//        val tempView = JitsiMeetView(requireActivity())
//        viewFragment.actMeetRunContainer.addView(tempView)
//        tempView.addOnAttachStateChangeListener(object: View.OnAttachStateChangeListener{
//            override fun onViewAttachedToWindow(v: View?) {
//                LogUtil.info(txt = "onViewAttachedToWindow")
//            }
//
//            override fun onViewDetachedFromWindow(v: View?) {
//                LogUtil.info(txt = "onViewDetachedFromWindow")
//            }
//        })
//        tempView.join(viewModel.fetchPreparingOptions())
//
//        viewModel.updateTypeFace().run {
////            viewFragment.frgContactMeetBar.typeface = this
////            viewFragment.frgAddMeetLabelTitle.typeface = this
////            viewFragment.frgAddMeetEditTitle.typeface = this
////            viewFragment.frgAddMeetLabelParticipants.typeface = this
////            viewFragment.frgAddMeetParticipantsLabel.typeface = this
////            viewFragment.frgAddMeetEmptyListLabel.typeface = this
////            viewFragment.frgAddMeetLabelLobby.typeface = this
////            viewFragment.frgAddMeetLabelDescription.typeface = this
////            viewFragment.frgAddMeetLabelPassword.typeface = this
////            viewFragment.frgAddMeetEnterPassword.typeface = this
////            viewFragment.frgAddMeetLabelSelectDateTime.typeface = this
////            viewFragment.frgAddMeetLabelTranslation.typeface = this
////            viewFragment.frgAddMeetLabelKey.typeface = this
////            viewFragment.frgAddMeetEditTranslationKey.typeface = this
////            viewFragment.frgAddMeetLabelNote.typeface = this
////            viewFragment.frgAddMeetEnterNote.typeface = this
//        }
//        /** recycler lists of participants **/
//
//        viewModel.mViewState.set(StateMeet.AddBroadcast)
//
////        registerForBroadcastMessages()
//    }
//
//    private fun addListener(){
//
//    }
//
//    @ExperimentalPagerApi
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        viewModel.mViewState.observe(viewLifecycleOwner, stateToMeetWait())
//
//        setupUI()
//        addListener()
//    }
//
//    /** View model observers... */
//    private fun stateToMeetWait() = Observer<StateMeet>{ meet ->
//        when(meet){
//            is StateMeet.Default -> {}
//            is StateMeet.SessionPst -> {
////                mRoomState.add(true)
//            }
//            is StateMeet.SessionPut -> {
////                mRoomState[0] = !mRoomState[0]
//            }
//            is StateMeet.RoomPut -> {}
//            is StateMeet.RoomDel -> {}
//        }
//    }
//
//    override fun onBackPressed(): Boolean {
//        return true
//    }
//
//    override fun getBinding(): FrgMeetRunBinding {
//        return FrgMeetRunBinding.inflate(layoutInflater)
//    }
//
//    override fun injectDependency(component: ComponentViewModel) {
//        component.inject(this)
//    }
//
//    override fun getEnterAnimation() = R.anim.anim_show_c_to_o
//
//    override fun getExitAnimation()  = R.anim.anim_hide_o_to_c
//
//    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
//        LogUtil.info(txt = "onTouch ${v!!::class.java.simpleName} :: ${event!!::class.java.simpleName} ")
//        return true
//    }
//
//}