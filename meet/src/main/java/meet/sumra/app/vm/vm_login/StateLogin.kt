package meet.sumra.app.vm.vm_login

sealed class StateLogin{
    class Session                 : StateLogin()
    class Section                 : StateLogin()
    class ToHome                  : StateLogin()
    class Success(message: String): StateLogin()
    class Failure(message: String): StateLogin()
}
