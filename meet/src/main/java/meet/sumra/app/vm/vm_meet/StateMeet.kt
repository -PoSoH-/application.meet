package meet.sumra.app.vm.vm_home

import meet.sumra.app.data.mdl.DtProfileUser
import meet.sumra.app.data.mdl.SessionData
import meet.sumra.app.data.mdl.roomModels
import meet.sumra.app.data.pojo.ResponseRepository
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions

sealed class StateMeet{
    object Default : StateMeet()
    object MeetRun : StateMeet()
    object MeetWait: StateMeet()
    object ActionToBack:StateMeet()

    data class SessionPst(val session: SessionData): StateMeet()
    data class SessionPut(val session: SessionData): StateMeet()
    object SessionDel: StateMeet()
    data class UserGet(val user: DtProfileUser): StateMeet()

    data class RoomGet(val dt: roomModels.RoomByInvite): StateMeet()
    data class RoomPut(val dt: roomModels.RoomByInvite): StateMeet()
    data class RoomDel(val dt: roomModels.RoomByInvite): StateMeet()
    object RoomEmp: StateMeet()

    data class ShareInvite(val inviteLink: String): StateMeet()
    data class MeetOptions(val Options: JitsiMeetConferenceOptions): StateMeet()
}
