package meet.sumra.app.vm

import meet.sumra.app.data.mdl.Banner
import meet.sumra.app.data.mdl.DtMeetHistory

sealed class RecentHome {
    object Default : RecentHome()
    data class BannerGet (val value: List<Banner>) : RecentHome()
    data class HistoryGet(val value: List<DtMeetHistory>): RecentHome()
}