package meet.sumra.app.vm.vm_profile

import androidx.lifecycle.MutableLiveData
import meet.sumra.app.AppMeet
import meet.sumra.app.ext.default
import meet.sumra.app.repository.Repository
import meet.sumra.app.vm.BaseViewModel

class ViewModelProfile (private val application: AppMeet,
                        private val repository : Repository,
                        override val viewMessageState: MutableLiveData<MessageState> = MutableLiveData<MessageState>()
                         .default(initialValue = MessageState.MessageNothing),
                        override val viewProgressState: MutableLiveData<LoadingState> = MutableLiveData<LoadingState>()
                         .default(initialValue = LoadingState.LoadingNothing)
): BaseViewModel(application){

    val viewStateProfile = MutableLiveData<StateProfile>().default(initialValue = StateProfile.Default)



}


