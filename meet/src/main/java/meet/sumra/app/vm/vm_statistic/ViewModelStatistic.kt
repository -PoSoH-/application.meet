package meet.sumra.app.vm.vm_statistic

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.LineDataSet
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import meet.sumra.app.AppMeet
import meet.sumra.app.data.mdl.earnings.cards
import meet.sumra.app.data.mdl.statistic.*
import meet.sumra.app.data.pojo.RespStatistic
import meet.sumra.app.ext.default
import meet.sumra.app.ext.set
import meet.sumra.app.repository.Repository
import meet.sumra.app.vm.BaseViewModel

class ViewModelStatistic /*@Inject constructor*/(
    private var application: AppMeet,
    private var repository : Repository,
    override val viewMessageState: MutableLiveData<MessageState> = MutableLiveData<MessageState>()
        .default(initialValue = MessageState.MessageNothing)): BaseViewModel(application) {
    override val viewProgressState: MutableLiveData<LoadingState>
        get() = TODO("Not yet implemented")

    internal val stateStatistic = MutableLiveData<StateStatistic>().default(StateStatistic.Default)

    internal val currentBalances  = mutableStateOf(Balances()); get() = field
    internal val globalEarnings   = mutableStateOf(Earnings()); get() = field
    internal val currentReferrals = mutableStateOf(Referrals()); get() = field
    internal val contactBooks     = mutableStateOf(Contacts()); get() = field
    internal val currentRewards   = mutableStateOf(Rewards()); get() = field
    internal val currentCashBack  = mutableStateOf(CashBack()); get() = field
    internal val currentRentPay   = mutableStateOf(RentPay()); get() = field
    internal val currentTransfers = mutableStateOf(Transfers()); get() = field
    internal val currentOverview  = mutableStateOf(Overview()); get() = field

    internal val prgBalances  = mutableStateOf(false); get() = field
    internal val prgEarnings  = mutableStateOf(false); get() = field
    internal val prgReferrals = mutableStateOf(false); get() = field
    internal val prgBooks     = mutableStateOf(false); get() = field
    internal val prgRewards   = mutableStateOf(false); get() = field
    internal val prgCashBack  = mutableStateOf(false); get() = field
    internal val prgRentPay   = mutableStateOf(false); get() = field
    internal val prgTransfers = mutableStateOf(false); get() = field
    internal val prgOverview  = mutableStateOf(false); get() = field

    private lateinit var job: Job
    internal val overviewPeriod = mutableStateOf(cards.EPeriod.YEAR); get() = field

    init{
        fetchBalances()
        fetchEarnings()
        fetchReferrals()
        fetchBooks()
        fetchRewards()
        fetchCashBack()
        fetchRentPay()
        fetchTransfers()
    }

    internal fun fetchBalances(){
        prgBalances.value = true
        job = viewModelScope.launch(Dispatchers.IO) {
            val dt = repository.updateCurrentBalances()
            withContext(Dispatchers.Main){
                prgBalances.value = false
                when(dt) {
                    is RespStatistic.Balances -> {
                        currentBalances.value = dt.body
//                        stateStatistic.set(StateStatistic.UpdateBalances(dt.body))
                    }
                    is RespStatistic.Failure  -> {
                        stateStatistic.set(StateStatistic.Failure("Error Balance Failure..."))
                    }
                 }
            }
        }
    }

    internal fun fetchEarnings() {
        prgEarnings.value = true
        job = viewModelScope.launch(Dispatchers.IO) {
            val dt = repository.updateCurrentEarnings()
            withContext(Dispatchers.Main) {
                prgEarnings.value = false
                when (dt) {
                    is RespStatistic.Earnings -> {
                        globalEarnings.value = dt.body
                    }
                    is RespStatistic.Failure  -> {
                        stateStatistic.set(StateStatistic
                            .Failure("Error updates earnings..."))
                    }
                }
            }
        }
    }

    internal fun fetchReferrals(){
        prgReferrals.value = true
        job = viewModelScope.launch(Dispatchers.IO) {
            val dt = repository.updateCurrentReferrals()
            withContext(Dispatchers.Main) {
                prgReferrals.value = false
                when (dt) {
                    is RespStatistic.Referrals -> {
                        currentReferrals.value = dt.body
                    }
                    is RespStatistic.Failure  -> {
                        stateStatistic.set(StateStatistic
                            .Failure("Error updates earnings..."))
                    }
                }
            }
        }
    }
    internal fun fetchBooks(){
        prgBooks.value = true
        job = viewModelScope.launch(Dispatchers.IO) {
            val dt = repository.updateCurrentContacts()
            withContext(Dispatchers.Main) {
                prgBooks.value = false
                when (dt) {
                    is RespStatistic.Contacts -> {
                        contactBooks.value = dt.body
                    }
                    is RespStatistic.Failure  -> {
                        stateStatistic.set(StateStatistic
                            .Failure("Error updates earnings..."))
                    }
                }
            }
        }
//        stateStatistic.set(StateStatistic.UpdateBalances(""))
//        repository.updateCurrentBalances()
    }
    internal fun fetchRewards(){
        prgRewards.value = true
        job = viewModelScope.launch(Dispatchers.IO) {
            val dt = repository.updateCurrentRewards()
            withContext(Dispatchers.Main) {
                prgRewards.value = false
                when (dt) {
                    is RespStatistic.Rewards -> {
                        currentRewards.value = dt.body
                    }
                    is RespStatistic.Failure  -> {
                        stateStatistic.set(StateStatistic
                            .Failure("Error updates earnings..."))
                    }
                }
            }
        }
//        stateStatistic.set(StateStatistic.UpdateBalances(""))
//        repository.updateCurrentBalances()
    }
    internal fun fetchCashBack(){
        prgCashBack.value = true
        job = viewModelScope.launch(Dispatchers.IO) {
            val dt = repository.updateCurrentCashBack()
            withContext(Dispatchers.Main) {
                prgCashBack.value = false
                when (dt) {
                    is RespStatistic.Cashback -> {
                        currentCashBack.value = dt.body
                    }
                    is RespStatistic.Failure  -> {
                        stateStatistic.set(StateStatistic
                            .Failure("Error updates earnings..."))
                    }
                }
            }
        }
    }
    internal fun fetchRentPay(){
        prgRentPay.value = true
        job = viewModelScope.launch(Dispatchers.IO) {
            val dt = repository.updateCurrentRentpayments()
            withContext(Dispatchers.Main) {
                prgRentPay.value = false
                when (dt) {
                    is RespStatistic.RentPay -> {
                        currentRentPay.value = dt.body
                    }
                    is RespStatistic.Failure  -> {
                        stateStatistic.set(StateStatistic
                            .Failure("Error updates earnings..."))
                    }
                }
            }
        }
    }
    internal fun fetchTransfers(){
        prgTransfers.value = true
        job = viewModelScope.launch(Dispatchers.IO) {
            val dt = repository.updateCurrentTransfers()
            withContext(Dispatchers.Main) {
                prgTransfers.value = false
                when (dt) {
                    is RespStatistic.Transfer -> {
                        currentTransfers.value = dt.body
                    }
                    is RespStatistic.Failure  -> {
                        stateStatistic.set(StateStatistic
                            .Failure("Error updates earnings..."))
                    }
                }
            }
        }
    }
    internal fun fetchUpdateOverviewPeriod(period: cards.EPeriod){
        prgOverview.value = true
        job = viewModelScope.launch(Dispatchers.IO) {
            val rr = repository.updateCurrentOverview(period)
            withContext(Dispatchers.Main){
                prgOverview.value = false
                currentOverview.value
//                stateStatistic.set
            }
        }
    }

}