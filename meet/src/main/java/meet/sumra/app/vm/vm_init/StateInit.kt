package meet.sumra.app.vm.vm_init

sealed class StateInit{
    class CheckSession()          : StateInit()
    class GoToLogin()             : StateInit()
    class GoToHome()              : StateInit()
    class Success(message: String): StateInit()
    class Failure(message: String): StateInit()
}

