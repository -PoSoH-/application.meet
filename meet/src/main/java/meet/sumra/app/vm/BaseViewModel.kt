package meet.sumra.app.vm

import android.app.Application
import androidx.annotation.StringRes
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import meet.sumra.app.AppMeet
import meet.sumra.app.R

abstract class BaseViewModel (appMeet: Application): AndroidViewModel(appMeet) {
    fun getContext() = getApplication<AppMeet>()
    fun getString(@StringRes id: Int): String = getContext().getString(id)

    abstract val viewMessageState  : MutableLiveData<MessageState>  //().default(initialValue = MessageState.Default())
    abstract val viewProgressState : MutableLiveData<LoadingState> //().default(initialValue = ProgressState.Default())

    companion object{
        const val PRIMARY = "primary"
        const val SECONDARY = "secondary"
        const val SUCCESS = "success"
        const val INFO = "info"
        const val WARNING = "warning"
        const val DANGER = "danger"
        const val LIGHT = "light"
        const val DARK = "dark"
    }

    sealed class MessageState {
        object MessageNothing : MessageState() //default values
        data class MessagePrimary  (val content: AlertText, val color: AlertColor = AlertColor(back = R.color.primary, text = R.color.primary_txt)): MessageState()     {companion object {val TAG = PRIMARY }}
        data class MessageSecondary(val content: AlertText, val color: AlertColor = AlertColor(back = R.color.secondary, text = R.color.secondary_txt)): MessageState() {companion object {val TAG = SECONDARY}}
        data class MessageSuccess  (val content: AlertText, val color: AlertColor = AlertColor(back = R.color.success, text = R.color.success_txt)): MessageState()     {companion object {val TAG = SUCCESS}}
        data class MessageInfo     (val content: AlertText, val color: AlertColor = AlertColor(back = R.color.info, text = R.color.info_txt)): MessageState()           {companion object {val TAG = INFO}}
        data class MessageWarning  (val content: AlertText, val color: AlertColor = AlertColor(back = R.color.warning, text = R.color.warning_txt)): MessageState()     {companion object {val TAG = WARNING}}
        data class MessageDanger   (val content: AlertText, val color: AlertColor = AlertColor(back = R.color.danger, text = R.color.danger_txt)): MessageState()       {companion object {val TAG = DANGER}}
        data class MessageLight    (val content: AlertText, val color: AlertColor = AlertColor(back = R.color.light, text = R.color.light_txt)): MessageState()         {companion object {val TAG = LIGHT}}
        data class MessageDark     (val content: AlertText, val color: AlertColor = AlertColor(back = R.color.dark, text = R.color.dark_txt)): MessageState()           {companion object {val TAG = DARK}}
        object DialogShow : MessageState()
    }

    data class AlertText(
        val title: String ,
        val texts: String
    )
    data class AlertColor(
        val back: Int ,
        val text: Int
    )
    sealed class LoadingState {
        object LoadingNothing: LoadingState() //default values
        object LoadingShow   : LoadingState()
        object LoadingHide   : LoadingState()
    }
}