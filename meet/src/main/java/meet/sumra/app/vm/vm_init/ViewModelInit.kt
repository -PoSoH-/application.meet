package meet.sumra.app.vm.vm_init

import android.app.Application
import androidx.lifecycle.MutableLiveData
import meet.sumra.app.ext.default
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import meet.sumra.app.check_ref.CheckReferral
import meet.sumra.app.ext.set
import meet.sumra.app.data.pojo.ResponseRepository
import meet.sumra.app.repository.Repository
import meet.sumra.app.vm.BaseViewModel
import java.lang.RuntimeException

class ViewModelInit(appContext: Application, private val repository: Repository,
                    override val viewMessageState: MutableLiveData<MessageState> = MutableLiveData<MessageState>()
                        .default(initialValue = MessageState.MessageNothing),
                    override val viewProgressState: MutableLiveData<LoadingState> = MutableLiveData<LoadingState>()
                        .default(initialValue = LoadingState.LoadingNothing)
): BaseViewModel(appContext){

    val viewState = MutableLiveData<StateInit>().default(initialValue = StateInit.CheckSession())
    private val mCheckReferral: CheckReferral

    private var currentJob: Job

    init {
        viewState.set(StateInit.CheckSession())
        mCheckReferral = CheckReferral(appContext)
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            try {
                repository.fetchSession()
            } catch (ex: RuntimeException){
                delay(2000)
                withContext(Dispatchers.Main) {
                    viewState.set(StateInit.GoToLogin())
                    viewState.set(StateInit.Failure(ex.message.toString()))
                }
            }.let{ response ->
                when(response){
                    is ResponseRepository.RespFailureFetchSession -> {
                        withContext(Dispatchers.Main) {
                            viewState.set(StateInit.GoToLogin())
                        }
                    }
                    is ResponseRepository.ResponseRefresh-> {
                        delay(2000)
                        withContext(Dispatchers.Main) {
                            try {
                                repository.refreshToken()
                            } catch (ex: RuntimeException){
                                delay(2000)
                                withContext(Dispatchers.Main) {
                                    viewState.set(StateInit.Failure(ex.message.toString()))
                                    delay(1000)
                                    viewState.set(StateInit.GoToLogin())
                                }
                            }.let { response ->
                                when(response) {
                                    is ResponseRepository.RequestWithoutResult   -> {
                                        withContext(Dispatchers.Main) {
                                            viewState.set(StateInit.GoToLogin())
                                        }
                                    }
                                    is ResponseRepository.RequestErrorResult     -> {
                                        withContext(Dispatchers.Main) {
//                                            viewState.set(StateInit.Failure(response.message))
                                            viewMessageState.set(response.message)
                                            delay(1000)
                                            viewState.set(StateInit.GoToLogin())
                                        }
                                    }
                                    is ResponseRepository.ResponseRefreshSuccess -> {
                                        withContext(Dispatchers.Main) {
                                            viewState.set(StateInit.GoToHome())
                                        }
                                    }
                                    else -> {}
                                }
                            }
                        }
                    }
                    is ResponseRepository.RequestWithoutResult -> {
                        withContext(Dispatchers.Main) {
                            viewState.set(StateInit.GoToLogin())
                        }
                    }
                }
            }
        }
    }
}
