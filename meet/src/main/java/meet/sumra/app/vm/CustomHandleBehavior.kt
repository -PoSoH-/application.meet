package meet.sumra.app.vm

import com.futuremind.recyclerviewfastscroll.viewprovider.ViewBehavior
import android.animation.AnimatorInflater

import android.animation.AnimatorSet
import android.view.View
import androidx.annotation.AnimatorRes
import androidx.annotation.Nullable

import com.futuremind.recyclerviewfastscroll.viewprovider.VisibilityAnimationManager




class CustomHandleBehavior(
    val visibilityManager: VisibilityAnimationManager? = null,
    val grabManager: HandleAnimationManager? = null
): ViewBehavior {

//    init {
//        if (grabAnimator != -1) {
//            this.grabAnimator =
//                AnimatorInflater.loadAnimator(handle.getContext(), grabAnimator) as AnimatorSet
//            this.grabAnimator.setTarget(handle)
//        }
//        if (releaseAnimator != -1) {
//            this.releaseAnimator = AnimatorInflater.loadAnimator(
//                handle.getContext(),
//                releaseAnimator
//            ) as AnimatorSet
//            this.releaseAnimator!!.setTarget(handle)
//        }
//    }

    private var isGrabbed = false

    override fun onHandleGrabbed() {
        isGrabbed = true
        visibilityManager!!.show()
        grabManager!!.onGrab()
    }

    override fun onHandleReleased() {
        isGrabbed = false
        visibilityManager!!.hide()
        grabManager!!.onRelease()
    }

    override fun onScrollStarted() {
        visibilityManager!!.show()
    }

    override fun onScrollFinished() {
        if (!isGrabbed) visibilityManager!!.hide()
    }

    class HandleAnimationManager protected constructor(
        handle: View,
        @AnimatorRes grabAnimator: Int,
        @AnimatorRes releaseAnimator: Int
    ) {
        @Nullable
        private val grabAnimator: AnimatorSet? = null

        @Nullable
        private val releaseAnimator: AnimatorSet? = null
        fun onGrab() {
            releaseAnimator?.cancel()
            grabAnimator?.start()
        }

        fun onRelease() {
            grabAnimator?.cancel()
            releaseAnimator?.start()
        }

        class Builder(handle: View) {
            private val handle: View
            private var grabAnimator = 0
            private var releaseAnimator = 0
            fun withGrabAnimator(@AnimatorRes grabAnimator: Int): Builder {
                this.grabAnimator = grabAnimator
                return this
            }

            fun withReleaseAnimator(@AnimatorRes releaseAnimator: Int): Builder {
                this.releaseAnimator = releaseAnimator
                return this
            }

            fun build(): HandleAnimationManager {
                return HandleAnimationManager(handle, grabAnimator, releaseAnimator)
            }

            init {
                this.handle = handle
            }
        }
    }
}