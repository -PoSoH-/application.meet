package meet.sumra.app.vm.vm_statistic

import androidx.compose.runtime.MutableState
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.LineDataSet
import meet.sumra.app.data.mdl.statistic.Balances

sealed class StateStatistic {
    object Default: StateStatistic()
    data class UpdateBalances (val body: Balances): StateStatistic()
    data class UpdateEarnings (val body: MutableState<LineDataSet>): StateStatistic()
    data class UpdateReferrals(val body: MutableState<BarDataSet>): StateStatistic()
    data class UpdateContacts (val body: String): StateStatistic()
    data class UpdateRewards  (val body: String): StateStatistic()
    data class UpdateCashback (val body: MutableState<LineDataSet>): StateStatistic()
    data class UpdateRentPay  (val body: MutableState<LineDataSet>): StateStatistic()
    data class UpdateTransfer (val body: MutableState<BarDataSet>): StateStatistic()
    data class UpdateOverview (val body: MutableState<BarDataSet>): StateStatistic()
    data class Failure (val fail: String): StateStatistic()
}