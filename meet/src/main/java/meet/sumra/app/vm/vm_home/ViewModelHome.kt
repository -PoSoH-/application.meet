package meet.sumra.app.vm.vm_home

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.util.Base64
import android.util.TypedValue
import androidx.annotation.ColorInt
import androidx.compose.runtime.*
import androidx.core.content.edit
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.github.mikephil.charting.data.*
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.gson.Gson
import kotlinx.coroutines.*
import meet.sumra.app.AppMeet
import meet.sumra.app.CONST
import meet.sumra.app.R
import meet.sumra.app.adptr.*
import meet.sumra.app.adptr.menus.AMenuChartsInterval
import meet.sumra.app.adptr.menus.AdapterMeetCalendar
import meet.sumra.app.check_ref.DEV
import meet.sumra.app.check_ref.REF
import meet.sumra.app.check_ref.VAL
import meet.sumra.app.data.enums.EDialog
import meet.sumra.app.data.enums.ELinkToSend
import meet.sumra.app.data.enums.EPlazaType
import meet.sumra.app.data.enums.EUType
import meet.sumra.app.data.mdl.*
import meet.sumra.app.data.mdl.earnings.cards
import meet.sumra.app.data.mdl.plaza.*
import meet.sumra.app.ext.default
import meet.sumra.app.ext.set
import meet.sumra.app.interfaces.SelectDrawerItemListener
import meet.sumra.app.data.pojo.ResponseRepository
import meet.sumra.app.data.pojo.contacts.ContactHoldCompose
import meet.sumra.app.data.pojo.contacts.ContactHoldState
import meet.sumra.app.data.pojo.contacts.UserContactCreate
import meet.sumra.app.interfaces.ContactOperation
import meet.sumra.app.interfaces.OnContactInviteListener
import meet.sumra.app.interfaces.sealeds.SChartInterval
import meet.sumra.app.repository.Repository
import meet.sumra.app.utils.ContactConfig
import meet.sumra.app.utils.openPhoto
import meet.sumra.app.vm.BaseViewModel
import meet.sumra.app.vm.Dialogue
import meet.sumra.app.vm.RecentHome
import net.sumra.auth.helpers.Finals
import sdk.net.meet.api.contacts.response.FetchContact
import sdk.net.meet.api.meet_calendar.response.MeetCalendarAttr
import sdk.net.meet.api.meet_profile.response.MeetUserInfo
import sdk.net.meet.api.referral.objModelReferralCode
import xyn.meet.storange.models.SessionEntity
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.lang.RuntimeException
import java.util.*
import kotlin.random.Random

class ViewModelHome (private val application: AppMeet,
                     private val repository : Repository,
                     override val viewMessageState: MutableLiveData<MessageState> = MutableLiveData<MessageState>()
                         .default(initialValue = MessageState.MessageNothing),
                     override val viewProgressState: MutableLiveData<LoadingState> = MutableLiveData<LoadingState>()
                         .default(initialValue = LoadingState.LoadingNothing)
): BaseViewModel(application){

    val viewState = MutableLiveData<StateHome>().default(initialValue = StateHome.Default())
//    private val mCheckReferral: CheckReferral

    lateinit var viewStatePrfl : MutableLiveData<StatePrfl>      // ().default(initialValue = StatePrfl.Default)

    lateinit var viewStateCldr : MutableLiveData<StateCalendar>     // ().default(initialValue = StatePrfl.Default)
    lateinit var viewStatePlaza : MutableLiveData<StateBonusPlaza>  // ().default(initialValue = StatePrfl.Default)

    lateinit var stateReferrals: MutableLiveData<StateReferral>
    lateinit var stateRewards  : MutableLiveData<StateRewards>
    lateinit var stateEarnings  : MutableLiveData<StateEarnings>
    lateinit var stateContacts : MutableLiveData<StateContacts>
    lateinit var viewMeetAdd   : MutableLiveData<StateMeetAdd> //().default(initialValue = StateMeetAdd.MeetAddDft)

    lateinit var stateRecent: MutableLiveData<RecentHome>

    private val applicationSettings: DtAppSetup

    lateinit var dateAdapter: AdapterBirthday

    private lateinit var currentJob: Job
    private var editAction: TypEdCode = TypEdCode.Nothing
    internal var urlTempAvatar: Uri? = null
        set(url : Uri?) {field = url}
        get() = field

    private val sessionActive = booleanArrayOf(false)
    private val adapterDrawerMenu  : AdapterDrawerMenuHome
//    private lateinit var adapterInviteSelect: AdapterInviteSelect
//    private lateinit var adapterContacts    : AdapterContacts

//    private lateinit var contactOperation   : OnContactInviteListener

    private lateinit var mChartLineInterval : AMenuChartsInterval
    private lateinit var mChartOtherInterval: AMenuChartsInterval

    private var sessionEntity: SessionEntity? = null

    private val adapterCalendar = AdapterMeetCalendar()
    private lateinit var adapterDateTime: AdapterSelectDateTime//(application)

    val isInvitedContacts = mutableStateListOf<ContactHoldCompose>(); get() = field
    val contactLoaded = mutableStateListOf<ContactHoldCompose>(); get() = field
    val contactFilter = mutableStateListOf<ContactHoldCompose>(); get() = field
    val letter        = mutableStateListOf<String>(); get() = field
    val isSearch = mutableListOf(false); get() = field
    val isInvite = mutableStateOf(false); get() = field
    lateinit var contactPagination: MutableState<ContactDataPagination>;

    internal val isCntcAddExpMode = ButtonState()
        get() {return field}

    fun updateTypeFace() : Typeface {
        application.run {
            return Typeface.createFromAsset(this.assets, Finals.FONT_FACE)
        }
    }

    internal fun adapterCldr() = adapterCalendar

    init {

        val fer = REF(); fer.read(application)
        val ved = DEV(); ved.read(application)
        val neo = VAL(fer, ved)
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            val resp = repository.sendReferralData(neo)
//            withContext(Dispatchers.Main){
//                when(resp){
//                    else ->
//                }
//            }
        }

        val tmp = application
            .getSharedPreferences(CONST.APP_SETTING, Context.MODE_PRIVATE)
            .getString(
                DtAppSetup.SETUP_KEY,
                Gson().toJson(DtAppSetup(UUID.randomUUID().toString()))).toString()

        applicationSettings = Gson().fromJson(tmp, DtAppSetup::class.java)
        adapterDrawerMenu = AdapterDrawerMenuHome(selectListener())

        fetchUserProfile(EUType.FIRST)
//        adapterInviteSelect = AdapterInviteSelect()
//        adapterContacts = AdapterContact
    }

    internal fun initReferral(){
        stateReferrals = MutableLiveData<StateReferral>()
            .default(initialValue = StateReferral.DefaultReferralCodes())
    }

    internal fun initRewards(){
        mChartLineInterval = AMenuChartsInterval(object :AMenuChartsInterval.IMenuItemUpdate{
            override fun selectItemMenu(item: SChartInterval) {
                mChartLineInterval.setNewItemSelect(item)
                chartDataLinearUpd()
                stateRewards.set(StateRewards.HideLinearMenu(item.itemName().toString()))
            }})
        mChartOtherInterval = AMenuChartsInterval(object :AMenuChartsInterval.IMenuItemUpdate{
            override fun selectItemMenu(item: SChartInterval) {
                mChartOtherInterval.setNewItemSelect(item)
                chartDataBoxPieUpd()
                stateRewards.set(StateRewards.HideBoxPieMenu(item.itemName().toString()))
            }})

        stateRewards = MutableLiveData<StateRewards>()
            .default(initialValue = StateRewards.Default())
    }

    internal fun initEarnings(){
//        mEarningLineInterval = AMenuChartsInterval(object :AMenuChartsInterval.IMenuItemUpdate{
//            override fun selectItemMenu(item: SChartInterval) {
//                mChartLineInterval.setNewItemSelect(item)
//                chartDataLinearUpd()
//                stateRewards.set(StateRewards.HideLinearMenu(item.itemName().toString()))
//            }})

        stateEarnings = MutableLiveData<StateEarnings>()
            .default(initialValue = StateEarnings.Default)
    }

    internal fun getChartLineAdapter()  = mChartLineInterval
    internal fun getChartOtherAdapter() = mChartOtherInterval

    internal fun initContact(){
        stateContacts = MutableLiveData<StateContacts>()
            .default(initialValue = StateContacts.Default())

//        contactOperation = object:OnContactInviteListener{
//            override fun selectItemOperation(c: ContactOperation) {
//                when(c){
//                    is ContactOperation.UnInvite -> {
//                        adapterInviteSelect.delInvite(c.c)
//                        adapterContacts.unSelectByInvite(c.c)
//                        if(adapterInviteSelect.isEmpty()){
//                            stateContacts.set(StateContacts.HideInvite())
//                        }
//                    }
//                    is ContactOperation.ToInvite -> {
////                        adapterContacts.unSelectInvite(c.c)
//                        if(adapterInviteSelect.isEmpty()){
//                            stateContacts.set(StateContacts.ShowInvite())
//                        }
//                        adapterInviteSelect.putInvite(c.c)
//                    }
//                }
//            }
//
//            override fun updatePagination() {
//                updateListContactByPagination()
//            }
//        }
//
//        adapterInviteSelect = AdapterInviteSelect(contactOperation)
//        adapterContacts     = AdapterContacts(contactOperation)
        dateAdapter         = AdapterBirthday(application)

        fetchAllContacts()
    }

    internal fun initProfile(){
        viewStatePrfl = MutableLiveData<StatePrfl>().default(initialValue = StatePrfl.Default)
        fetchUserProfile(EUType.LOADED)
    }

    internal fun initCalendar(){
        viewStateCldr = MutableLiveData<StateCalendar>()
            .default(initialValue = StateCalendar.CalendarDft)
        fetchCalendar()
    }

    internal fun initMeetAdd(){
        adapterDateTime = AdapterSelectDateTime(application)
        val participantOperation = object:OnContactInviteListener{
            override fun selectItemOperation(c: ContactOperation) {
                when(c){
                    is ContactOperation.UnInvite -> {
                        adapterInviteParticipantSelect.delInvite(c.c)
                        adapterParticipants.unSelectByInvite(c.c)
                        viewMeetAdd.set(StateMeetAdd
                            .MeetParticipantCount(adapterInviteParticipantSelect.itemCount))
                    }
                    is ContactOperation.ToInvite -> {
                        adapterInviteParticipantSelect.putInvite(c.c)
                        viewMeetAdd.set(StateMeetAdd
                            .MeetParticipantCount(adapterInviteParticipantSelect.itemCount))
                    }
                }
            }
            override fun updatePagination() {
//                updateListParticipantByPagination()
            }
        }
        adapterParticipants = AdapterParticipants(participantOperation, updateTypeFace())
        adapterInviteParticipantSelect = AdapterInviteSelect(participantOperation)

        viewMeetAdd = MutableLiveData<StateMeetAdd>()
            .default(initialValue = StateMeetAdd.MeetAddDft(adapterDateTime.isStatePM))
    }

    internal fun initBonusPlaza(){
        viewStatePlaza = MutableLiveData<StateBonusPlaza>()
            .default(initialValue = StateBonusPlaza.BonusPlazaDft)
    }

    internal fun initRecent() {
        stateRecent = MutableLiveData<RecentHome>()
            .default(initialValue = RecentHome.Default)
    }

    internal val viewStateSetup = MutableLiveData<StateAppSetup>().default(initialValue = StateAppSetup.SetupDefault)

    internal fun isVideoSetup() = applicationSettings.stateVideo
    internal fun isVoiceSetup() = applicationSettings.stateVoice
    internal fun isDarkModeSetup() = applicationSettings.stateDarkMode
    internal fun isAdditionalSetup() = applicationSettings.stateAdditional

    internal fun fetchCreateDateTime() = Calendar.getInstance().time.toString()

    internal fun fetchStartDateTime() = adapterDateTime.fetchStartDateTime()
    internal fun fetchFinalDateTime() = adapterDateTime.fetchFinalDateTime()
    internal fun fetchOwnerId() = repository.fetchOwnerId()
    internal fun fetchMeetParticipants() = adapterInviteParticipantSelect.fetchAllParticipants()

    // settings setup set block
    internal fun inVideoSetup(st: Boolean){
           applicationSettings.stateVideo = st
           viewState.set(StateHome.UpdateVideoAppBar())
           insertSettings()
    }
    internal fun inVideoAppBar(st: Boolean){
        applicationSettings.stateVideo = st
        viewStateSetup.set(StateAppSetup.UpdateVideo)
        insertSettings()
    }
    internal fun inVoiceSetup(st: Boolean){
        applicationSettings.stateVoice = st
//        repository.inVoice(st)
        insertSettings()
    }
    internal fun inDarkModeSetup(st: Boolean) {
        applicationSettings.stateDarkMode = st
//        repository.inDarkMode(st)
        insertSettings()
    }
    internal fun inAdditionalSetup(st: Boolean) {
        applicationSettings.stateAdditional = st
//        repository.inAdditional(st)
        insertSettings()
    }
    private fun insertSettings(){
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            application.getSharedPreferences(CONST.APP_SETTING, Context.MODE_PRIVATE).edit {
                putString(DtAppSetup.SETUP_KEY, Gson().toJson(applicationSettings)).commit()
            }
        }
    }

    internal fun getBirthDay() = dateAdapter.getBirthDay()

//    internal fun getAdapterContact() = adapterContacts
//
//    internal fun getAdapterInvite() = adapterInviteSelect

/***   ***   Section add contacts   ***   ***/
    @Composable
    @ExperimentalPagerApi
    internal fun fetchYearsSetup() {
        return dateAdapter.adapterYear()
    }
    @Composable
    @ExperimentalPagerApi
    internal fun fetchMonthsSetup() {
        return dateAdapter.adapterMonth()
    }
    @Composable
    @ExperimentalPagerApi
    internal fun fetchDaysSetup() {
        return dateAdapter.adapterDay()
    }
/***   ***   ***   Section add meet   ***   ***   ***/
    @Composable
    @ExperimentalPagerApi
    internal fun fetchTestShowYAdptr(){
        return adapterDateTime.GetViewSelectYPicker()
    }
    @Composable
    @ExperimentalPagerApi
    internal fun fetchTestShowMAdptr(){
        return adapterDateTime.GetViewSelectMPicker()
    }
    @Composable
    @ExperimentalPagerApi
    internal fun fetchTestShowDAdptr(){
        return adapterDateTime.GetViewSelectDPicker()
    }
    @Composable
    @ExperimentalPagerApi
    internal fun fetchTestShowHourAdptr(){
        return adapterDateTime.GetViewSelectHourPicker()
    }
    @Composable
    @ExperimentalPagerApi
    internal fun fetchTestShowMinAdptr(){
        return adapterDateTime.GetViewSelectMinPicker()
    }

    @Composable
    @ExperimentalPagerApi
    internal fun fetchTestShowTermHourAdptr(){
        return adapterDateTime.GetViewSelectTermHourPicker()
    }
    @Composable
    @ExperimentalPagerApi
    internal fun fetchTestShowTermMinAdptr(){
        return adapterDateTime.GetViewSelectTermMinPicker()
    }


    @Composable
    internal fun fetchTestAdptr(){
        return adapterDateTime.HorizontalScrollableComponent()
    }
    @Composable
    internal fun fetchMontchAdptr(){
        return adapterDateTime.getViewNumberPicker()
    }

/**  --------------------------  */

    private fun selectListener() = object: SelectDrawerItemListener {
        override fun selected(position: Int, state: StateHome) {
            adapterDrawerMenu.updateSelectPosition(position)
                viewState.set(state)
        }
    }

    internal fun onBackClickListener() {
        viewState.set(StateHome.BackPress())
    }

    fun logout(){
        viewModelScope.launch (Dispatchers.IO){
            try{
                repository.deleteSession()
            }catch(ex: Throwable){
                when(ex){
                    is CancellationException -> {  }
                    is RuntimeException      -> {  }
                    else                     -> {  }
                }
            }.let{
                withContext(Dispatchers.Main){
                    when (it) {
                        is ResponseRepository.RequestErrorResult     -> {
                            viewState.set(StateHome.Logout())
                        }
                        is ResponseRepository.ResponseRefreshSuccess -> {
                            viewState.set(StateHome.Logout())
                        }
                    }
                }
            }
        }
    }

    fun getAdapter() = adapterDrawerMenu

    internal fun updateMessageSate(state: MessageState){
        viewMessageState.set(state)
    }

    internal fun showSuccessMessage(text: AlertText){
        viewMessageState.set(MessageState.MessageSuccess(text))
    }

    internal fun showFailureMessage(text: AlertText){
        viewMessageState.set(MessageState.MessageDanger(text))
    }

    internal fun showErrorMessage(text: String){
        viewState.set(StateHome.Failure(text))
    }

    internal fun closePreviewCamera() {
        viewState.set(StateHome.Preview(false))
    }

    internal fun openPreviewCamera() {
        viewState.set(StateHome.Preview(true))
    }

    internal fun resetState() {
        adapterDrawerMenu.updateSelectPosition(0)
        viewState.set(StateHome.Default())
    }

    internal fun switchToPioneer(){
        viewState.set(StateHome.Pioneer())
    }

    internal fun updateDrawerMenu(){
        adapterDrawerMenu.updateStatePosition(viewState.value!!)
    }

    /**    request of referrals    **/

    internal fun onShowDialogEditProfile(){
        editAction = TypEdCode.CreateCode
//        viewState.set(StateHome.ShowDialogUser(editAction,null, null, null))
    }

    internal fun onShowDialogEditNewCode() {
        editAction = TypEdCode.CreateCode
        stateReferrals.set(StateReferral.EditedReferralCode(editAction,null, null, null))
    }

    internal fun onShowDialogEditNewMeet() {
        viewState.set(StateHome.CalendarMeetAdd)
    }

    internal fun showDialogCreateMeet() {
        viewState.set(StateHome.ForDialogue(
            dialogue = Dialogue.Default(
                type = EDialog.MEET_CREATE,
                isAction = true)))
    }

    internal fun showDialogJoinMeet() {
        viewState.set(StateHome.ForDialogue(
            dialogue = Dialogue.Default(
                type = EDialog.MEET_JOIN,
                isAction = true)))
    }

    internal fun onHelpDialogHide() {
        viewState.set(StateHome.FragmentEmpty)
    }

    internal fun onShareSocial(code: String, link: String){
        viewState.set(StateHome.ShareSocial(code, link))
    }

    private fun showLoadingView(){
        viewProgressState.set(LoadingState.LoadingShow)
    }
    private fun hideLoadingView(){
        viewProgressState.set(LoadingState.LoadingHide)
    }

    internal fun updateCalculateEarning(){
        showLoadingView()
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            try{
                repository.updateDataEarning()
            }catch (th: Throwable){
                withContext(Dispatchers.Main){
                    hideLoadingView()
                }
            }.let { earnings ->
                val data = earnings as _AppEarning
                var i = 0
                var b = 0
                var r = 0
                data.bonuses.forEach {
                    i += it.count
                    b = it.bonus
                    r += r + (it.bonus * it.count)
                }
                delay(1382)
                withContext(Dispatchers.Main){
                    stateReferrals.set(
                        StateReferral.UpdateEarningsCalc(
                            invoited = i.toString(),
                            bonus = b.toString(),
                            result = r.toString(),
                            cur = data.currency))
                    hideLoadingView()
                }
            }
        }
    }

    internal fun updateDataReferral(){
        showLoadingView()
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            val result = repository.updateDataReferrals()
            withContext(Dispatchers.Main) {
                when (result) {
                    is ResponseRepository.RequestErrorResult -> {
                        updateMessageSate(result.message)
                    }
                    is ResponseRepository.RespFetchRefCodes -> {
                        stateReferrals.set(StateReferral.UpdatesReferralCodes(result.codes))
                    }
                }
                hideLoadingView()
            }
        }
    }

    internal fun dataReferralClear() {
        repository.dataReferralClear()
    }

    internal fun updateReferralsAfterDelete(removedID: String){
        showLoadingView()
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            val result = repository.updateDataReferrals()
            withContext(Dispatchers.Main) {
                when (result) {
                    is ResponseRepository.RequestErrorResult -> {
                        updateMessageSate(result.message)
                    }
                    is ResponseRepository.RespFetchRefCodes -> {
                        var position = 0
                        result.codes.forEachIndexed {i, it ->
                            if(it.id.equals(removedID)){
                                position = i
                            }
                        }
                        result.codes.removeAt(position)
                        stateReferrals.set(StateReferral.UpdatesReferralCodes(result.codes))
                    }
                }
                hideLoadingView()
            }
        }
    }

    internal fun showEditCurrentRefCode(id: String){ //, note: String, isDefault: String){
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            val data = repository.updateDataReferrals()
            withContext(Dispatchers.Main){
                when(data){
                    is ResponseRepository.RespFetchRefCodes -> {
                        data.codes.forEach {
                            if(it.id.equals(id)){
                                editAction = TypEdCode.EditedCode
                                stateReferrals.set(StateReferral.EditedReferralCode(
                                    isEdited  = editAction,
                                    id        = id,
                                    note      = it.note,
                                    isDefault = it.isDefault))
                            }
                        }
                    }
                    is ResponseRepository.RequestErrorResult -> {
                        updateMessageSate(/*AlertText(title = "", texts = */data.message)
                    }
                }

            }
        }
    }

    internal fun onClickButtonSubmit(notes: String, isDefault: Boolean, id: String?){
        when(editAction){
            is TypEdCode.EditedCode -> {
                onUpdateReferralCode(id!!, notes, isDefault)
            }
            is TypEdCode.CreateCode -> {
                onCreateNewReferralCode(notes, isDefault)
            }
            else -> Unit
        }
    }

    private fun onCreateNewReferralCode(notes: String, isDefault: Boolean){
        showLoadingView()
        currentJob = viewModelScope.launch(Dispatchers.IO){
            val data = repository.onCreateNewReferralCode(notes, isDefault)
            withContext(Dispatchers.Main){
                when(data){
                    is ResponseRepository.TaskCreateReferralCode -> {
                        updateMessageSate(data.message)

                    }
                    is ResponseRepository.RequestErrorResult  -> { updateMessageSate(data.message) }
                }
                hideLoadingView()
            }
        }
    }

    internal fun onUpdateReferralCode(referralID: String, notes: String, isDefault: Boolean){
        showLoadingView()
        currentJob = viewModelScope.launch(Dispatchers.IO){
            val result = repository.onUpdateReferralCode(referralID, notes, isDefault)
            withContext(Dispatchers.Main) {
                when (result) {
                    is ResponseRepository.RequestErrorResult -> {
                        updateMessageSate(result.message)
                    }
                    is ResponseRepository.RespFetchRefCodes  -> {
                        stateReferrals.set(StateReferral.UpdatesReferralCodes(result.codes))
                    }
                }
                hideLoadingView()
            }
        }
    }

    internal fun onRemoveReferralCode(referralID: String){
        showLoadingView()
        currentJob = viewModelScope.launch(Dispatchers.IO){
            val resilt = repository.onRemoveReferralCode(referralID)
            withContext(Dispatchers.Main){
                when(resilt){
                    is ResponseRepository.RequestErrorResult -> {
                        updateMessageSate(resilt.message)
                    }
                    is ResponseRepository.RespDeleteRefCodes -> {
                        stateReferrals.set(StateReferral.UpdateAfterDelete(resilt.idCode))
                    }
                }
                hideLoadingView()
            }
        }
    }
/**   Contacts block   */
    internal fun fetchAllCategories(){
        showLoadingView()
        currentJob = viewModelScope.launch (Dispatchers.IO){
            val tmp =  repository.fetchContactCategories()
            withContext(Dispatchers.Main) {
                when (tmp) {
                    is ResponseRepository.RequestErrorResult -> {
                        updateMessageSate(tmp.message)
                    }
                    is ResponseRepository.TaskContactCategory -> {
                        updateMessageSate(tmp.message)
                    }
                }
                hideLoadingView()
            }
        }
    }

    internal fun fetchAllContacts(){
        fetchAllContacts(ContactConfig.getByPagination(1))
    }

//    private fun updateListContactByPagination(){
//        adapterContacts.getNextPage()?.let{
//            fetchAllContacts(it)
//        }
//    }

    private fun fetchAllContacts(config: ContactConfig){
        showLoadingView()
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            val data = repository.fetchAllContacts(isSearch[0], config)
            withContext(Dispatchers.Main) {
                when (data) {
                    is ResponseRepository.TaskContactsUpdate -> {
                        letter.clear()
                        letter.addAll(data.letters)
                        contactLoaded.clear()
                        data.contacts.forEachIndexed { indx, cnct ->
                            cnct.avatar = updateUserAvatar(Random.nextInt(0, 9))
                            contactLoaded.add(ContactHoldCompose(cnct))
                        }
                        contactFilter.clear()
                        contactFilter.addAll(contactLoaded)

                        contactPagination = mutableStateOf(data.pagination!!)

//                        val temp = mutableMapOf<String, MutableList<ContactHoldState>>()
//                        data.letters.forEach { letter ->
//                            temp.put(letter, mutableListOf<ContactHoldState>())
//                        }
//                        val other = mutableListOf<ContactHoldState>()
//                        val fullTmp = mutableListOf<MutableList<ContactHoldState>>()
//                        data.letters.forEach {letter ->
//                            data.contacts.forEachIndexed { indx, cnct ->
//                                if (cnct.displayName.isNullOrEmpty()) {
//                                    if (cnct.firstName.isNullOrEmpty()) {
//                                        if (cnct.lastName.isNullOrEmpty()) {
//                                            cnct.avatar = updateUserAvatar(Random.nextInt(0, 9))
//                                            other.add(ContactHoldState(contact = cnct))
//                                        } else {
//                                            if(cnct.lastName[0].lowercase().equals(letter[0].lowercase())) {
//                                                cnct.avatar = updateUserAvatar(Random.nextInt(0, 9))
//                                                temp.get(letter)!!.add(ContactHoldState(contact = cnct))
//                                            }
//                                        }
//                                    } else {
//                                        if(cnct.firstName[0].lowercase().equals(letter[0].lowercase())) {
//                                            cnct.avatar = updateUserAvatar(Random.nextInt(0, 9))
//                                            temp.get(letter)!!.add(ContactHoldState(contact = cnct))
//                                        }
//                                    }
//                                } else {
//                                    if(cnct.displayName[0].lowercase().equals(letter[0].lowercase())){
//                                        cnct.avatar = updateUserAvatar(Random.nextInt(0, 9))
//                                        temp.get(letter)!!.add(ContactHoldState(contact = cnct))
//                                    }
//                                }
////                                cnct.avatar = updateUserAvatar(Random.nextInt(0, 9))
////                                temp.add(ContactHoldState(contact = cnct))
//                            }
//                        }
//                        data.letters.forEach { letter ->
//                            fullTmp.add(temp.get(letter)!!)
//                        }
//                        if(!other.isNullOrEmpty()){
//                            fullTmp.add(other!!)
//                        }
//                        adapterContacts.updateContacts(fullTmp, data.letters)
////                        stateContacts.set(StateContacts.Updates(data.contacts))
//                        data.pagination?.apply {
//                            adapterContacts.updatePagination(this)
//                        }
                    }
                    is ResponseRepository.RequestErrorResult -> {
                        showFailureMessage(AlertText(title = "",
                            texts = data.message.toString()))
                    }
                }
                hideLoadingView()
                isSearch[0] = false
            }
        }
    }

    companion object{
        fun updateUserAvatar(q: Int):String{
            return when(q) {
                0 -> { "https://images.unsplash.com/photo-1570295999919-56ceb5ecca61?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80" }
                1 -> { "https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80" }
                2 -> { "https://images.unsplash.com/photo-1531427186611-ecfd6d936c79?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80" }
                3 -> { "https://images.unsplash.com/photo-1573496359142-b8d87734a5a2?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=688&q=80" }
                4 -> { "https://images.unsplash.com/photo-1614436163996-25cee5f54290?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=742&q=80" }
                5 -> { "https://images.unsplash.com/photo-1560250097-0b93528c311a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80" }
                6 -> { "https://images.unsplash.com/photo-1568602471122-7832951cc4c5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" }
                7 -> { "https://images.unsplash.com/photo-1573497019940-1c28c88b4f3e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=687&q=80" }
                8 -> { "https://images.unsplash.com/photo-1492562080023-ab3db95bfbce?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1148&q=80" }
                else -> { "https://images.unsplash.com/photo-1522075469751-3a6694fb2f61?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=880&q=80" }
            }
        }
    }

    internal fun fetchSearchContacts(config: ContactConfig){
        isSearch[0] = true
        showLoadingView()
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            val data = repository.fetchSearchContacts(config)
            withContext(Dispatchers.Main) {
                when (data) {
                    is ResponseRepository.TaskContactsUpdate -> {
                        val temp = mutableListOf<ContactHoldState>()
                        data.contacts.forEach { cnct ->
                            temp.add(ContactHoldState(contact = cnct))
                        }
//                        adapterContacts.updateContacts(temp, letters = data.letters)
                        data.pagination?.apply {
//                            adapterContacts.updatePagination(this)
                        }
                    }
                    is ResponseRepository.RequestErrorResult -> {
                        showFailureMessage(
                            text = AlertText(title = "", texts = data.message.toString()))
                    }
                }
                hideLoadingView()
            }
        }
    }

    internal fun createNewContact(create: UserContactCreate){
        showLoadingView()
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            create.uAvatar = preparingPhotoUpload()
            val tmp = repository.createNewContact(urlTempAvatar!!, create)
            withContext(Dispatchers.Main) {
                when (tmp) {
                    is ResponseRepository.RequestErrorResult -> {
                        updateMessageSate(tmp.message)
                    }
                    is ResponseRepository.TaskContactCreate -> {
                        fetchAllContacts()
                        updateMessageSate(tmp.message)
                    }
                }
                hideLoadingView()
            }
        }
    }

    private fun preparingPhotoUpload(): String{
        val imageString = StringBuilder()
        urlTempAvatar?.let {
            val baos: InputStream? = application.openPhoto(it)
            baos?.let { stream ->
                val bitmap = BitmapFactory.decodeStream(stream)
                val out = ByteArrayOutputStream()
                bitmap?.run {
                    compress(Bitmap.CompressFormat.JPEG, 75, out)
                    val imageBytes: ByteArray = out.toByteArray()
                    imageString.append(Base64.encodeToString(imageBytes, Base64.DEFAULT))
                }
            }
        }
        return imageString.toString()
    }

    /** Room create **/
    internal fun switchToInvite(type: ELinkToSend,  data: String, pass: String? = null) {
        viewState.set(StateHome.Invite(type = type, data = data, pass = pass))
    }

    /** Charts sections **/

    internal fun chartDataLinearUpd(){
        showLoadingView()
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            val result = repository.updateDataLineChart(mChartLineInterval.getCurrentSelect())
            withContext(Dispatchers.Main){
                when(result){
                    is ResponseRepository.TaskChartLinearUdt -> {
                        updateMessageSate(result.message)
                        stateRewards
                            .set(StateRewards.UpdateLines(result.content))}
                    is ResponseRepository.RequestErrorResult -> {
                        updateMessageSate(result.message)
                    }
                }
                hideLoadingView()
            }
        }
    }

    internal fun chartDataBoxPieUpd(){
        showLoadingView()
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            val bar = repository.updateDataBarChart(mChartOtherInterval.getCurrentSelect())
            withContext(Dispatchers.Main){
                when(bar){
                    is ResponseRepository.TaskChartBarUpdate -> {
                        updateMessageSate(bar.message)
                        stateRewards
                            .set(StateRewards.UpdateBars(bar.content))
                    }
                    is ResponseRepository.RequestErrorResult -> {
                        updateMessageSate(bar.message)
                    }
                }
                hideLoadingView()
            }
        }
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            val pie = repository.updateDataPieChart(mChartOtherInterval.getCurrentSelect())
            withContext(Dispatchers.Main){
                when(pie){
                    is ResponseRepository.TaskChartHalfPieUdt -> {
                        updateMessageSate(pie.message)
                        stateRewards
                            .set(StateRewards.UpdatePies(pie.content))
                    }
                    is ResponseRepository.RequestErrorResult -> {
                        updateMessageSate(pie.message)
                    }
                }
                hideLoadingView()
            }
        }
    }

    /** *** Profile update section ************************************************************* **/
    private fun fetchUserProfile(et: EUType){
        showLoadingView()
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            val res = repository.featchUserProfile()
            withContext(Dispatchers.Main){
                hideLoadingView()
                when(res){
                    is ResponseRepository.PrflSuccess -> {
                        when(et){
                            EUType.LOADED -> {
                                viewStatePrfl.set(StatePrfl.Success(res.content))
//                                stateRecent?.let {
//
//                                }
                                updateMessageSate(state = MessageState
                                    .MessageSuccess(content = AlertText(
                                        title = "User profile",
                                        texts = "Loaded user profile success!..")))
                            }
                            EUType.FIRST  -> {
                                viewState.set(StateHome.DrawerUpdate(res.content))
                                updateMessageSate(state = MessageState
                                    .MessageSuccess(content = AlertText(
                                        title = "User profile",
                                        texts = "Loaded user profile success!..")))
                            }
                        }
                    }
                    is ResponseRepository.RequestErrorResult -> {
                        showErrorMessage("Error downloaded user...")
                    }
                }
            }
        }
    }

    /***   ***   ---====|||||<  _Calendar_  >|||||====---   ***   ***/

    private fun fetchCalendar(){
        showLoadingView()
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            val _t = repository.fetchCalendar()
            withContext(Dispatchers.Main){
                when(_t){
                    is ResponseRepository.RequestCalendarGet -> {
                        adapterCalendar.updateCalendarScheduler(_t.content)
                    }
                    is ResponseRepository.RequestErrorResult -> {
                        showFailureMessage( AlertText(title = "", texts = _t.message.toString()) )
                    }
                }
                hideLoadingView()
            }
        }
    }

    private lateinit var adapterParticipants: AdapterParticipants
    private lateinit var adapterInviteParticipantSelect: AdapterInviteSelect

    fun getParticipantAdapter() = adapterParticipants
    fun getInviteParticipantAdapter() = adapterInviteParticipantSelect

    /***   000   ***/
    internal fun fetchParticipants() {
        showLoadingView()
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            val data = repository.fetchAllParticipants(
                config = ContactConfig.getByPagination(1))
            withContext(Dispatchers.Main) {
                when (data) {
                    is ResponseRepository.TaskMeetGet -> {
                        val temp = mutableListOf<ContactHoldState>()
                        data.contacts.forEach { cnct ->
                            temp.add(ContactHoldState(contact = cnct))
                        }
                        adapterParticipants.updateContacts(temp)
                    }
                    is ResponseRepository.RequestErrorResult -> {
                        showFailureMessage(AlertText(title = "",
                            texts = data.message.toString()))
                    }
                }
                hideLoadingView()
            }
        }
    }

    internal fun participantClear(){
        adapterInviteParticipantSelect.clear()
    }

    internal fun createNewMeetParticipants(c: MeetCalendarAttr){
        showLoadingView()
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            val t = repository.newMeetGET(c)
            withContext(Dispatchers.Main) {
                hideLoadingView()
                when (t) {
                    is ResponseRepository.RequestCalendarGet -> {
                        updateMessageSate(t.message)
                        adapterCalendar.updateCalendarScheduler(t.content)
                        viewMeetAdd.set(StateMeetAdd.MeetAddPst)
                    }
                    is ResponseRepository.RequestErrorResult -> {
                        updateMessageSate(t.message)
                    }
                }
            }
        }
    }

/****    00 Bonus Plaza 00    ****/

    internal fun fetchAllTabBonusPlaza() = listOf(
        TabPlazaShow(text = R.string.frgPlazaRewardBonus,  icon = R.drawable.ic_plaza_tab_reward_bonus),
        TabPlazaShow(text = R.string.frgPlazaBonusChart,  icon = R.drawable.ic_plaza_tab_reward_bonus),
        TabPlazaShow(text = R.string.frgPlazaTokenCharts,  icon =  R.drawable.ic_plaza_tab_token_chart),
        TabPlazaShow(text = R.string.frgPlazaHoeToRewards, icon =  R.drawable.ic_plaza_tab_how_to_get))

    internal fun fetchRewardBonus(){
        showLoadingView()
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            val t = repository.fethRewardBonus()
            withContext(Dispatchers.Main) {
                when (t) {
                    is ResponseRepository.RewardBonusGet -> {
                        viewStatePlaza.set(StateBonusPlaza.RewardBonusGet(t.body))
                    }


                    is ResponseRepository.RequestErrorResult -> {
//                    showFailureMessage(text = t.message)
                    }
                }
                hideLoadingView()
            }
        }
    }

    private val stateStatisticChart = mutableListOf<cards.EPeriod>()
    private fun updateStatisticChart(state: Int){
        when(state){
            EPlazaType.DAY.pItem ->   {stateStatisticChart.apply{clear(); add(cards.EPeriod.DAY)}}
            EPlazaType.WEEK.pItem ->  {stateStatisticChart.apply{clear(); add(cards.EPeriod.WEEK)}}
            EPlazaType.MONTH.pItem -> {stateStatisticChart.apply{clear(); add(cards.EPeriod.MONTH)}}
            EPlazaType.YEAR.pItem ->  {stateStatisticChart.apply{clear(); add(cards.EPeriod.YEAR)}}
        }
    }

    internal fun fetchRewardChart(state: Int){
        updateStatisticChart(state = state)
        showLoadingView()
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            val t = repository.fethRewardChart(stateStatisticChart[0])
            withContext(Dispatchers.Main) {
                when (t) {
                    is ResponseRepository.RewardChartGet -> {
                        viewStatePlaza.set(StateBonusPlaza.RewardChartGet(
                            body = t.body, stat = t.statistic
                        ))
                    }
                    is ResponseRepository.RequestErrorResult -> {
//                    showFailureMessage(text = t.message)
                    }
                }
                hideLoadingView()
            }
        }
    }

    internal fun fetchRewardHowTo(){
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            val t = repository.fethRewardHowTo()
            withContext(Dispatchers.Main) {
                when (t) {
                    is ResponseRepository.RewardHowToGet -> {
                        viewStatePlaza.set(StateBonusPlaza.HowToRewardGet(t.body))
                    }
                    is ResponseRepository.RequestErrorResult -> {
//                    showFailureMessage(text = t.message)
                    }
                }
            }
        }
    }


/****    0000    ****/
    internal val isNewMeetPassShow = mutableListOf(false)
            get() = field
//    internal fun getShowNewMeetPassword() = isNewMeetPassShow

    private lateinit var gradient: GradientDrawable
    internal fun fetchGradient(): GradientDrawable {
        val typedValue = TypedValue()
        val theme: Resources.Theme = application.getTheme()
        theme.resolveAttribute(R.attr.meetBarStartColor, typedValue, true)
        @ColorInt val startColor: Int = typedValue.data
        theme.resolveAttribute(R.attr.meetBarFinalColor, typedValue, true)
        @ColorInt val finalColor: Int = typedValue.data
        val gradientBackgroundList = intArrayOf(startColor, finalColor)
        gradient = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientBackgroundList)
        return gradient
    }

    internal fun fetchGradientColor(): IntArray {
        val typedValue = TypedValue()
        val theme: Resources.Theme = application.getTheme()
        theme.resolveAttribute(R.attr.meetBarStartColor, typedValue, true)
        @ColorInt val startColor: Int = typedValue.data
        theme.resolveAttribute(R.attr.meetBarFinalColor, typedValue, true)
        @ColorInt val finalColor: Int = typedValue.data
        val gradientBackgroundList = intArrayOf(startColor, finalColor)
        return gradientBackgroundList
    }


    internal fun initBanner(){
        showLoadingView()
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            val tBanner  = repository.fethBannerInformation()
            withContext(Dispatchers.Main) {
                hideLoadingView()
                when (tBanner) {
                    is ResponseRepository.RequestErrorResult -> updateMessageSate(tBanner.message)
                    is ResponseRepository.RecentToGet -> {
                        stateRecent.set(RecentHome.BannerGet(tBanner.body))
                    }
                }
            }
        }
    }

    internal fun initHistory() {
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            val tBanner  = repository.fetchHistoryInformation()
            withContext(Dispatchers.Main) {
                hideLoadingView()
                when (tBanner) {
                    is ResponseRepository.RequestErrorResult -> updateMessageSate(tBanner.message)
                    is ResponseRepository.HistoryToGet -> {
                        stateRecent.set(RecentHome.HistoryGet(value = tBanner.body))
                    }
                }
            }
        }
    }

    internal fun switchBannerDialogue(bannerId: Int){
        if(bannerId == 0){
            viewState.set(StateHome.ForDialogue(dialogue = Dialogue.DialogCancel))
        }else{
            viewState.set(StateHome.ForDialogue(dialogue = Dialogue.Banner(9898)))
        }

    }

    internal fun validMeetingName(name: String?, pass: String?){
        when(name?.length){
            in 6..100 -> {
                switchToInvite(ELinkToSend.NAME, name!!, pass!!)
                viewState.set(StateHome.ForDialogue(
                    dialogue = Dialogue.Default(
                        type = EDialog.MEET_CREATE,
                        isAction = false)))
            }
            else -> {
                val t = StringBuilder()
                if(name.isNullOrEmpty()){
                    t.append(application.getString(R.string.recentNewRoomTitleFailZeroText))
                }else if(name.length < 6){
                    t.append(application.getString(R.string.recentNewRoomTitleFailMinSizeText))
                }else if(name.length > 100) {
                    t.append(application.getString(R.string.recentNewRoomTitleFailMaxSizeText))
                }
                updateMessageSate(
                    BaseViewModel.MessageState.MessageWarning(
                        content = BaseViewModel.AlertText(
                            title = application.getString(R.string.recentNewRoomTitleFail),
                            texts = t.toString())))
            }
        }
    }

    internal fun validMeetingCode(code: String?, pass: String? = null) {
        code?.let{
            val c: String = it.trim('-')
            when (c.length){
                12 -> {
                    switchToInvite(ELinkToSend.CODE, code, pass)
                    viewState.set(
                        StateHome.ForDialogue(
                            dialogue = Dialogue.Default(
                                type = EDialog.MEET_CREATE,
                                isAction = false
                            )
                        )
                    )
                }
                else  -> {
                    val t = StringBuilder()
                    if (code.isNullOrEmpty()) {
                        t.append(application.getString(R.string.recentNewRoomTitleFailZeroText))
                    } else if (code.length < 6)   {
                        t.append(application.getString(R.string.recentNewRoomTitleFailMinSizeText))
                    } else if (code.length > 100) {
                        t.append(application.getString(R.string.recentNewRoomTitleFailMaxSizeText))
                    }
                    updateMessageSate(
                        BaseViewModel.MessageState.MessageWarning(
                            content = BaseViewModel.AlertText(
                                title = application.getString(R.string.recentNewRoomTitleFail),
                                texts = t.toString()
                            )
                        )
                    )
                }
            }
        }
    }
    private val isCreateDialog = "DIALOG_CREATE"
    private val isCreateCalendar = "CALENDAR_CREATE"
    private val passShow = mutableStateMapOf(
        Pair(isCreateDialog  , false),
        Pair(isCreateCalendar, false))
    internal fun passwordSeeBtn(boolean: Boolean): Int{
        return when(boolean){
            true -> net.sumra.auth.R.drawable.ic_eye_show
            else -> net.sumra.auth.R.drawable.ic_eye_hide
        }
    }

//    internal fun showImageChoose(){
//        viewState.set(StateHome.ForDialogue(Dialogue.Choose))
//    }

    sealed class TypEdCode{
        object Nothing   : TypEdCode()
        object CreateCode: TypEdCode()
        object EditedCode: TypEdCode()
        object OtherEdit : TypEdCode()
    }

    sealed class StateReferral {
        class DefaultReferralCodes(): StateReferral()
        class UpdatesReferralCodes(val data: MutableList<objModelReferralCode.Code>): StateReferral()
        class EditedReferralCode(
            val isEdited : TypEdCode,
            val id       : String?,
            val note     : String?,
            val isDefault: Boolean?): StateReferral()
        class UpdateMemberShip(val data: _AppMemberShip): StateReferral()
        class UpdateEarningsCalc(
            val invoited: String,
            val bonus: String,
            val result:String,
            val cur: String ) : StateReferral()
        class UpdateAfterDelete (val id: String): StateReferral()
    }

    sealed class StateContacts {
        class Default(): StateContacts()
        class ShowInvite(): StateContacts()
        class HideInvite(): StateContacts()
        class Invite() : StateContacts()
        class Search() : StateContacts()
        class Updates(val contacts: MutableList<FetchContact>) : StateContacts()
    }

    sealed class StateRewards{
        class Default(): StateRewards()

        class UpdateLines(val data: LineDataSet):StateRewards()
        class UpdatePies (val data: PieDataSet) :StateRewards()
        class UpdateBars(val data: BarDataSet) :StateRewards()

        class HideLinearMenu(val menuItemName: String) : StateRewards()
        class HideBoxPieMenu(val menuItemName: String) : StateRewards()
    }

    sealed class StateAppSetup{
        object SetupDefault: StateAppSetup()
        object UpdateVideo: StateAppSetup()
    }

    sealed class StatePrfl{
        object Default: StatePrfl()
        data class Success(val dt: MeetUserInfo): StatePrfl()
//        data class Failure(): StatePrfl()
    }

    sealed class StateCalendar{
        object CalendarDft: StateCalendar()
        object CalendarGet: StateCalendar()
        object CalendarPst: StateCalendar()
    }

    sealed class StateMeetAdd{
        data class MeetAddDft(val isStatePM: Boolean = false): StateMeetAdd()
        object MeetAddPst: StateMeetAdd()
        data class MeetParticipantCount(val c: Int): StateMeetAdd()
    }

    sealed class StateBonusPlaza{
        object     BonusPlazaDft                             : StateBonusPlaza()
        data class RewardBonusGet(val body: PlazaRewardBonus): StateBonusPlaza()
        data class RewardChartGet(
            val body: PlazaRewardChart,
            val stat: TotalBalance
        ): StateBonusPlaza()

        data class TokenChartBarGet(val body: MutableList<List<BarDataSet>>) : StateBonusPlaza()
        data class TokenChartPieGet(val body: MutableList<SetupBar>) : StateBonusPlaza()
        data class TokenChartLinGet(val body: MutableList<LineData>): StateBonusPlaza()

        data class HowToRewardGet(val body: PlazaHowToReward): StateBonusPlaza()
        data class EventDetailGet(val body: PlazaEventDetail): StateBonusPlaza()
        data class TestMeetingGet(val body: PlazaTestMeeting): StateBonusPlaza()
    }
}


