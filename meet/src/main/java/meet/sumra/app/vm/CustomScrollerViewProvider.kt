package meet.sumra.app.vm

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.futuremind.recyclerviewfastscroll.viewprovider.ScrollerViewProvider
import com.futuremind.recyclerviewfastscroll.viewprovider.ViewBehavior
import android.graphics.drawable.shapes.OvalShape
import android.graphics.drawable.ShapeDrawable
import com.futuremind.recyclerviewfastscroll.viewprovider.VisibilityAnimationManager
import com.futuremind.recyclerviewfastscroll.viewprovider.DefaultBubbleBehavior
import android.view.Gravity
import androidx.core.content.ContextCompat
import com.futuremind.recyclerviewfastscroll.Utils
import meet.sumra.app.R

class CustomScrollerViewProvider: ScrollerViewProvider() {
    private var bubble: TextView? = null
    private var handle: View? = null

    override fun provideHandleView(container: ViewGroup?): View? {
        handle = View(context)
        val dimen = context.resources.getDimensionPixelSize(R.dimen.customFastListSize)
        handle!!.layoutParams = ViewGroup.LayoutParams(dimen, dimen)
        Utils.setBackground(
            handle,
            drawCircle(dimen, dimen, ContextCompat.getColor(context, R.color.gray_300))
        )
        handle!!.visibility = View.INVISIBLE
        return handle
    }

    override fun provideBubbleView(container: ViewGroup?): View? {
        bubble = TextView(context)
        val dimen = context.resources.getDimensionPixelSize(R.dimen.customBubbleSize)
        bubble!!.layoutParams = ViewGroup.LayoutParams(dimen, dimen)
        Utils.setBackground(
            bubble,
            drawCircle(dimen, dimen, ContextCompat.getColor(context, R.color.gray_700)) //custom_bubble_color))
        )
        bubble!!.visibility = View.INVISIBLE
        bubble!!.gravity = Gravity.CENTER
        bubble!!.setTextColor(ContextCompat.getColor(context, R.color.white))
        scroller.addScrollerListener { relativePos ->
            // Yeah, yeah, but we were so preoccupied with whether or not we could,
            // That we didn't stop to think if we should.
            bubble!!.rotation = relativePos * 360f
        }
        return bubble
    }

    override fun provideBubbleTextView(): TextView? {
        return bubble
    }

    override fun getBubbleOffset(): Int {
        return (if (scroller.isVertical) handle!!.height.toFloat() / 2f - bubble!!.height
            .toFloat() / 2f else handle!!.width.toFloat() / 2f - bubble!!.width.toFloat() / 2).toInt()
    }

    override fun provideHandleBehavior(): ViewBehavior? {
        return CustomHandleBehavior(
            VisibilityAnimationManager.Builder(handle)
                .withHideDelay(2000)
                .build(),
            CustomHandleBehavior.HandleAnimationManager.Builder(handle!!)
                .withGrabAnimator(R.animator.custom_grab)
                .withReleaseAnimator(R.animator.custom_release)
                .build()
        )
    }

    override fun provideBubbleBehavior(): ViewBehavior? {
        return DefaultBubbleBehavior(
            VisibilityAnimationManager.Builder(bubble).withHideDelay(0).build()
        )
    }

    private fun drawCircle(width: Int, height: Int, color: Int): ShapeDrawable? {
        val oval = ShapeDrawable(OvalShape())
        oval.intrinsicHeight = height
        oval.intrinsicWidth = width
        oval.paint.color = color
        return oval
    }
}