package meet.sumra.app.vm.vm_login

import android.app.Application
import android.app.PendingIntent
import androidx.lifecycle.MutableLiveData
import meet.sumra.app.ext.default
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import meet.sumra.app.R
import meet.sumra.app.ext.set
import meet.sumra.app.data.pojo.ResponseRepository
import meet.sumra.app.repository.Repository
import meet.sumra.app.vm.BaseViewModel

class ViewModelLogin(
    appContext: Application,
    private val repository: Repository,
    override val viewMessageState: MutableLiveData<MessageState> = MutableLiveData<MessageState>()
        .default(initialValue = MessageState.MessageNothing),
    override val viewProgressState: MutableLiveData<LoadingState> = MutableLiveData<LoadingState>()
        .default(initialValue = LoadingState.LoadingNothing)
): BaseViewModel(appContext){

    val viewState = MutableLiveData<StateLogin>().default(initialValue = StateLogin.Session())

    private lateinit var currentJob: Job

    init {  }

    fun updateSessionToken(sessionContent: String){
        currentJob = viewModelScope.launch(Dispatchers.IO) {
            try {
                repository.updateSession(sessionContent)
            }catch (ex: PendingIntent.CanceledException){
                viewState.set(StateLogin.Failure(ex.message.toString()))
            }.let {
                withContext(Dispatchers.Main) {
                    selectionResponse(it as ResponseRepository)
                }
            }
        }
    }

    private fun selectionResponse(data: ResponseRepository){
        when(data){
            is ResponseRepository.ResponseRefreshSuccess -> {
                viewState.set(StateLogin.ToHome())
            }
            is ResponseRepository.ResponseRefresh        -> {
                viewState.set(StateLogin.Section())
            }
            is ResponseRepository.RequestWithoutResult   -> {
                viewState.set(StateLogin.Failure(getContext().resources.getString(R.string.error_text_unknown)))
            }
            is ResponseRepository.RequestErrorResult     -> {
                viewState.set(StateLogin.Section())
            }
        }
    }

}
