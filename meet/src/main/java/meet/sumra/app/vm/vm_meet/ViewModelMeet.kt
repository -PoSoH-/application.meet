package meet.sumra.app.vm.vm_meet

import android.content.Context
import android.content.res.Resources
import android.graphics.Typeface
import android.graphics.drawable.GradientDrawable
import android.util.TypedValue
import androidx.annotation.ColorInt
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import com.google.gson.Gson
import kotlinx.coroutines.*
import meet.sumra.app.AppMeet
import meet.sumra.app.CONST
import meet.sumra.app.R
import meet.sumra.app.data.enums.ELoadedPart
import meet.sumra.app.data.mdl.*
import meet.sumra.app.data.pojo.ResponseRepository
import meet.sumra.app.ext.default
import meet.sumra.app.ext.set
import meet.sumra.app.repository.Repository
import meet.sumra.app.vm.BaseViewModel
import meet.sumra.app.vm.vm_home.StateMeet
import net.sumra.auth.helpers.Finals
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions
import org.jitsi.meet.sdk.JitsiMeetUserInfo
import xyn.meet.storange.models.SessionEntity
import java.net.URL
import java.util.*

class ViewModelMeet (private val application: AppMeet,
                     private val repository : Repository,
                     override val viewMessageState: MutableLiveData<MessageState> = MutableLiveData<MessageState>()
                         .default(initialValue = MessageState.MessageNothing),
                     override val viewProgressState: MutableLiveData<LoadingState> = MutableLiveData<LoadingState>()
                         .default(initialValue = LoadingState.LoadingNothing)
): BaseViewModel(application) {

    private lateinit var mCurrentJob: Job
    private val mGradient: MutableSet<GradientDrawable> = mutableSetOf()

//    private val mCurrentRoom: MutableList<DtMeetRoom> = mutableListOf()
    private val mCurrentSession: MutableList<SessionData> = mutableListOf()

    private val isLoadedPart: MutableMap<ELoadedPart, Boolean>
    private val applicationSettings: DtAppSetup

    private val sessionActive = booleanArrayOf(false)
    private var sessionEntity: SessionEntity? = null

    private val sumraUserInfo = mutableListOf<JitsiMeetUserInfo>()
    private val mConference = mutableListOf(JitsiMeetConferenceOptions.Builder().build())

    var isPrivateRoom = mutableStateOf(false)
        set(value) {field = value}
        get() {return field}
    var securityWorld = mutableStateOf("")
        set(value) {
            if (value.value.isNullOrEmpty()) {
                isPrivateRoom.value = false
            }else{
                field = value
                isPrivateRoom.value = true
            }
        }
        get() {return field}

    val mViewState = MutableLiveData<StateMeet>().default(initialValue = StateMeet.Default)

    init {
//        repository.addBackListener(rSt())
        isLoadedPart = mutableMapOf(
            Pair(ELoadedPart.isINVITE, false),
            Pair(ELoadedPart.isSESSION, false),
            Pair(ELoadedPart.isUSER, false)
        )

        checkProfile()
        checkRoom()
        checkSession()

        val tmp = application
            .getSharedPreferences(CONST.APP_SETTING, Context.MODE_PRIVATE)
            .getString(
                DtAppSetup.SETUP_KEY,
                Gson().toJson(DtAppSetup(UUID.randomUUID().toString()))).toString()

        applicationSettings = Gson().fromJson(tmp, DtAppSetup::class.java)
    }

    internal fun getAppCtx() = application

    internal fun updateTypeFace() : Typeface {
        application.run {
            return Typeface.createFromAsset(this.assets, Finals.FONT_FACE)
        }
    }

    private fun checkProfile(){
        val dt = repository.checkCurrentUser()
        when(dt){
            is ResponseRepository.PrflUpdate -> {
                convertUserDataToUse(l = object : EventListener{
                    override fun updateState(b: Boolean) {
                        isLoadedPart.put(ELoadedPart.isUSER, b)
                    }
                })
            }
            is ResponseRepository.PrflNothing -> {
                fetchUserProfile()
                isLoadedPart.put(ELoadedPart.isUSER, false)
            }
        }
    }

    private fun checkRoom(){
        val dt = repository.checkCurrentRoom()
        when(dt){
            is ResponseRepository.TaskRoomUpdate -> {
                isLoadedPart.put(ELoadedPart.isINVITE, true)
                showMessage(dt.message)
            }
            is ResponseRepository.TaskNotRoom -> {
                isLoadedPart.put(ELoadedPart.isINVITE, false)
                // loaded room
            }
        }
    }

    private fun checkSession(){
//        val dt = repository.checkSession()
//        when(dt){
//            is ResponseRepository.TaskSessionUpdate -> {
//                isLoadedPart.put(ELoadedPart.isINVITE, true)
//                showMessage(dt.message)
//            }
//            is ResponseRepository.TaskNotSession -> {
//                isLoadedPart.put(ELoadedPart.isINVITE, false)
//                // loaded room
//            }
//        }
    }

    private fun fetchUserProfile(){
        mCurrentJob = viewModelScope.launch(Dispatchers.IO) {
            val ur = repository.featchUserProfile()
            withContext(Dispatchers.Main){
                when (ur) {
                    is ResponseRepository.PrflSuccess -> {
                        isLoadedPart.put(ELoadedPart.isUSER, true)
                        showMessage(ur.message)
                    }
                    is ResponseRepository.RequestErrorResult -> {
                        showMessage(ur.message)
                    }
                }
            }
        }
    }


//    internal fun updateInitialData() {
//        repository.featchUserProfile()
//
//            isLoadedPart.remove(ELoadedPart.isINVITE)
//            if(!mCurrentRoom[0].mInvite.isNullOrEmpty()){
//                isLoadedPart.put(ELoadedPart.isINVITE, true)
//            }else{
//                isLoadedPart.put(ELoadedPart.isINVITE, false)
//            }
//        }else {
//            mViewState.set(StateMeet.RoomGet(dt = mCurrentRoom[0]))
//        }
//
//            convertUserDataToUse(object: EventListener{
//                override fun updateState(b: Boolean) {
//
//                }
//            })
//    }


//    internal fun roomGetLoadRoomById(roomId: String){
//        showProgressView()
//        mCurrentJob = viewModelScope.launch(Dispatchers.IO){
////            val rc = repository.roomGetRoomById(roomId)
//            withContext(Dispatchers.Main) {
//                when (rc) {
//                    is ResponseRepository.TaskRoomUpdate -> {
//                        viewMessageState.set(rc.message)
//                        mViewState.set(StateMeet.RoomGet(rc.content))
//                    }
//                    is ResponseRepository.RequestErrorResult -> {
//                        viewMessageState.set(rc.message)
//                    }
//                }
//                hideProgressView()
//            }
//        }
//    }

    internal fun roomCreate(room: RoomData){
        showProgressView()
        mCurrentJob = viewModelScope.launch(Dispatchers.IO){
            val rc = repository.roomCreateByRoomName(room)
            withContext(Dispatchers.Main) {
                when (rc) {
                    is ResponseRepository.TaskRoomCreate -> {
//                        mRoomState.value.clear()
//                        mRoomState.value.add(rc.content)
                        viewMessageState.set(rc.message)
                        mViewState.set(StateMeet.RoomGet(rc.content))
                    }
                    is ResponseRepository.RequestErrorResult -> {
                        viewMessageState.set(rc.message)
                    }
                }
                hideProgressView()
            }
        }
    }

    internal fun roomCreateByInvite(invite: String){
        showProgressView()
        mCurrentJob = viewModelScope.launch(Dispatchers.IO) {
            val ri = repository.roomFetchByInvite(invite)
            withContext(Dispatchers.Main) {
                when (ri) {
                    is ResponseRepository.TaskRoomCreate -> {
                        showMessage(ri.message)
                        mViewState.set(StateMeet.RoomGet(dt = ri.content))
                    }
                    is ResponseRepository.RequestErrorResult -> {
                        showMessage(ri.message)
                    }
                }
                hideProgressView()
            }
        }
    }

    internal fun roomGetReady(){
        val cr = repository.checkCurrentRoom()
        when(cr){
            is ResponseRepository.TaskRoomUpdate -> {
                mViewState.set(StateMeet.RoomGet(cr.content))
            }
            is ResponseRepository.TaskNotRoom -> {
                mViewState.set(StateMeet.RoomEmp)
            }
        }
    }

//    internal fun updateRoomInfo(roomState: DtMeetRoom){
//        this.mRoomState.value.clear()
//        this.mRoomState.value.add(roomState)
//    }

//    internal fun fetchRoomInformation() = mRoomState

    internal fun fetchPreparingOptions(){
        mConference.clear()

        if(isLoadedPart.get(ELoadedPart.isUSER)!!) {
            val cu = repository.checkCurrentUser()
            when (cu) {
                is ResponseRepository.PrflUpdate -> {
                    convertUserDataToUse(l = object : EventListener {
                        override fun updateState(b: Boolean) {
                            isLoadedPart.put(ELoadedPart.isUSER, b)
                        }
                    })
                }
                is ResponseRepository.PrflNothing -> {
                    showMessage(
                        m = MessageState.MessageDanger(
                            content = AlertText(
                                title = "Conference Options!",
                                texts = "Error created Conference Options! Not user!"
                            )
                        )
                    )
                }
            }
        }

        val cr = repository.checkCurrentRoom()

        if(isLoadedPart.get(ELoadedPart.isUSER)!!) {
            when (cr) {
                is ResponseRepository.TaskRoomUpdate -> {
                    mConference.add(
                        JitsiMeetConferenceOptions.Builder()
                            .setServerURL(URL(application.resources.getString(R.string.address_url_home)))
                            .setRoom(cr.content.rRoomID)
                            .setVideoMuted(isVideo())
                            .setAudioMuted(isVoice())
                            .setAudioOnly(false)
//                .setColorScheme()
                            .setWelcomePageEnabled(false)
                            .setUserInfo(sumraUserInfo[0])
                            .build()
                    )
                    mViewState.set(StateMeet.MeetOptions(mConference[0]))
                }
                is ResponseRepository.TaskNotRoom -> {
                    showMessage(
                        m = MessageState.MessageDanger(
                            content = AlertText(
                                title = "Conference Options!",
                                texts = "Error created Conference Options! Not Room!"
                            )
                        )
                    )
                }
            }
        }
    }

    internal fun sessionCreate(session: ToSession){
        viewProgressState.set(LoadingState.LoadingShow)
        mCurrentJob = viewModelScope.launch(Dispatchers.IO){
            repository.createRoomSession(session).let {
                when (it) {
                    is ResponseRepository.TaskSessionCreate -> {
                        withContext(Dispatchers.Main){
                            viewMessageState.set(it.message)
                            mCurrentSession.clear()
                            mCurrentSession.add(SessionData(id = it.content.id
                                , userId = it.content.userId
                                , roomId = it.content.roomId))
                            mViewState.set(StateMeet.SessionPst(mCurrentSession[0]))
                        }
                    }
                    is ResponseRepository.RequestErrorResult -> {
                        withContext(Dispatchers.Main) {
                            viewMessageState.set(it.message)
                        }
                    }
                }
            }
        }
    }

    internal fun isVideo() = applicationSettings.stateVideo
    internal fun isVoice() = applicationSettings.stateVoice
    internal fun isDarkMode() = applicationSettings.stateDarkMode

    internal fun sessionUpdate(session: ToSession){
        viewProgressState.set(LoadingState.LoadingShow)
        mCurrentJob = viewModelScope.launch(Dispatchers.IO){
            repository.updateRoomSession(session, mCurrentSession[0].id).let {
                when (it) {
                    is ResponseRepository.TaskSessionUpdate -> {
                        withContext(Dispatchers.Main){
                            viewMessageState.set(it.message)
                            mCurrentSession.clear()
                            mCurrentSession.add(SessionData(
                                  id = it.content.id
                                , userId = it.content.userId
                                , roomId = it.content.roomId
                                , status = it.content.status
                                , startTime = it.content.startTime
                                , finishTime = it.content.finishTime
                                , deletedAt = it.content.deletedAt))
                            mViewState.set(StateMeet.SessionPst(mCurrentSession[0]))
                        }
                    }
                    is ResponseRepository.RequestErrorResult -> {
                        withContext(Dispatchers.Main) {
                            viewMessageState.set(it.message)
                        }
                    }
                }
            }
        }

    }

    internal fun sessionDelete() {

    }

    private interface EventListener {fun updateState(b: Boolean)}

    private fun convertUserDataToUse(l: EventListener) {
        mCurrentJob = viewModelScope.launch(Dispatchers.IO) {
            val tmp = repository.featchUserProfile()
            withContext(Dispatchers.Main) {
                when (tmp) {
                    is ResponseRepository.PrflSuccess -> {
                        sumraUserInfo.clear()
                        val usr = JitsiMeetUserInfo()
                        if(!tmp.content.avatar.isNullOrEmpty()) usr.setAvatar(URL(tmp.content.avatar))
                        usr.setEmail(tmp.content.email)
                        usr.setDisplayName(tmp.content.displayName)
                        sumraUserInfo.add(usr)
//                        isLoadedPart.put(ELoadedPart.isUSER, true)
                        l.updateState(true)
                    }
                    is ResponseRepository.RequestErrorResult -> {
                        showMessage(tmp.message)
//                        isLoadedPart.put(ELoadedPart.isUSER, false)
                        l.updateState(false)
                    }
                }
            }
        }
    }

    internal fun showMessage(m: MessageState){
        viewMessageState.set(m)
    }

    internal fun showProgressView(){
        viewProgressState.set(LoadingState.LoadingShow)
    }
    internal fun hideProgressView(){
        viewProgressState.set(LoadingState.LoadingHide)
    }

    internal fun actionToMeetRun() {
        mViewState.set(StateMeet.MeetRun)
    }

    internal fun actionToShareLink(isLink: Boolean) {
        val cr = repository.checkCurrentRoom()
        val cu = repository.checkCurrentUser()

        val cc = Calendar.getInstance()
        cc.timeInMillis = System.currentTimeMillis()
        val passPrep = if(isPrivateRoom.value) "This private Sumrameet conference\nPasscode: ${securityWorld.value}" else "This free Sumrameet conference"
        val un = cu as ResponseRepository.PrflUpdate
        val time = """${cc.get(Calendar.HOUR)}:${cc.get(Calendar.MINUTE)} ${if(cc.get(Calendar.AM_PM) == 0) "AM" else "PM"}"""
        val zone = cc.getTimeZone().displayName
        val userName = un.content.displayName
        when(cr){
            is ResponseRepository.TaskRoomUpdate -> {
                if(isLink){
                    mViewState.set(StateMeet.ShareInvite(inviteLink = """Sumrameet conference invitation ${userName}
                        |
                        |${userName} is inviting you to sheduled Sumrameeting conference
                        |
                        |Time: ${cc.get(Calendar.DATE)} ${cc.getMaximum(Calendar.MONTH)} ${cc.get(Calendar.YEAR)} ${time} : ${zone}
                        |
                        |Join Sumrameet conference: 
                        |
                        |${application.resources.getString(R.string.address_url_home)}j/${cr.content.rRoomInvite}
                        |
                        |${passPrep}
                        |""".trimMargin()
                    ))
                }else{
                    mViewState.set(StateMeet.ShareInvite(inviteLink = """Sumrameet conference invitation ${userName}
                        |  
                        |${userName} is inviting you to sheduled Sumrameeting conference
                        |
                        |Time: ${cc.get(Calendar.DATE)} ${cc.getMaximum(Calendar.MONTH)} ${cc.get(Calendar.YEAR)} ${time} : ${zone}
                        |
                        |Join Sumrameet conference use conference code: 
                        |
                        |${cr.content.rRoomInvite}
                        |
                        |${passPrep}
                        |""".trimMargin()
                    ))
                }
            }
            is ResponseRepository.TaskNotRoom -> {}
        }
    }

    internal fun actionToBack(){
        mViewState.set(StateMeet.ActionToBack)
    }

    internal fun toDefault(){
        mViewState.set(StateMeet.Default)
    }

    internal fun fetchGradient(): GradientDrawable {
        if(mGradient.isNullOrEmpty()) {
            val typedValue = TypedValue()
            val theme: Resources.Theme = application.getTheme()
            theme.resolveAttribute(R.attr.meetBarStartColor, typedValue, true)
            @ColorInt val startColor: Int = typedValue.data
            theme.resolveAttribute(R.attr.meetBarFinalColor, typedValue, true)
            @ColorInt val finalColor: Int = typedValue.data
            val gradientBackgroundList = intArrayOf(startColor, finalColor)
            mGradient.add(GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientBackgroundList))
        }
        return mGradient.elementAt(0)
    }

//    private fun rSt() = object: Repository.BackListener{
//        override fun toResult(r: ResponseRepository) {
//            when(r){
//                is ResponseRepository.TaskRoomCreate,
//                is ResponseRepository.TaskRoomUpdate -> {
//
//                }
//
//            }
//        }
//    }
}



