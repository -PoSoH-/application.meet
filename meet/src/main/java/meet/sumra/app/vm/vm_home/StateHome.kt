package meet.sumra.app.vm.vm_home

import meet.sumra.app.data.enums.ELinkToSend
import meet.sumra.app.data.mdl.DtProfileUser
import meet.sumra.app.vm.Dialogue
import sdk.net.meet.api.meet_profile.response.MeetUserInfo

sealed class StateHome{
    class Default: StateHome()

    class DrawerUpdate(val value: MeetUserInfo): StateHome()

    class BackPress(): StateHome()
    class Logout(): StateHome()
    class UpdateVideoAppBar(): StateHome()

    class ShareSocial(val code:String, val link: String): StateHome()

    class Success(val message: String): StateHome()
    class Failure(val message: String): StateHome()
    class Preview(val state: Boolean): StateHome()
//    class NewRoom(val name: String): StateHome()

    class Invite(val type: ELinkToSend, val data: String, val pass: String?): StateHome()

    class Dashboard(val currentColor:Int): StateHome()
    class Statistic(): StateHome()
    class Referrals(val statusColor:Int) : StateHome()
    class Contacts (val statusColor:Int) : StateHome()
    class Earnings (): StateHome()
    class Rewards  (): StateHome()
    class Plaza    (): StateHome()
    class Pioneer  (): StateHome()
    class Profile  (): StateHome()

//    class RoomCrt(val room: String): StateHome()
//    class RoomUpd(): StateHome()
//    class RoomDel(): StateHome()

    object CalendarMeetAdd: StateHome()
    object FragmentEmpty: StateHome()

    class ForDialogue(val dialogue: Dialogue): StateHome()
}

