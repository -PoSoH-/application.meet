package meet.sumra.app.vm

import meet.sumra.app.data.enums.EDialog

sealed class Dialogue{
    object DialogCancel            : Dialogue()
    data class Banner(val id: Int) : Dialogue()
    object Choose : Dialogue()
    data class Default(val type: EDialog, val isAction: Boolean = false): Dialogue()
}
