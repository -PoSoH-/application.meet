package meet.sumra.app.vm.vm_home

import com.github.mikephil.charting.data.LineData
import meet.sumra.app.data.mdl.earnings.modelEarnings

sealed class StateEarnings{
    object Default: StateEarnings()
    data class Upgrade(val info: modelEarnings.YourEarnings): StateEarnings()
    data class ByDate(val info: LineData): StateEarnings()
    data class LeaderBoard(val info: modelEarnings.LeaderBoard): StateEarnings()
    data class Projected(val info: modelEarnings.Projected): StateEarnings()
    data class GoldPlan(val info: modelEarnings.ShowGold): StateEarnings()
}
