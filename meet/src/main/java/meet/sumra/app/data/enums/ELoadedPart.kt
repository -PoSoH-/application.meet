package meet.sumra.app.data.enums

enum class ELoadedPart{
    isINVITE,
    isSESSION,
    isUSER
}