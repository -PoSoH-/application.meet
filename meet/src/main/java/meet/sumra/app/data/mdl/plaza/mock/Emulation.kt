package meet.sumra.app.data.mdl.plaza.mock

import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.Color
import com.github.tehras.charts.bar.BarChartData
import com.github.tehras.charts.bar.renderer.label.SimpleValueDrawer
import com.github.tehras.charts.line.LineChartData
import com.github.tehras.charts.piechart.PieChartData
import meet.sumra.app.data.enums.EPlazaType
import meet.sumra.app.data.mdl.roomModels
import java.util.*
import kotlin.random.Random

class Emulation {
    companion object {
        var labelDrawer = mutableStateOf(SimpleValueDrawer(drawLocation = SimpleValueDrawer.DrawLocation.XAxis))
            get
        var barChartData: BarChartData = barDefault()
            get
        var lineChartData: LineChartData = lineDefault()
            get
        var pieChartData = mutableListOf<PieChartData>()
            get

        fun fetchStatistic(type: EPlazaType): BarChartData{
            return when(type){
                EPlazaType.DAY ->   {fetchDay()}
                EPlazaType.WEEK ->  {fetchWeek()}
                EPlazaType.MONTH -> {fetchMonth(7)}
                EPlazaType.YEAR ->  {fetchYear()}
            }
        }

        fun fetchTokenCharts(type: EPlazaType): BarChartData{
            return when(type){
                EPlazaType.DAY ->   {fetchDay()}
                EPlazaType.WEEK ->  {fetchWeek()}
                EPlazaType.MONTH -> {fetchMonth(7)}
                EPlazaType.YEAR ->  {fetchYear()}
            }
        }


    private fun fetchDay() = BarChartData(
        bars = listOf(
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff451245),
                label = "01"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff457564),
                label = "02"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff455415),
                label = "03"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff452435),
                label = "04"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff769935),
                label = "05"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff766839),
                label = "06"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff548409),
                label = "07"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff548468),
                label = "08"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff590754),
                label = "09"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff998768),
                label = "10"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff548785),
                label = "11"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff765689),
                label = "12"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff451245),
                label = "13"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff457564),
                label = "14"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff455415),
                label = "15"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff452435),
                label = "16"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff769935),
                label = "17"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff766839),
                label = "18"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff548409),
                label = "19"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff548468),
                label = "20"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff590754),
                label = "21"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff998768),
                label = "22"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff548785),
                label = "23"
            ),
            BarChartData.Bar(
                value = Random.nextInt(10, 100).toFloat(),
                color = Color(color = 0xff765689),
                label = "24"
            )
        )
    )

    private fun fetchWeek() = BarChartData(
        bars = mutableListOf(
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff451245),
                label = "MON"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff457564),
                label = "TUE"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff455415),
                label = "WED"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff452435),
                label = "THU"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff769935),
                label = "FRI"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff766839),
                label = "SAT"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff548409),
                label = "SUN"
            )
        )
    )

    private fun fetchMonth(month: Int): BarChartData {
        val bars = mutableListOf(
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff451245),
                label = "01"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff457564),
                label = "02"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff455415),
                label = "03"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff452435),
                label = "04"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff769935),
                label = "05"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff766839),
                label = "06"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff548409),
                label = "07"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff548468),
                label = "08"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff590754),
                label = "09"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff998768),
                label = "10"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff548785),
                label = "11"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff765689),
                label = "12"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff451245),
                label = "13"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff457564),
                label = "14"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff455415),
                label = "15"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff452435),
                label = "16"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff769935),
                label = "17"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff766839),
                label = "18"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff548409),
                label = "19"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff548468),
                label = "20"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff590754),
                label = "21"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff998768),
                label = "22"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff548785),
                label = "23"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff765689),
                label = "24"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff451245),
                label = "25"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff457564),
                label = "26"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff455415),
                label = "27"
            ),
            BarChartData.Bar(
                value = Random.nextInt(8, 50).toFloat(),
                color = Color(color = 0xff452435),
                label = "28"
            ),
        )

        when (month) {
            4, 6, 9, 11 -> {
                bars.add(
                    BarChartData.Bar(
                        value = Random.nextInt(8, 50).toFloat(),
                        color = Color(color = 0xff769935),
                        label = "29"
                    )
                )
                bars.add(
                    BarChartData.Bar(
                        value = Random.nextInt(8, 50).toFloat(),
                        color = Color(color = 0xff766839),
                        label = "30"
                    )
                )
            }
            1, 3, 5, 7, 8, 10, 12 -> {
                BarChartData.Bar(
                    value = Random.nextInt(8, 50).toFloat(),
                    color = Color(color = 0xff548409),
                    label = "31"
                )
            }
        }
        return BarChartData(bars = bars)
    }

    private fun fetchYear() = BarChartData(
            bars = listOf(
                BarChartData.Bar(
                    value = Random.nextInt(10, 100).toFloat(),
                    color = Color(color = 0xff451245),
                    label = "SEP"
                ),
                BarChartData.Bar(
                    value = Random.nextInt(10, 100).toFloat(),
                    color = Color(color = 0xff457564),
                    label = "OKT"
                ),
                BarChartData.Bar(
                    value = Random.nextInt(10, 100).toFloat(),
                    color = Color(color = 0xff455415),
                    label = "NOV"
                ),
                BarChartData.Bar(
                    value = Random.nextInt(10, 100).toFloat(),
                    color = Color(color = 0xff452435),
                    label = "DEC"
                ),
                BarChartData.Bar(
                    value = Random.nextInt(10, 100).toFloat(),
                    color = Color(color = 0xff769935),
                    label = "JAN"
                ),
                BarChartData.Bar(
                    value = Random.nextInt(10, 100).toFloat(),
                    color = Color(color = 0xff766839),
                    label = "FEB"
                ),
                BarChartData.Bar(
                    value = Random.nextInt(10, 100).toFloat(),
                    color = Color(color = 0xff548409),
                    label = "MAR"
                ),
                BarChartData.Bar(
                    value = Random.nextInt(10, 100).toFloat(),
                    color = Color(color = 0xff548468),
                    label = "APR"
                ),
                BarChartData.Bar(
                    value = Random.nextInt(10, 100).toFloat(),
                    color = Color(color = 0xff590754),
                    label = "MAI"
                ),
                BarChartData.Bar(
                    value = Random.nextInt(10, 100).toFloat(),
                    color = Color(color = 0xff998768),
                    label = "JUN"
                ),
                BarChartData.Bar(
                    value = Random.nextInt(10, 100).toFloat(),
                    color = Color(color = 0xff548785),
                    label = "JUL"
                ),
                BarChartData.Bar(
                    value = Random.nextInt(10, 100).toFloat(),
                    color = Color(color = 0xff765689),
                    label = "AUG"
                ),
            )
        )

        fun barDefault() = BarChartData(
            bars = listOf(
                BarChartData.Bar(
                    value = 1f,
                    color = Color.Transparent,
                    label = ""
                ),
                BarChartData.Bar(
                    value = 1f,
                    color = Color.Transparent,
                    label = ""
                ),
                BarChartData.Bar(
                    value = 1f,
                    color = Color.Transparent,
                    label = ""
                )
            )
        )

        fun lineDefault() = LineChartData(
            points = mutableListOf(
                LineChartData.Point(
                    value = 0f,
                    label = ""
                ),
                LineChartData.Point(
                    value = 0f,
                    label = ""
                ),
                LineChartData.Point(
                    value = 0f,
                    label = ""
                )
            )
        )

        private fun fetchLineYear() = LineChartData(
            points = listOf(
                LineChartData.Point(
                    value = Random.nextInt(10, 100).toFloat(),
                    label = "SEP"
                ),
                LineChartData.Point(
                    value = Random.nextInt(10, 100).toFloat(),
                    label = "OKT"
                ),
                LineChartData.Point(
                    value = Random.nextInt(10, 100).toFloat(),
                    label = "NOV"
                ),
                LineChartData.Point(
                    value = Random.nextInt(10, 100).toFloat(),
                    label = "DEC"
                ),
                LineChartData.Point(
                    value = Random.nextInt(10, 100).toFloat(),
                    label = "JAN"
                ),
                LineChartData.Point(
                    value = Random.nextInt(10, 100).toFloat(),
                    label = "FEB"
                ),
                LineChartData.Point(
                    value = Random.nextInt(10, 100).toFloat(),
                    label = "MAR"
                ),
                LineChartData.Point(
                    value = Random.nextInt(10, 100).toFloat(),
                    label = "APR"
                ),
                LineChartData.Point(
                    value = Random.nextInt(10, 100).toFloat(),
                    label = "MAI"
                ),
                LineChartData.Point(
                    value = Random.nextInt(10, 100).toFloat(),
                    label = "JUN"
                ),
                LineChartData.Point(
                    value = Random.nextInt(10, 100).toFloat(),
                    label = "JUL"
                ),
                LineChartData.Point(
                    value = Random.nextInt(10, 100).toFloat(),
                    label = "AUG"
                ),
            )
        )

        fun pieDefault(cPie: MutableList<Color>): MutableList<PieChartData> {
            return mutableListOf(
                PieChartData(
                    slices = listOf(
                        PieChartData.Slice(
                            value = 50f,
                            color = cPie[0]
                        ),
                        PieChartData.Slice(
                            value = 50f,
                            color = cPie[1]
                        )
                    )
                ),
                PieChartData(
                    slices = listOf(
                        PieChartData.Slice(
                            value = 50f,
                            color = cPie[0]
                        ),
                        PieChartData.Slice(
                            value = 50f,
                            color = cPie[1]
                        )
                    )
                )
            )
        }

//        fun tokenDefault(colors: MutableList<Color>){
//            barDefault()
//            lineDefault()
////            pieDefault(colors[0], colors[1])
//        }

        fun getUserRoom() = mutableListOf(
                  roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 1", pUserPhone = "+38064578945", pUserEmail = "45412458451@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 2", pUserPhone = "+38064578946", pUserEmail = "45412458452@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 3", pUserPhone = "+38064578947", pUserEmail = "45412458453@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 4", pUserPhone = "+38064578948", pUserEmail = "45412458454@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 5", pUserPhone = "+38064578949", pUserEmail = "45412458455@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 6", pUserPhone = "+38064578940", pUserEmail = "45412458456@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 7", pUserPhone = "+38064578941", pUserEmail = "45412458457@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 8", pUserPhone = "+38064578942", pUserEmail = "45412458458@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 9", pUserPhone = "+38064578943", pUserEmail = "45412458459@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 0", pUserPhone = "+38064578944", pUserEmail = "45412458450@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 1", pUserPhone = "+38064578945", pUserEmail = "45412458451@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 2", pUserPhone = "+38064578946", pUserEmail = "45412458452@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 3", pUserPhone = "+38064578947", pUserEmail = "45412458453@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 4", pUserPhone = "+38064578948", pUserEmail = "45412458454@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 5", pUserPhone = "+38064578949", pUserEmail = "45412458455@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 6", pUserPhone = "+38064578940", pUserEmail = "45412458456@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 7", pUserPhone = "+38064578941", pUserEmail = "45412458457@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 8", pUserPhone = "+38064578942", pUserEmail = "45412458458@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 9", pUserPhone = "+38064578943", pUserEmail = "45412458459@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 0", pUserPhone = "+38064578944", pUserEmail = "45412458450@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 1", pUserPhone = "+38064578945", pUserEmail = "45412458451@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 2", pUserPhone = "+38064578946", pUserEmail = "45412458452@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 3", pUserPhone = "+38064578947", pUserEmail = "45412458453@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 4", pUserPhone = "+38064578948", pUserEmail = "45412458454@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 5", pUserPhone = "+38064578949", pUserEmail = "45412458455@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 6", pUserPhone = "+38064578940", pUserEmail = "45412458456@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 7", pUserPhone = "+38064578941", pUserEmail = "45412458457@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 8", pUserPhone = "+38064578942", pUserEmail = "45412458458@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 9", pUserPhone = "+38064578943", pUserEmail = "45412458459@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 0", pUserPhone = "+38064578944", pUserEmail = "45412458450@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 1", pUserPhone = "+38064578945", pUserEmail = "45412458451@124.124")
                , roomModels.ParticipantsRoom( pUserID = UUID.randomUUID().toString(), pDisplayName = "User 2", pUserPhone = "+38064578946", pUserEmail = "45412458452@124.124")
            )

        fun emulationDate(): List<Calendar> {
            val startMonth = Random.nextInt(7, 9)
            val startDay = Random.nextInt(1, 29)
            val startHour = Random.nextInt(9, 22)
            val startMinute = Random.nextInt(0, 60)
            val finalMinute = Random.nextInt(0, 60)
            val finalHour = startHour + 1
            val cS  = Calendar.getInstance()
            val cF  = Calendar.getInstance()
            cS.set(Calendar.YEAR, 2021)
            cS.set(Calendar.MONTH, startMonth-1)
            cS.set(Calendar.DATE, startDay)
            cS.set(Calendar.HOUR_OF_DAY, startHour)
            cS.set(Calendar.MINUTE, startMinute)

            cF.set(Calendar.YEAR, 2021)
            cF.set(Calendar.MONTH, startMonth-1)
            cF.set(Calendar.DATE, startDay)
            cF.set(Calendar.HOUR_OF_DAY, finalHour)
            cF.set(Calendar.MINUTE, finalMinute)
            return mutableListOf(cS, cF)
        }

    }
}
