package meet.sumra.app.data.mdl.statistic

import androidx.compose.runtime.MutableState
import com.github.mikephil.charting.data.BarEntry
import meet.sumra.app.data.configs.setupStatisticCharts
import meet.sumra.app.data.configs.setupStatisticCharts.fetchBarData
import meet.sumra.app.data.configs.setupStatisticCharts.fetchBarOverviewData
import meet.sumra.app.data.mdl.earnings.cards

data class Overview(
    val overBarChart  : MutableState<MutableMap<setupStatisticCharts.EStatistic, MutableList<BarEntry>>> = fetchBarOverviewData(cards.EPeriod.YEAR),
    val overGlEarnings: Int = 0,
    val overRewards   : Int = 0,
    val overCashbacks : Int = 0,
    val overIvdUsers  : Int = 0,
    val overConvRate  : Int = 0
)
