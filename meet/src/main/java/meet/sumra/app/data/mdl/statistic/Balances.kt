package meet.sumra.app.data.mdl.statistic

data class Balances(
    val cardManyAll: String = "0",
    val cardManyUpVal: String = "0",
    val cardManyUpPer: String = "0",
    val cardManyDownVal: String = "0",
    val cardManyDownPer: String = "0",
    val cardAvailable: String = "0"
)
