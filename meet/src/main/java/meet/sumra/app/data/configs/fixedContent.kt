package meet.sumra.app.data.configs

import androidx.compose.ui.graphics.Color
import meet.sumra.app.R

object fixedContent {
    fun getAllRewardStatisticLegend() = listOf(
        RewardStatisticLegend(Color(0xffFFAB00), R.string.frgDivitsRewStatisticCommercial),
        RewardStatisticLegend(Color(0xff38CB89), R.string.frgDivitsRewStatisticMobile),
        RewardStatisticLegend(Color(0xffFF5630), R.string.frgDivitsRewStatisticBalance),
        RewardStatisticLegend(Color(0xff377DFF), R.string.frgDivitsRewStatisticSumraID),
    )

    data class RewardStatisticLegend(
        val color: Color,
        val label: Int
    )




}

/*
*
*                             GetLegendStatistic(
                                indicatorColor = Color(0xffFFAB00),
                                labelLegend = stringResource(id = R.string.frgDivitsRewStatisticCommercial),
                                textStyle = txtRewStatTxtGr
                            )
                            GetLegendStatistic(
                                indicatorColor = Color(0xff38CB89),
                                labelLegend = stringResource(id = R.string.frgDivitsRewStatisticMobile),
                                textStyle = txtRewStatTxtGr
                            )
                            GetLegendStatistic(
                                indicatorColor = Color(0xffFF5630),
                                labelLegend = stringResource(id = R.string.frgDivitsRewStatisticBalance),
                                textStyle = txtRewStatTxtGr
                            )
                            GetLegendStatistic(
                                indicatorColor = Color(0xff377DFF),
                                labelLegend = stringResource(id = R.string.frgDivitsRewStatisticSumraID),
                                textStyle = txtRewStatTxtGr
                            )
* */