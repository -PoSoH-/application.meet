package meet.sumra.app.data.enums

sealed class EAvatarType {
    object SQUARE : EAvatarType()
    object CIRCLE : EAvatarType()
    data class RADIUS(val value: Float = .0f):EAvatarType()
}