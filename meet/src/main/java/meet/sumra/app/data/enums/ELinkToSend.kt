package meet.sumra.app.data.enums

enum class ELinkToSend {
    NAME,
    URL,
    CODE,
    NIL
}