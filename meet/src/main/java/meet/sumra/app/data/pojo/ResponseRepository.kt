package meet.sumra.app.data.pojo

import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.data.PieDataSet
import meet.sumra.app.data.mdl.*
import meet.sumra.app.data.mdl.plaza.PlazaHowToReward
import meet.sumra.app.data.mdl.plaza.PlazaRewardBonus
import meet.sumra.app.data.mdl.plaza.PlazaRewardChart
import meet.sumra.app.data.mdl.plaza.TotalBalance
import meet.sumra.app.vm.BaseViewModel
import sdk.net.meet.api.contacts.response.ContactCategory
import sdk.net.meet.api.contacts.response.FetchContact
import sdk.net.meet.api.contacts.response.SaveContact
import sdk.net.meet.api.meet_profile.response.MeetUserInfo
import sdk.net.meet.api.meet_session.response.FinalSession
import sdk.net.meet.api.meet_session.response.MeetSession
import sdk.net.meet.api.referral.objModelReferralCode

sealed class ResponseRepository{
    class RespFailureFetchSession(): ResponseRepository()
    class RequestWithoutResult   (): ResponseRepository()
    class ResponseRefresh        (): ResponseRepository()
    class ResponseRefreshSuccess (): ResponseRepository()
    object Nothing:ResponseRepository()

    object Success:ResponseRepository()
    object Failure:ResponseRepository()

    class RequestErrorResult     (val message: BaseViewModel.MessageState): ResponseRepository()

    class RespFetchRefCodes     (val codes: MutableList<objModelReferralCode.Code>): ResponseRepository()
    class TaskUpdateReferralCode(val message: BaseViewModel.MessageState, val code: objModelReferralCode.ReferralResult): ResponseRepository()
    class TaskCreateReferralCode(val message: BaseViewModel.MessageState, val code: objModelReferralCode.Code): ResponseRepository()
    class RespDeleteRefCodes    (val idCode: String, val message: String): ResponseRepository()

    class TaskContactCategory(val message: BaseViewModel.MessageState, val content: List<ContactCategory>): ResponseRepository()
    class TaskContactsUpdate (
        val contacts: MutableList<FetchContact>,
        val letters : List<String>,
        val pagination: ContactDataPagination?): ResponseRepository()
    class TaskContactCreate  (val message: BaseViewModel.MessageState, val content: SaveContact): ResponseRepository()

    class TaskRoomCreate(val message: BaseViewModel.MessageState = BaseViewModel.MessageState.MessageNothing,
                         val content: roomModels.RoomByInvite): ResponseRepository()
    class TaskRoomUpdate(val message: BaseViewModel.MessageState = BaseViewModel.MessageState.MessageNothing,
                         val content: roomModels.RoomByInvite): ResponseRepository()
    class TaskRoomDelete(val message: BaseViewModel.MessageState = BaseViewModel.MessageState.MessageNothing,
                         val content: roomModels.RoomByInvite): ResponseRepository()
    object TaskNotRoom: ResponseRepository()

    class TaskSessionCreate(val message: BaseViewModel.MessageState, val content: MeetSession): ResponseRepository()
    class TaskSessionUpdate(val message: BaseViewModel.MessageState, val content: FinalSession): ResponseRepository()
    object TaskNotSession: ResponseRepository()

    class TaskChartLinearUdt (val message: BaseViewModel.MessageState, val content: LineDataSet): ResponseRepository()
    class TaskChartHalfPieUdt(val message: BaseViewModel.MessageState, val content: PieDataSet): ResponseRepository()
    class TaskChartBarUpdate (val message: BaseViewModel.MessageState, val content: BarDataSet): ResponseRepository()

    class SetupPartSuccess(val state: Boolean): ResponseRepository()
    class SetupPartFailure(val title: String ): ResponseRepository()

    class RequestCalendarGet(val message: BaseViewModel.MessageState, val content: MutableList<DtMeetCalendar>): ResponseRepository()
    class CalendarPst(val message: BaseViewModel.MessageState, val content: DtMeetCalendar): ResponseRepository()

    class ClndrOprtnError(val title: String ): ResponseRepository()

    class PrflSuccess(val message: BaseViewModel.MessageState, val content: MeetUserInfo): ResponseRepository()
    class PrflUpdate(
        val message: BaseViewModel.MessageState = BaseViewModel.MessageState.MessageNothing,
        val content: MeetUserInfo): ResponseRepository()
    object PrflNothing: ResponseRepository()

    data class TaskMeetGet(val contacts: MutableList<FetchContact>, val pagination: ContactDataPagination?): ResponseRepository()
//    class PrflFailure(val failure:  ): ResponseRepository()

    data class RewardBonusGet(val message: BaseViewModel.MessageState, val body: PlazaRewardBonus):ResponseRepository()
    data class RewardChartGet(
        val message: BaseViewModel.MessageState,
        val body: PlazaRewardChart,
        val statistic: TotalBalance):ResponseRepository()
    data class RewardHowToGet(val message: BaseViewModel.MessageState, val body: PlazaHowToReward):ResponseRepository()

    data class RecentToGet(val message: BaseViewModel.MessageState, val body: List<Banner>):ResponseRepository()


    data class HistoryToGet(val message: BaseViewModel.MessageState, val body: List<DtMeetHistory>):ResponseRepository()
    data class HistoryToPst(val message: BaseViewModel.MessageState):ResponseRepository()

}
