package meet.sumra.app.data.pojo.resp_ref

import com.google.gson.annotations.SerializedName

data class PostBodyCrtRefLink(
    @SerializedName("application_id")
    val applicationID: String,
    @SerializedName("is_default")
    val isDefault: Boolean,
    @SerializedName("note")
    val note: String
)
