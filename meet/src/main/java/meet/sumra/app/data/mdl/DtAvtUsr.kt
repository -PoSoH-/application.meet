package meet.sumra.app.data.mdl

import java.util.*

data class DtAvtUsr(
    val kUsrId: String = UUID.randomUUID().toString(),
    var kUsrAvatar: String
)
