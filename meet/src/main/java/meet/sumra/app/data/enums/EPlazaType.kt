package meet.sumra.app.data.enums

enum class EPlazaType(val pItem: Int, val pName: String) {
    DAY (pItem = 0, pName = "Day"),
    WEEK (pItem = 1, pName = "Week"),
    MONTH (pItem = 2, pName = "Month"),
    YEAR (pItem = 3, pName = "Year");
    companion object{
        fun fetchNameList() = mutableListOf(
            DAY.pName,
            WEEK.pName,
            MONTH.pName,
            YEAR.pName,
        )
    }
}
