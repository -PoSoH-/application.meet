package meet.sumra.app.data.mdl.statistic

import androidx.compose.runtime.MutableState
import com.github.mikephil.charting.data.BarEntry
import meet.sumra.app.data.configs.setupStatisticCharts.fetchBarData

data class Rewards(
    val cardRewardsAll      : Float = 0.0F,  // 16.500
    val cardRewardsUSD      : Float = 0.0F,  // 1254
    val cardRewardsTotal    : Int = 0,       // 2568,
    val cardRewardsSpendVal : Int = 0,  //  96,
    val cardRewardsSpendPer : Int = 0,  //  -2,
    val cardRewardsEarnedDiv: Int = 0,  //  900,
    val cardRewardsEarnedPer: Int = 0   //  -3,
)
