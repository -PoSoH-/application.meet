package meet.sumra.app.data.pojo.contacts

data class UserContactCreate (
    var uAvatar : String,

    val uNameP  : String,

    val uNameF  : String,

    val uNameM  : String,

    val uNameL  : String,

    val uNameS  : String,

    val uNameD  : String,
    val uNameN  : String,


    val uPhones : List<String>,
    val uEmails : List<String>,

    val uBirthday : String,
    val uNote : String

//    class Builder(){
//
//
//        fun build(): Builder
//    }

)
