package meet.sumra.app.data.pojo

class JsonPojoBanner {
    companion object {
        val fetchInfoBanner = """{
                              "type":"success",
                              "title":"Response of banner",
                              "message":"Response of banner is loaded!...",
                              "data":
                                  [
                                      {
                                           "id":"0001",
                                           "title":"Referral",
                                           "section_icon":"referral",
                                           "article":"Get your first bonus for invited user",
                                           "description":"Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user",
                                           "button_name":"Learn more",
                                           "button_color":"referral"
                                      },
                                      {
                                           "id":"0002",
                                           "title":"Rewards",
                                           "section_icon":"rewards",
                                           "article":"Earn Divirs for your time and activities",
                                           "description":"Earn Divirs for your time and activities. Earn Divirs for your time and activities. Earn Divirs for your time and activities. Earn Divirs for your and activities.  Divirs for your and activities. Earn Divirs for your time and activities. Earn Divirs for your time and activities. Earn Divirs for  time and activities. Earn  for your timeand activities. Earn Divirs for your time and activities. Earn Divirs for your time and activities. Earn Divirs for your time and activities. Earn  for your time and activities. Earn Divirs for  time and activities. Earn Divirs for your time and. Earn Divirs for your time and activities. Earn Divirs for your time and activities",
                                           "button_name":"Learn more",
                                           "button_color":"rewards"
                                      },
                                      {
                                           "id":"0003",
                                           "title":"Free plan",
                                           "section_icon":"free_plan",
                                           "article":"Your current membership plan",
                                           "description":"Your current membership plan. Your current membership plan. Your current membership plan. Your current membership plan. Your current membership plan. Your current  plan. Your current membership plan. Your current membership plan. Your current membership plan. current membership plan. Your current  plan. Your current membership plan. Your current membership plan. Your current plan. Your current membership plan. Your membership plan",
                                           "button_name":"Upgrade",
                                           "button_color":"free_plan"
                                      }
                                  ]
                            }"""
            get() = field
    }
}

