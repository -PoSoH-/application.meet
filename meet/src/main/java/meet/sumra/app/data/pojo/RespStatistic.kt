package meet.sumra.app.data.pojo

import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.LineDataSet
import meet.sumra.app.data.mdl.statistic.Contacts

sealed class RespStatistic {
    data class Balances (val body: meet.sumra.app.data.mdl.statistic.Balances): RespStatistic()
    data class Earnings (val body: meet.sumra.app.data.mdl.statistic.Earnings): RespStatistic()
    data class Referrals(val body: meet.sumra.app.data.mdl.statistic.Referrals): RespStatistic()
    data class Contacts (val body: meet.sumra.app.data.mdl.statistic.Contacts): RespStatistic()
    data class Rewards  (val body: meet.sumra.app.data.mdl.statistic.Rewards): RespStatistic()
    data class Cashback (val body: meet.sumra.app.data.mdl.statistic.CashBack): RespStatistic()
    data class RentPay  (val body: meet.sumra.app.data.mdl.statistic.RentPay): RespStatistic()
    data class Transfer (val body: meet.sumra.app.data.mdl.statistic.Transfers): RespStatistic()
    data class Overview (val body: meet.sumra.app.data.mdl.statistic.Overview): RespStatistic()
    data class Failure  (val fail: String): RespStatistic()
}