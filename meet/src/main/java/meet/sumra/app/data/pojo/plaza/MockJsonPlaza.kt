package meet.sumra.app.data.pojo.plaza

class MockJsonPlaza {
    companion object {

        /**
        /*type_commercial ; type_sumra_id ; type_mobile_app ; type_publishing_to ; type_publishing_with ; type_surveys ; type_reviews ; */
         */
        fun fetchRewardBonus() = """
            {
                "id":"",
                "total":{
                    "value":"4300",
                    "currency":"DR"
                },
                "daily_bonus":
                [
                    {
                        "type":"type_daily_bonus",
                        "title":"Check in daily bonus",
                        "currency":"DR",
                        "values":"50",
                        "values_all":"350",
                        "label":"Your bonus"
                    },
                    {
                        "type":"type_commercial", 
                        "title":"Commercial activity",
                        "currency":"DR",
                        "values":"250",
                        "values_all":"750",
                        "label":"Your bonus"
                    },
                    {
                        "type":"type_sumra_id",
                        "title":"Register at Sumra ID",
                        "currency":"DR",
                        "values":"250",
                        "values_all":"1350",
                        "label":"Your bonus"
                    },
                    {
                        "type":"type_mobile_app", 
                        "title":"Check in daily bonus",
                        "currency":"DR",
                        "values":"350",
                        "values_all":"1350",
                        "label":"Your bonus"
                    },
                    {
                        "type":"type_daily_bonus",
                        "title":"Download our mobile apps",
                        "currency":"DR",
                        "values":"100",
                        "values_all":"300",
                        "label":"Your bonus"
                    },
                    {
                        "type":"type_publishing_to", 
                        "title":"'$'DIViTS Tokens publishing",
                        "currency":"DR",
                        "values":"250",
                        "values_all":"1750",
                        "label":"No Activity Yet"
                    },
                    {
                        "type":"type_publishing_with",
                        "title":"'$'DIViTS Tokens publishing",
                        "currency":"DR",
                        "values":"3000",
                        "values_all":"0",
                        "label":"No Activity Yet"
                    },
                    {
                        "type":"type_surveys", 
                        "title":"Surveys or product tests ",
                        "currency":"DR",
                        "values":"100",
                        "values_all":"400",
                        "label":"Your bonus"
                    },
                    {
                        "type":"type_reviews", 
                        "title":"Products or services reviews ",
                        "currency":"DR",
                        "values":"250",
                        "values_all":"750",
                        "label":"Your bonus"
                    }
                ],
                "rewards_statistic":{
                    "title":"TOTAL DIVITS BALANCE",
                    "total_balance":{
                        "curency":"DR",
                        "value":"4300"
                    },
                    "labels":[
                        {
                            "id":"0",
                            "title":"Commercial activity"
                        },
                        {
                            "id":"1",
                            "title":"Download our mobile apps"
                        },
                        {
                            "id":"2",
                            "title":"Referrals balance paid-out"
                        },
                        {
                            "id":"3",
                            "title":"Register at Sumra ID"
                        }
                    ],
                    "charts":[
                        {
                            "part":[ 
                              {
                                "label_id":"0",
                                "value":"92"
                              },
                              {
                                "label_id":"1",
                                "value":"85"
                              },
                              {
                                "label_id":"2",
                                "value":"143"
                              },
                              {
                                "label_id":"3",
                                "value":"98"
                              }
                            ]
                        },
                        {
                            "part":[ 
                              {
                                "label_id":"0",
                                "value":"92"
                            },
                            {
                                "label_id":"1",
                                "value":"85"
                            },
                            {
                                "label_id":"2",
                                "value":"143"
                            },
                            {
                                "label_id":"3",
                                "value":"98"
                            }
                            ]
                        },
                        {
                            "part":[ {
                                "label_id":"0",
                                "value":"92"
                            },
                            {
                                "label_id":"1",
                                "value":"85"
                            },
                            {
                                "label_id":"2",
                                "value":"143"
                            },
                            {
                                "label_id":"3",
                                "value":"98"
                            }
                            ]
                        }
                    ]
                }
            }
        """.trimIndent()

        fun fetchRewardHowTo() = """
        {
            "id": "d4e254cf-37fa-4cd8-bb28-aaec23fc460e",
            "characters": [{
                    "type_section": "method_a",
                    "label": "Method 1",
                    "message": "Check in Daily (login and browse/use) on each of our platforms that you are registered (50 DR) Unlimited.",
                    "reward": {
                        "value": "50",
                        "currency": "DR"
                    },
                    "limit": {
                        "value": "Unlimited",
                        "currency": ""
                    }
                },
                {
                    "type_section": "method_b",
                    "label": "Method 2",
                    "message": "Undertake a commercial activity.",
                    "reward": {
                        "value": "50",
                        "currency": "DR"
                    },
                    "limit": {
                        "value": "Unlimited",
                        "currency": ""
                    }
                },
                {
                    "type_section": "method_c",
                    "label": "Method 3",
                    "message": "Register at Sumra ID.",
                    "reward": {
                        "value": "500",
                        "currency": "DR"
                    },
                    "limit": {
                        "value": "Unlimited",
                        "currency": ""
                    }
                },
                {
                    "type_section": "method_d",
                    "label": "Method 4",
                    "message": "Download our mobile applications.",
                    "reward": {
                        "value": "100",
                        "currency": "DR"
                    },
                    "limit": {
                        "value": "Unlimited",
                        "currency": ""
                    }
                },
                {
                    "type_section": "method_e",
                    "label": "Method 5",
                    "message": "Publish your ${'$'}DIViTS Tokens Bonus on Facebook, Instagram, Twitter, Snapchat, YouTube, TikTok, Reddit and send us a screenshot or link to it at: reviews@divitsrewards.com. Remember to add your Sumra ID unique number.",
                    "reward": {
                        "value": "250",
                        "currency": "DR"
                    },
                    "limit": {
                        "value": "1750",
                        "currency": "DR"
                    }
                },
                {
                    "type_section": "method_f",
                    "label": "Method 6",
                    "message": "Achieve ${'$'}1,000 in Referrals balance paid-out.",
                    "reward": {
                        "value": "3000",
                        "currency": "DR"
                    },
                    "limit": {
                        "value": "Unlimited",
                        "currency": ""
                    }
                },
                {
                    "type_section": "method_g",
                    "label": "Method 7",
                    "message": "Take part in our surveys or product tests.",
                    "reward": {
                        "value": "100",
                        "currency": "DR"
                    },
                    "limit": {
                        "value": "Unlimited",
                        "currency": ""
                    }
                },
                {
                    "type_section": "method_i",
                    "label": "Method 8",
                    "message": "Review any of the platform’s products or services on Facebook, Instagram, Twitter, Snapchat, YouTube, TikTok, Reddit  and send us a screenshot or link to it at: reviews@divitsrewards.com. Remember to add your Sumra ID unique number.",
                    "reward": {
                        "value": "250",
                        "currency": "DR"
                    },
                    "limit": {
                        "value": "1750",
                        "currency": "DR"
                    }
                }
            ]
        }""".trimMargin().trimIndent()
    }
}