package meet.sumra.app.data.mdl.earnings

data class PeriodState(
    var isActive : Boolean = false,
    val period   : String
)
