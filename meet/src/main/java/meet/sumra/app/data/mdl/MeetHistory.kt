package meet.sumra.app.data.mdl

import java.util.*

data class MeetHistory(
    val historyId  : Long,
    val meetName   : String,
    val sessionMeet: String,

    val startTimeMeet: Long,
    val finalTimeMeet: Long,

    val meetParameterId: Long,
    val meetTextChatId : Long
){
    companion object{
        fun getItemHistory() = MeetHistory(
            historyId        =  1234567890123456789,
            meetName         =  "chatTested00124",
            sessionMeet      =  UUID.randomUUID().toString(),
            startTimeMeet    =  System.currentTimeMillis() - 10000,
            finalTimeMeet    =  System.currentTimeMillis() -  9500,
            meetParameterId  =  1234567899876543219,
            meetTextChatId   =  7588398593859395728
        )

        fun getItemsHistory(count: Int = 3) = mutableListOf<MeetHistory>().apply {
            var pos = count
            while(pos > 0){
                add(
                    MeetHistory (
                        historyId        =  1234567890123456789,
                        meetName         =  "chatTested00${53*pos}",
                        sessionMeet      =  UUID.randomUUID().toString(),
                        startTimeMeet    =  System.currentTimeMillis() - 1000*pos,
                        finalTimeMeet    =  System.currentTimeMillis() -  950*pos,
                        meetParameterId  =  1234567899876543219,
                        meetTextChatId   =  7588398593859395728 ) )
                pos --
            }
        }
    }
}
