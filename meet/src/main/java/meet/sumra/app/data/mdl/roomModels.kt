package meet.sumra.app.data.mdl

object roomModels {

    data class RoomByInvite(
        val rRoomID           : String,
        val rRoomName         : String,
        val rRoomStatus       : Boolean,
        val rRoomInvite       : String,
        val rRoomParticipants : List<ParticipantsRoom>
    )

    data class ParticipantsRoom(
        val pUserID      : String,
        val pDisplayName : String,
        val pUserPhone   : String,
        val pUserEmail   : String
    )

//    data class DtMeetRoom(
//        val mId: String,                 // "94737fab-97d6-4bcd-b7cc-64c0f8bf43d6",
//        val mName: String,               // "testedRoomm",
//        val mSlug: String? = null,       // "qxl-yzr",
//        val mStatus: Boolean,            // 1,
//        val mInvite:  String,            // "myq-5ld-qxl-yzr",
//        val mRoomUsers: List<DtUserRoom> // participants room
//    )
//    data class DtUserRoom(
//        val id: String,            // "00000000-2000-2000-2000-000000000000"
//        val mDisplayName: String,  // "Gudrun Gleason",
//        val mSlug: String? = null, // "myq-5ld",
//        val mPhone: String,        // "+15345336619",
//        val mEmail: String,        // "ernest.grimes@example.org",
//        val mTotalTime: Float      // 428
//    )
}