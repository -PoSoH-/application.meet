package meet.sumra.app.data.mdl

import meet.sumra.app.data.mdl.MockDBContacts.firstPart
import sdk.net.meet.api.contacts.response.FetchContact
import sdk.net.meet.api.meet_calendar.response.*
import java.util.*

object MockDataBase {
    const val user_id = "00000000-2000-2000-2000-000000000000"

    val f = firstPart()

    val avatarMock = listOf<DtAvtUsr>()

    fun searchDataBase(value: List<String>): MutableList<FetchContact> {
        val dt = mutableListOf<FetchContact>()
        value.forEach { kndt ->
            f.forEach { avtr ->
                if(kndt.equals(avtr.id)) {
                    dt.add(avtr)
                }
            }
        }
        return dt
    }

    fun readDataBaseCalendar() = FetchMeetCalendarAll(
            type = "success",
            title = "Loaded data calendar",
            message = "Success",
            body = listOf(
                MeetCalendar(
                type = "calendar",
                id   = UUID.randomUUID().toString(),
                attributes = MeetCalendarAttr(
                    title = "Lorem ipsum",
                    note = "Lorem ipsum dolor more text special not issues hjsfhj jshdfjhj ksjkdfj skdjfhjshdjfh jhjskdfjh sdfjshfksjdfh skdjfhsk",
                    startDate = "2021-09-02T10:30:00.000000Z",
                    finalDate = "2021-09-02T10:55:00.000000Z",
                    ownerId = user_id,
                    id = UUID.randomUUID().toString(),
                    password = null,
                    isLobby = false,
                    created_at = "2021-08-31T08:37:33.000000Z",
                    updated_at = "2021-08-31T08:37:33.000000Z",
                    participants = listOf(
                        "a5c6ecd4-7d37-49f1-957b-d28460f4443c",
                        "71c75003-127e-4643-a83d-42c4bceb122b",
                        "0fe0bb2b-fa67-48cc-b51a-855df815e09d",
                        "e504587d-4294-4579-aec2-765d9d29ab93",
                        "7d21b7a1-b3c9-4109-b82e-597fc5a0ab71",
                        "ee745eb9-a924-4919-9c03-39fcaad34648"),
                    liveStream = MeetStream(streamKey = "hj5da-hdjf-jdhsd6s-hd5d", streamLink = "youtube.link.com")
                )
            ),
            MeetCalendar(
                    type = "calendar",
                    id   = UUID.randomUUID().toString(),
                    attributes = MeetCalendarAttr(
                        title = "Lorem ipsum",
                        note = "Lorem ipsum dolor more text special not issues hjsfhj jshdfjhj ksjkdfj skdjfhjshdjfh jhjskdfjh sdfjshfksjdfh skdjfhsk",
                        startDate = "2021-09-02T10:30:00.000000Z",
                        finalDate = "2021-09-02T10:55:00.000000Z",
                        ownerId = user_id,
                        id = UUID.randomUUID().toString(),
                        password = null,
                        isLobby = false,
                        created_at = "2021-08-31T08:37:33.000000Z",
                        updated_at = "2021-08-31T08:37:33.000000Z",
                        participants = listOf(
                            "7c1f7300-5a5f-4c11-8992-8cff34830d86",
                            "e13b36c3-6c80-4e71-98f9-585bd4e8813e",
                            "f2d68b0f-de09-4dde-86ca-e8ea0825909f",
                            "38a67f63-4d3c-4102-85e9-23ae20499058"),
                        liveStream = MeetStream(streamKey = "hj5da-hdjf-jdhsd6s-hd5d", streamLink = "youtube.link.com")
                    )
                ),
            MeetCalendar(
                    type = "calendar",
                    id   = UUID.randomUUID().toString(),
                    attributes = MeetCalendarAttr(
                        title = "Lorem ipsum",
                        note = "Lorem ipsum dolor more text special not issues hjsfhj jshdfjhj ksjkdfj skdjfhjshdjfh jhjskdfjh sdfjshfksjdfh skdjfhsk",
                        startDate = "2021-09-02T10:30:00.000000Z",
                        finalDate = "2021-09-02T10:55:00.000000Z",
                        ownerId = user_id,
                        id = UUID.randomUUID().toString(),
                        password = null,
                        isLobby = false,
                        created_at = "2021-08-31T08:37:33.000000Z",
                        updated_at = "2021-08-31T08:37:33.000000Z",
                        participants = listOf(
                            "5172c1e3-3507-4bea-b43d-04a9ee0dbd62",
                            "75c73218-4a68-46e4-acd7-57abf1d40ed5",
                            "8e36daf4-0082-4474-8f51-931033abe063",
                            "c757a63e-9b74-44f8-b811-e9ac86f72dfd",
                            "26b48b07-7a08-49f0-8cdd-916902e3a1d7"),
                        liveStream = MeetStream(streamKey = "hj5da-hdjf-jdhsd6s-hd5d", streamLink = "youtube.link.com")
                    )
                ),
            MeetCalendar(
                    type = "calendar",
                    id   = UUID.randomUUID().toString(),
                    attributes = MeetCalendarAttr(
                        title = "Lorem ipsum",
                        note = "Lorem ipsum dolor more text special not issues hjsfhj jshdfjhj ksjkdfj skdjfhjshdjfh jhjskdfjh sdfjshfksjdfh skdjfhsk",
                        startDate = "2021-09-02T10:30:00.000000Z",
                        finalDate = "2021-09-02T10:55:00.000000Z",
                        ownerId = user_id,
                        id = UUID.randomUUID().toString(),
                        password = null,
                        isLobby = false,
                        created_at = "2021-08-31T08:37:33.000000Z",
                        updated_at = "2021-08-31T08:37:33.000000Z",
                        participants = listOf(
                            "1191d5fd-6342-4962-be85-1368c9036f59",
                            "262ecfa9-01da-466f-93e7-1337919bd224"),
                        liveStream = MeetStream(streamKey = "hj5da-hdjf-jdhsd6s-hd5d", streamLink = "youtube.link.com")
                    )
                ),
            MeetCalendar(
                    type = "calendar",
                    id   = UUID.randomUUID().toString(),
                    attributes = MeetCalendarAttr(
                        title = "Lorem ipsum",
                        note = "Lorem ipsum dolor more text special not issues hjsfhj jshdfjhj ksjkdfj skdjfhjshdjfh jhjskdfjh sdfjshfksjdfh skdjfhsk",
                        startDate = "2021-09-02T10:30:00.000000Z",
                        finalDate = "2021-09-02T10:55:00.000000Z",
                        ownerId = user_id,
                        id = UUID.randomUUID().toString(),
                        password = null,
                        isLobby = false,
                        created_at = "2021-08-31T08:37:33.000000Z",
                        updated_at = "2021-08-31T08:37:33.000000Z",
                        participants = listOf(
                            "9b1febd3-451a-4079-bb5e-b3bf4292ed9c",
                            "a50e2565-9525-40aa-8c19-0c68000c7d1a"),
                        liveStream = MeetStream(streamKey = "hj5da-hdjf-jdhsd6s-hd5d", streamLink = "youtube.link.com")
                    )
                ),
            MeetCalendar(
                    type = "calendar",
                    id   = UUID.randomUUID().toString(),
                    attributes = MeetCalendarAttr(
                        title = "Lorem ipsum",
                        note = "Lorem ipsum dolor more text special not issues hjsfhj jshdfjhj ksjkdfj skdjfhjshdjfh jhjskdfjh sdfjshfksjdfh skdjfhsk",
                        startDate = "2021-09-02T10:30:00.000000Z",
                        finalDate = "2021-09-02T10:55:00.000000Z",
                        ownerId = user_id,
                        id = UUID.randomUUID().toString(),
                        password = null,
                        isLobby = false,
                        created_at = "2021-08-31T08:37:33.000000Z",
                        updated_at = "2021-08-31T08:37:33.000000Z",
                        participants = listOf(
                            "af8e3b2e-8bd1-4712-83ec-3bfa8dae53a3",
                            "168faed3-ef7f-4794-adb0-42f122b4d1e6",
                            "8652f231-d93f-4da8-8340-869f3b499447"),
                        liveStream = MeetStream(streamKey = "hj5da-hdjf-jdhsd6s-hd5d", streamLink = "youtube.link.com")
                    )
                ),
            MeetCalendar(
                    type = "calendar",
                    id   = UUID.randomUUID().toString(),
                    attributes = MeetCalendarAttr(
                        title = "Lorem ipsum",
                        note = "Lorem ipsum dolor more text special not issues hjsfhj jshdfjhj ksjkdfj skdjfhjshdjfh jhjskdfjh sdfjshfksjdfh skdjfhsk",
                        startDate = "2021-09-02T10:30:00.000000Z",
                        finalDate = "2021-09-02T10:55:00.000000Z",
                        ownerId = user_id,
                        id = UUID.randomUUID().toString(),
                        password = null,
                        isLobby = false,
                        created_at = "2021-08-31T08:37:33.000000Z",
                        updated_at = "2021-08-31T08:37:33.000000Z",
                        participants = listOf(
                            "7f1626f2-fa93-4537-b474-5f371d410b27",
                            "232f213a-4a75-4fa6-97c7-fb2e591c25f6",
                            "173e453f-15e9-48dd-ac76-f1292cec93da",
                            "6fee7cd0-7795-4a8b-a68a-af58c5a2c271",
                            "0e41b75d-a43f-405c-b003-095b664c9e47",
                            "8cbf05df-ccf3-437c-9410-1b7aed499115",
                            "36b4173d-3889-431b-8340-36498843523f"),
                        liveStream = MeetStream(streamKey = "hj5da-hdjf-jdhsd6s-hd5d", streamLink = "youtube.link.com")
                    )
                ),
            MeetCalendar(
                    type = "calendar",
                    id   = UUID.randomUUID().toString(),
                    attributes = MeetCalendarAttr(
                        title = "Lorem ipsum",
                        note = "Lorem ipsum dolor more text special not issues hjsfhj jshdfjhj ksjkdfj skdjfhjshdjfh jhjskdfjh sdfjshfksjdfh skdjfhsk",
                        startDate = "2021-09-02T10:30:00.000000Z",
                        finalDate = "2021-09-02T10:55:00.000000Z",
                        ownerId = user_id,
                        id = UUID.randomUUID().toString(),
                        password = null,
                        isLobby = false,
                        created_at = "2021-08-31T08:37:33.000000Z",
                        updated_at = "2021-08-31T08:37:33.000000Z",
                        participants = listOf(
                            "b0a27971-789f-4a38-b52b-6aaf77cded0b",
                            "549c9811-a3b6-489b-9840-f74f5b82b8e7",
                            "8d8c02bc-8af1-4054-b5e8-2e31176b1a19",
                            "dbc9dc2d-20f9-45bd-8164-3a865ecae0b6",
                            "ba77dee9-5473-4c6c-b9ef-398f94b122fe"),
                        liveStream = MeetStream(streamKey = "hj5da-hdjf-jdhsd6s-hd5d", streamLink = "youtube.link.com")
                    )
                )
            )
        )


}