package meet.sumra.app.data.mdl.plaza

data class TabPlazaShow (
    val text: Int,
    val icon: Int
)