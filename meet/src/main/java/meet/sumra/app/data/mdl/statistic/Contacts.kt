package meet.sumra.app.data.mdl.statistic

data class Contacts(

    val cardBookAll: Int = 658,      // = 2541,
    val cardBookInv: Int = 354,      // = 1425,
    val cardBookPer: Float = 12.5F,  // = 25F,
    val cardConversionRate: Int = 55 // = 55 percent...

)
