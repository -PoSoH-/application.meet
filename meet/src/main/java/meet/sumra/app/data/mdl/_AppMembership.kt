package meet.sumra.app.data.mdl

data class _AppMemberShip(
    val userID: String,
    val membership: _AppMemberPlan,
    val createdAt: String,
    val updatedAt: String,
)

data class _AppMemberPlan(
    val memberShipID: String,
    val memberShipPlan: Int
)

/**
 * membership plan
 *   0 - platinum
 *   1 - gold
 *   2 - silver
 *   3 - bronze
 * */