package meet.sumra.app.data.pojo.res

import meet.sumra.app.vm.vm_home.StateHome

class ItemMenu ( val itemName: Int, val itemIconOf: Int, val itemIconOn: Int, val state: StateHome)