package meet.sumra.app.data.mdl

import com.google.gson.annotations.SerializedName

data class BannerLoaded (
    @SerializedName("type")
    val type: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val banner: List<Banner>
)

data class Banner (
    @SerializedName("id")
    val id: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("section_icon")
    val sectionIcon: String,
    @SerializedName("article")
    val article: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("button_name")
    val buttonName: String,
    @SerializedName("button_color")
    val buttonColor: String
)

//"type":"success",
//"title":"Response of banner",
//"data":
//[
//{
//    "id":"0001",
//    "title":"Referral",
//    "section_icon":"R.drawable.ic_banner_referral",
//    "article":"Get your first bonus for invited user",
//    "description":"Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user. Get your first bonus for invited user",
//    "button_name":"Learn more",
//    "button_color":"#F16868"
//},
//{
//    "id":"0002",
//    "title":"Rewards",
//    "section_icon":"R.drawable.ic_banner_rewards",
//    "article":"Earn Divirs for your time and activities",
//    "description":"Earn Divirs for your time and activities. Earn Divirs for your time and activities. Earn Divirs for your time and activities. Earn Divirs for your and activities.  Divirs for your and activities. Earn Divirs for your time and activities. Earn Divirs for your time and activities. Earn Divirs for  time and activities. Earn  for your timeand activities. Earn Divirs for your time and activities. Earn Divirs for your time and activities. Earn Divirs for your time and activities. Earn  for your time and activities. Earn Divirs for  time and activities. Earn Divirs for your time and. Earn Divirs for your time and activities. Earn Divirs for your time and activities",
//    "button_name":"Learn more",
//    "button_color":"#38CB89"
//},
//{
//    "id":"0003",
//    "title":"Free plan",
//    "section_icon":"R.drawable.ic_banner_free_plan",
//    "article":"Your current membership plan",
//    "description":"Your current membership plan. Your current membership plan. Your current membership plan. Your current membership plan. Your current membership plan. Your current  plan. Your current membership plan. Your current membership plan. Your current membership plan. current membership plan. Your current  plan. Your current membership plan. Your current membership plan. Your current plan. Your current membership plan. Your membership plan",
//    "button_name":"Upgrade",
//    "button_color":"#FFAB00"
//}
//]
//}"""