package meet.sumra.app.data.enums

enum class EDialog {
    MEET_CREATE,
    MEET_JOIN
}