package meet.sumra.app.data.mdl.plaza

import com.github.mikephil.charting.data.BarData

data class PageRewardsBonus(
    val plazaRewardBonus: MutableList<PlazaRewardBonus> = mutableListOf(),
    val plazaRewardStatistic: MutableList<TotalBalance> = mutableListOf<TotalBalance>(),
    val plazaRewardChart: MutableList<BarData> = mutableListOf<BarData>()
)
