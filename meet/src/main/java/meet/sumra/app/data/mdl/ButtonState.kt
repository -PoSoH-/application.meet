package meet.sumra.app.data.mdl

import meet.sumra.app.R

data class ButtonState(var enable: Boolean = false){

    private val enabled = StateParams(
        icon = R.drawable.ic_hide_excess_fields,
        tint = R.color.gray_700
    )

    private val disabled = StateParams(
        icon = R.drawable.ic_cntct_user_add_disclose,
        tint = R.color.white
    )

    fun switchStateGetParams():StateParams{
        enable = !enable
        return getInfoButtonState()
    }

    fun getInfoButtonState():StateParams{
        return when(enable) {
            true -> enabled
            false -> disabled
        }
    }

    fun getState() = enable

    data class StateParams (
        val icon: Int,
        val tint: Int
    )
}
