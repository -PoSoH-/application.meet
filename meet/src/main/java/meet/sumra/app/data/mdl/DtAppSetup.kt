package meet.sumra.app.data.mdl

import android.content.Context
import com.google.gson.annotations.SerializedName

data class DtAppSetup(
    @SerializedName("primary_id")
    val primaryID: String,
    @SerializedName("state_video")
    var stateVideo: Boolean = true,
    @SerializedName("state_voice")
    var stateVoice: Boolean = true,
    @SerializedName("state_dark_mode")
    var stateDarkMode: Boolean = false,
    @SerializedName("state_additional")
    var stateAdditional: Boolean = false,

    @SerializedName("state_reserve_a")
    var stateA: Boolean = false,
    @SerializedName("state_reserve_b")
    var stateB: Boolean = false,
    @SerializedName("state_reserve_c")
    var stateC: Boolean = false,
    @SerializedName("state_reserve_d")
    var stateD: Boolean = false
) {
    companion object{
        val SETUP_KEY = "86762f69-cac9-4550-9011-47e3fdf88313::SETTINGS"
    }
}
