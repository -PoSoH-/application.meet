package meet.sumra.app.data.mdl

data class DtProfileUser(
    val userID: String,
    val avatar: String? = null,
    val prefixName: String? = null,
    val firstName: String,
    val middleName: String? = null,
    val lastName: String,
    val suffixName: String? = null,
    val displayName: String,
    val nickname: String? = null,
    val birthday: String? = null,
    val phones: MutableList<Phone>?,
    val emails: MutableList<Email>?,
    val note: String? = null
)
data class Phone(val phone: String, val type : String, val isDefault: Boolean)
data class Email(val email: String, val type : String, val isDefault: Boolean)

