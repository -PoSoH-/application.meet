package meet.sumra.app.data.mdl

data class DtMeetHistory (
    val historyId   : Long,
    val meetName    : String,
    val sessionMeet : String,
    val meetDate    : String, //21 September 2021
    val meetStartTime    : String, // 15:10-15:55
    val meetFinalTime    : String, // 15:10-15:55
//    val startTimeMeet : Long,
//    val finalTimeMeet : Long,
//    val meetParameterId : Long,
//    val meetTextChatId  : Long
)