package meet.sumra.app.data.mdl.plaza

import java.util.*

data class PlazaEventDetail(
    val id: String = UUID.randomUUID().toString()
)
