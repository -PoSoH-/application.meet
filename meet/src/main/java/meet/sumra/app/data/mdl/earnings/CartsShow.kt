package meet.sumra.app.data.mdl.earnings

import android.annotation.SuppressLint
import android.util.Log
import androidx.appcompat.content.res.AppCompatResources
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.*
import com.github.tehras.charts.bar.BarChart
import meet.sumra.app.R
import meet.sumra.app.data.configs.setupStatisticCharts
import meet.sumra.app.data.configs.setupStatisticCharts.setupBarChart
import meet.sumra.app.data.configs.setupStatisticCharts.setupBarChartOverview
import meet.sumra.app.data.configs.setupStatisticCharts.setupLineChart
import meet.sumra.app.data.mdl.statistic.*
import meet.sumra.app.vm.vm_statistic.StateStatistic
import kotlin.math.acos

object cards {

    /**
    ic_statistic_card_wallet
    ic_statistic_card_cash
    ic_statistic_card_earning
    ic_statistic_card_referral
    ic_statistic_card_renpayment
    ic_statistic_card_reward
    ic_statistic_card_transfer
    **/

    interface PeriodValue{
        fun period(period: EPeriod)
    }
    enum class EPeriod(val label: String){
        DAY (label = "DAY"),
        WEEK (label = "WEEK"),
        MONTH (label = "MONTH"),
        YEAR (label = "YEAR");
        companion object {
            fun getPeriods() = mutableListOf<String>(
                    DAY.label,
                    WEEK.label,
                    MONTH.label,
                    YEAR.label
                )
            fun byCount(count: Int): EPeriod{
                return when(count){
                    0 -> DAY
                    1 -> WEEK
                    2 -> MONTH
                    else -> YEAR
                }
            }
        }
    }

    private val horPad = 20.dp
    private val verPad = 12.dp
    private val allPad = 20.dp

//    private val cardHeight = 193.dp
    private val cardRadius = 10.dp
    private val lineHeight = 84.dp

    val sTxt = TextStyle(color = Color(0xffB0B7C3), fontSize = 15.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W500)),
        textAlign = TextAlign.Justify)
    val sTxtBalW400 = TextStyle(color = Color(0xff323B4B), fontSize = 26.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W500)),
        textAlign = TextAlign.Justify)
    val sTxtBalW700 = TextStyle(color = Color(0xff323B4B), fontSize = 26.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W700)),
        textAlign = TextAlign.Justify)
    val sTxtNameW700 = TextStyle(color = Color(0xff323B4B), fontSize = 20.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W700)),
        textAlign = TextAlign.Justify)
    val sTxtGryW700 = TextStyle(color = Color(0xffB0B7C3), fontSize = 26.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W700)),
        textAlign = TextAlign.Justify)
    val sTxtIncGryW400 = TextStyle(color = Color(0xffB0B7C3), fontSize = 13.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W400)),
        textAlign = TextAlign.Justify)
    val sTxtIncBlcW400 = TextStyle(color = Color(0xff323B4B), fontSize = 13.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W400)),
        textAlign = TextAlign.Justify)
    val sTxtIncGrnW400 = TextStyle(color = Color(0xff52C41A), fontSize = 13.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W400)),
        textAlign = TextAlign.Justify)
    val sTxtIncRedW400 = TextStyle(color = Color(0xffFF4848), fontSize = 13.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W400)),
        textAlign = TextAlign.Justify)
    val sTxtAblGryW600 = TextStyle(color = Color(0xff8A94A6), fontSize = 13.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W600)),
        textAlign = TextAlign.Justify)
    val sTxtAblBalW600 = TextStyle(color = Color(0xff323B4B), fontSize = 13.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W600)),
        textAlign = TextAlign.Justify)

    val sTxtBookBlcW400 = TextStyle(color = Color(0xff323B4B), fontSize = 13.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W700)),
        textAlign = TextAlign.Justify)
    val sTxtBookGryW400 = TextStyle(color = Color(0xffB0B7C3), fontSize = 13.sp,
        fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W700)),
        textAlign = TextAlign.Justify)

    @Composable
    @ExperimentalMaterialApi
    fun CardCurrentBalances(
        balances: MutableState<Balances>,
        loading : MutableState<Boolean>
    ){
        Row(
            modifier = Modifier
                .fillMaxSize(1f)
                .padding(top = verPad, bottom = verPad, start = horPad, end = horPad)
        ) {
            Card(
                onClick = { /*TODO*/ },
                modifier = Modifier
//                    .height(cardHeight)
                    .fillMaxSize(1f),
                backgroundColor = Color(0xffFFffFF),
                shape = RoundedCornerShape(size = cardRadius)
            ) {
                Box(
                    modifier = Modifier.fillMaxSize(1f),
                    contentAlignment = Alignment.Center
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth(1f)
                            .padding(all = allPad)
                    ) {
                        Row(
                            modifier = Modifier
                                .fillMaxWidth(1f)
                                .padding(bottom = 4.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = stringResource(id = R.string.statisticCardBalances),
                                style = sTxt
                            )
                            Spacer(modifier = Modifier.width(10.dp))
                            Image(
                                painter = painterResource(id = R.drawable.ic_statistic_card_info),
                                contentDescription = "Info icon"
                            )
                            Spacer(modifier = Modifier.weight(1f))
                            Image(
                                modifier = Modifier
                                    .height(32.dp)
                                    .width(32.dp),
                                painter = painterResource(id = R.drawable.ic_statistic_card_wallet),
                                contentDescription = "Info icon"
                            )
                        }
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(top = 10.dp, bottom = 10.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                modifier = Modifier.padding(end = 8.dp),
                                text = "$ ",
                                style = sTxtBalW400
                            )
                            Text(
                                text = "${balances.value.cardManyAll}",
                                style = sTxtBalW700
                            )
                        }
                        Row(
                            modifier = Modifier
                                .height(21.dp)
                                .fillMaxWidth(1f),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = stringResource(id = R.string.statisticCardIncome),
                                style = sTxtIncGryW400
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = "$ ${balances.value.cardManyUpVal}",
                                style = sTxtIncBlcW400
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = "(+${balances.value.cardManyUpPer}%)",
                                style = sTxtIncGrnW400
                            )
                            Image(
                                modifier = Modifier
                                    .height(10.dp)
                                    .width(10.dp),
                                painter = painterResource(id = R.drawable.ic_statistic_card_arrow_up),
                                contentDescription = "Info icon down"
                            )
                        }
                        Row(
                            modifier = Modifier
                                .height(21.dp)
                                .fillMaxWidth(1f),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = stringResource(id = R.string.statisticCardSpend),
                                style = sTxtIncGryW400
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = "$ ${balances.value.cardManyDownVal}",
                                style = sTxtIncBlcW400
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = "(-${balances.value.cardManyDownPer}%)",
                                style = sTxtIncRedW400
                            )
                            Image(
                                modifier = Modifier
                                    .height(10.dp)
                                    .width(10.dp),
                                painter = painterResource(id = R.drawable.ic_statistic_card_arrow_down),
                                contentDescription = "Info icon up"
                            )
                        }
                        Row(
                            modifier = Modifier.padding(top = 10.dp, bottom = 10.dp),
                        ) {
                            Divider(
                                modifier = Modifier
                                    .height(1.dp)
                                    .fillMaxWidth(),
                                color = Color(0xffF0F0F6)
                            )
                        }
                        Row(
                            modifier = Modifier.fillMaxSize(1f),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = stringResource(id = R.string.statisticCardAvailableBalance),
                                style = sTxtAblGryW600
                            )
                            Text(
                                text = "$ ${balances.value.cardAvailable}",
                                style = sTxtAblBalW600
                            )
                        }
                    }
                    if(loading.value) {
                        Box(
                            modifier = Modifier
//                                .fillMaxHeight(1f)
//                                .fillMaxWidth(1f)
                                .width(120.dp)
                                .height(120.dp)
                                .background(
                                    color = Color(0x33181925),
                                    shape = RoundedCornerShape(cardRadius)
                                ),
                            contentAlignment = Alignment.Center
//                            verticalAlignment = Alignment.CenterVertically
                        ) {
//                            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                            CircularProgressIndicator(
                                modifier = Modifier
                                    .width(60.dp)
                                    .height(60.dp)
                            )
//                            }
                        }
                    }
                }
            }
        }
    }

    /**  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **/

    @Composable
    @ExperimentalMaterialApi
    fun CardGlobalEarnings(
        earnings : MutableState<Earnings>,
        loading  : MutableState<Boolean>
    ){
        Row(
            modifier = Modifier
                .fillMaxSize(1f)
                .padding(top = verPad, bottom = verPad, start = horPad, end = horPad)
        ) {
            Box(
                modifier = Modifier
                    .fillMaxSize(1f)
                    .background(
                        color = Color.Transparent,
                        shape = RoundedCornerShape(size = cardRadius)
                    )
            ) {
                Card(
                    onClick = { /*TODO*/ },
                    modifier = Modifier
                        .fillMaxSize(1f),
                    backgroundColor = Color(0xffFFffFF),
                    shape = RoundedCornerShape(size = cardRadius)
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth(1f)
                            .padding(vertical = allPad)
                    ) {
                        Row(
                            modifier = Modifier
                                .fillMaxWidth(1f)
                                .padding(horizontal = allPad), //bottom = 4.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = stringResource(id = R.string.statisticCardGlobalEarnings),
                                style = sTxt
                            )
                            Spacer(modifier = Modifier.width(10.dp))
                            Image(
                                painter = painterResource(id = R.drawable.ic_statistic_card_info),
                                contentDescription = "Info icon"
                            )
                            Spacer(modifier = Modifier.weight(1f))
                            Image(
                                modifier = Modifier
                                    .height(32.dp)
                                    .width(32.dp),
                                painter = painterResource(id = R.drawable.ic_statistic_card_earning),
                                contentDescription = "Info icon"
                            )
                        }
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(
                                    top = 10.dp, bottom = 0.dp,
                                    start = allPad, end = allPad
                                ),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                modifier = Modifier.padding(end = 8.dp),
                                text = "$ ",
                                style = sTxtBalW400
                            )
                            Text(
                                text = "${earnings.value.cardManyAll}",
                                style = sTxtBalW700
                            )
                        }
                        Row() {
                            AndroidView(
                                factory = { context ->
                                    LineChart(context).apply {
                                        val le = earnings.value.cardLineChart
                                        val ds = LineDataSet(
                                            le.value,
                                            "Earnings...")
                                        ds.fillDrawable =
                                            context.getDrawable(R.drawable.statistic_linear_chart_gradient_red)
                                        data = LineData(ds)
                                        setupLineChart(
                                            this,
                                            android.graphics.Color.parseColor("#FF4A7A")
                                        )
                                    }
                                },
                                modifier = Modifier
                                    .height(lineHeight)
                                    .fillMaxWidth(1f)
//                                    .padding(top = 6.dp, bottom = 10.dp)
                                    /*.background(color = Color.LightGray)*/
                            )
                        }
                        Row(
                            modifier = Modifier.padding(
                                start = allPad, end = allPad,
                                top = 0.dp, bottom = 10.dp),
                        ) {
                            Divider(
                                modifier = Modifier
                                    .height(1.dp)
                                    .fillMaxWidth(),
                                color = Color(0xffF0F0F6)
                            )
                        }
                        Row(
                            modifier = Modifier
                                .fillMaxSize(1f)
                                .padding(horizontal = allPad),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = stringResource(id = R.string.statisticCardGlobEarnMonth),
                                style = sTxtAblGryW600
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = "$ ${earnings.value.cardManyMonth}",
                                style = sTxtAblBalW600
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = when {
                                    earnings.value.cardPercent > 0 -> "(+${earnings.value.cardPercent}%)"
                                    earnings.value.cardPercent < 0 -> "(${earnings.value.cardPercent}%)"
                                    else -> "(0%)"
                                },
                                style = when {
                                    earnings.value.cardPercent > 0 -> sTxtIncGrnW400
                                    earnings.value.cardPercent < 0 -> sTxtIncRedW400
                                    else -> sTxtAblBalW600
                                }
                            )
                            Image(
                                modifier = Modifier
                                    .height(10.dp)
                                    .width(10.dp),
                                painter = when {
                                    earnings.value.cardPercent > 0 -> painterResource(id = R.drawable.ic_statistic_card_arrow_up)
                                    earnings.value.cardPercent < 0 -> painterResource(id = R.drawable.ic_statistic_card_arrow_down)
                                    else -> painterResource(id = R.drawable.ic_statistic_card_arrow_null)
                                },
                                contentDescription = "Info icon down"
                            )
                        }
                    }
                    if(loading.value) {
                        Box(
                            modifier = Modifier
//                                .fillMaxHeight(1f)
//                                .fillMaxWidth(1f)
                                .width(120.dp)
                                .height(120.dp)
                                .background(
                                    color = Color(0x33181925),
                                    shape = RoundedCornerShape(cardRadius)
                                ),
                            contentAlignment = Alignment.Center
//                            verticalAlignment = Alignment.CenterVertically
                        ) {
//                            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                            CircularProgressIndicator(
                                modifier = Modifier
                                    .width(60.dp)
                                    .height(60.dp)
                            )
//                            }
                        }
                    }
                }
            }
        }
    }

    /**  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **/

    @Composable
    @ExperimentalMaterialApi
    fun CardReferrals(
        referrals: MutableState<Referrals>,
        loading  : MutableState<Boolean>
    ){
        Row(
            modifier = Modifier
                .fillMaxSize(1f)
                .padding(top = verPad, bottom = verPad, start = horPad, end = horPad)
        ) {
            Card(
                onClick = { /*TODO*/ },
                modifier = Modifier
                    .fillMaxSize(1f),
                backgroundColor = Color(0xffFFffFF),
                shape = RoundedCornerShape(size = cardRadius)
            ) {
                Box(
                    modifier = Modifier.fillMaxSize(1f)
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth(1f)
                            .padding(vertical = allPad)
                    ) {
                        Row(
                            modifier = Modifier
                                .fillMaxWidth(1f)
                                .padding(horizontal = allPad),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = stringResource(id = R.string.statisticCardReferrals),
                                style = sTxt
                            )
                            Spacer(modifier = Modifier.width(10.dp))
                            Image(
                                painter = painterResource(id = R.drawable.ic_statistic_card_info),
                                contentDescription = "Info icon"
                            )
                            Spacer(modifier = Modifier.weight(1f))
                            Image(
                                modifier = Modifier
                                    .height(32.dp)
                                    .width(32.dp),
                                painter = painterResource(id = R.drawable.ic_statistic_card_referral),
                                contentDescription = "Info icon"
                            )
                        }
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(
                                    top = 10.dp, bottom = 0.dp,
                                    start = allPad, end = allPad
                                ),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = "${referrals.value.cardInvite}",
                                style = sTxtBalW700
                            )
                        }
                        Row(

                        ) {
                            AndroidView(
                                factory = {
                                    BarChart(it).apply {
                                        val le = referrals.value.cardBarChart
                                        val lbs = BarDataSet(le.value, "Referrals")
                                        data = BarData(lbs)
                                        setupBarChart(
                                            this,
                                            android.graphics.Color.parseColor("#1D4BEF")
                                        )
                                    }
                                },
                                modifier = Modifier
                                    .height(lineHeight)
                                    .fillMaxWidth(1f)
                                    .padding(top = 0.dp, bottom = 10.dp)
//                                    .background(color = Color.LightGray)
                            )
                        }
                        Row(
                            modifier = Modifier.padding(
                                top = 0.dp, bottom = 10.dp,
                                end = allPad, start = allPad),
                        ) {
                            Divider(
                                modifier = Modifier
                                    .height(1.dp)
                                    .fillMaxWidth(),
                                color = Color(0xffF0F0F6)
                            )
                        }
                        Row(
                            modifier = Modifier
                                .fillMaxSize(1f)
                                .padding(horizontal = allPad),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = stringResource(id = R.string.statisticCardInviteThis),
                                style = sTxtAblGryW600
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = "${referrals.value.cardReferrals}",
                                style = sTxtAblBalW600
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = if (referrals.value.cardGrOrFall > 0) {
                                    "(+${referrals.value.cardGrOrFall}%)"
                                } else if (referrals.value.cardGrOrFall < 0) {
                                    "(${referrals.value.cardGrOrFall}%)"
                                } else {
                                    "(0%)"
                                },
                                style = when {
                                    referrals.value.cardGrOrFall > 0 -> sTxtIncGrnW400
                                    referrals.value.cardGrOrFall < 0 -> sTxtIncRedW400
                                    else -> sTxtAblBalW600
                                }
                            )
                            Image(
                                modifier = Modifier
                                    .height(10.dp)
                                    .width(10.dp),
                                painter = when {
                                    referrals.value.cardGrOrFall > 0 -> painterResource(id = R.drawable.ic_statistic_card_arrow_up)
                                    referrals.value.cardGrOrFall < 0 -> painterResource(id = R.drawable.ic_statistic_card_arrow_down)
                                    else -> painterResource(id = R.drawable.ic_statistic_card_arrow_null)
                                },
                                contentDescription = "Info icon down"
                            )
                        }
                    }
                    if(loading.value) {
                        Box(
                            modifier = Modifier
//                                .fillMaxHeight(1f)
//                                .fillMaxWidth(1f)
                                .width(120.dp)
                                .height(120.dp)
                                .background(
                                    color = Color(0x33181925),
                                    shape = RoundedCornerShape(cardRadius)
                                ),
                            contentAlignment = Alignment.Center
//                            verticalAlignment = Alignment.CenterVertically
                        ) {
//                            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                            CircularProgressIndicator(
                                modifier = Modifier
                                    .width(60.dp)
                                    .height(60.dp)
                            )
//                            }
                        }
                    }
                }
            }
        }
    }

    /**  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **/

    @Composable
    @ExperimentalMaterialApi
    fun CardContactBooks(
        contacts: MutableState<Contacts>,
        loading : MutableState<Boolean>
    ){
        Row(
            modifier = Modifier
                .fillMaxSize(1f)
                .padding(top = verPad, bottom = verPad, start = horPad, end = horPad)
        ) {
            Card(
                onClick = { /*TODO*/ },
                modifier = Modifier
                    .fillMaxSize(1f),
                backgroundColor = Color(0xffFFffFF),
                shape = RoundedCornerShape(size = cardRadius)
            ) {
                Box(modifier = Modifier.fillMaxSize(1f)) {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth(1f)
                            .padding(all = allPad)
                    ) {
                        Row(
                            modifier = Modifier
                                .fillMaxWidth(1f)
                                .padding(bottom = 4.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = stringResource(id = R.string.statisticCardContactBook),
                                style = sTxt
                            )
                            Spacer(modifier = Modifier.width(10.dp))
                            Image(
                                painter = painterResource(id = R.drawable.ic_statistic_card_info),
                                contentDescription = "Info icon"
                            )
                            Spacer(modifier = Modifier.weight(1f))
                            Image(
                                modifier = Modifier
                                    .height(32.dp)
                                    .width(32.dp),
                                painter = painterResource(id = R.drawable.ic_statistic_card_referral),
                                contentDescription = "Info icon"
                            )
                        }
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(top = 10.dp, bottom = 10.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Column() {
                                Text(
                                    text = "${contacts.value.cardBookInv}",
                                    style = sTxtBalW700
                                )
                                Text(
                                    text = stringResource(id = R.string.statisticCardContactInvited),
                                    style = sTxtBookBlcW400
                                )
                            }
                            Column(
                                modifier = Modifier
                                    .fillMaxHeight(1f)
                                    .background(Color.Blue)
                            ) {
                                Box(
                                    modifier = Modifier
                                        .fillMaxHeight(1f)
                                        .background(Color.Transparent),
                                    contentAlignment = Alignment.TopCenter
                                ) {
                                    Text(
                                        text = "/",
                                        style = sTxtGryW700
                                    )
                                }
                            }
                            Column() {
                                Text(
                                    text = "${contacts.value.cardBookAll}",
                                    style = sTxtBalW700
                                )
                                Text(
                                    text = stringResource(id = R.string.statisticCardContactsAll),
                                    style = sTxtBookGryW400
                                )
                            }
                        }
                        Row {
                            LinearProgressIndicator(
                                modifier = Modifier
                                    .fillMaxWidth(1f)
                                    .height(10.dp)
                                    .padding(top = 19.dp, bottom = 14.dp),
                                progress = contacts.value.cardBookPer / 100,
                                backgroundColor = Color(0xffF0F0F6),
                                color = Color(0xffFADF33)
                            )
                        }
                        Row(
                            modifier = Modifier.padding(top = 10.dp, bottom = 10.dp),
                        ) {
                            Divider(
                                modifier = Modifier
                                    .height(1.dp)
                                    .fillMaxWidth(),
                                color = Color(0xffF0F0F6)
                            )
                        }
                        Row(
                            modifier = Modifier.fillMaxSize(1f),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = stringResource(id = R.string.statisticCardInviteThis),
                                style = sTxtAblGryW600
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = "${contacts.value.cardConversionRate}%",
                                style = sTxtAblBalW600
                            )
                        }
                    }
                    if(loading.value) {
                        Box(
                            modifier = Modifier
//                                .fillMaxHeight(1f)
//                                .fillMaxWidth(1f)
                                .width(120.dp)
                                .height(120.dp)
                                .background(
                                    color = Color(0x33181925),
                                    shape = RoundedCornerShape(cardRadius)
                                ),
                            contentAlignment = Alignment.Center
//                            verticalAlignment = Alignment.CenterVertically
                        ) {
//                            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                            CircularProgressIndicator(
                                modifier = Modifier
                                    .width(60.dp)
                                    .height(60.dp)
                            )
//                            }
                        }
                    }
                }
            }
        }
    }

    /**  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **/

    @Composable
    @ExperimentalMaterialApi
    fun CardReward(
        rewRewards: MutableState<Rewards>,
        rewLoading: MutableState<Boolean>
    ){
        Row(
            modifier = Modifier
                .fillMaxSize(1f)
                .padding(top = verPad, bottom = verPad, start = horPad, end = horPad)
        ) {
            Card(
                onClick = { /*TODO*/ },
                modifier = Modifier
                    .fillMaxSize(1f),
                backgroundColor = Color(0xffFFffFF),
                shape = RoundedCornerShape(size = cardRadius)
            ) {
                Box(modifier = Modifier.fillMaxSize(1f)){
                    Column(
                        modifier = Modifier
                            .fillMaxWidth(1f)
                            .padding(all = allPad)
                    ) {
                        Row(
                            modifier = Modifier
                                .fillMaxWidth(1f)
                                .padding(bottom = 4.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = stringResource(id = R.string.statisticCardBalances),
                                style = sTxt
                            )
                            Spacer(modifier = Modifier.width(10.dp))
                            Image(
                                painter = painterResource(id = R.drawable.ic_statistic_card_info),
                                contentDescription = "Info icon"
                            )
                            Spacer(modifier = Modifier.weight(1f))
                            Image(
                                modifier = Modifier
                                    .height(32.dp)
                                    .width(32.dp),
                                painter = painterResource(id = R.drawable.ic_statistic_card_reward),
                                contentDescription = "Info icon"
                            )
                        }

                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(top = 10.dp, bottom = 10.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                modifier = Modifier.padding(end = 8.dp),
                                text = "${rewRewards.value.cardRewardsAll}",
                                style = sTxtBalW700
                            )
                            Row(
                                modifier = Modifier.fillMaxHeight(),
                                verticalAlignment = Alignment.Bottom
                            ) {
                                Text(
                                    modifier = Modifier.padding(end = 8.dp),
                                    text = stringResource(id = R.string.statisticCardRewardDivits).uppercase(),
                                    style = sTxtNameW700
                                )
                            }
                            Text(
                                modifier = Modifier.padding(end = 8.dp),
                                text = "~ $${rewRewards.value.cardRewardsUSD}",
                                style = sTxtIncGryW400
                            )
                        }
                        Row(
                            modifier = Modifier
                                .height(21.dp)
                                .fillMaxWidth(1f),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = stringResource(id = R.string.statisticCardIncome),
                                style = sTxtIncBlcW400
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = "${rewRewards.value.cardRewardsTotal}",
                                style = sTxtIncGryW400
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = stringResource(id = R.string.statisticCardRewardsHours),
                                style = sTxtIncGryW400
                            )
                            Image(
                                modifier = Modifier
                                    .height(10.dp)
                                    .width(10.dp),
                                painter = painterResource(id = R.drawable.ic_statistic_card_arrow_up),
                                contentDescription = "Info icon down"
                            )
                        }
                        Row(
                            modifier = Modifier
                                .height(21.dp)
                                .fillMaxWidth(1f),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = stringResource(id = R.string.statisticCardSpend),
                                style = sTxtIncGryW400
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = "${rewRewards.value.cardRewardsSpendVal}",
                                style = sTxtIncBlcW400
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = when {
                                    rewRewards.value.cardRewardsSpendPer > 0 -> "(+${rewRewards.value.cardRewardsSpendPer}%)"
                                    rewRewards.value.cardRewardsSpendPer < 0 -> "(${rewRewards.value.cardRewardsSpendPer}%)"
                                    else -> "(0%)"
                                },
                                style = when {
                                    rewRewards.value.cardRewardsSpendPer > 0 -> sTxtIncGrnW400
                                    rewRewards.value.cardRewardsSpendPer < 0 -> sTxtIncRedW400
                                    else -> sTxtIncGryW400
                                }
                            )
                            Image(
                                modifier = Modifier
                                    .height(10.dp)
                                    .width(10.dp),
                                painter = when {
                                    rewRewards.value.cardRewardsSpendPer > 0 -> painterResource(id = R.drawable.ic_statistic_card_arrow_up)
                                    rewRewards.value.cardRewardsSpendPer < 0 -> painterResource(id = R.drawable.ic_statistic_card_arrow_down)
                                    else -> painterResource(id = R.drawable.ic_statistic_card_arrow_null)
                                },
                                contentDescription = "Info icon up"
                            )
                        }
                        Row(
                            modifier = Modifier.padding(top = 10.dp, bottom = 10.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Divider(
                                modifier = Modifier
                                    .height(1.dp)
                                    .fillMaxWidth(),
                                color = Color(0xffF0F0F6)
                            )
                        }
                        Row(
                            modifier = Modifier.fillMaxSize(1f)
                        ) {
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = stringResource(id = R.string.statisticCardRewardsEarned),
                                style = sTxtAblGryW600
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = "${rewRewards.value.cardRewardsEarnedDiv}",
                                style = sTxtAblBalW600
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = stringResource(id = R.string.statisticCardRewardDivits),
                                style = sTxtAblBalW600
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = when {
                                    rewRewards.value.cardRewardsEarnedPer > 0 -> "(+${rewRewards.value.cardRewardsEarnedPer}%)"
                                    rewRewards.value.cardRewardsEarnedPer < 0 -> "(${rewRewards.value.cardRewardsEarnedPer}%)"
                                    else -> "(0%)"
                                },
                                style = when {
                                    rewRewards.value.cardRewardsEarnedPer > 0 -> sTxtIncGrnW400
                                    rewRewards.value.cardRewardsEarnedPer < 0 -> sTxtIncRedW400
                                    else -> sTxtAblBalW600
                                }
                            )
                            Image(
                                modifier = Modifier
                                    .height(10.dp)
                                    .width(10.dp),
                                painter = when {
                                    rewRewards.value.cardRewardsEarnedPer > 0 -> painterResource(id = R.drawable.ic_statistic_card_arrow_up)
                                    rewRewards.value.cardRewardsEarnedPer < 0 -> painterResource(id = R.drawable.ic_statistic_card_arrow_down)
                                    else -> painterResource(id = R.drawable.ic_statistic_card_arrow_null)
                                },
                                contentDescription = "Info icon down"
                            )
                        }
                    }
                    if(rewLoading.value) {
                        Box(
                            modifier = Modifier
    //                                .fillMaxHeight(1f)
    //                                .fillMaxWidth(1f)
                                .width(120.dp)
                                .height(120.dp)
                                .background(
                                    color = Color(0x33181925),
                                    shape = RoundedCornerShape(cardRadius)
                                ),
                            contentAlignment = Alignment.Center
    //                            verticalAlignment = Alignment.CenterVertically
                        ) {
    //                            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                            CircularProgressIndicator(
                                modifier = Modifier
                                    .width(60.dp)
                                    .height(60.dp)
                            )
    //                            }
                        }
                    }
                }
            }
        }
    }

    /**  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **/

    @Composable
    @ExperimentalMaterialApi
    fun CardCashBacks(
        cashback: MutableState<CashBack>,
        loading : MutableState<Boolean>
    ){
        Row(
            modifier = Modifier
                .fillMaxSize(1f)
                .padding(top = verPad, bottom = verPad, start = horPad, end = horPad)
        ) {
            Card(
                onClick = { /*TODO*/ },
                modifier = Modifier
//                    .height(cardHeight)
                    .fillMaxSize(1f),
                backgroundColor = Color(0xffFFffFF),
                shape = RoundedCornerShape(size = cardRadius)
            ) {
                Box() {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth(1f)
                            .padding(vertical = allPad)
                    ) {
                        Row(
                            modifier = Modifier
                                .fillMaxWidth(1f)
                                .padding(
                                    bottom = 4.dp,
                                    start = allPad, end = allPad
                                ),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = stringResource(id = R.string.statisticCardCashbackLabel),
                                style = sTxt
                            )
                            Spacer(modifier = Modifier.width(10.dp))
                            Image(
                                painter = painterResource(id = R.drawable.ic_statistic_card_info),
                                contentDescription = "Info icon"
                            )
                            Spacer(modifier = Modifier.weight(1f))
                            Image(
                                modifier = Modifier
                                    .height(32.dp)
                                    .width(32.dp),
                                painter = painterResource(id = R.drawable.ic_statistic_card_cash),
                                contentDescription = "Info icon"
                            )
                        }
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(
                                    top = 10.dp, bottom = 0.dp,
                                    start = allPad, end = allPad
                                ),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                modifier = Modifier.padding(end = 8.dp),
                                text = "$ ",
                                style = sTxtBalW400
                            )
                            Text(
                                text = "${cashback.value.cardManyAll}",
                                style = sTxtBalW700
                            )
                        }
                        Row(
//                        modifier = Modifier
//                            .fillMaxWidth(1f),
//                        verticalAlignment = Alignment.CenterVertically
                        ) {
                            AndroidView(
                                factory = { context ->
                                    LineChart(context).apply {
                                        val le = cashback.value.cardLineChart.value
                                        val lds = LineDataSet(le, "Cashbacks...")
                                        lds.fillDrawable =
                                            context.getDrawable(R.drawable.statistic_linear_chart_gradient_purple)
                                        val dt = LineData(lds)
                                        data = dt
                                        setupLineChart(this,
                                            android.graphics.Color.parseColor("#C179EA"))
                                        invalidate()
                                    }
                                },
                                modifier = Modifier
                                    .height(lineHeight)
                                    .fillMaxWidth(1f)
                                    .padding(top = 0.dp, bottom = 0.dp)
                            )
                        }
                        Row(
                            modifier = Modifier.padding(top = 0.dp, bottom = 10.dp,
                                start = allPad, end = allPad),
                        ) {
                            Divider(
                                modifier = Modifier
                                    .height(1.dp)
                                    .fillMaxWidth(),
                                color = Color(0xffF0F0F6)
                            )
                        }
                        Row(
                            modifier = Modifier
                                .fillMaxSize(1f)
                                .padding(horizontal = allPad),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = stringResource(id = R.string.statisticCardCashMonts),
                                style = sTxtAblGryW600
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = "$ ${cashback.value.cardManyMonth}",
                                style = sTxtAblBalW600
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = when {
                                    cashback.value.cardPercent > 0 -> "(+${cashback.value.cardPercent}%)"
                                    cashback.value.cardPercent < 0 -> "(${cashback.value.cardPercent}%)"
                                    else -> "(0%)"
                                },
                                style = when {
                                    cashback.value.cardPercent > 0 -> sTxtIncGrnW400
                                    cashback.value.cardPercent < 0 -> sTxtIncRedW400
                                    else -> sTxtAblBalW600
                                }
                            )
                            Image(
                                modifier = Modifier
                                    .height(10.dp)
                                    .width(10.dp),
                                painter = when {
                                    cashback.value.cardPercent > 0 -> painterResource(id = R.drawable.ic_statistic_card_arrow_up)
                                    cashback.value.cardPercent < 0 -> painterResource(id = R.drawable.ic_statistic_card_arrow_down)
                                    else -> painterResource(id = R.drawable.ic_statistic_card_arrow_null)
                                },
                                contentDescription = "Info icon down"
                            )
                        }
                        if(loading.value) {
                            Box(
                                modifier = Modifier
//                                .fillMaxHeight(1f)
//                                .fillMaxWidth(1f)
                                    .width(120.dp)
                                    .height(120.dp)
                                    .background(
                                        color = Color(0x33181925),
                                        shape = RoundedCornerShape(cardRadius)
                                    ),
                                contentAlignment = Alignment.Center
//                            verticalAlignment = Alignment.CenterVertically
                            ) {
//                            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                                CircularProgressIndicator(
                                    modifier = Modifier
                                        .width(60.dp)
                                        .height(60.dp)
                                )
//                            }
                            }
                        }
                    }
                }
            }
        }
    }

    /**  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **/

    @Composable
    @ExperimentalMaterialApi
    fun CardRentPayments(
        cashback: MutableState<RentPay>,
        loading : MutableState<Boolean>
//        cardManyAll  : Float,
//        cardLineChart: LineChart,
//        cardManyMonth: Float,
//        cardPercent  : Int
    ){
        var lineChart: LineChart? = null
        Row(
            modifier = Modifier
                .fillMaxSize(1f)
                .padding(top = verPad, bottom = verPad, start = horPad, end = horPad)
        ) {
            Card(
                onClick = { /*TODO*/ },
                modifier = Modifier
//                    .height(cardHeight)
                    .fillMaxSize(1f),
                backgroundColor = Color(0xffFFffFF),
                shape = RoundedCornerShape(size = cardRadius)
            ) {
                Box(modifier = Modifier.fillMaxSize(1f)) {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth(1f)
                            .padding(vertical = allPad)
                    ) {
                        Row(
                            modifier = Modifier
                                .fillMaxWidth(1f)
                                .padding(
                                    bottom = 4.dp,
                                    start = allPad, end = allPad
                                ),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = stringResource(id = R.string.statisticCardRentpayments),
                                style = sTxt
                            )
                            Spacer(modifier = Modifier.width(10.dp))
                            Image(
                                painter = painterResource(id = R.drawable.ic_statistic_card_info),
                                contentDescription = "Info icon"
                            )
                            Spacer(modifier = Modifier.weight(1f))
                            Image(
                                modifier = Modifier
                                    .height(32.dp)
                                    .width(32.dp),
                                painter = painterResource(id = R.drawable.ic_statistic_card_renpayment),
                                contentDescription = "Info icon"
                            )
                        }
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(
                                    top = 10.dp, bottom = 0.dp,
                                    start = allPad, end = allPad
                                ),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                modifier = Modifier.padding(end = 8.dp),
                                text = "$ ",
                                style = sTxtBalW400
                            )
                            Text(
                                text = "${cashback.value.cardManyAll}",
                                style = sTxtBalW700
                            )
                        }
                        Row() {
                            AndroidView(
                                factory = { context ->
                                    LineChart(context).apply {
                                        val le = cashback.value.cardLineChart.value
                                        val lds = LineDataSet(le, "Rentpayments")
                                        lds.fillDrawable = context
                                            .getDrawable(R.drawable.statistic_linear_chart_gradient_blue)
                                        val dt = LineData(lds)
                                        data = dt
                                        setupLineChart(
                                            this,
                                            android.graphics.Color.parseColor("#00B4FF")
                                        )
                                    }
                                },
                                modifier = Modifier
                                    .height(lineHeight)
                                    .fillMaxWidth(1f)
                                    .padding(top = 6.dp, bottom = 10.dp)
                            )

                        }
                        Row(
                            modifier = Modifier.padding(
                                top = 0.dp, bottom = 10.dp,
                                start = allPad, end = allPad
                            ),
                        ) {
                            Divider(
                                modifier = Modifier
                                    .height(1.dp)
                                    .fillMaxWidth(),
                                color = Color(0xffF0F0F6)
                            )
                        }
                        Row(
                            modifier = Modifier
                                .fillMaxSize(1f)
                                .padding(horizontal = allPad),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = stringResource(id = R.string.statisticCardRentThisMonth),
                                style = sTxtAblGryW600
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = "$ ${cashback.value.cardManyMonth}",
                                style = sTxtAblBalW600
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = when {
                                    cashback.value.cardPercent > 0 -> "(+${cashback.value.cardPercent}%)"
                                    cashback.value.cardPercent < 0 -> "(${cashback.value.cardPercent}%)"
                                    else -> "(0%)"
                                },
                                style = when {
                                    cashback.value.cardPercent > 0 -> sTxtIncGrnW400
                                    cashback.value.cardPercent < 0 -> sTxtIncRedW400
                                    else -> sTxtAblBalW600
                                }
                            )
                            Image(
                                modifier = Modifier
                                    .height(10.dp)
                                    .width(10.dp),
                                painter = when {
                                    cashback.value.cardPercent > 0 -> painterResource(id = R.drawable.ic_statistic_card_arrow_up)
                                    cashback.value.cardPercent < 0 -> painterResource(id = R.drawable.ic_statistic_card_arrow_down)
                                    else -> painterResource(id = R.drawable.ic_statistic_card_arrow_null)
                                },
                                contentDescription = "Info icon down"
                            )
                        }
                    }
                    if(loading.value) {
                        Box(
                            modifier = Modifier
//                                .fillMaxHeight(1f)
//                                .fillMaxWidth(1f)
                                .width(120.dp)
                                .height(120.dp)
                                .background(
                                    color = Color(0x33181925),
                                    shape = RoundedCornerShape(cardRadius)
                                ),
                            contentAlignment = Alignment.Center
//                            verticalAlignment = Alignment.CenterVertically
                        ) {
//                            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                            CircularProgressIndicator(
                                modifier = Modifier
                                    .width(60.dp)
                                    .height(60.dp)
                            )
//                            }
                        }
                    }
                }
            }
        }
    }

    /**  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **/

    @Composable
    @ExperimentalMaterialApi
    fun CardTransfers(
        barTransfers: MutableState<Transfers>,
        barLoading  : MutableState<Boolean>
//        cardTransfers: Int,  // count referrals all period
//        cardBarChart : BarChart,
//        cardTransMnth: Int,  // count referrals this month
//        cardGrOrFall : Int   //
    ){
        Row(
            modifier = Modifier
                .fillMaxSize(1f)
                .padding(top = verPad, bottom = verPad, start = horPad, end = horPad)
        ) {
            Card(
                onClick = { /*TODO*/ },
                modifier = Modifier
//                    .height(cardHeight)
                    .fillMaxSize(1f),
                backgroundColor = Color(0xffFFffFF),
                shape = RoundedCornerShape(size = cardRadius)
            ) {
                Box(modifier = Modifier.fillMaxSize(1f)
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth(1f)
                            .padding(vertical = allPad)
                    ) {
                        Row(
                            modifier = Modifier
                                .fillMaxWidth(1f)
                                .padding(
                                    start = allPad,
                                    end = allPad,
                                    bottom = 4.dp
                                ),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = stringResource(id = R.string.statisticCardReferrals),
                                style = sTxt
                            )
                            Spacer(modifier = Modifier.width(10.dp))
                            Image(
                                painter = painterResource(id = R.drawable.ic_statistic_card_info),
                                contentDescription = "Info icon"
                            )
                            Spacer(modifier = Modifier.weight(1f))
                            Image(
                                modifier = Modifier
                                    .height(32.dp)
                                    .width(32.dp),
                                painter = painterResource(id = R.drawable.ic_statistic_card_referral),
                                contentDescription = "Info icon"
                            )
                        }
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(
                                    top = 10.dp, bottom = 10.dp,
                                    start = allPad, end = allPad
                                ),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = "${barTransfers.value.cardTransfers}",
                                style = sTxtBalW700
                            )
                        }
                        Row(

                        ) {
                            AndroidView(
                                factory = { context ->
                                    BarChart(context).apply {
                                        val be = barTransfers.value.cardBarChart.value
                                        val bds = BarDataSet(be, "Transfers")
                                        data = BarData(bds)
                                        setupBarChart(
                                            this,
                                            android.graphics.Color.parseColor("#C27AEB")
                                        )
                                    }
                                },
                                modifier = Modifier
                                    .height(lineHeight)
                                    .fillMaxWidth(1f)
                                    .padding(top = 6.dp, bottom = 10.dp)
                            )
                        }
                        Row(
                            modifier = Modifier.padding(
                                top = 10.dp, bottom = 10.dp,
                                start = allPad, end = allPad
                            ),
                        ) {
                            Divider(
                                modifier = Modifier
                                    .height(1.dp)
                                    .fillMaxWidth(),
                                color = Color(0xffF0F0F6)
                            )
                        }
                        Row(
                            modifier = Modifier
                                .fillMaxSize(1f)
                                .padding(horizontal = allPad),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = stringResource(id = R.string.statisticCardInviteThis),
                                style = sTxtAblGryW600
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = "${barTransfers.value.cardTransMnth}",
                                style = sTxtAblBalW600
                            )
                            Text(
                                modifier = Modifier.padding(end = 4.dp),
                                text = if (barTransfers.value.cardGrOrFall > 0) {
                                    "(+${barTransfers.value.cardGrOrFall}%)"
                                } else if (barTransfers.value.cardGrOrFall < 0) {
                                    "(${barTransfers.value.cardGrOrFall}%)"
                                } else {
                                    "(0%)"
                                },
                                style = when {
                                    barTransfers.value.cardGrOrFall > 0 -> sTxtIncGrnW400
                                    barTransfers.value.cardGrOrFall < 0 -> sTxtIncRedW400
                                    else -> sTxtAblBalW600
                                }
                            )
                            Image(
                                modifier = Modifier
                                    .height(10.dp)
                                    .width(10.dp),
                                painter = when {
                                    barTransfers.value.cardGrOrFall > 0 -> painterResource(id = R.drawable.ic_statistic_card_arrow_up)
                                    barTransfers.value.cardGrOrFall < 0 -> painterResource(id = R.drawable.ic_statistic_card_arrow_down)
                                    else -> painterResource(id = R.drawable.ic_statistic_card_arrow_null)
                                },
                                contentDescription = "Info icon down"
                            )
                        }
                    }
                    if(barLoading.value) {
                        Box(
                            modifier = Modifier
//                                .fillMaxHeight(1f)
//                                .fillMaxWidth(1f)
                                .width(120.dp)
                                .height(120.dp)
                                .background(
                                    color = Color(0x33181925),
                                    shape = RoundedCornerShape(cardRadius)
                                ),
                            contentAlignment = Alignment.Center
//                            verticalAlignment = Alignment.CenterVertically
                        ) {
//                            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                            CircularProgressIndicator(
                                modifier = Modifier
                                    .width(60.dp)
                                    .height(60.dp)
                            )
//                            }
                        }
                    }
                }
            }
        }
    }

    /**  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **/

    @SuppressLint("UnrememberedMutableState")
    @Composable
    @ExperimentalMaterialApi
    fun CardOverview(
        callSelect : PeriodValue,
        barOverview: MutableState<Overview>,
        barOverLoad: MutableState<Boolean>
    ){
        val menuShow = mutableStateOf(false)
        val isActiveMenu = mutableStateOf(0)
        val menuItems = EPeriod.getPeriods()
        Row(
            modifier = Modifier
                .fillMaxSize(1f)
                .padding(top = verPad, bottom = 30.dp, start = horPad, end = horPad)
        ) {
            Surface(
                onClick = {  },
                modifier = Modifier
                    .fillMaxSize(1f),
                color = Color.Transparent,
            ) {
                Box(
                    modifier = Modifier.fillMaxSize(1f),
                    contentAlignment = Alignment.Center
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth(1f)
                            .padding(vertical = allPad)
                    ) {
                        Row(
                            modifier = Modifier
                                .fillMaxWidth(1f)
                                .padding(start = 20.dp, bottom = 4.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text  = stringResource(id = R.string.statisticCardReferrals),
                                style = sTxt
                            )
                            Spacer(modifier = Modifier.weight(1f))

                            Surface(
                                onClick = { menuShow.value = !menuShow.value },
                                color = if(menuShow.value) Color(0xffE9E9F2) else Color(0xffFFffFF),
                                shape = RoundedCornerShape(size = 8.dp),
                                elevation = 2.dp
                            ) {
                                Row(
                                    modifier = Modifier.padding(all = 6.dp),
                                    verticalAlignment = Alignment.CenterVertically
                                ) {
                                    Text(
                                        modifier = Modifier.padding(end = 8.dp),
                                        text     = "${stringResource(id = R.string.statisticCardSelect)} ${menuItems[isActiveMenu.value].lowercase()}",
                                        style    = sTxtIncGryW400
                                    )
                                    Image(
                                        modifier = Modifier.size(12.dp), //.width(10.dp),
                                        painter = painterResource(id = R.drawable.ic_arrow_down),
                                        contentDescription = "Info icon",
                                        colorFilter = ColorFilter.tint(color = Color(0xffB0B7C3))
                                    )
                                }
                            }
                        }
                        Box(
                            modifier = Modifier.fillMaxSize(1f)
                        ) {
                            AndroidView(
                                factory = { context ->
//                                    cardBarChart
                                    BarChart(context).apply {

                                        val bea = barOverview.value.overBarChart.value[setupStatisticCharts.EStatistic.EARNING]
                                        val brw = barOverview.value.overBarChart.value[setupStatisticCharts.EStatistic.REWARD]
                                        val bcb = barOverview.value.overBarChart.value[setupStatisticCharts.EStatistic.CASHBACK]
                                        val bin = barOverview.value.overBarChart.value[setupStatisticCharts.EStatistic.INVITED]

                                        val bds_ea = BarDataSet(bea, "Global earnings")
                                        bds_ea.color = android.graphics.Color.parseColor("#FF4A7A")

                                        val bds_rw = BarDataSet(brw, "Rewards")
                                        bds_rw.color = android.graphics.Color.parseColor("#E9E9F2")
                                        val bds_cb = BarDataSet(bcb, "Cashbacks")
                                        bds_cb.color = android.graphics.Color.parseColor("#C27AEB")
                                        val bds_in = BarDataSet(bin, "InvitedUsers")
                                        bds_in.color = android.graphics.Color.parseColor("#FFDD00")

                                        data = BarData(bds_ea, bds_rw, bds_cb, bds_in)

                                        setupBarChartOverview(this, 0)
                                    }
                                },
                                modifier = Modifier
                                    .height(340.dp)
                                    .fillMaxWidth(1f)
                                    .padding(top = 6.dp, bottom = 10.dp)
                            )
                            if( menuShow.value ) {
                                Box(
                                    modifier = Modifier
                                        .fillMaxSize(1f)
                                        .padding(top = 4.dp, end = 4.dp),
//                                .background(color = Color(0x218523f5)),
                                    contentAlignment = Alignment.TopEnd
                                ) {
                                    Column(
                                        modifier = Modifier
                                            .background(
                                                color = Color(0xffFFffFF),
                                                shape = RoundedCornerShape(size = 4.dp))
                                    ) {
                                        menuItems.forEachIndexed { index, item ->
                                            Surface(
                                                onClick = {
                                                    menuShow.value = !menuShow.value
                                                    isActiveMenu.value = index
                                                    callSelect.period ( EPeriod.byCount(index) )
                                                }
                                            ) {
                                                Row(
                                                    modifier = Modifier
                                                        .padding(
                                                            vertical = 4.dp,
                                                            horizontal = 12.dp
                                                        )
                                                        .background(color = Color.Transparent),
                                                    verticalAlignment = Alignment.CenterVertically,
                                                ){
                                                    Log.i("Update index...", "${index}")
                                                    if( isActiveMenu.value == index ){
                                                        Image(
                                                            modifier = Modifier.size(10.dp),
                                                            painter = painterResource(id = R.drawable.ic_checkbox_selected),
                                                            contentDescription = "Indicator enabled")
                                                    }else{
                                                        Image(
                                                            modifier = Modifier.size(10.dp),
                                                            painter = painterResource(id = R.drawable.ic_checkbox_emty),
                                                            contentDescription = "Indicator disabled")
                                                    }
                                                    Text(
                                                        modifier = Modifier
                                                            .padding(start = 10.dp, end = 10.dp),
                                                        text  = item,
                                                        style = sTxtIncGryW400
                                                    )
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if(barOverLoad.value) {
                        Box(
                            modifier = Modifier
//                                .fillMaxHeight(1f)
//                                .fillMaxWidth(1f)
                                .width(120.dp)
                                .height(120.dp)
                                .background(
                                    color = Color(0x33181925),
                                    shape = RoundedCornerShape(cardRadius)
                                ),
                            contentAlignment = Alignment.Center
//                            verticalAlignment = Alignment.CenterVertically
                        ) {
//                            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                                CircularProgressIndicator(
                                    modifier = Modifier
                                        .width(60.dp)
                                        .height(60.dp)
                                )
//                            }
                        }
                    }
                }
            }
        }
    }




}
