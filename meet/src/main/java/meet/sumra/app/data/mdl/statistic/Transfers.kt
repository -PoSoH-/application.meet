package meet.sumra.app.data.mdl.statistic

import androidx.compose.runtime.MutableState
import com.github.mikephil.charting.data.BarEntry
import meet.sumra.app.data.configs.setupStatisticCharts.fetchBarData

data class Transfers(
    val cardTransfers: Float = 0F,
    val cardBarChart : MutableState<MutableList<BarEntry>> = fetchBarData(),
    val cardTransMnth: Float = 0F,
    val cardGrOrFall : Int = 0,
)
