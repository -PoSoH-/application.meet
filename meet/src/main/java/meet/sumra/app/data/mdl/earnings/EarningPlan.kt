package meet.sumra.app.data.mdl.earnings

data class EarningPlan(
    var isActive: Boolean = false,
    val planName: String,  // Free
    val planCount: Float = 0.0F, // 0.0

)
