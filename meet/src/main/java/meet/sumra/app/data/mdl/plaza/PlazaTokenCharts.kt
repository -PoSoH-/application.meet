package meet.sumra.app.data.mdl.plaza

import com.github.tehras.charts.bar.BarChartData
import com.github.tehras.charts.line.LineChartData
import com.github.tehras.charts.piechart.PieChartData
import java.util.*

data class PlazaTokenCharts(
    val id: String = UUID.randomUUID().toString(),
    val cB: BarChartData,
    val cL: LineChartData,
    val cP: PieChartData
)
