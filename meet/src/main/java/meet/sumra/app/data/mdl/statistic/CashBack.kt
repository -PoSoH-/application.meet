package meet.sumra.app.data.mdl.statistic

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import meet.sumra.app.data.configs.setupStatisticCharts

data class CashBack(
    val cardManyAll: Float = 0.0F,    // 2154.24F,
    val cardLineChart: MutableState<MutableList<Entry>> = setupStatisticCharts.fetchLineData(),
    val cardManyMonth: Float = 0.0F,  // 654.54F,
    val cardPercent: Int = 0          // 9
)
