package meet.sumra.app.data.mdl

import sdk.net.meet.api.contacts.response.FetchContact
import java.util.*

data class DtMeetCalendar (
    val dMeetId: String = UUID.randomUUID().toString(),
    val dType: String = "calendar",
    val dTitle: String,
    val dNote: String,
//    val dTypeMeet: EMeetType = EMeetType.NIT,
    val dStartDate: String,
    val dFinalDate: String,
    val dOwnerId: String,
    val dPassword: String? = null,
    val dIsLobby: Boolean = false,
    val dCreated_at: String,
    val dUpdated_at: String,
    val dParticipants: MutableList<FetchContact>,
    val dLiveStream: DtMeetStream
)

data class DtMeetStream (
    val dStreamKey :String? = null,
    val dStreamLink: String? = null
)
