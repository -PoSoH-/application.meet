package meet.sumra.app.data.mdl.plaza

import androidx.compose.ui.graphics.Color
import com.github.tehras.charts.bar.BarChartData
import com.github.tehras.charts.bar.renderer.label.SimpleValueDrawer
import com.github.tehras.charts.line.LineChartData
import com.github.tehras.charts.line.renderer.point.HollowCircularPointDrawer
import com.github.tehras.charts.line.renderer.point.PointDrawer
import com.github.tehras.charts.piechart.PieChartData
import meet.sumra.app.views.compose.ui_theme.ThemeSetup

data class PageTokenChart (
    val lists: MutableList<LineChartDataModel> = mutableListOf(LineChartDataModel()),
    val pies: MutableList<PieChartDataModel> = mutableListOf(
        PieChartDataModel(
            mutableListOf(50f, 50f),
            mutableListOf(ThemeSetup.getTheme().cPieChartColors[0], ThemeSetup.getTheme().cPieChartColors[1])),
        PieChartDataModel(
            mutableListOf(50f, 50f),
            mutableListOf(ThemeSetup.getTheme().cPieChartColors[0], ThemeSetup.getTheme().cPieChartColors[2]))),
    val bars: MutableList<BarChartDataModel> = mutableListOf(BarChartDataModel()),
)

data class LineChartDataModel(
    var lineChartData: LineChartData = LineChartData(
            points = listOf(
                LineChartData.Point(0f, ""),
                LineChartData.Point(0f, ""),
                LineChartData.Point(0f, ""),
                LineChartData.Point(0f, ""),
                LineChartData.Point(0f, ""),
                LineChartData.Point(0f, "")
            )),
    var horizontalOffset: Float  = 5f,
    var pointDrawerType: String = "HOLLOW",  /** FILLED or NOTHING **/
    val pointDrawer: PointDrawer = HollowCircularPointDrawer()
)

data class PieChartDataModel(val values: MutableList<Float> = mutableListOf(50f, 50f),
                             val colors: MutableList<Color>){
    var pieChartData: PieChartData = PieChartData(
        slices = listOf(
            PieChartData.Slice(
                value = values[0],
                color = colors[0]
            ),
            PieChartData.Slice(
                value = values[1],
                color = colors[1]
            )
        ))
    var labelLocation: SimpleValueDrawer.DrawLocation = SimpleValueDrawer.DrawLocation.XAxis
}

data class BarChartDataModel(
    var barChartData: BarChartData = BarChartData(
        bars = listOf(
            BarChartData.Bar(label = "", value = 0f, color = Color.White),
            BarChartData.Bar(label = "", value = 0f, color = Color.White),
            BarChartData.Bar(label = "", value = 0f, color = Color.White),
            BarChartData.Bar(label = "", value = 0f, color = Color.White),
            BarChartData.Bar(label = "", value = 0f, color = Color.White),
            BarChartData.Bar(label = "", value = 0f, color = Color.White))),
    var labelLocation: SimpleValueDrawer.DrawLocation = SimpleValueDrawer.DrawLocation.XAxis
)

private var colors = mutableListOf (
    Color(0XFFFF5722),
    Color(0XFFF44336),
    Color(0XFFE91E63),
    Color(0XFF9C27B0),
    Color(0XFF673AB7),
    Color(0XFF3F51B5),
    Color(0XFF03A9F4),
    Color(0XFF009688),
    Color(0XFFCDDC39),
    Color(0XFFFFC107),
    Color(0XFF795548),
    Color(0XFF9E9E9E),
    Color(0XFF607D8B)
)

enum class Period(){
    START,
    FINAL
}

data class SetupBar(
    val statisticPeriod: MutableMap<Period,Statistic>
)

data class Statistic(
    val year: String = "0",
    val value: String = "0" ,
    val percent: String = "0"
)


