package meet.sumra.app.data.mdl

data class SessionData(
    val id: String,
    val userId: String,
    val roomId: String,
    var status: Int? = null,
    var startTime: String? = null,
    var finishTime: String? = null,
    var deletedAt: String? = null
)


