package meet.sumra.app.data.mdl

import com.google.gson.annotations.SerializedName
import sdk.net.meet.api.contacts.response.Link

data class ContactDataPagination(
    var currentPage: Int,      // @SerializedName("current_page")
    val firstPageUrl: String?, // @SerializedName("first_page_url")
    val from: Int,             // @SerializedName("from")
    val lastPage: Int,         // @SerializedName("last_page")
    val lastPageUrl: String?,  // @SerializedName("last_page_url")
    val nextPageUrl: String?,  // @SerializedName("next_page_url")
    val path: String,          // @SerializedName("path")
    val perPage: Int,          // @SerializedName("per_page")
    val prevPageUrl: String?,  // @SerializedName("prev_page_url")
    val to: Int,               // @SerializedName("to")
    val total: Int,            // @SerializedName("total")
    val letters: List<String>, // @SerializedName("letters")
    val links: List<Link>,     // @SerializedName("links")
) {
    data class Link(
        val active: Boolean, // @SerializedName("active")
        val label: String,   // @SerializedName("label")
        val url: String?     // @SerializedName("url")
    )
}
