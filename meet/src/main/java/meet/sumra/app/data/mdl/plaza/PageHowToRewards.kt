package meet.sumra.app.data.mdl.plaza

data class PageHowToRewards(
    val howToRewards: MutableList<PlazaHowToReward> = mutableListOf()
)
