package meet.sumra.app.data.pojo.res

import meet.sumra.app.R
import meet.sumra.app.vm.vm_home.StateHome

data class DrawerItem (
    val mMeetings: ItemMenu = ItemMenu( itemName = R.string.drawer_menu_meeting,
        itemIconOf = R.drawable.ic_drawer_meet_chat_of,
        itemIconOn = R.drawable.ic_drawer_meet_chat_on,
        state = StateHome.Dashboard(R.color.blue_300)),
//    val mDashboard: ItemMenu = ItemMenu( itemName = R.string.drawer_menu_dashboard,
//        itemIconOf = R.drawable.ic_drawer_dashboard_of,
//        itemIconOn = R.drawable.ic_drawer_dashboard_on,
//        state = StateHome.Dashboard()),
    val mStatistic: ItemMenu = ItemMenu( itemName = R.string.drawer_menu_statistic,
        itemIconOf = R.drawable.ic_drawer_statistic_of,
        itemIconOn = R.drawable.ic_drawer_statistic_on,
        state = StateHome.Statistic()),
    val mReferrals: ItemMenu = ItemMenu( itemName = R.string.drawer_menu_referrals,
        itemIconOf = R.drawable.ic_drawer_referrals_of,
        itemIconOn = R.drawable.ic_drawer_referrals_on,
        state = StateHome.Referrals(R.color.blue_300)),
    val mEarnings : ItemMenu = ItemMenu( itemName = R.string.drawer_menu_earnings,
        itemIconOf = R.drawable.ic_drawer_earnings_of,
        itemIconOn = R.drawable.ic_drawer_earnings_on,
        state = StateHome.Earnings()),
    val mRewards  : ItemMenu = ItemMenu( itemName = R.string.drawer_menu_rewards,
        itemIconOf = R.drawable.ic_drawer_rewards_of,
        itemIconOn = R.drawable.ic_drawer_rewards_on,
        state = StateHome.Rewards()),
    val mBonusPlaza : ItemMenu = ItemMenu( itemName = R.string.drawer_menu_plaza,
        itemIconOf = R.drawable.ic_drawer_bonus_plaza_of,
        itemIconOn = R.drawable.ic_drawer_bonus_plaza_on,
        state = StateHome.Plaza()),
    val mPioneer  : ItemMenu = ItemMenu( itemName = R.string.drawer_menu_pioneer,
        itemIconOf = R.drawable.ic_drawer_pioneer_of,
        itemIconOn = R.drawable.ic_drawer_pioneer_on,
        state = StateHome.Pioneer()),
    val mProfile  : ItemMenu = ItemMenu( itemName = R.string.drawer_menu_profile,
        itemIconOf = R.drawable.ic_drawer_profile_of,
        itemIconOn = R.drawable.ic_drawer_profile_on,
        state = StateHome.Profile()),
) {
    fun getListMenu() = listOf(
//            mChatMeet,
            mMeetings,
            mStatistic,
            mReferrals,
            mEarnings,
            mRewards,
            mBonusPlaza,
            mPioneer,
            mProfile,
//            mSettings
    )
}
