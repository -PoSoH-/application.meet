package meet.sumra.app.data.mdl.earnings

import android.graphics.Color
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IFillFormatter
import com.github.mikephil.charting.formatter.PercentFormatter
import meet.sumra.app.R
import meet.sumra.app.data.pojo.ResponseRepository
import meet.sumra.app.ext.ClrExt
import meet.sumra.app.interfaces.sealeds.SChartInterval
import meet.sumra.app.vm.BaseViewModel
import meet.sumra.app.vm.vm_home.ViewModelHome

object modelEarnings {

    data class YourEarnings(
        val title: String,
        val image: Int,
        val value: String
    )

    data class LeaderBoard(
        val position: Int,
        val image: Int,
        val displayName: String,
        val count: String
    )

    data class Projected(
        val planFree  : PlanCharacter,
        val planBronze: PlanCharacter,
        val planSilver: PlanCharacter,
        val planGold  : PlanCharacter,
    )

    data class PlanCharacter(
        val planName: String,
        val planValues: String
    )

    data class ShowGold(
        val values: String
    )

/***

    internal fun updateDataLineChart(params: SChartInterval) : LineDataSet {
        val interval: String?
        return when(params){
            is SChartInterval.Days   -> {
                interval = params.interval
                setDataForLineChart(count = 30, range = 100F)
            }
            is SChartInterval.Weeks  -> {
                interval = params.interval
                setDataForLineChart(count = 7, range = 100F)
            }
            is SChartInterval.Months -> {
                interval = params.interval
                setDataForLineChart(count = 30, range = 100F)
            }
            is SChartInterval.Years  -> {
                interval = params.interval
                setDataForLineChart(count = 12, range = 100F)
            }
        }
    }

    private fun setDataForLineChart (count: Int, range: Float):LineDataSet {
        val values = ArrayList<Entry>()

        val fillFormatter = IFillFormatter { dataSet, dataProvider ->
            viewFragment.frgRewardsChartsShow.getAxisLeft().getAxisMinimum()
        }
        val data = LineData(state.data)
        data.setValueTypeface(viewModel.updateTypeFace())
        data.setValueTextSize(7f)
        data.setDrawValues(false)

        data.mode = LineDataSet.Mode.HORIZONTAL_BEZIER

        data.cubicIntensity = 0.2f
        data.setDrawFilled(true)
        data.setDrawCircles(false)
        data.lineWidth = 1.8f
        data.circleRadius = 4f
        data.setCircleColor(ResourcesCompat.getColor(resources, R.color.blue_400, null)) //  (Color.WHITE)

        data.highLightColor = ContextCompat.getColor(requireContext(), R.color.danger)  //ResourcesCompat.getColor(resources, R.color.blue_400, null) // resources.getColor(R.color.blue_400) //ColorCompat()
        data.color = ResourcesCompat.getColor(resources, R.color.blue_400, null) // resources.getColor(R.color.blue_400) //Color.WHITE

        data.fillColor = ContextCompat.getColor(requireContext(), R.color.transparent) // ResourcesCompat.getColor(resources, R.color.gray_500, null) // resources.getColor(R.color.gray_500) //Color.WHITE
        data.fillDrawable = AppCompatResources.getDrawable(requireContext(), R.drawable.reward_background_linear_chart_gradient)

        data.fillAlpha = 0
        data.setDrawHorizontalHighlightIndicator(false)
    }

***/

//    private fun setDataForPieChart(count: Int, range: Float): PieDataSet {
//        val values = java.util.ArrayList<PieEntry>()
//
//        val parties = arrayOf(
//            "", "", "Party C", "Party D", "Party E", "Party F", "Party G", "Party H",
//            "Party I", "Party J", "Party K", "Party L", "Party M", "Party N", "Party O", "Party P",
//            "Party Q", "Party R", "Party S", "Party T", "Party U", "Party V", "Party W", "Party X",
//            "Party Y", "Party Z")
//
//        for (i in 0 until count) {
//            values.add(
//                PieEntry((Math.random() * range + range/5).toFloat(),
//                    parties.get(i % parties.size)
//                )
//            )
//        }
//        val dataSet = PieDataSet(values, null) //"Election Results")
//        dataSet.sliceSpace     = 3f
//        dataSet.selectionShift = 5f
//
//        dataSet.setColors(ClrExt.color_gold_l, ClrExt.color_blue_500_op20)
//        dataSet.setSelectionShift(0f);
//        val data = PieData(dataSet)
//        data.setValueFormatter(PercentFormatter())
//        data.setValueTextSize(0f)
//        data.setValueTextColor(Color.WHITE)
//
//        return dataSet
//
//    }



//    class EmulationEarningsChartData(){
//
//    }
//
//    class EarningLinear() {
//
//        val LineDataSet
//
//        state.data .fillFormatter =
//        IFillFormatter
//        {
//            dataSet, dataProvider ->
//            viewFragment.frgRewardsChartsShow.getAxisLeft().getAxisMinimum()
//        }
//
//        val data = LineData(state.data)
//        data .setValueTypeface(viewModel.updateTypeFace())
//        data .setValueTextSize(7f)
//        data .setDrawValues(false)
//
//        state.data .mode = LineDataSet.Mode.HORIZONTAL_BEZIER
//
//        state.data .cubicIntensity = 0.2f
//        state.data .setDrawFilled(true)
//        state.data .setDrawCircles(false)
//        state.data .lineWidth = 1.8f
//        state.data .circleRadius = 4f
//        state.data .setCircleColor(ResourcesCompat.getColor(resources, R.color.blue_400, null)) //  (Color.WHITE)
//
//        state.data .highLightColor = ContextCompat.getColor(requireContext(), R.color.danger)  //ResourcesCompat.getColor(resources, R.color.blue_400, null) // resources.getColor(R.color.blue_400) //ColorCompat()
//        state.data .color = ResourcesCompat.getColor(resources, R.color.blue_400, null) // resources.getColor(R.color.blue_400) //Color.WHITE
//
//        state.data .fillColor = ContextCompat.getColor(requireContext(), R.color.transparent) // ResourcesCompat.getColor(resources, R.color.gray_500, null) // resources.getColor(R.color.gray_500) //Color.WHITE
//        state.data .fillDrawable = AppCompatResources.getDrawable(requireContext(), R.drawable.reward_background_linear_chart_gradient)
//
//        state.data .fillAlpha = 0
//        state.data .setDrawHorizontalHighlightIndicator(false)
//
//        viewFragment.frgRewardsChartsShow.setData(data )
//        viewFragment.frgRewardsChartsShow.invalidate()
//
//
//
//
//    }

}
