package meet.sumra.app.data.mdl

data class _AppEarning(
    val userID: String,
    val membershipPlanID: String,
    val currency : String = "USD",
    val bonuses: List<_AppBonuses>
)

data class _AppBonuses(
    val bonusID  : String,
    val count    : Int,
    val bonus    : Int,
    val createdAt: String = "",
    val updatedAt: String = "",
)
