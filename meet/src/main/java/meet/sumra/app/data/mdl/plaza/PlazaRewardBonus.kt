package meet.sumra.app.data.mdl.plaza

import androidx.compose.ui.graphics.Color
import com.github.mikephil.charting.data.BarData
import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList

data class PlazaRewardBonus (
    val id: String = UUID.randomUUID().toString(),
    @SerializedName("total")
    val total: Total = Total(),
    @SerializedName("daily_bonus")
    val dailyBonus: ArrayList<DailyBonus>? = null,
    @SerializedName("rewards_statistic")
    val rewardStatistic: RewardStatistic? = null
)

data class Total(
    @SerializedName("value")
    val value: String = "0",
    @SerializedName("currency")
    val currency: String = "DR"
)

data class DailyBonus(
    @SerializedName("type")
    val type: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("currency")
    val currency: String,
    @SerializedName("values")
    val value: String,
    @SerializedName("values_all")
    val valueAll: String,
    @SerializedName("label")
    val message: String
)

data class RewardStatistic(
    @SerializedName("title")
    val title: String,
    @SerializedName("total_balance")
    val totalBalance: TotalBalance,
    @SerializedName("labels")
    val labels: ArrayList<Legend>,
    @SerializedName("charts")
    val charts: ArrayList<ChartValue>
)

data class TotalBalance(
    @SerializedName("value")
    val value: String,
    @SerializedName("currency")
    val currency: String
)

data class Legend(
    @SerializedName("id")
    val id: String,
    @SerializedName("title")
    val legendTitle: String,
    @SerializedName("color")
    val legendColor: Color,
)

data class ChartValue(
    @SerializedName("part")
    val value: ArrayList<ChartPart>,
)

data class ChartPart(
    @SerializedName("label_id")
    val labelId: String,
    @SerializedName("value")
    val value: Float,
)

data class PlazaRewardChart(
    val data: BarData
)

enum class EDailyBonus {
    type_daily_bonus,
    type_commercial,
    type_sumra_id,
    type_mobile_app,
    type_publishing_to,
    type_publishing_with,
    type_surveys,
    type_reviews;

    fun equals(e: String):Boolean{
        if(e.hashCode() == this.hashCode()){
            return e.equals(this)
        }else{
            return false
        }
    }
}