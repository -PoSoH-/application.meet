package meet.sumra.app.data.pojo.contacts

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import sdk.net.meet.api.contacts.response.FetchContact

data class ContactHoldState(
    val contact: FetchContact,
    var isInvite: Boolean = false
)

data class ContactHoldCompose(
    val contact  : FetchContact,
    var isInvite : MutableState<Boolean> = mutableStateOf(false)
)