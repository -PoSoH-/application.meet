package meet.sumra.app.data.configs

import android.graphics.Typeface
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.BarLineChartBase
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.Legend.LegendForm
import com.github.mikephil.charting.components.XAxis.XAxisPosition
import com.github.mikephil.charting.components.YAxis.YAxisLabelPosition
import com.github.mikephil.charting.formatter.*
import net.sumra.auth.helpers.Finals
import java.text.DecimalFormat

object settingsCharts {

    fun setupDivitBalanceChartSetup(chart: BarChart){
        val tf = Typeface.createFromAsset(chart.context.assets, Finals.FONT_FACE)
        chart.apply {
            setDrawBarShadow(false)
            setDrawValueAboveBar(true)

            description.isEnabled = false

            setMaxVisibleValueCount(60)
            setPinchZoom(false)
            setDrawGridBackground(false)

            isScaleYEnabled = false
            isScaleXEnabled = true

            data.dataSets.forEach {
                it.setDrawValues(false)
                it.isHighlightEnabled = false }
        }
        val xAxisFormatter: ValueFormatter = modelDivitsRewardsBonus.XAxisValueFormatter(chart)

        val xAxis = chart.xAxis
        xAxis.position = XAxisPosition.BOTTOM
        xAxis.typeface = tf
        xAxis.setDrawGridLines(false)
        xAxis.granularity = 1f // only intervals of 1 day

        xAxis.labelCount = 7
        xAxis.setValueFormatter(xAxisFormatter)

        val custom: ValueFormatter = modelDivitsRewardsBonus.YAxisValueFormatter(chart = chart)

        chart.axisLeft.apply {
            typeface = tf
            setLabelCount(5, false)
            setValueFormatter(custom as ValueFormatter?)
            setPosition(YAxisLabelPosition.OUTSIDE_CHART)
            spaceTop = 15f
            axisMinimum = 0f // this replaces setStartAtZero(true)
            isEnabled = true
        }

        chart.axisRight.apply {
            typeface = tf
            setDrawGridLines(false)
            setLabelCount(5, false)
            setValueFormatter(custom as ValueFormatter?)
            spaceTop = 15f
            axisMinimum = 0f // this replaces setStartAtZero(true)
            isEnabled = false
        }

        chart.legend.apply {
            typeface = tf
            verticalAlignment =
                com.github.mikephil.charting.components.Legend.LegendVerticalAlignment.BOTTOM
            horizontalAlignment =
                com.github.mikephil.charting.components.Legend.LegendHorizontalAlignment.LEFT
            orientation =
                com.github.mikephil.charting.components.Legend.LegendOrientation.HORIZONTAL
            setDrawInside(false)
            form = LegendForm.CIRCLE //SQUARE
            formSize = 9f
            textSize = 11f
            xEntrySpace = 4f
            isEnabled = false
        }

//        val mv = RewardBarConfig.XYMarkerView(chart.context, xAxisFormatter)
//        mv.setChartView(chart) // For bounds control
//
//        chart.marker = mv

//        chart.setDrawBarShadow(false)
//        chart.setDrawValueAboveBar(true)
////        chart.getData().getDataSets().let { it ->
////            for (set in it) {
////                set.setDrawValues(false)
////            }
////        }
////        chart.invalidate()
//
//        chart.description.isEnabled = false
//
//        // if more than 60 entries are displayed in the chart, no values will be
//        // drawn
//        chart.setMaxVisibleValueCount(60)
//        // scaling can now only be done on x- and y-axis separately
//        chart.setPinchZoom(false)
//        chart.isScaleYEnabled = false
//        chart.isScaleXEnabled = true
//
//        chart.setDrawGridBackground(false)
//        // chart.setDrawYLabels(false);
//        val xAxisFormatter: IAxisValueFormatter = RewardBarConfig.DayAxisValueFormatter(chart)
//
//        chart.xAxis.apply {
//            position = XAxisPosition.BOTTOM
//            typeface = tf
//            setDrawGridLines(false)
//            setDrawAxisLine(false)
//            granularity = 1f // only intervals of 1 day
////            labelCount = 7
//            axisLineColor
//            textColor = R.color.gray_200 // Repository.color_gold.toInt()
//            textSize = 9f
//            setValueFormatter(xAxisFormatter as ValueFormatter)
//        }
//
////        val custom: IAxisValueFormatter = RewardBarConfig.MyAxisValueFormatter()
//
//        chart.axisLeft.apply {
////            typeface = tfLight
////            setLabelCount(8, false)
////            setValueForma0tter(custom as ValueFormatter)
////            setPosition(YAxisLabelPosition.OUTSIDE_CHART)
////            spaceTop = 15f
////            axisMinimum = 0f // this replaces setStartAtZero(true)
//            textColor = R.color.gray_200
//            isEnabled = false
//        }
//
//        chart.axisRight.apply {
////            setDrawGridLines(false)
////            typeface = tfLight
////            setLabelCount(8, false)
////            setValueFormatter(custom as ValueFormatter)
////            spaceTop = 15f
////            axisMinimum = 0f // this replaces setStartAtZero(true)
//            textColor = R.color.gray_200
//            isEnabled = false
//        }
//
//        chart.legend.apply {
////            verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
////            horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
////            orientation = Legend.LegendOrientation.HORIZONTAL
////            setDrawInside(false)
////            form = LegendForm.SQUARE
////            formSize = 9f
////            textSize = 11f
////            xEntrySpace = 4f
//            textColor = R.color.gray_200
//            isEnabled = false
//        }
//
//        // marker setup...
//        val mv = RewardBarConfig.XYMarkerView(chart.context, xAxisFormatter)
//        mv.setChartView(chart) // For bounds control
//        chart.marker = mv // Set the marker to the chart

        chart.invalidate()

        class XAxisValueFormatter(chart: BarChart) : ValueFormatter() {
            val w = mutableListOf("MON","TUE","WED","THU","FRI","SAT","SUN")
            val y = mutableListOf("YAN", "FEB", "MAR", "APR", "MAI", "JUN", "JUL", "AUG", "SEP", "OKT", "NOV", "DEC")
            val count = chart.legend.entries.size
            val data = mutableListOf<String>()

            init{
                when(count){
                    /*about day*/ 24 -> {chart.legend.entries.forEachIndexed{i, v -> data.add(i.toString())}}
                    /*about week*/ 7 -> {chart.legend.entries.forEachIndexed{i, v -> data.add(w[i])}}
                    /*about month*/ in 28..31 -> {chart.legend.entries.forEachIndexed{i, v ->  data.add(i.toString())}}
                    /*about year*/ 12 -> {chart.legend.entries.forEachIndexed{i, v -> data.add(y[i])}}
                    else -> {data.add("")}
                }
            }

//        when(count){
//            /*about day*/ 24 -> {data.add("1","2","3","4","5","6","7")}
//            /*about week*/ 7 -> {data.add("MON","TUE","WED","THU","FRI","SAT","SUN")}
//            /*about month*/ in 28..31 -> {chart.legend.entries  data.add("YAN","FEB","MAR","APR","MAI","JUN","JUL","AUG","SEP","OKT","NOV","DEC")}
//            /*about year*/ 12 -> {data.add("YAN","FEB","MAR","APR","MAI","JUN","JUL","AUG","SEP","OKT","NOV","DEC")}
//            else -> {data.add("")}
//         }

            override fun getFormattedValue(value: Float, axis: AxisBase): String {
                return data[value.toInt()] // "${axis.longestLabel[0]}${axis.longestLabel[1]}${axis.longestLabel[2]}"
            }
        }

        class YAxisValueFormatter(private val chart: BarLineChartBase<*>) : ValueFormatter() {
            private val mFormat: DecimalFormat
            override fun getFormattedValue(value: Float, axis: AxisBase): String {
                return mFormat.format(value.toDouble()) + ""
            }
            init {
                mFormat = DecimalFormat("###,###,###,##0") //.0")
            }
        }

    }

/***   Setup PieChart for total balance   ***/

    fun setupPieBarBonusChart(chart: PieChart){
        val tf = Typeface.createFromAsset(chart.context.assets, Finals.FONT_FACE)
        chart.apply {
            setUsePercentValues(false)
            getDescription().setEnabled(false)
//            setExtraOffsets(5f, 10f, 5f, 5f)
            setExtraOffsets(0f, 0f, 0f, 0f)

//            setDragDecelerationFrictionCoef(0.95f)
            setDragDecelerationFrictionCoef(1f)

            if(data.dataSets != null) {
                data.dataSets.forEach {
                    it.setDrawValues(false)
                }
            }
            setDrawEntryLabels(false)

            setBackgroundColor(android.graphics.Color.parseColor("#451278"))

            isClickable = false

//            tf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf")
//            setCenterTextTypeface(Typeface.createFromAsset(chart.context.assets, "OpenSans-Light.ttf"))
//            setCenterText(generateCenterSpannableText())

            setDrawCenterText(false)

            setExtraOffsets(20f, 0f, 20f, 0f)
            setDrawHoleEnabled(true)
            setHoleColor(android.graphics.Color.TRANSPARENT)

            setTransparentCircleColor(android.graphics.Color.TRANSPARENT)
            setTransparentCircleAlpha(110)

            setHoleRadius((height/4).toFloat()) // (58f)
            setTransparentCircleRadius((height/4).toFloat()) // (61f)

            setRotationAngle(0f)

            setRotationEnabled(true)
            setHighlightPerTapEnabled(false)

            // chart.setUnit(" €");
            // chart.setDrawUnitsInChart(true);

            // add a selection listener

            // chart.setUnit(" €");
            // chart.setDrawUnitsInChart(true);

            animateY(1400, Easing.EaseInOutQuad)
        }

        chart.legend.apply {
            verticalAlignment = com.github.mikephil.charting.components.Legend.LegendVerticalAlignment.TOP
            horizontalAlignment = com.github.mikephil.charting.components.Legend.LegendHorizontalAlignment.RIGHT
            orientation = com.github.mikephil.charting.components.Legend.LegendOrientation.VERTICAL
            setDrawInside(false)
            typeface = tf
            isEnabled = false
        }
    }

    fun setupLineTokenChartStatistic(chart: LineChart, color: Int){
        val tf = Typeface.createFromAsset(chart.context.assets, Finals.FONT_FACE)
        chart.apply {
            setBackgroundColor(android.graphics.Color.TRANSPARENT)
            description.isEnabled = false

            setTouchEnabled(true)
            setDrawGridBackground(false)

//            val mv = MyMarkerView(this, R.layout.custom_marker_view)
//            mv.setChartView(chart)
//            marker = mv

            isDragEnabled = true
            setScaleXEnabled(true)
            setScaleYEnabled(false)

            setPinchZoom(true)

            if(data!= null && data.dataSets!= null){
                data.dataSets.forEach {
                    it.setDrawValues(false)
//                    val t = it as LineDataSet
//                    t.setDrawCircles(true)
//                    t.setDrawCircleHole(true)
//                    t.setCircleColor(color)
                }
            }
        }

        chart.xAxis.apply {
            typeface = tf
            // vertical grid lines
            enableGridDashedLine(10f, 10f, 0f)
            position = XAxisPosition.BOTTOM
            labelCount = 10
//            setValueFormatter(ValueFormatter)
        }

        chart.axisLeft.apply {
            typeface = tf
            isEnabled = true
        }

        chart.axisRight.apply {
            typeface = tf
            isEnabled = false
            enableGridDashedLine(10f, 10f, 0f)
//        yAxis.axisMaximum = 200f
//        yAxis.axisMinimum = 0f
        }

//        val llXAxis = LimitLine(9f, "Index 10").apply {
//            lineWidth = 4f
//            enableDashedLine(10f, 10f, 0f)
//            labelPosition = LimitLabelPosition.RIGHT_BOTTOM
//            textSize = 10f
////            typeface =
//            val ll1 = LimitLine(150f, "Upper Limit").apply {
//                lineWidth = 4f
//                labelPosition = LimitLabelPosition.RIGHT_TOP
//                textSize = 10f
////                typeface = tfRegular
//            }
//            val ll2 = LimitLine(-30f, "Lower Limit").apply {
//                lineWidth = 4f
//                enableDashedLine(10f, 10f, 0f)
//                labelPosition = LimitLabelPosition.RIGHT_BOTTOM
//                textSize = 10f
////                typeface = tfRegular
//            }
//            // draw limit lines behind data instead of on top
//            yAxis.setDrawLimitLinesBehindData(true)
//            xAxis.setDrawLimitLinesBehindData(true)
//            yAxis.addLimitLine(ll1)
//            yAxis.addLimitLine(ll2)
//        }

        chart.animateX(1500)

        chart.legend.apply {
            typeface = tf
            setEnabled(false)
        }
//        l.form = LegendForm.LINE
    }

    fun setupBarTokenChartStatistic(chart: BarChart){
        val tf = Typeface.createFromAsset(chart.context.assets, Finals.FONT_FACE)
        chart.apply {
            description.isEnabled = false

//        chart.setDrawBorders(true);
            // scaling can now only be done on x- and y-axis separately
//        chart.setDrawBorders(true);

            // scaling can now only be done on x- and y-axis separately
            setPinchZoom(false)
            setDrawBarShadow(false)
            setDrawGridBackground(false)

//            val mv = MyMarkerView(this, R.layout.custom_marker_view)
//            mv.setChartView(chart) // For bounds control
//            marker = mv // Set the marker to the chart
        }

        chart.legend.apply {
            setDrawInside(true)
            typeface = tf
            verticalAlignment = Legend.LegendVerticalAlignment.TOP
            horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
            orientation = Legend.LegendOrientation.VERTICAL
            yOffset = 0f
            xOffset = 10f
            yEntrySpace = 0f
            textSize = 8f
            isEnabled = false
        }

        chart.xAxis.apply {
            typeface = tf
            position = XAxisPosition.BOTTOM
            granularity = 1f
            setCenterAxisLabels(true)
//            xAxis.setValueFormatter(IAxisValueFormatter { value, axis -> (return@IAxisValueFormatter value as Int.toString)() })
        }

        chart.axisLeft.apply {
            typeface = tf
            valueFormatter = LargeValueFormatter()
            setDrawGridLines(false)
            spaceTop = 35f
            axisMinimum = 0f // this replaces setStartAtZero(true)
        }

        chart.axisRight.apply {
            typeface = tf
            isEnabled = false
        }
    }

}


