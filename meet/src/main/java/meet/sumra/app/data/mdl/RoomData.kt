package meet.sumra.app.data.mdl

data class RoomData (
    val name: String,
    val status: Boolean = true,
    val password: String? = null
)