package meet.sumra.app.data.mdl.plaza

import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList

data class PlazaHowToReward(
    val id: String = UUID.randomUUID().toString(),
    @SerializedName("characters")
    val characters: ArrayList<HowToSection>
)

data class HowToSection(
    @SerializedName("type_section")
    val typeSection: String,
    @SerializedName("label")
    val label  : String,
    @SerializedName("message")
    val message: String,
    @SerializedName("reward")
    val reward : ObjData,
    @SerializedName("limit")
    val limit  : ObjData,
)

data class ObjData(
    @SerializedName("value")
    val value: String,
    @SerializedName("currency")
    val currency: String
){fun getData() = "$value $currency"}