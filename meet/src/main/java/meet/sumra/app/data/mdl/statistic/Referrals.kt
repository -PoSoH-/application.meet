package meet.sumra.app.data.mdl.statistic

import androidx.compose.runtime.MutableState
import com.github.mikephil.charting.data.BarEntry
import meet.sumra.app.data.configs.setupStatisticCharts.fetchBarData

data class Referrals(
    val cardInvite: Float = 0F,    // = 568,
    val cardBarChart: MutableState<MutableList<BarEntry>> = fetchBarData(), //.data = ,
    val cardReferrals: Float = 0F, // = 58,
    val cardGrOrFall: Int = 0,     // +8,
)
