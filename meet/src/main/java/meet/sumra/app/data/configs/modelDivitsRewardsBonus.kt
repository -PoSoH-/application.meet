package meet.sumra.app.data.configs

import androidx.compose.ui.graphics.Color
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.BarLineChartBase
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import meet.sumra.app.data.mdl.earnings.cards
import meet.sumra.app.data.mdl.plaza.*
import java.text.DecimalFormat
import kotlin.random.Random

object modelDivitsRewardsBonus {

    fun getDivitsRewardStatistic(period: cards.EPeriod): RewardStatistic {
        val labels = arrayListOf(
            Legend("12345", legendTitle = "Commercial activity", legendColor = Color(0xffFFAB00)),
            Legend("23456", legendTitle = "Download our mobile apps", legendColor = Color(0xff38CB89)),
            Legend("34567", legendTitle = "Referrals balance paid-out", legendColor = Color(0xffFF5630)),
            Legend("45678", legendTitle = "Register at Sumra ID", legendColor = Color(0xff377DFF))
        )
        return RewardStatistic(
            title = "Total Divits Balance",
            totalBalance = TotalBalance(value = "4,300", currency = "DR"),
            labels = labels,
            charts = when (period) {
                cards.EPeriod.DAY   -> { generatorChartDay  (labels = labels) }
                cards.EPeriod.WEEK  -> { generatorChartWeek (labels = labels) }
                cards.EPeriod.MONTH -> { generatorChartMonth(labels = labels) }
                cards.EPeriod.YEAR  -> { generatorChartYear (labels = labels) }
            }
        )
    }

    fun generatorChartDay(labels: ArrayList<Legend>): ArrayList<ChartValue>{
        val max = 24
        val index = mutableListOf(0)
        val chartValues = mutableListOf<ChartValue>()
        while (index[0] < max){
            chartValues.add(ChartValue(generatorChartPart(legends = labels)))
            index[0]++
        }
        return chartValues as ArrayList<ChartValue>
    }

    fun generatorChartWeek(labels: ArrayList<Legend>): ArrayList<ChartValue>{
        val max = 7
        val index = mutableListOf(0)
        val chartValues = mutableListOf<ChartValue>()
        while (index[0] < max){
            chartValues.add(ChartValue(generatorChartPart(legends = labels)))
            index[0]++
        }
        return chartValues as ArrayList<ChartValue>
    }

    fun generatorChartMonth(labels: ArrayList<Legend>): ArrayList<ChartValue>{
        val max = 31
        val index = mutableListOf(0)
        val chartValues = mutableListOf<ChartValue>()
        while (index[0] < max){
            chartValues.add(ChartValue(generatorChartPart(legends = labels)))
            index[0]++
        }
        return chartValues as ArrayList<ChartValue>
    }

    fun generatorChartYear(labels: ArrayList<Legend>): ArrayList<ChartValue>{
        val max = 12
        val index = mutableListOf(0)
        val chartValues = mutableListOf<ChartValue>()
        while (index[0] < max){
            chartValues.add(ChartValue(generatorChartPart(legends = labels)))
            index[0]++
        }
        return chartValues as ArrayList<ChartValue>
    }

    fun generatorChartPart(legends: ArrayList<Legend>): ArrayList<ChartPart>{
        val parts = arrayListOf<ChartPart>()
        legends.forEach {
            parts.add(ChartPart(labelId = it.id, value = Random.nextInt(10, 50).toFloat()))
        }
        return parts
    }

    fun convertToBarData(cd: RewardStatistic): BarData {
        val values = java.util.ArrayList<BarEntry>()
        cd.charts.forEachIndexed { index, chartdata ->
            val tmpVal = mutableListOf<Float>()
            chartdata.value.forEach{
                tmpVal.add(it.value)
            }
            values += BarEntry(index.toFloat(), tmpVal.toFloatArray())
        }
        val set1: BarDataSet
        set1 = BarDataSet(values, "Total Divits balance...")
        set1.setDrawIcons(false)
//        val colors = mutableListOf<Int>()
//        cd.labels.forEach {
//            colors.add (it.legendColor.toArgb()) // as android.graphics.Color )
//        }
        val labels = mutableListOf<String>()
        cd.labels.forEach {
            labels.add ( it.legendTitle)
        }
//        set1.setColors(colors)
        set1.stackLabels = labels.toTypedArray()
        val dataSets = java.util.ArrayList<IBarDataSet>()
        dataSets.add(set1)
        val data = BarData(dataSets)
//        data.setValueFormatter(MyValueFormatter())
        data.setValueTextColor(android.graphics.Color.BLACK)
        return data
//        if (chart.getData() != null &&
//            chart.getData().getDataSetCount() > 0
//        ) {
//            set1 = chart.getData().getDataSetByIndex(0)
//            set1.values = values
//            chart.getData().notifyDataChanged()
//            chart.notifyDataSetChanged()
//        } else {
//            set1 = BarDataSet(values, "Statistics Vienna 2014")
//            set1.setDrawIcons(false)
//            set1.setColors(*getColors())
//            set1.stackLabels = arrayOf("Births", "Divorces", "Marriages")
//            val dataSets = java.util.ArrayList<IBarDataSet>()
//            dataSets.add(set1)
//            val data = BarData(dataSets)
//            data.setValueFormatter(MyValueFormatter())
//            data.setValueTextColor(android.graphics.Color.WHITE)
//            chart.setData(data)
//        }
    }

//    fun setupDivitBalanceChartSetup(chart: BarChart, tf: Typeface){
//
//        chart.apply {
//            setDrawBarShadow(false)
//            setDrawValueAboveBar(true)
//
//            description.isEnabled = false
//
//            setMaxVisibleValueCount(60)
//            setPinchZoom(false)
//            setDrawGridBackground(false)
//
//            isScaleYEnabled = false
//            isScaleXEnabled = true
//
//            data.dataSets.forEach {
//                it.setDrawValues(false)
//                it.isHighlightEnabled = false }
//        }
//        val xAxisFormatter: ValueFormatter = XAxisValueFormatter(chart)
//
//        val xAxis = chart.xAxis
//        xAxis.position = XAxisPosition.BOTTOM
//        xAxis.typeface = tf
//        xAxis.setDrawGridLines(false)
//        xAxis.granularity = 1f // only intervals of 1 day
//
//        xAxis.labelCount = 7
//        xAxis.setValueFormatter(xAxisFormatter)
//
//        val custom: ValueFormatter = YAxisValueFormatter(chart = chart)
//
//        chart.axisLeft.apply {
//            typeface = tf
//            setLabelCount(5, false)
//            setValueFormatter(custom as ValueFormatter?)
//            setPosition(YAxisLabelPosition.OUTSIDE_CHART)
//            spaceTop = 15f
//            axisMinimum = 0f // this replaces setStartAtZero(true)
//            isEnabled = true
//        }
//
//        chart.axisRight.apply {
//            setDrawGridLines(false)
//            typeface = tf
//            setLabelCount(5, false)
//            setValueFormatter(custom as ValueFormatter?)
//            spaceTop = 15f
//            axisMinimum = 0f // this replaces setStartAtZero(true)
//            isEnabled = false
//        }
//
//        chart.legend.apply {
//            verticalAlignment =
//                com.github.mikephil.charting.components.Legend.LegendVerticalAlignment.BOTTOM
//            horizontalAlignment =
//                com.github.mikephil.charting.components.Legend.LegendHorizontalAlignment.LEFT
//            orientation =
//                com.github.mikephil.charting.components.Legend.LegendOrientation.HORIZONTAL
//            setDrawInside(false)
//            form = LegendForm.CIRCLE //SQUARE
//            formSize = 9f
//            textSize = 11f
//            xEntrySpace = 4f
//            isEnabled = false
//        }

//        val mv = RewardBarConfig.XYMarkerView(chart.context, xAxisFormatter)
//        mv.setChartView(chart) // For bounds control
//
//        chart.marker = mv

//        chart.setDrawBarShadow(false)
//        chart.setDrawValueAboveBar(true)
////        chart.getData().getDataSets().let { it ->
////            for (set in it) {
////                set.setDrawValues(false)
////            }
////        }
////        chart.invalidate()
//
//        chart.description.isEnabled = false
//
//        // if more than 60 entries are displayed in the chart, no values will be
//        // drawn
//        chart.setMaxVisibleValueCount(60)
//        // scaling can now only be done on x- and y-axis separately
//        chart.setPinchZoom(false)
//        chart.isScaleYEnabled = false
//        chart.isScaleXEnabled = true
//
//        chart.setDrawGridBackground(false)
//        // chart.setDrawYLabels(false);
//        val xAxisFormatter: IAxisValueFormatter = RewardBarConfig.DayAxisValueFormatter(chart)
//
//        chart.xAxis.apply {
//            position = XAxisPosition.BOTTOM
//            typeface = tf
//            setDrawGridLines(false)
//            setDrawAxisLine(false)
//            granularity = 1f // only intervals of 1 day
////            labelCount = 7
//            axisLineColor
//            textColor = R.color.gray_200 // Repository.color_gold.toInt()
//            textSize = 9f
//            setValueFormatter(xAxisFormatter as ValueFormatter)
//        }
//
////        val custom: IAxisValueFormatter = RewardBarConfig.MyAxisValueFormatter()
//
//        chart.axisLeft.apply {
////            typeface = tfLight
////            setLabelCount(8, false)
////            setValueForma0tter(custom as ValueFormatter)
////            setPosition(YAxisLabelPosition.OUTSIDE_CHART)
////            spaceTop = 15f
////            axisMinimum = 0f // this replaces setStartAtZero(true)
//            textColor = R.color.gray_200
//            isEnabled = false
//        }
//
//        chart.axisRight.apply {
////            setDrawGridLines(false)
////            typeface = tfLight
////            setLabelCount(8, false)
////            setValueFormatter(custom as ValueFormatter)
////            spaceTop = 15f
////            axisMinimum = 0f // this replaces setStartAtZero(true)
//            textColor = R.color.gray_200
//            isEnabled = false
//        }
//
//        chart.legend.apply {
////            verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
////            horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
////            orientation = Legend.LegendOrientation.HORIZONTAL
////            setDrawInside(false)
////            form = LegendForm.SQUARE
////            formSize = 9f
////            textSize = 11f
////            xEntrySpace = 4f
//            textColor = R.color.gray_200
//            isEnabled = false
//        }
//
//        // marker setup...
//        val mv = RewardBarConfig.XYMarkerView(chart.context, xAxisFormatter)
//        mv.setChartView(chart) // For bounds control
//        chart.marker = mv // Set the marker to the chart

//        chart.invalidate()
//    }

    class XAxisValueFormatter(chart: BarChart) : ValueFormatter() {
        val w = mutableListOf("MON","TUE","WED","THU","FRI","SAT","SUN")
        val y = mutableListOf("YAN", "FEB", "MAR", "APR", "MAI", "JUN", "JUL", "AUG", "SEP", "OKT", "NOV", "DEC")
        val count = chart.legend.entries.size
        val data = mutableListOf<String>()

        init{
            when(count){
                /*about day*/ 24 -> {chart.legend.entries.forEachIndexed{i, v -> data.add(i.toString())}}
                /*about week*/ 7 -> {chart.legend.entries.forEachIndexed{i, v -> data.add(w[i])}}
                /*about month*/ in 28..31 -> {chart.legend.entries.forEachIndexed{i, v ->  data.add(i.toString())}}
                /*about year*/ 12 -> {chart.legend.entries.forEachIndexed{i, v -> data.add(y[i])}}
                else -> {data.add("")}
            }
        }

//        when(count){
//            /*about day*/ 24 -> {data.add("1","2","3","4","5","6","7")}
//            /*about week*/ 7 -> {data.add("MON","TUE","WED","THU","FRI","SAT","SUN")}
//            /*about month*/ in 28..31 -> {chart.legend.entries  data.add("YAN","FEB","MAR","APR","MAI","JUN","JUL","AUG","SEP","OKT","NOV","DEC")}
//            /*about year*/ 12 -> {data.add("YAN","FEB","MAR","APR","MAI","JUN","JUL","AUG","SEP","OKT","NOV","DEC")}
//            else -> {data.add("")}
//         }

        override fun getFormattedValue(value: Float, axis: AxisBase): String {
            return data[value.toInt()] // "${axis.longestLabel[0]}${axis.longestLabel[1]}${axis.longestLabel[2]}"
        }
    }

    class YAxisValueFormatter(private val chart: BarLineChartBase<*>) : ValueFormatter() {
        private val mFormat: DecimalFormat
        override fun getFormattedValue(value: Float, axis: AxisBase): String {
            return mFormat.format(value.toDouble()) + ""
        }
        init {
            mFormat = DecimalFormat("###,###,###,##0") //.0")
        }
    }

/***   Setup PieChart for total balance   ***/

    fun setupPieBarBonusChart(chart: PieChart){

        chart.apply {
            setUsePercentValues(false)
            getDescription().setEnabled(false)
//            setExtraOffsets(5f, 10f, 5f, 5f)
            setExtraOffsets(0f, 0f, 0f, 0f)

//            setDragDecelerationFrictionCoef(0.95f)
            setDragDecelerationFrictionCoef(1f)

            if(data.dataSets != null) {
                data.dataSets.forEach {
                    it.setDrawValues(false)
                }
            }
            setDrawEntryLabels(false)

            setBackgroundColor(android.graphics.Color.parseColor("#451278"))

            isClickable = false

//            tf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf")
//            setCenterTextTypeface(Typeface.createFromAsset(chart.context.assets, "OpenSans-Light.ttf"))
//            setCenterText(generateCenterSpannableText())

            setDrawCenterText(false)

            setExtraOffsets(20f, 0f, 20f, 0f)
            setDrawHoleEnabled(true)
            setHoleColor(android.graphics.Color.TRANSPARENT)

            setTransparentCircleColor(android.graphics.Color.TRANSPARENT)
            setTransparentCircleAlpha(110)

            setHoleRadius((height/4).toFloat()) // (58f)
            setTransparentCircleRadius((height/4).toFloat()) // (61f)

            setRotationAngle(0f)

            setRotationEnabled(true)
            setHighlightPerTapEnabled(false)

            // chart.setUnit(" €");
            // chart.setDrawUnitsInChart(true);

            // add a selection listener

            // chart.setUnit(" €");
            // chart.setDrawUnitsInChart(true);

            animateY(1400, Easing.EaseInOutQuad)
        }

        chart.legend.apply {
            verticalAlignment = com.github.mikephil.charting.components.Legend.LegendVerticalAlignment.TOP
            horizontalAlignment = com.github.mikephil.charting.components.Legend.LegendHorizontalAlignment.RIGHT
            orientation = com.github.mikephil.charting.components.Legend.LegendOrientation.VERTICAL
            setDrawInside(false)
            isEnabled = false
        }
    }

    fun setupLineChartStatistic(chart: LineChart){

    }

    fun setupBarChartStatistic(chart: BarChart){

    }

}


