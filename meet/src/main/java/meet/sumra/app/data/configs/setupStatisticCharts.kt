package meet.sumra.app.data.configs

import android.graphics.Typeface
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.mutableStateOf
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.LargeValueFormatter
import com.github.mikephil.charting.formatter.ValueFormatter
import kotlinx.coroutines.flow.MutableStateFlow
import meet.sumra.app.data.mdl.earnings.cards
import meet.sumra.app.data.mdl.statistic.EState

import net.sumra.auth.helpers.Finals
import java.text.DecimalFormat
import kotlin.random.Random

object setupStatisticCharts {

    fun setupLineChart(chart: LineChart, color: Int){
        val tf = Typeface.createFromAsset(chart.context.assets, Finals.FONT_FACE)
        chart.apply {
            description.isEnabled = false
            setMaxVisibleValueCount(24)
            setDrawGridBackground(false)
            setPinchZoom(false)

            isScaleYEnabled = false
            isScaleXEnabled = false
            if(data != null){
                data.dataSets.forEach {
                    it.setDrawValues(false)
                    it.isHighlightEnabled = true
                    it.setDrawFilled(true)
                    val t = it as LineDataSet
                    t.setDrawCircles(false)
                    t.setDrawCircleHole(false)
                    t.cubicIntensity = 0.2f
                    t.lineWidth = 1.2f
                    t.color = color
                    t.mode = LineDataSet.Mode.HORIZONTAL_BEZIER
                }
            }
        }

        chart.xAxis.apply {
            position = XAxis.XAxisPosition.BOTTOM
            typeface = tf
            setDrawGridLines(false)
            granularity = 1f // only intervals of 1 day
            labelCount = 7
            isEnabled = false
        }

        chart.axisLeft.apply {
            typeface = tf
            setLabelCount(5, false)
            setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)
            spaceTop = 15f
            axisMinimum = 0f // this replaces setStartAtZero(true)
            isEnabled = false
        }

        chart.axisRight.apply {
            typeface = tf
            setDrawGridLines(false)
            setLabelCount(5, false)
            spaceTop = 15f
            axisMinimum = 0f // this replaces setStartAtZero(true)
            isEnabled = false
        }

        chart.legend.apply {
            typeface = tf
            verticalAlignment   = Legend.LegendVerticalAlignment.BOTTOM
            horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
            orientation         = Legend.LegendOrientation.HORIZONTAL
            setDrawInside(false)
            form = Legend.LegendForm.CIRCLE //SQUARE
            formSize = 9f
            textSize = 11f
            xEntrySpace = 4f
            isEnabled = false
        }
    }

    fun setupBarChart(chart: BarChart, color: Int){
        chart.apply {
            description.isEnabled = false
            setPinchZoom(false)
            setDrawGridBackground(false)
            data.barWidth = .6f
//            data.groupBars(0f, 1f, 1f)

            if(data!= null && data.dataSets!= null){
                data.dataSets.forEach {
                    it.setDrawValues(false)
                    val t = it as BarDataSet
                    t.barBorderColor = color
                    t.color = color
//                    t.barWidth = 1f
                }
            }
        }

        chart.legend.apply {
            setDrawInside(false)
            verticalAlignment = Legend.LegendVerticalAlignment.TOP
            horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
            orientation = Legend.LegendOrientation.VERTICAL
            yOffset = 0f
            xOffset = 10f
            yEntrySpace = 0f
            textSize = 8f
            isEnabled = false
        }

        chart.xAxis.apply {
            position = XAxis.XAxisPosition.BOTTOM
            granularity = 1f
            setCenterAxisLabels(false)
            isEnabled = false
        }

        chart.axisLeft.apply {
            valueFormatter = LargeValueFormatter()
            setDrawGridLines(false)
            spaceTop = 35f
            axisMinimum = 0f // this replaces setStartAtZero(true)
            isEnabled = false
        }

        chart.axisRight.apply {
            isEnabled = false
        }
    }

    fun setupBarChartOverview(chart: BarChart, period: Int){
        chart.apply {
            description.isEnabled = false
            setDrawGridBackground(false)
            data.barWidth = .9f
//            data.groupBars(0f, 1f, 1f)

            if(data!= null && data.dataSets!= null){
                data.dataSets.forEach {
                    it.setDrawValues(false)
//                    val t = it as BarDataSet
                }
            }
            setScaleXEnabled(true)
            setScaleYEnabled(false)
            setPinchZoom(false)
        }

        chart.legend.apply {
            setDrawInside(false)
            verticalAlignment = Legend.LegendVerticalAlignment.TOP
            horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
            orientation = Legend.LegendOrientation.VERTICAL
            yOffset = 0f
            xOffset = 10f
            yEntrySpace = 0f
            textSize = 8f
            isEnabled = false
        }

        chart.xAxis.apply {
            valueFormatter = XValues(period)
            position = XAxis.XAxisPosition.BOTTOM
            granularity = 1f
            setCenterAxisLabels(true)
            setDrawGridLines(true)
            labelCount = 4
            isEnabled = true
        }

        chart.axisLeft.apply {
            valueFormatter = YValues() //LargeValueFormatter()
            setCenterAxisLabels(true)
            setDrawGridLines(true)
//            spaceTop = 35f
//            axisMinimum = 0f // this replaces setStartAtZero(true)
            isEnabled = true
        }

        chart.axisRight.apply {
            isEnabled = false
        }



    }

    class YValues: ValueFormatter(){
        private var mFormat: DecimalFormat? = null
        fun LargeValueFormatter() {
            mFormat = DecimalFormat("####")
        }
    }

    class XValues(val period: Int): ValueFormatter(){
        private var mFormat: DecimalFormat? = null

        fun LargeValueFormatter() {
            mFormat = DecimalFormat("####")
        }
        override fun getFormattedValue(value: Float): String? {
            return when(period){
                0 -> parseThisYear(value.toInt())
                1 -> parseLastYear(value.toInt())
                else -> {""}
            }
        }
        private fun parseThisYear(value: Int): String{
            return when(value){
                0 -> "JAN/21";  1 -> "FEB/21";  2 -> "MAR/21"
                3 -> "APR/21";  4 -> "MAI/21";  5 -> "JUN/21"
                6 -> "JUL/21";  7 -> "AUG/21";  8 -> "SEP/21"
                9 -> "OKT/21"; 10 -> "NOV/21"; 11 -> "DEC/21"
                else -> ""
            }
        }
        private fun parseLastYear(value: Int): String{
            return when(value){
                0 -> "JAN/20";  1 -> "FEB/20";  2 -> "MAR/20"
                3 -> "APR/20";  4 -> "MAI/20";  5 -> "JUN/20"
                6 -> "JUL/20";  7 -> "AUG/20";  8 -> "SEP/20"
                9 -> "OKT/20"; 10 -> "NOV/20"; 11 -> "DEC/20"
                else -> ""
            }
        }

    }

/***   ***/

    fun fetchLineData(): MutableState<MutableList<Entry>> { // LineDataSet {
        val dp = mutableListOf(0, 20)
        val entry = mutableListOf<Entry>()
        while (dp[0] < dp[1]) {
            entry.add(
                Entry(
                    dp[0].toFloat(),
                    Random.nextInt(11, 99).toFloat()
                )
            )
            dp[0]++
        }
        return mutableStateOf(entry) // LineDataSet(entry, label)
    }

    fun fetchBarData(): MutableState<MutableList<BarEntry>> { // LineDataSet {
        val dp = mutableListOf(0, 20)
        val entry = mutableListOf<BarEntry>()
        while(dp[0] < dp[1]){
            entry.add(
                BarEntry(
                dp[0].toFloat(),
                Random.nextInt(21,49).toFloat())
            )
            dp[0]++
        }
        return mutableStateOf(entry) // LineDataSet(entry, label)
    }

    enum class EStatistic {
        EARNING,
        REWARD,
        CASHBACK,
        INVITED
    }

    fun fetchBarOverviewData(period: cards.EPeriod): MutableState<MutableMap<EStatistic, MutableList<BarEntry>>> { // LineDataSet {
        val dp = mutableListOf(0, 12)
        when(period){
            cards.EPeriod.DAY -> dp[1] = 24
            cards.EPeriod.WEEK -> dp[1] = 7
            cards.EPeriod.MONTH -> dp[1] = 31
            cards.EPeriod.YEAR -> dp[1] = 12
        }
        val entry_ea = mutableListOf<BarEntry>()
        val entry_rw = mutableListOf<BarEntry>()
        val entry_cb = mutableListOf<BarEntry>()
        val entry_in = mutableListOf<BarEntry>()
        while(dp[0] < dp[1]){
            entry_ea.add(
                BarEntry(
                    dp[0].toFloat(),
                    Random.nextInt(21,49).toFloat())
            )
            entry_rw.add(
                BarEntry(
                    dp[0].toFloat(),
                    Random.nextInt(21,49).toFloat())
            )
            entry_cb.add(
                BarEntry(
                    dp[0].toFloat(),
                    Random.nextInt(21,49).toFloat())
            )
            entry_in.add(
                BarEntry(
                    dp[0].toFloat(),
                    Random.nextInt(21,49).toFloat())
            )
            dp[0]++
        }
        return mutableStateOf(mutableMapOf(
            Pair(EStatistic.EARNING,  entry_ea),
            Pair(EStatistic.REWARD,   entry_rw),
            Pair(EStatistic.CASHBACK, entry_cb),
            Pair(EStatistic.INVITED,  entry_in)))
    }

}
