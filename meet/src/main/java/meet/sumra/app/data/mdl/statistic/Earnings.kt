package meet.sumra.app.data.mdl.statistic

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import meet.sumra.app.R
import meet.sumra.app.data.configs.setupStatisticCharts

data class Earnings(
    val cardManyAll: Float = 0F,   // 2597.45F,
    val cardLineChart: /*LineDataSet*/ MutableState<MutableList<Entry>> = setupStatisticCharts.fetchLineData(), //LineChart(requireContext()) , //.data = ,
    val cardLabel: Int = R.string.statisticCardGlobalEarnings,
    val cardManyMonth: Float = 0F, // 254.45F,
    val cardPercent: Int = 0       // -2
)
