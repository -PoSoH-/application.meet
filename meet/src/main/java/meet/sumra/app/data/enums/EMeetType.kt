package meet.sumra.app.data.enums

enum class EMeetType{
    GROUP,
    PAIR,
    NIT
}