package meet.sumra.app.data.mdl.statistic

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import meet.sumra.app.data.configs.setupStatisticCharts

data class RentPay(
    val cardManyAll  : Float = 0.0F, // 3241.14F,
    val cardLineChart: MutableState<MutableList<Entry>> = setupStatisticCharts.fetchLineData(),
    val cardManyMonth: Float = 0.0F, // 251.21,
    val cardPercent  : Int   = 12    // 12
)
