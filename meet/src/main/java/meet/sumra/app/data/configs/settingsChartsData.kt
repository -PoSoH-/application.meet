package meet.sumra.app.data.configs

import android.graphics.DashPathEffect
import androidx.compose.runtime.MutableState
import androidx.compose.ui.graphics.Color
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import meet.sumra.app.data.mdl.earnings.cards
import meet.sumra.app.data.mdl.plaza.*
import kotlin.random.Random

object settingsChartsData {

    fun getDivitsRewardStatistic(period: cards.EPeriod): RewardStatistic {
        val labels = arrayListOf(
            Legend("12345", legendTitle = "Commercial activity", legendColor = Color(0xffFFAB00)),
            Legend("23456", legendTitle = "Download our mobile apps", legendColor = Color(0xff38CB89)),
            Legend("34567", legendTitle = "Referrals balance paid-out", legendColor = Color(0xffFF5630)),
            Legend("45678", legendTitle = "Register at Sumra ID", legendColor = Color(0xff377DFF))
        )
        return RewardStatistic(
            title = "Total Divits Balance",
            totalBalance = TotalBalance(value = "4,300", currency = "DR"),
            labels = labels,
            charts = when (period) {
                cards.EPeriod.DAY   -> { generatorChartDay  (labels = labels) }
                cards.EPeriod.WEEK  -> { generatorChartWeek (labels = labels) }
                cards.EPeriod.MONTH -> { generatorChartMonth(labels = labels) }
                cards.EPeriod.YEAR  -> { generatorChartYear (labels = labels) }
            }
        )
    }

    fun generatorChartDay(labels: ArrayList<Legend>): ArrayList<ChartValue>{
        val max = 24
        val index = mutableListOf(0)
        val chartValues = mutableListOf<ChartValue>()
        while (index[0] < max){
            chartValues.add(ChartValue(generatorChartPart(legends = labels)))
            index[0]++
        }
        return chartValues as ArrayList<ChartValue>
    }

    fun generatorChartWeek(labels: ArrayList<Legend>): ArrayList<ChartValue>{
        val max = 7
        val index = mutableListOf(0)
        val chartValues = mutableListOf<ChartValue>()
        while (index[0] < max){
            chartValues.add(ChartValue(generatorChartPart(legends = labels)))
            index[0]++
        }
        return chartValues as ArrayList<ChartValue>
    }

    fun generatorChartMonth(labels: ArrayList<Legend>): ArrayList<ChartValue>{
        val max = 31
        val index = mutableListOf(0)
        val chartValues = mutableListOf<ChartValue>()
        while (index[0] < max){
            chartValues.add(ChartValue(generatorChartPart(legends = labels)))
            index[0]++
        }
        return chartValues as ArrayList<ChartValue>
    }

    fun generatorChartYear(labels: ArrayList<Legend>): ArrayList<ChartValue>{
        val max = 12
        val index = mutableListOf(0)
        val chartValues = mutableListOf<ChartValue>()
        while (index[0] < max){
            chartValues.add(ChartValue(generatorChartPart(legends = labels)))
            index[0]++
        }
        return chartValues as ArrayList<ChartValue>
    }

    fun generatorChartPart(legends: ArrayList<Legend>): ArrayList<ChartPart>{
        val parts = arrayListOf<ChartPart>()
        legends.forEach {
            parts.add(ChartPart(labelId = it.id, value = Random.nextInt(10, 50).toFloat()))
        }
        return parts
    }

    fun convertToBarData(cd: RewardStatistic): BarData {
        val values = java.util.ArrayList<BarEntry>()
        cd.charts.forEachIndexed { index, chartdata ->
            val tmpVal = mutableListOf<Float>()
            chartdata.value.forEach{
                tmpVal.add(it.value)
            }
            values += BarEntry(index.toFloat(), tmpVal.toFloatArray())
        }
        val set1: BarDataSet
        set1 = BarDataSet(values, "Total Divits balance...")
        set1.setDrawIcons(false)
//        val colors = mutableListOf<Int>()
//        cd.labels.forEach {
//            colors.add (it.legendColor.toArgb()) // as android.graphics.Color )
//        }
        val labels = mutableListOf<String>()
        cd.labels.forEach {
            labels.add ( it.legendTitle)
        }
//        set1.setColors(colors)
        set1.stackLabels = labels.toTypedArray()
        val dataSets = java.util.ArrayList<IBarDataSet>()
        dataSets.add(set1)
        val data = BarData(dataSets)
//        data.setValueFormatter(MyValueFormatter())
        data.setValueTextColor(android.graphics.Color.BLACK)
        return data
//        if (chart.getData() != null &&
//            chart.getData().getDataSetCount() > 0
//        ) {
//            set1 = chart.getData().getDataSetByIndex(0)
//            set1.values = values
//            chart.getData().notifyDataChanged()
//            chart.notifyDataSetChanged()
//        } else {
//            set1 = BarDataSet(values, "Statistics Vienna 2014")
//            set1.setDrawIcons(false)
//            set1.setColors(*getColors())
//            set1.stackLabels = arrayOf("Births", "Divorces", "Marriages")
//            val dataSets = java.util.ArrayList<IBarDataSet>()
//            dataSets.add(set1)
//            val data = BarData(dataSets)
//            data.setValueFormatter(MyValueFormatter())
//            data.setValueTextColor(android.graphics.Color.WHITE)
//            chart.setData(data)
//        }
    }

    fun getDataForLineChartSecTokenChart(count: MutableState<Int>, color: Int): LineData{
        val chartValues = mutableListOf<Entry>()
        val period = cards.EPeriod.byCount(count.value)
        when (period) {
            cards.EPeriod.DAY   -> {
                val max = 24; val index = mutableListOf(0)
                while (index[0] < max){
                    chartValues.add(Entry(
                        index[0].toFloat(), Random.nextInt(10, 99).toFloat()))
                    index[0]++
                }
            }
            cards.EPeriod.WEEK  -> {
                val max = 7; val index = mutableListOf(0)
                while (index[0] < max){
                    chartValues.add(Entry(
                        index[0].toFloat(), Random.nextInt(10, 99).toFloat()))
                    index[0]++
                }
            }
            cards.EPeriod.MONTH -> {
                val max = 31
                val index = mutableListOf(0)
                while (index[0] < max){
                    chartValues.add(Entry(
                        index[0].toFloat(), Random.nextInt(10, 99).toFloat()))
                    index[0]++
                }
            }
            cards.EPeriod.YEAR  -> {
                val max = 12; val index = mutableListOf(0)
                while (index[0] < max){
                    chartValues.add(Entry(
                        index[0].toFloat(), Random.nextInt(10, 99).toFloat()))
                    index[0]++
                }
            }
        }

        val dts = LineDataSet(chartValues, "Divits token chart")
        dts.setDrawIcons(false)
        dts.enableDashedLine(10f, 5f, 0f)

        dts.color = color
        dts.setCircleColor(color)

        dts.lineWidth = 1f
        dts.circleRadius = 3f
        dts.setDrawCircleHole(true)

        // customize legend entry
        dts.formLineWidth = 1f
        dts.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
        dts.formSize = 15f

        // text size of values
        dts.valueTextSize = 9f

        // draw selection line as dashed
        dts.enableDashedHighlightLine(10f, 5f, 0f)

        // set the filled area
        dts.setDrawFilled(false)
//        dts.fillFormatter = IFillFormatter { dataSet, dataProvider -> chart.axisLeft.axisMinimum }

        // set color of filled area
//        if (Utils.getSDKInt() >= 18) {
//            // drawables only supported on api level 18 and above
//            val drawable = ContextCompat.getDrawable(this, R.drawable.fade_red)
//            set1.fillDrawable = drawable
//        } else {
        dts.fillColor = color
        val dataSets = java.util.ArrayList<ILineDataSet>()
        dataSets.add(dts) // add the data sets

        // create a data object with the data sets
        return LineData(dataSets)
    }



    fun getDataForBarChartSecTokenChart(): List<BarDataSet> {

        val groupSpace = 0.08f
        val barSpace = 0.03f   // x4 DataSet
        val barWidth = 0.2f    // x4 DataSet

        // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"
        // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"

//        val groupCount: Int = seekBarX.getProgress() + 1
        val startYear = 2020
        val endYear   = startYear + 1

        val barValA = mutableListOf<BarEntry>()
        val barValB = mutableListOf<BarEntry>()

        val max = 12;
        val index = mutableListOf(0)

        while (index[0] < max) {
            barValA.add(BarEntry(index[0].toFloat(), Random.nextInt(10, 99).toFloat()))
            barValB.add(BarEntry(index[0].toFloat(), Random.nextInt(10, 99).toFloat()))
            index[0]++
        }

        val bdsStart = BarDataSet(barValA, "2020")
        val bdsFinal = BarDataSet(barValB, "2021")

        return mutableListOf(bdsStart, bdsFinal)
    }

//
//    fun setupLineChartStatistic(chart: LineChart){
//
//    }
//
//    fun setupBarChartStatistic(chart: BarChart){
//
//    }

}


