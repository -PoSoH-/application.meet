package meet.sumra.app.data.mdl.plaza

import java.util.*

data class PlazaTestMeeting(
    val id: String = UUID.randomUUID().toString()
)
