package meet.sumra.app.ext

import androidx.lifecycle.MutableLiveData

internal fun <T : Any?> MutableLiveData<T>.default(initialValue: T) = apply {setValue(initialValue)}
internal fun <T> MutableLiveData<T>.set(newValue: T) = apply {setValue(newValue)}