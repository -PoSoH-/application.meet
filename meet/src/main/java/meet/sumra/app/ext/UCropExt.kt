package meet.sumra.app.ext

import android.content.Context
import android.graphics.Color
import android.net.Uri
import androidx.core.content.ContextCompat
import com.yalantis.ucrop.UCrop
import com.yalantis.ucrop.UCropActivity
import meet.sumra.app.R
import meet.sumra.app.utils.setup.ThemeUtils

fun createUCropWithDefaultSettings(context: Context,
                                   source: Uri,
                                   destination: Uri,
                                   toolbarTitle: String?): UCrop {
    return UCrop.of(source, destination)
        .withOptions(
            UCrop.Options()
                .apply {
                    setAllowedGestures(
                        /* tabScale       = */ UCropActivity.SCALE,
                        /* tabRotate      = */ UCropActivity.ALL,
                        /* tabAspectRatio = */ UCropActivity.SCALE
                    )
                    setToolbarTitle(toolbarTitle)
                    // Disable freestyle crop, usability was not easy
                    // setFreeStyleCropEnabled(true)
                    // Color used for toolbar icon and text
                    setToolbarColor(ThemeUtils.getColor(context, R.attr.background))

                    setToolbarWidgetColor(ThemeUtils.getColor(context, R.attr.colorPrimaryDark))
                    // Background
                    setRootViewBackgroundColor(ThemeUtils.getColor(context, R.attr.colorOnBackground))
                    // Status bar color (pb in dark mode, icon of the status bar are dark)
                    setStatusBarColor(ThemeUtils.getColor(context, R.attr.actionMenuTextColor))
                    // Known issue: there is still orange color used by the lib
                    // https://github.com/Yalantis/uCrop/issues/602
                    setActiveControlsWidgetColor(ThemeUtils.getColor(context, R.attr.cardBackgroundColor))
                    // Hide the logo (does not work)
                    setLogoColor(Color.TRANSPARENT)
                }
        )
}