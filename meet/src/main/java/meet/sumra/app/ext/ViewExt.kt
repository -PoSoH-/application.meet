package meet.sumra.app.ext

import android.annotation.SuppressLint
import android.text.Editable
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.google.android.material.snackbar.Snackbar
import jp.wasabeef.glide.transformations.CropSquareTransformation
import jp.wasabeef.glide.transformations.RoundedCornersTransformation
import meet.sumra.app.R
import meet.sumra.app.data.enums.EAvatarType
import java.lang.StringBuilder
import kotlin.random.Random


private const val MIN_SNACKBAR_DURATION = 2000
private const val MAX_SNACKBAR_DURATION = 8000
private const val DURATION_PER_LETTER = 50

fun View.showOptimizedSnackbar(message: String) {
    Snackbar.make(this, message, getDuration(message)).show()
}

private fun getDuration(message: String): Int {
    return (message.length * DURATION_PER_LETTER).coerceIn(MIN_SNACKBAR_DURATION, MAX_SNACKBAR_DURATION)
}

internal fun EditText.editEmpty() {
    this.text = Editable.Factory.getInstance().newEditable("")
}
internal fun EditText.putString(text: CharSequence) {
    this.text = Editable.Factory.getInstance().newEditable(text)
}
internal fun TextView.hideScaleText() {
    this.text       = ""
    this.visibility = View.INVISIBLE
    this.animation  = AnimationUtils.loadAnimation(context, R.anim.anim_hide_with_right_to_left)
    this.animate()
}
internal fun TextView.switchBarText(newText: String) {
    this.visibility = View.INVISIBLE
    this.animation  = AnimationUtils.loadAnimation(context, R.anim.anim_hide_with_right_to_left)
    this.animate()
    this.text       = newText
    this.visibility = View.VISIBLE
    this.animation  = AnimationUtils.loadAnimation(context, R.anim.anim_show_with_left_to_right)
    this.animate()
}

@SuppressLint("CheckResult")
internal fun ImageView.loadUrl(url: String?, type: EAvatarType = EAvatarType.RADIUS(10f)) {
    Glide.with(context).load(url).apply {
            when(type){
                is EAvatarType.RADIUS -> {
                    transform(RoundedCornersTransformation(
                        type.value.toInt(),
                        0,
                        RoundedCornersTransformation.CornerType.ALL))
                }
                is EAvatarType.CIRCLE -> {
                    transform(CircleCrop())
                }
                is EAvatarType.SQUARE -> {
                    transform(CropSquareTransformation())
                }
            }
        }
        .placeholder(R.drawable.ic_placeholder)
        .into(this)
}

@SuppressLint("CheckResult")
internal fun ImageView.loadRes(res: Int?, type: EAvatarType = EAvatarType.RADIUS(10f)) {
    Glide.with(context)
        .load(res).apply {
            placeholder(R.drawable.ic_placeholder)
            when(type){
                is EAvatarType.SQUARE -> {
                    transform(CropSquareTransformation())
                }
                is EAvatarType.RADIUS -> {
                    transform(RoundedCornersTransformation(
                        (type as EAvatarType.RADIUS).value.toInt(),
                        0,
                        RoundedCornersTransformation.CornerType.ALL))
                }
                is EAvatarType.CIRCLE -> {
                    transform(CircleCrop())
                }
            }
        }.into(this)
}

internal fun ImageView.convertToBase64(): String {
    return ""
}

internal fun TextView.generateName(count: Int = 4): String {
    val tmp_a = StringBuilder()
    var tmp_b = count
    val data = context.resources.getStringArray(R.array.test_alphabet_lowercase)
    while(tmp_b > 0) {
        tmp_a.append(data[Random.nextInt(0, data.size - 1)])
        tmp_b--
    }
    return "${text}${tmp_a}"
}