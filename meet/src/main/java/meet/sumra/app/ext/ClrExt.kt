package meet.sumra.app.ext

import android.graphics.Color
import androidx.annotation.ColorInt

class ClrExt {
    companion object{
        @ColorInt val color_blue_400 = Color.parseColor("#ff2188ff")
        @ColorInt val color_blue_500_op20 = Color.parseColor("#320366D6")
        @ColorInt val color_blue_800 = Color.parseColor("#ff032F62")

        @ColorInt val color_gray_200 = Color.parseColor("#ff032F62")

        @ColorInt val color_gold_l = Color.parseColor("#ffFADB70")
        @ColorInt val color_gold_d = Color.parseColor("#ffF1C736")

        @ColorInt
        val RED = -0x10000
    }
}