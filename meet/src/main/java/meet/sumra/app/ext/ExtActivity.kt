package meet.sumra.app.ext

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.snackbar.Snackbar
import meet.sumra.app.BuildConfig
import meet.sumra.app.R
import meet.sumra.app.frg.FragmentBase
import net.sumra.auth.helpers.libBundle.toLibraryBundle
import sdk.net.meet.LogUtil

fun Fragment.registerStartForActivityResult(onResult: (ActivityResult) -> Unit): ActivityResultLauncher<Intent> {
    return registerForActivityResult(ActivityResultContracts.StartActivityForResult(), onResult)
}

internal fun FragmentActivity.addFragment(
    frameId: Int,
    fragment: FragmentBase<*>,
    allowStateLoss: Boolean = false
) {
    supportFragmentManager.commitTransaction(allowStateLoss) {
        setCustomAnimations(R.anim.anim_show_d_to_u, R.anim.anim_hide_d_to_u)
        add(frameId, fragment)
    }
}

internal fun FragmentActivity.addFragment(
    frameId: Int,
    fragment: Fragment,
    allowStateLoss: Boolean = false
) {
    supportFragmentManager.commitTransaction(allowStateLoss) {
        setCustomAnimations(R.anim.anim_show_d_to_u, R.anim.anim_hide_d_to_u)
        add(frameId, fragment)
    }
}

internal fun <T: FragmentBase<*>> FragmentActivity.addFragment(
    frameId: Int,
    fragmentClass: Class<T>,
    params: Parcelable? = null,
    tag: String? = null,
    allowStateLoss: Boolean = false
) {
    supportFragmentManager.commitTransaction(allowStateLoss) {
        add(frameId, fragmentClass, params.toLibraryBundle(), tag)
    }
}

internal fun FragmentActivity.replaceFragment(
    frameId: Int,
    fragment: FragmentBase<*>,
    tag: String? = null,
    allowStateLoss: Boolean = false
) {
    supportFragmentManager.commitTransaction(allowStateLoss) {
//        val frg = (fragment as FragmentBase<*>)
//        (fragment as FragmentBase<*>).getExitAnimation()
        setCustomAnimations(fragment.getEnterAnimation(), fragment.getExitAnimation())
        replace(frameId, fragment, tag) }
}
internal fun FragmentActivity.replaceFragment(
    frameId: Int,
    fragment: Fragment,
    tag: String? = null,
    allowStateLoss: Boolean = false
) {
    supportFragmentManager.commitTransaction(allowStateLoss) {
        setCustomAnimations(R.anim.anim_show_r_to_l, R.anim.anim_hide_r_to_l)
        replace(frameId, fragment, tag) }
}

internal fun <T : FragmentBase<*>> FragmentActivity.replaceFragment(
    frameId: Int,
    fragmentClass: Class<T>,
    params: Parcelable? = null,
    tag: String? = null,
    allowStateLoss: Boolean = false
) {
    supportFragmentManager.commitTransaction(allowStateLoss) {
        setCustomAnimations(R.anim.anim_show_d_to_u, R.anim.anim_hide_d_to_u)
        replace(frameId, fragmentClass, params.toLibraryBundle(), tag)
    }
}

internal fun FragmentActivity.addFragmentToBackStack(
    frameId: Int,
    fragment: FragmentBase<*>,
    tag: String? = null,
    allowStateLoss: Boolean = false
) {
    supportFragmentManager.commitTransaction(allowStateLoss) {
        setCustomAnimations(fragment.getEnterAnimation(), fragment.getExitAnimation())
        replace(frameId, fragment).addToBackStack(tag)
    }
}
internal fun FragmentActivity.addFragmentToBackStack(
    frameId: Int,
    fragment: Fragment,
    tag: String? = null,
    allowStateLoss: Boolean = false
) {
    supportFragmentManager.commitTransaction(allowStateLoss) {
        setCustomAnimations(R.anim.anim_show_d_to_u, R.anim.anim_hide_d_to_u)
        replace(frameId, fragment).addToBackStack(tag)
    }
}

internal fun <T : Fragment> FragmentActivity.addFragmentToBackStack(frameId: Int,
                                                                           fragmentClass: Class<T>,
                                                                           params: Parcelable? = null,
                                                                           tag: String? = null,
                                                                           allowStateLoss: Boolean = false,
                                                                           option: ((FragmentTransaction) -> Unit)? = null) {
    supportFragmentManager.commitTransaction(allowStateLoss) {
        option?.invoke(this)
        setCustomAnimations(R.anim.anim_show_d_to_u, R.anim.anim_hide_d_to_u)
        replace(frameId, fragmentClass, params.toLibraryBundle(), tag).addToBackStack(tag)
    }
}

internal fun FragmentActivity.backFragment(
    tag: String? = null,
) {
    supportFragmentManager.popBackStack() // popBackStack()
}

internal fun FragmentActivity.removeFragment(fragment: Fragment) {
    supportFragmentManager.commitTransaction {
        setCustomAnimations(R.anim.anim_show_u_to_d, R.anim.anim_hide_u_to_d)
        remove(fragment)
    }
}

fun FragmentActivity.hideKeyboard() {
    currentFocus?.hideKeyboard()
}

fun View.hideKeyboard(){
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

fun Activity.restart() {
    startActivity(intent)
    finish()
}

//fun Context.logI(tag: String = "ACTIVITY HOME", text: String) {
//    if(BuildConfig.DEBUG){
//        LogUtil.info("TAG:: ${tag}", "MES:: $text")}
//}
//fun Context.logE(tag: String = "ACTIVITY HOME", text: String) {
//    if(BuildConfig.DEBUG){LogUtil.error("TAG:: ${tag}", "MES:: $text")}
//}
