package meet.sumra.app.ext

import android.view.View
import android.view.animation.AnimationUtils
import android.widget.EditText
import androidx.core.content.res.TypedArrayUtils.getText
import meet.sumra.app.R
import meet.sumra.app.ext.ExtAnimation.showDtoU

object ExtAnimation {

    fun View.switchView(type: Boolean){
        if(type) {
            this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_show_d_to_u)
            this.animate()
            this.visibility = View.VISIBLE
        }else{
            this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_hide_u_to_d)
            this.animate()
            this.visibility = View.GONE
        }
    }

    fun View.showDtoU(){
        this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_show_d_to_u)
        this.animate()
        this.visibility = View.VISIBLE
    }

    fun View.hideDtoU(){
        this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_hide_d_to_u)
        this.animate()
        this.visibility = View.GONE
    }

    fun View.showUtoD(){
        this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_show_u_to_d)
        this.animate()
        this.visibility = View.VISIBLE
    }

    fun View.hideUtoD(){
        this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_hide_u_to_d)
        this.animate()
        this.visibility = View.GONE
    }

    fun View.showScaleWithCenterToOut(){
        this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_show_c_to_o)
        this.animate()
        this.visibility = View.VISIBLE
    }

    fun View.hideScaleWithOutToCenter(){
        this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_hide_o_to_c)
        this.animate()
        this.visibility = View.GONE
    }

    fun View.showScaleTopRBottomL(){
        this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_show_top_r_bottom_l_trans_scale)
        this.animate()
        this.visibility = View.VISIBLE
    }

    fun View.hideScaleBottomLTopR(){
        this.animation = AnimationUtils.loadAnimation(context, R.anim.anim_hide_bottm_l_top_r_trans_scale)
        this.animate()
        this.visibility = View.GONE
    }

    fun EditText.textTrim(): String {
        return text.toString().trim(' ')
    }
}