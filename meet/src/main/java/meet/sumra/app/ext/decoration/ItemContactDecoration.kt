package meet.sumra.app.ext.decoration

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.core.view.children
import androidx.recyclerview.widget.RecyclerView

class ItemContactDecoration constructor(context: Context/*, resId: Int*/) : RecyclerView.ItemDecoration() {

    companion object {
        private val ATTRS = intArrayOf(android.R.attr.listDivider)
    }

    private var divider: Drawable? = null

    constructor(context: Context, resId: Int) : this(context) {
        divider = ContextCompat.getDrawable(context, resId)!!;
    }

    init {
        val styledAttributes: TypedArray = context.obtainStyledAttributes(ATTRS);
        if(divider == null) {
            divider = styledAttributes.getDrawable(0);
            styledAttributes.recycle();
        }
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val left      : Int = parent.getPaddingLeft();
        val right     : Int = parent.getWidth() - parent.getPaddingRight();
        parent.children.forEachIndexed {i, v ->
            v.apply {
                val params = layoutParams  as RecyclerView.LayoutParams

                val top    = getBottom() + params.bottomMargin;
                val bottom = top + divider!!.getIntrinsicHeight();
                divider!!.setBounds(left, top, right, bottom);
                divider!!.draw(c);
            }
        }
//        super.onDraw(c, parent, state)
    }
}