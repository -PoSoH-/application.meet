package meet.sumra.app.ext

import androidx.fragment.app.FragmentTransaction

inline fun androidx.fragment.app.FragmentManager.commitTransaction(
        allowStateLoss: Boolean = false,
        func: FragmentTransaction.() -> FragmentTransaction) {

    val transaction = beginTransaction().func()
    if (allowStateLoss) {
        transaction.commitAllowingStateLoss()
    } else {
        transaction.commit()
    }

}