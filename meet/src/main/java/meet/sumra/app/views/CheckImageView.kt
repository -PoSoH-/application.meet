package meet.sumra.app.views

import android.R
import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.widget.Checkable
import androidx.appcompat.widget.AppCompatImageView

@SuppressLint("ViewConstructor")
class CheckImageView @JvmOverloads constructor(context: Context,
                      defStyleAttr: Int = 0,
                      attrs: AttributeSet?): AppCompatImageView(context, attrs, defStyleAttr), Checkable {

    companion object {
        private val CHECKED_STATE_SET = intArrayOf(R.attr.state_checked)
    }

    private var mChecked = false

    fun refreshStateList() {

    }

//    override fun onCreateDrawableState(extraSpace: Int): IntArray {
//        val drawableState: IntArray = super.onCreateDrawableState(extraSpace + 1)
//        if (isChecked) mergeDrawableStates(drawableState, CHECKED_STATE_SET)
//        return drawableState
//    }

    override fun toggle() {
        isChecked = !mChecked
    }

    override fun isChecked(): Boolean {
        return mChecked
    }

    override fun setChecked(checked: Boolean) {
        if (mChecked == checked) return
        mChecked = checked
        refreshDrawableState()
    }
}
