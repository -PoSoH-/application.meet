package meet.sumra.app.views.compose

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.layout.ScaleFactor
import androidx.compose.ui.layout.lerp
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.*
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.pager.*
import meet.sumra.app.R
import meet.sumra.app.adptr.AdapterSelectDateTime
import meet.sumra.app.views.compose.ui_theme.ThemeSetup
import kotlin.math.absoluteValue

class NewMeetPagerSelectorsViewHelpers() {

    interface CurrentStateListener{
        fun currentState(st: Int)
    }

    companion object {
        val headerTxtCrd =
            ThemeSetup.getTheme().cNewMeetColors.get(ThemeSetup.NewMeetKey.txtCrdLabel)?.let {
                TextStyle(
                    fontSize = 13.sp,
                    fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W700)),
                    color = it
                )
            }
        val headerTxtSec =
            ThemeSetup.getTheme().cNewMeetColors.get(ThemeSetup.NewMeetKey.txtSecLabel)?.let {
                TextStyle(
                    fontSize = 13.sp,
                    fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W700)),
                    color = it
                )
            }
        val cardTxtEnb =
            ThemeSetup.getTheme().cNewMeetColors.get(ThemeSetup.NewMeetKey.txtEnbLabel)?.let {
                TextStyle(
                    fontSize = 13.sp,
                    fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W400)),
                    textAlign = TextAlign.Center,
                    color = it
                )
            }
        val cardTxtDsb =
            ThemeSetup.getTheme().cNewMeetColors.get(ThemeSetup.NewMeetKey.txtDsbLabel)?.let {
                TextStyle(
                    fontSize = 11.sp,
                    fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W400)),
                    textAlign = TextAlign.Center,
                    color = it
                )
            }

        private val cardPageHeight = 22.dp

        @ExperimentalPagerApi
        @Composable
        fun GetViewIncludeSelectY(_y: MutableState<Int>) {
            Column(
                verticalArrangement = Arrangement.Center,
                modifier =  Modifier
                    .fillMaxWidth()
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.weight(1f),
                ) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center,
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(0.dp, 0.dp, 4.dp, 0.dp),
                    ) {
                        Text(
                            text = "Year:",
                            modifier = Modifier.padding(0.dp, 0.dp, 0.dp, 8.dp),
                            style = headerTxtSec!! //TextStyle(fontSize = 12.sp, color = Color.Gray)
                        )
                        Text(
                            text = "${_y.value}",
                            style = cardTxtEnb!! //TextStyle(fontSize = 12.sp, color = Color.LightGray)
                        )
                    }

                }
            }
        }
/**   000===000===000===000===000===000===000===000===000===000===000===000===000===000===000   **/
        @ExperimentalPagerApi
        @Composable
        fun GetViewIncludeSelectM(_m: MutableMap<Int, AdapterSelectDateTime.Month>
                                  , _cM: Int
                                  , _st: CurrentStateListener) {
            val pagerM = rememberPagerState(
                pageCount = _m.size,
                initialOffscreenLimit = 2,
                infiniteLoop = true,
                initialPage = _cM
            )
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
                modifier = Modifier.fillMaxSize()
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .weight(1f)
                        .fillMaxHeight(),
                ) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center,
                        modifier = Modifier
                            .padding(4.dp, 0.dp, 4.dp, 0.dp)
                            .width(150.dp)
                            .fillMaxHeight()
                    ) {
                        Text(
                            text = "Month:",
                            style = headerTxtSec!!
                        )
                        HorizontalPager(
                            horizontalAlignment = Alignment.CenterHorizontally,
                            state = pagerM,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(top = 8.dp)
                                .height(cardPageHeight)
                        ) { page ->
                            _st.currentState(page)
                            Card(
                                backgroundColor = ThemeSetup.getTheme().cNewMeetCardColor[0],
                                shape = RoundedCornerShape(8.dp), // MaterialTheme.shapes.small,
                                modifier = Modifier
                                    .width(44.dp)
                                    .height(cardPageHeight)
                                    .graphicsLayer {
                                        val pageOffset =
                                            calculateCurrentOffsetForPage(page).absoluteValue
                                        lerp(
                                            start = ScaleFactor(0.85f, .85f),
                                            stop = ScaleFactor(1.0f, 1.0f),
                                            fraction = 1f - pageOffset.coerceIn(0f, 1f)
                                        ).also { scale ->
                                            scaleX = scale.scaleX
                                            scaleY = scale.scaleY
                                        }
                                        alpha = lerp(
                                            start = ScaleFactor(0.5f, 0.5f),
                                            stop = ScaleFactor(1.0f, 1.0f),
                                            fraction = 1f - pageOffset.coerceIn(0f, 1f)
                                        ).scaleX
                                    },
                                elevation = 2.dp
                            ) {
                                Column(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .fillMaxHeight(),
                                    horizontalAlignment = Alignment.CenterHorizontally,
                                    verticalArrangement = Arrangement.Center
                                ) {
                                    Text(
                                          text = "${_m[page]!!.mTxtLow}"    //"${months[page].name}",
                                        , modifier = Modifier.padding(3.dp)
                                        , style = cardTxtEnb!!
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
        /**  ======================================================================================  **/
        @ExperimentalPagerApi
        @Composable
        fun GetViewIncludeSelectD(_d: MutableState<MutableList<String>>,
                                  _cD: Int,
                                  _st: CurrentStateListener) {
            val pagerD = rememberPagerState(
                pageCount = _d.value.size,
                initialOffscreenLimit = 2,
                infiniteLoop = true,
                initialPage = _cD-1
            )
            Column(
                horizontalAlignment = Alignment.End,
                verticalArrangement = Arrangement.Center,
                modifier = Modifier.fillMaxSize()
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .weight(1f)
                        .fillMaxHeight(),
                ) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center,
                        modifier = Modifier
                            .padding(4.dp, 0.dp, 0.dp, 0.dp)
                            .width(96.dp)
                            .fillMaxHeight()
                    ) {
                        Text(
                            text = "Day:",
                            style = headerTxtSec!! //TextStyle(fontSize = 12.sp, color = Color.LightGray)
                        )
                        HorizontalPager(
                            state = pagerD,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(top = 8.dp)
                                .height(cardPageHeight)
                        ) { page ->
                            _st.currentState(_d.value[page].toInt())
                            Card(
                                backgroundColor = ThemeSetup.getTheme().cNewMeetCardColor[0],
                                shape = RoundedCornerShape(8.dp), // MaterialTheme.shapes.small,
                                modifier = Modifier
                                    .width(32.dp)
                                    .height(cardPageHeight)
                                    .graphicsLayer {
                                        val pageOffset =
                                            calculateCurrentOffsetForPage(page).absoluteValue
                                        lerp(
                                            start = ScaleFactor(0.75f, .85f),
                                            stop = ScaleFactor(1.0f, 1.0f),
                                            fraction = 1f - pageOffset.coerceIn(0f, 1f)
                                        ).also { scale ->
                                            scaleX = scale.scaleX
                                            scaleY = scale.scaleY
                                        }
                                        alpha = lerp(
                                            start = ScaleFactor(0.5f, 0.5f),
                                            stop = ScaleFactor(1.0f, 1.0f),
                                            fraction = 1f - pageOffset.coerceIn(0f, 1f)
                                        ).scaleX
                                    },
                                elevation = 2.dp
                            ) {
                                Column(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .fillMaxHeight(),
                                    horizontalAlignment = Alignment.CenterHorizontally,
                                    verticalArrangement = Arrangement.Center
                                ) {
                                    Text(
                                          text = "${_d.value[page]}"    //"${months[page].name}",
                                        , modifier = Modifier.padding(3.dp)
                                        , style = cardTxtEnb!!
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
        /**  ======================================================================================  **/
        @ExperimentalPagerApi
        @Composable
        fun getViewIncludeSelectHour(_h: MutableList<AdapterSelectDateTime.Hour>,
                                     _cH: String,
                                     _st: CurrentStateListener) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
                modifier = Modifier.fillMaxSize()
            ) {
                var currentPage = 0
                _h.forEachIndexed { cI, cP ->
                    if(_cH.equals(cP.timeLow)){
                        currentPage = cI
                    }
                }
                val pagerM = rememberPagerState(
                    pageCount = _h.size,
                    initialOffscreenLimit = 2,
                    infiniteLoop = true,
                    initialPage = currentPage
                )
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .weight(1f)
                        .fillMaxHeight(),
                ) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center,
                        modifier = Modifier
                            .padding(4.dp, 0.dp, 4.dp, 0.dp)
                            .width(200.dp)
                            .fillMaxHeight()
                    ) {
                        Text(
                            text = "Hour:",
                            style = headerTxtSec!! // TextStyle(fontSize = 12.sp, color = Color.LightGray)
                        )
                        HorizontalPager(
                            horizontalAlignment = Alignment.CenterHorizontally,
                            state = pagerM,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(top = 8.dp)
                                .height(cardPageHeight)
                        ) { page ->
                            _st.currentState(_h[page].timeLow.toInt())
                            Card(
                                backgroundColor = ThemeSetup.getTheme().cNewMeetCardColor[0],
                                shape = RoundedCornerShape(8.dp), // MaterialTheme.shapes.small,
                                modifier = Modifier
                                    .width(48.dp)
                                    .height(cardPageHeight)
                                    .graphicsLayer {
                                        val pageOffset =
                                            calculateCurrentOffsetForPage(page).absoluteValue
                                        lerp(
                                            start = ScaleFactor(0.75f, .85f),
                                            stop = ScaleFactor(1.0f, 1.0f),
                                            fraction = 1f - pageOffset.coerceIn(0f, 1f)
                                        ).also { scale ->
                                            scaleX = scale.scaleX
                                            scaleY = scale.scaleY
                                        }
                                        alpha = lerp(
                                            start = ScaleFactor(0.5f, 0.5f),
                                            stop = ScaleFactor(1.0f, 1.0f),
                                            fraction = 1f - pageOffset.coerceIn(0f, 1f)
                                        ).scaleX
                                    }
                                , elevation = 2.dp
                            ) {
                                Column(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .fillMaxHeight(),
                                    horizontalAlignment = Alignment.CenterHorizontally,
                                    verticalArrangement = Arrangement.Center
                                ) {
//                                    mCurM = page
                                    Text(
                                        text = "${_h[page]!!.timeLow}",    //"${months[page].name}",
                                        modifier = Modifier.padding(3.dp),
                                        style = cardTxtEnb!!
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
        /**  ======================================================================================  **/
        @ExperimentalPagerApi
        @Composable
        fun GetViewIncludeSelectMin(_min: MutableList<AdapterSelectDateTime.Minute>,
                                    _cM: Int,
                                    _st: CurrentStateListener) {
            Column(
                horizontalAlignment = Alignment.End,
                verticalArrangement = Arrangement.Center,
                modifier = Modifier.fillMaxSize()
            ) {
                var curPosition = 0
                _min.forEachIndexed { cI, minute ->
                    var cP = 0
                    if(cI == 0) { cP = _min.size - 1 }
                    else if(cI == _min.size-1) { cP = 0 }
                    else {cP = cI+1}
                    if(minute.minLow.toInt() < _cM && _cM < _min[cP].minLow.toInt()){
                        curPosition = cI
                    }
                }
                val pagerM = rememberPagerState(
                    pageCount = _min.size,
                    initialOffscreenLimit = 2,
                    infiniteLoop = true,
                    initialPage = curPosition
                )
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .weight(1f)
                        .fillMaxHeight(),
                ) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center,
                        modifier = Modifier
                            .padding(4.dp, 0.dp, 4.dp, 0.dp)
                            .width(200.dp)
                            .fillMaxHeight()
                    ) {
                        Text(
                            text = "Minute:",
                            style = headerTxtSec!!
                        )
                        HorizontalPager(
                            horizontalAlignment = Alignment.CenterHorizontally,
                            state = pagerM,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(top = 8.dp)
                                .height(cardPageHeight)
                        ) { page ->
                            _st.currentState(_min[page].minLow.toInt())
                            Card(
                                  backgroundColor = ThemeSetup.getTheme().cNewMeetCardColor[0]
                                , shape = RoundedCornerShape(8.dp) // MaterialTheme.shapes.small,
                                , modifier = Modifier
                                    .width(32.dp)
                                    .height(cardPageHeight)
                                    .graphicsLayer {
                                        val pageOffset = calculateCurrentOffsetForPage(page)
                                            .absoluteValue
                                        lerp(
                                            start = ScaleFactor(0.75f, .85f),
                                            stop = ScaleFactor(1.0f, 1.0f),
                                            fraction = 1f - pageOffset.coerceIn(0f, 1f)
                                        ).also { scale ->
                                            scaleX = scale.scaleX
                                            scaleY = scale.scaleY
                                        }
                                        alpha = lerp(
                                            start = ScaleFactor(0.5f, 0.5f),
                                            stop = ScaleFactor(1.0f, 1.0f),
                                            fraction = 1f - pageOffset.coerceIn(0f, 1f)
                                        ).scaleX
                                    }
                                , elevation = 2.dp
                            ) {
                                Column(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .fillMaxHeight(),
                                    horizontalAlignment = Alignment.CenterHorizontally,
                                    verticalArrangement = Arrangement.Center
                                ) {
                                    Text(
                                          text = "${_min[page]!!.minLow}"    //"${months[page].name}",
                                        , modifier = Modifier
                                            .padding(3.dp)
                                            .graphicsLayer {

                                            }
                                        , style = cardTxtEnb!!
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }

        @Composable
        internal fun GetDropDownYear(
            lst: MutableList<Int>,
            mutableState: MutableState<Int>,
            param: CurrentStateListener
        ) {
            var expanded by remember { mutableStateOf(false) }
            Row(
                modifier = Modifier.fillMaxSize().padding(top = 4.dp),
                verticalAlignment = Alignment.CenterVertically,
            ){
                Column(modifier = Modifier
                    .padding(start = 4.dp, top = 4.dp, end = 4.dp, bottom = 0.dp)
                    .fillMaxSize(),
                    horizontalAlignment = Alignment.CenterHorizontally
                ){
                    Text(
                        text = "Year:",
                        style = headerTxtSec!!
                    )
                    Row(
                        Modifier
                            .height(cardPageHeight)
                            .padding(top = 8.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ){
                        Text(
                            text = "${lst[mutableState.value]}",
                            style = headerTxtSec,
                            modifier = Modifier.padding(
                                start = 5.dp,
                                top = 0.dp,
                                end = 8.dp,
                                bottom = 0.dp
                            ),
                        )
                        IconButton(onClick = { expanded = true }) {
                            Icon(
                                Icons.Filled.ArrowDropDown,
                                contentDescription = "Period selected",
                                tint = Color(0xffB0B7C3)
                            )
                        }
                    }
                    Box(modifier = Modifier
                        .width(150.dp)
                        .wrapContentSize(Alignment.BottomEnd)
                    ){
                        DropdownMenu(
                            expanded = expanded,
                            onDismissRequest = { expanded = false },
                            modifier = Modifier.background(Color.LightGray)
                        ) {
                            lst.forEachIndexed { index, s ->
                                DropdownMenuItem(onClick = {
                                    param.currentState(s)
                                    mutableState.value = index
                                    expanded = false
                                }) {
                                    Text(
                                        modifier = Modifier.padding(
                                            start = 8.dp, end = 8.dp,
                                            top = 2.dp, bottom = 2.dp),
                                        text = "$s",
                                        style = cardTxtEnb!!)
                                }
                                if(index >= 0 && index < lst.size-1) {
                                    Divider(
                                        modifier = Modifier.padding(start = 8.dp, end = 8.dp),
                                        color = Color(0xff333333),
                                        thickness = 1.dp)
                                }
                            }
                        }
                    }
                }
            }
        }

    }
}

