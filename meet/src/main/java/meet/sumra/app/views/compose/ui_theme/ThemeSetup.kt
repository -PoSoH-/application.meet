package meet.sumra.app.views.compose.ui_theme

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import meet.sumra.app.AppMeet
import meet.sumra.app.utils.setup.ThemeUtils

class ThemeSetup() {
    companion object{
        fun getTheme(): ColorTh {
            if(ThemeUtils.getApplicationTheme(AppMeet.getInstance())){
                return getLightTheme()
            }else{
                return getDarkTheme()
            }
        }

        fun getDailyBonus():ColorDailyBonus{
            return ColorDailyBonus()
        }

        fun getShadBonus():ShawDownDailyBonus{
            return ShawDownDailyBonus()
        }

        private fun getDarkTheme():ColorTh{
            return ColorTh(
                cText = Color(color = 0xff252525),
                cBackground = Color(color = 0xff377DFF), //(color = 0xffffffff),
                cBackLine   = Color(color = 0xff252525),
                cIconColor  = Color(color = 0xffffffff),

                cCardPlzColorBlue  = Color(color = 0xff377DFF),
                cCardPlzColorWhite = Color(color = 0xffF5FCF9),
                cJetBackground     = Color(color = 0xffFAF9FC),
            )
        }

        private fun getLightTheme():ColorTh{
            return ColorTh(
                cText = Color(color = 0xffffffff),
                cBackground = Color(color = 0xff377DFF), //Color(color = 0xff252525),
                cBackLine   = Color(color = 0xff252525),
                cIconColor  = Color(color = 0xff2F363D),

                cCardPlzColorBlue  = Color(color = 0xff377DFF),
                cCardPlzColorWhite = Color(color = 0xffF5FCF9),
                cJetBackground     = Color(color = 0xff252737),
            )
        }

//        private fun getStyleL()= StyleText.light()
//        private fun getStyleD()= StyleText.dark()

        fun getDimen() = CDimension()
        val bbd = BBD()
            get() {return field}
    }
    class ColorTh(
        val cText: Color,       // = Color(color = 0xffffffff),
        val cBackground: Color, // = Color(color = 0xff252525),
        val cBackLine: Color,   // = Color(color = 0xff252525)
        val cTextAddMeet: Color = Color(color = 0xffBDBDBD),
        val cIconColor: Color,

        val cCardPlzColorBlue  : Color,
        val cCardPlzColorWhite : Color,
        val cPlazaTabBackground: Color = Color(0xffFAFBFC),
        val cJetBackground: Color,

        val cPlazaText : Color = Color(color = 0xff323B4B),

        val cPieChartColors: MutableList<Color> = mutableListOf(
            Color(0xffFAFBFC),
            Color(0xffFFAB00),
            Color(0xff38CB89)
        ),

        val cNewMeetColors: MutableMap<NewMeetKey, Color> = mutableMapOf(
            Pair(NewMeetKey.txtCrdLabel, Color(0xff363941)),
            Pair(NewMeetKey.txtSecLabel, Color(0xff4E5D78)),
            Pair(NewMeetKey.txtEnbLabel, Color(0xffF0F0F0)),
            Pair(NewMeetKey.txtDsbLabel, Color(0xffD7D7D7))
        ),

        val cNewMeetCardColor: List<Color> = mutableListOf(Color(0xff377DFF),Color(0xffF0F0F0))
    )
    enum class NewMeetKey {
        txtCrdLabel,
        txtSecLabel,
        txtEnbLabel,
        txtDsbLabel
    }

    class ColorDailyBonus(
        val cDefault: Color = Color(color = 0xffffffff),
        val cType_daily_bonus: Color = Color(color = 0xff38CB89),
        val cType_commercial: Color = Color(color = 0xff38CB89),
        val cType_sumra_id: Color = Color(color = 0xff377DFF),
        val cType_mobile_app: Color = Color(color = 0xffFF5630),
        val cType_publishing_to: Color = Color(color = 0xff38CB89),
        val cType_publishing_with: Color = Color(color = 0xffFFAB00),
        val cType_surveys: Color = Color(color = 0xff377DFF),
        val cType_reviews: Color = Color(color = 0xffFF5630)
    )

    class ShawDownDailyBonus(
        val cDefault: Color = Color(color = 0xffffffff),
        val cType_daily_bonus: Color = Color(color = 0xffE5F6EF),
        val cType_commercial: Color = Color(color = 0xffFDF4E0),
        val cType_sumra_id: Color = Color(color = 0xffEBF2FF),
        val cType_mobile_app: Color = Color(color = 0xffFFEFEB),
        val cType_publishing_to: Color = Color(color = 0xffE5F6EF),
        val cType_publishing_with: Color = Color(color = 0xffFDF4E0),
        val cType_surveys: Color = Color(color = 0xffEBF2FF),
        val cType_reviews: Color = Color(color = 0xffFFEFEB)
    )

    data class CDimension(
        val sysBarHeight: Dp = 24.dp ,
        val appBarHeight: Dp = 56.dp ,
        val topBozRadius: Dp = 28.dp
    )

    class BBD(
        val bBH:  Dp = 30.dp,
        val bBW:  Dp = 92.dp,
        val bBBC: Color = Color(0xffF16868),
        val bBBR: Dp = 7.dp,
        val bBT:  Dp = 13.dp,

        val bBTC: Color = Color(0xffFFFFFF)
    )
}