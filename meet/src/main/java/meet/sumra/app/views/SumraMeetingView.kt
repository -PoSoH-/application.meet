package meet.sumra.app.views

import android.content.Context
import android.os.Bundle
import com.facebook.react.bridge.ReadableMap
import org.jitsi.meet.sdk.*
import org.jitsi.meet.sdk.log.JitsiMeetLogger
//import org.jitsi.meet.sdk.OngoingConferenceTracker
import java.lang.RuntimeException

class SumraMeetingView: BaseReactView<JitsiMeetViewListener> {

    constructor(context: Context) : super(context) {
        if (context !is JitsiMeetActivityInterface) {
            throw RuntimeException("Enclosing Activity must implement JitsiMeetActivityInterface")
        } else {
//            OngoingConferenceTracker.getInstance().addListener(this)
        }
    }

    override fun dispose() {
//        OngoingConferenceTracker.getInstance().removeListener(this)
        super.dispose()
    }

//    fun enterPictureInPicture() {
//        val pipModule = ReactInstanceManagerHolder.getNativeModule(
//            PictureInPictureModule::class.java
//        ) as PictureInPictureModule
//        if (pipModule != null && pipModule.isPictureInPictureSupported() && !JitsiMeetActivityDelegate.arePermissionsBeingRequested() && this.url != null) {
//            try {
//                pipModule.enterPictureInPicture()
//            } catch (var3: RuntimeException) {
//                JitsiMeetLogger.e(var3, "Failed to enter PiP mode", *arrayOfNulls(0))
//            }
//        }
//    }

    fun join(options: JitsiMeetConferenceOptions?) {
        setProps(if (options != null) asProps(options) else Bundle())
    }

    fun leave() {
        setProps(Bundle())
    }

    private fun setProps(newProps: Bundle) {
        val props = mergeProps(asProps(JitsiMeet.getDefaultConferenceOptions()), newProps)

        props?.putLong("timestamp", System.currentTimeMillis())
        createReactRootView("App", props)
    }

    fun onCurrentConferenceChanged(conferenceUrl: String) {
        this.url = conferenceUrl
    }

    private fun asProps(options: JitsiMeetConferenceOptions?): Bundle {
        val props = Bundle()
        options?.apply {
            if (!options.featureFlags.containsKey("pip.enabled")) {
                this.featureFlags.putBoolean("pip.enabled", true)
            }

            props.putBundle("flags", this.featureFlags)
            if (this.colorScheme != null) {
                props.putBundle("colorScheme", this.colorScheme)
            }

            val urlProps = Bundle()
            if (this.room != null && this.room.contains("://")) {
                urlProps.putString("url", this.room)
            } else {
                if (this.serverURL != null) {
                    urlProps.putString("serverURL", this.serverURL.toString())
                }
                if (this.room != null) {
                    urlProps.putString("room", this.room)
                }
            }

            if (this.token != null) {
                urlProps.putString("jwt", this.token)
            }

            if (this.userInfo != null) {
                props.putBundle("userInfo", asUserOption(this.userInfo))
            }

//            if (this.config != null) {
//                urlProps.putBundle("config", this.config)
//            }

            props.putBundle("url", urlProps)

            return props
        }
        return Bundle()
    }

    private fun asUserOption(user: JitsiMeetUserInfo): Bundle{
        val b = Bundle()
        if (user.displayName != null) {
            b.putString("displayName", user.displayName)
        }

        if (user.email != null) {
            b.putString("email", user.email)
        }

        if (user.avatar != null) {
            b.putString("avatarURL", user.avatar.toString())
        }

        return b
    }

    private val LISTENER_METHODS = ListenerUtils.mapListenerMethods(
        JitsiMeetViewListener::class.java
    )

    @Volatile
    private var url: String? = null

    private fun mergeProps(a: Bundle?, b: Bundle?): Bundle? {
        val result = Bundle()
        return if (a == null) {
            if (b != null) {
                result.putAll(b)
            }
            result
        } else if (b == null) {
            result.putAll(a)
            result
        } else {
            result.putAll(a)
            val var3: Iterator<*> = b.keySet().iterator()
            while (var3.hasNext()) {
                val key = var3.next() as String
                val bValue = b[key]
                val aValue = a[key]
                val valueType = bValue!!.javaClass.simpleName
                if (valueType.contentEquals("Boolean")) {
                    result.putBoolean(key, (bValue as Boolean?)!!)
                } else if (valueType.contentEquals("String")) {
                    result.putString(key, bValue as String?)
                } else {
                    if (!valueType.contentEquals("Bundle")) {
                        throw RuntimeException("Unsupported type: $valueType")
                    }
                    result.putBundle(key, mergeProps(aValue as Bundle?, bValue as Bundle?))
                }
            }
            result
        }
    }

    @Deprecated("")
    override fun onExternalAPIEvent(name: String?, data: ReadableMap?) {
        this.onExternalAPIEvent(LISTENER_METHODS, name, data)
    }

    override fun onDetachedFromWindow() {
        dispose()
        super.onDetachedFromWindow()
    }
}