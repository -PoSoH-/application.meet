package meet.sumra.app.views.compose.ui_theme

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import meet.sumra.app.R
import meet.sumra.app.views.compose.ui_theme.ThemeSetup.Companion.bbd

object StyleText {

//    fun light() = StyleLight()
//    fun dark() = StyleLight()

//    class StyleLight(){

        fun txtTabLbl() = androidx.compose.ui.text.TextStyle(
            fontSize = 13.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.Black)),
            textAlign = TextAlign.Justify,
            color = Color(0xff323B4B)
        )

        fun txtPgLbl() = androidx.compose.ui.text.TextStyle(
            fontSize = 18.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.Black)),
            textAlign = TextAlign.Justify,
            color = Color(0xff323B4B)
        )

        fun txtPgTkn() = androidx.compose.ui.text.TextStyle(
            fontSize = 16.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.Black)),
            textAlign = TextAlign.Justify,
            color = Color(0xff323B4B)
        )

        fun txtCrdLbl() = androidx.compose.ui.text.TextStyle(
            fontSize = 13.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.ExtraBold)),
            textAlign = TextAlign.Justify,
            color = Color(0xff4E5D78)
        )

        fun txtCrdTxt() = androidx.compose.ui.text.TextStyle(
            fontSize = 14.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.ExtraBold)),
            textAlign = TextAlign.Justify,
            color = Color(0xffB0B7C3)
        )

        fun txtCrdLim() = androidx.compose.ui.text.TextStyle(
            fontSize = 13.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.ExtraBold)),
            textAlign = TextAlign.Justify,
            color = Color(0xffB0B7C3)
        )

        fun txtCrdCur(color: Color) = androidx.compose.ui.text.TextStyle(
            fontSize = 13.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.ExtraBold)),
            textAlign = TextAlign.Justify,
            color = color //Color(0xff38CB89)
        )

        fun txtCrdUlt() = androidx.compose.ui.text.TextStyle(
            fontSize = 13.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.ExtraBold)),
            textAlign = TextAlign.Justify,
            color = Color(0xff8A94A6)
        )

        fun txtPieChart() = androidx.compose.ui.text.TextStyle(
            fontSize = 20.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.ExtraBold)),
            textAlign = TextAlign.Justify,
            color = Color(0xff4E5D78)
        )

        fun txtPieLblBig() = androidx.compose.ui.text.TextStyle(
            fontSize = 24.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.ExtraBold)),
            textAlign = TextAlign.Justify,
            color = Color(0xff323B4B)
        )

        fun txtPieLblLow() = androidx.compose.ui.text.TextStyle(
            fontSize = 13.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.ExtraBold)),
            textAlign = TextAlign.Justify,
            color = Color(0xff38CB89)
        )

        fun txtWaitLbl() = androidx.compose.ui.text.TextStyle(
            fontSize = 20.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W900)),
            textAlign = TextAlign.Justify,
            color = Color(0xffffffff)
        )

        /***   ***   ***/
        fun txtWaitBBRL() = androidx.compose.ui.text.TextStyle(
            fontSize = 16.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W600)),
            textAlign = TextAlign.Justify,
            color = Color(0xff323B4B) // bbd.bBTC
        )

        fun txtWaitBBRV() = androidx.compose.ui.text.TextStyle(
            fontSize = 13.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W900)),
            textAlign = TextAlign.Justify,
            color = Color(0xff8A94A6)  // bbd.bBTC
        )

        fun txtWaitBBT() = androidx.compose.ui.text.TextStyle(
            fontSize = 13.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W900)),
            textAlign = TextAlign.Justify,
            color = bbd.bBTC
        )


        fun txtListHead() = androidx.compose.ui.text.TextStyle(
            fontSize = 10.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W900)),
            textAlign = TextAlign.Justify,
            color = Color(0xff0366D6)
        )

        fun txtListName() = androidx.compose.ui.text.TextStyle(
            fontSize = 14.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W900)),
            textAlign = TextAlign.Justify,
            color = Color(0xff444D56)
        )

        fun txtListDate() = androidx.compose.ui.text.TextStyle(
            fontSize = 12.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W900)),
            textAlign = TextAlign.Justify,
            color = Color(0xff959DA5)
        )

        // dialog Button texts
        fun txtButtonLbl() = androidx.compose.ui.text.TextStyle(
            fontSize = 15.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify,
            color = Color(0xffFFFFFF)
        )

        fun txtInfoLbl() = androidx.compose.ui.text.TextStyle(
            fontSize = 13.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W500)),
            textAlign = TextAlign.Justify,
            color = Color(0xff6A737D)
        )

        // earnings
        fun txtEarnCardLbl() = androidx.compose.ui.text.TextStyle(
            fontSize = 20.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify,
            color = Color(0xffFFFFFF)
        )

        fun txtEarnCardTxt() = androidx.compose.ui.text.TextStyle(
            fontSize = 18.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W400)),
            textAlign = TextAlign.Justify,
            color = Color(0xffFFFFFF)
        )

        fun txtBoardCardLbl() = androidx.compose.ui.text.TextStyle(
            fontSize = 16.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify,
            color = Color(0xff323B4B)
        )

        fun txtBoardCardTxt() = androidx.compose.ui.text.TextStyle(
            fontSize = 13.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W500)),
            textAlign = TextAlign.Justify,
            color = Color(0xff4E5D78)
        )

        fun txtDateCardLst() = androidx.compose.ui.text.TextStyle(
            fontSize = 13.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W500)),
            textAlign = TextAlign.Justify,
            color = Color(0xffB0B7C3)
        )

        fun txtBoardCardHnt() = androidx.compose.ui.text.TextStyle(
            fontSize = 11.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W500)),
            textAlign = TextAlign.Justify,
            color = Color(0xffB0B7C3)
        )

        fun txtBoardCardVal() = androidx.compose.ui.text.TextStyle(
            fontSize = 13.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify,
            color = Color(0xff4E5D78)
        )

        fun txtEarnCardBtn() = androidx.compose.ui.text.TextStyle(
            fontSize = 16.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W900)),
            textAlign = TextAlign.Justify,
            color = Color(0xff323B4B)
        )

        /**  **  **/

        fun txtProjectedLbl() = androidx.compose.ui.text.TextStyle(
            fontSize = 13.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify,
            color = Color(0xffFFFFFF)
        )

        fun txtProjectedBtnOn() = androidx.compose.ui.text.TextStyle(
            fontSize = 13.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify,
            color = Color(0xffFFFFFF)
        )

        fun txtProjectedBtnOf() = androidx.compose.ui.text.TextStyle(
            fontSize = 13.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W400)),
            textAlign = TextAlign.Justify,
            color = Color(0xffB0B7C3)
        )

        fun txtProjectedCardLbl() = androidx.compose.ui.text.TextStyle(
            fontSize = 15.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify,
            color = Color(0xffFFffFF)
        )

        fun txtProjectedCardTxt() = androidx.compose.ui.text.TextStyle(
            fontSize = 12.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W500)),
            textAlign = TextAlign.Justify,
            color = Color(0xffFFffFF)
        )

        fun txtProjectedBtnLbl() = androidx.compose.ui.text.TextStyle(
            fontSize = 16.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify,
            color = Color(0xffFFffFF)
        )

        fun txtProjectedCrdVal() = androidx.compose.ui.text.TextStyle(
            fontSize = 15.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W500)),
            textAlign = TextAlign.Justify,
            color = Color(0xffFFffFF)
        )

        fun txtProjectedOp() = androidx.compose.ui.text.TextStyle(
            fontSize = 11.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify,
            color = Color(0xffB0B7C3)
        )

        /**  **  **/

        fun txtPlanLabelStart() = androidx.compose.ui.text.TextStyle(
            fontSize = 16.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify,
            color = Color(0xff377DFF)
        )

        fun txtPlanLabelFinal() = androidx.compose.ui.text.TextStyle(
            fontSize = 16.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W400)),
            textAlign = TextAlign.Justify,
            color = Color(0xff323B4B)
        )

        fun txtPlanCenterUp() = androidx.compose.ui.text.TextStyle(
            fontSize = 13.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W400)),
            textAlign = TextAlign.Justify,
            color = Color(0xff4E5D78)
        )

        fun txtPlanCenterDw() = androidx.compose.ui.text.TextStyle(
            fontSize = 16.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W400)),
            textAlign = TextAlign.Justify,
            color = Color(0xff4E5D78)
        )

        /** **  **/

        fun txtStatisticLbl() = androidx.compose.ui.text.TextStyle(
            fontSize = 16.sp,
            fontFamily = FontFamily(Font(R.font.font_dm_sans_family, FontWeight.W700)),
            textAlign = TextAlign.Justify,
            color = Color(0xff323B4B)
        )
//    }

}