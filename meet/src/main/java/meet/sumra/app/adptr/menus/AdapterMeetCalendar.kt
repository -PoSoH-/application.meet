package meet.sumra.app.adptr.menus

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import meet.sumra.app.adptr.hldr.HCalendarMeetItem
import meet.sumra.app.data.mdl.DtMeetCalendar
import meet.sumra.app.databinding.BoxItemCalendarPleningsBinding

class AdapterMeetCalendar: RecyclerView.Adapter<HCalendarMeetItem>() {

    private val aCalendars = mutableListOf<DtMeetCalendar>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HCalendarMeetItem {
        return HCalendarMeetItem(BoxItemCalendarPleningsBinding
            .inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: HCalendarMeetItem, position: Int){
        holder.bindMeetData(aCalendars[position])
    }

    override fun getItemCount() = aCalendars.size

    internal fun updateCalendarScheduler(fCalendars: MutableList<DtMeetCalendar>){
        aCalendars.clear()
        aCalendars.addAll(fCalendars)
        notifyDataSetChanged()
    }
}