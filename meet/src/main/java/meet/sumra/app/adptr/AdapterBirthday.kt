package meet.sumra.app.adptr

import android.content.Context
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.layout.ScaleFactor
import androidx.compose.ui.layout.lerp
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.lerp
import androidx.compose.ui.unit.sp
import com.google.accompanist.pager.*
import meet.sumra.app.R
import java.util.*
import kotlin.math.absoluteValue

class AdapterBirthday(val ctx: Context)  {

    private var years = DealerDate.getValuesYear()
    private var months = DealerDate.getMonths()

    private var mCurY = 0
    private var mCurM = 0
    private var mCurD = 0

    private var currentDays  = DealerDate.getMonthDays(2, 1985)
    private val dmSansFamily = FontFamily(Font(R.font.dm_sans_medium, FontWeight.Black))

    @Composable
    @ExperimentalPagerApi
    fun adapterYear() {

        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            val yearsState = rememberPagerState(
                infiniteLoop = true
                , pageCount = years.size
                , initialPage = years.size/2
                , initialOffscreenLimit = 2,
            )

            Text(
                text = "Year",
                modifier = Modifier.padding(4.dp),
                fontSize = 12.sp,
                color = Color(R.color.gray_500),
                fontFamily = dmSansFamily,
                textAlign = TextAlign.Center
            )

            HorizontalPager(
                state = yearsState,
//                        height(IntrinsicSize.Min)
            ) { page ->
//                LaunchedEffect(yearsState) {
//                    snapshotFlow { yearsState.currentPage }.collect { type ->
//                        mCurY = type
//                    }
//                }
                Card(
                    shape = MaterialTheme.shapes.small,
                    modifier = Modifier
                        .padding(4.dp)
                        .fillMaxWidth()
                        .graphicsLayer {
                            val pageOffset = calculateCurrentOffsetForPage(page).absoluteValue
                            lerp(
                                start = 0.85.dp,
                                stop = 1.dp,
                                fraction = 1f - pageOffset.coerceIn(0f, 1f)
                            ).also { scale ->
                                scaleX = scale.value
                                scaleY = scale.value
                            }
                            lerp(
                                start = 0.5.dp,
                                stop = 1.0.dp,
                                fraction = 1f - pageOffset.coerceIn(0f, 1f)
                            ).also { alp ->
                                alpha = alp.value
                            }
                        },
//                        .fillMaxWidth(0.8f),
//                        .aspectRatio(1f),
                    elevation = 2.dp
                ) {
                    Column(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        updateCurrentYear(page)
                        Text(
                            text = "${years[page]}",
                            modifier = Modifier.padding(4.dp),
                            fontSize = 14.sp,
                            maxLines = 1,
                            color = Color(R.color.blue_500),
                            fontFamily = dmSansFamily,
                            textAlign = TextAlign.Center
                        )
                    }
                }
            }
        }
    }

    @Composable
    @ExperimentalPagerApi
    fun adapterMonth() {

        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            val monthState = rememberPagerState(
                infiniteLoop = true
                , pageCount = months.size
                , initialPage = months.size/2
                , initialOffscreenLimit = 2
            )
            Text(
                text = "Month",
                modifier = Modifier.padding(4.dp),
                fontSize = 12.sp,
                color = Color(R.color.gray_500),
                fontFamily = dmSansFamily,
                textAlign = TextAlign.Center
            )

            HorizontalPager(state = monthState) { page ->
                Card(
                    shape = MaterialTheme.shapes.small,
                    modifier = Modifier
                        .padding(4.dp)
                        .fillMaxWidth()
                        .graphicsLayer {
                            val pageOffset = calculateCurrentOffsetForPage(page).absoluteValue
                            lerp(
                                start = ScaleFactor(0.85f, 1.0f),
                                stop = ScaleFactor(1.0f, 1.0f),
                                fraction = 1f - pageOffset.coerceIn(0f, 1f)
                            ).also { scale ->
                                scaleX = scale.scaleX
                                scaleY = scale.scaleY
                            }
                            alpha = lerp(
                                start = ScaleFactor(0.5f,0.5f),
                                stop = ScaleFactor(1.0f,1.0f),
                                fraction = 1f - pageOffset.coerceIn(0f, 1f)
                            ).scaleX
//                                .also { alp ->
//                                alpha = alp.value
//                            }
                        },
                    elevation = 2.dp
                ) {
                    Column(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        mCurM = page
                        Text(
                            text = "${months[page].name}",
                            modifier = Modifier.padding(4.dp),
                            fontSize = 14.sp,
                            maxLines = 1,
                            color = Color(R.color.blue_500),
                            fontFamily = dmSansFamily,
                            textAlign = TextAlign.Center
                        )
                    }
                }
            }
        }
    }

    @ExperimentalPagerApi
    @Composable
    fun adapterDay() {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            val datesState = rememberPagerState(
                infiniteLoop = true
                , pageCount = currentDays.size
                , initialPage = currentDays.size/2
                , initialOffscreenLimit = 1)

            Text(
                text = "Day"
                , modifier = Modifier.padding(4.dp)
                , fontSize = 12.sp
                , color = Color(R.color.gray_500)
                , fontFamily = dmSansFamily
                , textAlign = TextAlign.Center
            )

            HorizontalPager(
                state = datesState) { page ->
                Card(
                    shape = MaterialTheme.shapes.small
                    , modifier = Modifier
                        .padding(4.dp)
                        .fillMaxWidth()
                        .graphicsLayer {
                            val pageOffset = calculateCurrentOffsetForPage(page).absoluteValue
                            lerp(
                                start = 0.85.dp,
                                stop = 1.dp,
                                fraction = 1f - pageOffset.coerceIn(0f, 1f)
                            ).also { scale ->
                                scaleX = scale.value
                                scaleY = scale.value
                            }
                            lerp(
                                start = 0.5.dp,
                                stop = 1.0.dp,
                                fraction = 1f - pageOffset.coerceIn(0f, 1f)
                            ).also { alp ->
                                alpha = alp.value
                            }
                        }
                    , elevation = 2.dp
                ){
                    Column(
                        modifier = Modifier.fillMaxWidth()
                        , horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        mCurD = page
                        Text(
                            text = "${currentDays[page]}"
                            , modifier = Modifier.padding(4.dp)
                            , fontSize = 14.sp
                            , maxLines = 1
                            , color = Color(R.color.blue_500)
                            , fontFamily = dmSansFamily
                            , textAlign = TextAlign.Center
                        )
                    }
                }

            }
        }
    }

    @Preview
    @Composable
    @ExperimentalPagerApi
    fun composablePreview(){
        adapterMonth()
        adapterDay()
    }

    @ExperimentalPagerApi
    private fun updateCurrentYear(page: Int){
        mCurY = page
        currentDays = DealerDate.getMonthDays(month = mCurM+1, year = mCurY+1)
//        datesState.notifyAll()
    }

    fun getBirthDay() : String{
        return """${years[mCurY]}-${months[mCurM]}-${currentDays[mCurD]}"""
    }

//    fun adapterMonth(year: Int = 6): AdapterMonth {
////        mMonthAdapter.
//        return mMonthAdapter
//    }
//
//    fun adapterData(year: Int = 14): AdapterDay {
//        return mDateAdapter
//    }


//    class AdapterDay : RecyclerView.Adapter<AdapterDay.HolderDay>() {
//
//        private var days = DealerDate.getMonthDays(1, 2020)
//        private var mCurrentSelectedValue: String = "now select"
//        private var mCurrentTextView: TextView? = null
//
//
//        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderDay {
//            return HolderDay(  (createYearView())
////                LayoutInflater.from(parent.context)
////                    .inflate(R.layout.cell_view_day, parent, false)
////            )
//        }
//
//        override fun getItemCount(): Int {
//            return days.size
//        }
//
//        override fun onBindViewHolder(holder: HolderDay, position: Int) {
//            mCurrentTextView = holder.getViewTXT()
//            mCurrentSelectedValue = days[position]
//            holder.bind(mCurrentSelectedValue)
//            LogUtil.error("ADAPTER DAY", "Current   day: $mCurrentSelectedValue")
//            LogUtil.error("ADAPTER DAY", "Text view day: ${mCurrentTextView?.text}")
//        }
//
//        fun bindDayOfMonth(year : String, month : String){
//            days = DealerDate.getMonthDays(month = month.toInt(), year = year.toInt())
//            notifyDataSetChanged()
//        }
//
//        fun getCurentPosition() = mCurrentSelectedValue
//        fun getDay() : String{
//            return mCurrentTextView?.text.toString()
//    //      return mCurrentSelectedValue
//        }
//
//        fun getDay(position: Int) : String{
//            return days[position]
//        }
//
////        @Preview
//        @Composable
//        fun createYearView(){
//            Card(
//                shape = MaterialTheme.shapes.small,
//                modifier = Modifier.padding(10.dp).fillMaxWidth(),
//                elevation = 8.dp
//            ){
//                Column(
//                    modifier = Modifier.fil
//                ) {
//                    Text(
//                        text = "Hello Year"
//                    )
//                }
//            }
//        }
//
//        class HolderDay(itemView : View) : RecyclerView.ViewHolder(itemView) {
//            private val textDay = itemView.cellDayView
//            fun bind(currentDay: String){
//                textDay.text = currentDay
//            }
//
//            fun getViewTXT() = textDay
//        }
//
//    }
//
//    class AdapterMonth : RecyclerView.Adapter<AdapterMonth.HolderMonth>() {
//        private val months = MonthSumra.getCollectionMonth()
//
//        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderMonth {
//            return HolderMonth(
//                LayoutInflater.from(parent.context)
//                    .inflate(R.layout.cell_view_month, parent, false)
//            )
//        }
//
//        override fun getItemCount(): Int {
//            return months.size
//        }
//
//        override fun onBindViewHolder(holder: HolderMonth, position: Int) {
//            holder.bind(months[position])
//        }
//
//        fun getNameMonthFull(selPos: Int) = months[selPos].full
//        fun getNameMonthShort(selPos: Int) = months[selPos].short
//        fun getNumberMonth(selPos: Int) = months[selPos].number
//
//        class HolderMonth(itemView : View) : RecyclerView.ViewHolder(itemView) {
//            private val textView = itemView.cellMonthView
//            fun bind(currentMonth: MonthSumra){
//                textView.text = currentMonth.short
//            }
//        }
//
//    }
//
//    class AdapterYear : RecyclerView.Adapter<AdapterYear.HolderYear>() {
//        private val years = DealerDate.getValuesYear()
//
//        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderYear {
//            return HolderYear(
//                LayoutInflater.from(parent.context)
//                    .inflate(R.layout.cell_view_year, parent, false)
//            )
//        }
//
//        override fun getItemCount(): Int {
//            return years.size
//        }
//
//        override fun onBindViewHolder(holder: HolderYear, position: Int) {
//            holder.bind(years[position].toString())
//        }
//
//        fun getCurrentYear(selPos:Int) = years[selPos]
//
//        class HolderYear(itemView : View) : RecyclerView.ViewHolder(itemView) {
//            private val textYear = itemView.cellYearView
//            fun bind(currentYear: String){
//                textYear.text = currentYear
//            }
//        }
//
//    }

    private object DealerDate {

        private val MIN = 10
        private val MAX = 99

        const val NUM = 0
        const val WRD = 1

        fun getMonths(type: Int = WRD): List<MonthSumra>{
            return MonthSumra.getCollectionMonth()
        }

        fun getValuesYear () : List<Int> {
            val start = Calendar.getInstance()
            start.timeInMillis = System.currentTimeMillis()
            val startYear = start.get(Calendar.YEAR) //- MIN
            val collections  = mutableListOf<Int>()
            for(i in MIN..MAX){
                collections.add(startYear - i)
            }
            return collections
        }

        fun getMonthDays(month:Int, year:Int):List<String>{
            return when(month){
                1,3,5,7,8,10,12 -> getCollectionNumber(31)
                2 -> if(checkLeapYear(year)) getCollectionNumber(29) else getCollectionNumber(28)
                else -> getCollectionNumber(30)
            }
        }

        fun getCollectionNumber(count:Int): List<String>{
            val result = mutableListOf<String>()
            for (i in 1..count) result.add(i.toString())
            return result
        }

        private fun checkLeapYear (year:Int) = ((year%400)%100)%4 == 0

    }

    private enum class MonthSumra (val full:String, val short:String, val number:Int ){
        JAN ("January", "Jan", 1),
        FEB ("February", "Feb", 2),
        MAR ("March", "Mar", 3),
        APR ("April", "Apr", 4),
        MAY ("May", "May", 5),
        JUNE ("June", "June", 6),
        JULY ("July", "July", 7),
        AUG ("August", "Aug", 8),
        SEPT ("September", "Sept", 9),
        OCT ("October", "Oct", 10),
        NOV ("November", "Nov", 11),
        DEC ("December", "Dec", 12);

        companion object {
            fun getCollectionMonth(): List<MonthSumra> {
                return listOf<MonthSumra>(
                    JAN
                    , FEB
                    , MAR
                    , APR
                    , MAY
                    , JUNE
                    , JULY
                    , AUG
                    , SEPT
                    , OCT
                    , NOV
                    , DEC
                )
            }
        }
    }

}