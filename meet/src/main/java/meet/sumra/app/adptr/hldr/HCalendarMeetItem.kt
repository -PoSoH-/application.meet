package meet.sumra.app.adptr.hldr

import android.annotation.SuppressLint
import android.view.View
import android.widget.ImageView
import androidx.core.view.forEach
import androidx.core.view.forEachIndexed
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import meet.sumra.app.R
import meet.sumra.app.adptr.AdapterGrpUsrImg
import meet.sumra.app.data.enums.EAvatarType
import meet.sumra.app.data.mdl.DtMeetCalendar
import meet.sumra.app.databinding.BoxItemCalendarPleningsBinding
import meet.sumra.app.ext.loadRes
import meet.sumra.app.ext.loadUrl
import sdk.net.meet.api.contacts.response.FetchContact
import java.util.*

class HCalendarMeetItem(hold: ViewBinding): RecyclerView.ViewHolder(hold.root) {
    private val hView = hold as BoxItemCalendarPleningsBinding
//    private val adapter = AdapterGrpUsrImg()

//    init {
//        hView.calendarItemStartUserValue.adapter = adapter
//        hView.calendarItemStartUserValue.layoutManager = LinearLayoutManager(
//            hView.root.context,
//            LinearLayoutManager.HORIZONTAL,
//            true)
//    }

    internal fun bindMeetData(dMeetData: DtMeetCalendar){ //, event: EventContactListener){

        hView.calendarCardLabel.text = dMeetData.dTitle
        val tmpS = parseToDate(dMeetData.dStartDate)
        hView.calendarCardDateValue.text = tmpS.get(0)
        hView.calendarCardTimeStartValue.text = tmpS.get(1)
        val tmpF = parseToDate(dMeetData.dFinalDate)
        hView.calendarCardTimeFinalValue.text = tmpF.get(1)

        showingParticipantsCSS(dMeetData.dParticipants)
    }

    private fun parseToDate(t: String): MutableList<String>{
        val tmp_d = t.split('T')[0]
        val tmp_t = t.split('T')[1].split(':')
        return mutableListOf(tmp_d, "${tmp_t[0]}:${tmp_t[1]}")
    }

    @SuppressLint("SetTextI18n")
    private fun showingParticipantsCSS(participant: MutableList<FetchContact>){
        hideAllParticipant()
        if(participant.size<=4){
            participant.forEachIndexed{i, c ->
                val view = hView.calendarCardParticipantsBox.getChildAt(i) as ImageView
                view.loadUrl(c.avatar, EAvatarType.CIRCLE)
                view.visibility = View.VISIBLE
                when(i){
                    0 -> {
                        val v = hView.calendarCardParticipantsBox.getViewById(R.id.calendarCardUserIconE) as ImageView
                        v.loadUrl(c.avatar, EAvatarType.CIRCLE)
                        v.visibility = View.VISIBLE
                    }
                    1 -> {
                        val v = hView.calendarCardParticipantsBox.getViewById(R.id.calendarCardUserIconD) as ImageView
                        v.loadUrl(c.avatar, EAvatarType.CIRCLE)
                        v.visibility = View.VISIBLE
                    }
                    2 -> {
                        val v = hView.calendarCardParticipantsBox.getViewById(R.id.calendarCardUserIconC) as ImageView
                        v.loadUrl(c.avatar, EAvatarType.CIRCLE)
                        v.visibility = View.VISIBLE
                    }
                    3 -> {
                        val v = hView.calendarCardParticipantsBox.getViewById(R.id.calendarCardUserIconB) as ImageView
                        v.loadUrl(c.avatar, EAvatarType.CIRCLE)
                        v.visibility = View.VISIBLE
                    }
                    4 -> {
                        val v = hView.calendarCardParticipantsBox.getViewById(R.id.calendarCardUserIconA) as ImageView
                        v.loadUrl(c.avatar, EAvatarType.CIRCLE)
                        v.visibility = View.VISIBLE
                    }
                }
            }
        }else{
            hView.calendarCardCountValue.visibility = View.VISIBLE
            hView.calendarCardCountValue.text = "+${participant.size - 4}"
            participant.forEachIndexed{i, c ->
                if(i<4) {
                    val view = hView.calendarCardParticipantsBox.getChildAt(i) as ImageView
                    view.loadUrl(c.avatar, EAvatarType.CIRCLE)
                    view.visibility = View.VISIBLE
                }else{
                    return@forEachIndexed
                }
            }
        }
    }

    private fun hideAllParticipant(){
        hView.calendarCardParticipantsBox.forEach {
            it.visibility = View.INVISIBLE
        }
        if(hView.calendarCardCountValue.isVisible) {
            hView.calendarCardCountValue.visibility = View.INVISIBLE
        }
    }


}