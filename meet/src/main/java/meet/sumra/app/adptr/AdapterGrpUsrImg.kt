package meet.sumra.app.adptr

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import meet.sumra.app.adptr.hldr.ImgGrpLstHolder
import meet.sumra.app.data.mdl.DtAvtUsr
import meet.sumra.app.databinding.BoxItmUsrImgBinding
import sdk.net.meet.api.contacts.response.FetchContact

class AdapterGrpUsrImg: RecyclerView.Adapter<ImgGrpLstHolder>() {

    private val images = mutableListOf<FetchContact>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImgGrpLstHolder {
        return ImgGrpLstHolder(BoxItmUsrImgBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ImgGrpLstHolder, position: Int) {
        holder.bindImg(images[position])
    }

    override fun getItemCount() = images.size

    internal fun updateUsrAvatar(f_usrAvt: MutableList<FetchContact>){
        images.clear()
        images.addAll(f_usrAvt)
        notifyDataSetChanged()
    }
}