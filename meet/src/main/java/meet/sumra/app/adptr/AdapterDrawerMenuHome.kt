package meet.sumra.app.adptr

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import meet.sumra.app.adptr.hldr.DrawerItemMenu
import meet.sumra.app.databinding.MenuDrawerItemBinding
import meet.sumra.app.interfaces.SelectDrawerItemListener
import meet.sumra.app.data.pojo.res.DrawerItem
import meet.sumra.app.vm.vm_home.StateHome
import meet.sumra.app.vm.vm_home.StateMeet

class AdapterDrawerMenuHome(val listener: SelectDrawerItemListener): RecyclerView.Adapter<DrawerItemMenu>(){

    private val itemMenuList = DrawerItem().getListMenu()
    private lateinit var context: Context
    private var select = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrawerItemMenu {
        context = parent.context
        return DrawerItemMenu(MenuDrawerItemBinding.inflate(LayoutInflater.from(context), parent, false))
    }

    override fun onBindViewHolder(holder: DrawerItemMenu, position: Int) {
        holder.updateItem(itemMenuList[position], listener, position, select == position)
    }

    override fun getItemCount() = itemMenuList.size

    fun updateSelectPosition(position: Int){
        select = position
        notifyDataSetChanged()
    }

    fun updateStatePosition(position: StateHome){
        when(position){
            is StateHome.Dashboard-> select = 0
            is StateHome.Statistic-> select = 1
            is StateHome.Referrals-> select = 2
            is StateHome.Earnings -> select = 3
            is StateHome.Rewards  -> select = 4
            is StateHome.Pioneer  -> select = 5
            is StateHome.Profile  -> select = 6
            else -> Unit
        }
        notifyDataSetChanged()
    }

}