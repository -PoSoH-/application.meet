package meet.sumra.app.adptr.hldr

import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import meet.sumra.app.data.enums.EAvatarType
import meet.sumra.app.data.mdl.DtAvtUsr
import meet.sumra.app.databinding.BoxItmUsrImgBinding
import meet.sumra.app.ext.loadUrl
import sdk.net.meet.api.contacts.response.FetchContact

class ImgGrpLstHolder(imgView: ViewBinding): RecyclerView.ViewHolder(imgView.root) {

    private val kImgView = imgView as BoxItmUsrImgBinding

    internal fun bindImg(dataImg: FetchContact){
        kImgView.calendarItmCntImg.loadUrl(dataImg.avatar, EAvatarType.CIRCLE)
    }
}

