package meet.sumra.app.adptr.hldr

import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import meet.sumra.app.databinding.BoxItemInviteBinding
import meet.sumra.app.ext.loadUrl
import meet.sumra.app.interfaces.CnctAction
import meet.sumra.app.interfaces.OnActionListener
import sdk.net.meet.api.contacts.response.FetchContact

class ItemSelectBox(vBind: ViewBinding): RecyclerView.ViewHolder(vBind.root) {

    private val v = vBind as BoxItemInviteBinding

    /***    ----****||||****----    ***/

    internal fun bindInviteItem(c: FetchContact, l: OnActionListener){
        v.apply {
            c.avatar?.run{
                inviteItemCnctImage.loadUrl(this)
            }
            inviteItemCnctButton.setOnClickListener { close ->
                l.selectItemCandidate(CnctAction.UnInvite(c))
            }
        }
    }
}