package meet.sumra.app.adptr.hldr

import android.graphics.Typeface
import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.color.MaterialColors
import meet.sumra.app.R
import meet.sumra.app.interfaces.SelectDrawerItemListener
import meet.sumra.app.data.pojo.res.ItemMenu
import meet.sumra.app.databinding.MenuDrawerItemBinding
import net.sumra.auth.helpers.Finals

class DrawerItemMenu(val box: MenuDrawerItemBinding): RecyclerView.ViewHolder(box.root) {

    fun updateItem(item: ItemMenu, listener: SelectDrawerItemListener, itemPosition: Int, isSelect: Boolean){
        box.run {
            drawerItemIcon.setImageResource(item.itemIconOf)
            drawerItemName.text = root.context.resources.getString(item.itemName)
            root.context.let{ ctx ->
                drawerItemName.typeface = Typeface.createFromAsset(ctx.assets, Finals.FONT_FACE)
            }
            when (isSelect) {
                true -> {
                    root.setBackgroundResource(R.color.blue_400)
                    drawerItemIcon.setImageResource(item.itemIconOn)
                    drawerItemName.setTextColor(MaterialColors.getColor(root, R.attr.meetMenuColorDrawerEbl))
//                        context.resources
//                            .getColor(R.attr.meetMenuColorDrawerEbl, context.resources.newTheme())
//                    )
                }
                else -> {
                    root.setBackgroundResource(R.color.transparent)
                    drawerItemIcon.setImageResource(item.itemIconOf)
                    drawerItemName.setTextColor(MaterialColors.getColor(root, R.attr.meetMenuColorDrawerDbl))
//                        context.resources
//                            .getColor(R.attr.meetMenuColorDrawerDbl, context.resources.newTheme())
//                    )
                }
            }
        }

        box.root.setOnClickListener {
                result->listener.selected(itemPosition, item.state)
        }
    }
}