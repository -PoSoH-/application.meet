package meet.sumra.app.adptr

import android.content.Context
import androidx.compose.animation.core.*
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.*
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.pager.ExperimentalPagerApi
import kotlinx.coroutines.launch
import meet.sumra.app.views.compose.NewMeetPagerSelectorsViewHelpers
import java.lang.Math.abs
import java.util.*
import kotlin.math.roundToInt

class AdapterSelectDateTime(val ctx: Context)  {

    data class Month(
        val mNum: String = "01",
        val mTxtLow: String = "Jan",
        val mTxtFull: String = "January")
    data class Hour(
        val timeLow: String = "00",
        val timeFull: String = "00 hour")
    data class Minute(
        val minLow: String = "00",
        val minFull: String = "00 minute")

    private val month   = mutableMapOf<Int, Month>()
    private val days    = mutableStateOf(mutableListOf<String>())
    private val hours   = mutableListOf<Hour>()
    private val minutes = mutableListOf<Minute>()

    private val currentDateTime: Calendar

    private val currentMonth: Int
    private val currentDay  : Int
    private val currentYear : Int

    private val currentSH: Int
    private val currentSM: Int
    private val yearsList: MutableList<Int>

    private val mutableStartStateY: MutableList<Int> // MutableState<Int> // = mutableStateOf(currentYear)
    private val mutableStartStateM: MutableList<Int> //MutableState<Int> // = mutableStateOf(currentMonth)
    private val mutableStartStateD: MutableList<Int> //MutableState<Int> // = mutableStateOf(currentDay)
    private val mutableStartStateH: MutableList<Int> //MutableState<Int> // = mutableStateOf(currentSH)
    private val mutableStartStateS: MutableList<Int> //MutableState<Int> // = mutableStateOf(currentSM) // minute
    private val mutableFinalStateH: MutableList<Int> //MutableState<Int> // = mutableStateOf(currentSH + 1)
    private val mutableFinalStateS: MutableList<Int> //MutableState<Int> // = mutableStateOf(currentSM + 30) // minute
    private val mutableYearPosition: MutableState<Int> // = mutableStateOf(currentSM + 30) // minute

    internal var isStatePM: Boolean = false
        get() = field

    init{
        minutes.add(Minute("05", "05 minutes"))
        minutes.add(Minute("10", "10 minutes"))
        minutes.add(Minute("15", "15 minutes"))
        minutes.add(Minute("20", "20 minutes"))
        minutes.add(Minute("25", "25 minutes"))
        minutes.add(Minute("30", "30 minutes"))
        minutes.add(Minute("35", "35 minutes"))
        minutes.add(Minute("40", "40 minutes"))
        minutes.add(Minute("45", "45 minutes"))
        minutes.add(Minute("50", "50 minutes"))
        minutes.add(Minute("55", "55 minutes"))

        hours.add(Hour())
        hours.add(Hour("01", "01 hour"))
        hours.add(Hour("02", "02 hours"))
        hours.add(Hour("03", "03 hours"))
        hours.add(Hour("04", "04 hours"))
        hours.add(Hour("05", "05 hours"))
        hours.add(Hour("06", "06 hours"))
        hours.add(Hour("07", "07 hours"))
        hours.add(Hour("08", "08 hours"))
        hours.add(Hour("09", "09 hours"))
        hours.add(Hour("10", "10 hours"))
        hours.add(Hour("11", "11 hours"))
        hours.add(Hour("12", "12 hours"))

        month.put(0,  Month())
        month.put(1,  Month("02","Feb","February"))
        month.put(2,  Month("03","Mar","March"))
        month.put(3,  Month("04","Apr","April"))
        month.put(4,  Month("05","Mai","May"))
        month.put(5,  Month("06","Jun","June"))
        month.put(6,  Month("07","Jul","July"))
        month.put(7,  Month("08","Aug","August"))
        month.put(8,  Month("09","Sep","September"))
        month.put(9,  Month("10","Okt","October"))
        month.put(10, Month("11","Nov","November"))
        month.put(11, Month("12","Dec","December"))

        currentDateTime = Calendar.getInstance()

        currentYear = currentDateTime.get(Calendar.YEAR)
        val countPlaningYears = 5
        var count = 0
        yearsList = mutableListOf()
        while (count < countPlaningYears) {
            yearsList.add(currentYear + count)
            count++
        }

        currentMonth = currentDateTime.get(Calendar.MONTH)
        currentDay = currentDateTime.get(Calendar.DATE)

        currentSH = currentDateTime.get(Calendar.HOUR)
        currentSM = currentDateTime.get(Calendar.MINUTE)

        if(currentDateTime.get(Calendar.AM_PM) > 0){ isStatePM = true }

        mutableStartStateY = mutableListOf(currentYear) //mutableStateOf(currentYear)
        mutableStartStateM = mutableListOf(currentMonth) //mutableStateOf(currentMonth)
        mutableStartStateD = mutableListOf(currentDay) //mutableStateOf(currentDay)
        mutableStartStateH = mutableListOf(currentSH) //mutableStateOf(currentSH)
        mutableStartStateS = mutableListOf(currentSM) //mutableStateOf(currentSM) // minute
        mutableFinalStateH = mutableListOf(currentSH + 1) //mutableStateOf(currentSH + 1)
        mutableFinalStateS = mutableListOf(currentSM + 30) //mutableStateOf(currentSM + 30) // minute

        mutableYearPosition = mutableStateOf(0) // minute

        updateDayCalculate()
    }

    private fun updateDayCalculate(){
        val t = mutableListOf<String>()
        t.apply {
            add("01");add("02");add("03");add("04");add("05");add("06");add("07");add("08")
            add("09");add("10");add("11");add("12");add("13");add("14");add("15");add("16")
            add("17");add("18");add("19");add("20");add("21");add("22");add("23");add("24")
            add("25");add("26");add("27");add("28");add("29");add("30");add("31")
        }
        when(mutableStartStateM[0]-1){
            4,6,9,11 ->{t.remove("31")}
            2 -> {
                if((mutableStartStateY[0] % 4 == 0)
                    && (mutableStartStateY[0] % 100 != 0)
                    || (mutableStartStateY[0] % 400 == 0)){
                    t.remove("31")
                    t.remove("30")
                }else{
                    t.remove("31")
                    t.remove("30")
                    t.remove("29")
                }
            }
        }
        days.value = t
    }

    internal fun fetchStartDateTime(): String {
        return "${mutableStartStateY[0]}-${mutableStartStateM[0]}-${mutableStartStateD[0]}T${mutableStartStateH[0]}:${mutableStartStateS[0]}:00"
    }

    internal fun fetchFinalDateTime(): String {
        return "${mutableStartStateY[0]}-${mutableStartStateM[0]}-${mutableStartStateD[0]}T${mutableFinalStateH[0]}:${mutableFinalStateS[0]}:00"
    }

    @ExperimentalPagerApi
    @Composable
    fun GetViewSelectYPicker() {
        NewMeetPagerSelectorsViewHelpers.GetDropDownYear(yearsList, mutableYearPosition, object:
            NewMeetPagerSelectorsViewHelpers.CurrentStateListener {
            override fun currentState(st: Int) {
                mutableStartStateY.add(0, st)
                updateDayCalculate()
            }
        })
    }
    @ExperimentalPagerApi
    @Composable
    fun GetViewSelectMPicker() {
        NewMeetPagerSelectorsViewHelpers.GetViewIncludeSelectM(_m = month, _cM = currentMonth, object:
            NewMeetPagerSelectorsViewHelpers.CurrentStateListener {
            override fun currentState(st: Int) {
                mutableStartStateM.add(0, st)
                updateDayCalculate()
            }
        })
    }
    @ExperimentalPagerApi
    @Composable
    fun GetViewSelectDPicker() {
        NewMeetPagerSelectorsViewHelpers.GetViewIncludeSelectD(_d = days, _cD = currentDay, object :
            NewMeetPagerSelectorsViewHelpers.CurrentStateListener {
            override fun currentState(st: Int) {
                mutableStartStateD.add(0, st)
            }
        })
    }
    @ExperimentalPagerApi
    @Composable
    fun GetViewSelectHourPicker() {
        NewMeetPagerSelectorsViewHelpers.getViewIncludeSelectHour(_h = hours,
                                                                  _cH = mutableStartStateH[0].toString(),
                                                                  object :
            NewMeetPagerSelectorsViewHelpers.CurrentStateListener {
            override fun currentState(st: Int) {
                mutableStartStateH.add(0, st)
            }
        })
    }
    @ExperimentalPagerApi
    @Composable
    fun GetViewSelectMinPicker() {
        NewMeetPagerSelectorsViewHelpers.GetViewIncludeSelectMin(_min = minutes,
                                                                 _cM = mutableStartStateS[0],
                                                                 object :
            NewMeetPagerSelectorsViewHelpers.CurrentStateListener { override fun currentState(st: Int) {
                mutableStartStateS.add(0, st)
            }
        })
    }
//==================================================================================================
    @ExperimentalPagerApi
    @Composable
    fun GetViewSelectTermHourPicker() {
        NewMeetPagerSelectorsViewHelpers.getViewIncludeSelectHour(_h = hours,
                                                                  _cH = mutableFinalStateH[0].toString(),
                                                                  object :
            NewMeetPagerSelectorsViewHelpers.CurrentStateListener {
            override fun currentState(st: Int) {
                mutableFinalStateH.add(0, st)
            }
        })
    }
    @ExperimentalPagerApi
    @Composable
    fun GetViewSelectTermMinPicker() {
        NewMeetPagerSelectorsViewHelpers.GetViewIncludeSelectMin(_min = minutes,
                                                                 _cM = mutableFinalStateS[0],
                                                                 object :
            NewMeetPagerSelectorsViewHelpers.CurrentStateListener {
            override fun currentState(st: Int) {
                mutableFinalStateS.add(0, st)
            }
        })
    }
//==================================================================================================
    @Composable
    fun getViewNumberPicker() {
        Box(modifier = Modifier.fillMaxSize()) {
            NumberPicker(
                state = remember { mutableStateOf(9) },
                range = 0..10,
                modifier = Modifier.align(Alignment.Center)
            )
        }
    }

    @Composable
    fun HorizontalScrollableComponent() {
        // We create a ScrollState that's "remember"ed  to add proper support for a scrollable component.
        // This allows us to also control the scroll position and other scroll related properties.

        // remember calculates the value passed to it only during the first composition. It then
        // returns the same value for every subsequent composition. More details are available in the
        // comments below.
        val scrollState = rememberScrollState()
        // Row is a composable that places its children in a horizontal sequence. You
        // can think of it similar to a LinearLayout with the horizontal orientation.

        // You can think of Modifiers as implementations of the decorators pattern that are used to
        // modify the composable that its applied to. In this example, we ask the HorizontalScroller
        // to occupy the entire available width.
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .horizontalScroll(
                    state = scrollState,
                    reverseScrolling = true,
//                    enabled = true
                ),
            content = {
                // We iterate over each item from the personList and define what each item should
                // look like.
                for ((index, person) in minutes.withIndex()) {
                    // Card composable is a predefined composable that is meant to represent the card
                    // surface as specified by the Material Design specification. We also configure it
                    // to have rounded corners and apply a modifier.

                    // You can think of Modifiers as implementations of the decorators pattern that are
                    // used to modify the composable that its applied to. In this example, we assign a
                    // padding of 16dp to the Card.
                    Card(
                        shape = RoundedCornerShape(4.dp),
//                        backgroundColor = colors[index % colors.size],
                        modifier = Modifier.padding(8.dp)
                    ) {
                        // The Text composable is pre-defined by the Compose UI library; you can use this
                        // composable to render text on the screen
                        Text(
                            person.minFull,
                            modifier = Modifier.padding(4.dp),
                            style = TextStyle(
                                color = Color.Black,
                                fontSize = 12.sp
                            )
                        )
                    }
                }
            })
    }

    @Preview
    @ExperimentalPagerApi
    @Composable
    fun HorizontalScrollableComponentPreview() {
        HorizontalScrollableComponent()
        getViewNumberPicker()
    }

    /** functions **/

    @Composable
    fun NumberPicker(
        state: MutableState<Int>,
        modifier: Modifier = Modifier,
        range: IntRange? = null,
        textStyle: TextStyle = LocalTextStyle.current,
        onStateChanged: (Int) -> Unit = {},
    ) {
        val coroutineScope = rememberCoroutineScope()
        val numbersColumnHeight = 36.dp
        val halvedNumbersColumnHeight = numbersColumnHeight / 2
        val halvedNumbersColumnHeightPx = with(LocalDensity.current) { halvedNumbersColumnHeight.toPx() }

        fun animatedStateValue(offset: Float): Int = state.value - (offset / halvedNumbersColumnHeightPx).toInt()

        val animatedOffset = remember { Animatable(0f) }.apply {
            if (range != null) {
                val offsetRange = remember(state.value, range) {
                    val value = state.value
                    val first = -(range.last - value) * halvedNumbersColumnHeightPx
                    val last = -(range.first - value) * halvedNumbersColumnHeightPx
                    first..last
                }
                updateBounds(offsetRange.start, offsetRange.endInclusive)
            }
        }
        val coercedAnimatedOffset = animatedOffset.value % halvedNumbersColumnHeightPx
        val animatedStateValue = animatedStateValue(animatedOffset.value)
        Column(
            modifier = modifier
                .wrapContentSize()
                .draggable(
                    orientation = Orientation.Vertical,
                    state = rememberDraggableState { deltaY ->
                        coroutineScope.launch {
                            animatedOffset.snapTo(animatedOffset.value + deltaY)
                        }
                    },
                    onDragStopped = { velocity ->
                        coroutineScope.launch {
                            val endValue = animatedOffset.fling(
                                initialVelocity = velocity,
                                animationSpec = exponentialDecay(frictionMultiplier = 20f),
                                adjustTarget = { target ->
                                    val coercedTarget = target % halvedNumbersColumnHeightPx
                                    val coercedAnchors = listOf(
                                        -halvedNumbersColumnHeightPx,
                                        0f,
                                        halvedNumbersColumnHeightPx
                                    )
                                    val coercedPoint =
                                        coercedAnchors.minByOrNull { abs(it - coercedTarget) }!!
                                    val base =
                                        halvedNumbersColumnHeightPx * (target / halvedNumbersColumnHeightPx).toInt()
                                    coercedPoint + base
                                }
                            ).endState.value

                            state.value = animatedStateValue(endValue)
                            onStateChanged(state.value)
                            animatedOffset.snapTo(0f)
                        }
                    }
                )
        ) {
            val spacing = 4.dp
            val arrowColor = MaterialTheme.colors.onSecondary.copy(alpha = ContentAlpha.disabled)
            Spacer(modifier = Modifier.height(spacing))
            Box(
                modifier = Modifier
                    .width(50.dp)
                    .align(Alignment.CenterHorizontally)
                    .offset { IntOffset(x = 0, y = coercedAnimatedOffset.roundToInt()) }
            ) {
                val baseLabelModifier = Modifier.align(Alignment.Center)
                ProvideTextStyle(textStyle) {
                    Card(
                        modifier = Modifier
                            .background(color = Color.LightGray)
                            .fillMaxWidth()
                            .offset { IntOffset(x = 0, y = coercedAnimatedOffset.roundToInt()) }) {
                        Label(
                            text = (animatedStateValue - 1).toString(),
                            modifier = baseLabelModifier
                                .offset(y = -halvedNumbersColumnHeight)
                                .alpha(coercedAnimatedOffset / halvedNumbersColumnHeightPx)
                        )
                    }
                    Card(
                        modifier = Modifier
                            .background(color = Color.LightGray)
                            .fillMaxWidth()
                            .offset { IntOffset(x = 0, y = coercedAnimatedOffset.roundToInt()) }) {
                        Label(
                            text = animatedStateValue.toString(),
                            modifier = baseLabelModifier
                                .alpha(1 - abs(coercedAnimatedOffset) / halvedNumbersColumnHeightPx)
                        )
                    }
                    Card(
                        modifier = Modifier
                            .background(color = Color.LightGray)
                            .fillMaxWidth()
                            .offset { IntOffset(x = 0, y = coercedAnimatedOffset.roundToInt()) }) {
                        Label(
                            text = (animatedStateValue + 1).toString(),
                            modifier = baseLabelModifier
                                .offset(y = halvedNumbersColumnHeight)
                                .alpha(-coercedAnimatedOffset / halvedNumbersColumnHeightPx)
                        )
                    }
                }
            }
            Spacer(modifier = Modifier.height(spacing))
        }
    }

    @Composable
    private fun Label(text: String, modifier: Modifier) {
        Text(
            text = text,
            modifier = modifier.pointerInput(Unit) {
                detectTapGestures(onLongPress = {
                    // FIXME: Empty to disable text selection
                })
            }
        )
    }

    private suspend fun Animatable<Float, AnimationVector1D>.fling(
        initialVelocity: Float,
        animationSpec: DecayAnimationSpec<Float>,
        adjustTarget: ((Float) -> Float)?,
        block: (Animatable<Float, AnimationVector1D>.() -> Unit)? = null,
    ): AnimationResult<Float, AnimationVector1D> {
        val targetValue = animationSpec.calculateTargetValue(value, initialVelocity)
        val adjustedTarget = adjustTarget?.invoke(targetValue)

        return if (adjustedTarget != null) {
            animateTo(
                targetValue = adjustedTarget,
                initialVelocity = initialVelocity,
                block = block
            )
        } else {
            animateDecay(
                initialVelocity = initialVelocity,
                animationSpec = animationSpec,
                block = block,
            )
        }
    }
}
