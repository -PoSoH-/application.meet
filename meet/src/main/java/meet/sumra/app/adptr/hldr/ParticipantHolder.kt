package meet.sumra.app.adptr.hldr

import android.content.res.Resources
import android.graphics.Typeface
import android.graphics.drawable.GradientDrawable
import android.util.TypedValue
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import meet.sumra.app.R
import meet.sumra.app.data.pojo.contacts.ContactHoldState
import meet.sumra.app.databinding.BoxItemParticipantBinding
import meet.sumra.app.interfaces.CnctAction
import meet.sumra.app.interfaces.OnActionListener

class ParticipantHolder(vBinding: ViewBinding): RecyclerView.ViewHolder(vBinding.root) {
    private val v = vBinding as BoxItemParticipantBinding
    internal fun bindInvite(c: ContactHoldState, tf: Typeface, pn: Int, l: OnActionListener){
        val typedValue = TypedValue()
        val theme: Resources.Theme = v.root.context.getTheme()
        v.apply {
            if(pn%2 == 0){
                theme.resolveAttribute(R.attr.meetNowCardItemDark, typedValue, true)
            }else{
                theme.resolveAttribute(R.attr.meetNowCardItemLight, typedValue, true)
            }
            root.setBackgroundColor(typedValue.data)
            inviteItemCnctFullName.typeface = tf
            inviteItemCnctEmail.typeface    = tf
            if(!c.contact.displayName.isNullOrEmpty()) {
                inviteItemCnctFullName.text = c.contact.displayName
            }else{
                inviteItemCnctFullName.text = "${c.contact.firstName} ${c.contact.lastName}"
            }
            if(!c.contact.email.isNullOrEmpty()){
                inviteItemCnctEmail.text = c.contact.email
            }else{
                if(!c.contact.phone.isNullOrEmpty()) {
                    inviteItemCnctEmail.text = c.contact.phone
                }else{
                    inviteItemCnctEmail.text = v.root.context.resources.getString(R.string.frgNoEnteredEmailOrPhone)
                }
            }
            itemCheck(c.isInvite)
        }
        v.root.setOnClickListener { box ->
            c.isInvite = !c.isInvite
            when(c.isInvite){
                true  -> {
                    itemCheck(c.isInvite)
                    l.selectItemCandidate(CnctAction.ToInvite(c.contact))
                }
                false -> {
                    itemCheck(c.isInvite)
                    l.selectItemCandidate(CnctAction.UnInvite(c.contact))
                }
            }
        }
    }
    private fun itemCheck(_is: Boolean){
        when(_is) {
            true -> {v.inviteItemCnctSelect.setBackgroundResource(R.drawable.ic_checkbox_selected)}
            else -> {v.inviteItemCnctSelect.setBackgroundResource(R.drawable.ic_checkbox_emty)}
        }
    }
}