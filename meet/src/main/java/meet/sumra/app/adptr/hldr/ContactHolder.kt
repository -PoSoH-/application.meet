//package meet.sumra.app.adptr.hldr
//
//import androidx.recyclerview.widget.RecyclerView
//import androidx.viewbinding.ViewBinding
//import meet.sumra.app.R
//import meet.sumra.app.data.pojo.contacts.ContactHoldState
////import meet.sumra.app.data.pojo.contacts.ContactItem
//import meet.sumra.app.databinding.BoxItemContactBinding
//import meet.sumra.app.ext.loadUrl
//import meet.sumra.app.interfaces.CnctAction
//import meet.sumra.app.interfaces.OnActionListener
//
//class ContactHolder(vBinding: ViewBinding): RecyclerView.ViewHolder(vBinding.root) {
//
//    private val v = vBinding as BoxItemContactBinding
//
//    internal fun bindInvite(c: ContactHoldState, l: OnActionListener){
//        v.apply {
////            inviteItemCnctSelect.isChecked = false
//            c.contact.avatar?.let{
//                inviteItemCnctImage.loadUrl(it) //(R.drawable.ic_referral_user_add)
//            }
//
//            if(!c.contact.displayName.isNullOrEmpty()) {
//                inviteItemCnctFullName.text = c.contact.displayName
//            }else{
//                inviteItemCnctFullName.text = "${c.contact.firstName} ${c.contact.lastName}"
//            }
//
//            if(!c.contact.email.isNullOrEmpty()){
//                inviteItemCnctEmail.text = c.contact.email
//            }else{
//                if(!c.contact.phone.isNullOrEmpty()) {
//                    inviteItemCnctEmail.text = c.contact.phone
//                }else{
//                    inviteItemCnctEmail.text = v.root.context.resources.getString(R.string.frgNoEnteredEmailOrPhone)
//                }
//            }
//
//            inviteItemCnctSelect.isChecked = c.isInvite
//        }
//        v.root.setOnClickListener { box ->
//            when(c.isInvite){
//                true  -> {
//                    c.isInvite = !c.isInvite
//                    v.inviteItemCnctSelect.isChecked = c.isInvite
//                    l.selectItemCandidate(CnctAction.UnInvite(c.contact))
//                }
//                false -> {
//                    c.isInvite = !c.isInvite
//                    v.inviteItemCnctSelect.isChecked = c.isInvite
//                    l.selectItemCandidate(CnctAction.ToInvite(c.contact))
//                }
//            }
//        }
//    }
//}