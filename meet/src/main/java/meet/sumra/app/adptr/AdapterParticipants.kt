package meet.sumra.app.adptr

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import meet.sumra.app.adptr.hldr.ParticipantHolder
import meet.sumra.app.data.mdl.ContactDataPagination
import meet.sumra.app.data.pojo.contacts.ContactHoldState
import meet.sumra.app.databinding.BoxItemParticipantBinding
import meet.sumra.app.interfaces.CnctAction
import meet.sumra.app.interfaces.ContactOperation
import meet.sumra.app.interfaces.OnActionListener
import meet.sumra.app.interfaces.OnContactInviteListener
import sdk.net.meet.api.contacts.response.FetchContact

class AdapterParticipants(val lr: OnContactInviteListener, val tf: Typeface): RecyclerView.Adapter<ParticipantHolder>() {

    private val contacts = mutableListOf<ContactHoldState>()
    private val filtered = mutableListOf<ContactHoldState>()

    private var pagination: ContactDataPagination? = null
    private var searchMode: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParticipantHolder {
        return ParticipantHolder(BoxItemParticipantBinding
            .inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ParticipantHolder, position: Int) {
        holder.bindInvite(filtered[position], tf, position, object: OnActionListener{
        override fun selectItemCandidate(c: CnctAction) {
            when(c){
                is CnctAction.UnInvite -> {
                    lr.selectItemOperation(ContactOperation.UnInvite(c.c))
                }
                is CnctAction.ToInvite -> {
                    lr.selectItemOperation(ContactOperation.ToInvite(c.c))
                }
            }
        }})
        if (!searchMode) {
            pagination?.apply {
                if (position >= filtered.size - 5) {
                    lr.updatePagination()
                }
            }
        }
    }

    override fun getItemCount() = filtered.size

    internal fun updateContacts(contacts: MutableList<ContactHoldState>){
        this.contacts.clear()
        this.contacts.addAll(contacts)
        this.filtered.clear()
        this.filtered.addAll(this.contacts)
        notifyDataSetChanged()
    }
    internal fun unSelect(c: FetchContact){
        notifyDataSetChanged()
//        contacts.forEach { deacctive ->
//            if(deacctive.contact.equals(c)){
//                deacctive.isInvite = !!deacctive.isInvite
//                notifyDataSetChanged()
//                return@forEach
//            }
//        }
    }
    internal fun unSelectByInvite(c: FetchContact){
        filtered.forEach { deacctive ->
            if(deacctive.contact.equals(c)){
                if(deacctive.isInvite){
                    deacctive.isInvite = !deacctive.isInvite
                }
                notifyDataSetChanged()
                return@forEach
            }
        }
    }

    internal fun updatePagination(pagination: ContactDataPagination){
        this.pagination = pagination
    }

    internal fun enlSearchMode(){ searchMode = true }
    internal fun disSearchMode(){ searchMode = false }
    internal fun isSearchMode() = searchMode

    internal fun getNextPage(): Int? {
        pagination?.apply {
            if(currentPage < lastPage){
                if(links[currentPage].active) {
                    currentPage++
                    return currentPage
                }
            }
        }
        return null
    }

}