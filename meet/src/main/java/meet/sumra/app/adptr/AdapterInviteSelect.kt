package meet.sumra.app.adptr

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import meet.sumra.app.adptr.hldr.ItemSelectBox
import meet.sumra.app.databinding.BoxItemInviteBinding
import meet.sumra.app.interfaces.CnctAction
import meet.sumra.app.interfaces.ContactOperation
import meet.sumra.app.interfaces.OnActionListener
import meet.sumra.app.interfaces.OnContactInviteListener
import sdk.net.meet.api.contacts.response.FetchContact

class AdapterInviteSelect(val lr: OnContactInviteListener): RecyclerView.Adapter<ItemSelectBox>() {

    private val invites = mutableListOf<FetchContact>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemSelectBox {
        return ItemSelectBox(BoxItemInviteBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ItemSelectBox, position: Int) {
        holder.bindInviteItem(invites[position], object: OnActionListener {
            override fun selectItemCandidate(c: CnctAction) {
                when(c){
                    is CnctAction.UnInvite -> {
                        invites.remove(c.c)
                        notifyDataSetChanged()
                        lr.selectItemOperation(ContactOperation.UnInvite(c.c))
                    }
                    else -> Unit
                }
            }
        })
    }

    internal fun clear(){
        invites.clear()
        notifyDataSetChanged()
    }

    override fun getItemCount() = invites.size

    internal fun putInvite(invite: FetchContact){
        this.invites.add(invite)
        notifyDataSetChanged()
    }

    internal fun delInvite(invite: FetchContact){
        this.invites.remove(invite)
        notifyDataSetChanged()
    }

    internal fun updateInvitingList(invites :MutableList<FetchContact>){
        this.invites.clear()
        this.invites.addAll(invites)
        notifyDataSetChanged()
    }

    internal fun fetchAllParticipants(): List<String>{
        val t = mutableListOf<String>()
        invites.forEach {
            t.add(it.id)
        }
        return t
    }

    internal fun isEmpty() = invites.size == 0
    internal fun isIssues() = invites.size > 0

}