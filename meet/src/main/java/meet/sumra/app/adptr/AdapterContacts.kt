//package meet.sumra.app.adptr
//
//import android.view.LayoutInflater
//import android.view.ViewGroup
//import com.futuremind.recyclerviewfastscroll.SectionTitleProvider
//import com.truizlop.sectionedrecyclerview.SimpleSectionedAdapter
//import meet.sumra.app.adptr.hldr.ContactHolder
//import meet.sumra.app.data.mdl.ContactDataPagination
//import meet.sumra.app.data.pojo.contacts.ContactHoldState
//import meet.sumra.app.databinding.BoxItemContactBinding
//import meet.sumra.app.interfaces.CnctAction
//import meet.sumra.app.interfaces.ContactOperation
//import meet.sumra.app.interfaces.OnActionListener
//import meet.sumra.app.interfaces.OnContactInviteListener
//import meet.sumra.app.utils.ContactConfig
//import sdk.net.meet.api.contacts.response.FetchContact
//
//class AdapterContacts(val lr: OnContactInviteListener): SimpleSectionedAdapter<
//        ContactHolder>(), // RecyclerView.Adapter<ContactHolder>(),
//    SectionTitleProvider {
//
//    private val contacts = mutableListOf<MutableList<ContactHoldState>>()
//    private val filtered = mutableListOf<MutableList<ContactHoldState>>()
//    private val letters  = mutableListOf<String>()
//
//    private var pagination: ContactDataPagination? = null
//    private var searchMode: Boolean = false
//
////    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactHolder {
////        return ContactHolder(BoxItemContactBinding
////            .inflate(LayoutInflater.from(parent.context), parent, false))
////    }
//
////    override fun onBindViewHolder(holder: ContactHolder, position: Int) {
////        holder.bindInvite(filtered[position], object: OnActionListener{
////        override fun selectItemCandidate(c: CnctAction) {
////            when(c){
////                is CnctAction.UnInvite -> {
////                    lr.selectItemOperation(ContactOperation.UnInvite(c.c))
////        //                        holder.unSelectContact()
////                }
////                is CnctAction.ToInvite -> {
////                    lr.selectItemOperation(ContactOperation.ToInvite(c.c))
////        //                        holder.toSelectContact()
////                }
////            }
////        }})
////        if (!searchMode) {
////            pagination?.apply {
////                if (position >= filtered.size - 4 && lastPage > currentPage) {
////                    lr.updatePagination()
////                }
////            }
////        }
////    }
//
////    override fun getItemCount() = filtered.size + letters.size -2
//
//    internal fun updateContacts(contacts: MutableList<MutableList<ContactHoldState>>, letters: List<String>){
//        val mCtx = contacts
//
//        this.contacts.clear()
//        this.contacts.addAll(contacts)
//        this.filtered.clear()
//        this.filtered.addAll(this.contacts)
//        this.letters.clear()
//        this.letters.addAll(letters)
//        notifyDataSetChanged()
//    }
//    internal fun unSelect(c: FetchContact){
//        notifyDataSetChanged()
////        contacts.forEach { deacctive ->
////            if(deacctive.contact.equals(c)){
////                deacctive.isInvite = !!deacctive.isInvite
////                notifyDataSetChanged()
////                return@forEach
////            }
////        }
//    }
//    internal fun unSelectByInvite(c: FetchContact){
////        notifyDataSetChanged()
////        filtered.forEach { deacctive ->
////            if(deacctive.contact.equals(c)){
////                if(deacctive.isInvite){
////                    deacctive.isInvite = !deacctive.isInvite
////                }
////                notifyDataSetChanged()
////                return@forEach
////            }
////        }
//    }
//
//    internal fun updateListByFilter(textFilter: String){
//        if(textFilter.length > 0){
//            filtered.clear()
//            contacts.forEach { item ->
////                if(item.contact.displayName.length>=textFilter.length) {
////                    val names = item.contact.displayName.split(" ")
////                    val count = names.size
////                    var symbol = 0
////                    while (symbol < count){
////                        if(names[symbol].length >= textFilter.length){
////                            val tmp: String = names[symbol].substring(0, textFilter.length).lowercase()
////                            if (tmp.equals(textFilter.lowercase())) {
////                                filtered.add(item)
////                                symbol = count
////                            }
////                        }
////                        symbol ++
////                    }
////                }
//            }
//        }else{
//            filtered.clear()
//            filtered.addAll(contacts)
//        }
//        notifyDataSetChanged()
//    }
//
////    private fun getCountry(position: Int): String? {
////        return filtered[position].contact.displayName
////    }
//
//    internal fun updatePagination(pagination: ContactDataPagination){
//        this.pagination = pagination
//    }
//
//    internal fun enlSearchMode(){ searchMode = true }
//    internal fun disSearchMode(){ searchMode = false }
//    internal fun isSearchMode() = searchMode
//
//    internal fun getNextPage(): ContactConfig? {
//        pagination?.apply {
//            if(currentPage < lastPage){
//                if(links[currentPage].active) {
//                    val next = if(currentPage<lastPage) currentPage++ else currentPage
//                    return ContactConfig(
//                        limit = 32, currentPage = currentPage, nextPage = next, lastPage = lastPage, search = null, isFavorite = null, isRecently = null,
//                        byLetter = null, groupId  = null, sortBy  = arrayOf("name"), sortOrder = null)
//                }
//            }else{
//                return ContactConfig(
//                    limit = 32, currentPage = 0, nextPage = 0, lastPage = 0, search = null, isFavorite = null, isRecently = null,
//                    byLetter = null, groupId  = null, sortBy  = arrayOf("name"), sortOrder = null)
//            }
//        }
//        return null
//    }
//
//    override fun getSectionTitle(position: Int): String {
//        if(letters.size == position)
//            return letters[position]
//        else
//            return letters[letters.size-1]
//    }
//
//    override fun getSectionCount(): Int {
//        return filtered.size
//    }
//
//    override fun getItemCountForSection(section: Int): Int {
//        return filtered[section].size
//    }
//
//    override fun onCreateItemViewHolder(parent: ViewGroup, viewType: Int): ContactHolder {
//        return ContactHolder(BoxItemContactBinding
//            .inflate(LayoutInflater.from(parent.context), parent, false))
//    }
//
//    override fun onBindItemViewHolder(holder: ContactHolder, section: Int, position: Int) {
//        holder.bindInvite(filtered[section][position], object: OnActionListener{
//            override fun selectItemCandidate(c: CnctAction) {
//                when(c){
//                    is CnctAction.UnInvite -> {
//                        lr.selectItemOperation(ContactOperation.UnInvite(c.c))
//                        //                        holder.unSelectContact()
//                    }
//                    is CnctAction.ToInvite -> {
//                        lr.selectItemOperation(ContactOperation.ToInvite(c.c))
//                        //                        holder.toSelectContact()
//                    }
//                }
//            }})
////        if (!searchMode) {
////            pagination?.apply {
////                if (p1 >= filtered.size - 4 && lastPage > currentPage) {
////                    lr.updatePagination()
////                }
////            }
////        }
//    }
//
//    override fun getSectionHeaderTitle(section: Int): String {
//        return letters[section]
//    }
//
//}