package meet.sumra.app.adptr.menus

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import meet.sumra.app.R
import meet.sumra.app.databinding.FrgRewardsMenuItemBinding
import meet.sumra.app.interfaces.sealeds.SChartInterval

internal class AMenuChartsInterval(val listener: IMenuItemUpdate): RecyclerView.Adapter<AMenuChartsInterval.MenuItemHolder>() {

    internal interface IMenuItemUpdate{
        fun selectItemMenu(item: SChartInterval)
    }

    private val items: MutableList<SChartInterval> = SChartInterval.getAll()
    private val select: MutableList<SChartInterval> = mutableListOf(SChartInterval.Weeks(null))

    class MenuItemHolder(val viewBind: ViewBinding): RecyclerView.ViewHolder(viewBind.root){
        internal fun bindMenuItem(menu: SChartInterval, isActive:Boolean, iSelectItem:IMenuItemUpdate){
            viewBind.root.let {
                (it as TextView).apply {
                    if(isActive){
                        setBackgroundResource(R.color.gray_400)
                    }else{
                        setBackgroundResource(R.color.gray_300)
                    }

                    text = menu.itemName()

                    setOnClickListener { item ->
                        iSelectItem.selectItemMenu(menu)
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuItemHolder {
        return MenuItemHolder(FrgRewardsMenuItemBinding.inflate(
            LayoutInflater.from(parent.context), parent,false
        ))
    }

    override fun onBindViewHolder(holder: MenuItemHolder, position: Int) {
//        if(select[0] == items[position])
        holder.bindMenuItem(items[position], select[0] == items[position], listener )
    }

    override fun getItemCount() = items.size

    fun getCurrentSelect() = select[0]

    fun setNewItemSelect(item: SChartInterval){
        select.clear()
        select.add(item)
        notifyDataSetChanged()
    }

}