package meet.sumra.app.conf

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.os.LocaleList
import androidx.annotation.RequiresApi
import meet.sumra.app.utils.setup.FontScale
import meet.sumra.app.utils.setup.MeetLocale
import meet.sumra.app.utils.setup.ThemeUtils
import meet.sumra.app.utils.LogUtil
import java.util.*
import javax.inject.Inject
import kotlin.collections.LinkedHashSet

class MeetConfig @Inject constructor(private val context: Context) {

    fun onConfigurationChanged() {
        if (Locale.getDefault().toString() != MeetLocale.applicationLocale.toString()) {
            LogUtil.info(txt = "## onConfigurationChanged(): the locale has been updated to ${Locale.getDefault()}")
            LogUtil.info(txt = "## onConfigurationChanged(): restore the expected value ${MeetLocale.applicationLocale}")
            Locale.setDefault(MeetLocale.applicationLocale)
        }
    }

    fun applyToApplicationContext() {
        val locale = MeetLocale.applicationLocale
        val fontScale = FontScale.getFontScaleValue(context)

        Locale.setDefault(locale)
        val config = Configuration(context.resources.configuration)
        @Suppress("DEPRECATION")
        config.locale = locale
        config.fontScale = fontScale.scale
        @Suppress("DEPRECATION")
        context.resources.updateConfiguration(config, context.resources.displayMetrics)
    }

    /**
     * Compute a localised context
     *
     * @param context the context
     * @return the localised context
     */
    fun getLocalisedContext(context: Context): Context {
        try {
            val locale = MeetLocale.applicationLocale

            // create new configuration passing old configuration from original Context
            val configuration = Configuration(context.resources.configuration)

            configuration.fontScale = FontScale.getFontScaleValue(context).scale

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                setLocaleForApi24(configuration, locale)
            } else {
                configuration.setLocale(locale)
            }
            configuration.setLayoutDirection(locale)
            return context.createConfigurationContext(configuration)
        } catch (e: Exception) {
            LogUtil.info(e.toString(), "## getLocalisedContext() failed")
        }
        return context
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun setLocaleForApi24(config: Configuration, locale: Locale) {
        val set: MutableSet<Locale> = LinkedHashSet()
        // bring the user locale to the front of the list
        set.add(locale)
        val all = LocaleList.getDefault()
        for (i in 0 until all.size()) {
            // append other locales supported by the user
            set.add(all[i])
        }
        val locales = set.toTypedArray()
        config.setLocales(LocaleList(*locales))
    }

    /**
     * Compute the locale status value
     * @return the local status value
     */
//    fun getHash(): String {
//        return (MeetLocale.applicationLocale.toString()
//                + "_" + FontScale.getFontScaleValue(context).preferenceValue
//                + "_" + ThemeUtils.getApplicationTheme(context))
//    }
}