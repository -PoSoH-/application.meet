package meet.sumra.app

object CONST {
    const val APP_PACKAGE = BuildConfig.APP_ID   // "app.sumra.meet" // ::6f1d7082-c437-4796-b974-af70bb3bf3c6"
    val APP_SETTING   by lazy { "df90d04d-17c9-4ff2-aa31-0d0deb1dbe45::SeTtInGs&pArAmS" }
    val KEY_ROOM_ID   by lazy { "jhh667d3-hf7d-kf6f-lf8d-jdy5mal4j6sa::Jkd6e6shd8dha0Q" }

    val KEY_ROOM_INFO by lazy { "df9fyUd6-75gS-kjf5-jf75-ldlHJDhj5hf5::JSdg7cmJLj7cJKD" }
    val KEY_ROOM_TYPE by lazy { "tR9rQUr6-78TT-ldf6-jfu7-nfj9js1d9x0v::HD2c3cmi7dz0SDF" }
    val KEY_ROOM_PASS by lazy { "j9sd6wRr-9823-ks7d-sa5f-Sa4s6kD9s3s2::KsxUxOs7dx5axsQ" }
}