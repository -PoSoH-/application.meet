package meet.sumra.app.ctrl

import android.app.Activity
import android.content.Context
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import meet.sumra.app.R
import meet.sumra.app.data.mdl.MeetHistory
import java.util.*

class ControllerHistory(
    private val ctx: Context,
    private val history: MutableList<MeetHistory>) {

    private lateinit var box: LinearLayout
//    private var loadedPosition: Int = 10

    fun showDateBoxHistory(): LinearLayout {
        box = (ctx as Activity)
            .layoutInflater
            .inflate(R.layout.menu_history_date_box, null, true) as LinearLayout
        box.orientation = LinearLayout.VERTICAL
        box.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT)
        box.removeAllViews()
        history.apply {
            forEach { item ->
                box.addView(createDateBox(item))
            }
        }
        return box
    }

    private fun createDateBox(history: MeetHistory): LinearLayout {
        val item = (ctx as Activity)
            .layoutInflater
            .inflate(R.layout.menu_history_item, null, true) as LinearLayout
        box.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT)
        box.findViewById<TextView>(R.id.itemConferenceName).text = history.meetName
        val calendar = Calendar.getInstance()
        calendar.time = Date(history.startTimeMeet)
        box.findViewById<TextView>(R.id.itemConferenceDateTime).text =
        return item
    }

}