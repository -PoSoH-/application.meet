package meet.sumra.app.di.component

import dagger.Component
import meet.sumra.app.di.module.RoomDatabaseModule
import meet.sumra.app.di.scope.ApiScope
//import meet.sumra.app.sss.RoomMeetDatabase
import xyn.meet.storange.RoomMeetDatabase

@ApiScope
@Component(modules = [RoomDatabaseModule::class])
interface ComponentDatabase {
    val database: RoomMeetDatabase
}