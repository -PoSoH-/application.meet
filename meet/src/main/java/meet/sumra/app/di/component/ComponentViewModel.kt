package meet.sumra.app.di.component

import androidx.fragment.app.Fragment
import dagger.Component
import meet.sumra.app.act.*
import meet.sumra.app.di.module.ModuleViewModel
import meet.sumra.app.di.scope.ViewModelScope
import meet.sumra.app.frg.bottom.FrgCalendar
import meet.sumra.app.frg.bottom.FrgContacts
import meet.sumra.app.frg.bottom.FrgRecent
import meet.sumra.app.frg.bottom.FrgSettings
import meet.sumra.app.frg.drawer.*
import meet.sumra.app.frg.drawer.bonusPlaza.FrgBonusPlaza
import meet.sumra.app.frg.helpers.FrgMeetAdd

@ViewModelScope
@Component(modules      = [ModuleViewModel::class],
           dependencies = [ComponentRepository::class])

interface ComponentViewModel {

    // activities...
    fun injectInit(actInit: ActivityInit)
    fun injectLogin(actLogin: ActivityLogin)
    fun injectHome(actHome: ActivityHome)
    fun injectWeb(actWebView: ActivityWebView)

    fun injectInvite(actInvite: ActMeetInvite)
    fun injectRunning(actRunning: ActMeetRunning)

    // fragments...
    fun inject(that: FrgRecent)
    fun inject(that: FrgContacts)
    fun inject(that: FrgCalendar)
    fun inject(that: FrgSettings)

    fun inject(that: FrgMeetChat)
    fun inject(that: FrgDashboard)

    fun inject(that: FrgStatistic)

    fun inject(that: FrgReferrals)
    fun inject(that: FrgEarnings)

    fun inject(that: FrgRewards)
    fun inject(that: FrgBonusPlaza)
    fun inject(that: FrgProfile)
    fun inject(that: FrgMeetAdd)

    fun inject(that: Fragment)
}