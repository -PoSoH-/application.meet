package meet.sumra.app.di.module

import dagger.Module
import dagger.Provides
import meet.sumra.app.di.scope.RepositoryScope
import meet.sumra.app.repository.Repository
import meet.sumra.app.repository.server.ServerCommunicator
import xyn.meet.storange.RoomMeetDatabase

@Module
class RepositoryModule {
    @RepositoryScope
    @Provides
    internal fun getServerProvider(communicate : ServerCommunicator,
                                   meetDatabase: RoomMeetDatabase
    ): Repository{
        return Repository(communicate, meetDatabase)
    }
}