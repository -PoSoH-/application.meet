package meet.sumra.app.di.module

import dagger.Module
import dagger.Provides
import meet.sumra.app.AppMeet
//import meet.sumra.app.di.scope.StatisticModelScope
import meet.sumra.app.di.scope.ViewModelScope
import meet.sumra.app.repository.Repository
import meet.sumra.app.vm.vm_home.ViewModelHome
import meet.sumra.app.vm.vm_login.ViewModelLogin
import meet.sumra.app.vm.vm_init.ViewModelInit
import meet.sumra.app.vm.vm_meet.ViewModelMeet
import meet.sumra.app.vm.vm_profile.ViewModelProfile
import meet.sumra.app.vm.vm_statistic.ViewModelStatistic

@Module
class ModuleViewModel(val app: AppMeet) {

    @ViewModelScope
    @Provides
    internal fun providesSplashViewModel(repository: Repository): ViewModelInit {
        return ViewModelInit(app, repository)
    }

    @ViewModelScope
    @Provides
    internal fun providesLoginViewModel(repository: Repository): ViewModelLogin {
        return ViewModelLogin(app, repository)
    }

    @ViewModelScope
    @Provides
    internal fun providesHomeViewModel(repository: Repository): ViewModelHome {
        return ViewModelHome(app, repository)
    }

    @ViewModelScope
    @Provides
    internal fun providesViewModelMeet(repository: Repository): ViewModelMeet {
        return ViewModelMeet(app, repository)
    }

    @ViewModelScope
    @Provides
    internal fun providesViewModelProfile(repository: Repository): ViewModelProfile{
        return ViewModelProfile(app, repository)
    }

    @ViewModelScope
    @Provides
    internal fun providesViewModelStatistic(repository: Repository): ViewModelStatistic{
        return ViewModelStatistic(app, repository)
    }

}