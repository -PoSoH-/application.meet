//package meet.sumra.app.di.component
//
//import dagger.Component
//import meet.sumra.app.di.module.ModuleViewModelStatistic
//import meet.sumra.app.di.scope.StatisticModelScope
//import meet.sumra.app.frg.drawer.*
//
//@StatisticModelScope
//@Component(modules      = [ModuleViewModelStatistic::class],
//           dependencies = [ComponentRepository::class])
//interface ComponentViewStatistic {
//
//    fun inject(that: FrgStatistic)
//}