package meet.sumra.app.di

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

object DefaultSharedPreferences {
    @Volatile private var INSTANCE: SharedPreferences? = null
    fun getInstance(context: Context): SharedPreferences =
        INSTANCE ?: synchronized(this) {
            INSTANCE ?: PreferenceManager
                .getDefaultSharedPreferences(context.applicationContext).also { INSTANCE = it }
        }
}