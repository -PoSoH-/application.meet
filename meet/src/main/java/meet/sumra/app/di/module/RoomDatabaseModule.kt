package meet.sumra.app.di.module

import dagger.Module
import dagger.Provides
import xyn.meet.storange.RoomMeetDatabase

//import meet.sumra.app.sss.RoomMeetDatabase

@Module
class RoomDatabaseModule (private val databaseModule: RoomMeetDatabase) {
    @Provides
    internal fun providesRoomDatabase(): RoomMeetDatabase {
        return databaseModule
    }
}
