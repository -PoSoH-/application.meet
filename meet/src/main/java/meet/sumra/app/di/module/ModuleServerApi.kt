package meet.sumra.app.di.module

import dagger.Module
import dagger.Provides
import meet.sumra.app.di.scope.ApiScope
import sdk.net.meet.api.NetworkServices
import meet.sumra.app.repository.server.ServerCommunicator

@Module
class ModuleServerApi(private val networkServices: NetworkServices) {
    @ApiScope
    @Provides
    fun provideCommunicator(): ServerCommunicator {
        return ServerCommunicator(networkServices)
    }

//    @Module
//    class RoomDatabaseModule (private val databaseModule: RoomMeetDatabase) {
//        @Provides
//        internal fun providesRoomDatabase(): RoomMeetDatabase {
//            return databaseModule
//        }
//    }

//    @Provides
//    @ApiScope
//    fun provideApiService(retrofit: Retrofit): ApiService {
//        return retrofit.create<ApiService>(ApiService::class.kotlin!!)
//    }

//    @Provides
//    @ApiScope
//    fun provideRetrofit(builder: Retrofit.Builder): Retrofit {
//        return builder.baseUrl(API_URL).build()
//    }

//    @ApiScope
//    @Provides
//    fun providesRetrofitBuilder(): Retrofit.Builder {
//        val builder = OkHttpClient.Builder().apply {
//            connectionPool(
//                connectionPool = ConnectionPool(
//                    maxIdleConnections = 5,
//                    keepAliveDuration  = 30,
//                    timeUnit           = TimeUnit.SECONDS
//                )
//            )
//            connectTimeout(timeout = 30, TimeUnit.SECONDS)
//            readTimeout(   timeout = 30, TimeUnit.SECONDS)
//            writeTimeout(  timeout = 30, TimeUnit.SECONDS)
//            if(BuildConfig.DEBUG){
//                addInterceptor(
//                    HttpLoggingInterceptor().apply{
//                        setLevel(HttpLoggingInterceptor.Level.BODY)
//                    }
//                )
//            }
//        }
//        return Retrofit.Builder().apply {
//            client(builder.build())
//            addConverterFactory(GsonConverterFactory.create())
//            }
//    }

//    companion object {
//        private val API_URL = "https://meet.sumra.net/"
//    }

}