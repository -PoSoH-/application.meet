package meet.sumra.app.di.component

import dagger.Component
import meet.sumra.app.di.module.RepositoryModule
import meet.sumra.app.di.scope.RepositoryScope
import meet.sumra.app.repository.Repository

@RepositoryScope
@Component(modules      = [RepositoryModule  ::class],
           dependencies = [ComponentServerApi::class,
                           ComponentDatabase ::class])
interface ComponentRepository {
    val repository: Repository
}