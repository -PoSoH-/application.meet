package meet.sumra.app.di.component

import dagger.Component
import meet.sumra.app.di.module.ModuleServerApi
import meet.sumra.app.di.scope.ApiScope
import meet.sumra.app.repository.server.ServerCommunicator

@ApiScope
@Component(modules = [ModuleServerApi::class])
interface ComponentServerApi {
    val serverCommunicator: ServerCommunicator
}