package meet.sumra.app.interfaces

import sdk.net.meet.api.contacts.response.FetchContact


internal interface OnActionListener {
    fun selectItemCandidate(c: CnctAction)
}

sealed class CnctAction {
    class ToInvite(val c: FetchContact): CnctAction()
    class UnInvite(val c: FetchContact): CnctAction()
}