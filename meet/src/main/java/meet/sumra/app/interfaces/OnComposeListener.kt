package meet.sumra.app.interfaces

interface OnComposeListener {
    fun click(state: Boolean)
}