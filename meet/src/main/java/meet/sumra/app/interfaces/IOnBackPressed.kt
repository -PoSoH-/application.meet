package meet.sumra.app.interfaces

interface IOnBackPressed {
    fun onBackPressed(): Boolean
}