package meet.sumra.app.interfaces

import meet.sumra.app.vm.vm_home.StateHome

interface SelectDrawerItemListener {
    fun selected(position: Int, state: StateHome)
}