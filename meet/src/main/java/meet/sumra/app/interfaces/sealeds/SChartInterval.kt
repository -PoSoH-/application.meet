package meet.sumra.app.interfaces.sealeds

sealed class SChartInterval {
    abstract fun itemName(): CharSequence?
    data class Days  (val interval: String?):SChartInterval(){
        private val name = "Day"
        override fun itemName() = name}
    data class Weeks (val interval: String?): SChartInterval(){
        private val name = "Week"
        override fun itemName() = name}
    data class Months(val interval: String?): SChartInterval(){
        private val name = "Month"
        override fun itemName() = name}
    data class Years (val interval: String?): SChartInterval(){
        private val name = "Year"
        override fun itemName() = name}

    companion object{
        fun getAll():MutableList<SChartInterval>{
            return mutableListOf(
                Days(null),
                Weeks(null),
                Months(null),
                Years(null)
            )
        }
    }
//    sealed interface MenuData{
//        fun itemName(): String
//    }
}
