package meet.sumra.app.interfaces

import android.net.Uri

interface UrlListener {
    fun onImageReady(uri: Uri?)
}