

package meet.sumra.app.interfaces

internal interface Task<PARAMS, RESULT> {
    suspend fun execute(params: PARAMS): RESULT
}