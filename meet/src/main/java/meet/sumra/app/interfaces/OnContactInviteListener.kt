package meet.sumra.app.interfaces

import sdk.net.meet.api.contacts.response.FetchContact

interface OnContactInviteListener {
    fun selectItemOperation(c: ContactOperation)
    fun updatePagination()
}

sealed class ContactOperation {
    class ToInvite(val c: FetchContact): ContactOperation()
    class UnInvite(val c: FetchContact): ContactOperation()
}