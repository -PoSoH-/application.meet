package meet.sumra.app.act

import androidx.annotation.StyleRes
import meet.sumra.app.R


sealed class ActivityOtherThemes(@StyleRes val dark: Int,
                                 @StyleRes val light: Int) {

    object Default : ActivityOtherThemes(
        R.style.app_theme_dark,
        R.style.app_theme_light
    )
//    object Launcher : ActivityOtherThemes(
//            R.style.AppTheme_Launcher,
//            R.style.AppTheme_Launcher
//    )
//
//    object AttachmentsPreview : ActivityOtherThemes(
//            R.style.AppTheme_AttachmentsPreview,
//            R.style.AppTheme_AttachmentsPreview
//    )
//
//    object VectorAttachmentsPreview : ActivityOtherThemes(
//            R.style.AppTheme_Transparent,
//            R.style.AppTheme_Transparent
//    )
}