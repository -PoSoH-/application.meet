package meet.sumra.app.act.jet

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import meet.sumra.app.utils.LogUtil
import meet.sumra.app.R
import okhttp3.internal.wait

//object jetChooseView {

    private val btnHeight = 40

    @Preview
    @Composable
    @ExperimentalMaterialApi
    private fun ShowPreview(){
//        FetchDialogChoose(isShow = BottomSheetState(BottomSheetValue.Expanded), modifier = Modifier.fillMaxSize(1.0f))
    }

    @Composable
    @ExperimentalMaterialApi
    fun FetchDialogChoose2(
        state: MutableState<Boolean>,
        clickChoose: () -> Unit,
        clickSelect: () -> Unit,
        clickCancel: () -> Unit,
        modifier: Modifier
    ) {
        if(state.value) {
            Surface(
                modifier = modifier,
                color = Color(red = 255, green = 255, blue = 255, alpha = 20),
            ) {
                Row(
                    verticalAlignment = Alignment.Bottom
                ) {
                    Surface(
                        modifier = Modifier
                            .fillMaxWidth(1f)
                            .padding(all = 25.dp),
                        shape = RoundedCornerShape(topStart = 35.dp, topEnd = 35.dp),
                        color = Color.White
                    ) {
                        Column(
                            modifier = Modifier.fillMaxWidth(1f)
                        ) {


                            // Button select
                            Surface(
                                color = Color.Blue,
                                shape = RoundedCornerShape(8.dp),
                                modifier = Modifier
                                    .fillMaxWidth(1f)
                                    .height(btnHeight.dp),
                                onClick = {
                                    clickSelect
                                }
                            ) {
                                Text(text = stringResource(id = R.string.button_confirmation_code))
                            }
                            // Button choose
                            Surface(
                                color = Color.Blue,
                                shape = RoundedCornerShape(8.dp),
                                modifier = Modifier
                                    .fillMaxWidth(1f)
                                    .height(btnHeight.dp),
                                onClick = {
                                    clickChoose
                                }
                            ) {

                            }
                            // Button close...
                            Surface(
                                color = Color.Red,
                                shape = RoundedCornerShape(8.dp),
                                modifier = Modifier
                                    .fillMaxWidth(1f)
                                    .height(btnHeight.dp),
                                onClick = {
                                    clickCancel
                                }
                            ) {


                            }
                        }
                    }
                }
            }
        }
    }


/***
    @Composable
    @ExperimentalMaterialApi
    fun FetchDialogChoose(
    //        isShow: BottomSheetState,
        scope: CoroutineScope,
        state: MutableState<BottomSheetState>,
        clickChoose: () -> Unit,
        clickSelect: () -> Unit,
        clickCancel: () -> Unit,
        modifier: Modifier) {

        val bottomSheetScaffoldState = rememberBottomSheetScaffoldState(
            bottomSheetState = state.value
        )

        BottomSheetScaffold(
            scaffoldState = bottomSheetScaffoldState,
            modifier = modifier,
            sheetContent = {
                Box(
                    Modifier
                        .fillMaxWidth()
                        .height(200.dp)
                ) {
                    Text(text = "Hello from sheet")
                }
            }, sheetPeekHeight = 0.dp
        ) {
            Row(
                verticalAlignment = Alignment.Bottom
            ) {
                // Button select
                Surface(
                    color = Color.Blue,
                    shape = RoundedCornerShape(8.dp),
                    modifier = Modifier
                        .fillMaxWidth(1f)
                        .height(btnHeight.dp),
                    onClick = {
                        clickSelect
                    }
                ) {
                    Text(text = stringResource(id = R.string.button_confirmation_code))
                }
                // Button choose
                Surface(
                    color = Color.Blue,
                    shape = RoundedCornerShape(8.dp),
                    modifier = Modifier
                        .fillMaxWidth(1f)
                        .height(btnHeight.dp),
                    onClick = {
                        clickChoose
                    }
                ) {

                }
                // Button close...
                Surface(
                    color = Color.Red,
                    shape = RoundedCornerShape(8.dp),
                    modifier = Modifier
                        .fillMaxWidth(1f)
                        .height(btnHeight.dp),
                    onClick = {
                        clickCancel
                    }
                ) {


                }
            }



            Button(onClick = {
                scope.launch {
                    if (bottomSheetScaffoldState.bottomSheetState.isCollapsed) {
                        bottomSheetScaffoldState.bottomSheetState.expand()

                    } else {
                        bottomSheetScaffoldState.bottomSheetState.collapse()

                    }
                }
            }) {
                Text(text = "Expand/Collapse Bottom Sheet")
            }
        }
    }
***/


//        if( isShow.isExpanded) {
//            val bottomSheetScaffoldState = rememberBottomSheetScaffoldState(
//                bottomSheetState = BottomSheetState(BottomSheetValue.Expanded)
//            )
//            val coroutineScope = rememberCoroutineScope()
//            Surface(
//                modifier = modifier,
//                color = Color.Cyan
//            ) {
//                BottomSheetScaffold(sheetContent = bottomSheetScaffoldState.drawerState) {
//                    Row (
//                        verticalAlignment = Alignment.Bottom
//                    ){
//                                // Button select
//                        Surface(
//                            color = Color.Blue,
//                            shape = RoundedCornerShape(8.dp),
//                            modifier = Modifier
//                                .fillMaxWidth(1f)
//                                .height(btnHeight.dp),
//                            onClick = {
//
//                            }
//                        ) {
//                            Text(text = stringResource(id = R.string.button_confirmation_code))
//                        }
//                        // Button choose
//                        Surface(
//                            color = Color.Blue,
//                            shape = RoundedCornerShape(8.dp),
//                            modifier = Modifier
//                                .fillMaxWidth(1f)
//                                .height(btnHeight.dp),
//                            onClick = {
//
//                            }
//                        ) {
//
//
//
//                        }
//                        // Button close...
//                        Surface(
//                            color = Color.Red,
//                            shape = RoundedCornerShape(8.dp),
//                            modifier = Modifier
//                                .fillMaxWidth(1f)
//                                .height(btnHeight.dp),
//                            onClick = {
//
//                            }
//                        ) {
//
//
//
//                        }
//
//
//                    }
//                }
//            }
//        }


//}