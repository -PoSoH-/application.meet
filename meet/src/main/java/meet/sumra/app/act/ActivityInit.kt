package meet.sumra.app.act

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import meet.sumra.app.databinding.ActInitialBinding
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.utils.LogUtil
import meet.sumra.app.vm.vm_init.StateInit
import meet.sumra.app.vm.vm_init.ViewModelInit
import javax.inject.Inject
import android.content.Intent

import android.content.SharedPreferences
import android.net.Uri
import meet.sumra.app.data.enums.ELinkToSend


class ActivityInit: ActivityBase<ActInitialBinding>() {

    var vmInitial: ViewModelInit? = null
        @Inject set

    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)

//        vmInitial.checkReferral()

        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        actionBar?.hide()

//        if (!UserConfig.getInstance(currentAccount).isClientActivated()) {
            val intent = intent
            var isProxy = false
            if (intent != null && intent.action != null) {
                if (Intent.ACTION_SEND == intent.action || Intent.ACTION_SEND_MULTIPLE == intent.action) {
                    super.onCreate(savedInstanceState)
                    finish()
                    return
                } else if (Intent.ACTION_VIEW == intent.action) {
                    val uri: Uri? = intent.data
                    if (uri != null) {
                        val url: String = uri.toString().lowercase()
                        isProxy =
                               url.startsWith("http://sumrameet.com")
                            || url.startsWith("https://sumrameet.com")
                    }
                }
            }
        super.onCreate(savedInstanceState)

//            var preferences: SharedPreferences = MessagesController.getGlobalMainSettings()
//            val crashed_time = preferences.getLong("intro_crashed_time", 0)
//            val fromIntro = intent != null && intent.getBooleanExtra("fromIntro", false)
//            if (fromIntro) {
//                preferences.edit().putLong("intro_crashed_time", 0).commit()
//            }
//            if (!isProxy && Math.abs(crashed_time - System.currentTimeMillis()) >= 60 * 2 * 1000 && intent != null && !fromIntro) {
//                preferences = ApplicationLoader.applicationContext.getSharedPreferences(
//                    "logininfo2",
//                    MODE_PRIVATE
//                )
//                val state = preferences.all
//                if (state.isEmpty()) {
//                    val intent2 = Intent(this, IntroActivity::class.java)
//                    intent2.data = intent.data
//                    startActivity(intent2)
//                    super.onCreate(savedInstanceState)
//                    finish()
//                    return
//                }
//            }
////        }

        if(isProxy){
            ActMeetInvite.getInstance(activity = this, type = ELinkToSend.URL, data = intent.data.toString(), pass = null)
        }else {
            vmInitial?.viewState?.observe(
                this,
                Observer<StateInit> { state ->
                    when (state) {
                        is StateInit.CheckSession -> {
                            LogUtil.info(tag = "ACTIVITY.INIT :: ", txt = "${StateInit.CheckSession::class.java.simpleName}")
//                        showSnackbar("${StateInit.CheckSession::class.kotlin.simpleName }")
                        }
                        is StateInit.GoToLogin -> {
                            LogUtil.info(tag = "ACTIVITY.INIT :: ", txt = "${StateInit.GoToLogin::class.java.simpleName}")
//                        showSnackbar("${StateInit.GoToLogin::class.kotlin.simpleName }")
                            ActivityLogin.getInstance(this@ActivityInit)
                        }
                        is StateInit.GoToHome -> {
                            LogUtil.info(tag = "ACTIVITY.INIT :: ", txt = "${StateInit.GoToHome::class.java.simpleName}")
                            ActivityHome.getInstance(this@ActivityInit)
                        }
                        is StateInit.Success -> {
                            LogUtil.info(tag = "ACTIVITY.INIT :: ", txt = "${StateInit.Success::class.java.simpleName}")
//                        showSnackbar("${StateInit.Success::class.kotlin.simpleName }")
                        }
                        is StateInit.Failure -> {
                            LogUtil.info(tag = "ACTIVITY.INIT :: ", txt = "${StateInit.Failure::class.java.simpleName}")
//                        showSnackbar("${StateInit.Failure::class.kotlin.simpleName }")
                        }
                    }
                }
            )
        }
    }

    override fun getBinding(): ActInitialBinding {
        return ActInitialBinding.inflate(layoutInflater)
    }

    override fun injectDependency(component: ComponentViewModel) {
        component.injectInit(this@ActivityInit)
    }
}