package meet.sumra.app.act

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.provider.UserDictionary.Words.APP_ID
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.gson.Gson
import meet.sumra.app.R
import meet.sumra.app.databinding.ActLoginBinding
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.ext.*
import meet.sumra.app.ext.ExtAnimation.hideUtoD
import meet.sumra.app.ext.ExtAnimation.showDtoU
import meet.sumra.app.ext.addFragment
import meet.sumra.app.ext.addFragmentToBackStack
import meet.sumra.app.ext.backFragment
import meet.sumra.app.ext.replaceFragment
import meet.sumra.app.frg.FragmentBase
import meet.sumra.app.utils.*
import meet.sumra.app.vm.vm_login.StateLogin
import meet.sumra.app.vm.vm_login.ViewModelLogin
import net.sumra.auth.ControllerListener
import net.sumra.auth.ControllerWrapper
import net.sumra.auth.Session
import net.sumra.auth.helpers.Finals
import net.sumra.auth.states.MessageTypeEvents
import sdk.net.meet.BuildConfig
import java.util.*
import javax.inject.Inject

open class ActivityLogin : ActivityBase<ActLoginBinding>(), ControllerListener {

    companion object {
        fun getInstance(activity: Activity){
            activity.startActivity(Intent(activity, ActivityLogin::class.java))
            activity.finish()
        }
    }

    private lateinit var controllerWrapper: ControllerWrapper

    var viewLoginModel: ViewModelLogin? = null
    @Inject set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        AppMeet.getInstance().meetComponent.inject(this)
//        bind = ActLoginBinding.inflate(layoutInflater)
//        setContentView(bind.root)
//        loginViewModel.viewState.observe(this){  }

        viewLoginModel?.viewState?.observe(this@ActivityLogin, state())

        controllerWrapper = ControllerWrapper.Builder().apply {
            _logoResId  = R.drawable.ic_sumrameet_icon
            _congratsIconId = R.drawable.ic_congrats_icon
            _identifyApp    = "meet.sumra.app" // BuildConfig.APP_ID
            _lightColor = resources.getColor(R.color.blue_200, resources.newTheme())
            _darkColor  = resources.getColor(R.color.blue_600, resources.newTheme())

            _thumbEnable  = resources.getColor(R.color.white, resources.newTheme())
            _thumbDisable = resources.getColor(R.color.white, resources.newTheme())
            _trackEnable  = resources.getColor(R.color.blue_600,   resources.newTheme())
            _trackDisable = resources.getColor(R.color.gray_track, resources.newTheme())

            _textColPrimDark  = resources.getColor(R.color.text_800, resources.newTheme())
            _textColNormDark  = resources.getColor(R.color.text_600, resources.newTheme())
            _textColPrimary   = resources.getColor(R.color.text_400, resources.newTheme())
            _textColLight     = resources.getColor(R.color.text_200, resources.newTheme())
            _textColSelect    = resources.getColor(R.color.blue_600, resources.newTheme())
            _checkedFigure    = R.drawable.ic_meet_check
            _fontAssetsPath   = Finals.FONT_FACE
            _backgroundColor  = resources.getColor(R.color.meetBackgroundLight, resources.newTheme())
            _codeColorFocusNo = resources.getColor(R.color.white,    resources.newTheme())
            _codeColorFocusOk = resources.getColor(R.color.blue_400, resources.newTheme())
            context          = getContext()
        }.build()

        controllerWrapper.let {
            addFrg(fragment = controllerWrapper.checkFirstPage())
        }

//        if(BuildConfig.DEBUG){
//            views.buttonGoUp.visibility = View.VISIBLE
//            views.buttonGoUp.setOnClickListener(object : View.OnClickListener{
//                override fun onClick(v: View?) {
//                    viewLoginModel?.updateSessionToken(Gson().toJson(
//                                                Session(token      = UUID.randomUUID().toString(),
//                                                        userName   = "AlligatorLocal",
//                                                        code       = "TESTER",
//                                                        homeServer = "localhost::/meet.test.net",
//                                                        homeUserId = UUID.randomUUID().toString(),
//                                                        avatar     = null)))
//                }
//            })
//        }
    }

    private fun getContext() = this

    private fun state() = Observer<StateLogin> { state ->
        when(state){
            is StateLogin.Session -> { LogUtil.info(txt = "${StateLogin.Session::class.java.simpleName }") }
            is StateLogin.Section -> { LogUtil.info(txt = "${StateLogin.Section::class.java.simpleName }") }
            is StateLogin.Success -> { LogUtil.info(txt = "${StateLogin.Success::class.java.simpleName }") }
            is StateLogin.Failure -> { LogUtil.info(txt = "${StateLogin.Failure::class.java.simpleName }") }
            is StateLogin.ToHome  -> {
                LogUtil.info(txt = "${StateLogin.ToHome::class.java.simpleName }")
                ActivityHome.getInstance(this@ActivityLogin)
            }
        }
    }

    override fun updateControllerState(
        fragment : Fragment?,
        params   : Parcelable?,
        addBack  : Boolean,
        isBack   : Boolean
    ) {
        if(isBack){
            backFrg(fragment)
        }else{
            if (fragment == null) {
                viewLoginModel?.updateSessionToken(
                    sessionContent = Gson()
                        .toJson(params as Session))
                ActivityHome.getInstance(this)
            } else {
                if(addBack) {
                    if(params == null) {
                        addFrgBackStack(fragment = fragment, tag = fragment.javaClass.simpleName)
                    }else{
                        addFrgParamsBackStack(fragment = fragment, params = params, tag = fragment.javaClass.simpleName)
                    }
                }else{
                    replaceFrg(fragment = fragment, tag = fragment.javaClass.simpleName)
                }
            }
        }
    }

    override fun sendMessageToSystem(type: MessageTypeEvents) {
        when(type){
            is MessageTypeEvents.MessageSuccess -> Toast.makeText(this, type.message, Toast.LENGTH_SHORT).show()
            is MessageTypeEvents.MessageFailure -> Toast.makeText(this, type.message, Toast.LENGTH_SHORT).show()
            is MessageTypeEvents.MessageSimple  -> Toast.makeText(this, type.message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun checkProgressState(state: Boolean) {
        when(state) {
            true -> views.activityProgress.showDtoU()
            else -> views.activityProgress.hideUtoD()
        }
    }

    override fun checkPermissionContacts() {
        if (checkPermissions(PERMISSIONS_FOR_MEMBERS_SEARCH,
                this,
                PERMISSION_REQUEST_CODE_READ_CONTACTS,
                0)) {
            controllerWrapper.nextStepsReferralContacts()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (allGranted(grantResults)) {
            if (requestCode == PERMISSION_REQUEST_CODE_READ_CONTACTS) {
                controllerWrapper.nextStepsReferralContacts()
            }
        }
    }

    private fun addFrg(
        frameId: Int = R.id.frgContainer,
        fragment: FragmentBase<*>,
        allowStateLoss: Boolean = false) {
        addFragment(
            frameId = frameId,
            fragment = fragment,
            allowStateLoss = allowStateLoss)
    }

    private fun addFrg(
        frameId: Int = R.id.frgContainer,
        fragment: Fragment,
        allowStateLoss: Boolean = false) {
        addFragment(
            frameId = frameId,
            fragment = fragment,
            allowStateLoss = allowStateLoss)
    }

    private fun replaceFrg(
        frameId: Int = R.id.frgContainer,
        fragment: FragmentBase<*>,
        tag: String?,
        allowStateLoss: Boolean = false
    ) {
        for (i in 0 until supportFragmentManager.getBackStackEntryCount()) {
            supportFragmentManager.popBackStack()
        }
        replaceFragment(
            frameId  = frameId,
            fragment = fragment,
            tag      = tag,
            allowStateLoss = allowStateLoss
        )
    }
    private fun replaceFrg(
        frameId: Int = R.id.frgContainer,
        fragment: Fragment,
        tag: String?,
        allowStateLoss: Boolean = false
    ) {
        for (i in 0 until supportFragmentManager.getBackStackEntryCount()) {
            supportFragmentManager.popBackStack()
        }
        replaceFragment(
            frameId  = frameId,
            fragment = fragment,
            tag      = tag,
            allowStateLoss = allowStateLoss
        )
    }

    private fun addFrgBackStack(
        frameId: Int = R.id.frgContainer,
        fragment: FragmentBase<*>,
        tag: String?,
        allowStateLoss: Boolean = false
    ) {
        addFragmentToBackStack(
            frameId  = frameId,
            fragment = fragment,
            tag      = tag,
            allowStateLoss = allowStateLoss
        )
    }
    private fun addFrgBackStack(
        frameId: Int = R.id.frgContainer,
        fragment: Fragment,
        tag: String?,
        allowStateLoss: Boolean = false
    ) {
        addFragmentToBackStack(
            frameId  = frameId,
            fragment = fragment,
            tag      = tag,
            allowStateLoss = allowStateLoss
        )
    }

    private fun addFrgParamsBackStack(
        frameId: Int = R.id.frgContainer,
        fragment: FragmentBase<*>,
        params: Parcelable,
        tag: String?,
        allowStateLoss: Boolean = false
    ) {
        addFragmentToBackStack(
            frameId  = frameId,
            fragmentClass = fragment::class.java,
            params = params,
            tag      = tag,
            allowStateLoss = allowStateLoss
        )
    }
    private fun addFrgParamsBackStack(
        frameId: Int = R.id.frgContainer,
        fragment: Fragment,
        params: Parcelable,
        tag: String?,
        allowStateLoss: Boolean = false
    ) {
        addFragmentToBackStack(
            frameId  = frameId,
            fragmentClass = fragment::class.java,
            params = params,
            tag      = tag,
            allowStateLoss = allowStateLoss
        )
    }

    private fun backFrg(fragment: FragmentBase<*>?){
        if(fragment != null){
            replaceFrg(fragment = fragment, tag = fragment.tag)
        }else {
            backFragment(tag = null)
        }
    }
    private fun backFrg(fragment: Fragment?){
        if(fragment != null){
            replaceFrg(fragment = fragment, tag = fragment.tag)
        }else {
            backFragment(tag = null)
        }
    }

//    override fun getBinding() = ActLoginBinding.inflate(layoutInflater)

    override fun injectDependency(component: ComponentViewModel) {
        component.injectLogin(this@ActivityLogin)
    }

    override fun getBinding(): ActLoginBinding {
        return ActLoginBinding.inflate(layoutInflater)
    }
}