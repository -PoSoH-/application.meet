////
//// Source code recreated from a .class file by IntelliJ IDEA
//// (powered by FernFlower decompiler)
////
//package com.facebook.react
//
//import android.annotation.SuppressLint
//import android.content.Context
//import android.graphics.Canvas
//import android.graphics.Rect
//import android.os.Build.VERSION
//import android.os.Bundle
//import android.util.AttributeSet
//import android.util.DisplayMetrics
//import android.view.*
//import android.view.ViewTreeObserver.OnGlobalLayoutListener
//import android.widget.FrameLayout
//import com.facebook.common.logging.FLog
//import com.facebook.infer.annotation.Assertions
//import com.facebook.react.bridge.*
//import com.facebook.react.common.MapBuilder
//import com.facebook.react.common.annotations.VisibleForTesting
//import com.facebook.react.modules.appregistry.AppRegistry
//import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter
//import com.facebook.react.modules.deviceinfo.DeviceInfoModule
//import com.facebook.react.uimanager.*
//import com.facebook.systrace.Systrace
//import org.jitsi.meet.sdk.JitsiMeet
//import org.jitsi.meet.sdk.JitsiMeetConferenceOptions
//import org.jitsi.meet.sdk.JitsiMeetUserInfo
//import java.lang.Exception
//import java.lang.RuntimeException
//
//class ActMeetInvite : FrameLayout, RootView, ReactRoot {
//    var reactInstanceManager: ReactInstanceManager? = null
//        private set
//    private var mJSModuleName: String? = null
//    private var mAppProperties: Bundle? = null
//    private var mInitialUITemplate: String? = null
//    private var mCustomGlobalLayoutListener: CustomGlobalLayoutListener? = null
//    private var mRootViewEventListener: ReactRootViewEventListener? = null
//    private var mRootViewTag = 0
//    private var mIsAttachedToInstance = false
//    private var mShouldLogContentAppeared = false
//    private var mJSTouchDispatcher: JSTouchDispatcher? = null
//    private val mAndroidHWInputDeviceHelper = CustomAndroidHWInputDeviceHelper(this)
//    private var mWasMeasured = false
//    private var mWidthMeasureSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
//    private var mHeightMeasureSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
//    private var mLastWidth = 0
//    private var mLastHeight = 0
//    private var mUIManagerType = 1
//    private val mUseSurface: Boolean
//
//    constructor(context: Context?) : super(context!!) {
//        mUseSurface = false
//        init()
//    }
//
//    constructor(context: Context?, useSurface: Boolean) : super(context!!) {
//        mUseSurface = useSurface
//        init()
//    }
//
//    constructor(context: Context?, attrs: AttributeSet?) : super(
//        context!!, attrs
//    ) {
//        mUseSurface = false
//        init()
//    }
//
//    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(
//        context!!, attrs, defStyle
//    ) {
//        mUseSurface = false
//        init()
//    }
//
//    private fun init() {
//        this.clipChildren = false
//    }
//
//    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
//        if (mUseSurface) {
//            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
//        } else {
//            Systrace.beginSection(0L, "ReactRootView.onMeasure")
//            try {
//                val measureSpecsUpdated =
//                    widthMeasureSpec != mWidthMeasureSpec || heightMeasureSpec != mHeightMeasureSpec
//                mWidthMeasureSpec = widthMeasureSpec
//                mHeightMeasureSpec = heightMeasureSpec
//                var width = 0
//                var height = 0
//                val widthMode = MeasureSpec.getMode(widthMeasureSpec)
//                var i: Int
//                if (widthMode != -2147483648 && widthMode != 0) {
//                    width = MeasureSpec.getSize(widthMeasureSpec)
//                } else {
//                    i = 0
//                    while (i < this.childCount) {
//                        val child = getChildAt(i)
//                        val childSize =
//                            child.left + child.measuredWidth + child.paddingLeft + child.paddingRight
//                        width = Math.max(width, childSize)
//                        ++i
//                    }
//                }
//                i = MeasureSpec.getMode(heightMeasureSpec)
//                if (i != -2147483648 && i != 0) {
//                    height = MeasureSpec.getSize(heightMeasureSpec)
//                } else {
//                    for (i in 0 until this.childCount) {
//                        val child = getChildAt(i)
//                        val childSize =
//                            child.top + child.measuredHeight + child.paddingTop + child.paddingBottom
//                        height = Math.max(height, childSize)
//                    }
//                }
//                setMeasuredDimension(width, height)
//                mWasMeasured = true
//                if (reactInstanceManager != null && !mIsAttachedToInstance) {
//                    attachToReactInstanceManager()
//                } else if (measureSpecsUpdated || mLastWidth != width || mLastHeight != height) {
//                    updateRootLayoutSpecs(mWidthMeasureSpec, mHeightMeasureSpec)
//                }
//                mLastWidth = width
//                mLastHeight = height
//            } finally {
//                Systrace.endSection(0L)
//            }
//        }
//    }
//
//    override fun onChildStartedNativeGesture(androidEvent: MotionEvent) {
//        if (reactInstanceManager != null && mIsAttachedToInstance && reactInstanceManager!!.currentReactContext != null) {
//            if (mJSTouchDispatcher == null) {
//                FLog.w(
//                    "ReactNative",
//                    "Unable to dispatch touch to JS before the dispatcher is available"
//                )
//            } else {
//                val reactContext = reactInstanceManager!!.currentReactContext
//                val eventDispatcher = (reactContext!!.getNativeModule(
//                    UIManagerModule::class.java
//                ) as UIManagerModule).eventDispatcher
//                mJSTouchDispatcher!!.onChildStartedNativeGesture(androidEvent, eventDispatcher)
//            }
//        } else {
//            FLog.w(
//                "ReactNative",
//                "Unable to dispatch touch to JS as the catalyst instance has not been attached"
//            )
//        }
//    }
//
//    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
//        dispatchJSTouchEvent(ev)
//        return super.onInterceptTouchEvent(ev)
//    }
//
//    override fun onTouchEvent(ev: MotionEvent): Boolean {
//        dispatchJSTouchEvent(ev)
//        super.onTouchEvent(ev)
//        return true
//    }
//
//    override fun dispatchDraw(canvas: Canvas) {
//        try {
//            super.dispatchDraw(canvas)
//        } catch (var3: StackOverflowError) {
//            handleException(var3)
//        }
//    }
//
//    override fun dispatchKeyEvent(ev: KeyEvent): Boolean {
//        return if (reactInstanceManager != null && mIsAttachedToInstance && reactInstanceManager!!.currentReactContext != null) {
//            mAndroidHWInputDeviceHelper.handleKeyEvent(ev)
//            super.dispatchKeyEvent(ev)
//        } else {
//            FLog.w(
//                "ReactNative",
//                "Unable to handle key event as the catalyst instance has not been attached"
//            )
//            super.dispatchKeyEvent(ev)
//        }
//    }
//
//    override fun onFocusChanged(gainFocus: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
//        if (reactInstanceManager != null && mIsAttachedToInstance && reactInstanceManager!!.currentReactContext != null) {
//            mAndroidHWInputDeviceHelper.clearFocus()
//            super.onFocusChanged(gainFocus, direction, previouslyFocusedRect)
//        } else {
//            FLog.w(
//                "ReactNative",
//                "Unable to handle focus changed event as the catalyst instance has not been attached"
//            )
//            super.onFocusChanged(gainFocus, direction, previouslyFocusedRect)
//        }
//    }
//
//    override fun requestChildFocus(child: View, focused: View) {
//        if (reactInstanceManager != null && mIsAttachedToInstance && reactInstanceManager!!.currentReactContext != null) {
//            mAndroidHWInputDeviceHelper.onFocusChanged(focused)
//            super.requestChildFocus(child, focused)
//        } else {
//            FLog.w(
//                "ReactNative",
//                "Unable to handle child focus changed event as the catalyst instance has not been attached"
//            )
//            super.requestChildFocus(child, focused)
//        }
//    }
//
////    public fun join(options: JitsiMeetConferenceOptions?){
////        options?.apply {
////            setProps(asProps(this)!!)
////        }
////    }
//
//    fun leave() {
//        setProps(Bundle())
//    }
//
//    private fun setProps(newProps: Bundle) {
//        val props = mergeProps(asProps(JitsiMeet.getDefaultConferenceOptions()), newProps)
//        props.putLong("timestamp", System.currentTimeMillis())
////        this.createReactRootView(BuildConfig.APPLICATION_ID, props)
//    }
//
//    private fun mergeProps(a: Bundle?, b: Bundle?): Bundle {
//        val result = Bundle()
//        return if (a == null) {
//            if (b != null) {
//                result.putAll(b)
//            }
//            result
//        } else if (b == null) {
//            result.putAll(a)
//            result
//        } else {
//            result.putAll(a)
//            val var3: Iterator<*> = b.keySet().iterator()
//            while (var3.hasNext()) {
//                val key = var3.next() as String
//                val bValue = b[key]
//                val aValue = a[key]
//                val valueType = bValue!!.javaClass.simpleName
//                if (valueType.contentEquals("Boolean")) {
//                    result.putBoolean(key, (bValue as Boolean?)!!)
//                } else if (valueType.contentEquals("String")) {
//                    result.putString(key, bValue as String?)
//                } else {
//                    if (!valueType.contentEquals("Bundle")) {
//                        throw java.lang.RuntimeException("Unsupported type: $valueType")
//                    }
//                    result.putBundle(key, mergeProps(aValue as Bundle?, bValue as Bundle?))
//                }
//            }
//            result
//        }
//    }
//
//    fun asProps(temp: JitsiMeetConferenceOptions?): Bundle? {
//        temp?.apply {
//            val props = Bundle()
//            if (!featureFlags.containsKey("pip.enabled")) {
//                featureFlags.putBoolean("pip.enabled", true)
//            }
//            props.putBundle("flags", featureFlags)
//            if (colorScheme != null) {
//                props.putBundle("colorScheme", colorScheme)
//            }
//            val config = Bundle()
////            if (audioMuted != null) {
////                config.putBoolean("startWithAudioMuted", audioMuted)
////            }
////            if (audioOnly != null) {
////                config.putBoolean("startAudioOnly", audioOnly)
////            }
////            if (videoMuted != null) {
////                config.putBoolean("startWithVideoMuted", videoMuted)
////            }
////            if (subject != null) {
////                config.putString("subject", subject)
////            }
//            val urlProps = Bundle()
//            if (room != null && room.contains("://")) {
//                urlProps.putString("url", room)
//            } else {
//                if (serverURL != null) {
//                    urlProps.putString("serverURL", serverURL.toString())
//                }
//                if (room != null) {
//                    urlProps.putString("room", room)
//                }
//            }
//            if (token != null) {
//                urlProps.putString("jwt", token)
//            }
//            if (userInfo != null) {
//                props.putBundle("userInfo", asUserBundle(userInfo))
//            }
//            urlProps.putBundle("config", config)
//            props.putBundle("url", urlProps)
//            return props
//        }
//        return Bundle()
//    }
//
//    private fun asUserBundle(tmp: JitsiMeetUserInfo): Bundle? {
//        val b = Bundle()
//        if (tmp.displayName != null) {
//            b.putString("displayName", tmp.displayName)
//        }
//        if (tmp.email != null) {
//            b.putString("email", tmp.email)
//        }
//        if (tmp.avatar != null) {
//            b.putString("avatarURL", tmp.avatar.toString())
//        }
//        return b
//    }
//
//
//
//    private fun dispatchJSTouchEvent(event: MotionEvent) {
//        if (reactInstanceManager != null && mIsAttachedToInstance && reactInstanceManager!!.currentReactContext != null) {
//            if (mJSTouchDispatcher == null) {
//                FLog.w(
//                    "ReactNative",
//                    "Unable to dispatch touch to JS before the dispatcher is available"
//                )
//            } else {
//                val reactContext = reactInstanceManager!!.currentReactContext
//                val eventDispatcher = (reactContext!!.getNativeModule(
//                    UIManagerModule::class.java
//                ) as UIManagerModule).eventDispatcher
//                mJSTouchDispatcher!!.handleTouchEvent(event, eventDispatcher)
//            }
//        } else {
//            FLog.w(
//                "ReactNative",
//                "Unable to dispatch touch to JS as the catalyst instance has not been attached"
//            )
//        }
//    }
//
//    override fun requestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
//        if (this.parent != null) {
//            this.parent.requestDisallowInterceptTouchEvent(disallowIntercept)
//        }
//    }
//
//    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
//        if (mUseSurface) {
//            super.onLayout(changed, left, top, right, bottom)
//        }
//    }
//
//    override fun onAttachedToWindow() {
//        super.onAttachedToWindow()
//        if (mIsAttachedToInstance) {
//            removeOnGlobalLayoutListener()
//            this.viewTreeObserver.addOnGlobalLayoutListener(customGlobalLayoutListener)
//        }
//    }
//
//    override fun onDetachedFromWindow() {
//        super.onDetachedFromWindow()
//        if (mIsAttachedToInstance) {
//            removeOnGlobalLayoutListener()
//        }
//    }
//
//    private fun removeOnGlobalLayoutListener() {
//        this.viewTreeObserver.removeOnGlobalLayoutListener(customGlobalLayoutListener)
//    }
//
//    override fun onViewAdded(child: View) {
//        super.onViewAdded(child)
//        if (mShouldLogContentAppeared) {
//            mShouldLogContentAppeared = false
//            if (mJSModuleName != null) {
//                ReactMarker.logMarker(
//                    ReactMarkerConstants.CONTENT_APPEARED, mJSModuleName,
//                    mRootViewTag
//                )
//            }
//        }
//    }
//
//    override fun getRootViewGroup(): ViewGroup {
//        return this
//    }
//
//    @JvmOverloads
//    fun startReactApplication(
//        reactInstanceManager: ReactInstanceManager?,
//        moduleName: String?,
//        initialProperties: Bundle? = null as Bundle?,
//        initialUITemplate: String? = null as String?
//    ) {
//        Systrace.beginSection(0L, "startReactApplication")
//        try {
//            UiThreadUtil.assertOnUiThread()
//            Assertions.assertCondition(
//                this.reactInstanceManager == null,
//                "This root view has already been attached to a catalyst instance manager"
//            )
//            this.reactInstanceManager = reactInstanceManager
//            mJSModuleName = moduleName
//            mAppProperties = initialProperties
//            mInitialUITemplate = initialUITemplate
//            if (mUseSurface) {
//            }
//            this.reactInstanceManager!!.createReactContextInBackground()
//            attachToReactInstanceManager()
//        } finally {
//            Systrace.endSection(0L)
//        }
//    }
//
//    override fun getWidthMeasureSpec(): Int {
//        return mWidthMeasureSpec
//    }
//
//    override fun getHeightMeasureSpec(): Int {
//        return mHeightMeasureSpec
//    }
//
//    override fun setShouldLogContentAppeared(shouldLogContentAppeared: Boolean) {
//        mShouldLogContentAppeared = shouldLogContentAppeared
//    }
//
//    private fun updateRootLayoutSpecs(widthMeasureSpec: Int, heightMeasureSpec: Int) {
//        if (reactInstanceManager == null) {
//            FLog.w(
//                "ReactNative",
//                "Unable to update root layout specs for uninitialized ReactInstanceManager"
//            )
//        } else {
//            val reactApplicationContext = reactInstanceManager!!.currentReactContext
//            if (reactApplicationContext != null) {
//                UIManagerHelper.getUIManager(reactApplicationContext, this.uiManagerType)
//                    .updateRootLayoutSpecs(this.rootViewTag, widthMeasureSpec, heightMeasureSpec)
//            }
//        }
//    }
//
//    fun unmountReactApplication() {
//        if (reactInstanceManager != null && mIsAttachedToInstance) {
//            reactInstanceManager!!.detachRootView(this)
//            reactInstanceManager = null
//            mIsAttachedToInstance = false
//        }
//        mShouldLogContentAppeared = false
//    }
//
//    override fun onStage(stage: Int) {
//        when (stage) {
//            101 -> onAttachedToReactInstance()
//            else -> {
//            }
//        }
//    }
//
//    fun onAttachedToReactInstance() {
//        mJSTouchDispatcher = JSTouchDispatcher(this)
//        if (mRootViewEventListener != null) {
//            mRootViewEventListener!!.onAttachedToReactInstance(this)
//        }
//    }
//
//    fun setEventListener(eventListener: ReactRootViewEventListener?) {
//        mRootViewEventListener = eventListener
//    }
//
//    override fun getJSModuleName(): String {
//        return Assertions.assertNotNull(mJSModuleName) as String
//    }
//
//    override fun getAppProperties(): Bundle? {
//        return mAppProperties
//    }
//
//    override fun getInitialUITemplate(): String? {
//        return mInitialUITemplate
//    }
//
//    fun setAppProperties(appProperties: Bundle?) {
//        UiThreadUtil.assertOnUiThread()
//        mAppProperties = appProperties
//        if (this.rootViewTag != 0) {
//            runApplication()
//        }
//    }
//
//    override fun runApplication() {
//        Systrace.beginSection(0L, "ReactRootView.runApplication")
//        try {
//            if (reactInstanceManager != null && mIsAttachedToInstance) {
//                val reactContext = reactInstanceManager!!.currentReactContext ?: return
//                val catalystInstance = reactContext.catalystInstance
//                val jsAppModuleName = this.jsModuleName
//                if (!mUseSurface) {
//                    if (mWasMeasured) {
//                        updateRootLayoutSpecs(mWidthMeasureSpec, mHeightMeasureSpec)
//                    }
//                    val appParams = WritableNativeMap()
//                    appParams.putDouble("rootTag", this.rootViewTag.toDouble())
//                    val appProperties = this.appProperties
//                    if (appProperties != null) {
//                        appParams.putMap("initialProps", Arguments.fromBundle(appProperties))
//                    }
//                    mShouldLogContentAppeared = true
//                    (catalystInstance.getJSModule(AppRegistry::class.java) as AppRegistry).runApplication(
//                        jsAppModuleName,
//                        appParams
//                    )
//                }
//                return
//            }
//        } finally {
//            Systrace.endSection(0L)
//        }
//    }
//
//    @VisibleForTesting
//    fun simulateAttachForTesting() {
//        mIsAttachedToInstance = true
//        mJSTouchDispatcher = JSTouchDispatcher(this)
//    }
//
//    private val customGlobalLayoutListener: CustomGlobalLayoutListener?
//        get() {
//            if (mCustomGlobalLayoutListener == null) {
//                mCustomGlobalLayoutListener = CustomGlobalLayoutListener()
//            }
//            return mCustomGlobalLayoutListener
//        }
//
//    private fun attachToReactInstanceManager() {
//        Systrace.beginSection(0L, "attachToReactInstanceManager")
//        try {
//            if (mIsAttachedToInstance) {
//                return
//            }
//            mIsAttachedToInstance = true
//            (Assertions.assertNotNull(reactInstanceManager) as ReactInstanceManager).attachRootView(
//                this
//            )
//            this.viewTreeObserver.addOnGlobalLayoutListener(customGlobalLayoutListener)
//        } finally {
//            Systrace.endSection(0L)
//        }
//    }
//
//    @Throws(Throwable::class)
//    protected fun finalize() {
//        this.finalize()
//        Assertions.assertCondition(
//            !mIsAttachedToInstance,
//            "The application this ReactRootView was rendering was not unmounted before the ReactRootView was garbage collected. This usually means that your application is leaking large amounts of memory. To solve this, make sure to call ReactRootView#unmountReactApplication in the onDestroy() of your hosting Activity or in the onDestroyView() of your hosting Fragment."
//        )
//    }
//
//    override fun getRootViewTag(): Int {
//        return mRootViewTag
//    }
//
//    override fun setRootViewTag(rootViewTag: Int) {
//        mRootViewTag = rootViewTag
//    }
//
//    override fun handleException(t: Throwable) {
//        if (reactInstanceManager != null && reactInstanceManager!!.currentReactContext != null) {
//            val e: Exception = IllegalViewOperationException(t.message, this, t)
//            reactInstanceManager!!.currentReactContext!!.handleException(e)
//        } else {
//            throw RuntimeException(t)
//        }
//    }
//
//    fun setIsFabric(isFabric: Boolean) {
//        mUIManagerType = if (isFabric) 2 else 1
//    }
//
//    @SuppressLint("WrongConstant")
//    override fun getUIManagerType(): Int {
//        return mUIManagerType
//    }
//
//    fun sendEvent(eventName: String?, params: WritableMap?) {
//        if (reactInstanceManager != null) {
//            (reactInstanceManager!!.currentReactContext!!.getJSModule(
//                RCTDeviceEventEmitter::class.java
//            ) as RCTDeviceEventEmitter).emit(eventName!!, params)
//        }
//    }
//
//    private inner class CustomGlobalLayoutListener internal constructor() :
//        OnGlobalLayoutListener {
//        private val mVisibleViewArea: Rect
//        private val mMinKeyboardHeightDetected: Int
//        private var mKeyboardHeight = 0
//        private var mDeviceRotation = 0
//        private val mWindowMetrics = DisplayMetrics()
//        private val mScreenMetrics = DisplayMetrics()
//        override fun onGlobalLayout() {
//            if (reactInstanceManager != null && mIsAttachedToInstance && reactInstanceManager!!.currentReactContext != null) {
//                checkForKeyboardEvents()
//                checkForDeviceOrientationChanges()
//                checkForDeviceDimensionsChanges()
//            }
//        }
//
//        private fun checkForKeyboardEvents() {
//            this@ActMeetInvite.rootView.getWindowVisibleDisplayFrame(mVisibleViewArea)
//            val heightDiff =
//                DisplayMetricsHolder.getWindowDisplayMetrics().heightPixels - mVisibleViewArea.bottom
//            val isKeyboardShowingOrKeyboardHeightChanged =
//                mKeyboardHeight != heightDiff && heightDiff > mMinKeyboardHeightDetected
//            if (isKeyboardShowingOrKeyboardHeightChanged) {
//                mKeyboardHeight = heightDiff
//                sendEvent(
//                    "keyboardDidShow", createKeyboardEventPayload(
//                        PixelUtil.toDIPFromPixel(
//                            mVisibleViewArea.bottom.toFloat()
//                        ).toDouble(),
//                        PixelUtil.toDIPFromPixel(mVisibleViewArea.left.toFloat()).toDouble(),
//                        PixelUtil.toDIPFromPixel(
//                            mVisibleViewArea.width().toFloat()
//                        ).toDouble(), PixelUtil.toDIPFromPixel(mKeyboardHeight.toFloat()).toDouble()
//                    )
//                )
//            } else {
//                val isKeyboardHidden =
//                    mKeyboardHeight != 0 && heightDiff <= mMinKeyboardHeightDetected
//                if (isKeyboardHidden) {
//                    mKeyboardHeight = 0
//                    sendEvent(
//                        "keyboardDidHide", createKeyboardEventPayload(
//                            PixelUtil.toDIPFromPixel(
//                                mVisibleViewArea.height().toFloat()
//                            ).toDouble(),
//                            0.0,
//                            PixelUtil.toDIPFromPixel(mVisibleViewArea.width().toFloat()).toDouble(),
//                            0.0
//                        )
//                    )
//                }
//            }
//        }
//
//        @SuppressLint("WrongConstant")
//        private fun checkForDeviceOrientationChanges() {
//            val rotation =
//                (this@ActMeetInvite.context
//                    .getSystemService("window") as WindowManager).defaultDisplay.rotation
//            if (mDeviceRotation != rotation) {
//                mDeviceRotation = rotation
//                emitOrientationChanged(rotation)
//            }
//        }
//
//        private fun checkForDeviceDimensionsChanges() {
//            DisplayMetricsHolder.initDisplayMetrics(this@ActMeetInvite.context)
//            if (!areMetricsEqual(
//                    mWindowMetrics,
//                    DisplayMetricsHolder.getWindowDisplayMetrics()
//                ) || !areMetricsEqual(
//                    mScreenMetrics, DisplayMetricsHolder.getScreenDisplayMetrics()
//                )
//            ) {
//                mWindowMetrics.setTo(DisplayMetricsHolder.getWindowDisplayMetrics())
//                mScreenMetrics.setTo(DisplayMetricsHolder.getScreenDisplayMetrics())
//                emitUpdateDimensionsEvent()
//            }
//        }
//
//        private fun areMetricsEqual(
//            displayMetrics: DisplayMetrics,
//            otherMetrics: DisplayMetrics
//        ): Boolean {
//            return if (VERSION.SDK_INT >= 17) {
//                displayMetrics.equals(otherMetrics)
//            } else {
//                displayMetrics.widthPixels == otherMetrics.widthPixels && displayMetrics.heightPixels == otherMetrics.heightPixels && displayMetrics.density == otherMetrics.density && displayMetrics.densityDpi == otherMetrics.densityDpi && displayMetrics.scaledDensity == otherMetrics.scaledDensity && displayMetrics.xdpi == otherMetrics.xdpi && displayMetrics.ydpi == otherMetrics.ydpi
//            }
//        }
//
//        private fun emitOrientationChanged(newRotation: Int) {
//            var isLandscape = false
//            val name: String
//            val rotationDegrees: Double
//            when (newRotation) {
//                0 -> {
//                    name = "portrait-primary"
//                    rotationDegrees = 0.0
//                }
//                1 -> {
//                    name = "landscape-primary"
//                    rotationDegrees = -90.0
//                    isLandscape = true
//                }
//                2 -> {
//                    name = "portrait-secondary"
//                    rotationDegrees = 180.0
//                }
//                3 -> {
//                    name = "landscape-secondary"
//                    rotationDegrees = 90.0
//                    isLandscape = true
//                }
//                else -> return
//            }
//            val map = Arguments.createMap()
//            map.putString("name", name)
//            map.putDouble("rotationDegrees", rotationDegrees)
//            map.putBoolean("isLandscape", isLandscape)
//            sendEvent("namedOrientationDidChange", map)
//        }
//
////        private val defaultConferenceOptions: JitsiMeetConferenceOptions? = null
////
////        private fun setDefaultConferenceOptions(options: JitsiMeetConferenceOptions?) {
////            if (options != null && options.room != null) {
////                throw RuntimeException("'room' must be null in the default conference options")
////            } else {
////                defaultConferenceOptions = options
////            }
////        }
////
////        fun getCurrentConference(): String? {
////            return OngoingConferenceTracker.getInstance().getCurrentConference()
////        }
//
////        fun getDefaultProps(): Bundle? {
////            return if (JitsiMeet.getDefaultConferenceOptions() != null) JitsiMeet.getDefaultConferenceOptions().asProps() else Bundle()
////        }
//
//
//        private fun emitUpdateDimensionsEvent() {
//            (reactInstanceManager!!.currentReactContext!!.getNativeModule(
//                DeviceInfoModule::class.java
//            ) as DeviceInfoModule).emitUpdateDimensionsEvent()
//        }
//
//        private fun createKeyboardEventPayload(
//            screenY: Double,
//            screenX: Double,
//            width: Double,
//            height: Double
//        ): WritableMap {
//            val keyboardEventParams = Arguments.createMap()
//            val endCoordinates = Arguments.createMap()
//            endCoordinates.putDouble("height", height)
//            endCoordinates.putDouble("screenX", screenX)
//            endCoordinates.putDouble("width", width)
//            endCoordinates.putDouble("screenY", screenY)
//            keyboardEventParams.putMap("endCoordinates", endCoordinates)
//            keyboardEventParams.putString("easing", "keyboard")
//            keyboardEventParams.putDouble("duration", 0.0)
//            return keyboardEventParams
//        }
//
//        init {
//            DisplayMetricsHolder.initDisplayMetricsIfNotInitialized(this@ActMeetInvite.context.applicationContext)
//            mVisibleViewArea = Rect()
//            mMinKeyboardHeightDetected = PixelUtil.toPixelFromDIP(60.0f).toInt()
//        }
//    }
//
//    interface ReactRootViewEventListener {
//        fun onAttachedToReactInstance(var1: ActMeetInvite?)
//    }
//}
//
//class CustomAndroidHWInputDeviceHelper internal constructor(private val mReactRootView: ActMeetInvite) {
//    private var mLastFocusedViewId = -1
//    fun handleKeyEvent(ev: KeyEvent) {
//        val eventKeyCode = ev.keyCode
//        val eventKeyAction = ev.action
//        if ((eventKeyAction == 1 || eventKeyAction == 0) && KEY_EVENTS_ACTIONS.containsKey(
//                eventKeyCode
//            )
//        ) {
//            dispatchEvent(
//                KEY_EVENTS_ACTIONS[eventKeyCode],
//                mLastFocusedViewId, eventKeyAction
//            )
//        }
//    }
//
//    fun onFocusChanged(newFocusedView: View) {
//        if (mLastFocusedViewId != newFocusedView.id) {
//            if (mLastFocusedViewId != -1) {
//                dispatchEvent("blur", mLastFocusedViewId)
//            }
//            mLastFocusedViewId = newFocusedView.id
//            dispatchEvent("focus", newFocusedView.id)
//        }
//    }
//
//    fun clearFocus() {
//        if (mLastFocusedViewId != -1) {
//            dispatchEvent("blur", mLastFocusedViewId)
//        }
//        mLastFocusedViewId = -1
//    }
//
//    private fun dispatchEvent(eventType: String?, targetViewId: Int, eventKeyAction: Int = -1) {
//        val event: WritableMap = WritableNativeMap()
//        event.putString("eventType", eventType)
//        event.putInt("eventKeyAction", eventKeyAction)
//        if (targetViewId != -1) {
//            event.putInt("tag", targetViewId)
//        }
//        mReactRootView.sendEvent("onHWKeyEvent", event)
//    }
//
//    companion object {
//        private val KEY_EVENTS_ACTIONS: Map<Int, String> =
//            MapBuilder.builder<Int, String>().put(23, "select").put(66, "select").put(62, "select")
//                .put(85, "playPause").put(89, "rewind").put(90, "fastForward").put(19, "up")
//                .put(22, "right").put(20, "down").put(21, "left").build()
//    }
//}