package meet.sumra.app.act

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.camera.core.CameraSelector
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.OffsetMapping
import androidx.compose.ui.text.input.TransformedText
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import meet.sumra.app.R
import meet.sumra.app.act.jet.FetchDialogChoose2
import meet.sumra.app.data.enums.EAvatarType
import meet.sumra.app.data.enums.EDialog
import meet.sumra.app.databinding.ActHomeBinding
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.ext.*
import meet.sumra.app.ext.ExtAnimation.hideScaleWithOutToCenter
import meet.sumra.app.ext.ExtAnimation.showScaleWithCenterToOut
import meet.sumra.app.frg.FragmentBase
import meet.sumra.app.frg.bottom.FrgCalendar
import meet.sumra.app.frg.bottom.FrgContacts
import meet.sumra.app.frg.bottom.FrgRecent
import meet.sumra.app.frg.bottom.FrgSettings
import meet.sumra.app.frg.drawer.*
import meet.sumra.app.frg.drawer.bonusPlaza.FrgBonusPlaza
import meet.sumra.app.frg.helpers.FrgMeetAdd
import meet.sumra.app.interfaces.IOnBackPressed
import meet.sumra.app.utils.LogUtil
import meet.sumra.app.utils.UtilPassword
import meet.sumra.app.views.OnCheckedListener
import meet.sumra.app.views.SumraSwitch
import meet.sumra.app.views.compose.ui_theme.StyleText
import meet.sumra.app.vm.BaseViewModel
import meet.sumra.app.vm.Dialogue
import meet.sumra.app.vm.vm_home.StateHome
import meet.sumra.app.vm.vm_home.ViewModelHome
import sdk.net.meet.api.meet_profile.response.MeetUserInfo
import javax.inject.Inject

class ActivityHome: ActivityBase<ActHomeBinding>(){

    private var currentFrg = FrgRecent.TAG
    private var currentBaseFragment = mutableListOf<FragmentBase<*>?>(null, null)
    private var currentHelpFragment = mutableListOf<FragmentBase<*>>()

    private val dialogShow = mutableStateOf(false)
    private val dialogMeetCreate = mutableStateOf(false)
    private val dialogMeetJoin   = mutableStateOf(false)

    private val switchPrivateRoom = mutableStateOf(false)

    private val meetCreateName = mutableStateOf("")
    private val meetJoinName = mutableStateOf("")
    private val autoPassGenerate = mutableStateOf(UtilPassword.generatePass(6))

    protected lateinit var viewModelHome: ViewModelHome
    @Inject set

    companion object {
        fun getInstance(activity: Activity){
            val intent = Intent(activity, ActivityHome::class.java)
            activity.startActivity(intent)
            activity.finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val gradient = viewModelHome.fetchGradient()
        views.homeViewData.background = gradient
        views.toolbarContainer.background = gradient
    }

    override fun onBackPressed() {
        val fragment = this.supportFragmentManager.findFragmentById(R.id.frgHomeContainer)

        when(fragment!!::class.java){
            FrgMeetChat::class.java,
            FrgDashboard::class.java,
            FrgProfile::class.java,
            FrgRewards::class.java,
            FrgBonusPlaza::class.java,
            FrgReferrals::class.java,
            FrgEarnings::class.java,
            FrgStatistic::class.java -> {
                showToolBar()
                showBottomBar()
                (fragment as? IOnBackPressed)?.onBackPressed()?.not()?.let {
                    replaceFrg(fragment = currentBaseFragment[0]!!, tag = currentBaseFragment[0]!!::class.java.simpleName)
                    viewModelHome.resetState()
                }
                viewModelHome.viewState.set(StateHome.Dashboard(Color.TRANSPARENT))
            }
            else -> {
                if( currentHelpFragment.size > 0) {
                    removeFragment(currentHelpFragment[0])
                    currentHelpFragment.clear()
                } else {
                    super.onBackPressed()
                }
            }
        }

        viewModelHome.updateDrawerMenu()
    }

    override fun onDestroy() {
        super.onDestroy()
        currentBaseFragment.clear()
        currentHelpFragment.clear()
    }

    override fun onResume() {
        setupUI()
        addListeners()
        requestPermission()
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onStart() {
        viewModelHome.viewState.observe(this@ActivityHome, state())
        if(currentBaseFragment.size>0 && currentBaseFragment[0] == null) {
            viewModelHome.resetState()
        }
        viewModelHome.viewProgressState.observe(this@ActivityHome, progressState())
        viewModelHome.viewMessageState .observe(this@ActivityHome, messageState())
        super.onStart()
    }

    override fun onStop() {
        cameraProvider?.unbindAll()
        super.onStop()
    }

    @OptIn(ExperimentalMaterialApi::class)
    private fun setupUI() {
        // setup video/voice checker
        views./*homeToolbarContainer.*/switchVoiceVideo.setChecked(viewModelHome.isVideoSetup())

        views.drawerMenuList.apply {
            layoutManager = LinearLayoutManager(this@ActivityHome, LinearLayoutManager.VERTICAL, false)
            adapter = viewModelHome.getAdapter()
        }

        viewModelHome.updateTypeFace().run {
            views./*homeToolbarContainer.*/homeToolbarLabel.typeface = this
            views./*homeToolbarContainer.*/homeToolbarVoice.typeface = this
            views./*homeToolbarContainer.*/homeToolbarVideo.typeface = this

            views.drawerLabel.typeface = this
            views.drawerUserDeviceName.typeface = this

            views.drawerFooterInfo.typeface = this
            views.drawerFooterMore.typeface = this
        }

        views.specialForDialogue.setContent{
            DialogBannerInfo(dialogShow)
            DialogCreateMeet(isShowDialog = dialogMeetCreate)
            DialogJoinMeet(isShowDialog = dialogMeetJoin)
//            FetchDialogChoose(
//                scope = rememberCoroutineScope(),
//                state = currentBottomSheet,
//                clickChoose = {
//                    LogUtil.info(tag = "BUTTON CLICK", txt = "CLICK CHOOSE PHOTO")
//                },
//                clickSelect = {
//                    LogUtil.info(tag = "BUTTON CLICK", txt = "CLICK SELECT PHOTO")
//                },
//                clickCancel = {
//                    LogUtil.info(tag = "BUTTON CLICK", txt = "CLICK CANCEL DIALOG")
//                },
//                modifier = Modifier.fillMaxWidth(1f)
//            )
            FetchDialogChoose2(
                state = currentChooseState,
                clickChoose = {
                    LogUtil.info(tag = "BUTTON CLICK", txt = "CLICK CHOOSE PHOTO")
                },
                clickSelect = {
                    LogUtil.info(tag = "BUTTON CLICK", txt = "CLICK SELECT PHOTO")
                },
                clickCancel = {
                    LogUtil.info(tag = "BUTTON CLICK", txt = "CLICK CANCEL DIALOG")
                },
                modifier = Modifier.fillMaxWidth(1f)
            )
        }
    }

    private fun updateDrawerUI(dt: MeetUserInfo){
        // setup drawer view...
        views.drawerUserDeviceName.text = dt.displayName // "User Testname"
        views.drawerItemIcon.loadUrl(dt.avatar, EAvatarType.CIRCLE)        // setImageResource(R.drawable.ic_app_icon_signal)
    }

    private fun requestPermission(){
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(arrayOf(Manifest.permission.CAMERA), 1)
        } else {
//            initialCameraPreview()
        }
    }

    private var cameraProvider: ProcessCameraProvider? = null

    private fun initialCameraPreview(){

        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)

        cameraProviderFuture.addListener(Runnable {
            // Used to bind the lifecycle of cameras to the lifecycle owner
            cameraProvider = cameraProviderFuture.get()
            // Preview
            val preview = Preview.Builder()
                .build()
                .also {
                    it.setSurfaceProvider(views.frgCustomCamera.surfaceProvider)
                }
            // Select back camera as a default
            val cameraSelector = CameraSelector.DEFAULT_FRONT_CAMERA

            try {
                // Unbind use cases before rebinding
                cameraProvider?.let { provider ->
                    provider.unbindAll()
                    // Bind use cases to camera
                    provider.bindToLifecycle(this, cameraSelector, preview)
                }

            } catch(exc: Exception) {
                LogUtil.error(txt = "Use case binding failed ${exc.message}")
            }

        }, ContextCompat.getMainExecutor(this))
    }

    private fun state() = Observer<StateHome> { state ->
        when(state){
            is StateHome.Logout    -> {
                ActivityLogin.getInstance(this)
            }
            is StateHome.Success   -> {
                LogUtil.info(txt = "${StateHome.Success  ::class.java.simpleName }")
            }
            is StateHome.ShareSocial -> {
                val intent = Intent(Intent.ACTION_SEND).apply {
                    putExtra(Intent.EXTRA_TEXT, "Referral code: ${state.code}\nReferral link: ${state.link}")
                    type = "text/plain"
                }
                startActivity(intent)
            }
            is StateHome.Failure   -> {
                Toast.makeText(this, state.message, Toast.LENGTH_SHORT).show()
                LogUtil.info(txt = "${StateHome.Failure  ::class.java.simpleName }")
            }
            is StateHome.BackPress -> {
                onBackPressed()
            }
//            is StateHome.NewRoom   -> {
//                ActMeetInvite.getInstance(this, state.type, state.data)
//                viewModelHome.resetState()
//            }
            is StateHome.Preview -> {
                if(!state.state){
//                    registerForBroadcastMessages()
                    cameraProvider?.run {
                        unbindAll()
                    }
                    hideBottomBar()
                    hideToolBar()
                }
            }
            /** ================================================================== **/
            is StateHome.Profile   -> {
                LogUtil.info(txt = "....Profile:")
                currentBaseFragment[1] = FrgProfile.getInstance()
                addFrgToBack(
                    fragment = currentBaseFragment[1]!!,
                    tag      = currentBaseFragment[1]!!::class.java.simpleName
                )
                views.drawerViewData.closeDrawer(GravityCompat.START)
                hideBottomBar()
                hideToolBar()
            }
            is StateHome.Plaza   -> {
                LogUtil.info(txt = "....Divits bonus plaza:"  )
                currentBaseFragment[1] = FrgBonusPlaza.getInstance()
//                if(currentFrg != FrgBonusPlaza.TAG) {
//                    currentFrg = FrgBonusPlaza.TAG
                    addFrgToBack(
                        fragment = currentBaseFragment[1]!!,
                        tag      = currentBaseFragment[1]!!::class.java.simpleName
                    )
//                }
                views.drawerViewData.closeDrawer(GravityCompat.START)
                hideBottomBar()
                hideToolBar()
            }
            is StateHome.Pioneer   -> {
                LogUtil.info(txt = "....Pioneer:"  )
                ActivityWebView.getInstance(this)
            }
            is StateHome.Rewards   -> {
                LogUtil.info(txt = "....Rewards:"  )
                currentBaseFragment[1] = FrgRewards.getInstance()
                if(currentFrg != FrgRewards.TAG) {
                    currentFrg = FrgRewards.TAG
                    addFrgToBack(
                        fragment = currentBaseFragment[1]!!,
                        tag      = currentBaseFragment[1]!!::class.java.simpleName
                    )
                }
                views.drawerViewData.closeDrawer(GravityCompat.START)
                hideBottomBar()
                hideToolBar()
            }
            is StateHome.Earnings  -> {
                LogUtil.info(txt = "....Earnings:" )
                currentBaseFragment[1] = FrgEarnings.getInstance()
                addFrgToBack(
                    fragment = currentBaseFragment[1]!!,
                    tag      = currentBaseFragment[1]!!::class.java.simpleName
                )
                views.drawerViewData.closeDrawer(GravityCompat.START)
                hideBottomBar()
                hideToolBar()
            }
            is StateHome.Referrals -> {
                LogUtil.info(txt = "....Referrals:")
//                updateStatusBarColor(state.statusColor)
                currentBaseFragment[1] = FrgReferrals.getInstance()
                addFrgToBack(
                    fragment = currentBaseFragment[1]!!,
                    tag      = currentBaseFragment[1]!!::class.java.simpleName
                )
                views.drawerViewData.closeDrawer(GravityCompat.START)
                hideBottomBar()
                hideToolBar()
            }
            is StateHome.Statistic -> {
//                currentBaseFragment.add()
                currentBaseFragment[1] = FrgStatistic.getInstance()
                addFrgToBack(
                    fragment = currentBaseFragment[1]!!,
                    tag      = currentBaseFragment[1]!!::class.java.simpleName
                )
                views.drawerViewData.closeDrawer(GravityCompat.START)
                hideBottomBar()
                hideToolBar()
            }
            is StateHome.Default -> {
                LogUtil.info("DEFAULT BLOCK")
                currentBaseFragment[0] = FrgRecent.getInstance()
                replaceFrg(
                    fragment = currentBaseFragment[0]!!,
                    tag      = currentBaseFragment[0]!!::class.java.simpleName)
            }
            is StateHome.DrawerUpdate -> {
                // setup drawer
                updateDrawerUI(state.value)
            }
            is StateHome.Dashboard -> {
                views./*homeToolbarContainer.*/homeToolbarLabel.hideScaleText()
                if(currentBaseFragment.size >= 2 && currentBaseFragment[1] != null){
                    removeFragment(currentBaseFragment[1]!!)
                }
            }
            is StateHome.UpdateVideoAppBar -> {
                views./*homeToolbarContainer.*/switchVoiceVideo.setChecked(viewModelHome.isVideoSetup())
            }

            is StateHome.CalendarMeetAdd -> {
                currentHelpFragment.add(FrgMeetAdd.getInstance())
                currentHelpFragment.let { col ->
                    val t = col.get(0) as FrgMeetAdd
                    replaceFrg(
                         frameId = R.id.frgHomeHelper
                        ,fragment = t
                        ,tag = t.getTag()
                        ,allowStateLoss = false
                    )
                }
            }
            is StateHome.FragmentEmpty -> {
                currentHelpFragment.let { col ->
                    removeFragment((col.get(0) as FrgMeetAdd))
                    col.clear()
                }
            }
            is StateHome.Invite -> {
                ActMeetInvite.getInstance(
                    activity = this,
                    type = state.type,
                    data = state.data,
                    pass = state.pass)
                viewModelHome.resetState()
//                currentBaseFragment.clear()
            }
            is StateHome.ForDialogue -> {
                when(state.dialogue) {
                    is Dialogue.Banner  -> {
                        dialogShow.value = true
                    }
                    is Dialogue.Default -> {
                        when(state.dialogue.type){
                            EDialog.MEET_CREATE -> { dialogMeetCreate.value = state.dialogue.isAction }
                            EDialog.MEET_JOIN   -> { dialogMeetJoin.value = state.dialogue.isAction }
                        }
                    }
                    is Dialogue.DialogCancel -> {
                        dialogShow.value = false
                    }
                    is Dialogue.Choose -> {
                        currentChooseState.value
                    }
                }
            }
        }
        viewModelHome.updateDrawerMenu()
    }

    private fun progressState() = Observer<BaseViewModel.LoadingState> { state ->
        when (state) {
            is BaseViewModel.LoadingState.LoadingShow -> {views.activityHomeProgress.showScaleWithCenterToOut()}
            is BaseViewModel.LoadingState.LoadingHide -> {views.activityHomeProgress.hideScaleWithOutToCenter()}
            else -> {Unit}
        }
    }

    private fun messageState() = Observer<BaseViewModel.MessageState> { state ->
        when (state) {
            is BaseViewModel.MessageState.MessageSuccess -> {
                val snackbar = Snackbar.make(views.homeViewData, state.content.texts, Snackbar.LENGTH_LONG)
                snackbar.setBackgroundTint(state.color.back)
                snackbar.setTextColor(state.color.text)
                snackbar.show()
                LogUtil.info(tag = "Message success", txt = "${state.content.title} => ${state.content.texts}")
            }
            is BaseViewModel.MessageState.MessageDanger -> {
                val snackbar = Snackbar.make(views.homeViewData, state.content.texts, Snackbar.LENGTH_LONG)
                snackbar.setBackgroundTint(state.color.back)
                snackbar.setTextColor(state.color.text)
                snackbar.show()
                LogUtil.info(tag = "Message failure", txt = "${state.content.title} => ${state.content.texts}")
            }
            is BaseViewModel.MessageState.MessageWarning -> {
                val snackbar = Snackbar.make(views.homeViewData, state.content.texts, Snackbar.LENGTH_LONG)
                snackbar.setBackgroundTint(state.color.back)
                snackbar.setTextColor(state.color.text)
                snackbar.show()
                LogUtil.info(tag = "Message Warnings", txt = "${state.content.title} => ${state.content.texts}")
            }
            is BaseViewModel.MessageState.DialogShow   -> {
                LogUtil.info(txt = "Dialog message show")
            }
            else -> {LogUtil.info(txt = "Other message show")}
        }
    }

    fun nothing(view: View){
        LogUtil.info(txt = "fragment background click of -> ${view::class.java.simpleName}")
    }

    private val opened = booleanArrayOf(false)



    private fun addListeners() {
        views.homeToolbarVideo.setOnClickListener { view ->
            viewModelHome.logout()
        }

        views.switchVoiceVideo.setOnCheckedListener { sw ->
            viewModelHome.inVideoAppBar(views.switchVoiceVideo.isChecked())
        }

        views.drawerMenuButton.setOnClickListener { btn ->
            views.drawerViewData.openDrawer(GravityCompat.START)
        }

        views.drawerViewData.addDrawerListener(object : DrawerLayout.DrawerListener{
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                LogUtil.info(txt = "onDrawerSlide")
            }
            override fun onDrawerOpened(drawerView: View) {
                opened[0] = !opened[0]
            }
            override fun onDrawerClosed(drawerView: View) {
                opened[0] = !opened[0]
            }
            override fun onDrawerStateChanged(newState: Int) {
                LogUtil.info(txt = "onDrawerStateChanged (new state) -> $newState")
            }
        })

        views.homeBottomNavigation.setOnNavigationItemSelectedListener(
            object: BottomNavigationView.OnNavigationItemSelectedListener{
                override fun onNavigationItemSelected(item: MenuItem): Boolean {
                    when(item.itemId){
                        R.id.menu_home_recent -> {
                            LogUtil.info(txt = "${R.id.menu_home_recent}")
                            currentBaseFragment[0] = FrgRecent.getInstance()
                            if(currentFrg != FrgRecent.TAG) {
                                currentFrg = FrgRecent.TAG
                                replaceFrg(
                                    fragment = currentBaseFragment[0]!!,
                                    tag = currentBaseFragment[0]!!::class.java.simpleName
                                )
                            }
                        }
                        R.id.menu_home_calendar -> {
                            LogUtil.info(txt = "${R.id.menu_home_calendar}")
                            currentBaseFragment[0] = FrgCalendar.getInstance()
                            if(currentFrg != FrgCalendar.TAG) {
                                currentFrg = FrgCalendar.TAG
                                replaceFrg(
                                    fragment = currentBaseFragment[0]!!,
                                    tag = currentBaseFragment[0]!!::class.java.simpleName
                                )
                            }
                        }
                        R.id.menu_home_contacts -> {
                            LogUtil.info(txt = "${R.id.menu_home_contacts}")
                            currentBaseFragment[0] = FrgContacts.getInstance()
                            if(currentFrg != FrgContacts.TAG) {
                                currentFrg = FrgContacts.TAG
                                replaceFrg(
                                    fragment = currentBaseFragment[0]!!,
                                    tag = currentBaseFragment[0]!!::class.java.simpleName
                                )
                            }
                        }
                        R.id.menu_home_settings -> {
                            LogUtil.info(txt = "${R.id.menu_home_settings}")
                            currentBaseFragment[0] = FrgSettings.getInstance()
                            if(currentFrg != FrgSettings.TAG) {
                                currentFrg = FrgSettings.TAG
                                replaceFrg(
                                    fragment = currentBaseFragment[0]!!,
                                    tag = currentBaseFragment[0]!!::class.java.simpleName
                                )
                            }
                        }
                    }
                    return true
            }
        })
    }

    @ExperimentalMaterialApi
    private var currentBottomSheet = mutableStateOf(BottomSheetState(BottomSheetValue.Collapsed))
    private var currentChooseState = mutableStateOf(false)

    private fun showBottomBar(){ views.homeBottomNavigation.visibility = View.VISIBLE }

    private fun hideBottomBar(){ views.homeBottomNavigation.visibility = View.GONE    }
    private var layout: ViewGroup.LayoutParams? = null
    private fun showToolBar(){
        views.homeToolbarContainer.visibility = View.VISIBLE
        if(layout == null) layout = views.toolbarContainer.layoutParams
        layout!!.height =
            resources.getDimension(R.dimen.toolBarHeight).toInt() +
                    resources.getDimension(R.dimen.sysBarHeight).toInt()
    }

    private fun hideToolBar(){
        views.homeToolbarContainer.visibility = View.GONE
        if(layout == null) layout = views.toolbarContainer.layoutParams
        layout!!.height = resources.getDimension(R.dimen.sysBarHeight).toInt()
    }

    fun helpFragmentHide(){
        onBackPressed()
//        showBottomBar()
//        showToolBar()
//        removeFragment()
    }

    private fun replaceFrg(
        frameId: Int = R.id.frgHomeContainer,
        fragment: FragmentBase<*>,
        tag: String?,
        allowStateLoss: Boolean = false
    ) {
        replaceFragment(
            frameId  = frameId,
            fragment = fragment,
            tag      = tag,
            allowStateLoss = allowStateLoss
        )
    }

    private fun addFrgToBack(
        frameId: Int = R.id.frgHomeContainer,
        fragment: FragmentBase<*>,
        tag: String?,
        allowStateLoss: Boolean = false
    ) {
        addFragmentToBackStack(
            frameId  = frameId,
            fragment = fragment,
            tag      = tag,
            allowStateLoss = allowStateLoss
        )
    }

    override fun injectDependency(component: ComponentViewModel) {
        component.injectHome(this@ActivityHome)
    }

    override fun getBinding(): ActHomeBinding {
        return ActHomeBinding.inflate(layoutInflater)
    }

    @Composable
    fun DialogBannerInfo(isShowDialog: MutableState<Boolean>) {
        if (isShowDialog.value) {
            AlertDialog(
                onDismissRequest = {  },
                title = {
                    Text("Title")
                },
                confirmButton = {
                    Button(
                        onClick = {
                            // Change the state to close the dialog
                            isShowDialog.value = false
                        },
                    ) {
                        Text("Confirm")
                    }
                },
                dismissButton = {
                    Button(
                        onClick = {
                            // Change the state to close the dialog
                            isShowDialog.value = false
                        },
                    ) {
                        Text("Dismiss")
                    }
                },
                text = {
                    Text("This is a text on the dialog")
                },
            )
        }
    }

    private val dH   = 44
    private val padD = 10
    private val spH  = 24
    @Composable
    @ExperimentalMaterialApi
    fun DialogCreateMeet(isShowDialog: MutableState<Boolean>) {
        if (isShowDialog.value) {
            val brush = Brush.horizontalGradient(
                colors = listOf(
                      androidx.compose.ui.graphics.Color(0xff2188FF)
                    , androidx.compose.ui.graphics.Color(0xff0366D6)
                )
            )

            AlertDialog(
                shape = RoundedCornerShape(16.dp),
                onDismissRequest = { LogUtil.info(tag = "DIALOG", txt = "Dialog create meet!..") },
                title = null,
                dismissButton = {
                    Surface(
                        onClick = {
                            hideKeyboard()
                            isShowDialog.value = false
                        },
                        modifier = Modifier
                            .height(dH.dp)
                            .padding(bottom = padD.dp),
                        shape = RoundedCornerShape(size = (dH/2).dp)
                    ) {
                        Box(
                            modifier = Modifier
                                .background(
                                    brush = brush,
                                    shape = RoundedCornerShape(size = (dH/2).dp)),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(
                                  text = stringResource(id = R.string.text_recent_meet_cancel),
                                style = StyleText.txtButtonLbl(),
                                modifier = Modifier
                                    .padding(horizontal = 20.dp)
                            )
                        }
                    }
                },
                confirmButton = {
                    Surface(
                        onClick = {
                            views.root.hideKeyboard()
                            viewModelHome.validMeetingName(meetCreateName.value, autoPassGenerate.value)},
                        modifier = Modifier
                            .height(dH.dp)
                            .padding(bottom = padD.dp, end = padD.dp),
                        shape = RoundedCornerShape(size = (dH/2).dp)
                    ) {
                        Box(
                            modifier = Modifier
                                .background(
                                    brush = brush,
                                    shape = RoundedCornerShape(size = (dH/2).dp)),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(
                                text = stringResource(id = R.string.text_recent_button_meet_create),
                                style = StyleText.txtButtonLbl(),
                                modifier = Modifier
                                    .padding(horizontal = 20.dp)
                            )
                        }
                    }
                },
                text = {
                    Column (
                        modifier = Modifier
                            .fillMaxWidth(1f),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(
                            text = stringResource(id = R.string.text_recent_meet_create_title),
                            style = StyleText.txtPgLbl(),
                            textAlign = TextAlign.Center,
                            modifier = Modifier.fillMaxWidth(1f))
                        Spacer(modifier = Modifier.height(spH.dp))
                        OutlinedTextField (
                            label = {
                                Text(
                                    text = stringResource(id = R.string.text_recent_meet_name),
                                    style = StyleText.txtCrdLim(),
                                    textAlign = TextAlign.Start
                                )},
                            colors = TextFieldDefaults.outlinedTextFieldColors(
                                unfocusedBorderColor =  androidx.compose.ui.graphics.Color(0xffECEDF3),
                                focusedBorderColor = androidx.compose.ui.graphics.Color(0xff0366D6),

                                focusedLabelColor  = androidx.compose.ui.graphics.Color(0xffDBEDFF),
                                unfocusedLabelColor = androidx.compose.ui.graphics.Color(0xff6A737D),

                                backgroundColor = androidx.compose.ui.graphics.Color(0xffDBEDFF),

                                textColor =  androidx.compose.ui.graphics.Color(0xff444D56),
                                cursorColor        = androidx.compose.ui.graphics.Color(0xff444D56)),
                            keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Text),
                            value = meetCreateName.value,
                            onValueChange = {
                                meetCreateName.value = it },
                            shape = RoundedCornerShape(8.dp),
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(64.dp)
                        )

                        Spacer(modifier = Modifier.height(spH.dp))

                        Text(
                            text = stringResource(id = R.string.text_recent_conversation)
                            , style = StyleText.txtInfoLbl()
                            , textAlign = TextAlign.Center
                            , modifier = Modifier
                                .fillMaxWidth(1f)
                        )

                        Spacer(modifier = Modifier.height(spH.dp))
        /***    ****    Create button and input text for private room    ****    ****/
                        Surface(
                            border = BorderStroke(
                                width = 1.dp,
                                color = androidx.compose.ui.graphics.Color(0xffECEDF3)),
                            shape = RoundedCornerShape(10.dp),
                        ) {
                            Box (
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(all = 16.dp)
                                ) {
                                Column(
                                    modifier = Modifier.fillMaxWidth(1f)
                                ) {
                                    Row(
                                        modifier = Modifier.fillMaxWidth(1f),
                                        verticalAlignment = Alignment.CenterVertically
                                    ) {
                                        AndroidView(
                                            factory = { context ->
                                                SumraSwitch(context).apply {
                                                    addBackgroundIsCheckColor(Color.parseColor("#ECEDF3"))
                                                    addBackgroundUnCheckColor(Color.parseColor("#ECEDF3"))
                                                    addCircleUnCheckColor(Color.parseColor("#6A737D"))
                                                    addCircleIsCheckColor(Color.parseColor("#2188FF"))
                                                    setChecked(switchPrivateRoom.value)
                                                    setOnSwitchedListener {
                                                        object : OnCheckedListener {
                                                            override fun invoke(p1: Boolean) {
                                                                Log.i(
                                                                    "Is AwesomeSwitch",
                                                                    "Switch to $p1")
                                                                setChecked(p1)
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                        )
                                        Spacer(modifier = Modifier.width(10.dp))
                                        Text(
                                            text = stringResource(id = R.string.text_recent_meet_pass),
                                            style = StyleText.txtTabLbl(),
                                            textAlign = TextAlign.Left,
                                            modifier = Modifier.fillMaxWidth()
                                        )
                                    }
                                    Spacer(modifier = Modifier.height(10.dp))
                                    OutlinedTextField(
                                        label = {
                                            Text(
                                                text = stringResource(id = R.string.text_recent_meet_password),
                                                style = StyleText.txtCrdLim(),
                                                textAlign = TextAlign.Start
                                            )
                                        },
                                        colors = TextFieldDefaults.outlinedTextFieldColors(
                                            unfocusedBorderColor = androidx.compose.ui.graphics.Color(0xffECEDF3),
                                            focusedBorderColor = androidx.compose.ui.graphics.Color(0xff0366D6),
                                            focusedLabelColor = androidx.compose.ui.graphics.Color(0xffDBEDFF),
                                            unfocusedLabelColor = androidx.compose.ui.graphics.Color(0xff6A737D),
                                            backgroundColor = androidx.compose.ui.graphics.Color(0xffDBEDFF),
                                            textColor = androidx.compose.ui.graphics.Color(0xff444D56),
                                            cursorColor = androidx.compose.ui.graphics.Color(0xff444D56)),
                                        keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Text),
                                        value = autoPassGenerate.value,
                                        onValueChange = { autoPassGenerate.value = it },
                                        shape = RoundedCornerShape(8.dp),
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .height(60.dp)
                                    )
                                }
                            }
                        }
                    }
                },
            )
        }
    }

    @Composable
    @ExperimentalMaterialApi
    fun DialogJoinMeet(isShowDialog: MutableState<Boolean>) {
        if (isShowDialog.value) {
            val brush = Brush.horizontalGradient(
                colors = listOf(
                    androidx.compose.ui.graphics.Color(0xff2188FF)
                    , androidx.compose.ui.graphics.Color(0xff0366D6)
                )
            )

            AlertDialog(
                shape = RoundedCornerShape(16.dp),
                onDismissRequest = {  },
                title = null,
                dismissButton = {
                    Surface(
                        onClick = {
                            // Change the state to close the dialog
                            isShowDialog.value = false },
                        modifier = Modifier
                            .height(dH.dp)
                            .padding(bottom = padD.dp),
                        shape = RoundedCornerShape(size = (dH/2).dp)
                    ) {
                        Box(
                            modifier = Modifier
                                .background(
                                    brush = brush,
                                    shape = RoundedCornerShape(size = (dH/2).dp)),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(
                                text = stringResource(id = R.string.text_recent_meet_cancel),
                                style = StyleText.txtButtonLbl(),
                                modifier = Modifier
                                    .padding(horizontal = 20.dp)
                            )
                        }
                    }
                },
                confirmButton = {
                    Surface(
                        onClick = {
                            views.root.hideKeyboard()
                            viewModelHome.validMeetingCode(meetJoinName.value) },
                        modifier = Modifier
                            .height(dH.dp)
                            .padding(bottom = padD.dp, end = padD.dp),
                        shape = RoundedCornerShape(size = (dH/2).dp)
                    ) {
                        Box(
                            modifier = Modifier
                                .background(
                                    brush = brush,
                                    shape = RoundedCornerShape(size = (dH/2).dp)),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(
                                text = stringResource(id = R.string.text_recent_button_meet_join),
                                style = StyleText.txtButtonLbl(),
                                modifier = Modifier
                                    .padding(horizontal = 20.dp)
                            )
                        }
                    }
                },
                text = {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth(1f),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(
                            text = stringResource(id = R.string.text_recent_meet_join_title),
                            style = StyleText.txtPgLbl(),
                            textAlign = TextAlign.Center,
                            modifier = Modifier.fillMaxWidth(1f))
                        Spacer(modifier = Modifier.height(spH.dp))
                        OutlinedTextField(
                            label = {
                                Text(
                                    text = stringResource(id = R.string.text_recent_meet_join),
                                    style = StyleText.txtCrdLim(),
                                    textAlign = TextAlign.Start
                                )},
                            colors = TextFieldDefaults.outlinedTextFieldColors(
                                unfocusedBorderColor =  androidx.compose.ui.graphics.Color(0xffECEDF3),
                                focusedBorderColor = androidx.compose.ui.graphics.Color(0xff0366D6),

                                focusedLabelColor  = androidx.compose.ui.graphics.Color(0xffDBEDFF),
                                unfocusedLabelColor = androidx.compose.ui.graphics.Color(0xff6A737D),

                                backgroundColor = androidx.compose.ui.graphics.Color(0xffDBEDFF),

                                textColor =  androidx.compose.ui.graphics.Color(0xff444D56),
                                cursorColor        = androidx.compose.ui.graphics.Color(0xff444D56)),
                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
                            visualTransformation = MaskTransformation(),
//                            KeyboardOptions.Default.copy(keyboardType = KeyboardType.Text),
                            value = meetJoinName.value,
                            onValueChange = {
                                meetJoinName.value = it.take(12)
                            },
                            shape = RoundedCornerShape(8.dp),
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(56.dp)
                        )
                        Spacer(modifier = Modifier.height(spH.dp))
                        Text(
                            text = stringResource(id = R.string.text_recent_join_conversation)
                            , style = StyleText.txtInfoLbl()
                            , textAlign = TextAlign.Center
                            , modifier = Modifier
                                .fillMaxWidth(1f)
                        )
                        Spacer(modifier = Modifier.height(spH.dp))
                    }
                },
            )
        }
    }


    class MaskTransformation() : VisualTransformation {
        override fun filter(text: AnnotatedString): TransformedText {
            return maskFilter(text)
        }

        fun maskFilter(text: AnnotatedString): TransformedText {

            // 1 2 3 4 5 6 7 8 9 10 11 12
            // S s S s S s S s S  s  S  s
            // 0 1 2 3 4 5 6 7 8  9 10 11

            // 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
            // S s S - s S s - S  s  S  -  s  S  s
            // 0 1 2 3 4 5 6 7 8  9 10 11 12 13 14
            LogUtil.info(tag = "INPUT STRING", txt = "${text.text}")
            val trimmed = if (text.text.length >= 12) text.text.substring(0..11) else text.text
            var out = ""
            for (i in trimmed.indices) {
                out += trimmed[i]
                if (i == 2) out += "-"
                if (i == 5) out += "-"
                if (i == 8) out += "-"
            }

            val numberOffsetTranslator = object : OffsetMapping {
                override fun originalToTransformed(offset: Int): Int {offset
                    if (offset <=  2) { return offset }
                    if (offset <=  5) { return offset + 1 }
                    if (offset <=  8) { return offset + 2 }
                    if (offset <= 12) { return offset + 3 }
                    return 15
                }

                override fun transformedToOriginal(offset: Int): Int {
                    if (offset <=  3) { return offset }
                    if (offset <=  7) { return offset - 1 }
                    if (offset <= 11) { return offset - 2 }
                    if (offset <= 15) { return offset - 3 }
                    return 12
                }
            }

            return TransformedText(AnnotatedString(out), numberOffsetTranslator)
        }
    }

}