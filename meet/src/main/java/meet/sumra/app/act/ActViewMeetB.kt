//package meet.sumra.app.act
//
//import android.content.Context
//import android.os.Bundle
//import android.view.*
//import com.facebook.react.bridge.*
//import com.facebook.react.uimanager.*
//import org.jitsi.meet.sdk.*
//import org.jitsi.meet.sdk.log.JitsiMeetLogger
//import java.lang.RuntimeException
////import org.jitsi.meet.sdk.OngoingConferenceTracker.OngoingConferenceListener
//
//class ActViewMeetB: BaseReactView<JitsiMeetViewListener>
////    OngoingConferenceListener
//{
//    val LISTENER_METHODS = ListenerUtils.mapListenerMethods(
//        JitsiMeetViewListener::class.java)
//
//    @Volatile
//    private var url: String? = null
//
//    open fun mergeProps(a: Bundle?, b: Bundle?): Bundle {
//        val result = Bundle()
//        return if (a == null) {
//            if (b != null) {
//                result.putAll(b)
//            }
//            result
//        } else if (b == null) {
//            result.putAll(a)
//            result
//        } else {
//            result.putAll(a)
//            val var3: Iterator<*> = b.keySet().iterator()
//            while (var3.hasNext()) {
//                val key = var3.next() as String
//                val bValue = b[key]
//                val aValue = a[key]
//                val valueType = bValue!!.javaClass.simpleName
//                if (valueType.contentEquals("Boolean")) {
//                    result.putBoolean(key, (bValue as Boolean?)!!)
//                } else if (valueType.contentEquals("String")) {
//                    result.putString(key, bValue as String?)
//                } else {
//                    if (!valueType.contentEquals("Bundle")) {
//                        throw RuntimeException("Unsupported type: $valueType")
//                    }
//                    result.putBundle(key, mergeProps(aValue as Bundle?, bValue as Bundle?))
//                }
//            }
//            result
//        }
//    }
//
//    constructor(context: Context) : super(context) {
//        if (context !is JitsiMeetActivityInterface) {
////            throw RuntimeException("Enclosing Activity must implement JitsiMeetActivityInterface")
//        } else {
////            OngoingConferenceTracker.getInstance().addListener(this)
//        }
//    }
//
//    override fun dispose() {
////        OngoingConferenceTracker.getInstance().removeListener(this)
//        super.dispose()
//    }
//
//    override fun onExternalAPIEvent(p0: String?, p1: ReadableMap?) {
//        TODO("Not yet implemented")
//    }
//
////    fun enterPictureInPicture() {
////        val pipModule = ReactInstanceManagerHolder.getNativeModule(
////            PictureInPictureModule::class.java
////        ) as PictureInPictureModule
////        if (pipModule != null && pipModule.isPictureInPictureSupported() && !JitsiMeetActivityDelegate.arePermissionsBeingRequested() && this.url != null) {
////            try {
////                pipModule.enterPictureInPicture()
////            } catch (var3: RuntimeException) {
////                JitsiMeetLogger.e(var3, "Failed to enter PiP mode", *arrayOfNulls(0))
////            }
////        }
////    }
////
////    fun join(options: JitsiMeetConferenceOptions?) {
////        this.setProps(if (options != null) options.asProps() else Bundle())
////    }
////
////    fun leave() {
////        this.setProps(Bundle())
////    }
////
////    open fun setProps(newProps: Bundle) {
////        val props = mergeProps(JitsiMeet.getDefaultProps(), newProps)
////        props.putLong("timestamp", System.currentTimeMillis())
////        this.createReactRootView("App", props)
////    }
////
////    fun onCurrentConferenceChanged(conferenceUrl: String) {
////        this.url = conferenceUrl
////    }
////
////
////    @Deprecated("")
////    fun onExternalAPIEvent(name: String?, data: ReadableMap?) {
////        this.onExternalAPIEvent(LISTENER_METHODS, name, data)
////    }
////
////    fun onDetachedFromWindow() {
////        this.dispose()
////        super.onDetachedFromWindow()
////    }
//}
