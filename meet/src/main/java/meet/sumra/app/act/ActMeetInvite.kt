package meet.sumra.app.act

import android.app.Activity
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.util.TypedValue
import android.widget.Toast
import androidx.annotation.ColorInt
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.lifecycle.Observer
import com.facebook.infer.annotation.ThreadConfined
import com.google.android.material.snackbar.Snackbar
import meet.sumra.app.CONST
import meet.sumra.app.R
import meet.sumra.app.data.enums.ELinkToSend
import meet.sumra.app.data.mdl.RoomData
import meet.sumra.app.data.mdl.plaza.mock.Emulation
import meet.sumra.app.data.mdl.roomModels
import meet.sumra.app.databinding.FrgMeetWaitBinding
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.views.compose.ui_theme.StyleText
import meet.sumra.app.views.compose.ui_theme.ThemeSetup
import meet.sumra.app.vm.BaseViewModel
import meet.sumra.app.vm.vm_home.StateMeet
import meet.sumra.app.vm.vm_meet.ViewModelMeet
import org.jitsi.meet.sdk.*
import java.util.*
import javax.inject.Inject

class ActMeetInvite : ActivityBase<FrgMeetWaitBinding>() {

    companion object {
        fun getInstance(activity: Activity, type: ELinkToSend, data: String?, pass: String?){
            val intent = Intent(activity, ActMeetInvite::class.java)
            intent.putExtra(CONST.KEY_ROOM_TYPE, type.toString())
            intent.putExtra(CONST.KEY_ROOM_INFO, data)
            intent.putExtra(CONST.KEY_ROOM_PASS, pass)
            activity.startActivity(intent)
            activity.finish()
        }
    }

    private val mRoomState = mutableListOf<Boolean>()

    private val mRoomLoaded = mutableStateOf(mutableListOf<roomModels.RoomByInvite>())

    private val mRoomName = mutableStateOf("")
    private val mRoomLink = mutableStateOf("")
    private lateinit var mRoomLastTime: MutableState<String>  // = mutableStateOf()
    private val mRoomParticipants = mutableStateOf(mutableListOf<roomModels.ParticipantsRoom>())

    private val isShowDialog = mutableStateOf(false)
    private val isEnterPass  = mutableStateOf(false)


    @Inject
    lateinit var viewModel: ViewModelMeet

    @ThreadConfined("UI")
    @ExperimentalMaterialApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.mViewState.observe(this@ActMeetInvite, stateMeetInvite())
        viewModel.viewProgressState.observe(this@ActMeetInvite, stateProgress())
        viewModel.viewMessageState.observe(this@ActMeetInvite, stateMessages())

        mRoomLastTime = mutableStateOf(resources.getString(R.string.toMeetWaitLastTimeDefault))
//        viewModel.updateUserInformation()

        views.root.background = viewModel.fetchGradient()
        views.root.setContent { FetchWaitView() }

//        viewModel.updateRoomInfo(Gson()
//            .fromJson(intent.getStringExtra(CONST.KEY_ROOM), MeetRoom::class.java))

        val ir = intent.getStringExtra(CONST.KEY_ROOM_INFO)
        val it = intent.getStringExtra(CONST.KEY_ROOM_TYPE)
        val ip = intent.getStringExtra(CONST.KEY_ROOM_PASS)

        when(it){
            ELinkToSend.NAME.toString(),
            ELinkToSend.URL.toString() -> {
                if (ir != null) {
                    if (   ir.startsWith("http://")
                        || ir.startsWith("https://")
                    ) {
                        if (   ir.startsWith("http://sumrameet.com")
                            || ir.startsWith("https://sumrameet.com")
                        ) {
                            // fetch room by invite code

                            val res = ir.split('/')
                            viewModel.roomCreateByInvite(res[res.size-1])
                        } else {
                            // message for user, about fail sumrameet link
                            viewModel.showMessage(
                                m = BaseViewModel.MessageState.MessageWarning(
                                    content = BaseViewModel.AlertText(
                                        title = "Warning!",
                                        texts = "Current link, don`t is Sumrameet room!"
                                    )
                                )
                            )
                        }
                    } else {
                        val ic = ir.split('-')
                        if(ic.size==4) {
                            viewModel.roomCreateByInvite(ir)
                        }else {
                            viewModel.roomCreate(RoomData(name = ir, password = null))
                        }
                    }
                } else {
                    viewModel.roomGetReady()
                }
            }
            ELinkToSend.CODE.toString() -> {
                if (ir != null) {
                    viewModel.roomCreateByInvite(ir)
                } else {
                    viewModel.showMessage(
                        m = BaseViewModel.MessageState.MessageWarning(
                            content = BaseViewModel.AlertText(
                                title = "Warning!",
                                texts = "Current code, don`t is inviting with Sumrameet room!"
                            )
                        )
                    )
                }
            }
            ELinkToSend.NIL.toString() -> {
                viewModel.roomGetReady()
            }
        }

    }

    override fun onBackPressed() {
        ActivityHome.getInstance(this)
    }

    override fun injectDependency(component: ComponentViewModel) {
        component.injectInvite(this@ActMeetInvite)
    }

    override fun getBinding(): FrgMeetWaitBinding {
        return FrgMeetWaitBinding.inflate(layoutInflater)
    }

    /** View model observers... */
    private fun stateMeetInvite() = Observer<StateMeet>{ meet ->
        when(meet){
            is StateMeet.Default  -> {

            }
            is StateMeet.ActionToBack -> {
                onBackPressed()
            }
            is StateMeet.MeetRun  -> {
                ActMeetRunning.getInstance(this, "")
                viewModel.toDefault()
                Toast.makeText(
                    this@ActMeetInvite,
                    "Meet Run",
                    Toast.LENGTH_SHORT).show()
            }
            is StateMeet.MeetWait -> {

            }
            is StateMeet.ShareInvite -> {
                val share = Intent(Intent.ACTION_SEND)
                share.type = "text/plain"
//        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
                share.putExtra(Intent.EXTRA_SUBJECT, "Sumrameet invite link!")
                share.putExtra(Intent.EXTRA_TEXT, meet.inviteLink)
                startActivity(Intent.createChooser(share, "Sumrameet invite link!"))

                Toast.makeText(
                    this@ActMeetInvite,
                    "Show Invite",
                    Toast.LENGTH_SHORT).show()
            }
            is StateMeet.SessionPst -> {
                mRoomState.add(true)
            }
            is StateMeet.SessionPut -> {
                mRoomState[0] = !mRoomState[0]
            }
            is StateMeet.RoomGet -> {
                mRoomLoaded.value.clear()
                mRoomLoaded.value.add(meet.dt)
                mRoomName.value = mRoomLoaded.value[0].rRoomName
                mRoomLink.value = mRoomLoaded.value[0].rRoomInvite
                mRoomParticipants.value.clear()
                mRoomParticipants.value = Emulation.getUserRoom() // meet.dt.mRoomUsers.toMutableList()
            }
            is StateMeet.RoomPut -> {
                mRoomLoaded.value.clear()
                mRoomLoaded.value.add(meet.dt)
                mRoomName.value = mRoomLoaded.value[0].rRoomName
                mRoomLink.value = mRoomLoaded.value[0].rRoomInvite
                mRoomParticipants.value.clear()
                mRoomParticipants.value = Emulation.getUserRoom() // meet.dt.mRoomUsers.toMutableList()
            }
            is StateMeet.RoomDel -> {
                mRoomLoaded.value.clear()
                mRoomLoaded.value.add(meet.dt)
                mRoomName.value = ""
                mRoomLink.value = ""
                mRoomParticipants.value.clear()
            }
            is StateMeet.RoomEmp -> {
                ActivityHome.getInstance(this)
            }
        }
    }

    fun setShow(state: Boolean){}

    private fun stateProgress() = Observer<BaseViewModel.LoadingState>{ load ->
        when(load){
            is BaseViewModel.LoadingState.LoadingNothing -> {}
            is BaseViewModel.LoadingState.LoadingShow -> {
//                views.activityHomeProgress.showScaleWithCenterToOut()
            }
            is BaseViewModel.LoadingState.LoadingHide -> {
//                views.activityHomeProgress.hideScaleWithOutToCenter()
            }
        }
    }

    private fun stateMessages() = Observer<BaseViewModel.MessageState>{ text ->
        when(text){
            is BaseViewModel.MessageState.MessageDanger -> {
                val snackbar = Snackbar.make(views.root, text.content.texts, Snackbar.LENGTH_LONG)
                snackbar.setBackgroundTint(text.color.back)
                snackbar.setTextColor(text.color.text)
                snackbar.show()
            }
            is BaseViewModel.MessageState.MessageSuccess -> {
                val snackbar = Snackbar.make(views.root, text.content.texts, Snackbar.LENGTH_LONG)
                snackbar.setBackgroundTint(text.color.back)
                snackbar.setTextColor(text.color.text)
                snackbar.show()
            }
            is BaseViewModel.MessageState.MessageInfo -> {
                val snackbar = Snackbar.make(views.root, text.content.texts, Snackbar.LENGTH_LONG)
                snackbar.setBackgroundTint(text.color.back)
                snackbar.setTextColor(text.color.text)
                snackbar.show()
            }
        }
    }

    val brush = Brush.horizontalGradient(
        colors = listOf(
            androidx.compose.ui.graphics.Color(0xff2188FF)
            , androidx.compose.ui.graphics.Color(0xff0366D6)
        )
    )

    val dH = 48

/***   ***   ***   ***   ***   ***   ***   ***   |   ***   ***   ***   ***   ***    ***   ***/

    @Preview
    @Composable
    @ExperimentalMaterialApi
    fun ComposePreview() {
        FetchWaitView()
    }

    @Composable
    @ExperimentalMaterialApi
    fun FetchWaitView() {
        val typedValue = TypedValue()
        val theme: Resources.Theme = viewModel.getAppCtx().getTheme()
        theme.resolveAttribute(R.attr.meetBarStartColor, typedValue, true)
        @ColorInt val startColor: Int = typedValue.data
        theme.resolveAttribute(R.attr.meetBarFinalColor, typedValue, true)
        @ColorInt val finalColor: Int = typedValue.data
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(
                    brush = Brush.horizontalGradient(
                        colors = listOf(Color(startColor), Color(finalColor))
                    )
                )
        ){
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(top = ThemeSetup.getDimen().sysBarHeight)
                    .background(color = Color.Transparent)
                ) {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .background(color = Color.Transparent)
                            .height(56.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ){
                        IconButton(onClick = { viewModel.actionToBack() },
                            modifier = Modifier
                                .height(ThemeSetup.getDimen().appBarHeight)
                                .width(ThemeSetup.getDimen().appBarHeight)
                        ){
                            Icon(
                                  painter = painterResource(id = R.drawable.ic_arrow_back_ios),
                                  tint = ThemeSetup.getTheme().cIconColor,
                                  contentDescription = "Button back",
                                  modifier = Modifier
                                      .padding(start = 28.dp)
                                      .height(ThemeSetup.getDimen().appBarHeight)
                                      .width(ThemeSetup.getDimen().appBarHeight))
                        }
                        Text(
                            text = viewModel.getAppCtx().getString(R.string.toMeetWaitToolbar),
                            style = StyleText.txtWaitLbl(),
                            modifier = Modifier.padding(start = 8.dp, end = 16.dp)
                        )
                    }
    /***   ***   ***   ***   ***   ***   ***   ***   |   ***   ***   ***   ***   ***   ***   ***/
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .clip(
                            shape = RoundedCornerShape(
                                topStart = ThemeSetup.getDimen().topBozRadius,
                                topEnd = ThemeSetup.getDimen().topBozRadius ))
                        .background(Color(0xffFAFBFC)) // (ThemeSetup.getTheme().cJetBackground)
                ){
                    Column(
                        modifier = Modifier
                            .padding(all = 25.dp)
                            .fillMaxWidth()
                    ) {
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(color = Color.Transparent)
                        ) {
                            Text(
                                text = viewModel.getAppCtx().getString(R.string.toMeetWaitRoomName),
                                style = StyleText.txtTabLbl()
                            )
                            Text(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(start = 8.dp, bottom = 8.dp),
                                text = mRoomName.value,
                                style = StyleText.txtTabLbl()
                            )
                        }
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(color = Color.Transparent)
                        ) {
                            Text(
                                text = viewModel.getAppCtx().getString(R.string.toMeetWaitRoomLink),
                                style = StyleText.txtTabLbl()
                            )
                            Text(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(start = 8.dp, bottom = 8.dp),
                                text = mRoomLink.value,
                                style = StyleText.txtTabLbl()
                            )
                        }
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(color = Color.Transparent)
                        ) {
                            Text(
                                text = viewModel.getAppCtx().getString(R.string.toMeetWaitLastTime),
                                style = StyleText.txtTabLbl()
                            )
                            Text(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(start = 8.dp, bottom = 16.dp),
                                text = mRoomLastTime.value,
                                style = StyleText.txtTabLbl()
                            )
                        }


                        Row(modifier = Modifier.fillMaxSize(),
                            verticalAlignment = Alignment.Bottom
                        ){
                            Column(
                                modifier = Modifier
                                    .padding(top = 16.dp),
                                verticalArrangement = Arrangement.Bottom
                            ) {
                                Row(
                                    modifier = Modifier.fillMaxWidth(),
                                    horizontalArrangement = Arrangement.Center,
                                    verticalAlignment = Alignment.CenterVertically
                                ) {
                                    Surface(
                                        onClick = {
                                            viewModel.actionToShareLink(true) },
                                        modifier = Modifier
                                            .weight(.5f)
                                            .fillMaxWidth()
                                            .height(dH.dp),
//                                            .padding(bottom = padD.dp),
                                        shape = RoundedCornerShape(size = (dH/2).dp)
                                    ) {
                                        Box(
                                            modifier = Modifier
                                                .background(
                                                    brush = brush,
                                                    shape = RoundedCornerShape(size = (dH/2).dp)),
                                            contentAlignment = Alignment.Center
                                        ) {
                                            Text(
                                                text = "Invite Link", //stringResource(id = R.string.text_recent_meet_cancel),
                                                style = StyleText.txtButtonLbl(),
                                                modifier = Modifier
                                                    .padding(horizontal = 20.dp)
                                            )
                                        }
                                    }
                                    Spacer(modifier = Modifier.width(8.dp))
                                    Surface(
                                        onClick = {
                                            viewModel.actionToShareLink(false) },
                                        modifier = Modifier
                                            .weight(.5f)
                                            .fillMaxWidth()
                                            .height(dH.dp),
//                                            .padding(bottom = padD.dp),
                                        shape = RoundedCornerShape(size = (dH/2).dp)
                                    ) {
                                        Box(
                                            modifier = Modifier
                                                .background(
                                                    brush = brush,
                                                    shape = RoundedCornerShape(size = (dH/2).dp)),
                                            contentAlignment = Alignment.Center
                                        ) {
                                            Text(
                                                text = "Invite Code", //stringResource(id = R.string.text_recent_meet_cancel),
                                                style = StyleText.txtButtonLbl(),
                                                modifier = Modifier
                                                    .padding(horizontal = 20.dp)
                                            )
                                        }
                                    }
                                }
                                Spacer(modifier = Modifier.height(8.dp))
                                Surface(
                                    onClick = {
                                        viewModel.actionToMeetRun() },
                                    modifier = Modifier
                                        .fillMaxWidth(1f)
                                        .height(dH.dp),
//                                            .padding(bottom = padD.dp),
                                    shape = RoundedCornerShape(size = (dH/2).dp)
                                ) {
                                    Box(
                                        modifier = Modifier
                                            .background(
                                                brush = brush,
                                                shape = RoundedCornerShape(size = (dH/2).dp)),
                                        contentAlignment = Alignment.Center
                                    ) {
                                        Text(
                                            text = "Run Meeting", //stringResource(id = R.string.text_recent_meet_cancel),
                                            style = StyleText.txtButtonLbl(),
                                            modifier = Modifier
                                                .padding(horizontal = 20.dp)
                                        )
                                    }
                                }
                            }
                        }

                    }

//
//                        ParticipantList(
//                            modifier = Modifier.constrainAs(lst) {
//                                top.linkTo    (lbl.bottom,   margin = 0.dp)
//                                bottom.linkTo (btn.top,      margin = 0.dp)
//                                start.linkTo  (parent.start, margin = 0.dp)
//                                end.linkTo    (parent.end,   margin = 0.dp)
//                            }
//                        )
//
//                        ButtonAction(
//                            modifier = Modifier.constrainAs(btn) {
//                                top.linkTo    (lst.bottom,    margin = 0.dp)
//                                bottom.linkTo (parent.bottom, margin = 0.dp)
//                                start.linkTo  (parent.start,  margin = 0.dp)
//                                end.linkTo    (parent.end,    margin = 0.dp)
//                            }
//                        )
//                    }
//                    ConstraintLayoutContent()

                }
            }
            DialogDemo()
        }
    }

    @Composable
    fun DialogDemo() { //showDialog: Boolean, setShow: (Boolean) -> Unit) {
        if (isShowDialog.value) {
            AlertDialog(
                onDismissRequest = {
                },
                title = {
                    Text("Title")
                },
                confirmButton = {
                    Button(
                        onClick = {
                            // Change the state to close the dialog
                            isShowDialog.value = false
                        },
                    ) {
                        Text("Confirm")
                    }
                },
                dismissButton = {
                    Button(
                        onClick = {
                            // Change the state to close the dialog
                            isShowDialog.value = false
                        },
                    ) {
                        Text("Dismiss")
                    }
                },
                text = {
                    Text("This is a text on the dialog")
                },
            )
        }
    }

    @Composable
    private fun PartTopText(modifier: Modifier){
        Column(
            modifier = modifier
                .fillMaxWidth()
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(color = Color.Transparent)
            ) {
                Text(
                    text = viewModel.getAppCtx().getString(R.string.toMeetWaitRoomName),
                    style = StyleText.txtTabLbl()
                )
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 8.dp, bottom = 8.dp),
                    text = mRoomName.value,
                    style = StyleText.txtTabLbl()
                )
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(color = Color.Transparent)
            ) {
                Text(
                    text = viewModel.getAppCtx().getString(R.string.toMeetWaitRoomLink),
                    style = StyleText.txtTabLbl()
                )
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 8.dp, bottom = 8.dp),
                    text = mRoomLink.value,
                    style = StyleText.txtTabLbl()
                )
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(color = Color.Transparent)
            ) {
                Text(
                    text = viewModel.getAppCtx().getString(R.string.toMeetWaitLastTime),
                    style = StyleText.txtTabLbl()
                )
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 8.dp, bottom = 16.dp),
                    text = mRoomLastTime.value,
                    style = StyleText.txtTabLbl()
                )
            }
        }
    }


    @Composable
    private fun ParticipantList(modifier: Modifier){
        val stateList = rememberLazyListState()
        LazyColumn(
            state = stateList,
            modifier = modifier
//                .fillMaxSize(1f)
        ) {
            items(mRoomParticipants.value.size) { index ->
                Column(
                    modifier = Modifier
                        .height(48.dp)
                ) {
                    Text(text = mRoomParticipants.value[index].pDisplayName)
                    mRoomParticipants.value[index].pUserEmail.let{
                        Text(text = it)
                    }
                }
            }
        }
    }

    @Composable
    private fun ButtonAction(modifier: Modifier){
        Column(
            modifier = modifier
                .padding(top = 16.dp)
        ) {
            Button(
                onClick = { viewModel.actionToShareLink(true) },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 8.dp)
                    .height(44.dp),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Color(0xffbbbbbb)
                )
            ) {
                Text(
                    text = "Invite Link",
                    style = StyleText.txtPgTkn()
                )
            }

            Button(
                onClick = {
                    isShowDialog.value = true
//                    viewModel.actionToMeetRun()
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 8.dp)
                    .height(44.dp),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Color(0xffbbbbbb)
                )
            ) {
                Text(
                    text = "Run Meeting",
                    style = StyleText.txtPgTkn()
                )
            }
        }
    }

    @Composable
    fun ConstraintLayoutContent() {
        ConstraintLayout(
            modifier = Modifier
                .fillMaxSize()
                .background(Color.Transparent)
        ){
            val (lbl, lst, btn) = createRefs()

            PartTopText(
                modifier = Modifier.constrainAs(lbl){
                    top.linkTo    (parent.top,   margin = 0.dp)
                    bottom.linkTo (lst.top,      margin = 0.dp)
                    start.linkTo  (parent.start, margin = 0.dp)
                    end.linkTo    (parent.end,   margin = 0.dp)
                }
            )

            ParticipantList(
                modifier = Modifier.constrainAs(lst) {
                        top.linkTo    (lbl.bottom,   margin = 0.dp)
                        bottom.linkTo (btn.top,      margin = 0.dp)
                        start.linkTo  (parent.start, margin = 0.dp)
                        end.linkTo    (parent.end,   margin = 0.dp)
                    }
            )

            ButtonAction(
                modifier = Modifier.constrainAs(btn) {
                        top.linkTo    (lst.bottom,    margin = 0.dp)
                        bottom.linkTo (parent.bottom, margin = 0.dp)
                        start.linkTo  (parent.start,  margin = 0.dp)
                        end.linkTo    (parent.end,    margin = 0.dp)
                    }
            )
        }
    }

    @Composable
    fun MovieItem(roomParticipant: roomModels.ParticipantsRoom) {
        Row(
            modifier = Modifier
                .padding(start = 16.dp, top = 16.dp, end = 16.dp)
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Column() {
                MovieTitle(
                    roomParticipant.pDisplayName,
                    modifier = Modifier.weight(1f)
                )
                MovieTitle(
                    roomParticipant.pUserEmail,
                    modifier = Modifier.weight(1f)
                )
            }
        }
    }

    @Composable
    fun MovieTitle(
        title: String,
        modifier: Modifier = Modifier
    ) {
        Text(
            modifier = modifier,
            text = title,
            maxLines = 2,
            style = MaterialTheme.typography.h6,
            overflow = TextOverflow.Ellipsis
        )
    }

}
