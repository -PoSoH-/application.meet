package meet.sumra.app.act

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import androidx.annotation.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentFactory
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.google.android.material.snackbar.Snackbar
import meet.sumra.app.AppMeet
import meet.sumra.app.R
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.ext.showOptimizedSnackbar
import meet.sumra.app.utils.setup.FontScale
import meet.sumra.app.utils.setup.ThemeUtils

abstract class ActivityBase <VB: ViewBinding> : AppCompatActivity() {

    /* ==========================================================================================
     * View
     * ========================================================================================== */

    protected lateinit var views: VB

    private lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var fragmentFactory : FragmentFactory

    protected val viewModelProvider
        get() = ViewModelProvider(this, viewModelFactory)

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
//        LogUtil.info(txt = "onCreate Activity ${javaClass.simpleName}")
        createDaggerDependencies()
        ThemeUtils.setActivityTheme(this, getOtherThemes())

        fragmentFactory = FragmentFactory()
        supportFragmentManager.fragmentFactory = fragmentFactory
        super.onCreate(savedInstanceState)
        doBeforeSetContentView()
        // Hack for font size
        applyFontSize()

        views = getBinding()
        setContentView(views.root)

        backGroundColor()
//        val toolBar = views.root.findViewById<Toolbar>(R.id.customization_toolbar)
//        setSupportActionBar(toolBar)
////        toolBar.setBackgroundColor(Color.TRANSPARENT)

        initUiAndData()

    }

//    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun backGroundColor() {
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        window.statusBarColor = ContextCompat.getColor(this, android.R.color.transparent)
        window.navigationBarColor = ContextCompat.getColor(this, android.R.color.transparent)
        window.setBackgroundDrawableResource(R.drawable.meet_bars_gradient)
    }

    private fun applyFontSize() {
        resources.configuration.fontScale = FontScale.getFontScaleValue(this).scale

        @Suppress("DEPRECATION")
        resources.updateConfiguration(resources.configuration, resources.displayMetrics)
    }

    abstract fun getBinding(): VB

    open fun displayInFullscreen() = false

    open fun doBeforeSetContentView() = Unit

    open fun initUiAndData() = Unit

    open fun getOtherThemes(): ActivityOtherThemes = ActivityOtherThemes.Default

    /* ==========================================================================================
     * PUBLIC METHODS
     * ========================================================================================== */

    fun showSnackbar(message: String) {
        getConstraintLayout()?.showOptimizedSnackbar(message)
    }

    fun showSnackbar(message: String, @StringRes withActionTitle: Int?, action: (() -> Unit)?) {
        getConstraintLayout()?.let {
            Snackbar.make(it, message, Snackbar.LENGTH_LONG).apply {
                withActionTitle?.let {
                    setAction(withActionTitle, { action?.invoke() })
                }
            }.show()
        }
    }

    open fun getConstraintLayout(): ConstraintLayout? = null

    protected abstract fun injectDependency(component: ComponentViewModel)

    private fun createDaggerDependencies() {
        injectDependency((application as AppMeet).getViewModelComponent())
    }
}