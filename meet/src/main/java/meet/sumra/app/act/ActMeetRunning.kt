package meet.sumra.app.act

import android.Manifest
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.facebook.infer.annotation.ThreadConfined
import com.facebook.react.modules.core.PermissionListener
import com.google.android.material.snackbar.Snackbar
import meet.sumra.app.CONST
import meet.sumra.app.data.enums.ELinkToSend
import meet.sumra.app.databinding.ActMeetBinding
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.ext.ExtAnimation.hideScaleWithOutToCenter
import meet.sumra.app.ext.ExtAnimation.showScaleWithCenterToOut
import meet.sumra.app.vm.BaseViewModel
import meet.sumra.app.vm.vm_home.StateMeet
import meet.sumra.app.vm.vm_meet.ViewModelMeet
import org.jitsi.meet.sdk.*
import sdk.net.meet.LogUtil
import javax.inject.Inject

class ActMeetRunning : ActivityBase<ActMeetBinding>(), JitsiMeetActivityInterface {

    companion object {
        fun getInstance(activity: Activity, roomId: String){
            val intent = Intent(activity, ActMeetRunning::class.java)
            intent.putExtra(CONST.KEY_ROOM_ID, roomId)
            activity.startActivity(intent)
            activity.finish()
        }
    }

    private val mRoomState = mutableListOf<Boolean>()
    private val mRoomId = mutableListOf<String>()
    private lateinit var meetView: JitsiMeetView

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            onBroadcastReceived(intent)
        }
    }

    @Inject
    lateinit var viewModel: ViewModelMeet

    @ThreadConfined("UI")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mRoomId.add(0, intent.getStringExtra(CONST.KEY_ROOM_ID)!!)

        views.toMeetSysAppBarContainer.background = viewModel.fetchGradient()
        viewModel.mViewState.observe(this@ActMeetRunning, stateMeetRunning())
        viewModel.viewProgressState.observe(this@ActMeetRunning, stateProgress())
        viewModel.viewMessageState.observe(this@ActMeetRunning, stateMessages())

        meetView = JitsiMeetView(this)
        views.root.addView(meetView)

        viewModel.fetchPreparingOptions()
            registerForBroadcastMessages()
    }

    override fun onStart() {
//        check roomState for null
//        if(!mRoomState.isNullOrEmpty()) {
//            viewModel.sessionUpdate(ToSession(roomState[0].id))
//        }
        super.onStart()
    }

    override fun onPause() {
//        viewModel.sessionUpdate(ToSession(roomState[0].id))
        super.onPause()
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
        super.onDestroy()
//        val data = JitsiMeet.getDefaultConferenceOptions()
//        viewMeet.unmountReactApplication()
    }

    private fun registerForBroadcastMessages() {
        val intentFilter = IntentFilter()
        for (type in BroadcastEvent.Type.values()) {
            intentFilter.addAction(type.action)
        }
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(broadcastReceiver, intentFilter)
    }
//
//    // Example for handling different JitsiMeetSDK events
    private fun onBroadcastReceived(intent: Intent?) {
        if (intent != null) {
            val event = BroadcastEvent(intent)
            when (event.getType()) {
                BroadcastEvent.Type.CONFERENCE_JOINED     -> LogUtil.info(txt  = "Conference Joined with url%s, ${event.getData().get("url")}")
                BroadcastEvent.Type.PARTICIPANT_JOINED    -> LogUtil.info(txt  = "Participant joined%s,         ${event.getData().get("name")}")
                BroadcastEvent.Type.CHAT_MESSAGE_RECEIVED -> LogUtil.info(txt  = "Chat Message received%s,      ${event.getData().get("name")}")
                BroadcastEvent.Type.PARTICIPANT_LEFT      -> LogUtil.info(txt  = "Participant left%s,           ${event.getData().get("name")}")

                BroadcastEvent.Type.CONFERENCE_WILL_JOIN  -> {LogUtil.info(txt  = "Conference will join, ${event.getData().get("name")}")}
                BroadcastEvent.Type.AUDIO_MUTED_CHANGED   -> {LogUtil.info(txt  = "Audio muted changed,  ${event.getData().get("name")}")}

                BroadcastEvent.Type.ENDPOINT_TEXT_MESSAGE_RECEIVED -> {LogUtil.info(txt  = "Endpoint text message received, ${event.getData().get("name")}")}
                BroadcastEvent.Type.SCREEN_SHARE_TOGGLED           -> {LogUtil.info(txt  = "Screen share toggled, ${event.getData().get("name")}")}
                BroadcastEvent.Type.PARTICIPANTS_INFO_RETRIEVED    -> {LogUtil.info(txt  = "Participant info retrieved, ${event.getData().get("name")}")}

                BroadcastEvent.Type.CHAT_TOGGLED        -> {LogUtil.info(txt  = "Chat toggled, ${event.getData().get("name")}")}
                BroadcastEvent.Type.VIDEO_MUTED_CHANGED -> {LogUtil.info(txt  = "Video muted changed, ${event.getData().get("name")}")}

                BroadcastEvent.Type.CONFERENCE_TERMINATED -> {
                    LogUtil.info(txt  = "Conference terminated, ${event.getData().get("name")}")
                    finalSessionConference()
                }
            }
        }
    }

    private fun finalSessionConference(){
        ActivityHome.getInstance(this)
    }

//    private fun hangUp() {
//        val hangupBroadcastIntent: Intent = BroadcastIntentHelper.buildHangUpIntent()
//        LocalBroadcastManager.getInstance(org.webrtc.ContextUtils.getApplicationContext()).sendBroadcast(hangupBroadcastIntent)
//    }

    override fun injectDependency(component: ComponentViewModel) {
        component.injectRunning(this@ActMeetRunning)
    }

    override fun getBinding(): ActMeetBinding {
        return ActMeetBinding.inflate(layoutInflater)
    }

    override fun requestPermissions(p0: Array<out String>?, p1: Int, p2: PermissionListener?) {
        LogUtil.info("tag", "permissions")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?){
        JitsiMeetActivityDelegate.onActivityResult(this, requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onNewIntent(intent: Intent?) {
        JitsiMeetActivityDelegate.onNewIntent(intent)
        super.onNewIntent(intent)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        JitsiMeetActivityDelegate.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                initialCameraPreview()
            } else {
                requestPermission();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    override fun onBackPressed() {
        ActMeetInvite.getInstance(
            activity = this,
            type     = ELinkToSend.NIL,
            data     = null,
            pass     = null)
    }

    private fun initialCameraPreview(){
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)
//        cameraProviderFuture.addListener(Runnable {
//            // Used to bind the lifecycle of cameras to the lifecycle owner
//            cameraProvider = cameraProviderFuture.get()
//            // Preview
//            val preview = Preview.Builder()
//                .build()
//                .also {
//                    it.setSurfaceProvider(views.frgCustomCamera.surfaceProvider)
//                }
//            // Select back camera as a default
//            val cameraSelector = CameraSelector.DEFAULT_FRONT_CAMERA
//
//            try {
//                // Unbind use cases before rebinding
//                cameraProvider?.let { provider ->
//                    provider.unbindAll()
//                    // Bind use cases to camera
//                    provider.bindToLifecycle(this, cameraSelector, preview)
//                }
//
//            } catch(exc: Exception) {
//                LogUtil.error(txt = "Use case binding failed ${exc.message}")
//            }
//        }, ContextCompat.getMainExecutor(this))
    }

    private fun requestPermission(){
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
//            || ContextCompat.checkSelfPermission(this@ActivityHome, Manifest.permission.WRITE_EXTERNAL_STORAGE) !== PackageManager.PERMISSION_GRANTED){
            requestPermissions(arrayOf(Manifest.permission.CAMERA /*Manifest.permission.WRITE_EXTERNAL_STORAGE*/), 1)
        } else {
            initialCameraPreview()
        }
    }

    /** View model observers... */
    private fun stateMeetRunning() = Observer<StateMeet>{meet ->
        when(meet){
            is StateMeet.Default     -> { }
            is StateMeet.MeetWait    -> { }
            is StateMeet.SessionPst  -> {
                mRoomState.add(true)
            }
            is StateMeet.SessionPut  -> {
                mRoomState[0] = !mRoomState[0]
            }
            is StateMeet.RoomPut     -> { }
            is StateMeet.RoomDel     -> { }
            is StateMeet.MeetOptions -> {
                meetView.join(meet.Options)
            }
            is StateMeet.RoomEmp     -> {
//                viewModel.roomGetLoadRoomById(mRoomId[0])
            }
        }
    }

    private fun stateProgress() = Observer<BaseViewModel.LoadingState>{ load ->
        when(load){
            is BaseViewModel.LoadingState.LoadingNothing -> { }
            is BaseViewModel.LoadingState.LoadingShow -> {
                views.activityHomeProgress.showScaleWithCenterToOut()
            }
            is BaseViewModel.LoadingState.LoadingHide -> {
                views.activityHomeProgress.hideScaleWithOutToCenter()
            }
        }
    }

    private fun stateMessages() = Observer<BaseViewModel.MessageState>{ text ->
        when(text){
            is BaseViewModel.MessageState.MessageDanger -> {
                val snackbar = Snackbar.make(views.actMeetConference, text.content.texts, Snackbar.LENGTH_LONG)
                snackbar.setBackgroundTint(text.color.back)
                snackbar.setTextColor(text.color.text)
                snackbar.show()
            }
            is BaseViewModel.MessageState.MessageSuccess -> {
                val snackbar = Snackbar.make(views.actMeetConference, text.content.texts, Snackbar.LENGTH_LONG)
                snackbar.setBackgroundTint(text.color.back)
                snackbar.setTextColor(text.color.text)
                snackbar.show()
            }
            is BaseViewModel.MessageState.MessageInfo -> {
                val snackbar = Snackbar.make(views.actMeetConference, text.content.texts, Snackbar.LENGTH_LONG)
                snackbar.setBackgroundTint(text.color.back)
                snackbar.setTextColor(text.color.text)
                snackbar.show()
            }
        }
    }

}