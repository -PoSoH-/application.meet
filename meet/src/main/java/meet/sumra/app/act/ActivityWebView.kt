package meet.sumra.app.act

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.lifecycle.Observer
import meet.sumra.app.databinding.ActWebViewBinding
import meet.sumra.app.di.component.ComponentViewModel
import meet.sumra.app.ext.ExtAnimation.hideUtoD
import meet.sumra.app.ext.ExtAnimation.showDtoU
import meet.sumra.app.vm.vm_home.StateHome
import meet.sumra.app.vm.vm_home.StateMeet
import meet.sumra.app.vm.vm_home.ViewModelHome
import sdk.net.meet.LogUtil
import javax.inject.Inject


class ActivityWebView: ActivityBase<ActWebViewBinding>() {

    internal lateinit var viewModel: ViewModelHome
    @Inject set

//    private lateinit var geckoSession: GeckoSession
//    private var geckoRuntime: GeckoRuntime? = null

    companion object {
        fun getInstance(activity: Activity) {
            val pioneerIntent = Intent(activity, ActivityWebView::class.java)
            activity.startActivity(pioneerIntent)
            activity.finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initObservable()
//        initiateWebView()
    }
    override fun getBinding(): ActWebViewBinding {
        return ActWebViewBinding.inflate(layoutInflater)
    }

    override fun injectDependency(component: ComponentViewModel) {
        component.injectWeb(this@ActivityWebView)
    }

    override fun onStop() {
        super.onStop()
//        geckoSession.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
//        geckoRuntime = null
//        geckoSession.close()
//        views.actGeckoView.
    }

//    private val mWebView = views.actWebViewRTC
//    private val webViewClient = WaitForLoadedClient(this)
//    private val webChromeClient = WaitForProgressClient(this)
//    private val mWebView = views.actWebViewRTC

//    class WaitForProgressClient(onUiThread: WebViewOnUiThread) : WebChromeClient() {
//        private val mOnUiThread: WebViewOnUiThread
//        override fun onProgressChanged(view: WebView, newProgress: Int) {
//            super.onProgressChanged(view, newProgress)
//            mOnUiThread.onProgressChanged(newProgress)
//        }
//
//        init {
//            mOnUiThread = onUiThread
//        }
//    }
//
//    /**
//     * A WebViewClient that captures the onPageFinished for use in
//     * waitFor functions. Using initializeWebView sets the WaitForLoadedClient
//     * into the WebView. If a test needs to set a specific WebViewClient and
//     * needs the waitForCompletion capability then it should derive from
//     * WaitForLoadedClient or call WebViewOnUiThread.onPageFinished.
//     */
//    class WaitForLoadedClient(onUiThread: WebViewOnUiThread) : WebViewClient() {
//        private val mOnUiThread: WebViewOnUiThread
//        override fun onPageFinished(view: WebView, url: String) {
//            super.onPageFinished(view, url)
//            mOnUiThread.onPageFinished()
//        }
//
//        override fun onPageStarted(view: WebView, url: String, favicon: Bitmap) {
//            super.onPageStarted(view, url, favicon)
//            mOnUiThread.onPageStarted()
//        }
//
//        init {
//            mOnUiThread = onUiThread
//        }
//    }

    private fun initiateWebView() {

//        initWebViewRTC()
        initWebViewBase()



//        try {
//            val session = GeckoSessionSettings.Builder().run {
//                usePrivateMode           (false)
//                useTrackingProtection    (true)
//                userAgentMode            (USER_AGENT_MODE_MOBILE)
//                userAgentOverride        ("")
//                suspendMediaWhenInactive (true)
//                allowJavascript          (true)
////                runOnUiThread(runtime())
//            }
//
//            val geckoRunSession = GeckoRuntimeSettings.Builder().run {
////                aboutConfigEnabled          (true)
////                loginAutofillEnabled        (true)
//                fontInflation               (true)
//                automaticFontSizeAdjustment (true)
//                webFontsEnabled             (true)
////                webManifest(true)
//            }
//
//            geckoRuntime = GeckoRuntime.getDefault(this) //, geckoRunSession.build())
//
//            geckoSession = GeckoSession(session.build())
//            geckoSession.open(geckoRuntime!!)
//
//            views.actGeckoView.run {
//                setSession(geckoSession)
//                requestFocus()
//            }
//
//            geckoSession.loadUri(URL_MEMBERSHIP)
//
//        } catch (ext: Throwable){
//            logI(
//                tag = "ACTIVE HOME",
//                text = "ERROR INITIATE FIREFOX GECKO VIEW ... ${ext.message}")
//        }
    }

    private fun initWebViewBase() {

        views.actWebView.run {
//            loadUrl(URL_MEMBERSHIP)
//            clearCache(true)
//            webChromeClient              = WebChromeClient().run {
//
//            }

            webViewClient = WebViewClient()

/***********************************************************************************************************************
 *         these settings allow you to download a site from the Amazon s3 file server to WebView
 *         Do not touch
 **********************************************************************************************************************/
            settings.apply {
                pluginState = WebSettings.PluginState.ON
                javaScriptEnabled = true
                javaScriptCanOpenWindowsAutomatically = true
                setSupportMultipleWindows(false)
                setSupportZoom(false)
                loadWithOverviewMode = true
                useWideViewPort = true
                domStorageEnabled = true

                isVerticalScrollBarEnabled   = false
                isHorizontalScrollBarEnabled = false

                setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE)

                val m1 = WebSettings::class.java.getMethod("setDomStorageEnabled", *arrayOf<Class<*>>(java.lang.Boolean.TYPE))
                m1.invoke(this@apply, java.lang.Boolean.TRUE)

                val m2 = WebSettings::class.java.getMethod("setDatabaseEnabled", *arrayOf<Class<*>>(java.lang.Boolean.TYPE))
                m2.invoke(this@apply, java.lang.Boolean.TRUE)

//                val m3 = WebSettings::class.kotlin.getMethod("setDatabasePath", *arrayOf<Class<*>>(String::class.kotlin))
//                m3.invoke(this@apply, "/data/data/$packageName/databases/")

                val m4 = WebSettings::class.java.getMethod("setAppCacheMaxSize", *arrayOf<Class<*>>(java.lang.Long.TYPE))
                m4.invoke(this@apply, 1024 * 1024 * 8)

                val m5 = WebSettings::class.java.getMethod("setAppCachePath", *arrayOf<Class<*>>(String::class.java))
                m5.invoke(this@apply, "/data/data/$packageName/cache/")

                val m6 = WebSettings::class.java.getMethod("setAppCacheEnabled", *arrayOf<Class<*>>(java.lang.Boolean.TYPE))
                m6.invoke(this@apply, java.lang.Boolean.TRUE)

            }

            webViewClient = object: WebViewClient(){
                override fun shouldOverrideUrlLoading(view: WebView, url: String?): kotlin.Boolean {
                    // TODO Auto-generated method stub
                    views.webViewGroupProgress.showDtoU()
                    view.loadUrl(URL_MEMBERSHIP)
                    return true
                }

                override fun onPageFinished(view: WebView?, url: String?) {
                    // TODO Auto-generated method stub
                    super.onPageFinished(view, url)
                    views.webViewGroupProgress.hideUtoD()
                }
            }

            loadUrl(URL_MEMBERSHIP)
        }
    }

    private fun initWebViewRTC() {
//        views.actWebViewRTC.run{
//
//        }
    }

    private fun initObservable(){
        viewModel.viewState.observe(this, initiateState())
    }

    private fun initiateState() = Observer<StateHome> { changed ->
        when(changed){
            is StateHome.Default -> {
                LogUtil.info("WEB VIEW", "BUTTON BACK PRESSED...")
                ActivityHome.getInstance(this)
            }
            is StateHome.Pioneer -> {
                LogUtil.info("WEB VIEW", "INITIATE WEB VIEW...")
                initiateWebView()
            }
        }
    }

    override fun onBackPressed() {
        viewModel.resetState()
        super.onBackPressed()
    }

    private fun runtime(){
        kotlinx.coroutines.Runnable {
            LogUtil.info("COROUTINE UI", "START")
        }
    }

    private val URL_MEMBERSHIP = "http://sumrawallets-web.s3-website-us-west-2.amazonaws.com/pioneer_memberships"

//    private val URL_MEMBERSHIP = "https://google.com"
//    private val URL_MEMBERSHIP = "https://www.facebook.com/"

}