package meet.sumra.app.utils.setup

import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.util.TypedValue
import android.view.Menu
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import androidx.core.content.edit
import androidx.core.graphics.drawable.DrawableCompat
import com.google.gson.Gson
import meet.sumra.app.CONST
import meet.sumra.app.R
import meet.sumra.app.act.ActivityOtherThemes
import meet.sumra.app.data.mdl.DtAppSetup
import meet.sumra.app.di.DefaultSharedPreferences
import meet.sumra.app.utils.LogUtil
import java.util.concurrent.atomic.AtomicReference

object ThemeUtils {
    // preference key
    const val APPLICATION_THEME_KEY = "APPLICATION_THEME_KEY"

    // the theme possible values
//    private const val SYSTEM_THEME_VALUE = "system"
//    private const val THEME_DARK_VALUE = "dark"
//    private const val THEME_LIGHT_VALUE = "light"
//    private const val THEME_BLACK_VALUE = "black"

    // The default theme
//    private const val DEFAULT_THEME = SYSTEM_THEME_VALUE

    private var currentTheme = AtomicReference<Boolean>(false)

    private val mColorByAttr = HashMap<Int, Int>()

    // init the theme
    fun init(context: Context) {
        val theme = getApplicationTheme(context)
        setApplicationTheme(context, theme)
    }

    /**
     * @return true if current theme is System
     */
//    fun isSystemTheme(context: Context): Boolean {
//        val theme = getApplicationTheme(context)
//        return theme == SYSTEM_THEME_VALUE
//    }

    /**
     * @return true if current theme is Light or current theme is System and system theme is light
     */
//    fun isLightTheme(context: Context): Boolean {
//        val theme = getApplicationTheme(context)
//        return theme == THEME_LIGHT_VALUE
//                || (theme == SYSTEM_THEME_VALUE && !isSystemDarkTheme(context.resources))
//    }

    /**
     * Provides the selected application theme
     *
     * @param context the context
     * @return the selected application theme
     */
    fun getApplicationTheme(context: Context): Boolean {
        val prefs = context.getSharedPreferences(CONST.APP_SETTING, Context.MODE_PRIVATE)
        val themeFromPref = Gson().fromJson(prefs.getString(DtAppSetup.SETUP_KEY, Gson().toJson(DtAppSetup)), DtAppSetup::class.java)
        return themeFromPref.stateDarkMode
    }

    /**
     * @return true if system theme is dark
     */
//    private fun isSystemDarkTheme(resources: Resources): Boolean {
//        return resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES
//    }

    /**
     * Update the application theme
     *
     * @param aTheme the new theme
     */
    fun setApplicationTheme(context: Context, aTheme: Boolean) {
        currentTheme.set(aTheme)
        context.setTheme(
            when (aTheme) {
                true -> R.style.app_theme_dark
                else -> R.style.app_theme_light
            }
        )
        // Clear the cache
        mColorByAttr.clear()
    }

    /**
     * Set the activity theme according to the selected one.
     *
     * @param activity the activity
     */
    fun setActivityTheme(activity: Activity, otherThemes: ActivityOtherThemes) {
        when (getApplicationTheme(activity)) {
            true -> activity.setTheme(otherThemes.dark)
            else -> activity.setTheme(otherThemes.light)
//            SYSTEM_THEME_VALUE -> if (isSystemDarkTheme(activity.resources)) activity.setTheme(otherThemes.dark)
//            THEME_DARK_VALUE   -> activity.setTheme(otherThemes.dark)
//            THEME_BLACK_VALUE  -> activity.setTheme(otherThemes.black)
        }

        mColorByAttr.clear()
    }

    /**
     * Translates color attributes to colors
     *
     * @param c              Context
     * @param colorAttribute Color Attribute
     * @return Requested Color
     */
    @ColorInt
    fun getColor(c: Context, @AttrRes colorAttribute: Int): Int {
        return mColorByAttr.getOrPut(colorAttribute) {
            try {
                val color = TypedValue()
                c.theme.resolveAttribute(colorAttribute, color, true)
                color.data
            } catch (e: Exception) {
                LogUtil.info(e.toString(), "Unable to get color")
                ContextCompat.getColor(c, android.R.color.holo_red_dark)
            }
        }
    }

    fun getAttribute(c: Context, @AttrRes attribute: Int): TypedValue? {
        try {
            val typedValue = TypedValue()
            c.theme.resolveAttribute(attribute, typedValue, true)
            return typedValue
        } catch (e: Exception) {
            LogUtil.info(e.toString(), "Unable to get color")
        }
        return null
    }

    /**
     * Update the menu icons colors
     *
     * @param menu  the menu
     * @param color the color
     */
    fun tintMenuIcons(menu: Menu, color: Int) {
        for (i in 0 until menu.size()) {
            val item = menu.getItem(i)
            val drawable = item.icon
            if (drawable != null) {
                val wrapped = DrawableCompat.wrap(drawable)
                drawable.mutate()
                DrawableCompat.setTint(wrapped, color)
                item.icon = drawable
            }
        }
    }

    /**
     * Tint the drawable with a theme attribute
     *
     * @param context   the context
     * @param drawable  the drawable to tint
     * @param attribute the theme color
     * @return the tinted drawable
     */
    fun tintDrawable(context: Context, drawable: Drawable, @AttrRes attribute: Int): Drawable {
        return tintDrawableWithColor(drawable, getColor(context, attribute))
    }

    /**
     * Tint the drawable with a color integer
     *
     * @param drawable the drawable to tint
     * @param color    the color
     * @return the tinted drawable
     */
    fun tintDrawableWithColor(drawable: Drawable, @ColorInt color: Int): Drawable {
        val tinted = DrawableCompat.wrap(drawable)
        drawable.mutate()
        DrawableCompat.setTint(tinted, color)
        return tinted
    }
}