package meet.sumra.app.utils

//import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.ContactsContract
import java.io.ByteArrayInputStream
import java.io.InputStream

fun Context.openPhoto(photoUri: Uri): InputStream? {
//    val contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId)
//    val photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY)
    val cursor: Cursor =
        applicationContext.getContentResolver().query(
            photoUri,
            arrayOf(ContactsContract.Contacts.Photo.PHOTO),
            null,
            null,
            null) ?: return null
    try {
        cursor.let {
            if (cursor.moveToFirst()) {
                val data = cursor.getBlob(0)
                if (data != null) {
                    return ByteArrayInputStream(data)
                }
            }
        }
    } finally {
        cursor.close()
    }
    return null
}