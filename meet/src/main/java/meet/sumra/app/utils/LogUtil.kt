package meet.sumra.app.utils

import android.util.Log
import meet.sumra.app.BuildConfig

object LogUtil {
    fun error(txt: String){
        if(BuildConfig.DEBUG){
            Log.e("${BuildConfig.APPLICATION_ID}::", txt)
        }
    }
    fun error(tag: String, txt: String){
        if(BuildConfig.DEBUG){
            Log.e("${BuildConfig.APPLICATION_ID}::$tag::", txt)
        }
    }
    fun info(txt: String){
        if(BuildConfig.DEBUG){
            Log.i("${BuildConfig.APPLICATION_ID}::", txt)
        }
    }
    fun info(tag: String, txt: String){
        if(BuildConfig.DEBUG){
            Log.i("${BuildConfig.APPLICATION_ID}::$tag::", txt)
        }
    }
}