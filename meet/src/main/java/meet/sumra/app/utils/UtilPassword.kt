package meet.sumra.app.utils

import java.lang.StringBuilder
import kotlin.random.Random

object UtilPassword {
    private val symbols = arrayOf(
        "q","Q","w","W","e","E","r","R","t","T","y","Y","u","U","i","I","o","O","p","P","a","A","s",
        "S","d","D","f","F","g","G","h","H","j","J","k","K","l","L","z","Z","x","X","c","C","v","V",
        "b","B","n","N","m","M","1","2","3","4","5","6","7","8","9","0"
    )

    fun generatePass(count: Long): String {
        val n = mutableListOf(0)
        val d = StringBuilder()
        while (count > n[0]){
            d.append(symbols[Random.nextInt(0, symbols.size-1)])
            n[0]++
        }
        return d.toString()
    }
}