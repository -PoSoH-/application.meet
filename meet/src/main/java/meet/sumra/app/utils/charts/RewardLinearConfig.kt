package meet.sumra.app.utils.charts

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.CandleEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import com.github.mikephil.charting.utils.Utils
import meet.sumra.app.R
import net.sumra.auth.helpers.Finals
import java.text.DecimalFormat

class RewardLinearConfig(chart: LineChart) {

    private val mContext : Context
    private val mTypeFace: Typeface

    init {
        mContext  = chart.context
        mTypeFace = Typeface.createFromAsset(mContext.assets, Finals.FONT_FACE)

//        viewFragment.frgRewardsChartsShow.setViewPortOffsets(100f, 0f, 0f, 100f)
//        viewFragment.frgRewardsChartsShow.setBackgroundColor(Color.rgb(104, 241, 175))
        chart.setBackgroundColor(Color.TRANSPARENT)
        chart.getDescription().setEnabled(false) // вмикає/вимикає напис внизу графіка
        // enable touch gestures
//        viewFragment.frgRewardsChartsShow.setTouchEnabled(true)
        // enable scaling and dragging
//        viewFragment.frgRewardsChartsShow.setDragEnabled(true)
        // if disabled, scaling can be done on x- and y-axis separately
//        viewFragment.frgRewardsChartsShow.setPinchZoom(true)
//        viewFragment.frgRewardsChartsShow.setDrawGridBackground(true)
//        viewFragment.frgRewardsChartsShow.setMaxHighlightDistance(300f)

        chart.isScaleXEnabled = true
        chart.isScaleYEnabled = false
        chart.getXAxis().apply {
            typeface = mTypeFace
            textColor = ResourcesCompat.getColor(mContext.resources, R.color.text_100, null)
            setLabelCount(4, true)
            setDrawAxisLine(true)
            setPosition(XAxis.XAxisPosition.BOTTOM)
            setDrawGridLines(false)
            enableGridDashedLine(10f, 10f, 0f)
            axisLineColor = ResourcesCompat.getColor(
                mContext.resources,
                R.color.home_app_separate,
                null
            ) //Color.TRANSPARENT
            isEnabled = true
        }

        chart.getAxisRight().isEnabled = false
        chart.getAxisLeft().apply {
            typeface = mTypeFace
            setLabelCount(6, false)
            setDrawZeroLine(true)
            zeroLineWidth = 1.2F
            textColor = ResourcesCompat.getColor(mContext.resources, R.color.text_100, null)
            setDrawGridLines(true)
            enableGridDashedLine(10f, 10f, 0f)
            axisLineColor = ResourcesCompat.getColor(
                mContext.resources,
                R.color.home_app_separate,
                null
            ) //Color.TRANSPARENT
        }

        // draw limit lines behind data instead of on top
//        y.setDrawLimitLinesBehindData(true)
//        x.setDrawLimitLinesBehindData(true)

        /* Marker customize*/

        // create marker to display box when values are selected
        val mv: MarkerForChart = MarkerForChart(mContext, R.layout.frg_rewards_marker_chart)
        // Set the marker to the chart
        mv.setChartView(chart)
        chart.setMarker(mv)

        chart.getLegend().setEnabled(false)
        chart.animateXY(2000, 2000)

        // don't forget to refresh the drawing

        // don't forget to refresh the drawing
//        chart.invalidate()
    }

    private class MarkerForChart(ctx: Context, layoutResource:Int): MarkerView(ctx, layoutResource) {

        private val tvContent: TextView

        init {
            tvContent = findViewById(R.id.txtViewContent);
        }

        override fun refreshContent(e: Entry, highlight: Highlight) {
//            val s = (e.y % 60).toInt()
//            val m = ((e.y - s) / 60).toInt()
//
//            tvContent.text = "${m} min ${s} sec"
////            tvContent.text = String.format(
////                "x: %s, y: %s",
////                xAxisValueFormatter.getFormattedValue(e.x, null),
////                format.format(e.y.toDouble())
////            )
//            super.refreshContent(e, highlight)
            val f = DecimalFormat("#.00")
            tvContent.text = "${f.format(e.y)} DVT"
//
//            if (e is CandleEntry) {
//                val ce = e as CandleEntry
//                tvContent.setText(Utils.formatNumber(ce.getHigh(), 0, true));
//            } else {
//                tvContent.setText(Utils.formatNumber(e.getY(), 0, true));
//            }
            super.refreshContent(e, highlight);
        }


        override fun getOffset(): MPPointF {
            return MPPointF(-(getWidth()/2).toFloat(), -getHeight().toFloat())
        }

    }

}