package meet.sumra.app.utils

data class ContactConfig(
    val limit: Int?,
    val currentPage: Int?,
    val perPage: Int? = null,
    val nextPage: Int? = null,
    val lastPage: Int? = null,     // Limit contacts of page
    val search: String?,           // Count contacts of page
    val isFavorite: Boolean?,      // Search keywords
    val isRecently: Boolean?,      // Show contacts that is favorite
    val byLetter: String?,         // Show recently added contacts
    val groupId: String?,          // Show contacts by letter
    val sortBy: Array<String>?,    // Sort by field (name, email, phone)
    val sortOrder: Array<String>?, // Sort order
){
    companion object{
        fun getDefault() = ContactConfig(
            limit = null, currentPage = null, search = null, isFavorite = null, isRecently = null,
            byLetter = null, groupId  = null, sortBy  = arrayOf("name"), sortOrder = arrayOf("asc"))
        fun getByPagination(page: Int) = ContactConfig(
            limit = 32, currentPage = page, search = null, isFavorite = null, isRecently = null,
            byLetter = null, groupId  = null, sortBy  = arrayOf("name"), sortOrder = arrayOf("asc"))  // arrayOf("desc")
        fun getSearch(text: String) = ContactConfig(
            limit = 32, currentPage = null, search = text, isFavorite = null, isRecently = null,
            byLetter = null, groupId  = null, sortBy  = arrayOf("name"), sortOrder = arrayOf("asc"))
        fun getFavorite() = ContactConfig(
            limit = 32, currentPage = null, search = null, isFavorite = true, isRecently = null,
            byLetter = null, groupId  = null, sortBy  = arrayOf("name"), sortOrder = arrayOf("asc"))
    }
}
