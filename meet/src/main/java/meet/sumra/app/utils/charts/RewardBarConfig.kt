package meet.sumra.app.utils.charts

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.widget.TextView
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.core.view.isVisible
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.BarLineChartBase
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.*
import com.github.mikephil.charting.components.Legend.LegendForm
import com.github.mikephil.charting.components.XAxis.XAxisPosition
import com.github.mikephil.charting.components.YAxis.YAxisLabelPosition
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.datasets.IDataSet
import com.github.mikephil.charting.utils.MPPointF
import meet.sumra.app.R
import meet.sumra.app.ext.ClrExt
import meet.sumra.app.repository.Repository
import net.sumra.auth.helpers.Finals
import net.sumra.auth.helpers.LogUtil
import java.text.DecimalFormat

class RewardBarConfig(chart: BarChart) {

    private val tfLight: Typeface
    private val mContext: Context

    init {
        mContext = chart.context
        tfLight = Typeface.createFromAsset(mContext.assets, Finals.FONT_FACE)

//        chart.setOnChartValueSelectedListener(mContext)
        chart.setDrawBarShadow(false)
        chart.setDrawValueAboveBar(true)
//        chart.getData().getDataSets().let { it ->
//            for (set in it) {
//                set.setDrawValues(false)
//            }
//        }
//        chart.invalidate()

        chart.description.isEnabled = false

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        chart.setMaxVisibleValueCount(60)
        // scaling can now only be done on x- and y-axis separately
        chart.setPinchZoom(false)
        chart.isScaleYEnabled = false
        chart.isScaleXEnabled = true

        chart.setDrawGridBackground(false)
        // chart.setDrawYLabels(false);
        val xAxisFormatter: IAxisValueFormatter = DayAxisValueFormatter(chart)

        chart.xAxis.apply {
            position = XAxisPosition.BOTTOM
            typeface = tfLight
            setDrawGridLines(false)
            setDrawAxisLine(false)
            granularity = 1f // only intervals of 1 day
//            labelCount = 7
            axisLineColor
            textColor = R.color.gray_200 //Repository.color_gold.toInt()
            textSize = 9f
            setValueFormatter(xAxisFormatter as ValueFormatter)
        }

        val custom: IAxisValueFormatter = MyAxisValueFormatter()

        chart.axisLeft.apply {
//            typeface = tfLight
//            setLabelCount(8, false)
//            setValueForma0tter(custom as ValueFormatter)
//            setPosition(YAxisLabelPosition.OUTSIDE_CHART)
//            spaceTop = 15f
//            axisMinimum = 0f // this replaces setStartAtZero(true)
            textColor = R.color.gray_200
            isEnabled = false
        }

        chart.axisRight.apply {
//            setDrawGridLines(false)
//            typeface = tfLight
//            setLabelCount(8, false)
//            setValueFormatter(custom as ValueFormatter)
//            spaceTop = 15f
//            axisMinimum = 0f // this replaces setStartAtZero(true)
            textColor = R.color.gray_200
            isEnabled = false
        }

        chart.legend.apply {
//            verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
//            horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
//            orientation = Legend.LegendOrientation.HORIZONTAL
//            setDrawInside(false)
//            form = LegendForm.SQUARE
//            formSize = 9f
//            textSize = 11f
//            xEntrySpace = 4f
            textColor = R.color.gray_200
            isEnabled = false
        }

        // marker setup...
        val mv = XYMarkerView(mContext, xAxisFormatter as ValueFormatter)
        mv.setChartView(chart) // For bounds control
        chart.marker = mv      // Set the marker to the chart

        chart.invalidate()
    }

    class DayAxisValueFormatter(private val chart: BarLineChartBase<*>) : ValueFormatter() {
        private val mMonths = arrayOf(
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        )

        override fun getFormattedValue(value: Float/*, axis: AxisBase*/): String {
            val days  = value.toInt()
            val year  = determineYear(days)
            val month = determineMonth(days)
            val monthName = mMonths[month % mMonths.size]
            val yearName  = year.toString()
            return if (chart.visibleXRange > 30 * 6) {
                "$monthName $yearName"
            } else {
                val dayOfMonth = determineDayOfMonth(days, month + 12 * (year - 2016))
                var appendix = "th"
                when (dayOfMonth) {
                     1 -> appendix = "st"
                     2 -> appendix = "nd"
                     3 -> appendix = "rd"
                    21 -> appendix = "st"
                    22 -> appendix = "nd"
                    23 -> appendix = "rd"
                    31 -> appendix = "st"
                }
                if (dayOfMonth == 0) "" else "$dayOfMonth$appendix $monthName"
            }
        }

        private fun getDaysForMonth(month: Int, year: Int): Int {
            // month is 0-based
            if (month == 1) {
                var is29Feb = false
                if (year < 1582) is29Feb =
                    (if (year < 1) year + 1 else year) % 4 == 0 else if (year > 1582) is29Feb =
                    year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)
                return if (is29Feb) 29 else 28
            }
            return if (month == 3 || month == 5 || month == 8 || month == 10) 30 else 31
        }

        private fun determineMonth(dayOfYear: Int): Int {
            var month = -1
            var days = 0
            while (days < dayOfYear) {
                month = month + 1
                if (month >= 12) month = 0
                val year = determineYear(days)
                days += getDaysForMonth(month, year)
            }
            return Math.max(month, 0)
        }

        private fun determineDayOfMonth(days: Int, month: Int): Int {
            var count = 0
            var daysForMonths = 0
            while (count < month) {
                val year = determineYear(daysForMonths)
                daysForMonths += getDaysForMonth(count % 12, year)
                count++
            }
            return days - daysForMonths
        }

        private fun determineYear(days: Int): Int {
            return if (days <= 366) 2016 else if (days <= 730) 2017 else if (days <= 1094) 2018 else if (days <= 1458) 2019 else 2020
        }
    }

    private class MyAxisValueFormatter: ValueFormatter()  {

        private val mFormat: DecimalFormat  // ? = null

        init {
            mFormat = DecimalFormat("###.#")
//            mFormat = DecimalFormat("###,###,###,##0.0")
        }

//        override fun getFormattedValue(value: Float, axis: AxisBase): String {
//            return "${mFormat.format(value.toDouble())} $"
//        }

        override fun getFormattedValue(value: Float): String {
            return "${mFormat.format(value.toDouble())} $"
        }
    }

    @SuppressLint("ViewConstructor")
    class XYMarkerView(context: Context?, private val xAxisValueFormatter: ValueFormatter) :
        MarkerView(context, R.layout.frg_rewards_marker_chart) {
        private val tvContent: TextView
        private val format: DecimalFormat

        // runs every time the MarkerView is redrawn, can be used to update the
        // content (user-interface)
        override fun refreshContent(e: Entry, highlight: Highlight) {
            val s = (e.y % 60).toInt()
            val m = ((e.y - s) / 60).toInt()

            tvContent.text = e.data.toString() // "${m} min ${s} sec"

//            tvContent.text = String.format(
//                "x: %s, y: %s",
//                xAxisValueFormatter.getFormattedValue(e.x, null),
//                format.format(e.y.toDouble())
//            )
            super.refreshContent(e, highlight)
        }

        override fun getOffset(): MPPointF {
            return MPPointF((-(width / 2)).toFloat(), (-height).toFloat())
        }

        init {
            tvContent = findViewById(R.id.txtViewContent)
            format = DecimalFormat("###.0")
        }
    }

}