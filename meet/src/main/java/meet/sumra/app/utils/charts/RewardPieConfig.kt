package meet.sumra.app.utils.charts

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import net.sumra.auth.helpers.Finals

class RewardPieConfig(chart: PieChart) {

    private val mContext: Context
    private val mTypeface: Typeface

    init {
        mContext = chart.context
        mTypeface = Typeface.createFromAsset(mContext.assets, Finals.FONT_FACE)

        chart.apply {
            setBackgroundColor(Color.TRANSPARENT)

//        moveOffScreen()

            setUsePercentValues(true)
            getDescription().setEnabled(false)

//        viewFragment.frgRewardsDiagram.setCenterTextTypeface(viewModel.updateTypeFace())
//        viewFragment.frgRewardsDiagram.setCenterText(generateCenterSpannableText())

            setDrawHoleEnabled(true)
            setHoleColor(Color.TRANSPARENT)

            setTransparentCircleColor(Color.TRANSPARENT)
            setTransparentCircleAlpha(0)

            setHoleRadius(80f)
            setTransparentCircleRadius(100f)
//
            setDrawCenterText(false) // (true)
            setRotationEnabled(false)
            setHighlightPerTapEnabled(false)

//        viewFragment.frgRewardsDiagram.set
//
            setMaxAngle(270f) // HALF CHART
//
            setRotationAngle(135f)
            setCenterTextOffset(0f, -50f)


//        setData(2, 50f)

            chart.animateY(1400, Easing.EaseInOutQuad)

            getLegend().let { l ->
                l.isEnabled = false
//                l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
//                l.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
//                l.orientation = Legend.LegendOrientation.HORIZONTAL
//                l.setDrawInside(false)
//                l.xEntrySpace = 5f
//                l.yEntrySpace = 5f
//                l.yOffset = 5f
            }

            // entry label styling
            chart.setEntryLabelColor(Color.WHITE)
            chart.setEntryLabelTypeface(mTypeface)
            chart.setEntryLabelTextSize(12f)
        }

    }




}