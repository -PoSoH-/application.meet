//package meet.sumra.app.utils.converters
//
//import com.google.gson.Gson
//import com.google.gson.annotations.SerializedName
//
//class ReferralConvert {
//
//    fun refConvertByLink(args: String): RefByLink{
//        return Gson().fromJson(args, RefByLink::class.kotlin)
//    }
//    fun refConvertUnregistered(args: String): RefUnregistered{
//        return Gson().fromJson(args, RefUnregistered::class.kotlin)
//    }
//
//    fun refInviteConvert(args: String): RefInvite{
//        return Gson().fromJson(args, RefInvite::class.kotlin)
//    }
//    fun refSendConvert(args: String): RefReferral{
//        return Gson().fromJson(args, RefReferral::class.kotlin)
//    }
//    fun refRespConvert(versionKey: String, data: String): String {
//        return Gson().toJson(PostReferral(versionKey, data))
//    }
//    private data class PostReferral(
//        @SerializedName("version_key")
//        val versionKey: String,
//        @SerializedName("data")
//        val data: String)
//
//
//    fun refUserValidConverter(userID: Int, applicationID: Int, status: Int): String {
//        return Gson().toJson(PostValidUser(userID, applicationID, status))
//    }
//    private data class PostValidUser (
//        @SerializedName("user_id")
//        val referralID: Int,
//        @SerializedName("application_id")
//        val applicationID: Int,
//        @SerializedName("status")
//        val status: Int )
//
//    fun refReferrerValidConverter(referrerID: Int, applicationID: Int, status: Int): String {
//        return Gson().toJson(PostValidReferrer(referrerID, applicationID, status))
//    }
//    private data class PostValidReferrer (
//        @SerializedName("referral_id")
//        val referralID: Int,
//        @SerializedName("application_id")
//        val applicationID: Int,
//        @SerializedName("status")
//        val status: Int )
//
//}