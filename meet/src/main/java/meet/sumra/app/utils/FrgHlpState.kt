package meet.sumra.app.utils

import android.os.Parcel

sealed class FrgHlpState{
    data class FrgNewMeeting (val arg: Parcel?) : FrgHlpState()
    data class FrgJoinMeeting(val arg: Parcel?) : FrgHlpState()
}
